package com.privasia.procurehere.test;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.privasia.procurehere.core.entity.Supplier;
import com.privasia.procurehere.core.pojo.SupplierPojo;
import com.privasia.procurehere.service.OdelaIntegrationService;
import com.privasia.procurehere.web.config.WebAppConfig;

@RunWith(SpringJUnit4ClassRunner.class)
//@WebAppConfiguration
@ContextConfiguration(classes = WebAppConfig.class)
public class OdelaIntegrationServiceTest {

	@Autowired
	OdelaIntegrationService odelaIntegrationService;
	
	@Test
	@Ignore
	public void integrateSupplierIntoOdelaTest() {
		try {
			System.out.println("Sending email..."); 
			Supplier supplier =new Supplier();
			odelaIntegrationService.integrateSupplierIntoOdela(supplier);
			//System.out.println("Email gayo...");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
