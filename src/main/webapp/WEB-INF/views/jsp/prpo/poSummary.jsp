<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<sec:authorize access="hasRole('ADMIN')" var="isAdmin" />
<sec:authorize access="hasRole('ROLE_ADMIN_READONLY')" var="buyerReadOnlyAdmin" />
<sec:authentication property="principal.id" var="loggedInUserId" />
<jsp:include page="/WEB-INF/views/jsp/templates/message.jsp" />
<jsp:include page="/WEB-INF/views/jsp/templates/ajaxMessage.jsp" />
<spring:message code="eventsummary.checkbox.editor" var="editorLabel" />
<spring:message code="eventsummary.checkbox.viewer" var="viewerLabel" />
<spring:message code="eventsummary.checkbox.associate.owner" var="associateOwnerLabel" />
<style>
.ph_btn_custom {
	height: 40px !important;
	min-width: 170px !important;
	font-size: 16px !important;
	line-height: 39px;
	font-weight: 500;
}
</style>
<div class="clear"></div>
<div class="white-bg border-all-side float-left width-100 pad_all_15">
	<div class="row">
		<div class="col-md-6 col-sm-12 col-xs-12">
			<input type="hidden" id="poId" value="${po.id}">
			<div class="tag-line">
				<h2>
					<spring:message code="supplier.po.summary.poNumber" />
					: ${po.poNumber}
				</h2>
				<br>
				<h2>
					<spring:message code="supplier.po.summary.poDate" />
					: <fmt:formatDate value="${po.createdDate}" pattern="dd/MM/yyyy hh:mm a" timeZone="<%=request.getSession().getAttribute(\"timeZone\")%>" />
				</h2>
				<br>
				<c:if test="${not empty po.supplier}">
					<h2>
						<spring:message code="application.supplier" />
						: ${po.supplier.supplier.companyName}
					</h2>
				</c:if>
				<c:if test="${empty po.supplier}">
					<h2>
						<spring:message code="application.supplier" />
						: ${po.supplierName}
					</h2>
				</c:if>
			</div>
		</div>
	</div>
	

	<div class="pull-right">
		<form:form action="${pageContext.request.contextPath}/buyer/downloadPoReport/${po.id}" method="GET">
			<button class="btn btn-sm float-right btn-info hvr-pop marg-left-10 downloadPoBtn" data-toggle="tooltip" data-placement="top" data-original-title='<spring:message code="tooltip.prsummary.download.po" />'>
				<span class="glyph-icon icon-separator"> <i class="glyph-icon icon-download"></i>
				</span> <span class="button-content"><spring:message code="prsummary.download.po.button" /></span>
			</button>
		</form:form>
	</div>
	<c:if test="${(po.status ne 'DECLINED') and (po.status ne 'CANCELLED')}">
		<div class="pull-right">
			<button class="btn btn-sm float-right btn-danger hvr-pop marg-left-10  " type="submit" id="idCancelPo">
				<span class="button-content"><spring:message code="poSummary.cancelPO.label" /></span>
			</button>
		</div>
	</c:if>
	
	<c:if test="${(po.status eq 'READY') and (isAdmin or loggedInUserId eq po.createdBy.id ) and !buyerReadOnlyAdmin}">
		<div class="pull-right">
			<button class="btn btn-sm float-right btn-success hvr-pop marg-left-10" type="submit" id="idSendPo">
				<span class="button-content"><spring:message code="posummary.send.po.button" /></span>
			</button>
		</div>
	</c:if>
	
</div>
<div class="clear"></div>
<div class="tab-pane" style="display: block">
	<div class="upload_download_wrapper clearfix event_info">
		<h4>
			<spring:message code="prsummary.general.information" />
		</h4>
		<div class="form-tander1 requisition-summary-box">
			<div class="col-sm-4 col-md-3 col-xs-6">
				<label> <spring:message code="prSummary.pr.number" />
				</label>
			</div>
			<div class="col-sm-5 col-md-5 col-xs-6">
				<p><a href="${pageContext.request.contextPath}/buyer/prView/${po.pr.id}">${po.pr.prId}</a></p>
			</div>
		</div>
		<c:if test="${erpSetup.isErpEnable}">
			<div class="form-tander1 requisition-summary-box">
				<div class="col-sm-4 col-md-3 col-xs-6">
					<label> <spring:message code="prsummary.erp.doc.no" />
					</label>
				</div>
				<div class="col-sm-5 col-md-5 col-xs-6">
					<p>${po.erpDocNo}</p>
				</div>
			</div>
		</c:if>
		<div class="form-tander1 requisition-summary-box">
			<div class="col-sm-4 col-md-3 col-xs-6">
				<label> <spring:message code="poSummary.creator" />
				</label>
			</div>
			<div class="col-sm-5 col-md-5 col-xs-6">
				<p>
					${po.createdBy.name} <br> ${po.createdBy.communicationEmail} <br>
					<c:if test="${not empty po.buyer.companyContactNumber}">
						<spring:message code="prdraft.tel" />: ${po.buyer.companyContactNumber}</c:if>
					<c:if test="${not empty po.buyer.faxNumber}">
						<spring:message code="prtemplate.fax" />: ${po.buyer.faxNumber}</c:if>
					<c:if test="${not empty po.createdBy.phoneNumber}">
						<spring:message code="prtemplate.hp" />: ${ po.createdBy.phoneNumber}</c:if>
				</p>
			</div>
		</div>
		<div class="form-tander1 requisition-summary-box">
			<div class="col-sm-4 col-md-3 col-xs-6">
				<label> <spring:message code="pr.requester" />
				</label>
			</div>
			<div class="col-sm-5 col-md-5 col-xs-6">
				<p>${po.requester}</p>
			</div>
		</div>
		<div class="form-tander1 requisition-summary-box">
			<div class="col-sm-4 col-md-3 col-xs-6">
				<label> <spring:message code="pr.description" />
				</label>
			</div>
			<div class="col-sm-5 col-md-5 col-xs-6">
				<p>${po.description}</p>
			</div>
		</div>
	</div>
	<div class="upload_download_wrapper clearfix marg-top-10 event_info">
		<h4>
			<spring:message code="rfs.summary.finance.information" />
		</h4>
		<div class="form-tander1 requisition-summary-box">
			<div class="col-sm-4 col-md-3 col-xs-6">
				<label> <spring:message code="pr.base.currency" /> :
				</label>
			</div>
			<div class="col-sm-5 col-md-5 col-xs-6">
				<p>${po.currency.currencyCode}</p>
			</div>
		</div>
		<div class="form-tander1 requisition-summary-box">
			<div class="col-sm-4 col-md-3 col-xs-6">
				<label> <spring:message code="eventdescription.decimal.label" /> :
				</label>
			</div>
			<div class="col-sm-5 col-md-5 col-xs-6">
				<p>${po.decimal}</p>
			</div>
		</div>
		<div class="form-tander1 requisition-summary-box">
			<div class="col-sm-4 col-md-3 col-xs-6">
				<label> <spring:message code="eventdescription.costcenter.label" /> :
				</label>
			</div>
			<div class="col-sm-5 col-md-5 col-xs-6">
				<p>${po.costCenter.costCenter}-${po.costCenter.description}</p>
			</div>
		</div>
		<div class="form-tander1 requisition-summary-box">
			<div class="col-sm-4 col-md-3 col-xs-6">
				<label> <spring:message code="label.businessUnit" /> :
				</label>
			</div>
			<div class="col-sm-5 col-md-5 col-xs-6">
				<p>
					<c:if test="${empty po.businessUnit}">
						<spring:message code="application.not.applicable" />
					</c:if>${po.businessUnit.unitName}</p>
			</div>
		</div>
		<div class="form-tander1 requisition-summary-box">
			<div class="col-sm-4 col-md-3 col-xs-6">
				<label> <spring:message code="eventdescription.paymentterm.label" /> :
				</label>
			</div>
			<div class="col-sm-5 col-md-5 col-xs-6">
				<p>${po.paymentTerm}</p>
			</div>
		</div>
	</div>
	<div class="upload_download_wrapper clearfix marg-top-10 event_info">
		<h4>
			<spring:message code="pr.correspondence.address" />
		</h4>
		<div class="form-tander1 requisition-summary-box marg-bottom-20">
			<div class="col-sm-4 col-md-3 col-xs-6">
				<label class="set-line-height"><spring:message code="prsummary.address.title" /></label>
			</div>
			<div class="col-sm-5 col-md-5 col-xs-6">
				<p class="set-line-height">
					<c:if test="${not empty po.correspondAddressTitle}">
						${po.correspondAddressTitle} <br> ${po.correspondAddressLine1},
						<c:if test="${not empty po.correspondAddressLine2}">${po.correspondAddressLine2} ,</c:if>${ po.correspondAddressZip},
						<br>${ po.correspondAddressState},${ po.correspondAddressCountry}
					</c:if>
				</p>
			</div>
		</div>
	</div>

	<div class="upload_download_wrapper clearfix marg-top-10 event_info">
		<h4>
			<spring:message code="application.supplier.detail" />
		</h4>
		<div class="form-tander1 requisition-summary-box">
			<div class="col-sm-4 col-md-3 col-xs-6">
				<label><spring:message code="prsummary.supplier.info" /></label>
			</div>
			<div class="col-sm-5 col-md-5 col-xs-6">
				<c:if test="${not empty po.supplier}">
					<div class="">
						<%-- <h5>${pr.supplier.supplier.companyName}</h5> --%>
						<span class='desc'>${po.supplier.supplier.companyName}<br />${po.supplier.fullName}<br />${po.supplier.communicationEmail}<br />${po.supplier.companyContactNumber}</span>
					</div>
				</c:if>
				<c:if test="${empty po.supplier}">
					<div class="">
						<span class='desc'>${po.supplierName}<br />${po.supplierTelNumber}<br />${po.supplierFaxNumber}<br />${po.supplierTaxNumber}</span>
					</div>
				</c:if>
			</div>
		</div>
		<div class="form-tander1 requisition-summary-box">
			<div class="col-sm-4 col-md-3 col-xs-6">
				<label><spring:message code="prevent.supplier.address" /></label>
			</div>
			<div class="col-sm-5 col-md-5 col-xs-6">
				<c:if test="${not empty po.supplier}">
					<div class="">
						<span class='desc'>${po.supplier.supplier.line1}<br />${po.supplier.supplier.line2}<br />${po.supplier.supplier.city}</span>
					</div>
				</c:if>
				<c:if test="${empty po.supplier}">
					<div class="">
						<span class='desc'>${po.supplierAddress}</span>
					</div>
				</c:if>
			</div>
		</div>
	</div>
	<div class="upload_download_wrapper clearfix marg-top-10 event_info">
		<h4>
			<spring:message code="pr.delivery.detail" />
		</h4>
		<div class="form-tander1 requisition-summary-box marg-bottom-20">
			<div class="col-sm-4 col-md-3 col-xs-6">
				<label class="set-line-height"><spring:message code="prsummary.receiver" /></label>
			</div>
			<div class="col-sm-5 col-md-5 col-xs-6">
				<p class="set-line-height">${po.deliveryReceiver}</p>
			</div>
		</div>
		<div class="form-tander1 requisition-summary-box marg-bottom-20">
			<div class="col-sm-4 col-md-3 col-xs-6">
				<label class="set-line-height"><spring:message code="eventsummary.eventdetail.deliveryadds" /></label>
			</div>
			<div class="col-sm-5 col-md-5 col-xs-6">
				<p class="set-line-height">
					<c:if test="${not empty po.deliveryAddressTitle}">
						<h5>${po.deliveryAddressTitle}</h5>
						<span class='desc'>${po.deliveryAddressLine1}, ${po.deliveryAddressLine2}, ${po.deliveryAddressCity}, ${po.deliveryAddressZip}, ${po.deliveryAddressState}, ${po.deliveryAddressCountry}</span>
					</c:if>
				</p>
			</div>
		</div>
		<div class="form-tander1 requisition-summary-box ">
			<div class="col-sm-4 col-md-3 col-xs-6">
				<label><spring:message code="eventsummary.eventdetail.deliverydate" /> &amp; <spring:message code="application.time" /></label>
			</div>
			<div class="col-sm-5 col-md-5 col-xs-6">
				<p>
					<fmt:formatDate value="${po.deliveryDate}" pattern="dd/MM/yyyy hh:mm a" timeZone="<%=request.getSession().getAttribute(\"timeZone\")%>" />
				</p>
			</div>
		</div>
	</div>
	<div class="upload_download_wrapper clearfix marg-top-10 event_info">
		<h4>
			<spring:message code="prevent.purchase.item" />
		</h4>
			<div class="col-md-2 pull-right">
				<div class="marg-top-10 marg-bottom-10">
					<button class="btn btn-default hvr-pop hvr-rectangle-out3" id="downloadTemplate">
						<i class="excel_icon"></i>
						<spring:message code="prsummary.export.excel.button" />
					</button>
				</div>
			</div>
		<div class="pad_all_15 float-left width-100">
			<div class="main_table_wrapper ph_table_border mega">
				<table class="ph_table border-none header parentBlocks" border="0" cellspacing="0" cellpadding="0" width="100%">
					<thead>
						<tr>
							<th class="width_50 width_50_fix"><spring:message code="supplier.no.col" /></th>
							<th class="align-left width_200_fix"><spring:message code="label.rftbq.th.itemname" /></th>
							<th class="align-left width_100_fix"><spring:message code="label.rftbq.th.uom" /></th>
							<th class="align-right width_100 width_100_fix"><spring:message code="label.rftbq.th.quantity" /></th>
							<th class=" align-right width_100 width_100_fix"><spring:message code="label.rftbq.th.unitprice" /> (${po.currency})</th>
							<c:if test="${not empty po.field1Label}">
								<th class="align-left width_100 width_100_fix">${po.field1Label}</th>
							</c:if>
							<c:if test="${not empty po.field2Label}">
								<th class="align-left width_100 width_100_fix">${po.field2Label}</th>
							</c:if>
							<c:if test="${not empty po.field3Label}">
								<th class="align-left width_100 width_100_fix">${po.field3Label}</th>
							</c:if>
							<c:if test="${not empty po.field4Label}">
								<th class="align-left width_100 width_100_fix">${po.field4Label}</th>
							</c:if>
							<c:if test="${not empty po.field5Label}">
								<th class="align-left width_100 width_100_fix">${po.field5Label}</th>
							</c:if>
							<c:if test="${not empty po.field6Label}">
								<th class="align-left width_100 width_100_fix">${po.field6Label}</th>
							</c:if>
							<c:if test="${not empty po.field7Label}">
								<th class="align-left width_100 width_100_fix">${po.field7Label}</th>
							</c:if>
							<c:if test="${not empty po.field8Label}">
								<th class="align-left width_100 width_100_fix">${po.field8Label}</th>
							</c:if>
							<c:if test="${not empty po.field9Label}">
								<th class="align-left width_100 width_100_fix">${po.field9Label}</th>
							</c:if>
							<c:if test="${not empty po.field10Label}">
								<th class="align-left width_100 width_100_fix">${po.field10Label}</th>
							</c:if>

							<th class="width_150 width_150_fix align-right"><spring:message code="prtemplate.total.amount" /> (${po.currency})</th>
							<th class="width_150 align-right width_150_fix"><spring:message code="prtemplate.tax.amount" /> (${po.currency})</th>
							<th class="width_200 width_200_fix align-right"><spring:message code="prtemplate.total.amount.tax" /> (${po.currency})</th>
						</tr>
					</thead>
				</table>
				<table class="data ph_table border-none childBlocks" border="0" cellspacing="0" cellpadding="0" width="100%">
					<tbody>
						<c:forEach items="${poItemlist}" var="item">
							<tr>
								<td class="width_50 width_50_fix">${item.level}.${item.order}</td>
								<td class="align-left width_200_fix">${item.itemName}<c:if test="${not empty item.itemDescription}">
										<span class="item_detail s1_view_desc"><spring:message code="application.view.description" /></span>
									</c:if>
									<p class="s1_tent_tb_description s1_text_small">${item.itemDescription}</p>
								</td>
								<td class="align-left width_100_fix">${item.product.uom.uom}</td>
								<td class="align-right width_100 width_100_fix"><fmt:formatNumber type="number" minFractionDigits="${po.decimal}" maxFractionDigits="${po.decimal}" value="${item.quantity}" /> </td>
								<td class=" align-right width_100 width_100_fix"><fmt:formatNumber type="number" minFractionDigits="${po.decimal}" maxFractionDigits="${po.decimal}" value="${item.unitPrice}" /></td>
								<c:if test="${not empty po.field1Label}">
									<td class=" align-left width_100 width_100_fix">${item.field1}&nbsp;</td>
								</c:if>
								<c:if test="${not empty po.field2Label}">
									<td class="align-left width_100 width_100_fix">${item.field2}&nbsp;</td>
								</c:if>
								<c:if test="${not empty po.field3Label}">
									<td class="align-left width_100 width_100_fix">${item.field3}&nbsp;</td>
								</c:if>
								<c:if test="${not empty po.field4Label}">
									<td class="align-left width_100 width_100_fix">${item.field4}&nbsp;</td>
								</c:if>
								<c:if test="${not empty po.field5Label}">
									<td class=" align-left width_100 width_100_fix">${item.field5}&nbsp;</td>
								</c:if>
								<c:if test="${not empty po.field6Label}">
									<td class="align-left width_100 width_100_fix">${item.field6}&nbsp;</td>
								</c:if>
								<c:if test="${not empty po.field7Label}">
									<td class="align-left width_100 width_100_fix">${item.field7}&nbsp;</td>
								</c:if>
								<c:if test="${not empty po.field8Label}">
									<td class="align-left width_100 width_100_fix">${item.field8}&nbsp;</td>
								</c:if>

								<c:if test="${not empty po.field9Label}">
									<td class=" align-left width_100 width_100_fix">${item.field9}&nbsp;</td>
								</c:if>
								<c:if test="${not empty po.field10Label}">
									<td class="align-left width_100 width_100_fix">${item.field10}&nbsp;</td>
								</c:if>

								<td class="width_150 width_150_fix align-right"><c:if test="${item.order != '0' }">
										<fmt:formatNumber type="number" minFractionDigits="${po.decimal}" maxFractionDigits="${po.decimal}" value="${item.totalAmount}" />
									</c:if></td>
								<td class="width_150 align-right width_150_fix"><c:if test="${item.order != '0' }">
										<fmt:formatNumber type="number" minFractionDigits="${po.decimal}" maxFractionDigits="${po.decimal}" value="${item.taxAmount}" />
									</c:if></td>
								<td class="width_200 width_200_fix align-right"><c:if test="${item.order != '0' }">
										<fmt:formatNumber type="number" minFractionDigits="${po.decimal}" maxFractionDigits="${po.decimal}" value="${item.totalAmountWithTax}" />
									</c:if></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>

		<div class="total_all total-with-tax-final table-resposive" style="font-size: 16px;">
			<table cellspacing="3" cellpadding="3" style="width: 98.8%; border-collapse: separate; border-spacing: 8px; margin-right: 20px;">
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td class="align-left" style="white-space: nowrap;"><strong><spring:message code="prsummary.total2" /> (${po.currency}) :</strong></td>
					<td style="white-space: nowrap;"><fmt:formatNumber type="number" minFractionDigits="${po.decimal}" maxFractionDigits="${po.decimal}" value="${po.total}" /></td>
				</tr>
				<tr>
					<td style="white-space: nowrap; vertical-align: top;"><strong><spring:message code="prtemplate.additional.charges" />:</strong></td>
					<td style="width: 100%; padding-left: 10px; padding-right: 10px;">${po.taxDescription}</td>
					<td style="white-space: nowrap; vertical-align: top;" class="align-left"><strong>(${po.currency}):</strong></td>
					<td style="white-space: nowrap; vertical-align: top;" class="align-right"><fmt:formatNumber type="number" minFractionDigits="${po.decimal}" maxFractionDigits="${po.decimal}" value="${po.additionalTax}" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td style="white-space: nowrap;" class="align-left"><strong><spring:message code="submission.report.grandtotal" /> (${po.currency}):</strong></td>
					<td style="white-space: nowrap;" class="align-right"><fmt:formatNumber type="number" minFractionDigits="${po.decimal}" maxFractionDigits="${po.decimal}" value="${po.grandTotal}" /></td>
				</tr>
			</table>
		</div>

	</div>
	<div class="upload_download_wrapper clearfix marg-top-10 event_info remark-tab">
		<h4>
			<spring:message code="Product.remarks" />
		</h4>
		<div class="pad_all_15 float-left width-100">
			<label><spring:message code="prtemplate.general.remark" /></label>
			<p>${po.remarks}</p>
		</div>
		<div class="pad_all_15 float-left width-100">
			<label><spring:message code="prtemplate.terms.condition" /></label>
			<p>${po.termsAndConditions}</p>
		</div>
	</div>

	<jsp:include page="poAudit.jsp" />
</div>
<!-- send po popup  -->
<div class="modal fade" id="confirmSendPo" role="dialog">
	<div class="modal-dialog for-delete-all reminder">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h3>
					<spring:message code="poSummary.confirm.sendPO" />
				</h3>
				<button class="close for-absulate" type="button" data-dismiss="modal">&times;</button>
			</div>
			<%-- <form id="" action="${pageContext.request.contextPath}/buyer/sendPo" method="post"> --%>
			<form id="sendPoForm" method="post">
				<input type="hidden" name="poId" value="${po.id}"> <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				<div class="col-md-12">
					<div class="row">
						<div class="modal-body col-md-12">
							<label> <spring:message code="posummary.sure.want.sendPo" />
							</label>
						</div>
					</div>
				</div>
				<div class="modal-footer pad_all_15 float-left width-100 border-top-width-1">
					<input type="button" id="sendPo" class="btn btn-info ph_btn_small hvr-pop hvr-rectangle-out pull-left" value="Yes">
					<button type="button" class="btn btn-black btn-default ph_btn_small hvr-pop hvr-rectangle-out1 pull-right" data-dismiss="modal">
						<spring:message code="application.no2" />
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- cancel po popup  -->
<div class="modal fade" id="confirmCancelPo" role="dialog">
	<div class="modal-dialog for-delete-all reminder">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h3>
					<spring:message code="poSummary.confirm.cancelPo" />
				</h3>
				<button class="close for-absulate" type="button" data-dismiss="modal">&times;</button>
			</div>
			<%-- <form id="" action="${pageContext.request.contextPath}/buyer/cancelPo" method="post"> --%>
			<form id="cancelPoForm" method="post">
				<input type="hidden" name="poId" value="${po.id}"> <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				<div class="col-md-12">
					<div class="row">
						<div class="modal-body col-md-12">
							<label> <spring:message code="posummary.sure.want.cancelPo" />
							</label>
						</div>
						<div class="form-group col-md-6">
							<spring:message code="po.reason.cancellation.placeholder" var="reasonCancellation" />
							<textarea class="width-100" placeholder="${reasonCancellation}" rows="3" name="poRemarks" id="poRemarks" data-validation="required length" data-validation-length="max500"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer pad_all_15 float-left width-100 border-top-width-1">
					<input type="button" id="cancelPo" class="btn btn-info ph_btn_small hvr-pop hvr-rectangle-out pull-left" value="Yes">
					<button type="button" class="btn btn-black btn-default ph_btn_small hvr-pop hvr-rectangle-out1 pull-right" data-dismiss="modal">
						<spring:message code="application.no2" />
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
<style>
.d-flex {
	display: flex;
}

.center-btn {
	text-align: center;
	margin: 0 auto;
	margin-top: 25px !important;
}

.width-90 {
	width: 90%;
}

.editTeamMemberList {
	margin-left: 50px;
}

div.radio[id^='uniform-']>span {
	margin-top: 0 !important;
}

label.select-radio {
	width: auto;
}

#event {
	padding-left: 0;
}

.input-group.mrg15T.mrg15B {
	background-color: #f5f5f5;
	margin-bottom: 0;
	padding: 0;
}

.margeinAllMDZero {
	margin: 0;
	clear: both;
}

.marginBottomA {
	margin-bottom: 20px;
}

#appUsersList td {
	padding: 5px;
}

.mem-tab {
	border: 1px solid #ddd;
	border-radius: 2px;
	float: left;
	height: 300px;
	overflow: auto;
	position: relative;
	width: 100%;
}

.box_active {
	background: rgba(0, 0, 0, 0) none repeat scroll 0 0 !important;
}

.caret {
	color: #fff !important;
}

.cq_row_parent .input-group-btn {
	width: 200px;
}

.dropdown-menu input {
	display: inline !important;
	width: auto !important;
}

.advancee_menu ul {
	top: 100% !important;
	left: 0 !important;
}

.dropdown-menu .checkbox {
	display: inline-block !important;
	margin: 0 !important;
	padding: 0 !important;
	position: relative !important;
}

.dropdown-menu .checkbox input[type="checkbox"] {
	position: relative !important;
	margin-left: 0 !important;
}

.open>.dropdown-menu {
	padding-bottom: 17px;
	padding-top: 0;
}

#appUsersList tr:nth-child(odd) {
	background: #FFF
}

#appUsersList tr:nth-child(even) {
	background: #CCC
}

#eventTeamMembersList td, #eventTeamMembersList th {
	text-align: left !important;
	max-width: 100px !important;
}

#eventTeamMembersList {
	margin: 0 !important;
}

#eventTeamMembersList_length, #eventTeamMembersList_info,
	#eventTeamMembersList_paginate {
	display: none;
}

.dataTables_wrapper.form-inline.no-footer {
	overflow: hidden;
}

.editTeamMemberList {
	margin-left: 50px;
}

.grand-price-heading {
	width: 250px;
}

table.dataTable thead tr.tableHeaderWithongoing th.sorting_asc::after {
	content: "" !important;
}

table.dataTable thead tr.tableHeaderWithongoing th.sorting::after {
	content: "" !important
}

.inactiveCaption {
	margin: 0 0px 0 0px !important;
	font-weight: bold !important;
	color: #ff1d33 !important;
}

.white-space-pre {
	white-space: pre;
}
</style>

<script type="text/javascript">
	$('document').ready(function() {
		$('.childBlocks').css('margin-top', $('.parentBlocks').height());
		$('#downloadTemplate').click(function(e) {
			e.preventDefault();
			var poId = $.trim($('#poId').val());
			window.location.href = getContextPath() + "/buyer/exportPoItemTemplate/" + poId;
		});
		$('#idSendPo').click(function(e) {
			$('#confirmSendPo').modal('show');
		});
		
		$('#idCancelPo').click(function(e) {
			$('#confirmCancelPo').modal('show');
		});
	});
</script>
<script type="text/javascript" src="<c:url value="/resources/assets/js-core/jquery.form-validator.js"/>"></script>
<script>
	<c:if test="${buyerReadOnlyAdmin }">
	$(window).bind('load', function() {
		var allowedFields = '#nextButton,#dashboardLink,#previousButton,#bubble, #downloadButton,#idSumDownload,.s1_view_desc,.bluelink,#approvalremarks, .downloadPoBtn, #downloadTemplate';
		//var disableAnchers = ['#reloadMsg'];        
		disableFormFields(allowedFields);
	});
	</c:if>
	$.validate({
		lang : 'en',
		modules : 'file'
	});
</script>

<script type="text/javascript">
	$('document').ready(function() {
		
		$('#sendPo').on('click', function(e) {
			e.preventDefault();
			if($("#sendPoForm").isValid()) {
				$(this).addClass('disabled');
	 			$('#idSendPo').addClass('disabled');
				$('#idCancelPo').addClass('disabled');
	 			$('#sendPoForm').attr('action', getContextPath() + '/buyer/sendPo');
				$("#sendPoForm").submit();
			}else{
				return;
			}
		});
		
		$('#cancelPo').on('click', function(e) {
			e.preventDefault();
			if($("#cancelPoForm").isValid()) {
				$(this).addClass('disabled');
				$('#idCancelPo').addClass('disabled');
	 			$('#idSendPo').addClass('disabled');
	 			$('#cancelPoForm').attr('action', getContextPath() + '/buyer/cancelPo');
				$("#cancelPoForm").submit();
			}else{
				return;
			}
		});
		
	});
</script>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/elements/cq_form.css"/>">