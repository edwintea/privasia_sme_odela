<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/elements/procurehere1.css"/>">
<div id="page-content-wrapper">
	<div id="page-content">
		<div class="container">
			<!-- pageging  block -->
			<ol class="breadcrumb">
				<c:url var="buyerDashboard" value="/buyer/buyerDashboard" />
				<li><a href="${buyerDashboard}"> <spring:message code="application.dashboard" />
				</a></li>
				<li class="active"><spring:message code="request.purchase.requisition" /></li>
			</ol>
			<div class="Section-title title_border gray-bg mar-b20">
				<h2 class="trans-cap supplier">
					<spring:message code="request.purchase.requisition" />
					<h2 class="trans-cap pull-right">Status : ${sourcingFormRequest.status}</h2>
				</h2>
			</div>
			<div class="clear"></div>
			<jsp:include page="requestSummary.jsp"></jsp:include>


			<c:if test="${sourcingFormRequest.status eq 'APPROVED' or sourcingFormRequest.status eq 'FINISHED' }">
				<div class="btn-group dropup">
					<button type="button" class="btn btn-warning dropdown-toggle1" id="crateNewEventId" data-toggle="dropdown" aria-expanded="false"><spring:message code="application.create.event" /></button>
				</div>
			</c:if>


		</div>
	</div>
</div>



<div class="modal fade" id="crateNewEvent" role="dialog">
	<div class="modal-dialog for-delete-all reminder">
		<div class="modal-content">
			<div class="modal-header">
				<h3>Create Event</h3>
				<button class="close for-absulate" type="button" data-dismiss="modal">&times;</button>
			</div>
			<jsp:include page="createNewEventArchive.jsp"></jsp:include>
		</div>
	</div>
</div>

<script>

$(document).ready(function() {
	
	 $('#crateNewEventId').click(function (event) {
		 
	        $('#crateNewEvent').modal('show')
	    });
});


</script>