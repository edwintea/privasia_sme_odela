<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div id="page-wrapper">
	<div id="page-content-wrapper">
		<div id="page-content">
			<div class="container">
				<ol class="breadcrumb">
					<li><a
						href="${pageContext.request.contextPath}/buyer/buyerDashboard">
							<spring:message code="application.dashboard" />
					</a></li>
					<li class="active"><spring:message
							code="rfs.sourcing.form.creation" /></li>
				</ol>
				<!-- page title block -->
				<div class="example-box-wrapper wigad-new">
					<div class="rft-creater-heading marg-top-10">
						<jsp:include page="/WEB-INF/views/jsp/templates/message.jsp" />
						<jsp:include page="/WEB-INF/views/jsp/templates/ajaxMessage.jsp" />
						<div class="alert alert-notice" id="idEventInfo"
							style="display: none">
							<div class="bg-blue alert-icon">
								<i class="glyph-icon icon-info"></i>
							</div>
							<div class="alert-content">
								<h4 class="alert-title">Info</h4>
								<p id="idEventInfoMessage">
									Information message box using the
									<code>.alert-notice</code>
									color scheme. <a title="Link" href="#">Link</a>
								</p>
							</div>
						</div>
						<div class="alert alert-notice row" id="idTemplateInfo"
							style="display: none">
							<div class="bg-blue alert-icon">
								<i class="glyph-icon icon-info"></i>
							</div>
							<div class="alert-content">
								<h4 class="alert-title">Info</h4>
								<p id="idTemplateInfoMessage">
									Information message box using the
									<code>.alert-notice</code>
									color scheme. <a title="Link" href="#">Link</a>
								</p>
							</div>
						</div>
						<h3 class="marg-bottom-10">
							<spring:message code="rfs.like.request.sourcing" />
						</h3>
						<div class="rft-creater-heading-inner example-box-wrapper">
							<div class="row">
								<div class="col-md-12 ">
									<ul class="nav-responsive nav nav-tabs bg-gradient-9">
										<li class="active"><a href="#tabTemplate"
											class="createEventsTopTabs" data-toggle="tab"
											id="tabTemplateId"> <img
												src="${pageContext.request.contextPath}/resources/images/new-tender.png"
												alt="New Tender" /> <span><spring:message
														code="rfs.new.request.label" /></span>
										</a></li>
										<li><a href="#tabPrevious" data-toggle="tab"
											id="tabPreviousId"> <img
												src="${pageContext.request.contextPath}/resources/images/copy-from-privious.png"
												alt=" Copy Previous" /> <span> <spring:message
														code="rfa.createrft.copyfromprevious2" /></span>
										</a></li>
										<div class="pull-right searchTemplatefield">
											<h3 class="marg-template row">
												<spring:message code="rfi.createrfi.search.fromlist" />
											</h3>
											<div class="row">
												<div class="col-md-4 col-md-4-custom">
													<input name="templateName" id="idTemplateName"
														placeholder='<spring:message code="createrfi.template.name.placeholder"/>'
														type="text" data-validation="alphanumeric length "
														data-validation-allowing="/ "
														data-validation-length="max64" class="form-control"
														style="width: 108%" />
												</div>
												<div class="col-md-2">
													<button
														class="searchSourcingTemplate btn ph_btn_small btn-info hvr-pop hvr-rectangle-out"
														type="submit">
														<spring:message code="application.search" />
													</button>
												</div>
											</div>
										</div>
										<div class="pull-right searchpreviousfield flagvisibility">
											<h3 class="marg-previous row">
												<spring:message code="rfs.search.list.previously" />
											</h3>
											<div class="row">
												<div class="col-md-4 col-md-4-custom">
													<input name="formName" id="searchValue"
														placeholder='<spring:message code="rfs.sourcing.form.name"/>'
														class="form-control" style="width: 108%" />
												</div>
												<div class="col-md-2">
													<button
														class="searchrftEvent btn ph_btn_small btn-info hvr-pop hvr-rectangle-out"
														type="submit">
														<spring:message code="application.search" />
													</button>
												</div>
											</div>
										</div>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="tabTemplate">
											<div class="list-table-event marg-top-20">
												<div class="col-md-3 col-sm-6" style="padding-left: 0;"></div>
												<div class="col-md-9 col-sm-6"></div>
											</div>
											<div class="privious-box-main pad_all_20 white-bg"
												id="prTemplates">
												<div class="row">
													<c:if test="${empty allSourcingTemplate }">
														<h4>
															<a style="background: none; border: none;"><spring:message
																	code="application.not.templates" /></a>
														</h4>
													</c:if>
													<c:forEach items="${allSourcingTemplate}"
														var="sourcingTemp">
														<div
															class="col-md-3 marg-bottom-10 idRftEvent currentTemplates"
															id="${sourcingTemp.id}" data-value="${sourcingTemp.id}"
															style="display: block">
															<div
																class="lower-bar-search-contant-main-block copy-frm-prev onHoverDiv"
																id="test">
																<div
																	class="lower-bar-search-contant-main-block-heading light-gray-bg pad_all_10">
																	<h4 class="text-ellipsis-x"
																		title="${sourcingTemp.formName}">${sourcingTemp.formName}</h4>
																</div>
																<div
																	class="lower-bar-search-contant-main-contant pad-top-side-5 descBlock disp-flex">
																	<div class="green text-ellipsis-x description test">
																		<label class="pull-left "><spring:message
																				code="application.description" /> :</label><span class="desc-color" data-toggle="tooltip" data-original-title="${sourcingTemp.description}">${sourcingTemp.description}</span></div>
																</div>
																<div
																	class="lower-bar-search-contant-main-contant pad-top-side-5">
																	<label><spring:message
																			code="application.createdby" /> :</label> <span class="green">${sourcingTemp.createdBy.name}</span>
																</div>
																<div
																	class="lower-bar-search-contant-main-contant pad-top-side-5">
																	<label><spring:message
																			code="application.createddate" /> :</label> <span
																		class="green"> <fmt:formatDate
																			value="${sourcingTemp.createdDate}"
																			timeZone="${sessionScope['timeZone']}"
																			pattern="dd/MM/yyyy hh:mm a" />
																	</span>
																</div>
																<div
																	class="lower-bar-search-contant-main-contant  pad_all_10">
																	<div>
																		<spring:url value="/buyer/copyFromSourcingTemplate"
																			var="newSourcing" htmlEscape="true" />
																		<form action="${newSourcing}" class="col-md-12"
																			method="post" style="float: right;">
																			<input type="hidden" id="sourcingTemplateId"
																				value="${sourcingTemp.id}" name="sourcingTemplateId">
																			<input type="hidden" name="${_csrf.parameterName}"
																				value="${_csrf.token}" />
																			<button
																				class="btn btn-info btn-block hvr-pop hvr-rectangle-out"
																				style="width: 100%" type="submit">
																				<spring:message code="application.use.this.button" />
																			</button>
																		</form>
																	</div>
																</div>
															</div>
														</div>
													</c:forEach>
												</div>
											</div>
										</div>
										<div class="tab-pane" id="tabPrevious">
											<div
												class="privious-box-main marg-top-20 pad_all_20 white-bg"
												id="rftEvents">
												<div class="row">
													<c:if test="${empty allSourcingRequest}">
														<h4>
															<spring:message code="rfs.no.past.sourcing" />
														</h4>
													</c:if>
													<c:forEach items="${allSourcingRequest}"
														var="sourcingFormRequest">
														<div
															class="col-md-3 marg-bottom-10 idRftEvent currentTemplates"
															id="${sourcingFormRequest.id}"
															data-value="${sourcingFormRequest.id}"
															style="display: block">
															<div
																class="lower-bar-search-contant-main-block copy-frm-prev"
																id="test">
																<div
																	class="lower-bar-search-contant-main-block-heading light-gray-bg pad_all_10">
																	<h4>${sourcingFormRequest.sourcingFormName}</h4>
																</div>
																<div
																	class="lower-bar-search-contant-main-contant pad-top-side-5">
																	<label><spring:message
																			code="eventdetails.event.referencenumber" /> :</label> <span
																		class="green">${sourcingFormRequest.referanceNumber}</span>
																</div>
																<div
																	class="lower-bar-search-contant-main-contant pad-top-side-10">
																	<label><spring:message
																			code="application.createdby" /> :</label> <span class="green">
																		${sourcingFormRequest.createdBy.name}</span>
																</div>
																<div
																	class="lower-bar-search-contant-main-contant pad-top-side-10">
																	<label><spring:message
																			code="application.createddate" /> : </label> <span
																		class="green"> <fmt:formatDate
																			pattern="dd/MM/yyyy hh:mm a"
																			value="${sourcingFormRequest.createdDate}"
																			timeZone="<%=request.getSession().getAttribute(\"timeZone\")%>" />
																	</span>
																</div>
																<div
																	class="lower-bar-search-contant-main-contant  pad_all_10">
																	<spring:url value="/buyer/copyFromSourcingFormRequest"
																		var="copyFromSourcingFormRequest" htmlEscape="true" />
																	<form action="${copyFromSourcingFormRequest}"
																		class="col-md-12" method="post" style="float: right;">
																		<input type="hidden" id="formId"
																			value="${sourcingFormRequest.id}" name="formId">
																		<input type="hidden" name="${_csrf.parameterName}"
																			value="${_csrf.token}" />
																		<button
																			class="btn btn-info btn-block hvr-pop hvr-rectangle-out"
																			data-toggle="tooltip" data-placement="top"
													                    	data-original-title="Create New RFS"
																			style="width: 100%" type="submit">Use This</button> 
																	</form>
																</div>
															</div>
															<div class="flagvisibility dialogBox"
																id="${sourcingFormRequest.id}_dia">hello How are
																you</div>
														</div>
													</c:forEach>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:if test="${not empty openModelBu}">
		<div class="modal fade" id="myModal-auction" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<form
					action="${pageContext.request.contextPath}/buyer/copyFromSourcingFormRequest"
					method="post">
					<div class="modal-content">
						<div class="modal-header">
							<h3>
								<spring:message code="prevent.bu.would.like" />
							</h3>
							<button class="close for-absulate" type="button"
								data-dismiss="modal">�</button>
						</div>
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<div class="modal-body">
							<div class="auction-body">
								<input type="hidden" name="formId" value="${formId}"> <select
									name="businessUnitId" class="chosen-select disablesearch"
									id="idSettingType">
									<c:forEach items="${businessUnits}" var="businessUnit">
										<option value="${businessUnit.id}">${businessUnit.unitName}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div
							class="modal-footer border-none float-left width-100 pad-top-0 ">
							<button type="submit"
								class="btn btn-info ph_btn_small hvr-pop hvr-rectangle-out marg-left-20 auctionTypeButton">
								<spring:message code="application.next" />
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</c:if>
	<c:if test="${not empty openModelForTemplateBu}">
		<div class="modal fade" id="myModal-template" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<form
					action="${pageContext.request.contextPath}/buyer/copyFromSourcingTemplate"
					method="post">
					<div class="modal-content" style="width: 100%; float: left;">
						<div class="modal-header">
							<h3>
								<spring:message code="prevent.bu.would.like.template" />
							</h3>
							<button class="close for-absulate" type="button"
								data-dismiss="modal">�</button>
						</div>
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<div class="modal-body">
							<div class="auction-body">
								<input type="hidden" name="templateId" value="${templateId}">
								<select name="businessUnitId"
									class="chosen-select disablesearch" id="idSettingType">
									<c:forEach items="${businessUnits}" var="businessUnit">
										<option value="${businessUnit.id}">${businessUnit.unitName}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div
							class="modal-footer border-none float-left width-100 pad-top-0 ">
							<button type="submit"
								class="btn btn-info ph_btn_small hvr-pop hvr-rectangle-out marg-left-20 auctionTypeButton">
								<spring:message code="application.next" />
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</c:if>
	<div class="modal fade" id="myModal-blank" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<c:set var="createBlankUrl"
				value="${pageContext.request.contextPath}/buyer/copyFromSourcingTemplate" />
			<form action="${createBlankUrl}" method="post">
				<div class="modal-content" style="width: 100%; float: left;">
					<div class="modal-header">
						<h3>
							<spring:message code="prevent.bu.would.like.template" />
						</h3>
						<button class="close for-absulate" type="button"
							data-dismiss="modal">�</button>
					</div>
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" /> <input type="hidden"
						name="sourcingTemplateId" value="${sourcingTemplateId}" />
					<div class="modal-body">
						<select name="businessUnitId" class="chosen-select disablesearch">
							<c:forEach items="${businessUnits}" var="businessUnit">
								<option value="${businessUnit.id}">${businessUnit.unitName}</option>
							</c:forEach>
						</select>
					</div>
					<div
						class="modal-footer border-none float-left width-100 pad-top-0 ">
						<button type="submit"
							class="btn btn-info ph_btn_small hvr-pop hvr-rectangle-out marg-left-20 ">
							<spring:message code="application.next" />
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<style>
/* .idRftEvent {
	min-height: 230px;
} */
.desc-color {
	color: #06ccb3 !important;
}
.tooltip { 
    left: 80px !important;
}
.h-30 .tooltip-inner { 
    word-break: break-all;
}
.currentTemplates>div>div:last-child {
	position: absolute;
	bottom: 10px;
}

#rftEvents {
	min-height: 350px;
}

.marg-template {
	margin-bottom: 5px;
	margin-top: 10px;
	margin-left: 0px;
	margin-right: 94px;
	color: white !important;
}

.marg-previous {
	margin-bottom: 5px;
	margin-top: 10px;
	margin-left: 0px;
	margin-right: 10px;
	color: white !important;
}

.col-md-4.col-md-4-custom {
	width: 68.333333%;
}

.copy-frm-prev {
	min-height: 205px !important;
	/* overflow-y: scroll; */ 
}

.disp-flex {
	display: flex;
}

@media ( max-width : 1366px) and (min-width: 768px) {
	.text-ellipsis-x {
		width: 215px !important;
		white-space: nowrap !important;
		overflow: hidden;
		text-overflow: ellipsis;
	}
	.copy-frm-prev {
		min-height: 225px !important;
		/* overflow-y: scroll; */ 
	}	
}
.lower-bar-search-contant-main-contant span {    
    white-space: nowrap;
   }
.text-ellipsis-x {
	white-space: nowrap !important;
	width: 300px;
	overflow: hidden;
	text-overflow: ellipsis;
	display: block;
	cursor: pointer;
}

.currentTemplates>div>div:last-child {
    position: relative !important;
    bottom: 5px;
}

/* #test:hover */
/* #test:hover {
cursor: pointer;
     overflow-y: scroll; 
     min-height: 205px; 
} */
</style>
	<script type="text/javascript"
		src="<c:url value="/resources/js/view/createSourcingRequest.js"/>"></script>
	<script type="text/javascript">
	$("#test-select ").treeMultiselect({
		enableSelectAll : true,
		sortable : true
	});
</script>
	<script type="text/javascript"
		src="<c:url value="/resources/js/numeral.min.js"/>"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/js/jquery.form-validator.js"/>"></script>
	<script>
	$.validate({
		lang : 'en',
		modules : 'date,sanitize'
	});
	$(document).ready(function(){
		<c:if test="${not empty openModelForTemplateBu}">
		console.log("hiiiii");
		$('#myModal-blank').modal();
		</c:if>
		
		$('body').tooltip({
		    selector: '[data-toggle="tooltip"]'
		});
	});
	
	
 
</script>
	<script type="text/javascript"
		src="<c:url value="/resources/js/jquery.mask.min.js"/>"></script>