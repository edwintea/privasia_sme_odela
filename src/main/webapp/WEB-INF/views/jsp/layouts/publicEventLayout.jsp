<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="shortcut icon" href="<c:url value="/resources/assets/images/icons/favicon.png"/>">
<link rel="stylesheet" type="text/css" href="<c:url value="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"/>">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/bootstrap/css/bootstrap.css"/>">
<script type="text/javascript" src="<c:url value="/resources/assets/widgets/jquery-ui.min/jquery-3.5.1.min.js?1"/>"></script>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/elements/privasia1.css"/>">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/elements/privasia2.css"/>">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/themes/admin/procurehere.css?1"/>">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/themes/admin/color-schemes/Procurehere-theme.css"/>">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/themes/components/default.css"/>">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/themes/admin/layout.css"/>">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/elements/procurehere-public.css"/>">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/elements/open-sans.css"/>">


<!-- ICONS -->
<script type="text/javascript" src="<c:url value="/resources/js/jquery.scrollTo.min.js"/>"></script>
<!-- Admin responsive -->
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/helpers/admin-responsive.css"/>">
<script src="<c:url value="/resources/assets/widgets/jquery-ui.min/jquery-ui.min.js"/>"></script>
<title><tiles:insertAttribute name="title" /></title>

<style>
.pos-t-100 {
	position: relative;
	top: 100px;
}
.w-100 {
width: 100%;
}
#main {
	overflow-x: hidden;
}
#page-content {
    margin-left: 0;
}
#page-content-wrapper {
    z-index: 0;
}
.logo-width-27 {
    width: 27%;
}
</style>

<script type="text/javascript">
	function getContextPath() {
		return "${pageContext.request.contextPath}";
	}
	
	$(window).scroll(function(){
		
		  var header_top = $('.header_top');
		   scroll = $(window).scrollTop();
		  if (scroll >= 10) {header_top.addClass('fixed-head');			
		  $('.logo').addClass('logo-width-27');
		  $('.header-bottom').addClass('header-btm-55');  
		  $('.logo a img').addClass('logo-img-size');
		  }
		  else {header_top.removeClass('fixed-head');
		  $('.logo').removeClass('logo-width-27');
		  $('.header-bottom').removeClass('header-btm-55');  
		  $('.logo a img').removeClass('logo-img-size');  
		  }
		});
</script>

</head>
<body>

	<header id="header">
		<tiles:insertAttribute name="header" />
	</header>
	<div class="w-100">
		<div class="">
			<div id="main" style="padding-top: 100px">
			<section id="site-content"> <tiles:insertAttribute name="body" /> </section>
		</div>
	</div>
	</div>
	  
	
	<footer id="footer">
		<tiles:insertAttribute name="footer" />
	</footer>

</body>
</html>