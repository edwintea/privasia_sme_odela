<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/elements/procurehere1.css"/>">
<script type="text/javascript" src="<c:url value="/resources/js/numeral.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/assets/js-core/jquery.form-validator.js"/>"></script>
<div id="page-content-wrapper">
	<div id="page-content">
		<div class="container">
			<ol class="breadcrumb">
				<li><a href="${pageContext.request.contextPath}/buyer/buyerDashboard"> <spring:message code="application.dashboard" />
				</a></li>
				<li class="active">${eventType.value}</li>
			</ol>
			<!-- page title block -->
			<div class="Section-title title_border gray-bg">
				<h2 class="trans-cap tender-request-heading">
					<spring:message code="label.event.award" />
				</h2>
				<h2 class="trans-cap pull-right">
					<spring:message code="application.status" />
					: ${event.status}
				</h2>
			</div>
			<div class="Invited-Supplier-List import-supplier white-bg">
				<div class="meeting2-heading">
					<h3>
						<spring:message code="label.event.award" />
					</h3>
					<div class="marg-bottom-20 marg-left-20">
						<a href="${pageContext.request.contextPath}/buyer/${eventType}/eventSummary/${event.id}">
							<button style="margin-top: 4px; margin-right: 4px;" class="btn btn-info ph_btn_midium hvr-pop hvr-rectangle-out pull-right">
								<spring:message code="eventdetails.event.details" />
							</button>
						</a>
					</div>
				</div>
				<div class="import-supplier-inner-first-new pad_all_15 global-list">
					<!-- <header class="form_header"> </header> -->
					<jsp:include page="/WEB-INF/views/jsp/templates/message.jsp" />
					<jsp:include page="/WEB-INF/views/jsp/templates/ajaxMessage.jsp" />
					<div class="clear"></div>
					<div class="tab-main">
						<input type="hidden" value="${event.decimal}" id="decimal">
						<ul class="nav-tabs nav tab event-details-tabs supplier-tab">
							<c:forEach items="${supplierBqs}" var="suppBqs">
								<li class="tab-link ${suppBqs.bq.id == suppBqId ? 'current' : ''}"><a class="${suppBqs.bq.id == suppBqId ? 'font-gray' : 'font-white'}" href="${pageContext.request.contextPath}/buyer/${eventType}/eventAward/${event.id}/${suppBqs.bq.id}">${suppBqs.bq.name}</a></li>
							</c:forEach>
						</ul>
					</div>
					<c:if test="${not empty eventSuppliers }">
						<form:form method="POST" modelAttribute="eventAward" id="eventAwardCriteriaForm">
							<div class="row marg-bottom-20" style="margin-top: 70px;">
								<input type="hidden" name="eventId" class="rfxEventId" value="${event.id}" /> <input type="hidden" name="bqId" class="bqId" value="${suppBqId}" />
								<div class="col-md-2">
									<label class="marg-top-10"><spring:message code="eventawards.criteria.label" /></label>
								</div>
								<div class="col-md-4 itemised">
									<form:select path="awardCriteria" id="awardCriteria" var="awdCriteria" cssClass="chosen-select form-control disablesearch awardCriteria" data-validation="required">
										<form:options items="${awardCriteria}" itemLabel="value" />
									</form:select>
								</div>
							</div>
						</form:form>
						<form:form method="POST" action="${pageContext.request.contextPath}/buyer/${eventType}/saveEventAward?${_csrf.parameterName}=${_csrf.token}" modelAttribute="eventAward" id="eventAwardForm" enctype="multipart/form-data">
							<div class="table-responsive" style="max-height: 400px;">
								<table class="table table-striped table-top-mrgn ">
									<thead>
										<tr class="thead-style">
											<th>No.</th>
											<th><p align="left" class="w-110">
													<spring:message code="label.rftbq.th.itemname" />
												</p></th>
											<th><p align="left" class="w-110">
													<spring:message code="supplier.name" />
												</p></th>
											<th><p align="left">
													<spring:message code="eventaward.supplier.price" />
												</p></th>
											<th><p align="left">
													<spring:message code="summarydetails.event.conclusion.award.price" />
												</p></th>
											<th><p align="left">
													<spring:message code="account.overview.tax" />
												</p></th>
											<th><p align="left">
													<spring:message code="eventawards.tax.value" />
												</p></th>
											<th><p align="left">
													<spring:message code="eventawards.total.price" />
												</p></th>
											<th><p align="left">
													<spring:message code="eventawards.add.product.list" />
												</p></th>
											<th><p align="left">
													<spring:message code="application.catagory" />
												</p></th>
											<th><p align="left">
													<spring:message code="productz.code" />
												</p></th>
										</tr>
									</thead>
									<tbody>
										<form:input type="hidden" path="awardCriteria" name="awardCriteria" value="${eventAward.awardCriteria}" />
										<form:input type="hidden" path="rfxEvent.id" name="rfxEventId" class="rfxEventId" value="${event.id}" />
										<form:input type="hidden" path="bq.id" name="bqId" class="bqId" value="${supplierBq.bq.id}" />
										<form:input type="hidden" path="id" name="eventAwardId" class="eventAwardId" value="${id}" />
										<c:forEach items="${supplierBq.supplierBqItems}" var="suppBqItem" varStatus="count">
											<tr>
												<input type="hidden" class="rowSeries" id="rowSeries-${count.index}" value="${count.index}" />
												<form:input type="hidden" path="rfxAwardDetails[${count.index}].id" name="awardDetailId" />
												<form:input type="hidden" path="rfxAwardDetails[${count.index}].bqItem.uom.id" id="bqUomId-${count.index}" />
												<form:input type="hidden" path="rfxAwardDetails[${count.index}].bqItem.level" value="${suppBqItem.bqItem.level}" id="bqLevel-${count.index}" />
												<form:input type="hidden" path="rfxAwardDetails[${count.index}].bqItem.order" value="${suppBqItem.bqItem.order}" id="bqOrder-${count.index}" />
												<input type="hidden" name="priceType" id="" value="${suppBqItem.bqItem.priceType}" class="priceType2" />
												<td>${suppBqItem.bqItem.level}.${suppBqItem.bqItem.order}</td>
												<td align="left">${suppBqItem.bqItem.itemName}<form:input type="hidden" path="rfxAwardDetails[${count.index}].bqItem.id" name="bqItemId" value="${suppBqItem.bqItem.id}" id="bqItemId-${count.index}" /> <br /> <c:if test="${suppBqItem.bqItem.priceType == 'BUYER_FIXED_PRICE'}">
														<span class="bs-label label-success" style="color: #fff"><spring:message code="eventsummary.bq.buyer.fixedprice" /></span>&nbsp;
												</c:if> <c:if test="${suppBqItem.bqItem.priceType == 'TRADE_IN_PRICE'}">
														<span class="bs-label label-info" style="color: #fff"><spring:message code="eventsummary.bq.trade.price" /></span>&nbsp;
												</c:if>
												</td>
												<td align="left">
													<div class="dropdown">
														<c:if test="${suppBqItem.bqItem.order != '0'}">
															<c:if test="${eventAward.awardCriteria == 'LOWEST_TOTAL_PRICE' || eventAward.awardCriteria == 'LOWEST_ITEMIZED_PRICE' || eventAward.awardCriteria == 'HIGHEST_TOTAL_PRICE' || eventAward.awardCriteria == 'HIGHEST_ITEMIZED_PRICE'}">
																<form:input type="hidden" path="rfxAwardDetails[${count.index}].supplier.id" value="${suppBqItem.supplier.id }" />${suppBqItem.supplier.companyName}</option>
															</c:if>
															<c:if test="${!(eventAward.awardCriteria == 'LOWEST_TOTAL_PRICE' || eventAward.awardCriteria == 'LOWEST_ITEMIZED_PRICE' || eventAward.awardCriteria == 'HIGHEST_TOTAL_PRICE' || eventAward.awardCriteria == 'HIGHEST_ITEMIZED_PRICE')}">
																<form:select cssClass="chosen-select" id="awardSupplier" class="awardSupplier" onchange="manualSelection(this.value, $('#bqItemId-${count.index}').val(), $('#rowSeries-${count.index}').val())" path="rfxAwardDetails[${count.index}].supplier.id" data-validation="required">
																	<form:options items="${eventSuppliers}" itemValue="id" itemLabel="companyName" />
																</form:select>
															</c:if>
														</c:if>
													</div>
												</td>
												<td align="left"><c:if test="${suppBqItem.bqItem.order != '0'}">
														<c:set var="originalPrice" value="${(eventAward.rfxAwardDetails[count.index].originalPrice == null || changeCriteria) ? suppBqItem.totalAmount : eventAward.rfxAwardDetails[count.index].originalPrice}" />
														<form:input type="hidden" path="rfxAwardDetails[${count.index}].originalPrice" value="${originalPrice}" class="originalPrice" id="originalPricehid-${count.index}" />
														<div id="originalPrice-${count.index}">
															<fmt:formatNumber type="number" minFractionDigits="${event.decimal}" maxFractionDigits="${event.decimal}" value="${originalPrice}" />
														</div>
													</c:if></td>
												<td align="left"><c:choose>
														<c:when test="${event.decimal==1}">
															<c:set var="decimalSet" value="0,0.0"></c:set>
														</c:when>
														<c:when test="${event.decimal==2}">
															<c:set var="decimalSet" value="0,0.00"></c:set>
														</c:when>
														<c:when test="${event.decimal==3}">
															<c:set var="decimalSet" value="0,0.000"></c:set>
														</c:when>
														<c:when test="${event.decimal==4}">
															<c:set var="decimalSet" value="0,0.0000"></c:set>
														</c:when>
														<c:when test="${event.decimal==5}">
															<c:set var="decimalSet" value="0,0.00000"></c:set>
														</c:when>
														<c:when test="${event.decimal==6}">
															<c:set var="decimalSet" value="0,0.000000"></c:set>
														</c:when>
													</c:choose> <c:if test="${suppBqItem.bqItem.order != '0'}">
														<div id="awardPrice-${count.index}" style="display: none;">
															<fmt:formatNumber var="fmtAwardPrice" type="number" minFractionDigits="${event.decimal}" maxFractionDigits="${event.decimal}" value="${(eventAward.rfxAwardDetails[count.index].awardedPrice  == null || changeCriteria)? suppBqItem.totalAmount : eventAward.rfxAwardDetails[count.index].awardedPrice}" />
														</div>
														<form:input type="text" data-changeto="${count.index}" data-rate="${not empty suppBqItem.tax ? suppBqItem.tax : 0 }" data-taxtype="${suppBqItem.taxType}" path="rfxAwardDetails[${count.index}].awardedPrice" value="${fmtAwardPrice}" id="awardPricehid-${count.index}" class="form-control form-control-inp awardPrice" data-validation="" data-validation-regexp="^[\d,]{1,14}(\.\d{1,${event.decimal}})?$" />
													</c:if></td>
												<td align="left"><c:if test="${suppBqItem.bqItem.order != '0'}">
														<form:input type="hidden" path="rfxAwardDetails[${count.index}].taxType" value="${eventAward.rfxAwardDetails[count.index].taxType == null || changeCriteria ? suppBqItem.taxType : eventAward.rfxAwardDetails[count.index].taxType}" class="taxType" id="taxTypehid-${count.index}" />
													</c:if>
													<div id="taxType-${count.index}">${eventAward.rfxAwardDetails[count.index].taxType == null || changeCriteria ? suppBqItem.taxType : eventAward.rfxAwardDetails[count.index].taxType}</div></td>
												<td><c:if test="${suppBqItem.bqItem.order != '0'}">
														<form:input type="hidden" path="rfxAwardDetails[${count.index}].tax" value="${eventAward.rfxAwardDetails[count.index].tax == null || changeCriteria ? suppBqItem.tax : eventAward.rfxAwardDetails[count.index].tax}" id="taxhid-${count.index}" />
														<div id="tax-${count.index}">
															<fmt:formatNumber type="number" minFractionDigits="${event.decimal}" maxFractionDigits="${event.decimal}" value="${eventAward.rfxAwardDetails[count.index].tax == null || changeCriteria ? suppBqItem.tax : eventAward.rfxAwardDetails[count.index].tax}" />
														</div>
													</c:if></td>
												<td align="left"><c:if test="${suppBqItem.bqItem.order != '0'}">
														<form:input type="hidden" id="rfxawhid-${count.index}" path="rfxAwardDetails[${count.index}].totalPrice" value="${eventAward.rfxAwardDetails[count.index].totalPrice == null || changeCriteria ? suppBqItem.totalAmountWithTax : eventAward.rfxAwardDetails[count.index].totalPrice}" class="totalwithTax" />
													</c:if>
													<div id="rfxawdsp-${count.index}">
														<c:if test="${suppBqItem.bqItem.order != '0'}">
															<fmt:formatNumber type="number" minFractionDigits="${event.decimal}" maxFractionDigits="${event.decimal}" value="${eventAward.rfxAwardDetails[count.index].totalPrice == null || changeCriteria ? suppBqItem.totalAmountWithTax : eventAward.rfxAwardDetails[count.index].totalPrice}" />

														</c:if>
													</div></td>
												<td align="left"><c:if test="${suppBqItem.bqItem.order != '0'}">
														<input data-pid="${suppBqItem.bqItem.id}-${suppBqItem.supplier.id }" value="${suppBqItem.bqItem.id}-${suppBqItem.supplier.id }" type="checkbox" class="custom-itemCheck" name="rfxAwardDetails[${count.index}].selectItem" id="selectItem-${suppBqItem.bqItem.id}-${suppBqItem.supplier.id }">
													</c:if></td>
												<td align="left">
													<div class="selectCategory">
														<c:if test="${suppBqItem.bqItem.order != '0'}">
															<select data-pid="${suppBqItem.bqItem.itemName}-${suppBqItem.supplier.id }" class="chosen-select" name="rfxAwardDetails[${count.index}].productCategory" id="category-${suppBqItem.bqItem.id}">
																<option value="">Select Category</option>
																<c:forEach items="${suppBqItem.productCategoryList}" var="item">
																	<option value="${item.id}">${item}</option>
																</c:forEach>
															</select>
														</c:if>
													</div>
												</td>
												<td align="left">
													<div class="product Code">
														<c:if test="${suppBqItem.bqItem.order != '0'}">
															<spring:message code="systemsetting.product.code.placeholder" var="prodcode" />
															<form:input type="text" path="rfxAwardDetails[${count.index}].productCode" id="productCode-${suppBqItem.bqItem.id}" value="${productCode}" cssClass="form-control" class="form-control animated-search-filter1 search_textbox_1 " placeholder="${prodcode}" maxlength="15" />
														</c:if>
													</div>
												</td>
											</tr>
										</c:forEach>
									</tbody>
									<tfoot class="tfoot-style">
										<tr>
											<td></td>
											<td></td>
											<td align="left"><spring:message code="eventawards.total" /></td>
											<td align="left"><form:input id="totalSupplierPrice" type="hidden" path="totalSupplierPrice" class="totalSupplierPrice" />
												<div id="originalPriceSum"></div></td>
											<td align="left"><form:input id="totalAwardPrice" type="hidden" path="totalAwardPrice" class="totalAwardPrice" />
												<div style="margin-left: 14px;" id="awardPriceSum"></div></td>
											<td></td>
											<td></td>
											<td align="left"><form:input id="grandTotal" type="hidden" path="grandTotalPrice" class="grandTotal" />
												<div id="totalAfterTax">
													<fmt:formatNumber type="number" minFractionDigits="${event.decimal}" maxFractionDigits="${event.decimal}" value="${eventAward.rfxAwardDetails[count.index].grandTotalPrice == null ? supplierBq.totalAfterTax : eventAward.rfxAwardDetails[count.index].grandTotalPrice}" />
												</div></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</tfoot>
								</table>
							</div>
							<div class="row" style="margin-right: -15px; margin-left: -15px;">
								<div class="col-sm-6 col-md-6 col-lg-4 col-xs-12 ">

									<label><spring:message code="pr.remark" />: </label>&nbsp;&nbsp;
									<spring:message code="conclude.remark.placeholder" var="remark" />
									<form:textarea path="awardRemarks" name="awardRemarks" class="form-control textarea-counter" value="${awardRemarks}" data-validation="required length" placeholder="${remark}" data-validation-length="max500" rows="2" />


								</div>
								<div class="col-sm-12 col-md-12 col-lg-8 col-xs-12"></div>

							</div>

							<div class="row" style="margin-top: 15px; margin-left: -15px;">
								<div class="col-sm-6 col-md-6 col-lg-4 col-xs-12 ">
									<div class="fileinput fileinput-new input-group " data-provides="fileinput">
										<div class="form-control" data-trigger="fileinput" style="width: 400px;">
											<i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span>
										</div>
										<span class="input-group-addon btn btn-black btn-file"> <span class="fileinput-new">Select file </span> <span class="fileinput-exists">Change</span> <c:set var="fileType" value="" /> <c:forEach var="type" items="${ownerSettings.fileTypes}" varStatus="index">
												<c:set var="fileType" value="${fileType}${index.first ? '': ', '}${type}" />
											</c:forEach> <form:input class="uploadAwardFileAttch" path="attachment" type="file" id="uploadAwardFileId" data-validation-allowing="${fileType}" data-validation="extension size" data-validation-max-size="${ownerSettings.fileSizeLimit}M" data-validation-error-msg-container="#Load_File-error-dialog" data-validation-error-msg-size="You can not upload file larger than ${ownerSettings.fileSizeLimit} MB" data-validation-error-msg-mime="You can only upload file ${ownerSettings.fileTypes}"></form:input>
										</span> <a href="#" class="input-group-addon btn btn-black fileinput-exists pos-l-10" data-dismiss="fileinput">Remove</a>
									</div>
									<div id="Load_File-error-dialog" style="width: 100%; float: left; margin: 0 0 0 0;"></div>
									<div style="margin-top: 15px;">
										Note:<br />
										<ul>
											<li>Max allowed file size is ${ownerSettings.fileSizeLimit} MB</li>
											<li>Allowed file extensions:${ownerSettings.fileTypes}.</li>
										</ul>
									</div>
								</div>
							</div>

							<c:if test="${!erpSetup.isErpEnable}">
								<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
								<div class="row btn-group" style="margin-top: 10px; margin-right: -15px; margin-left: -15px;">


									<div class="col-sm-6 ${event.status ne 'FINISHED' ?'col-md-3 col-lg-3': 'col-md-6 col-lg-6' }  col-xs-12">
										<button type="button" id="createPr" class="btn btn-info ph_btn_midium hvr-pop hvr-rectangle-out">
											<spring:message code="eventawards.create.pr.title" />
										</button>
									</div>
									<c:if test="${event.status ne 'FINISHED'}">
										<div class="col-sm-6 col-md-3 col-lg-3 col-xs-12">
											<button type="submit" class=" btn btn-info ph_btn_midium hvr-pop hvr-rectangle-out">
												<spring:message code="application.save" />
											</button>
										</div>
									</c:if>
									<c:if test="${event.status ne 'FINISHED'}">
										<div class="col-sm-6 col-md-3 col-lg-3 col-xs-12">
											<button type="button" id="saveEventAward" class=" btn btn-info ph_btn_midium hvr-pop hvr-rectangle-out">
												<spring:message code="application.save" />
												&amp;
												<spring:message code="eventarchieve.conclude" />
											</button>
										</div>
									</c:if>
									<div class="col-sm-6 col-md-3 col-lg-3 col-xs-12">
										<a class="btn  btn-black hvr-pop hvr-rectangle-out1 ph_btn_midium button-previous" href="${pageContext.request.contextPath}/buyer/${eventType}/eventAward/${eventId}"> <spring:message code="application.reset" />
										</a>
									</div>

								</div>
							</c:if>
							<c:if test="${erpSetup.isErpEnable}">
								<div class="row btn-group" style="margin-top: 10px; margin-right: -15px; margin-left: -15px;">


									<div class="col-sm-6 ${event.status ne 'FINISHED' ?'col-md-3 col-lg-3': 'col-md-6 col-lg-6' }   col-xs-12">
										<button type="button" id="createPr" class="btn btn-info ph_btn_midium hvr-pop hvr-rectangle-out">
											<spring:message code="eventawards.create.pr.title" />
										</button>
									</div>
									<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
									<c:if test="${event.status ne 'FINISHED'}">
										<div class="col-sm-6 col-md-3 col-lg-3 col-xs-12">
											<button type="submit" class=" btn btn-info ph_btn_midium hvr-pop hvr-rectangle-out">
												<spring:message code="application.save" />
											</button>
										</div>
									</c:if>
									<div class="col-sm-6 col-md-3 col-lg-3 col-xs-12">
										<a class="btn  btn-black hvr-pop hvr-rectangle-out1 ph_btn_midium button-previous" href="${pageContext.request.contextPath}/buyer/${eventType}/eventAward/${eventId}"> <spring:message code="application.reset" />
										</a>
									</div>
									<c:if test="${event.status ne 'FINISHED'}">
										<div class="col-sm-6 col-md-3 col-lg-3 col-xs-12">

											<button type="button" id="saveOrTrasfer" class="btn btn-info ph_btn_midium hvr-pop hvr-rectangle-out ">
												<spring:message code="application.save" />
												&amp;
												<spring:message code="application.transfer.btn" />
											</button>

										</div>
									</c:if>

								</div>
							</c:if>

						</form:form>
					</c:if>
				</div>
				<c:if test="${empty eventSuppliers }">
					<div class="row marg-bottom-20" style="margin-top: 70px;">
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
									<div id="idGlobalWarn" class="alert alert-warning">
										<h3 align="center">
											<spring:message code="eventaward.no.qualified.found" />
										</h3>
									</div>
								</div>
							</div>
						</div>
					</div>
				</c:if>
			</div>
			<jsp:include page="eventAwardAudit.jsp" />
		</div>
	</div>
</div>
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog for-delete-all reminder">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h3>
					<spring:message code="eventaward.confirm.update" />
				</h3>
				<input type="hidden" id="selectedAwardId" name="selectedAwardId" value="">
				<button class="close for-absulate" onclick="javascript:doCancel();" type="button" data-dismiss="modal">�</button>
			</div>
			<div class="modal-body">
				<label><spring:message code="eventaward.item.already.exist" /></label>
			</div>
			<div class="modal-footer pad_all_15 float-left width-100 border-top-width-1">
				<button id="idConfirmUpdate" type="button" onclick="javascript:doUpdate();" class="btn btn-info pull-left ph_btn_small hvr-pop hvr-rectangle-out" data-dismiss="modal">
					<spring:message code="application.update" />
				</button>
				<button type="button" onclick="javascript:doCancel();" class="btn btn-black pull-right btn-default ph_btn_small hvr-pop hvr-rectangle-out1" data-dismiss="modal">
					<spring:message code="application.cancel" />
				</button>
			</div>
		</div>
	</div>
</div>
<div class=" modal flagvisibility dialogBox" id="pushPr" role="dialog">
	<div class="float-left width100 pad_all_15 white-bg pop-up-pr">
		<form class="bordered-row">
			<div class="row">
				<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">
					<h3 id="ui-id-1" class="ui-dialog-title">Generate PR</h3>
					<button type="button" class="ui-dialog-titlebar-close closePd" data-dismiss="modal"></button>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-sm-6 col-md-6">
					<label><spring:message code="eventawards.pr.creator" /></label>
				</div>
				<div class="col-sm-6 col-md-6">
					<div class="input_wrapper">
						<select class="chosen-select" name="selectPRCreator" required id="selectPRCreator">
							<option value="" selected disabled><spring:message code="award.pr.creator.placeholder" /></option>
							<c:forEach items="${prCreator}" var="item">
								<option value="${item.id}">${item.name}</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-sm-6 col-md-6">
					<label>PR Template</label>
				</div>
				<div class="col-sm-6 col-md-6">
					<div class="input_wrapper">
						<select class="chosen-select" name="selectPRTemplate" required id="selectPRTemplate" required>
							<option value="" selected disabled><spring:message code="award.pr.template.placeholder" /></option>
							<c:forEach items="${prTemplate}" var="item">
								<option value="${item.id}">${item.templateName}</option>
							</c:forEach>
						</select>
						<div style="font-style: italic;">*Decimal values will be rounded to the decimal setting of the PR template.</div>
					</div>
				</div>
			</div>
			<div class="row ">
				<div class="col-md-12">
					<div class="form-group align-center margin-tp-5">
						<button id="pushToPR" type="button" data-dismiss="modal" class="btn  marg-left-10 hvr-pop ph_btn  btn btn-success frm_bttn1 sub_frm">
							<spring:message code="eventawards.push.pr.button" />
						</button>
						<!-- <button type="submit" class=" hvr-pop sub_frm">Confirm & Pay</button> -->
					</div>
				</div>
			</div>
		</form>
	</div>
</div>



<div class=" modal flagvisibility dialogBox" id="ConfirmSaveOrTrasfer" role="dialog">
	<div class="float-left width100 pad_all_15 white-bg pop-up-pr">
		<form class="bordered-row">
			<div class="row">
				<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">
					<h3 id="ui-id-1" class="ui-dialog-title">Please Confirm To Save Award</h3>
					<button type="button" class="ui-dialog-titlebar-close closePd" data-dismiss="modal"></button>
				</div>
			</div>

			<div class="row ">
				<div class="col-md-12">
					<div class="form-group align-center margin-tp-5">
						<button id="transferBtn" type="button" data-dismiss="modal" class="btn  marg-left-10 hvr-pop ph_btn  btn btn-success frm_bttn1 sub_frm">
							<spring:message code="application.transfer.btn" />
						</button>
						<!-- <button type="submit" class=" hvr-pop sub_frm">Confirm & Pay</button> -->
					</div>
				</div>
			</div>
		</form>
	</div>
</div>


<div class=" modal flagvisibility dialogBox" id="ConfirmSaveModal" role="dialog">
	<div class="float-left width100 pad_all_15 white-bg pop-up-pr">
		<form class="bordered-row">
			<div class="row">
				<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">
					<h3 id="ui-id-1" class="ui-dialog-title">Please Confirm To Save Award</h3>
					<button type="button" class="ui-dialog-titlebar-close closePd" data-dismiss="modal"></button>
				</div>
			</div>

			<div class="row ">
				<div class="col-md-12">
					<div class="form-group align-center margin-tp-5">
						<button id="saveConfirm" type="button" data-dismiss="modal" class="btn  marg-left-10 hvr-pop ph_btn  btn btn-success frm_bttn1 sub_frm">
							<spring:message code="application.save" />
							&amp;
							<spring:message code="eventarchieve.conclude" />
						</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>




<script type="text/javascript">
	var decimalLimit = $('#decimal').val();
	$(document).ready(
			function() {
				var totalwithTax = parseFloat(0);
				var grandTotalVal = 0;
				$('.totalwithTax').each(function() {
					var priceType = $(this).closest('tr').find('[name="priceType"]').val();

					if (priceType == 'TRADE_IN_PRICE') {
						grandTotalVal = (parseFloat(grandTotalVal) - parseFloat($(this).val().replace(/,/g, ''))).toFixed(decimalLimit);
					} else {
						grandTotalVal = (parseFloat(grandTotalVal) + parseFloat($(this).val().replace(/,/g, ''))).toFixed(decimalLimit);
					}
					totalwithTax = grandTotalVal;
					//totalwithTax += parseFloat($(this).val());
				});

				totalwithTax = ReplaceNumberWithCommas(parseFloat(totalwithTax).toFixed(decimalLimit));
				$('#totalAfterTax').html(totalwithTax);
				$('#grandTotal').val(totalwithTax);
				//---------------------------------------------------------------------------//

				var awardPriceSum = parseFloat(0);
				var grandTotalVal = 0;
				$('.awardPrice').each(function() {
					var priceType = $(this).closest('tr').find('[name="priceType"]').val();

					if (priceType == 'TRADE_IN_PRICE') {
						grandTotalVal = (parseFloat(grandTotalVal.toString().replace(/,/g, "").match(new RegExp("^\\d+\\.?\\d{0," + decimalLimit + "}"))) - parseFloat($(this).val().replace(/,/g, ''))).toFixed(decimalLimit);
					} else {
						grandTotalVal = (parseFloat(grandTotalVal.toString().replace(/,/g, "").match(new RegExp("^\\d+\\.?\\d{0," + decimalLimit + "}"))) + parseFloat($(this).val().replace(/,/g, ''))).toFixed(decimalLimit);
					}
					awardPriceSum = grandTotalVal;
					//awardPriceSum += parseFloat($(this).val().replace(/\,|\s|\#/g, ''));
					//console.log(parseFloat($(this).val().replace(/\,|\s|\#/g, '')));GeneralEnquiryServiceImpl
				});

				//console.log(awardPriceSum);
				awardPriceSum = ReplaceNumberWithCommas(parseFloat(awardPriceSum).toFixed(decimalLimit));

				var grandTotalVal = 0;
				var originalPriceSum = parseFloat(0);
				$('.originalPrice').each(function() {
					var priceType = $(this).closest('tr').find('[name="priceType"]').val();

					if (priceType == 'TRADE_IN_PRICE') {
						grandTotalVal = (parseFloat(grandTotalVal.toString().replace(/,/g, "").match(new RegExp("^\\d+\\.?\\d{0," + decimalLimit + "}"))) - parseFloat($(this).val().replace(/,/g, ''))).toFixed(decimalLimit);
					} else {
						grandTotalVal = (parseFloat(grandTotalVal.toString().replace(/,/g, "").match(new RegExp("^\\d+\\.?\\d{0," + decimalLimit + "}"))) + parseFloat($(this).val().replace(/,/g, ''))).toFixed(decimalLimit);
					}
					originalPriceSum = grandTotalVal;

					//originalPriceSum += parseFloat($(this).val());
				});

				originalPriceSum = ReplaceNumberWithCommas(parseFloat(originalPriceSum).toFixed(decimalLimit));

				$("#totalSupplierPrice").val(originalPriceSum);
				$("#originalPriceSum").text(originalPriceSum);

				$("#totalAwardPrice").val(awardPriceSum);
				$("#awardPriceSum").text(awardPriceSum);

				$(document).delegate(
						'.awardPrice',
						'change',
						function(e) {
							e.preventDefault();
							decimalLimit = $('#decimal').val();

							this.value = this.value.replace(/\,|\s|\#/g, '').match(new RegExp("^\\d+\\.?\\d{0," + decimalLimit + "}"));

							var amt = parseFloat(0);
							amt = parseFloat(this.value);

							var type = $(this).data('taxtype');

							var rate = parseFloat($(this).attr('data-rate').replace(/\,|\s|\#/g, '').match(new RegExp("^\\d+\\.?\\d{0," + decimalLimit + "}")));

							var changeto = $(this).data('changeto');

							if (type == 'Percent') {
								var add = parseFloat((amt * rate / 100).toString().replace(/\,|\s|\#/g, '').match(new RegExp("^\\d+\\.?\\d{0," + decimalLimit + "}")));
								amt = (parseFloat(amt.toString().replace(/\,|\s|\#/g, '').match(new RegExp("^\\d+\\.?\\d{0," + decimalLimit + "}"))) + parseFloat(add.toString().replace(/\,|\s|\#/g, '').match(
										new RegExp("^\\d+\\.?\\d{0," + decimalLimit + "}")))).toFixed(decimalLimit);
							} else if (type == 'Amount') {
								var add = amt;
								amt = (parseFloat(rate.toString().replace(/\,|\s|\#/g, '').match(new RegExp("^\\d+\\.?\\d{0," + decimalLimit + "}"))) + parseFloat(amt.toString().replace(/\,|\s|\#/g, '').match(
										new RegExp("^\\d+\\.?\\d{0," + decimalLimit + "}")))).toFixed(decimalLimit);
							}
							if (isNaN(amt)) {
								amt = parseFloat(ReplaceNumberWithCommas((0).toFixed(decimalLimit)));
							}
							$('#rfxawhid-' + changeto).val(amt);
							$('#rfxawdsp-' + changeto).html(ReplaceNumberWithCommas(parseFloat(amt.toString().replace(/\,|\s|\#/g, '').match(new RegExp("^\\d+\\.?\\d{0," + decimalLimit + "}"))).toFixed(decimalLimit)));

							var totalwithTax = parseFloat(0);
							var grandTotalVal = 0;
							$('.totalwithTax').each(function() {
								var priceType = $(this).closest('tr').find('[name="priceType"]').val();
								if (priceType == 'TRADE_IN_PRICE') {

									grandTotalVal = (parseFloat(grandTotalVal.toString().replace(/\,|\s|\#/g, '').match(new RegExp("^\\d+\\.?\\d{0," + decimalLimit + "}"))) - parseFloat($(this).val().replace(/,/g, ''))).toFixed(decimalLimit);
								} else {
									grandTotalVal = (parseFloat(grandTotalVal.toString().replace(/\,|\s|\#/g, '').match(new RegExp("^\\d+\\.?\\d{0," + decimalLimit + "}"))) + parseFloat($(this).val().replace(/,/g, ''))).toFixed(decimalLimit);
								}
								totalwithTax = grandTotalVal;
								//totalwithTax += parseFloat($(this).val());
							});

							totalwithTax = ReplaceNumberWithCommas(parseFloat(totalwithTax.toString().replace(/\,|\s|\#/g, '').match(new RegExp("^\\d+\\.?\\d{0," + decimalLimit + "}"))).toFixed(decimalLimit));

							$('#totalAfterTax').html(totalwithTax);
							$('#grandTotal').val(totalwithTax);
							var decimalLimit = $('#decimal').val();
							var awardPriceSum = parseFloat(0);
							var grandTotalVal = 0;
							$('.awardPrice').each(function() {

								var priceType = $(this).closest('tr').find('[name="priceType"]').val();

								if (priceType == 'TRADE_IN_PRICE') {
									grandTotalVal = (parseFloat(grandTotalVal.toString().replace(/\,|\s|\#/g, '').match(new RegExp("^\\d+\\.?\\d{0," + decimalLimit + "}"))) - parseFloat($(this).val().replace(/,/g, ''))).toFixed(decimalLimit);
								} else {
									grandTotalVal = (parseFloat(grandTotalVal.toString().replace(/\,|\s|\#/g, '').match(new RegExp("^\\d+\\.?\\d{0," + decimalLimit + "}"))) + parseFloat($(this).val().replace(/,/g, ''))).toFixed(decimalLimit);
								}

								if ($(this).val().length != 0 && !isNaN($(this).val().replace(/\,|\s|\#/g, ''))) {
									awardPriceSum = grandTotalVal;
									//awardPriceSum += parseFloat($(this).val().replace(/\,|\s|\#/g, ''));
								}

							});
							this.value = ReplaceNumberWithCommas(parseFloat(this.value.replace(/\,|\s|\#/g, '').match(new RegExp("^\\d+\\.?\\d{0," + decimalLimit + "}"))).toFixed(decimalLimit));
							$("#awardPriceSum").html(ReplaceNumberWithCommas((parseFloat(awardPriceSum.toString().replace(/\,|\s|\#/g, '').match(new RegExp("^\\d+\\.?\\d{0," + decimalLimit + "}"))).toFixed(decimalLimit))));
							$("#totalAwardPrice").val(ReplaceNumberWithCommas((parseFloat(awardPriceSum.toString().replace(/\,|\s|\#/g, '').match(new RegExp("^\\d+\\.?\\d{0," + decimalLimit + "}"))).toFixed(decimalLimit))));
						});

				$('.custom-itemCheck').on('change', function() {
					var currentBlock = $(this);
					var pid = currentBlock.attr('data-pid');

					if ($(this).prop("checked")) {
						$('#loading').show();
						var itemAndSupplierId = $(this).attr('data-pid');

						$.ajax({
							type : "POST",
							url : getContextPath() + '/buyer/${eventType}/checkProductItemExistOrNot',
							data : {
								'itemAndSupplierId' : itemAndSupplierId

							},
							success : function(data) {
								$("#myModal").find('#selectedAwardId').val(pid);
								$('#myModal').modal('show');
								$('#loading').hide();
							},
							error : function(data) {
								$('#loading').hide();
							}
						});
					}
				});

				var header = $("meta[name='_csrf_header']").attr("content");
				var token = $("meta[name='_csrf']").attr("content");
				$('#awardCriteria').change(function(e) {
					e.preventDefault();
					$('#eventAwardCriteriaForm').attr('action', getContextPath() + "/buyer/" + getEventType() + "/criteria/" + getEventType());
					$('#eventAwardCriteriaForm').submit();

				});

			});

	function manualSelection(val, itemId, row) {
		var supplierId = val;
		var bqItem = itemId;
		var eventId = $('.rfxEventId').val();
		var eventType = getEventType();
		var header = $("meta[name='_csrf_header']").attr("content");
		var token = $("meta[name='_csrf']").attr("content");
		var changeto = row;
		var decimalLimit = $('#decimal').val();
		//alert(changeto);
		$.ajax({
			type : "POST",
			url : getContextPath() + "/buyer/" + getEventType() + "/getSupplierData/" + getEventType(),
			data : {
				'supplierId' : supplierId,
				'bqItemId' : bqItem,
				'eventId' : eventId
			},
			dataType : "json",
			beforeSend : function(xhr) {
				$('#loading').show();
				xhr.setRequestHeader(header, token);
			},
			complete : function() {
				$('#loading').hide();
			},
			success : function(data) {
				//id = "awardPricehid-${count.index}"
				$('#originalPricehid-' + changeto).val(data.totalAmount);
				$('#originalPrice-' + changeto).html(ReplaceNumberWithCommas((data.totalAmount).toFixed(decimalLimit)));
				$('#awardPricehid-' + changeto).val(ReplaceNumberWithCommas((data.totalAmount.toFixed(decimalLimit))));
				$('#awardPrice-' + changeto).html(ReplaceNumberWithCommas((data.totalAmount).toFixed(decimalLimit)));
				var dataPid = itemId + "-" + supplierId;
				//var  selected=$('#selectItem').html(dataPid);
				$('#selectItem').html(dataPid);
				$('#selectItem').val(dataPid);
				$('.custom-itemCheck').val(dataPid);
				$('.custom-itemCheck').attr('data-pid', dataPid);
				idValue = "selectItem-" + dataPid;

				var categoryId = "category-" + itemId;

				$('.custom-itemCheck').attr('id', idValue);

				var productCategoryList = '';
				$.each(data.productCategoryList, function(i, obj) {
					productCategoryList += '<option value="' + obj.id + '">' + obj.productName + '</option>';
				});
				$('#' + categoryId).html(productCategoryList);
				$('#' + categoryId).trigger("chosen:updated");

				if (data.tax !== undefined && data.tax !== '') {
					$('#taxTypehid-' + changeto).val(data.taxType);
					$('#taxType-' + changeto).html(data.taxType);
					$('#taxhid-' + changeto).val(data.tax);
					$('#tax-' + changeto).html(ReplaceNumberWithCommas((data.tax).toFixed(decimalLimit)));
				} else {
					$('#taxTypehid-' + changeto).val("");
					$('#taxType-' + changeto).html("");
					$('#taxhid-' + changeto).val("");
					$('#tax-' + changeto).html("");
				}
				$('#rfxawhid-' + changeto).val(data.totalAmountWithTax);
				$('#rfxawdsp-' + changeto).html(ReplaceNumberWithCommas((data.totalAmountWithTax).toFixed(decimalLimit)));

				var grandTotalVal = 0;
				var awardPriceSum = parseFloat(0);
				$('.awardPrice').each(function() {
					var priceType = $(this).closest('tr').find('[name="priceType"]').val();

					if (priceType == 'TRADE_IN_PRICE') {
						grandTotalVal = (parseFloat(grandTotalVal) - parseFloat($(this).val().replace(/,/g, ''))).toFixed(decimalLimit);
					} else {
						grandTotalVal = (parseFloat(grandTotalVal) + parseFloat($(this).val().replace(/,/g, ''))).toFixed(decimalLimit);
					}
					awardPriceSum = grandTotalVal;
					//awardPriceSum += parseFloat($(this).val().replace(/\,|\s|\#/g, ''));
				});

				awardPriceSum = ReplaceNumberWithCommas(parseFloat(awardPriceSum).toFixed(decimalLimit));
				var grandTotalVal = 0;
				var originalPriceSum = parseFloat(0);
				$('.originalPrice').each(function() {
					var priceType = $(this).closest('tr').find('[name="priceType"]').val();

					if (priceType == 'TRADE_IN_PRICE') {
						grandTotalVal = (parseFloat(grandTotalVal) - parseFloat($(this).val().replace(/,/g, ''))).toFixed(decimalLimit);
					} else {
						grandTotalVal = (parseFloat(grandTotalVal) + parseFloat($(this).val().replace(/,/g, ''))).toFixed(decimalLimit);
					}
					originalPriceSum = grandTotalVal;

					//originalPriceSum += parseFloat($(this).val());
				});

				originalPriceSum = ReplaceNumberWithCommas(parseFloat(originalPriceSum).toFixed(decimalLimit));

				$("#totalSupplierPrice").val(originalPriceSum);
				$("#originalPriceSum").text(originalPriceSum);

				$("#totalAwardPrice").val(awardPriceSum);
				$("#awardPriceSum").text(awardPriceSum);
				var totalwithTax = parseFloat(0);
				var grandTotalVal = 0;
				$('.totalwithTax').each(function() {
					var priceType = $(this).closest('tr').find('[name="priceType"]').val();

					if (priceType == 'TRADE_IN_PRICE') {
						grandTotalVal = (parseFloat(grandTotalVal) - parseFloat($(this).val().replace(/,/g, ''))).toFixed(decimalLimit);
					} else {
						grandTotalVal = (parseFloat(grandTotalVal) + parseFloat($(this).val().replace(/,/g, ''))).toFixed(decimalLimit);
					}
					totalwithTax = grandTotalVal;
					/* totalwithTax += parseFloat($(this).val()); */

				});
				totalwithTax = ReplaceNumberWithCommas(parseFloat(totalwithTax).toFixed(decimalLimit));
				$('#totalAfterTax').html(totalwithTax);
				$('#grandTotal').val(totalwithTax);
			},
			error : function(request, textStatus, errorThrown) {
				console.log(" Error : " + request.getResponseHeader('error'));
			}
		});
	}

	function ReplaceNumberWithCommas(yourNumber) {
		// Seperates the components of the number
		var n = yourNumber.toString().split(".");
		// Comma-fies the first part
		n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		// Combines the two sections
		return n.join(".");
	}
	function validateform() {
		//alert();
	}

	$('#saveEventAward').click(function(e) {
		$('#ConfirmSaveModal').modal('show');
		$('.error-range.text-danger').remove();
		$('#loading').hide();
	});

	$('#saveConfirm').click(function(e) {
		$('#eventAwardForm').attr('action', getContextPath() + "/buyer/" + getEventType() + "/saveEventAward/true?${_csrf.parameterName}=${_csrf.token}");
		$('#eventAwardForm').submit();
	});

	$('#saveOrTrasfer').click(function(e) {
		$('#ConfirmSaveOrTrasfer').modal('show');
		$('.error-range.text-danger').remove();
		$('#loading').hide();
	});

	$('#transferBtn').click(function(e) {

		$('#eventAwardForm').attr('action', getContextPath() + "/buyer/" + getEventType() + "/saveEventAward/true?${_csrf.parameterName}=${_csrf.token}");
		$('#eventAwardForm').submit();

	});
	$('#pushToPR').click(function(e) {

		//$('#eventAwardForm').validate();
		var prTemplate = $("select[name='selectPRTemplate']").val();
		var prCreator = $("select[name='selectPRCreator']").val();
		if (prCreator == null) {
			$('#selectPRCreator').after('<p class="error-pos error-range text-danger">Please Select PR Creator</p>');
			return false;
		}
		if (prTemplate == null) {
			$('#selectPRTemplate').after('<p class="error-pos error-range text-danger">Please Select PR Template</p>');
			return false;
		}
		$('#eventAwardForm').attr('action', getContextPath() + "/buyer/" + getEventType() + "/transferEventAwardToPr/" + prTemplate + "/" + prCreator + "/${event.id}?${_csrf.parameterName}=${_csrf.token}");
		$('#eventAwardForm').submit();

	});
	$('#selectPRCreator').on('change', function() {
		$('.error-range.text-danger').remove();
	});

	$('#selectPRTemplate').on('change', function() {
		$('.error-range.text-danger').remove();
	});

	function doUpdate() {
		var awardPid = $("#myModal").find('#selectedAwardId').val();
		$('.selectCategory').prop("disabled", true);
	}

	function doCancel() {
		var awardPid = $("#myModal").find('#selectedAwardId').val();
		$('#selectItem-' + awardPid).prop('checked', false);
	}
	$("#createPr").click(function(e) {
		$('#pushPr').modal('show');
		$('.error-range.text-danger').remove();
		$('#loading').hide();
	});

	$.formUtils.addValidator({
		name : 'validate_custom_length',
		validatorFunction : function(value, $el, config, language, $form) {
			var val = value.split(".");
			if (val[0].replace(/,/g, '').length > 14) {
				return false;
			} else {
				return true;
			}
		},
		errorMessage : 'The input value is longer than 14 characters',
		errorMessageKey : 'validateLengthCustom'
	});
</script>
</div>

<script type="text/javascript" src="<c:url value="/resources/js/numeral.min.js"/>"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/assets/widgets/file-input/file-input.js"></script>
<script type="text/javascript" src="<c:url value="/resources/assets/js-core/jquery.form-validator.js"/>"></script>
<script>
	$.validate({
		lang : 'en',
		onfocusout : false,
		validateOnBlur : true,
		modules : 'date,sanitize,file'
	});
</script>
<style type="text/css">
.row {
	margin-right: 0px;
	margin-left: 0px;
}

.saas-content {
	margin-top: 3%;
	height: auto;
}

.dropdown-div {
	float: left;
}

.dropdwon-button {
	padding: 10px;
	font-size: 16px;
}

.float-right {
	float: right;
}

.dropdwon-caret {
	margin-left: 8px !important;
}

.span-price {
	font-size: 16px;
	font-weight: normal !important;
}

.head-four {
	font-family: sans-serif;
	font-weight: 600;
}

.form-control-inp {
	width: 100%;
	max-width: 80%;
}

.table-top-mrgn {
	margin-top: 0%;
	border: 1px solid #ddd;
}

.thead-style {
	background-color: #f1f1f1;
}

th {
	padding: 15px !important;
}

td {
	padding-left: 15px !important;
}

.tfoot-style {
	font-weight: 600;
}

.form-left-marg {
	margin-left: 11%;
}

.btn-div {
	margin-top: 3%;
	width: 70%;
	box-sizing: border-box;
	float: left;
}

.btn-div button {
	padding: 8px;
	width: 100%;
	font-size: 16px;
	max-width: 12%;
}

.mr-20 {
	margin-right: 20px;
}

.last-drpdwn-marg {
	margin-top: 3%;
}

.fit-left {
	margin-right: -30px;
	margin-left: 0px;
	float: right;
	margin-top: 4%;
}

.error-pos {
	position: absolute;
	top: 40px;
}

.margin-tp-5 {
	margin-top: 5%;
}

@media only screen and (max-width: 600px) {
	.itemised {
		float: left !important;
		width: 100%;
	}
	/* 	.fit-left{
margin-right: 0px; 
margin-left: 0px;
width: 100% !important;
	} */
}

.pop-up-pr {
	width: 30%;
	position: absolute;
	left: 50%;
	top: 50%;
	transform: translate(-50%, -50%);
	padding: 0;
	box-shadow: 0px 0px 10px 1px #ccc;
	border-top-left-radius: 9px;
	border-top-right-radius: 9px;
}

.ui-dialog-titlebar {
	padding: 10px;
	border-bottom: 1px solid transparent;
	background: #0095d5;
	line-height: 1;
	width: 100%;
	display: inline-block;
	margin-top: 0;
	border-top-left-radius: 9px;
	border-top-right-radius: 9px;
}

.ui-dialog-titlebar h3 {
	float: left;
	color: #fff;
}

.ui-dialog-titlebar button {
	float: right;
	padding: 3px;
	background: #fff;
}

.form-group {
	border: none !important;
}

.selectCategory {
	min-width: 170px;
}

.chosen-container .chosen-results li.active-result {
	text-align: left;
	word-wrap: normal;
}

.w-110 {
	width: 110px;
}

.pos-l-10 {
	position: relative;
	left: 10px
}

@media only screen and (max-width: 767px) {
	.pop-up-pr {
		width: 85%;
	}
}

@media only screen and (min-width: 768px) and (max-width: 1024px) {
	.pop-up-pr {
		width: 40%;
		margin-left: 10%;
	}
}
</style>
