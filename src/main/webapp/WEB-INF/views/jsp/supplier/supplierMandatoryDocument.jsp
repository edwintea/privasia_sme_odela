<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="/WEB-INF/template-functions.tld" prefix="tf"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/elements/procurehere1.css"/>">
<link rel="stylesheet" type="text/css" href="<c:url value="https://cdn.datatables.net/2.0.0/css/dataTables.dataTables.css"/>">

<!-- Dialog Block -->
<link rel="stylesheet" type="text/css" href="/procurehere/resources/assets/widgets/dialog/dialog.css">
<script type="text/javascript" src="/procurehere/resources/assets/widgets/dialog/dialog.js"></script>

<sec:authentication property="principal.languageCode" var="languageCode" />
<sec:authorize access="hasRole('ADMIN')" var="isAdmin" />
<sec:authorize access="hasRole('ROLE_ADMIN_READONLY')" var="buyerReadOnlyAdmin" />

<script src="<c:url value="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js"/>"></script>
<script src="<c:url value="/resources/assets/widgets/jquery-ui.min/jquery-ui.min.js"/>"></script>

<style>
.disableEve:hover {
	cursor: default;
}

.disableEve {
	background: #0095d5 none repeat scroll 0 0;
	border-color: #0095d5;
}
.cancelButton{
    background: #0095d5 none repeat scroll 0 0;
    border-color: #0095d5;
}
</style>

<div id="page-content-wrapper">
	<div id="page-content">
		<div class="container">
			<ol class="breadcrumb">
				<c:url var="supplierMandatoryDocument" value="/buyer/supplierMandatoryDocument" />
				<li>
					<a id="dashboardLink" href="${supplierMandatoryDocument}"> <spring:message code="application.dashboard" /></a>
				</li>
				<li class="active">
                    Supplier Mandatory Documents<span class='header_action'></span>
				</li>
			</ol>
			<div class="Section-title title_border gray-bg">
				<h2 class="trans-cap manage_icon">
					<span class='header_title'>Supplier Mandatory Documents</span>
				</h2>
			</div>

			<div class="row" style="position: relative; top: 10px;">
			    <div id="content">
			        <!-- main content-->
                    <div class="col-md-12" id="dtList"  >
                        <div  style='min-height:100px;'>
							<table id="tblDocuments" class="display" style="width:100%;font-size:11px;">
                            <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>Document Title</th>
                                    <th>Created By</th>
                                    <th>Created Date</th>
                                    <th>Modified By</th>
                                    <th>Modified Date</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody id='tbl_rows'>
                                <tr>
                                    <td colspan='6'><center>No Data of Supplier's Documents</td>
                                </tr>
                            </tbody>
							</table>

                        </div>

                        <div class="col-md-12">
                            <a href='#' id='btn_new' class='btn btn-sm btn-primary hvr-pop marg-top-10 pull-left'> Create Document</a>
                        </div>
                    </div>

                     <div class="col-md-12" id="dtForm" style="display:none;">
                        <form id="myForm"  enctype="multipart/form-data" >
                            <div class="row">
                                <div class="input-group mb-4">
                                    <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 col-form-label">Document Title</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="doc_title" required>
                                        <span id='notif_file_title' style='color:red;display:none;'>You can not upload file larger than 50MB</span>
                                    </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-group mb-4">
                                    <label for="inputEmail3" class="col-sm-3 col-form-label">Document</label>
                                    <div class="col-sm-9">
                                        <div data-provides="fileinput" class="fileinput fileinput-new input-group">
                                            <div data-trigger="fileinput" class="form-control">
                                                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                                <span id="idICfileUploadSpan" class="fileinput-filename show_name_icAttachment"></span>
                                            </div>
                                            <span class="input-group-addon btn btn-black btn-file">
                                                <span class="fileinput-new">Select file</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input
                                                    type="file" id="doc_file" name="doc_file"
                                                    data-buttonName="btn-black"
                                                    data-validation-allowing="${fileType}"
                                                    data-validation-error-msg-container="#Load_File-error-icAttachment"
                                                    data-validation-max-size="${ownerSettings.fileSizeLimit}M"
                                                    data-validation="extension size required"
                                                    data-validation-error-msg-size="You can not upload file larger than ${ownerSettings.fileSizeLimit}MB"
                                                    data-validation-error-msg-mime="${mimetypes}">
                                                </span>
                                            <a data-dismiss="fileinput" class="input-group-addon btn btn-default fileinput-exists" href="#">Remove</a>
                                        </div>
                                        <div id="Load_File-error-icAttachment" style="width: 100%; float: left; margin: 0 0 10px 0;"></div>
                                        <span id='notif_file_size' style='color:red;display:none;'>You can not upload file larger than 50MB</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-group mb-4">
                                    <label for="inputEmail3" class="col-sm-3 col-form-label"></label>

                                    <div class="col-sm-9">
                                        <div class="card" style="width: 100%;">
                                            <div class="card-body">
                                                <figure>
                                                    <figcaption>Notes:</figcaption>
                                                    <ul>
                                                        <li>Max allowed file size is 50MB</li>
                                                        <li>Allow file type:pdf,doc,docx,xls,xlsx,txt,ziprar,png,pptpptx,jpeg</li>
                                                    </ul>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-group mb-4">
                                    <label for="inputEmail3" class="col-sm-3 col-form-label">Status</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="doc_status">
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <center>

                                    <a href='#' id="btnSubmit" class="btn btn-sm btn-primary hvr-pop marg-top-10 pull-center" >Create</a>

                                    <a href='#'  class="btn btn-sm btn-warning hvr-pop marg-top-10 btnCancel">Cancel</a>
                                </center>
                            </div>
                        </form>

                     </div>
                </div>
                <!-- end main content-->

			</div>
		</div>
	</div>
</div>

<div id="dialog" title="Basic dialog"></div>
<input type="hidden" id="token" name="${_csrf.parameterName}" value="${_csrf.token}" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js"/>"></script>

<script type="text/javascript">
	$('document').ready(function() {
	    var header = $("meta[name='_csrf_header']").attr("content");
        var token = $("meta[name='_csrf']").attr("content");

	    var datas=localStorage.getItem('datas')!=null?localStorage.getItem('datas'):JSON.stringify([]);
        datas=JSON.parse(datas);
	    var res=[];

	    function convertDate(dt){
	        var a = dt.split(" ");
	        var b = a[1].slice(0, 2);
	        var c  = a[1].split(":");
	        var d  = parseInt(b) > 12?"PM":"AM";

	        return a[0]+" "+b+":"+c[1]+":00 "+d;
	    }

	    var App={
	        stateOn:"idle",
	        selectedId:"",
            init:()=>{

                $('#tblDocuments thead tr')
                    .clone(true)
                    .addClass('filters')
                    .appendTo('#tblDocuments thead');


                App.getAll();
                App.handler();
            },
            handler:()=>{
                $('#btn_new').off().on('click',function(){
                    $('.header_title').text('Create Document');
                    $('.header_action').text(' > Create Document');

                    $('#doc_title').val('');
                    $('#doc_file').val('');

                    $('#dtForm').show();
                    $('#dtList').hide();

                    $('.notif_file_title').val('');
                    $('.notif_file_size').val('');

                    $('.notif_file_title').hide()
                    $('.notif_file_size').hide()

                    return false;
                })

                $('.btnCancel').off().on('click',function(){
                    $('.header_title').text('Supplier Mandatory Documents');
                    $('.header_action').text('');
                    $('#btnSubmit').text('Create');
                    App.selectId="";


                    $('#dtForm').hide();
                    $('#dtList').show();

                    return false;
                })


                $.each($('.btn_edit'),function(i,e){
                    $(e).off().on('click',function(){
                        App.stateOn="edit";
                        App.selectedId=$(e).data('id');
                        var status = $(e).data('status')?1:0;


                        $('#doc_title').val($(e).data('title'));
                        $('#idICfileUploadSpan').text($(e).data('filename'));
                        $('#doc_status').val(status)

                        $('.header_title').text('Update Document');
                        $('.header_action').text(' > Update Document');
                        $('#btnSubmit').text('Update');


                        $('#dtForm').show();
                        $('#dtList').hide();

                        return false;
                    });
                });

                $.each($('.btn_delete'),function(i,e){
                    $(e).off().on('click',function(){
                        let id=$(e).data('id');
                        let title=$(e).data('filename');

                        $( "#dialog" ).dialog({
                            content:"delete",
                            title: "Confirm Delete",
                            buttons: [
                                {
                                  text: "Delete",
                                  click: function() {
                                    $.ajax({
                                        url: getContextPath() + "/buyer/deleteDocument",
                                        type: "GET",
                                        data: {
                                            id: id,
                                            name: title
                                        },
                                        beforeSend: function (xhr) {
                                            xhr.setRequestHeader(header, token);
                                            $('#loading').show();
                                        },
                                        success: function (data, textStatus, request) {

                                            window.location.reload();

                                        },
                                        error: function (request, textStatus, errorThrown) {
                                            $('p[id=idGlobalErrorMessage]').html(request.getResponseHeader('error').split(",").join("<br/>"));
                                            $('div[id=idGlobalError]').show();
                                            $('#loading').hide();
                                        },
                                        complete: function () {
                                            $('#loading').hide();
                                        }
                                    });

                                    //$('.ui-dialog-content').empty().html("<div style='padding:5%;background-color:#64FF33;'><i class='glyph-icon icon-ok'></i><center> Document is successfully deleted.</center></div>")

                                    setTimeout(() => {
                                      $( this ).dialog( "close" );
                                    }, 1000);

                                  }
                                },
                                {
                                  text: "Cancel",
                                  class: 'cancelButton',
                                  click: function() {
                                    $( this ).dialog( "close" );
                                  }
                                }
                              ]
                        });

                        $('.ui-dialog-content').empty().html("<div style='padding:5%;'>Are you sure want to delete <b>"+title+"</b> document?</div>")
                        $('.ui-dialog-buttonset > button').eq(0).css({'float':'left',"marginLeft" : "5%"})
                        $('.ui-dialog-buttonset > button').eq(1).css({"float":"right","backgroundColor": "#000","color" : "white","marginRight" : "5%"}).addClass('cancelButton');
                        return false;
                    });
                });

                $('#doc_file').change(()=>{

                    var size=App.findPropFile()['size']/1000000; //MB
                    var type=App.findPropFile()['type'].split('/');

                    if(parseFloat(size) > 50){
                        $('#notif_file_size').show().text('You can not upload file larger than 50MB');
                        var $el = $(this);
                        $el.wrap('<form>').closest('form').get(0).reset();
                        $el.unwrap();
                        return;
                    }else{
                        $('#notif_file_size').hide();
                    }

                    if(type[0]=="video"){
                        $('#notif_file_size').show().text('Only File of type pdf,doc,docx,xls,xlsx,txt,zip,rar,png,ppt,pptx,jpeg,jpg is allowed.');
                        return;
                    }

                    if($('#doc_file') && $('#doc_file')[0] && $('#doc_file')[0].files && $('#doc_file')[0].files[0]) {
                        $(".show_name_icAttachment").html($('#doc_file')[0].files[0].name);
                    }

                })

                $('#btnSubmit').off().on('click',function(){

                    if($('#doc_title').val()== ""){
                        $('#notif_file_title').show().text("This is a required field")
                        return false;
                    }else{
                        $('#notif_file_title').show().text("")
                    }

                    if($('#doc_title').val().length > 128){
                        $('#notif_file_title').show().text("The input value is longer than 128 characters")
                        return false;
                    }else{
                        $('#notif_file_title').show().text("")
                    }

                    if($('#doc_file').val()== ""){
                        $('#notif_file_size').show().text("This is a required field")
                        return false;
                    }

                    $('.notif_file_title').hide()
                    $('.notif_file_size').hide()

                    $(this).parents('form').submit()

                });

                $( "#myForm" ).off().on('submit',function (e) {
                    var ep="/buyer/createSupplierMandatoryDocuments";

                    var formData = new FormData();
                    if(App.stateOn="edit"){
                        ep="/buyer/updateSupplierMandatoryDocuments";
                        formData.append('id', App.selectedId);
                    }

                    formData.append('title', $('#doc_title').val());
                    formData.append('status', $('#doc_status').find(':selected').val());
                    formData.append('file', $('input[type=file]')[0].files[0]);

                    $.ajax({
                        url         :   getContextPath() + ep,
                        beforeSend  :   function(xhr){xhr.setRequestHeader("X-AUTH-KEY",$('#token').val())},
                        enctype     :   'multipart/form-data',
                        type        :   "post",
                        data        :   formData,
                        contentType :   false,
                        processData :   false,
                        success     :   function(response) {
                            //$('.btnCancel').trigger('click');
                            window.location.reload();
                        },
                        error: function(xhr) {
                            $('#notif_file_title').show().text("Document Title already exist")
                            return false;
                        }
                    });

                    e.preventDefault();
                    return false;
                });
            },
            findPropFile:() =>{

                var fileInput =  document.getElementById("doc_file");
                try{
                    console.log(fileInput.files[0]);
                    return {
                        'size':fileInput.files[0].size,
                        'type':fileInput.files[0].type,
                    };
                }catch(e){
                    var objFSO = new ActiveXObject("Scripting.FileSystemObject");
                    var e = objFSO.getFile( fileInput.value);
                    console.log(e);
                    var fileSize = e.size;
                    return {
                        'size':e.size,
                        'type':e.type,
                    };
                }
            },
            getAll:()=>{
                App.render("/buyer/allSupplierMandatoryDocuments",'GET',(f)=>{
                    App.renderTable(function(){
                        App.handler();
                    })
                })
            },
            renderTable:(f) =>{
                var el="";

                if(res.length > 0){

                    $.each(res,function(i,e){
                        let status=e.status?'ACTIVE':'NON ACTIVE';

                        el+="<tr>";
                            el+="<td><center>";
                                el+="<a data-id='"+e.id+"' data-title='"+e.description+"' data-filename='"+e.fileName+"' data-file='"+e.fileData+"' data-content='"+e.contentType+"' data-status='"+e.status+"' href='#'  class='btn_edit'><img src='"+getContextPath()+"/resources/images/edit1.png' /></a>";
                                el+="<a data-id='"+e.id+"' data-filename='"+e.fileName+"' data-file='"+e.fileData+"' href='#'   class='btn_delete' ><img src='"+getContextPath()+"/resources/images/delete1.png' /></a>";
                            el+="</td></center>";
                            el+="<td><a href='"+getContextPath()+"/downloadSMD/"+e.id+"'>"+e.description+"</a></td>";
                            el+="<td>"+e.createdBy.loginId+"</td>";
                            el+="<td>"+convertDate(e.uploadDate)+"</td>";
                            el+="<td>"+e.createdBy.loginId+"</td>";
                            el+="<td>"+convertDate(e.uploadDate)+"</td>";
                            el+="<td>"+status+"</td>";
                        el+="</tr>";
                    });

                }else{
                    el+="<tr>";
                            el+="<td colspan=7><center><div class='alert alert-warning'>No Data of Supplier's Documents</div></center></td>";
                    el+="</tr>"
                }

                $('#tbl_rows').empty().html(el)
                if(res.length > 0){

                    App.dataTables();
                }

                f()

            },
            dataTables:()=>{
                try{

                    $('#tblDocuments').DataTable({
                        //bDestroy: true,
                         orderCellsTop: true,
                         fixedHeader: true,
                         initComplete: function () {
                            if(res.length > 0){

                            $('.filters').find('th').eq(0).text('');

                             var api = this.api();
                             api.columns()
                                 .eq(0)
                                 .each(function (colIdx) {

                                    if(colIdx==1 || colIdx==2 || colIdx==4 || colIdx==6){
                                         var cell = $('.filters th').eq(
                                             $(api.column(colIdx).header()).index()
                                         );
                                         var title = $(cell).text();
                                         $(cell).html('<input type="text" class="form-control" placeholder="Search ' + title + '" />');

                                         $('input',$('.filters th').eq($(api.column(colIdx).header()).index()))
                                         .off('keyup change')
                                         .on('change', function (e) {

                                             $(this).attr('title', $(this).val());
                                             var regexr = '({search})'; //$(this).parents('th').find('select').val();

                                             var cursorPosition = this.selectionStart;

                                             api
                                                 .column(colIdx)
                                                 .search(
                                                     this.value != ''
                                                         ? regexr.replace('{search}', '(((' + this.value + ')))')
                                                         : '',
                                                     this.value != '',
                                                     this.value == ''
                                                 )
                                                 .draw();
                                         })
                                         .on('keyup', function (e) {
                                             e.stopPropagation();

                                             $(this).trigger('change');
                                             $(this)
                                                 .focus()[0]
                                                 .setSelectionRange(cursorPosition, cursorPosition);
                                         });
                                    }
                                });
                            }
                         }
                    });

                }catch(e){

                }

            },
            render:(ep,type,f)=>{

                $.ajax({
                    url         :   getContextPath() + ep,
                    beforeSend  :  function(xhr){xhr.setRequestHeader("X-AUTH-KEY",$('#token').val())},
                    enctype     : 'multipart/form-data',
                    type        :   type,
                    success     :   function(response) {
                        res=response;
                        console.log(response);
                        f();
                    },
                    error: function(xhr) {

                    }
                });
            }
	    }

	    App.init();

	});

</script>