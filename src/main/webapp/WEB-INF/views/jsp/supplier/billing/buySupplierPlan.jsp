<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/elements/procurehere-public.css"/>">
<div class="select_pack_wrap">
	<div class="sp_inner">
		<div class="sp_box1">
			<jsp:include page="/WEB-INF/views/jsp/templates/ajaxMessage.jsp" />
			<jsp:include page="/WEB-INF/views/jsp/templates/message.jsp" />
			<div class="clear"></div>


			<c:forEach items="${planList}" var="plan" varStatus="status">
				<div id="tab-${status.index}" class="tab-content ${status.index == 0 ? 'current' : '' } doc-fir tab-main-inner" style="display: none;">




					<fmt:formatNumber groupingUsed="false" type="number" var="basePrice" minFractionDigits="2" maxFractionDigits="2" value="${plan.price}" />
					<div>
						<c:choose>
							<c:when test="${empty changePlan}">
								<c:url var="buyerSubs" value="/buyerSubscription/get/${plan.id}" />
							</c:when>
							<c:otherwise>
								<c:url var="buyerSubs" value="/buyer/billing/buyBuyerPlan/${plan.id}" />
							</c:otherwise>
						</c:choose>

						<form:form action="${pageContext.request.contextPath}/supplier/billing/doBuyPlanInitiate/${plan.id}" id="supplierCheckoutForm" method="POST"  commandName="subscription">

							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
							<div class="col-sm-12 col-md-12 col-lg-4 col-xs-12 pack-amount-details" id="ubp">
								<h4 class="border-btm">${plan.planName}</h4>
								<div class="pack-details">


									<div class="pack-details">
										<label class="width-label-100"> <input type="radio" name="supplerPlan" value="ALLBUYER" checked="checked"> <span class="left-span">${plan.planName}</span> <span class="pull-right width-label-100-span-2">${plan.price}</span>
										</label>
										<p class="usd-details">${plan.currency.currencyCode}&nbsp;${plan.price}&nbsp;/&nbsp;${plan.periodUnit}</p>
									</div>

								</div>


								<div class="pack-details">
									<div class="form-inline">
										<label class="margin-right-20">Promo Code</label> <input type="text" name="promocode" id="promocode" class="form-control"> <span class="pull-right " id="promoDiscountPrice"> <input type="hidden" name="promoDiscountPrice" value="0.00"> 0.00
										</span> <span id="promoError"></span>
									</div>
								</div>


								<div class="pack-details">

									<div class="pack-details">
										<h4 class="border-btm"></h4>
										<h4 class="pull-right" id="totalPrice">
											US$ <input type="hidden" name="totalFeeAmount" value="${basePrice}"> ${basePrice}
										</h4>

										<div id="paypalPayment">
											<span class="rfr_field proceedPayment" id="supplierCheckOutBtn"></span>
										</div>
									</div>

								</div>
							</div>
						</form:form>
					</div>
				</div>
			</c:forEach>
		</div>
		<!--
		<div class="planGroup">
			<c:forEach items="${planList}" var="plan">
				<div class="sp_box1">
					<div class="spb_heading1">
						<c:out value="${plan.planName}" />
					</div>
					<div class="tpb_open_box">
						<c:out value="${plan.description}" escapeXml="false" />
					</div>
					<div class="choose_bttn">
						<a href="${pageContext.request.contextPath}/subscription/get/${plan.id}" class="cb_style cb_grey hvr-pop">Get ${plan.planName}</a>
					</div>
				</div>
			</c:forEach>
		</div> -->
		<c:if test="${empty planList}">
			<div class="sp_box2">
				<div class="spb_heading4">NO PLANS DEFINED. LOGIN AS ADMIN AND DEFINE SOME PLANS.</div>
			</div>
		</c:if>
	</div>
	<div class="clear"></div>
</div>
<style>
div#idGlobalError {
	float: left;
	width: 100%;
}

.alert .alert-icon {
	line-height: 34px;
	float: left;
	width: 34px;
	height: 34px;
	margin: 5px 10px 0 0;
	text-align: center;
}

.alert .alert-title {
	font-size: 12px;
	font-weight: bold;
	margin: 4px 0 3px;
	padding: 0;
	text-transform: uppercase;
}

.table_align_center {
	width: 80%;
	margin-left: 10%;
	margin-right: 10%;
}

.spb_heading1 {
	font-size: 30px;
	text-transform: none;
}

.spb_heading1 {
	width: 100%;
	float: left;
	text-align: center;
	font-size: 35px;
	text-transform: uppercase;
	color: #758d94;
	padding: 0 0 20px 0;
}

.sp_box1 {
	width: 70%;
	border: 1px solid #ccc;
	margin-bottom: 10px;
}

<!--
for buyer subscription tab  -->.home_tab_wrap {
	width: 100%;
	float: left;
}

ul.tabs {
	margin: 0px;
	padding: 0px;
	list-style: none;
}

.pull-left, .float-left {
	float: left !important;
}

ul.tabs li.current {
	background: #fff;
	color: #636363;
	border-left: 1px solid #e8e8e8;
	border-right: 1px solid #e8e8e8;
	border-top: 1px solid #e8e8e8;
}

ul.tabs li {
	background: #a0a0a0;
	color: #fff;
	display: inline-block;
	padding: 10px 25px;
	cursor: pointer;
	font-family: 'open_sanssemibold';
	font-weight: normal;
	margin-right: 5px;
	border-radius: 3px 3px 0 0;
	position: relative;
	z-index: 2;
}

.tab-content.current {
	display: block !important;
}

.tab-main-inner {
	float: left;
	width: 100%;
	text-align: left;
	background: #fff;
	margin-top: -1px;
	border: 1px solid #e8e8e8;
	position: relative;
	z-index: 1;
}

.marg_top_30 {
	margin-top: 30px;
}

.marg_bottom_10 {
	margin-bottom: 10px;
}

.table1 td, th {
	text-align: center;
}

.td-border td {
	border: 2px solid #a1a1a1;
	padding-top: 10px;
	padding-bottom: 10px;
	padding-left: 10px;
	padding-right: 10px;
	font-size: 15px;
	padding-right: 10px;
}

.td-border {
	border: 2px solid #a1a1a1;
}

.total-td {
	text-align: right !important;
	padding-top: 0px !important;
	padding-bottom: 0px !important;
}

.flagvisibility {
	display: none;
}

.help {
	color: #fff;
	background-color: #feb22a;
	width: 12px;
	height: 12px;
	display: inline-block;
	border-radius: 100%;
	font-size: 10px;
	text-align: center;
	text-decoration: none;
	-webkit-box-shadow: inset -1px -1px 1px 0px rgba(0, 0, 0, 0.25);
	-moz-box-shadow: inset -1px -1px 1px 0px rgba(0, 0, 0, 0.25);
	box-shadow: inset -1px -1px 1px 0px rgba(0, 0, 0, 0.25);
}

.paypal1 {
	margin-left: 45px;
}
</style>


<script src="<c:url value="/resources/assets/js-core/jquery.form-validator.js"/>"></script>
<script src="//www.paypalobjects.com/api/checkout.js" async></script>
<script type="text/javascript">
	$.validate({
		lang : 'en',
		modules : 'date, security'
	});

	window.paypalCheckoutReady = function() {
		paypal.checkout.setup('${merchantId}', {
			environment : '${paypalEnvironment}',
			container : 'supplierCheckoutForm',
			condition : function() {
				return $('#supplierCheckoutForm').isValid();
			},
			//button: 'placeOrderBtn'
			buttons : [ {
				container : 'supplierCheckOutBtn',
				type : 'checkout',
				color : 'blue',
				size : 'medium',
				shape : 'rect'
			} ]
		});

	};
</script>
<style type="text/css">
.pass-desc {
	color: #7f7f7f;
	font-weight: normal;
	/* 	font-size: 13px; */
}
</style>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form-validator.js"/>"></script>
<script>
	$.validate({
		lang : 'en'
	});
</script>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.mask.min.js"/>"></script>
<script>
	$(document).ready(function() {
		$(".pwd").click(function() {
			$('#passwordPlaceHolder').hide();
		});

		$('#idAdminMobileNo').mask('+00 00000000000', {
			placeholder : "+60 122735465"
		});
		$('#idCompanyContactNumber').mask('+00 00000000000', {
			placeholder : "+60 322761533"
		});
	});
</script>

<script type="text/javascript" src="<c:url value="/resources/js/view/supplierCheckout.js"/>"></script>