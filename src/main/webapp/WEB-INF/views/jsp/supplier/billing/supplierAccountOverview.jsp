<%@ page import="org.apache.velocity.runtime.parser.node.GetExecutor"%>
<%@ page import="org.w3c.dom.Document"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<script>
	$(document).ready(function() {
		$('.mega').on('scroll', function() {
			$('.header').css('top', $(this).scrollTop());
		});
	});
</script>
<div id="page-content" view-name="buyerAccountOverview">
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="${pageContext.request.contextPath}/supplier/supplierDashboard"><spring:message code="application.dashboard"/></a></li>
			<li class="active"><spring:message code="paymentbilling.account.overview"/></li>
		</ol>
		<!-- page title block -->
		<div class="Section-title title_border gray-bg">
			<h2 class="trans-cap acc-over"><spring:message code="paymentbilling.account.overview"/></h2>
		</div>
		<div class="clear"></div>
		
		<c:if test="${supplier.supplierPackage != null}">
							<div class="Invited-Supplier-List create_sub marg-bottom-20">
								<div class="col-md-20">
									<c:if test="${!empty supplier.supplierPackage.endDate}">
										<div class="col-md-8 marg-top-10">
											<h4>
												<spring:message code="supplierbilling.current.subscription.expires"/>
												<fmt:formatDate value="${supplier.supplierPackage.endDate }" pattern="dd/MM/yyyy" timeZone="<%=request.getSession().getAttribute(\"timeZone\")%>" />
											</h4>
										</div>
									</c:if>
									<div class="marg-top-10">

										<c:if test="${empty subscription.id or empty subscription.supplierPlan.id}">
											<a href="${pageContext.request.contextPath}/supplier/billing/buyPlan" class="btn btn-info ph_btn_midium hvr-pop hvr-rectangle-out marg-right-10 marg-bottom-10" style="float: right;"><spring:message code="supplier.dashboard.subscribe"/></a>
										</c:if>

										 <c:if test="${not empty subscription.supplierPlan.id}">
											<form:form action="${pageContext.request.contextPath}/supplier/billing/renew/${subscription.supplierPlan.id}" id="supplierCheckoutForm" method="POST" commandName="subscription">
												<div class="choose_bttn pull-right marg-right-10 marg-bottom-20" id="RenewBtn">
													<label><spring:message code="paymentbilling.renew.subscription.btn"/> :&nbsp;</label>
												</div>
												<!-- <button class="btn btn-info pad-left-15 pad-right-15 hvr-pop hvr-rectangle-out marg-right-10 marg-bottom-10" id="s1_tender_adddel_btn" style="float: right; ">Renew Subscription</button> -->
											</form:form>
										</c:if> 

									</div>
								</div>
							</div>
						</c:if>
		<div class="Invited-Supplier-List marg-bottom-20 bill-detail">
		
		
			<div class="Invited-Supplier-List-table add-supplier white-bg">
			
			
			
				<div class="ph_tabel_wrapper">
				
				
				
				
					<div class="account-overview">
						<table class="border-none" width="100%" border="0" cellspacing="0" cellpadding="0">
							<tbody>
								<tr>
									<td style="vertical-align: top;"><spring:message code="account.overview.plan"/></td>
									<td><b>${supplier.supplierPackage == null ? "No Subscription" : (supplier.supplierPackage.supplierPlan != null ?supplier.supplierPackage.supplierPlan.planName:"FREE TRIAL")}</b> ${supplier.supplierPackage == null ? "" : "<br/>"} ${supplier.supplierPackage == null ? "" : supplier.supplierPackage.supplierPlan.shortDescription}</td>
								</tr>
								<tr>
									<td>Subscription Status</td>
									<td><span style="color: ${supplier.supplierPackage != null ? (supplier.supplierPackage.subscriptionStatus == 'ACTIVE' ? 'green;' : '') : ''}"> ${supplier.supplierPackage != null ? supplier.supplierPackage.subscriptionStatus : 'Not Applicable'}</span></td>
								</tr>
								<tr>
									<td><spring:message code="account.overview.subscription.valid" /></td>
									<td><strong> <jsp:useBean id="now" class="java.util.Date" /> <c:if test="${supplier.supplierPackage != null}">
												<fmt:formatDate value="${supplier.supplierPackage.startDate}" pattern="dd/MM/yyyy" timeZone="<%=request.getSession().getAttribute(\"timeZone\")%>" /> to 
															<fmt:formatDate value="${supplier.supplierPackage.endDate}" pattern="dd/MM/yyyy" timeZone="<%=request.getSession().getAttribute(\"timeZone\")%>" />
												<c:choose>
													<c:when test="${supplier.supplierPackage != null and supplier.supplierPackage.endDate != null and (now gt supplier.supplierPackage.endDate)}">
														<span style="color: red;"> (<spring:message code="account.overview.status.expired"/>) </span>
													</c:when>
													<c:when test="${supplier.supplierPackage != null and supplier.supplierPackage.endDate != null and supplier.supplierPackage.startDate != null and (now ge supplier.supplierPackage.startDate and now le supplier.supplierPackage.endDate)}">
														<span style="color: green;">(<spring:message code="account.overview.status.active"/>) </span>
													</c:when>
													<c:otherwise>
														<span style="color: blue;">(<spring:message code="account.overview.status.notactive"/>)</span>
													</c:otherwise>
												</c:choose>
											</c:if> <c:if test="${supplier.supplierPackage == null }">
											<spring:message code="application.not.applicable2"/>
										</c:if>
									</strong></td>
								</tr>
								<%-- <tr>
									<td>Payment</td>
									<td>
										<strong> <c:if test="${supplier.supplierSubscription.paymentTransaction != null}">
												${supplier.supplierSubscription.paymentTransaction.currencyCode}&nbsp;${supplier.supplierSubscription.paymentTransaction.amount}
											</c:if> <c:if test="${supplier.supplierSubscription.paymentTransaction == null}">
												Not Applicable
											</c:if>
										</strong>
									</td>
								</tr> --%>
								<tr>
									<td><spring:message code="accountoverview.buyer.limit"/></td>
									<td><strong> <c:if test="${supplier.supplierPackage != null}">
												${supplier.supplierPackage.buyerLimit >= 999 ? 'All Buyers' : supplier.supplierPackage.buyerLimit }
											</c:if> <c:if test="${supplier.supplierPackage == null }">
												<spring:message code="application.not.applicable2"/>
											</c:if>
									</strong></td>
								</tr>
								<c:if test="${!empty supplier.associatedBuyers}">
									<tr>
										<td style="vertical-align: top;"><spring:message code="accountoverview.associated.buyers"/></td>
										<td>
											<ul style="padding-left: 20px;">
												<c:forEach items="${supplier.associatedBuyers}" var="buyer">
													<li>${buyer.companyName}</li>
												</c:forEach>
											</ul>
										</td>
									</tr>
								</c:if>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	
<c:if test="${not empty subscription.supplierPlan.id}">	
<script src="//www.paypalobjects.com/api/checkout.js" async></script>
<script type="text/javascript">
	window.paypalCheckoutReady = function() {
		paypal.checkout.setup('${merchantId}', {
			environment : '${paypalEnvironment}',
			container : 'supplierCheckoutForm',
			//button: 'placeOrderBtn'
			buttons : [ {
				container : 'RenewBtn',
				type : 'checkout',
				color : 'blue',
				size : 'medium',
				shape : 'rect',
				label : 'Renew',
			} ]
		});

	};
</script>
</c:if>
	
</div>
