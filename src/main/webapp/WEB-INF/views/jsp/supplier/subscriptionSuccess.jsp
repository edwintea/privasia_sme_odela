<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/elements/saas.css"/>">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/elements/procurehere1.css"/>">
<div id="page-content">
	<div class="container col-md-12">
		<div class="registraion_wrap">
			<div class="reg_inner">
				<div class="reg_heading">${renewal ? 'Renewal' : ''} Subscription successful for plan [${subscription.supplierPlan.shortDescription}]</div>
				<div class="reg_form_box">
					<div class="con_inner">
						<div class="con_row">
							<div class="con_text">Summary of Transaction</div>
							<c:if test="${subscription.supplierPlan.price > 0 }">
								<div class="con_result">Subscription ${renewal ? 'Renewal' : ''} for ${subscription.supplierPlan.period} ${subscription.supplierPlan.chargeModel == 'FLAT_FEE' ? (subscription.supplierPlan.periodUnit) : ' CREDIT'}S.</div>
							</c:if>
							<c:if test="${subscription.supplierPlan.price == 0 }">
								<div class="con_result">Trial subscription for ${subscription.supplierPlan.period} ${subscription.supplierPlan.periodUnit}S.</div>
							</c:if>
						</div>
						<div class="con_row">
							<div class="con_text">Transaction Status</div>
							<div class="con_result" style="color: ${paypalResponse['ACK'] == 'Success' ? 'green' : 'red'};">${paypalResponse['ACK']}</div>
						</div>
						<div class="con_row">
							<div class="con_text">Transaction ID</div>
							<div class="con_result">${paypalResponse['PAYMENTINFO_0_TRANSACTIONID']}</div>
						</div>
						<c:if test="${!empty paypalResponse['PAYMENTINFO_0_CURRENCYCODE'] }">
							<div class="con_row">
								<div class="con_text">Total Amount</div>
								
						<c:set var="tax" value="${subscription.supplierPlan.tax != null ?subscription.supplierPlan.tax : 0}" scope="page"/>
								<fmt:formatNumber var="tax" type="number" minFractionDigits="0" maxFractionDigits="2" value="${tax}" />
								<div class="con_result">${paypalResponse['PAYMENTINFO_0_CURRENCYCODE']}&nbsp;${paypalResponse['PAYMENTINFO_0_AMT']} (Inclusive of ${tax}% GST)</div>
							</div>
						</c:if>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>