<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/elements/procurehere1.css"/>">

<c:set var="viewMode" value="true" scope="request" />
<div id="page-content-wrapper">
	<div id="page-content">
		<div class="container">
			<ol class="breadcrumb">
				<c:url var="supplierDashboard" value="/supplier/supplierDashboard" />
				<li><a href="${supplierDashboard}"> <spring:message code="application.dashboard" />
				</a></li>
				<li class="active"><spring:message code="supplier.po.purchase.order" /></li>
			</ol>


			<ul class="nav-responsive nav nav-tabs marg-bottom-5">
			    <li class="active"><a href="#tab1" data-toggle="tab">Purchase Order</a></li>
			    <li><a href="#tab2"  data-toggle="tab">Delivery Orders <i class="noti-round-absolute bs-badge badge-absolute badge-blue-alt">${po.doCount != null ? po.doCount : 0} </i> </a></li>
			    <li><a href="#tab3" data-toggle="tab">Invoices <i class="noti-round-absolute bs-badge badge-absolute badge-blue-alt">${po.invoiceCount != null ? po.invoiceCount : 0}</i> </a></li>
			</ul>
			<div class="tab-content">
			    <div class="tab-pane active" id="tab1">
					<div class="Section-title title_border gray-bg mar-b20">
						<h2 class="trans-cap supplier">
							<spring:message code="defaultmenu.po" /> : ${po.name}
						</h2>
						<h2 class="trans-cap pull-right">
						<spring:message code="supplier.po.summary.poStatus" /> :  ${po.status}
						</h2>
					</div>
					<div class="clear"></div>
					<jsp:include page="supplierPrSummary.jsp"></jsp:include>
				</div>
			    <div class="tab-pane" id="tab2">
					<div class="Section-title title_border gray-bg mar-b20">
						<h2 class="trans-cap supplier">
							<spring:message code="defaultmenu.po" /> : ${po.name}
						</h2>
						<h2 class="trans-cap pull-right">
						<spring:message code="supplier.po.summary.poStatus" /> :  ${po.status}
						</h2>
					</div>
					<div class="clear"></div>

			    	<jsp:include page="doQuickView.jsp"></jsp:include>
				</div>
			    <div class="tab-pane" id="tab3">
					<div class="Section-title title_border gray-bg mar-b20">
						<h2 class="trans-cap supplier">
							<spring:message code="defaultmenu.po" /> : ${po.name}
						</h2>
						<h2 class="trans-cap pull-right">
						<spring:message code="supplier.po.summary.poStatus" /> :  ${po.status}
						</h2>
					</div>
					<div class="clear"></div>

			    	<jsp:include page="invoiceQuickView.jsp"></jsp:include>
				</div>
			</div>			

		</div>
	</div>
</div>