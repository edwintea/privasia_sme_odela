<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/elements/saas.css"/>">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/elements/procurehere1.css"/>">
<div id="page-content">
	<div class="container col-md-12">
		<header class="form_header">
			<div class="Section-title title_border gray-bg">
				<h2 class="trans-cap Events-Listing-heading">Supplier Subscription Plans</h2>
			</div>
		</header>
		<div class="select_pack_wrap marg-top-20">
			<div class="sp_inner">
				<c:forEach items="${planList}" var="plan">
					<c:if test="${empty supplier.supplierSubscription or (!empty supplier.supplierSubscription && supplier.supplierSubscription.supplierPlan.id != plan.id)}">
						<div class="sp_box1">
							<div class="spb_heading1">
								<c:out value="${plan.planName}" />
							</div>
							<div class="tpb_open_box">
								<c:out value="${plan.description}" escapeXml="false" />
							</div>
							<div class="clear"></div>
							<div class="marg-top-10">
								<form id="idSubscribeForm${plan.id}" action='${pageContext.request.contextPath}/supplier/payment/${plan.id}' method="post">
									<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
									<c:if test="${plan.buyerLimit == 1}">
										<select name="buyerId" id="buyerId" data-validation="required" class="chosen-select">
											<option value="">Select Buyer</option>
											<c:forEach items="${buyerList}" var="buyer">
												<option value="${buyer.id}">${buyer.companyName}</option>
											</c:forEach>
										</select>
									</c:if>
									<div class="choose_bttn" id="btn${plan.id}">
										<%-- 					<a href="${pageContext.request.contextPath}/supplierSignup/${plan.id}" class="cb_style cb_grey hvr-pop">Get ${plan.planName}</a>
 --%>
									</div>
								</form>
							</div>
						</div>
					</c:if>
				</c:forEach>
				<c:if test="${empty planList}">
					<div class="sp_box2">
						<div class="spb_heading4">NO PLANS DEFINED. LOGIN AS ADMIN AND DEFINE SOME PLANS.</div>
					</div>
				</c:if>
			</div>
			<div class="clear"></div>
			<div style="height: 150px;"></div>
		</div>
	</div>
</div>
<style>
.spb_heading1 {
	font-size: 30px;
	text-transform: none;
}

.sp_box1 {
	border: 1px solid #ccc;
	margin-bottom: 10px;
}
</style>
<script type="text/javascript" src="<c:url value="/resources/js/masonry.js"/>"></script>
<script src="<c:url value="/resources/assets/js-core/jquery.form-validator.js"/>"></script>
<script>
	$.validate({
		lang : 'en',
		modules : 'date, security'
	});

	$(document).ready(function() {

		$('.sp_inner').masonry({
			// options
			itemSelector : '.sp_box1',
			gutter : 10
		});

		/*
		
		$('.planGroup').each(function(i) {
			$(this).addClass('planGroupColor_' + i);
		}); //cb_grey 
		$('.sp_box1, .sp_box2').each(function(i) {
			$(this).find("div[class^='spb_heading']").addClass('planColor_' + (i + 1));
			$(this).find(".spa_left_bot").addClass('planColor_' + (i + 1));
			$(this).find(".cb_style").addClass('planColorBg_' + (i + 1));
		}); */
	});
	window.paypalCheckoutReady = function() {
		paypal.checkout.setup('${merchantId}', {
			environment : '${paypalEnvironment}',
			/* container : 'idSubscribeForm', */
			condition : function() {
				return $('#idSubscribeForm').isValid();
			},
			//button: 'placeOrderBtn'
			buttons : [ 
<c:forEach items="${planList}" var="plan" varStatus="status">
<c:if test="${empty supplier.supplierSubscription or (!empty supplier.supplierSubscription && supplier.supplierSubscription.supplierPlan.id != plan.id)}">
${status.index > 0 ? "," : ""}
{
				container : 'btn${plan.id}',
				type : 'checkout',
				color : 'blue',
				size : 'medium',
				shape : 'rect'
			}
</c:if>
</c:forEach>
			]
		});
	};
</script>
<script src="//www.paypalobjects.com/api/checkout.js" async></script>
