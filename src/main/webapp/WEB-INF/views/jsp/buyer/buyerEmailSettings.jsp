<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<sec:authorize access="hasRole('ADMIN') and hasRole('BUYER')" var="canEdit" />
<sec:authorize access="hasRole('ROLE_ADMIN_READONLY')" var="buyerReadOnlyAdmin" />
<div id="page-content">
	<div class="col-md-offset-1 col-md-10">
		<ol class="breadcrumb">
			<c:url var="buyerDashboard" value="/buyer/buyerDashboard" />
			<li>
				<a id="dashboardLink" href="${buyerDashboard}"> <spring:message code="application.dashboard" />
				</a>
			</li>
			<li class="active">Email Settings</li>
		</ol>
		<div class="Section-title title_border gray-bg">
			<h2 class="trans-cap manage_icon">Email Server Settings</h2>
		</div>
		<div class="Invited-Supplier-List import-supplier white-bg">
			<div class="meeting2-heading">
				<jsp:include page="/WEB-INF/views/jsp/templates/message.jsp" />
				<h3>
					<spring:message code="emailSettings.email.server" />
				</h3>
			</div>
			<div class="import-supplier-inner-first-new pad_all_15 global-list">
				<form:form method="post" id="emailSettingform" action="saveEmailSettings" modelAttribute="buyerEmailSettings" autocomplete="off">
					<form:hidden path="id" />
					<div class="row marg-bottom-20 marg_left_0">
						<div class="col-md-3">
							<form:label path="host" cssClass="marg-top-10">
								<spring:message code="email.settings.host" />
							</form:label>
						</div>
						<div class="col-md-5">
							<form:input path="host" id="emailHost" cssClass="form-control" maxlength="45" placeholder="Enter host" data-validation="required" data-validation-allowing="." />
						</div>
					</div>
					<div class="row marg-bottom-20 marg_left_0">
						<div class="col-md-3">
							<form:label path="port" cssClass="marg-top-10">
								<spring:message code="email.settings.port" />
							</form:label>
						</div>
						<div class="col-md-5">
							<form:input path="port" data-validation="required number" maxlength="6" cssClass="form-control" placeholder="Enter Port Number" />
						</div>
					</div>
					<div class="row marg-bottom-20 marg_left_0">
						<div class="col-md-3">
							<form:label path="tls" cssClass="marg-top-10">
								<spring:message code="email.settings.tls" />
							</form:label>
						</div>
						<div class="col-md-5">
							<form:select path="tls" value="${buyerEmailSettings.tls}" class="chosen-select" id="chosenOrder">
								<form:option value="Yes">Yes</form:option>
								<form:option value="No">No</form:option>
							</form:select>
						</div>
					</div>
					<div class="row marg-bottom-20 marg_left_0">
						<div class="col-md-3">
							<form:label path="userName" cssClass="marg-top-10">
								<spring:message code="email.settings.username" />
							</form:label>
						</div>
						<div class="col-md-5">
							<form:input path="userName" id="emailUserName" data-validation="required" data-validation-length="4-145" data-validation-error-msg-length="User name value must be between 4 to 145." cssClass="form-control" placeholder="Enter User name" autocomplete="off"/>
						</div>
					</div>
					<div class="row marg-bottom-20 marg_left_0">
						<div class="col-md-3">
							<form:label path="password" cssClass="marg-top-10">
								<spring:message code="email.settings.password" />
							</form:label>
						</div>
						<div class="col-md-5">
							<input type="password" Class="form-control" data-validation="required length" data-validation-length="min8" placeholder='<spring:message code="application.password.placeholder"/>' id="idEmailPassword" name="emailPassword" />
						</div>
					</div>
					<div class="row marg-bottom-20 marg_left_0">
						<div class="col-md-3">
							<form:label path="emailAddress" cssClass="marg-top-10">
								<spring:message code="email.settings.email" />
							</form:label>
						</div>
						<div class="col-md-5">
							<form:input path="emailAddress" data-validation="required email" cssClass="form-control" placeholder="Enter Email Address" />
						</div>
					</div>
					<div class="row marg-bottom-20 marg_left_0">
						<div class="col-md-4 mr_25">
<%-- 						<c:if test="${canEdit and !buyerReadOnlyAdmin}"> --%>
							<form:button type="submit" id="submitSettingForm" class="btn btn-info ph_btn_midium hvr-pop hvr-rectangle-out">Update</form:button>
<%-- 						</c:if> --%>
						</div>
						<div class="col-md-4">
							<a href="${pageContext.request.contextPath}/buyer/buyerDashboard" class="btn btn-black hvr-pop hvr-rectangle-out1 ph_btn_midium button-previous">Cancel</a>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form-validator.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.mask.min.js"/>"></script>
<script type="text/javascript">
$( document ).ready(function() {
    

});

</script>
<style>
.d-flex {
	display: flex;
}

.mr_25 {
	margin-left: 25%;
}
</style>
<script>
$.validate({
		lang : 'en'
		
	});

</script>
