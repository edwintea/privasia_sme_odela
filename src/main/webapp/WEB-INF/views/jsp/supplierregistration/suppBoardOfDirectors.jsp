<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div id="page-content-wrapper">
<style>
.fa-trash-o {
	color: red;
}

.showModal {
	display: block;
	background: #ffffffa6;
}

.hideModal {
	display: none;
}

.no-padding {
	padding: 0;
}
</style>
    <div id="page-content">
        <div class="container">
            <div class="clear"></div>
            <jsp:include page="supplierProfileDetails.jsp" />
            <div class="tab-main-inner pad_all_15">
                <div class="content-box">
                    <!-- <jsp:include page="/WEB-INF/views/jsp/templates/ajaxMessage.jsp" /> -->
                    <h3 class="content-box-header">
                        <spring:message code="application.org.details" />
                    </h3>
                    <form:form id="boardOfDirectorForm" class="form-horizontal"  method="post" enctype="multipart/form-data" autocomplete="off"
                        modelAttribute="boardOfDirector"
                        action="${pageContext.request.contextPath}/supplier/supplierOrganizationalDetails?${_csrf.parameterName}=${_csrf.token}">
                        <%-- <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> --%>
                        <div class="form-group" style="display: none;">
                            <label for="id" class="col-sm-3 control-label">Id</label>
                            <div class="col-sm-6 col-md-5">
                                <form:input type="text" path="id" cssClass="form-control" name="id" placeholder=""
                                    id="directorId" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="dirName" class="col-sm-3 control-label">Director's Name</label>
                            <div class="col-sm-6 col-md-5">
                                <form:input type="text" path="directorName" cssClass="form-control" id="idDirectorName"
                                    name="idDirectorName" placeholder="Key in Director's name" data-validation="length required custom"
                                    data-validation-length="1-64" data-validation-regexp="^[A-Za-z-\/'' '.]{0,64}$" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="dirType" class="col-sm-3 control-label">Citizenship :</label>
                            <div class="col-sm-6 col-md-5">
                                <form:select path="citizenship" id="citizenship" cssClass="chosen-select"
                                    data-validation="required">
                                    <form:option value="">Select Citizenship</form:option>
                                    <form:option value="Malaysian">Malaysian</form:option>
                                    <form:option value="Non-Malaysian">Non-Malaysian</form:option>
                                </form:select>
                            </div>
                        </div>
                       

                        <div class="form-group">
                            <label for="idType" class="col-sm-3 control-label">Identification Type:</label>
                            <div class="col-sm-6 col-md-5">
                                <form:select path="idType" id="idType" cssClass="chosen-select"
                                    data-validation="required">
                                    <form:option value="">Select Identification Type</form:option>
                                    <form:option value="Identification Card (IC)" >Identification Card (IC)</form:option>
                                    <form:option value="Passport">Passport</form:option>
                                    <form:option value="Social Security Card">Social Security Card</form:option>
                                </form:select>
                            </div>
                        </div>
                        <div class="form-group" >
                            <label for="idNumber" class="col-sm-3 control-label">IC/Passport/Social Security Card Number:</label>
                            <div class="col-sm-6 col-md-5">
                                <form:input type="text" path="idNumber" cssClass="form-control" id="idNumber"
                                    name="idNumber" placeholder="e.g 123456789999" data-validation="required length custom " data-validation-length="1-32" data-validation-regexp="^[a-zA-Z0-9]{0,32}$" />
                                <div class="sky-blue">*Malaysian to provide Identification Card (IC) Number. Format (e.g: 123456789999)
												</div>
								<div class="sky-blue">*Non-Malaysian to provide Passport or Social Security Card Number. Format (e.g: A000000000 / 123456789)</div>
                            </div>
                        </div>
                        
						<div class="form-group" style="margin-bottom: 0px;">
							<c:set var="fileType" value="" />
							<c:forEach var="type" items="${ownerSettings.fileTypes}" varStatus="index">
								<c:set var="fileType" value="${fileType}${index.first ? '': ', '}${type}" />
							</c:forEach>
							<label for="idCopy" class="col-sm-3 control-label">Copy of Director Identification Card (IC) for Malaysia, Passport/ Social Security Card (For Non-Malaysian):</label>
							<div class="col-sm-6 col-md-5">
								<div data-provides="fileinput" class="fileinput fileinput-new input-group">
									<div data-trigger="fileinput" class="form-control">
										<i class="glyphicon glyphicon-file fileinput-exists"></i>
										<span id="idICfileUploadSpan" class="fileinput-filename show_name_icAttachment"></span>
									</div>
									<span class="input-group-addon btn btn-black btn-file">
										<span class="fileinput-new">Select file</span>
										<span class="fileinput-exists">Change</span> 
										<input
											type="file" id="icAttachment" name="icAttachment"
											data-buttonName="btn-black"
											data-validation-allowing="${fileType}"
											data-validation-error-msg-container="#Load_File-error-icAttachment"
											data-validation-max-size="${ownerSettings.fileSizeLimit}M"
											data-validation="extension size required"
											data-validation-error-msg-size="You can not upload file larger than ${ownerSettings.fileSizeLimit}MB"
											data-validation-error-msg-mime="${mimetypes}">
										</span>
									<a data-dismiss="fileinput" class="input-group-addon btn btn-default fileinput-exists" href="#">Remove</a>
								</div>
								<div id="Load_File-error-icAttachment" style="width: 100%; float: left; margin: 0 0 10px 0;"></div>														
							</div>
							</div>
							<div class="form-group">
							<label class="col-sm-3 control-label"></label>
							<div class="col-sm-6">
								<span><b>Note: Please attach the copy of the Director Identification Card (IC)</b><br/>
									<ul>
										<li>Max allowed file size is ${ownerSettings.fileSizeLimit} MB</li>
										<li>Allowed file extensions: ${fileType}.</li>
									</ul>
								</span>
							</div>
							</div>												
                        
                        
                        <div class="form-group">
                            <label for="dirType" class="col-sm-3 control-label">Type of Director :</label>
                            <div class="col-sm-6 col-md-5">
                                <form:select path="dirType" id="idDirType" cssClass="chosen-select"
                                    data-validation="required">
                                    <form:option value="">Select Type of Director</form:option>
                                    <form:option value="Executive">Executive</form:option>
                                    <form:option value="Non-Executive">Non-Executive</form:option>
                                    <form:option value="Managing">Managing</form:option>
                                    <form:option value="Independent">Independent</form:option>
                                    <form:option value="Others">Others</form:option>
                                </form:select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="idNumber" class="col-sm-3 control-label">Email Address :</label>
                            <div class="col-sm-6 col-md-5">
                                <form:input type="email" path="dirEmail" cssClass="form-control" id="idDirEmail"
                                    name="dirEmail" placeholder="Director's email address" data-validation="required length email"
                                    data-validation-length="1-64" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="dirContact" class="col-sm-3 control-label">Contact Number </label>
                            <div class="col-sm-6 col-md-5">
                                <form:input type="text" path="dirContact" cssClass="form-control" id="idDirContact"
                                    name="dirContact" placeholder="Director's contact number" data-validation="required length custom"
                                    data-validation-length="1-24" data-validation-regexp="^[0-9-+]{0,24}$" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"></label>
                            <div class="col-sm-6 col-md-5">
                                <form:button type="submit" class="btn btn-primary hvr-pop hvr-rectangle-out ph_btn_midium" id="addNewDir" >Add New</form:button>
                                <form:button type="submit" class="btn btn-primary ph_btn_midium hvr-pop hvr-rectangle-out" id="saveDir" style="display: none" >Update</form:button>
								<form:button type="button" class="btn btn-black btn-default ph_btn_midium hvr-pop hvr-rectangle-out1" id="cancelSaveDir" style="display: none" ><spring:message code="application.cancel" /></form:button>                                
                            </div>
                        </div>
                    </form:form>
                    <div style="margin-left: 20px; margin-right: 20px;">
                        <section class="step4_table">
                            <div class="mega">
                                <table class="table" id="directorsDisplay">
                                    <thead>
                                        <tr>
                                            <th>Action</th>
                                            <th>No.</th>
                                            <th>Director's Name</th>
                                            <th>Identification Number </th>
                                            <th>Type of Director</th>
                                            <th>Email Address</th>
                                            <th>Contact Number</th>
                                            <th>IC/Passport/Social Security Card Copy</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${boardOfDirectors}" var="sp" varStatus="loop">
                                            <tr>
                                                <td class="word-break">
                                                    <div>
                                                        <span class="col-sm-6 p-l-0 no-padding" id="showConfirmDeletePopUp"
                                                            delete-id="${sp.id}" delete-name="${sp.directorName}">
                                                            <a>
                                                                <i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
                                                            </a>
                                                        </span>
                                                        <span class="col-sm-6 p-l-0 no-padding" id="editDirector"
                                                            edit-id="${sp.id}">
                                                            <a>
                                                                <i class="fa fa-edit fa-lg" aria-hidden="true"></i>
                                                            </a>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td class="word-break">${loop.count}</td>
                                                <td class="word-break">${sp.directorName}</td>
                                                <td class="word-break">
                                                	${sp.idNumber} 
                                                </td>
											
                                                <td>${sp.dirType}</td>
                                                <td class="word-break">${sp.dirEmail}</td>
                                                <td class="word-break">${sp.dirContact}</td>
												<c:url var="download" value="/downloadIcCopy/${sp.id}" />
												<td class="word-break width_300_fix"><a href="${download}">${sp.identificationCardFileName}</a></td>                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </section>
                    </div>
                   	<div class="row">
                   		<div class="col-sm-1"></div>
                   		<div class="col-sm-8">
												<div class="row">
													<h3 class="blue_form_sbtitle p_t20">Mandatory DocumentsA</h3>
													<div class="col-sm-12">
														<span><b>Download below documents, complete the required information and upload the updated document at the provided section </b>
                                                                                                                    <ul>
                                                                                                                        <c:forEach items="${supplierMandatoryDocument}" var="smd" varStatus="loop">

                                                                                                                            <c:url var="download" value="/downloadSMD/${smd.id}" />
                                                                                                                            <li><a href="${download}">${smd.description}</a></li>

                                                                                                                        </c:forEach>

                                                                                                                    </ul>
                                                        														</span>
													</div>
													<div class="col-sm-12">
														<span><b>Note: Mandatory to attach Latest SSM Certificate, Form 9, Form 24, Form 49, MOF Certificate/Bumiputera Status Certificate, SME Bank Vendor Declaration Form and Vendor Code of Conduct Declaration Form</b><br/>
															<ul>
																<li>Max allowed file size is ${ownerSettings.fileSizeLimit} MB</li>
																<li>Allowed file extensions: ${fileType}.</li>
															</ul>
														</span>													
													</div>
													<!-- LATEST SSM -->
													<div class="col-sm-5 formSSM">
														<div class="control-label form-group">
															<h4>Latest SSM Certificate </h4>
														</div>
														<div class="">
															<form id="mandatoryDocumentFormSSMForm">
																<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
																<div class="add_file_row">
																	<div data-provides="fileinput" class="fileinput fileinput-new input-group">
																		<div data-trigger="fileinput" class="form-control">
																			<i class="glyphicon glyphicon-file fileinput-exists"></i>
																			<span id="idMandatoryDocumentFormSSMSpan" class="fileinput-filename"></span>
																		</div>
																		<span class="input-group-addon btn btn-black btn-file">
																		<span class="fileinput-new">Select file</span>
																		<span class="fileinput-exists">Change</span>
																		<input type="file" data-buttonName="btn-black"
																			   id="mandatoryDocumentFormSSMFile" name="mandatoryDocumentFormSSMFile"
																			   data-buttonName="btn-black"
																			   data-validation-allowing="${fileType}"
																			   data-validation-error-msg-container="#file-error-formSSM"
																			   data-validation-max-size="${ownerSettings.fileSizeLimit}M"
																			   data-validation="extension size"
																			   data-validation-error-msg-size="You can not upload file larger than ${ownerSettings.fileSizeLimit} MB"
																			   data-validation-error-msg-mime="${mimetypes}">
																	</span>
																		<a data-dismiss="fileinput" class="input-group-addon btn btn-default fileinput-exists" href="#">Remove</a>
																	</div>
																</div>
																<div id="file-error-formSSM" style="width: 100%; float: left; margin: 0 0 10px 0;"></div>
																<div class="form-group other_attachemts" style="margin-left: 0; margin-right: 0;">
																	<button class="btn btn-gray btn-lg btn-block up_btn" type="button" name="mandatoryDocumentFormSSMUpload" id="mandatoryDocumentFormSSMUpload">Upload</button>
																</div>
															</form>
														</div>
													</div>
													<div class="col-sm-7">
														<table class="table" id="mandatoryDocumentFormSSMDisplay" style="margin-top: 30px; margin-bottom: 0px;">
															<thead>
															<tr>
																<th>Action</th>
																<th>File name</th>
																<th>Upload Date</th>
															</tr>
															</thead>
															<tbody>
															<c:if test="${supplierOrganizationDocuments == null}">
																<tr>
																	<td colspan="3">Document Not Uploaded</td>
																</tr>
															</c:if>
															<c:if test="${supplierOrganizationDocuments != null && supplierOrganizationDocuments.ssmFileName != null}">
																<tr>
																	<td>
																		<a class="removeMandatoryDocumentSSMFile" href="#" docType="SSM" removeDocumentId="${supplierOrganizationDocuments.id}" removeDocumentFileName="${supplierOrganizationDocuments.ssmFileName}">
																			<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
																		</a>
																	</td>
																	<td>
																		<form:form method="GET">
																			<c:url var="download" value="/downloadSupplierOrganizationDocuments/SSM/${supplierOrganizationDocuments.id}" />
																			<a class="word-break" mandatory-document-ssm-id="${supplierOrganizationDocuments.id}" href="${download}">${supplierOrganizationDocuments.ssmFileName}</a>
																		</form:form>
																	</td>
																	<td>
																			<span class="col-sm-10 no-padding">
																				<fmt:formatDate value="${supplierOrganizationDocuments.ssmUploadDate}" pattern="dd/MM/yyyy hh:mm a" timeZone="<%=request.getSession().getAttribute(\"timeZone\")%>" />
																			</span>
																	</td>
																</tr>
															</c:if>
															</tbody>
														</table>
														<hr style="margin-top: 0px;" />
													</div>
												</div>

							<!-- FORM 9 -->
							<div class="row">
								<div class="col-sm-5 form9">
									<div class="control-label form-group">
										<h4>Form 9</h4>
									</div>
									<div class="">
										<form id="mandatoryDocumentForm9Form">
											<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
											<div class="add_file_row">
												<div data-provides="fileinput" class="fileinput fileinput-new input-group">
													<div data-trigger="fileinput" class="form-control">
														<i class="glyphicon glyphicon-file fileinput-exists"></i>
														<span id="idMandatoryDocumentForm9Span" class="fileinput-filename"></span>
													</div>
													<span class="input-group-addon btn btn-black btn-file">
                                                                        <span class="fileinput-new">Select file</span>
                                                                        <span class="fileinput-exists">Change</span>
                                                                        <input type="file" data-buttonName="btn-black"
																			   id="mandatoryDocumentForm9File" name="mandatoryDocumentForm9File"
																			   data-buttonName="btn-black"
																			   data-validation-allowing="${fileType}"
																			   data-validation-error-msg-container="#file-error-form9"
																			   data-validation-max-size="${ownerSettings.fileSizeLimit}M"
																			   data-validation="extension size"
																			   data-validation-error-msg-size="You can not upload file larger than ${ownerSettings.fileSizeLimit} MB"
																			   data-validation-error-msg-mime="${mimetypes}">
                                                                    </span>
													<a data-dismiss="fileinput" class="input-group-addon btn btn-default fileinput-exists" href="#">Remove</a>
												</div>
											</div>
											<div id="file-error-form9" style="width: 100%; float: left; margin: 0 0 10px 0;"></div>
											<div class="form-group other_attachemts" style="margin-left: 0; margin-right: 0;">
												<button class="btn btn-gray btn-lg btn-block up_btn" type="button" name="mandatoryDocumentForm9Upload" id="mandatoryDocumentForm9Upload">Upload</button>
											</div>
										</form>
									</div>
								</div>
								<div class="col-sm-7">
									<table class="table" id="mandatoryDocumentForm9Display" style="margin-top: 30px; margin-bottom: 0px;">
										<thead>
										<tr>
											<th>Action</th>
											<th>File name</th>
											<th>Upload Date</th>
										</tr>
										</thead>
										<tbody>
										<c:if test="${supplierOrganizationDocuments == null}">
											<tr>
												<td colspan="3">Document Not Uploaded</td>
											</tr>
										</c:if>
										<c:if test="${supplierOrganizationDocuments != null && supplierOrganizationDocuments.form9FileName != null}">
											<tr>
												<td>
													<a class="removeMandatoryDocumentForm9File" href="#" docType="FORM9" removeDocumentId="${supplierOrganizationDocuments.id}" removeDocumentFileName="${supplierOrganizationDocuments.form9FileName}">
														<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
													</a>
												</td>
												<td>
													<form:form method="GET">
														<c:url var="download" value="/downloadSupplierOrganizationDocuments/FORM9/${supplierOrganizationDocuments.id}" />
														<a class="word-break" mandatory-document-form9-id="${supplierOrganizationDocuments.id}" href="${download}">${supplierOrganizationDocuments.form9FileName}</a>
													</form:form>
												</td>
												<td>
                                                                            <span class="col-sm-10 no-padding">
                                                                                <fmt:formatDate value="${supplierOrganizationDocuments.form9UploadDate}" pattern="dd/MM/yyyy hh:mm a" timeZone="<%=request.getSession().getAttribute(\"timeZone\")%>" />
                                                                            </span>
												</td>
											</tr>
										</c:if>
										</tbody>
									</table>
									<hr style="margin-top: 0px;" />
								</div>
							</div>


							<!-- FORM 24 -->
							<div class="row">
								<div class="col-sm-5 form24">
									<div class="control-label form-group">
										<h4>Form 24</h4>
									</div>
									<div class="">
										<form id="mandatoryDocumentForm24Form">
											<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
											<div class="add_file_row">
												<div data-provides="fileinput" class="fileinput fileinput-new input-group">
													<div data-trigger="fileinput" class="form-control">
														<i class="glyphicon glyphicon-file fileinput-exists"></i>
														<span id="idMandatoryDocumentForm24Span" class="fileinput-filename"></span>
													</div>
													<span class="input-group-addon btn btn-black btn-file">
																		<span class="fileinput-new">Select file</span>
																		<span class="fileinput-exists">Change</span>
																		<input type="file" data-buttonName="btn-black"
																			   id="mandatoryDocumentForm24File" name="mandatoryDocumentForm24File"
																			   data-buttonName="btn-black"
																			   data-validation-allowing="${fileType}"
																			   data-validation-error-msg-container="#file-error-form24"
																			   data-validation-max-size="${ownerSettings.fileSizeLimit}M"
																			   data-validation="extension size"
																			   data-validation-error-msg-size="You can not upload file larger than ${ownerSettings.fileSizeLimit} MB"
																			   data-validation-error-msg-mime="${mimetypes}">
																	</span>
													<a data-dismiss="fileinput" class="input-group-addon btn btn-default fileinput-exists" href="#">Remove</a>
												</div>
											</div>
											<div id="file-error-form24" style="width: 100%; float: left; margin: 0 0 10px 0;"></div>
											<div class="form-group other_attachemts" style="margin-left: 0; margin-right: 0;">
												<button class="btn btn-gray btn-lg btn-block up_btn" type="button" name="mandatoryDocumentForm24Upload" id="mandatoryDocumentForm24Upload">Upload</button>
											</div>
										</form>
									</div>
								</div>
								<div class="col-sm-7">
									<table class="table" id="mandatoryDocumentForm24Display" style="margin-top: 30px; margin-bottom: 0px;">
										<thead>
										<tr>
											<th>Action</th>
											<th>File name</th>
											<th>Upload Date</th>
										</tr>
										</thead>
										<tbody>
										<c:if test="${supplierOrganizationDocuments == null}">
											<tr>
												<td colspan="3">Document Not Uploaded</td>
											</tr>
										</c:if>
										<c:if test="${supplierOrganizationDocuments != null && supplierOrganizationDocuments.form24FileName != null}">
											<tr>
												<td>
													<a class="removeMandatoryDocumentForm24File" href="#" docType="FORM24" removeDocumentId="${supplierOrganizationDocuments.id}" removeDocumentFileName="${supplierOrganizationDocuments.form24FileName}">
														<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
													</a>
												</td>
												<td>
													<form:form method="GET">
														<c:url var="download" value="/downloadSupplierOrganizationDocuments/FORM24/${supplierOrganizationDocuments.id}" />
														<a class="word-break" mandatory-document-form24-id="${supplierOrganizationDocuments.id}" href="${download}">${supplierOrganizationDocuments.form24FileName}</a>
													</form:form>
												</td>
												<td>
																			<span class="col-sm-10 no-padding">
																				<fmt:formatDate value="${supplierOrganizationDocuments.form24UploadDate}" pattern="dd/MM/yyyy hh:mm a" timeZone="<%=request.getSession().getAttribute(\"timeZone\")%>" />
																			</span>
												</td>
											</tr>
										</c:if>
										</tbody>
									</table>
									<hr style="margin-top: 0px;" />
								</div>
							</div>

							<!-- FORM 49 -->
							<div class="row">
								<div class="col-sm-5 form49">
									<div class="control-label form-group">
										<h4>Form 49</h4>
										<div class="sky-blue"><em>* Please ensure that the information provided in Board of Directors Section is the same as registered in Form 49</em></div>
									</div>
									<div class="">
										<form id="mandatoryDocumentForm49Form">
											<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
											<div class="add_file_row">
												<div data-provides="fileinput" class="fileinput fileinput-new input-group">
													<div data-trigger="fileinput" class="form-control">
														<i class="glyphicon glyphicon-file fileinput-exists"></i>
														<span id="idMandatoryDocumentForm49Span" class="fileinput-filename"></span>
													</div>
													<span class="input-group-addon btn btn-black btn-file">
																		<span class="fileinput-new">Select file</span>
																		<span class="fileinput-exists">Change</span>
																		<input type="file" data-buttonName="btn-black"
																			   id="mandatoryDocumentForm49File" name="mandatoryDocumentForm49File"
																			   data-buttonName="btn-black"
																			   data-validation-allowing="${fileType}"
																			   data-validation-error-msg-container="#file-error-form49"
																			   data-validation-max-size="${ownerSettings.fileSizeLimit}M"
																			   data-validation="extension size"
																			   data-validation-error-msg-size="You can not upload file larger than ${ownerSettings.fileSizeLimit} MB"
																			   data-validation-error-msg-mime="${mimetypes}">
																	</span>
													<a data-dismiss="fileinput" class="input-group-addon btn btn-default fileinput-exists" href="#">Remove</a>
												</div>
											</div>
											<div id="file-error-form49" style="width: 100%; float: left; margin: 0 0 10px 0;"></div>
											<div class="form-group other_attachemts" style="margin-left: 0; margin-right: 0;">
												<button class="btn btn-gray btn-lg btn-block up_btn" type="button" name="mandatoryDocumentForm49Upload" id="mandatoryDocumentForm49Upload">Upload</button>
											</div>
										</form>
									</div>
								</div>
								<div class="col-sm-7">
									<table class="table" id="mandatoryDocumentForm49Display" style="margin-top: 70px; margin-bottom: 0px;">
										<thead>
										<tr>
											<th>Action</th>
											<th>File name</th>
											<th>Upload Date</th>
										</tr>
										</thead>
										<tbody>
										<c:if test="${supplierOrganizationDocuments == null}">
											<tr>
												<td colspan="3">Document Not Uploaded</td>
											</tr>
										</c:if>
										<c:if test="${supplierOrganizationDocuments != null && supplierOrganizationDocuments.form49FileName != null}">
											<tr>
												<td>
													<a class="removeMandatoryDocumentForm49File" href="#" docType="FORM49" removeDocumentId="${supplierOrganizationDocuments.id}" removeDocumentFileName="${supplierOrganizationDocuments.form49FileName}">
														<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
													</a>
												</td>
												<td>
													<form:form method="GET">
														<c:url var="download" value="/downloadSupplierOrganizationDocuments/FORM49/${supplierOrganizationDocuments.id}" />
														<a class="word-break" mandatory-document-form49-id="${supplierOrganizationDocuments.id}" href="${download}">${supplierOrganizationDocuments.form49FileName}</a>
													</form:form>
												</td>
												<td>
																			<span class="col-sm-10 no-padding">
																				<fmt:formatDate value="${supplierOrganizationDocuments.form49UploadDate}" pattern="dd/MM/yyyy hh:mm a" timeZone="<%=request.getSession().getAttribute(\"timeZone\")%>" />
																			</span>
												</td>
											</tr>
										</c:if>
										</tbody>
									</table>
									<hr style="margin-top: 0px;" />
								</div>
							</div>

							<!-- FORM MOF -->
							<div class="row">
								<div class="col-sm-5 formMof">
									<div class="control-label form-group">
										<h4>MOF Certificate / Bumiputera Status Certificate</h4>
									</div>
									<div class="">
										<form id="mandatoryDocumentFormMofForm">
											<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
											<div class="add_file_row">
												<div data-provides="fileinput" class="fileinput fileinput-new input-group">
													<div data-trigger="fileinput" class="form-control">
														<i class="glyphicon glyphicon-file fileinput-exists"></i>
														<span id="idMandatoryDocumentFormMofSpan" class="fileinput-filename"></span>
													</div>
													<span class="input-group-addon btn btn-black btn-file">
																		<span class="fileinput-new">Select file</span>
																		<span class="fileinput-exists">Change</span>
																		<input type="file" data-buttonName="btn-black"
																			   id="mandatoryDocumentFormMofFile" name="mandatoryDocumentFormMofFile"
																			   data-buttonName="btn-black"
																			   data-validation-allowing="${fileType}"
																			   data-validation-error-msg-container="#file-error-formMof"
																			   data-validation-max-size="${ownerSettings.fileSizeLimit}M"
																			   data-validation="extension size"
																			   data-validation-error-msg-size="You can not upload file larger than ${ownerSettings.fileSizeLimit} MB"
																			   data-validation-error-msg-mime="${mimetypes}">
																	</span>
													<a data-dismiss="fileinput" class="input-group-addon btn btn-default fileinput-exists" href="#">Remove</a>
												</div>
											</div>
											<div id="file-error-formMof" style="width: 100%; float: left; margin: 0 0 10px 0;"></div>
											<div class="form-group other_attachemts" style="margin-left: 0; margin-right: 0;">
												<button class="btn btn-gray btn-lg btn-block up_btn" type="button" name="mandatoryDocumentFormMofUpload" id="mandatoryDocumentFormMofUpload">Upload</button>
											</div>
										</form>
									</div>
								</div>
								<div class="col-sm-7">
									<table class="table" id="mandatoryDocumentFormMofDisplay" style="margin-top: 30px; margin-bottom: 0px;">
										<thead>
										<tr>
											<th>Action</th>
											<th>File name</th>
											<th>Upload Date</th>
										</tr>
										</thead>
										<tbody>
										<c:if test="${supplierOrganizationDocuments == null}">
											<tr>
												<td colspan="3">Document Not Uploaded</td>
											</tr>
										</c:if>
										<c:if test="${supplierOrganizationDocuments != null && supplierOrganizationDocuments.mofFileName != null}">
											<tr>
												<td>
													<a class="removeMandatoryDocumentFormMofFile" href="#" docType="FORMMOF" removeDocumentId="${supplierOrganizationDocuments.id}" removeDocumentFileName="${supplierOrganizationDocuments.mofFileName}">
														<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
													</a>
												</td>
												<td>
													<form:form method="GET">
														<c:url var="download" value="/downloadSupplierOrganizationDocuments/FORMMOF/${supplierOrganizationDocuments.id}" />
														<a class="word-break" mandatory-document-formMof-id="${supplierOrganizationDocuments.id}" href="${download}">${supplierOrganizationDocuments.mofFileName}</a>
													</form:form>
												</td>
												<td>
																			<span class="col-sm-10 no-padding">
																				<fmt:formatDate value="${supplierOrganizationDocuments.mofUploadDate}" pattern="dd/MM/yyyy hh:mm a" timeZone="<%=request.getSession().getAttribute(\"timeZone\")%>" />
																			</span>
												</td>
											</tr>
										</c:if>
										</tbody>
									</table>
									<hr style="margin-top: 0px;" />
								</div>
							</div>

							<!-- FORM SME Bank Vendor DECLARATION -->
							<div class="row">
								<div class="col-sm-5 formVDF">
									<div class="control-label form-group">
										<h4>SME Bank Vendor Declaration Form (Connected Parties) </h4>
									</div>
									<div class="">
										<form id="mandatoryDocumentFormVDFForm">
											<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
											<div class="add_file_row">
												<div data-provides="fileinput" class="fileinput fileinput-new input-group">
													<div data-trigger="fileinput" class="form-control">
														<i class="glyphicon glyphicon-file fileinput-exists"></i>
														<span id="idMandatoryDocumentFormVDFSpan" class="fileinput-filename"></span>
													</div>
													<span class="input-group-addon btn btn-black btn-file">
                                                                        <span class="fileinput-new">Select file</span>
                                                                        <span class="fileinput-exists">Change</span>
                                                                        <input type="file" data-buttonName="btn-black"
																			   id="mandatoryDocumentFormVDFFile" name="mandatoryDocumentFormVDFFile"
																			   data-buttonName="btn-black"
																			   data-validation-allowing="${fileType}"
																			   data-validation-error-msg-container="#file-error-formVDF"
																			   data-validation-max-size="${ownerSettings.fileSizeLimit}M"
																			   data-validation="extension size"
																			   data-validation-error-msg-size="You can not upload file larger than ${ownerSettings.fileSizeLimit} MB"
																			   data-validation-error-msg-mime="${mimetypes}">
                                                                    </span>
													<a data-dismiss="fileinput" class="input-group-addon btn btn-default fileinput-exists" href="#">Remove</a>
												</div>
											</div>
											<div id="file-error-formVDF" style="width: 100%; float: left; margin: 0 0 10px 0;"></div>
											<div class="form-group other_attachemts" style="margin-left: 0; margin-right: 0;">
												<button class="btn btn-gray btn-lg btn-block up_btn" type="button" name="mandatoryDocumentFormVDFUpload" id="mandatoryDocumentFormVDFUpload">Upload</button>
											</div>
										</form>
									</div>
								</div>
								<div class="col-sm-7">
									<table class="table" id="mandatoryDocumentFormVDFDisplay" style="margin-top: 30px; margin-bottom: 0px;">
										<thead>
										<tr>
											<th>Action</th>
											<th>File name</th>
											<th>Upload Date</th>
										</tr>
										</thead>
										<tbody>
										<c:if test="${supplierOrganizationDocuments == null}">
											<tr>
												<td colspan="3">Document Not Uploaded</td>
											</tr>
										</c:if>
										<c:if test="${supplierOrganizationDocuments != null && supplierOrganizationDocuments.vdfFileName != null}">
											<tr>
												<td>
													<a class="removeMandatoryDocumentFormVDFFile" href="#" docType="VDF" removeDocumentId="${supplierOrganizationDocuments.id}" removeDocumentFileName="${supplierOrganizationDocuments.vdfFileName}">
														<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
													</a>
												</td>
												<td>
													<form:form method="GET">
														<c:url var="download" value="/downloadSupplierOrganizationDocuments/VDF/${supplierOrganizationDocuments.id}" />
														<a class="word-break" mandatory-document-vdf-id="${supplierOrganizationDocuments.id}" href="${download}">${supplierOrganizationDocuments.vdfFileName}</a>
													</form:form>
												</td>
												<td>
                                                                            <span class="col-sm-10 no-padding">
                                                                                <fmt:formatDate value="${supplierOrganizationDocuments.vdfUploadDate}" pattern="dd/MM/yyyy hh:mm a" timeZone="<%=request.getSession().getAttribute(\"timeZone\")%>" />
                                                                            </span>
												</td>
											</tr>
										</c:if>
										</tbody>
									</table>
									<hr style="margin-top: 0px;" />
								</div>
							</div>

							<!-- FORM Vendor Code of Conduct Declaration Form  -->
							<div class="row">
								<div class="col-sm-5 formCCDF">
									<div class="control-label form-group">
										<h4>Vendor Code of Conduct Declaration Form  </h4>
									</div>
									<div class="">
										<form id="mandatoryDocumentFormCCDFForm">
											<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
											<div class="add_file_row">
												<div data-provides="fileinput" class="fileinput fileinput-new input-group">
													<div data-trigger="fileinput" class="form-control">
														<i class="glyphicon glyphicon-file fileinput-exists"></i>
														<span id="idMandatoryDocumentFormCCDFSpan" class="fileinput-filename"></span>
													</div>
													<span class="input-group-addon btn btn-black btn-file">
                                                                        <span class="fileinput-new">Select file</span>
                                                                        <span class="fileinput-exists">Change</span>
                                                                        <input type="file" data-buttonName="btn-black"
																			   id="mandatoryDocumentFormCCDFFile" name="mandatoryDocumentFormCCDFFile"
																			   data-buttonName="btn-black"
																			   data-validation-allowing="${fileType}"
																			   data-validation-error-msg-container="#file-error-formVENDOR"
																			   data-validation-max-size="${ownerSettings.fileSizeLimit}M"
																			   data-validation="extension size"
																			   data-validation-error-msg-size="You can not upload file larger than ${ownerSettings.fileSizeLimit} MB"
																			   data-validation-error-msg-mime="${mimetypes}">
                                                                    </span>
													<a data-dismiss="fileinput" class="input-group-addon btn btn-default fileinput-exists" href="#">Remove</a>
												</div>
											</div>
											<div id="file-error-formCCDF" style="width: 100%; float: left; margin: 0 0 10px 0;"></div>
											<div class="form-group other_attachemts" style="margin-left: 0; margin-right: 0;">
												<button class="btn btn-gray btn-lg btn-block up_btn" type="button" name="mandatoryDocumentFormCCDFUpload" id="mandatoryDocumentFormCCDFUpload">Upload</button>
											</div>
										</form>
									</div>
								</div>
								<div class="col-sm-7">
									<table class="table" id="mandatoryDocumentFormCCDFDisplay" style="margin-top: 30px; margin-bottom: 0px;">
										<thead>
										<tr>
											<th>Action</th>
											<th>File name</th>
											<th>Upload Date</th>
										</tr>
										</thead>
										<tbody>
										<c:if test="${supplierOrganizationDocuments == null}">
											<tr>
												<td colspan="3">Document Not Uploaded</td>
											</tr>
										</c:if>
										<c:if test="${supplierOrganizationDocuments != null && supplierOrganizationDocuments.ccdfFileName != null}">
											<tr>
												<td>
													<a class="removeMandatoryDocumentFormCCDFFile" href="#" docType="CCDF" removeDocumentId="${supplierOrganizationDocuments.id}" removeDocumentFileName="${supplierOrganizationDocuments.ccdfFileName}">
														<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
													</a>
												</td>
												<td>
													<form:form method="GET">
														<c:url var="download" value="/downloadSupplierOrganizationDocuments/CCDF/${supplierOrganizationDocuments.id}" />
														<a class="word-break" mandatory-document-ccdf-id="${supplierOrganizationDocuments.id}" href="${download}">${supplierOrganizationDocuments.ccdfFileName}</a>
													</form:form>
												</td>
												<td>
                                                                            <span class="col-sm-10 no-padding">
                                                                                <fmt:formatDate value="${supplierOrganizationDocuments.ccdfUploadDate}" pattern="dd/MM/yyyy hh:mm a" timeZone="<%=request.getSession().getAttribute(\"timeZone\")%>" />
                                                                            </span>
												</td>
											</tr>
										</c:if>
										</tbody>
									</table>
									<hr style="margin-top: 0px;" />
								</div>
							</div>

							<!-- FORM ODELA MERCHANT -->
							<div class="row">
								<div class="col-sm-6 formMERCHANT">
									<div class="control-label form-group" style="display: flex; justify-content: space-between; align-items: center;">
										<h4 style="margin: 0;">ODELA Merchant</h4>
										<div style="margin-top: 20px;">
											<form id="mandatoryDocumentFormMERCHANTForm">
												<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
												<div class="form-group" style="display: flex; align-items: center;">
													<label for="radioNo" style="margin-right: 10px; margin-top: 12px;">NO</label>
													<input class="radioNo" type="radio" id="radioNo" name="radioNo" value=${!supplierOrganizationDocuments.odelaMerchant} style="margin-right: 50px; margin-top: 10px;" >
													<label for="radioYes" style="margin-right: 10px; margin-top: 12px;">YES</label>
													<input class="radioYes" type="radio" id="radioYes" name="radioYes" value=${supplierOrganizationDocuments.odelaMerchant} style="margin-top: 10px;" >
												</div>
												<div id="file-error-formMerchant" style="width: 100%; float: left; margin: 0 0 10px 0;"></div>
											</form>
										</div>
									</div>
								</div>
							</div>

							<!-- FORM ODELA MFA -->
							<div class="row">
								<div class="col-sm-5 formODELA">
									<div class="control-label form-group">
										<h4>ODELA MFA </h4>
									</div>
									<div class="">
										<form id="mandatoryDocumentFormODELAForm">
											<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
											<div class="add_file_row">
												<div data-provides="fileinput" class="fileinput fileinput-new input-group">
													<div data-trigger="fileinput" class="form-control">
														<i class="glyphicon glyphicon-file fileinput-exists"></i>
														<span id="idMandatoryDocumentFormODELASpan" class="fileinput-filename"></span>
													</div>
													<span class="input-group-addon btn btn-black btn-file">
                                                                        <span class="fileinput-new">Select file</span>
                                                                        <span class="fileinput-exists">Change</span>
                                                                        <input type="file" data-buttonName="btn-black"
																			   id="mandatoryDocumentFormODELAFile" name="mandatoryDocumentFormODELAFile"
																			   data-buttonName="btn-black"
																			   data-validation-allowing="${fileType}"
																			   data-validation-error-msg-container="#file-error-formSME"
																			   data-validation-max-size="${ownerSettings.fileSizeLimit}M"
																			   data-validation="extension size"
																			   data-validation-error-msg-size="You can not upload file larger than ${ownerSettings.fileSizeLimit} MB"
																			   data-validation-error-msg-mime="${mimetypes}">
                                                                    </span>
													<a data-dismiss="fileinput" class="input-group-addon btn btn-default fileinput-exists" href="#">Remove</a>
												</div>
											</div>
											<div id="file-error-formODELA" style="width: 100%; float: left; margin: 0 0 10px 0;"></div>
											<div class="form-group other_attachemts" style="margin-left: 0; margin-right: 0;">
												<button class="btn btn-gray btn-lg btn-block up_btn" type="button" name="mandatoryDocumentFormODELAUpload" id="mandatoryDocumentFormODELAUpload">Upload</button>
											</div>
										</form>
									</div>
								</div>
								<div class="col-sm-7">
									<table class="table" id="mandatoryDocumentFormODELADisplay" style="margin-top: 30px; margin-bottom: 0px;">
										<thead>
										<tr>
											<th>Action</th>
											<th>File name</th>
											<th>Upload Date</th>
										</tr>
										</thead>
										<tbody>
										<c:if test="${supplierOrganizationDocuments == null}">
											<tr>
												<td colspan="3">Document Not Uploaded</td>
											</tr>
										</c:if>
										<c:if test="${supplierOrganizationDocuments != null && supplierOrganizationDocuments.odelaFileName != null}">
											<tr>
												<td>
													<a class="removeMandatoryDocumentFormODELAFile" href="#" docType="ODELA" removeDocumentId="${supplierOrganizationDocuments.id}" removeDocumentFileName="${supplierOrganizationDocuments.odelaFileName}">
														<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
													</a>
												</td>
												<td>
													<form:form method="GET">
														<c:url var="download" value="/downloadSupplierOrganizationDocuments/ODELA/${supplierOrganizationDocuments.id}" />
														<a class="word-break" mandatory-document-odela-id="${supplierOrganizationDocuments.id}" href="${download}">${supplierOrganizationDocuments.odelaFileName}</a>
													</form:form>
												</td>
												<td>
                                                                            <span class="col-sm-10 no-padding">
                                                                                <fmt:formatDate value="${supplierOrganizationDocuments.odelaUploadDate}" pattern="dd/MM/yyyy hh:mm a" timeZone="<%=request.getSession().getAttribute(\"timeZone\")%>" />
                                                                            </span>
												</td>
											</tr>
										</c:if>
										</tbody>
									</table>
									<hr style="margin-top: 0px;" />
								</div>
							</div>

						</div>
												

												
												
											
                   		</div>
                   		<div class="col-sm-2"></div>
                   	</div>
											
                </div>
            </div>
            <div class="modal fade hideModal" id="confirmDeleteDirector" role="dialog">
                <div class="modal-dialog for-delete-all reminder documentBlock">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3>
                                <spring:message code="application.confirm.delete" />
                            </h3>
                            <button class="close for-absulate" id="confirmDeleteDirectorDismiss" type="button"
                                data-dismiss="modal">X</button>
                        </div>
                        <div class="modal-body">
                            <label>
                                Are you sure you want to delete this record ?
                            </label>
                            <input type="hidden" />
                        </div>
                        <div class="modal-footer pad_all_15 float-left width-100 border-top-width-1">
                            <button type="button" class="btn btn-info ph_btn_small hvr-pop hvr-rectangle-out"
                                id="confDelDir">
                                <spring:message code="application.delete" />
                            </button>
                            <button type="button" id="confirmDeleteDirectorClose"
                                class="btn btn-black btn-default ph_btn_small hvr-pop hvr-rectangle-out1 pull-right"
                                data-dismiss="modal">
                                <spring:message code="application.cancel" />
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<c:url value="/resources/js/view/suppBoardOfDirectors.js?1"/>"></script>
