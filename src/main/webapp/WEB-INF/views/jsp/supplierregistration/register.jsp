<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<spring:message var="supplierPlanDesk" code="application.owner.supplier.subscription.plan" />

<!-- HELPERS -->

<link rel="stylesheet" type="text/css" href="/procurehere/resources/assets/helpers/utils.css">
<link rel="stylesheet" type="text/css" href="/procurehere/resources/assets/helpers/colors.css">

<!-- ELEMENTS -->
<link rel="stylesheet" type="text/css" href="/procurehere/resources/assets/elements/buttons.css">
<link rel="stylesheet" type="text/css" href="/procurehere/resources/assets/elements/forms.css">
<link rel="stylesheet" type="text/css" href="/procurehere/resources/assets/elements/content-box.css">
<link rel="stylesheet" type="text/css" href="/procurehere/resources/assets/elements/response-messages.css">


<!-- ICONS -->
<link href="/procurehere/resources/assets/icons/fontawesome/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/procurehere/resources/assets/icons/fontawesome/fontawesome.css">

<!-- WIDGETS -->
<link rel="stylesheet" type="text/css" href="/procurehere/resources/assets/widgets/chosen/chosen.css">


<div class="container margin-bottom-5">
	<style>
	.buyer-check-section p:nth-child(1) {
		margin: auto !important;
	}
	
	.chosen-search-input {
		width: 150px !important;
	}
	
	.chosen-container-multi .chosen-choices li.search-field .default {
	  color: #999 !important;
	}
	
	.chosen-container {
		border: 0px !important;
	}
	
	.search-choice {
		border: 1px solid #555 !important;
	}

	#loading {
    	background: rgba(255, 255, 255, 0.5);
    }

	</style>

	<div id="loading" class="opacity-60" style="display:none;">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>


	<form:form action="register1" id="myForm" method="post" modelAttribute="supplier" autocomplete="off">

		<div class="buyer-check-section" style="margin-bottom:10px !important;">
			<p class="font-27">Fill in your details to start your registration process</p>
		</div>
		<div class="container-fluid">
			<div class="col-md-offset-3 col-sm-12 col-md-12 col-lg-6 col-xs-12 buyer-purchase-details">
			    <jsp:include page="/WEB-INF/views/jsp/templates/ajaxMessage.jsp" />
				<div class="form-group">
					<label>Company Name</label>
					<form:input path="companyName" cssClass="form-control" id="idCompanyName" autocomplete="off" placeholder="Once submitted, cannot be changed"
						data-validation="required length alphanumeric custom"
						data-validation-regexp="^[A-Za-z0-9 _&.()-]+[,-_ &.()\/]*[A-Za-z0-9 _&.()-]*$"
						data-validation-error-msg="This is a required field"
						data-validation-allowing="(,-_ &.()/\\s)"
						data-validation-length="4-124"
						data-validation-error-msg-length="The input value must be between 4-124 characters"
						data-validation-error-msg-regexp="The input value can only contain alphanumeric characters and -_&.() and spaces" />
				</div>


				<div class="form-group">
                   <label><spring:message code="supplier.registration.company.number" /></label>
                   <form:input path="companyRegistrationNumber" cssClass="form-control" id="idCompRegNum" autocomplete="off"
                       placeholder="Once submitted, cannot be changed"
                       data-validation="required length alphanumeric"
					   data-validation-regexp="^[A-Za-z0-9 ,-_.&()\/]{2,124}$"
					   data-validation-allowing="(,-_ #&.()/\\s)"
                       data-validation-error-msg="This is a required field"
                       data-validation-length="2-124"
                       data-validation-error-msg-length="The input value must be between 2-124 characters"
                       data-validation-error-msg-regexp="The input value can only contain alphanumeric characters, -, _, &, ., (), and spaces" />
                </div>

				 <div class="form-group">
					<label>Country of Registration</label>
					<form:select path="registrationOfCountry" id="idRegCountry" cssClass="form-control" data-validation="required ">
						<form:option value="">Select Country of Registration</form:option>
						<form:options items="${countryList}" itemValue="id" itemLabel="countryName" />
					</form:select>
				</div> 

				<div class="form-group">
					<label>Login Email</label>
					<form:input path="loginEmail" cssClass="form-control" id="idLoginEmail" autocomplete="off" 
						placeholder="This email cannot be changed later" 
						data-validation="required length email" 
						data-validation-error-msg="This is a required field"
						data-validation-length="1-124" 
						data-validation-error-msg-length="The input value is longer than 124 characters" />
				</div>
				

				<div class="form-group">
					<label>Password</label>
					<c:set var="rgx" value="^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{8,15}$"></c:set>
					<input type="text" style="display:none;">
					<form:password path="password" cssClass="form-control pwd" autocomplete="new-password" readonly="readonly" onfocus="this.removeAttribute('readonly');" id="exampleInputPass1" placeholder="Password should contain at least one digit, one small case letter, one capital case letter, one special character, password should contain minimum 8 character" data-validation="custom" data-validation-regexp="${!empty regex ? regex.regx :rgx}" data-validation-error-msg="This is required field" />
					<p class="pass-desc" id="passwordPlaceHolder">${!empty regex.message?regex.message:''}</p>
				</div>

                <div class="form-group">
					<label>Communication Email</label>
					<form:input path="communicationEmail" cssClass="form-control" id="idCommunicationEmail" autocomplete="off" 
						placeholder="Email will be sent to this address" 
						data-validation="required length email" 
						data-validation-error-msg="This is a required field" 
						data-validation-length="2-128" 
						data-validation-regexp="^[A-Za-z0-9 _.-]+$" 
						data-validation-error-msg-length="The input value must be between 2-128 characters" 
						data-validation-error-msg-regexp="The input value can only contain alphanumeric characters and -_. and spaces" />
				</div>
				

				<div class="form-group">
					<label>Full Name</label>
					<form:input path="fullName" cssClass="form-control" autocomplete="off" id="idFullName" 
						placeholder="User Name "
						data-validation="required length alphanumeric custom"
						data-validation-error-msg="Please Enter Full Name" 
						data-validation-length="1-124" 
						data-validation-error-msg-length="The input value is longer than 124 characters" />
				</div>
				
 				<div class="form-group">
					<label>Contact Number</label>
					<form:input path="companyContactNumber" cssClass="form-control" autocomplete="off" id="idCompanyContactNumber" placeholder="+60345678906" data-validation="required length number" data-validation-ignore="+ " data-validation-length="6-14" />
				</div>
				<div class="form-group">
					<label>Select Categories</label>
					<span class="dropUp">
						<form:select autocomplete="off" data-validation="required" path="industryCategories" cssClass="chosen-select"  multiple="multiple" data-placeholder="Select Categories" id="industryCategoriesSelect">
							<c:forEach items="${icList}" var="ic" >
								<c:if test="${ic.id == '-1' }">
									<form:option value="-1" label="${ic.name}" disabled="true" />
								</c:if>
								<c:if test="${ic.id != '-1' }">
									<form:option value="${ic.id}" label="${ic.name}" />
								</c:if>
							</c:forEach>						
						</form:select>
					</span>
				
					<p class="ts-pera">
						Select the categories that are related to the services you provide
						<span> 
							<a target="_blank" href="<c:url value="/resources/category.pdf"/>">Click here to view Category</a>
						</span>
					</p>
				</div>
				 

				<div class="form-group tc-style06">
					<span>
						<input type="checkbox" name="agree" id="agree" value="agree">&nbsp;I have read and agree to SME Bank <a target="_blank" href="<c:url value="/resources/termsandcondition.pdf"/>">Terms and Conditions & Anti-Bribery policy.</a>
					</span>
				</div>

				<div id="">

					<button disabled="true" id="btnSubmit" class="btn btn-info ph_btn_midium hvr-pop hvr-rectangle-out full-width">Register as Supplier</button>
 				</div>
				
			</div>
			<div class="col-sm-1 col-md-1 col-lg-1 col-xs-0"></div>
		</div>
	</form:form>
</div>

<input type="hidden" id="token" name="${_csrf.parameterName}" value="${_csrf.token}" />
<script type="text/javascript" src="<c:url value="/resources/assets/bootstrap/js/bootstrap.js"/>"></script>
<script src="<c:url value="/resources/assets/js-core/jquery.form-validator.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.mask.min.js"/>"></script>
<script type="text/javascript">

    $(function(){

        window.onerror = function(msg, url, linenumber) {
            alert('Error message: '+msg+'\nURL: '+url+'\nLine Number: '+linenumber);
            return true;
        }

        // set logo
        $('.logo ').find('img').attr('src',getContextPath()+'/resources/images/customer_logo/smebank_logo.png');

        $("#agree").click(function() {
            if ($('#agree').is(":checked")) {
                $("#btnSubmit").attr("disabled", false);
            } else {
                $("#btnSubmit").attr("disabled", true);
            }
        });

        $(".pwd").click(function() {
            $('#passwordPlaceHolder').hide();
        });

        $('#idCompanyContactNumber').mask('+00 00000000000', {
            placeholder : "+60 322761533"
        });

        $("#myForm").submit(function(e){
                 $('div[id=idGlobalError]').hide();

                var theForm = this;
                var postData = $(this).serializeArray();
                var formURL = $(this).attr("action");


                if($(this).isValid()){
                    $('#loading').show();
                    $('#btnSubmit').attr('disabled','disabled').text('Processing ...');

                    $.ajax(
                    {
                        url         :   formURL,
                        type        :   "POST",
                        beforeSend  :   function(xhr){
                            xhr.setRequestHeader("X-AUTH-KEY",$('#token').val());
                            $('#loading').show();
                        },
                        data        :   postData,
                        statusCode: {
                            400: function(r) {
                              $('#idCompanyName').focus();
                              $('#btnSubmit').removeAttr('disabled','disabled').text('Register as Supplier');

                              $('p[id=idGlobalErrorMessage]').html("Duplicate Entry Supplier name "+$('#idCompanyName').val()+" or Registration No "+$('#idCompRegNum').val()+" already exist");
                              $('div[id=idGlobalError]').show();
                              $('#loading').hide();
                            },
                            500: function(r) {
                              $('#idCompanyName').focus();
                                $('#btnSubmit').removeAttr('disabled','disabled').text('Register as Supplier');

                                $('p[id=idGlobalErrorMessage]').html(r.message);
                                $('div[id=idGlobalError]').show();
                                $('#loading').hide();
                            }
                          },
                        success     :   function(data, textStatus, jqXHR)
                        {

                            setTimeout(function(){
                                $('#btnSubmit').attr('disabled','disabled').text('Sending email ...');
                            },3000);

                            window.location.href=getContextPath()+"/buyerSupplierCreationThankyou";

                        }
                    });
                }

                return false;

                e.preventDefault();
        });

        $.validate({
            lang : 'en',
            modules : 'date, security'
        });
    });
 </script>
