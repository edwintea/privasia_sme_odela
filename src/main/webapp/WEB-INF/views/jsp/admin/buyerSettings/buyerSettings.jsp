<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<sec:authorize access="hasRole('ROLE_ADMIN_READONLY')" var="buyerReadOnlyAdmin" />
<sec:authentication property="principal.isBuyerErpEnable" var="isBuyerErpEnable" />

<style type="text/css">
.chosen-select {
	display: block !important;
	position: absolute;
	opacity: 0;
}

.chosen-select.error+.chosen-container {
	border: 1px solid #b94a48;
}
</style>
<div id="page-content">
	<div class="col-md-offset-1 col-md-10">
		<!-- pageging  block -->
		<ol class="breadcrumb">
			<li><a id="dashboardLink" href='<c:url value="/buyer/buyerDashboard"/>'> <spring:message code="application.dashboard" />
			</a></li>
			<%-- <c:url value="/buyer/listBuyerSettings" var="listUrl"  />
					<li><a href="${listUrl }"><spring:message code="buyersettings.list"/></a></li> --%>
			<li class="active">
				<%-- <c:out  value='${btnValue}'/>  --%> <spring:message code="label.buyersettings" />
			</li>
		</ol>
		<div class="Section-title title_border gray-bg">
			<h2 class="trans-cap manage_icon">
				<spring:message code="buyersettings.administration" />
			</h2>
		</div>
		<div class="Invited-Supplier-List import-supplier white-bg">
			<div class="meeting2-heading">
				<h3>
					<spring:message code="label.buyersettings" />
				</h3>
			</div>
			<div class="import-supplier-inner-first-new pad_all_15 global-list">
				<form:form id="buyerSettingsForm" data-parsley-validate="" cssClass="form-horizontal bordered-row" commandName="buyerSettings" method="post" action="buyerSettings">
					<header class="form_header"> </header>
					<jsp:include page="/WEB-INF/views/jsp/templates/message.jsp" />
					<form:hidden path="id" />
					<div class="row marg-bottom-20">
						<div class="col-md-3">
							<form:label path="timeZone" for="idTimeZone" class="marg-top-10">
								<spring:message code="label.timezone" />
							</form:label>
						</div>
						<div class="col-md-5">
							<spring:message code="timezone.required" var="required" />
							<form:select path="timeZone" id="idTimeZone" cssClass="chosen-select" data-validation="required" data-validation-error-msg-required="${required}">
								<form:option value="">
									<spring:message code="buyersettings.selecttimezone" />
								</form:option>
								<form:options items="${timeZone}" itemValue="id" itemLabel="fullTimeZone"></form:options>
							</form:select>
							<form:errors path="timeZone" cssClass="error" />
						</div>
					</div>
					<div class="row marg-bottom-20">
						<div class="col-md-3">
							<form:label path="currency" cssClass="marg-top-10">
								<spring:message code="label.currency" />
							</form:label>
						</div>
						<div class="col-md-5">
							<spring:message code="buyersetting.currency.required" var="required" />
							<form:select path="currency" data-validation="required" data-validation-error-msg-required="${required}" cssClass="form-control chosen-select" id="idCurrency">
								<form:option value="">
									<spring:message code="currency.select" />
								</form:option>
								<form:options items="${currency}" itemValue="id" itemLabel="currencyName" />
							</form:select>
						</div>
					</div>
					<div class="row marg-bottom-20">
						<div class="col-md-3">
							<form:label path="decimal" cssClass="marg-top-10">
								<spring:message code="buyersettings.decimal" />
							</form:label>
						</div>
						<div class="col-md-5">
							<spring:message code="buyersetting.decimal.required" var="required" />
							<form:select path="decimal" data-validation="required" data-validation-error-msg-required="${required}" cssClass="form-control chosen-select" id="iddecimal">
								<form:option value="">
									<spring:message code="buyersettings.selectdacimal" />
								</form:option>
								<form:option value="1"></form:option>
								<form:option value="2"></form:option>
								<form:option value="3"></form:option>
								<form:option value="4"></form:option>
							</form:select>
						</div>
					</div>


					<div class="row marg-bottom-20">
						<div class="col-md-3">
							<form:label path="decimal" cssClass="marg-top-10">
								<spring:message code="buyer.setting.erp.notification" />
							</form:label>
						</div>
						<div class="col-md-5  ${isBuyerErpEnable ? '' : 'disabled'} ">

							<form:input type="text" path="erpNotificationEmails" maxlength="500" class="form-control " data-role="tagsinput" />
						</div>
					</div>
					<div class="row marg-bottom-20">
						<div class="col-md-3">
							<form:label path="decimal" cssClass="marg-top-10">
								<spring:message code="buyer.setting.autocreatepo.label" />
							</form:label>
						</div>
						<div class="col-md-5" style="margin-top:12px;">
							<form:checkbox id="idAutoCreatePo"  path="autoCreatePo"  class="custom-checkbox"  />
						</div>
					</div>
					<div class="row marg-bottom-20">
						<div class="col-md-3 ">
							<form:label path="decimal" cssClass="marg-top-10">
								<spring:message code="buyer.setting.autopublishpo.label" />
							</form:label>
						</div>
						<div class="col-md-5" style="margin-top:12px;">
							<form:checkbox id="idAutoPublishPo"   path="autoPublishPo"  class="custom-checkbox marg-top-10"  />
						</div>
					</div>
					<c:if test="${not empty enableEventPublishing and enableEventPublishing}">
						<div class="row marg-bottom-20">
							<div class="col-md-3">
								<form:label path="decimal" cssClass="marg-top-10">
								RFI Publish URL
							</form:label>
							</div>
							<div class="col-md-5">
								<form:input type="text" path="rfiPublishUrl" maxlength="1000" class="form-control " />
							</div>
						</div>
						<div class="row marg-bottom-20">
							<div class="col-md-3">
								<form:label path="decimal" cssClass="marg-top-10">
								RFQ Publish URL
							</form:label>
							</div>
							<div class="col-md-5">
								<form:input type="text" path="rfqPublishUrl" maxlength="1000" class="form-control " />
							</div>
						</div>
						<div class="row marg-bottom-20">
							<div class="col-md-3">
								<form:label path="decimal" cssClass="marg-top-10">
								RFT Publish URL
							</form:label>
							</div>
							<div class="col-md-5">
								<form:input type="text" path="rftPublishUrl" maxlength="1000" class="form-control " />
							</div>
						</div>
						<div class="row marg-bottom-20">
							<div class="col-md-3">
								<form:label path="decimal" cssClass="marg-top-10">
								RFI Update Publish URL
							</form:label>
							</div>
							<div class="col-md-5">
								<form:input type="text" path="rfiUpdatePublishUrl" maxlength="1000" class="form-control " />
							</div>
						</div>
						<div class="row marg-bottom-20">
							<div class="col-md-3">
								<form:label path="decimal" cssClass="marg-top-10">
								RFQ Update Publish URL
							</form:label>
							</div>
							<div class="col-md-5">
								<form:input type="text" path="rfqUpdatePublishUrl" maxlength="1000" class="form-control " />
							</div>
						</div>
						<div class="row marg-bottom-20">
							<div class="col-md-3">
								<form:label path="decimal" cssClass="marg-top-10">
								RFT Update Publish URL
							</form:label>
							</div>
							<div class="col-md-5">
								<form:input type="text" path="rftUpdatePublishUrl" maxlength="1000" class="form-control " />
							</div>
						</div>
					</c:if>
					<div class="row marg-bottom-20">
						<div class="col-md-3">
							<form:label path="decimal" cssClass="marg-top-10">
								<spring:message code="buyer.profile.secret.key" />
							</form:label>
						</div>
						<div class="col-md-5">

							<form:input type="password" path="stripeSecretKey" maxlength="500" class="form-control" autocomplete="off"/>
						</div>
					</div>
					<div class="row marg-bottom-20">
						<div class="col-md-3">
							<form:label path="decimal" cssClass="marg-top-10">
								<spring:message code="buyer.profile.publish.key" />
							</form:label>
						</div>
						<div class="col-md-5">
							<form:input type="text" path="stripePublishKey" maxlength="500" class="form-control" autocomplete="off"/>
						</div>
					</div>
					<div class="row marg-bottom-20">
						<div class="col-md-3">
							<form:label path="decimal" cssClass="marg-top-10">
								<spring:message code="buyer.profile.techone.inbound" />
							</form:label>
						</div>
						<div class="col-md-5">

							<form:input type="text" path="prInboundPath" maxlength="256" class="form-control" autocomplete="off"/>
						</div>
					</div>
					<div class="row marg-bottom-20">
						<div class="col-md-3">
							<form:label path="decimal" cssClass="marg-top-10">
								<spring:message code="buyer.profile.techone.outbound" />
							</form:label>
						</div>
						<div class="col-md-5">
							<form:input type="text" path="prOutBoundPath" maxlength="256" class="form-control" autocomplete="off"/>
						</div>
					</div>					
					<div class="row marg-bottom-202">
						<div class="col-md-3">
							<label class="marg-top-10"></label>
						</div>
						<div class="col-md-9 dd sky mar_b_15">
							<spring:message code="application.update" var="updateLabel" />
							<spring:message code="application.save" var="saveLabel" />
							<form:button type="submit" id="saveBuyerSettings" class="btn btn-info ph_btn_midium hvr-pop hvr-rectangle-out ">${not empty buyerSettings.id ? updateLabel : saveLabel }</form:button>
							<c:url value="/buyer/buyerDashboard" var="buyerDashboard" />
							<a href="${buyerDashboard}" class="btn btn-black hvr-pop hvr-rectangle-out1 ph_btn_midium button-previous"> <spring:message code="application.cancel" />
							</a>
						</div>
					</div>
				</form:form>
			</div>
		</div>


		<sec:authorize access="(hasRole('BUYER') and hasRole('ADMIN'))">
			<c:if test="${ buyerSettings.id != null }">
				<div class="Invited-Supplier-List import-supplier white-bg" style="margin-top: 10px;">
					<div class="meeting2-heading">

						<h3>
							<spring:message code="buyer.backup.close.account" />
						</h3>


					</div>
					<div class="import-supplier-inner-first-new pad_all_15 global-list">

						<c:if test="${! buyerSettings.isClose and !buyerSettings.isBackup}">
							<div class="row marg-bottom-202">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-12">
											<button id="exportAccount" type="button" class="btn btn-info ph_btn_midium  " style="margin: 10px;">
												<spring:message code="suppliersetting.export.data" />
											</button>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<label class="marg-top-10"> <b><spring:message code="application.note" />:</b> <spring:message code="buyer.setting.note1" />
											</label>
										</div>
									</div>
								</div>
								<div class="col-md-9 dd sky mar_b_15"></div>
							</div>
						</c:if>

						<c:if test="${buyerSettings.isBackup and buyerSettings.exportURL == null and !buyerSettings.isClose  }">
							<div class="row marg-bottom-202">
								<div class="col-md-12">
									<label class="marg-top-10"> <spring:message code="buyer.setting.note2" />
									</label>
								</div>
							</div>
						</c:if>


						<c:if test="${! buyerSettings.isClose}">
							<div class="row marg-bottom-202">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-12">
											<button id="closeAccount" type="button" class="btn btn-danger ph_btn_midium  " style="margin: 10px;">
												<spring:message code="suppliersetting.export.data.close" />
											</button>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<label class="marg-top-10"> <b><spring:message code="application.note" />:</b> <spring:message code="buyer.setting.note1" />
											</label>
										</div>
									</div>
								</div>
								<div class="col-md-9 dd sky mar_b_15"></div>
							</div>
						</c:if>

						<c:if test="${ buyerSettings.isClose}">


							<div class="row marg-bottom-202">
								<div class="col-md-12">
									<label class="marg-top-10"> <fmt:formatDate var="closeRequestDate" value="${ buyerSettings.closeRequestDate}" pattern="yyyy-MM-dd HH:mm:ss" timeZone="<%=request.getSession().getAttribute(\"timeZone\")%>" /> </b> <spring:message code="suppliersetting.user" /> <b>${ buyerSettings.requestedBy.name}</b> </b> <spring:message code="suppliersetting.has.requested.close" /> ${ closeRequestDate}

									</label>
								</div>
							</div>


							<div class="row marg-bottom-202">
								<div class="col-md-3">
									<label class="marg-top-10"><spring:message code="buyer.setting.to.cancel" /> </label>
								</div>
								<div class="col-md-9 dd sky mar_b_15">
									<button id="cancalRequest" type="button" class="btn btn-info ph_btn_midium hvr-pop hvr-rectangle-out ">
										<spring:message code="rfs.summary.cancel.request" />
									</button>
								</div>
							</div>

						</c:if>


						<c:if test="${ buyerSettings.exportURL != null}">
							<div class="row marg-bottom-202">
								<div class="col-md-3">
									<label class="marg-top-10"> <spring:message code="buyer.setting.link.download" />
									</label>
								</div>
								<div class="col-md-7 dd sky mar_b_15">
									<div class="input-group">
										<input type="text" id="link" class="form-control disabled" readonly="true" value="${ buyerSettings.exportURL}">
										<div class="input-group-btn">
											<button id="copyKey" class="btn btn-default" type="button" data-placement="bottom" title="Copy">
												<i class="glyphicon  glyphicon-copy"></i>
											</button>
										</div>
										<div class="col-sm-1 col-md-1 col-xs-3 col-xs-3">
											<a href="${ buyerSettings.exportURL}" class="btn btn-success" id="generateKey"><span class="glyphicon glyphicon-download-alt"></span></a>
										</div>
									</div>

								</div>


							</div>
						</c:if>



					</div>
				</div>
			</c:if>
		</sec:authorize>
	</div>
</div>


<div class="modal fade" id="colseAccountPopup" role="dialog">
	<div class="modal-dialog for-delete-all reminder">
		<!-- Modal content-->
		<form:form method="post" action="${pageContext.request.contextPath}/buyer/closeAccount">
			<div class="modal-content" style="width: 100%; float: left;">
				<div class="modal-header">
					<label style="font-size: 16px;">
						<spring:message code="buyer.setting.confirm.close.account" />
					</label>
					<button class="close for-absulate" type="button" data-dismiss="modal">�</button>
				</div>
				<div class="modal-body">
					<label style="font-size: 14px;"><spring:message code="buyer.setting.sure.close" /></label>
				</div>
				<div class="modal-footer pad_all_15 float-left width-100 border-top-width-1 ">
					<button type="submit" class="btn btn-info ph_btn_small hvr-pop hvr-rectangle-out" style="margin-left: 1%;">
						<spring:message code="buyer.setting.close.account" />
					</button>
					<button type="button" class="btn btn-black ph_btn_small " style="margin-left: 48%;" data-dismiss="modal">
						<spring:message code="application.cancel" />
					</button>

				</div>
			</div>
		</</form:form>
	</div>
</div>


<div class="modal fade" id="exportAccountPopup" role="dialog">
	<div class="modal-dialog for-delete-all reminder">
		<!-- Modal content-->
		<form:form method="post" action="${pageContext.request.contextPath}/buyer/exportAccount">
			<div class="modal-content" style="width: 100%; float: left;">
				<div class="modal-header">
					<label style="font-size: 16px;"><spring:message code="buyer.setting.confirm.export.account" /></label>
					<button class="close for-absulate" type="button" data-dismiss="modal">�</button>
				</div>
				<div class="modal-body">
					<label style="font-size: 14px;"> <spring:message code="buyer.setting.sure.to.export" />
					</label>
				</div>
				<div class="modal-footer pad_all_15 float-left width-100 border-top-width-1 ">
					<button type="submit" class="btn btn-info ph_btn_small hvr-pop hvr-rectangle-out" style="margin-right: 57%; margin-left: 1%;">
						<spring:message code="buyer.setting.export" />
					</button>
					<button type="button" class="btn btn-black ph_btn_small" data-dismiss="modal">
						<spring:message code="application.cancel" />
					</button>

				</div>
			</div>
		</</form:form>
	</div>
</div>



<div class="modal fade" id="cancelRequstPopup" role="dialog">
<div class="modal-dialog for-delete-all reminder">
		<!-- Modal content-->
		<form:form method="post" action="${pageContext.request.contextPath}/buyer/closeAccount">
			<div class="modal-content" style="width: 110%; float: left;">
				<div class="modal-header">
					<label style="font-size: 16px;"><spring:message code="buyer.setting.confirm.cancel.request" /></label>
					<button class="close for-absulate" type="button" data-dismiss="modal">�</button>
				</div>
				<div class="modal-body">
					<label style="font-size: 14px;"> <spring:message code="buyer.setting.sure.cancel.request" />
					</label>
				</div>

					<div class="modal-footer pad_all_15 float-left width-100 border-top-width-1 ">
					<button type="submit" class="btn btn-info ph_btn_small hvr-pop hvr-rectangle-out " style="margin-right: 49%; margin-left: 1%;">
						<spring:message code="rfs.summary.cancel.request" />
					</button>
					<button type="button" class="btn btn-black ph_btn_small " data-dismiss="modal">
						<spring:message code="application.button.closed" />
					</button>

				</div>
			</div>
		</</form:form>
	</div>
</div>


<script type="text/javascript" src="<c:url value="/resources/js/jquery.form-validator.js"/>"></script>
<script>
<c:if test="${buyerReadOnlyAdmin}">
$(window).bind('load', function() {
	var allowedFields = '.button-previous, #dashboardLink';
	//var disableAnchers = ['#reloadMsg'];        
	disableFormFields(allowedFields);
	$('#page-content').find('select').not(allowedFields).parent('div').addClass('disabled');
});
</c:if>
	$.validate({
		lang : 'en'
	});
</script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.mask.min.js"/>"></script>
<script>
	$(document).ready(function() {
		$("#erpNotificationEmails").tagsinput('items');
		$('#idTimeZone').chosen({ search_contains: true });
		$('#idtimeZone').mask('GMT+00:00', {
			placeholder : "<spring:message code="timezone.placeholder"/>"
		});
		
		$('#erpNotificationEmails').keypress(function(e) {
			if (e.which == 13) {

				e.preventDefault();
				
				return false; //<---- Add this line
			}
		});
		$('#buyerSettingsForm .bootstrap-tagsinput').on('keyup keypress', function(e) {


			
			console.log(e.keyCode + " " + e.which);
			var keyCode = e.keyCode || e.which;
			if (keyCode === 13 ) {
				e.preventDefault();
				return false;
			}

		});

		
		
		
	});
	
	
	$(document).delegate('#closeAccount', 'click', function(e) {
		
		$('#colseAccountPopup').modal();
	});
	
	
$(document).delegate('#exportAccount', 'click', function(e) {
		
		$('#exportAccountPopup').modal();
	});
	
	
	$(document).delegate('#cancalRequest', 'click', function(e) {
		$('#cancelRequstPopup').modal();
	});
	

	$( '#copyKey' ).click( function()
			 {
			     var clipboardText = "";

			     clipboardText = $( '#link' ).val();

			     copyToClipboard( clipboardText );
			  
			 });
	
	
	
	function copyToClipboard(text) {

		   var textArea = document.createElement( "textarea" );
		   textArea.value = text;
		   document.body.appendChild( textArea );

		   textArea.select();

		   try {
		      var successful = document.execCommand( 'copy' );
		      var msg = successful ? 'successful' : 'unsuccessful';
		      console.log('Copying text command was ' + msg);
		   } catch (err) {
		      console.log('Oops, unable to copy');
		   }

		   document.body.removeChild( textArea );
		}

	
</script>
<script type="text/javascript" src="<c:url value="/resources/js/buyer-settings-bootstrap-tagsinput.js"/>"></script>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/bootstrap-tagsinput.css"/>">
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form-validator.min.js"/>"></script>