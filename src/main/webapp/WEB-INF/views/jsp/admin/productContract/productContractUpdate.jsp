<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<sec:authorize access="hasRole('ROLE_PRODUCT_EDIT') or hasRole('ADMIN')" var="canEdit" />
<sec:authorize access="hasRole('ROLE_ADMIN_READONLY')" var="buyerReadOnlyAdmin" />
<spring:message var="productListDesk" code="application.buyer.product.list" />
<sec:authentication property="principal.languageCode" var="languageCode" />

<style type="text/css">
.chosen-select {
	display: block !important;
	position: absolute;
	opacity: 0;
}

.chosen-select.error+.chosen-container {
	border: 1px solid #b94a48;
}

.col-custom-md-7 {
	width: 54.333333%;
}

.col-custom-md-4 {
	width: 38.333333%;
}

.col-custom-md-1 {
	width: 3.333333%;
	margin-top: 10px;
}
</style>
<script src="<c:url value="/resources/assets/widgets/tree-multiselect/jquery.tree-multiselect.js"/>"></script>
<link rel="stylesheet" href="<c:url value="/resources/assets/widgets/tree-multiselect/jquery.tree-multiselect.min.css"/>">
<!--Side Bar End  -->
<div id="page-content" view-name="product">
	<div class="col-md-offset-1 col-md-10">
		<!-- pageging  block -->
		<ol class="breadcrumb">
			<li><a id="dashboardLink" href="${pageContext.request.contextPath}/buyer/buyerDashboard"> <spring:message code="application.dashboard" />
			</a></li>
			<c:url value="/buyer/productContractList" var="listUrl" />
			<li><a id="listLink" href="${listUrl} "> <spring:message code="Product.contract.list" /> </a></li>
			<li class="active"><spring:message code="Productz.list.contract" /></li>
		</ol>
		<div class="Section-title title_border gray-bg">
			<h2 class="trans-cap manage_icon">
				<spring:message code="Productz.list.contract" />
			</h2>
		</div>
		<div class="Invited-Supplier-List import-supplier white-bg">
			<div class="meeting2-heading">
				<h3><spring:message code="systemsetting.productlist.detail" /></h3>
			</div>
			<div class="import-supplier-inner-first-new pad_all_15 global-list">
				<jsp:include page="/WEB-INF/views/jsp/templates/message.jsp" />
				<!-- 	------ form start  here--------- -->
				<form:form id="productListMaintenanceForm" data-parsley-validate="" commandName="productListMaintenance" action="saveProductContract?${_csrf.parameterName}=${_csrf.token}" method="post" cssClass="form-horizontal bordered-row" enctype="multipart/form-data">
					<form:hidden path="id" id="idcontract"  />
		
					<div class="row marg-bottom-20">
						<div class="col-md-3">
							<form:label path="contractReferenceNumber" cssClass="marg-top-10">
								<spring:message code="product.Contract.code" />
							</form:label>
						</div>
						<div class="col-md-5 disableCq">
							<spring:message code="productz.description" var="desc" />
							<spring:message code="productz.code.required" var="required" />
							<spring:message code="productz.code.length" var="length" />
							<spring:message code="systemsetting.product.code.placeholder" var="productcode" />
							<form:input path="contractReferenceNumber"  data-validation-length="1-15" data-validation="required length" data-validation-error-msg-required="${required}" data-validation-error-msg-length="${length}" cssClass="form-control " id="idProductCode" placeholder="${contractReferenceNumber}" />
							<form:errors path="contractReferenceNumber" cssClass="error" />
						</div>
					</div>
					<div class="row marg-bottom-20">
						<div class="col-md-3">
							<form:label path="groupCode" cssClass="marg-top-10">
								<spring:message code="product.Contract.groupCode" />
							</form:label>
						</div>
						<div class="col-md-5 disableCq">
							<spring:message code="productz.name.placeholder" var="name" />
							<spring:message code="productz.name.required" var="required" />
							<spring:message code="productz.name.length" var="length" />
							<form:input path="groupCode" data-validation="required length" data-validation-length="1-64" cssClass="form-control" id="idProductName" placeholder="${name}" data-validation-error-msg-required="${required}" />
							<form:errors path="groupCode" cssClass="error" />
						</div>
					</div>
					<div class="row marg-bottom-20 disableCq">
						<div class="col-md-3">
							<form:label path="businessUnit" for="businessUnit" class="marg-top-10">
								<spring:message code="label.businessUnit" />
							</form:label>
						</div>
						<div class="col-md-5 disableCq">
							<spring:message code="application.uom.required" var="required" />
							<form:select path="businessUnit" id="chosenBusinessUnit" cssClass="chosen-select" data-validation="required" data-validation-error-msg-required="${required}">
								<form:option value="">
									<spring:message code="pr.select.business.unit" />
								</form:option>
								<c:forEach items="${businessUnit}" var="unitData">
									<form:option value="${unitData.id}">${unitData.displayName}</form:option>
								</c:forEach>
							</form:select>
						</div>
					</div>
			
					<div class="row marg-bottom-20 disableCq">
						<div class="col-md-3">
							<label for="idpc" class="marg-top-10"> <spring:message code="Productz.favoriteSupplier" />
							</label>
						</div>
						<div class="col-md-5">
							<spring:message code="application.productCategory.required" var="required" />
							<form:select path="supplier" id="chosenSupplier" class="chosen-select">
								<form:option value="">
									<spring:message code="Product.favoriteSupplier" />
								</form:option>
								<c:forEach items="${favSupp}" var="supp">
									<form:option value="${supp.id}">${supp.companyName}</form:option>
								</c:forEach>
							</form:select>
						</div>
					</div>
					<div class="row marg-bottom-20 disableCq">
						<div class="col-md-3">
							<form:label path="contractValue" cssClass="marg-top-10">
								<spring:message code="product.Contract.value" />
							</form:label>
						</div>
						<div class="col-md-5 disableCq">
							<spring:message code="systemsetting.enter.unit.placeholder" var="unitpriceplace" />
							<fmt:formatNumber var="uPrice" type="number" minFractionDigits="${decimal}" maxFractionDigits="${decimal}" value="${productListMaintenance.contractValue}" />
							
							<form:input path="contractValue" value="${uPrice}" cssClass="form-control" id="contractValue" data-validation="contract_item validate_custom_length" placeholder="${unitpriceplace}"  data-validation-regexp="^[\d,]{1,16}(\.\d{1,${decimal}})?$"/>
							<form:errors path="contractValue" cssClass="error" />
						</div>
					</div>
				
				

					<div class="row marg-bottom-20">
						<div class="col-md-3">
							<form:label path="contractStartDate" cssClass="marg-top-10">
								<spring:message code="productlist.startDate.item" />
							</form:label>
						</div>
				
						<div class="col-md-4 col-custom-md-4 disableCq">
							<div class="input-prepend input-group">
								<spring:message code="dateformat.placeholder" var="dateformat" />
								
								<fmt:formatDate var="validityDate" value="${productListMaintenance.contractStartDate}" pattern="yyyy-mm-dd" timeZone="<%=request.getSession().getAttribute(\"timeZone\")%>" />
								<form:input id="datepicker" value="${contractStartDate}" path="contractStartDate" class="bootstrap-datepicker form-control for-clander-view"  data-validation-optional="true" data-fv-date-min="15/10/2016" placeholder="${dateformat}"
									autocomplete="off" />
								<div id="contract_dateErrorBlock"></div>
								<form:errors path="contractStartDate" cssClass="error" />
							</div>

						</div>
					</div>
					
					
						<div class="row marg-bottom-20">
						<div class="col-md-3">
							<form:label path="contractEndDate" cssClass="marg-top-10">
								<spring:message code="productlist.endDate.item" />
							</form:label>
						</div>
						<div class="col-md-4 col-custom-md-4 disableCq">
							<div class="input-prepend input-group">
								<spring:message code="dateformat.placeholder" var="dateformat" />
								<fmt:formatDate var="validityDate" value="${productListMaintenance.contractStartDate}" pattern="yyyy-mm-dd" timeZone="<%=request.getSession().getAttribute(\"timeZone\")%>" />
								<form:input id="datepicker" value="${contractStartDate}" path="contractEndDate" class="bootstrap-datepicker form-control for-clander-view"  data-validation-optional="true" data-fv-date-min="15/10/2016" placeholder="${dateformat}"
									autocomplete="off" />
								<div id="contract_dateErrorBlock"></div>
								<form:errors path="contractEndDate" cssClass="error" />
							</div>

						</div>
					</div>
					
						
					<div class="row marg-bottom-20">
						<div class="col-md-3">
							<label for="idStatus" class="marg-top-10"><spring:message code="Productz.status" /> </label>
						</div>
						<div class="col-md-5">
							<form:select path="status" cssClass="form-control chosen-select disablesearch" id="idStatus">
								<form:options items="${statusList}" />
							</form:select>
							<form:errors path="status" cssClass="error" />
						</div>
					</div>
				
					
			
					

					<div class="row marg-bottom-20">
						<div class="col-md-3">
							<label class="marg-top-10"></label>
						</div>
						<div class="col-md-9 dd sky mar_b_15">
							<input type="submit" value="${btnValue2}" id="saveProductListMaintenance" class="btn btn-info ph_btn_midium hvr-pop hvr-rectangle-out" ${!canEdit ? "disabled='disabled'" : ""}>
							<c:url value="/buyer/productContractList" var="createUrl" />
							<a href="${createUrl}" class="btn btn-black hvr-pop hvr-rectangle-out1 ph_btn_midium button-previous"> <spring:message code="application.cancel" />
							</a>
						</div>
					</div>
				</form:form>
								
					
			
			</div>
			
		</div>
		<div id="page-content" view-name="product">
	
	</div>
	<div class="container col-md-12">
		
		<div class="Section-title title_border gray-bg">
			<h2 class="trans-cap manage_icon">
				<spring:message code="Productz.list.contract" />
			</h2>
		</div>
		<div class="container-fluid col-md-12">
			<jsp:include page="/WEB-INF/views/jsp/templates/ajaxMessage.jsp" />
			<div class="row">
				<div class="col_12">
					<div class="white_box_brd pad_all_15">
						<section class="index_table_block">
							<jsp:include page="/WEB-INF/views/jsp/templates/message.jsp" />
							<div class="row">
								<div class="col-xs-12">
								
									<div class="ph_tabel_wrapper scrolableTable_UserList">
										<table id="tableList" class=" display table table-bordered noarrow" cellspacing="0" width="100%">
											<thead>
												<tr class="tableHeaderWithSearch">
													<th class="w-100" search-type="" class="align-left">#</th>
													<th search-type="text" class="align-left"><spring:message code="product.Contract.itemNum" /></th>
													<th search-type="text" class="align-left"><spring:message code="product.itemName" /></th>
													<th search-type="text" class="align-left"><spring:message code="product.itemCode" /></th>
													<th search-type="text" class="align-left"><spring:message code="product.item.Quantity" /></th>
													<th search-type="text" class="align-left"><spring:message code="product.item.BQuantity" /></th>
													<th search-type="text" class="align-left"><spring:message code="productlist.unitPrice.item" /></th>
													<th search-type="text" class="align-left"><spring:message code="storage.Location" /></th>
													<th search-type="text" class="align-left"><spring:message code="storage.uom" /></th>
													<th search-type="text" class="align-left"><spring:message code="label.businessUnit" /></th>
													<th search-type="text" class="align-left"><spring:message code="label.costcenter" /></th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									<%-- 	<form method="get" action="productListMaintenance">
											<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
											<button type="submit" class="btn btn-plus btn-info top-marginAdminList ${canEdit and !buyerReadOnlyAdmin ? '' : 'disabled' }">
												<spring:message code="Productz.list.create" />
											</button>
										</form> --%>
									</div>
									<div id="morris-bar-yearly" class="graph" style="visibility: hidden"></div>
								</div>

							</div>

						</section>
					</div>
				</div>
			</div>
			<div class="row" style="height: 10px;"></div>

		</div>
	</div>	
</div>
</div>

<style>
.disableCq{
  pointer-events: none !important;
}

</style>
<script type="text/javascript" src="<c:url value="/resources/assets/js-core/jquery.form-validator.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/assets/widgets/file-input/file-input.js"/>"></script>
<script type="text/javascript">
var table = '';
var idx = 0;
	$('document').ready(function() {
		var contractId = $("#idcontract").val()

		var header = $("meta[name='_csrf_header']").attr("content");
		var token = $("meta[name='_csrf']").attr("content");

		table = $('#tableList').DataTable({
			"oLanguage":{
				"sUrl":  getContextPath() + "/resources/assets/widgets/datatable/datatable.${languageCode}.json"
			},
			"processing" : true,
			"deferRender" : true,
			"preDrawCallback" : function(settings) {
				setTimeout(function() { 
					$('div[id=idGlobalError]').hide();
				}, 3000);
				$('#loading').show();
				return true;
			},
			"drawCallback" : function() {
				// in case your overlay needs to be put away automatically you can put it here
				$('#loading').hide();
			},
			"serverSide" : true,
			"pageLength" : 10,
			"searching" : true,
			"ajax" : {
				"url" : getContextPath() + "/buyer/productContractListItem",
				"data" : function(d) {
					d.id = contractId
					//var table = $('#tableList').DataTable()
					//d.page = (table != undefined) ? table.page.info().page : 0;
					//d.size = (table != undefined) ? table.page.info().length : 10;
					//d.sort = d.columns[d.order[0].column].data + ',' + d.order[0].dir;
				},
				beforeSend : function(xhr) {
					$('#loading').show();
				},
				"error" : function(request, textStatus, errorThrown) {
					var error = request.getResponseHeader('error');
					if (error != undefined) {
						$('p[id=idGlobalErrorMessage]').html(error != null ? error.split(",").join("<br/>") : "");
						$('div[id=idGlobalError]').show();
					}
					$('#loading').hide();
				},
				complete : function() {
					$('#loading').hide();
				}
			},
			"order" : [],
			"columns" : [ {
				"data" : "id",
				"searchable" : false,
				"orderable" : false,
				"render" : function(data, type, row, meta) {
                   /* idx++; */
					var ret = '<a href="productContractListEdit?id=' + row.id + '"  title=<spring:message code="tooltip.edit" /> style="color: rgb(93,93,93);"><i class="fa fa-lg fa-pencil-square-o" aria-hidden="true"></i></a>';
					ret += '&nbsp;<a href="#myModal" onClick="javascript:updateLink(\'' + row.id + '\');" title=<spring:message code="tooltip.delete" /> role="button" class="${!buyerReadOnlyAdmin ? "":"disabled"}" data-toggle="modal" style="color: rgb(93,93,93);">';
					return ++meta.row;
				}
			},{
				"data" : "contractItemNumber"
			},{
				"data" : "itemName",
				"defaultContent" : ""
			},{
				"data" : "itemCode",
				"defaultContent" : ""
			},{
				"data" : "quantity",
				"defaultContent" : ""
			},{
				"data" : "balanceQuantity",
				"defaultContent" : ""
			},{
				"data" :"unitPrice",
				"defaultContent" : ""
			},{
				"data" : "storageLoc",
				"defaultContent" : ""
			},{
				"data" : "uom",
				"defaultContent" : ""
			},{
				"data":"businessUnit",
				"defaultContent" : ""
			},{
				"data":"costCenter",
				"defaultContent" : ""
			}],
			"initComplete": function(settings, json) {
				var htmlSearch = '<tr class="tableHeaderWithSearch">';
				$('#tableList thead tr:nth-child(1) th').each(function(i) {
					var title = $(this).text();
					if (!(title == "Actions") && $(this).attr('search-type') != '') {
						if ($(this).attr('search-type') == 'select') {
							var optionsType = $(this).attr('search-options');
							htmlSearch += '<th style="' + $(this).attr("style") + '"><select data-index="'+i+'"><option value=""><spring:message code="application.search"/> ' + title + '</option>';
							if (optionsType == 'statusList') {
								<c:forEach items="${statusList}" var="item">
								htmlSearch += '<option value="${item}">${item}</option>';
								</c:forEach>
							}
							htmlSearch += '</select></th>';
						} else {
							htmlSearch += '<th style="' + $(this).attr("style") + '"><input type="text" placeholder="<spring:message code="application.search"/> '+title+'" data-index="'+i+'" /></th>';
						}
					} else {
						htmlSearch += '<th style="' + $(this).attr("style") + '"><div style="visibility:hidden;' + $(this).attr("style") + '"></div></th>';
					}
				});
				htmlSearch += '</tr>';
				$('#tableList thead').append(htmlSearch);
				$(table.table().container()).on('keyup', 'thead input', function() {
					if ($.trim(this.value).length > 2 || this.value.length == 0) {
						table.column($(this).data('index')).search(this.value).draw();
					}
				});
				$(table.table().container()).on('change', 'thead select', function() {
					table.column($(this).data('index')).search(this.value).draw();
				});
			}
		});
		
		$(document).on("keyup", "#chosenBusinessUnit_chosen .chosen-search input", keyDebounceDelay(function(e) {
			// ignore arrow keys
			switch (e.keyCode) {
			case 17: // CTRL
				return false;
				break;
			case 18: // ALT
				return false;
				break;
			case 37:
				return false;
				break;
			case 38:
				return false;
				break;
			case 39:
				return false;
				break;
			case 40:
				return false;
				break;
			}
			var businessUnit = $.trim(this.value);
			if (businessUnit.length > 2 || businessUnit.length == 0 || e.keyCode == 8) {
				reloadBusinessUnitList();
			}
		}, 650));
		
		$(document).on("keyup", "#chosenSupplier_chosen .chosen-search input", keyDebounceDelay(function(e) {
			// ignore arrow keys
			switch (e.keyCode) {
			case 17: // CTRL
				return false;
				break;
			case 18: // ALT
				return false;
				break;
			case 37:
				return false;
				break;
			case 38:
				return false;
				break;
			case 39:
				return false;
				break;
			case 40:
				return false;
				break;
			}
			var supplier = $.trim(this.value);
			if (supplier.length > 2 || supplier.length == 0 || e.keyCode == 8) {
				reloadSupplierList();
			}
		}, 650));
	});
	
	function reloadBusinessUnitList() {
		var searchUnit = $.trim($('#chosenBusinessUnit_chosen .chosen-search input').val());
		var header = $("meta[name='_csrf_header']").attr("content");
		var token = $("meta[name='_csrf']").attr("content");
		$.ajax({
			url : getContextPath() + '/buyer/searchBusinessUnit',
			data : {
				'searchUnit' : searchUnit,
			},
			type : 'POST',
			dataType : 'json',
			beforeSend : function(xhr) {
				xhr.setRequestHeader(header, token);
				$('#loading').show();
			},
			success : function(data) {
				var html = '';
				if (data != '' && data != null && data.length > 0) {
					$('#chosenBusinessUnit').find('option:not(:first)').remove();
					$.each(data, function(key, value) {
						if (value.id == null || value.id == '') {
							html += '<option value="" disabled>' + value.displayName + '</option>';
						}else{
							html += '<option value="' + value.id + '">' + value.displayName + '</option>';
						}
					});
				}
				$('#chosenBusinessUnit').append(html);
				$("#chosenBusinessUnit").trigger("chosen:updated");
				$('#chosenBusinessUnit_chosen .chosen-search input').val(searchUnit);
				$('#loading').hide();
			},
			error : function(error) {
				console.log(error);
			}
		});
	}
	
	function reloadSupplierList() {
		var searchSupplier = $.trim($('#chosenSupplier_chosen .chosen-search input').val());
		var header = $("meta[name='_csrf_header']").attr("content");
		var token = $("meta[name='_csrf']").attr("content");
		$.ajax({
			url : getContextPath() + '/buyer/searchFavouriteuppliers',
			data : {
				'searchSupplier' : searchSupplier,
			},
			type : 'POST',
			dataType : 'json',
			beforeSend : function(xhr) {
				xhr.setRequestHeader(header, token);
				$('#loading').show();
			},
			success : function(data) {
				var html = '';
				if (data != '' && data != null && data.length > 0) {
					$('#chosenSupplier').find('option:not(:first)').remove();
					$.each(data, function(key, value) {
						if (value.id == null || value.id == '') {
							html += '<option value="" disabled>' + value.companyName + '</option>';
						}else{
							html += '<option value="' + value.id + '">' + value.companyName + '</option>';
						}
					});
				}
				$('#chosenSupplier').append(html);
				$("#chosenSupplier").trigger("chosen:updated");
				$('#chosenSupplier_chosen .chosen-search input').val(searchSupplier);
				$('#loading').hide();
			},
			error : function(error) {
				console.log(error);
			}
		});
	}
	
	function updateLink(id) {
		var link = $("a#idConfirmDelete");
		link.data('href', link.attr("href"));
		link.attr("href", link.data('href') + '' + id);
	}

	function doCancel() {
		var link = $("a#idConfirmDelete");
		link.data('href', link.attr("href"));
		link.attr("href", '${pageContext.request.contextPath}/productListDelete?id=');
	}
	
	<c:if test="${buyerReadOnlyAdmin}">
	$(window).bind('load', function() {
		var allowedFields = '.button-previous, #dashboardLink, #listLink';
		//var disableAnchers = ['#reloadMsg'];        
		disableFormFields(allowedFields);
		$('#page-content').find('select').not(allowedFields).parent('div').addClass('disabled');
	});
	</c:if>

	$.formUtils.addValidator({
		name : 'contract_item',
		validatorFunction : function(value, $el, config, language, $form) {
			var response = true;
			console.log($el.attr('name'), value);
			if ($('#contractItem1').is(":checked") && $.trim(value) == '') {
				response = false;
			}
			return response;
		},
		errorMessage : 'This is a required field',
		errorMessageKey : 'badContractItem'
	});

	$.formUtils.addValidator({
		name : 'contract_date',
		validatorFunction : function(value, $el, config, language, $form) {
			var response = true;
			console.log($('[name="dateTimeRange"]').val());
			if ($('#contractItem1').is(":checked") && $.trim($('#datepicker').val()) == '') {
				response = false;
			}
			return response;
		},
		errorMessage : 'This is a required field',
		errorMessageKey : 'badContractDate'
	});

 	var dateTimeRange;
 	var historyPricing;
	 $(function () {
        $("#contractItem1").click(function () {
            if ($(this).is(":checked")) {
                $("#datepicker").prop("disabled", false);
                $("#datepicker").focus();	
             $("#datepicker-date-time-nodisable").val(dateTimeRange);
             $("#datepicker-date-time-nodisable").val();
             $("#contractReferenceNumber").val(historyPricing);
             $("#contractReferenceNumber").val();
                $("#datepicker-date-time-nodisable").prop('disabled',false);
                $("#datepicker-date-time-nodisable").focus();
                $("#contractReferenceNumber").prop('disabled',false);
                $("#contractReferenceNumber").focus();
                
            } else {
            	 $("#datepicker").val('');
            	 $("#datepicker").prop("disabled", true);
            	 dateTimeRange=$("#datepicker-date-time-nodisable").val();
            	 $("#datepicker-date-time-nodisable").val('');
            	 $("#datepicker-date-time-nodisable").prop('disabled', true);
            	 historyPricing=$("#contractReferenceNumber").val();
            	 $("#contractReferenceNumber").val('');
            	 $("#contractReferenceNumber").prop('disabled',true);
            } 
        });
    });

	$.validate({
		lang : 'en',
		modules : 'file'
	});
</script>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.mask.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/assets/widgets/datepicker/datepicker.js"/>"></script>
<script type="text/javascript">
	// Datepicker bootstrap /

	$(function() {
		"use strict";
		var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate() + 1, 0, 0, 0, 0);
		// console.log(now);
		$('.bootstrap-datepicker').bsdatepicker({
			format : 'yyyy-mm-dd',
			onRender : function(date) {
				return date.valueOf() < now.valueOf() ? 'disabled' : '';
			}
		}).on('changeDate', function(e) {
			$(this).blur();
			$(this).bsdatepicker('hide');
		});
	});
	
	$.formUtils.addValidator({
		name : 'validate_custom_length',
		validatorFunction : function(value, $el, config, language, $form) {
			var val = value.split(".");
			var regp = new RegExp('[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)');
			var t=regp.test(val); 
			if (val[0].replace(/,/g, '').length > 16 || t==true) {
				return false;
			} else {
				return true;
			}
		},
		errorMessage : 'The input value is longer than 16 characters',
		errorMessageKey : 'validateLengthCustom'
	});
	

</script>


<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/widgets/timepicker/timepicker.css"/>">
<script type="text/javascript" src="<c:url value="/resources/assets/widgets/timepicker/timepicker.js"/>"></script>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/widgets/daterangepicker/daterangepicker.css"/>">
<script type="text/javascript" src="<c:url value="/resources/assets/widgets/daterangepicker/moment.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/assets/widgets/daterangepicker/daterangepicker.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/assets/widgets/daterangepicker/daterangepicker-demo.js"/>"></script>