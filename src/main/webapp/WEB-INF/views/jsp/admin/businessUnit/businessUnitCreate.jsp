<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<sec:authorize access="hasRole('ROLE_ADMIN_READONLY')" var="buyerReadOnlyAdmin" />
<sec:authorize access="hasRole('BUSINESS_UNIT_LIST') or hasRole('ADMIN')" var="canEdit" />
<spring:message var="businessUnitDesk" code="application.buyer.business.unit" />
<div id="page-content">
	<div class="col-md-offset-1 col-md-10">
		<ol class="breadcrumb">
			<c:url value="/buyer/buyerDashboard" var="dashboardUrl" />
			<li>
				<a id="dashboardLink" href="${dashboardUrl}">
					<spring:message code="application.dashboard" />
				</a>
			</li>
			<c:url value="/buyer/listBusinessUnit" var="listUrl" />
			<li>
				<a id="listLink" href="${listUrl}">
					<spring:message code="businessUnit.list" />
				</a>
			</li>
			<li class="active">
				<c:out value='${btnValue}' />
				<spring:message code="label.businessUnit" />
			</li>
		</ol>
		<!-- page title block -->
		<div class="Section-title title_border gray-bg">
			<h2 class="trans-cap line_icon">
				<spring:message code="label.businessUnit" />
			</h2>
		</div>
		<div class="col_12 graph">
			<div class="white_box_brd pad_all_15">
				<section class="index_table_block">
					<div class="row">
						<div class="col-xs-12 ">
							<jsp:include page="/WEB-INF/views/jsp/templates/message.jsp" />
							<div class="Invited-Supplier-List import-supplier white-bg">
								<div class="meeting2-heading">
									<h3>
										<c:out value='${btnValue}' />
										<spring:message code="label.businessUnit" />
									</h3>
								</div>
								<div class="import-supplier-inner-first-new pad_all_15 global-list">
									<form:form cssClass="form-horizontal" action="businessUnit?${_csrf.parameterName}=${_csrf.token}" method="post" modelAttribute="businessUnit" id="idBusinessUnit" enctype="multipart/form-data">
										<div class="row marg-bottom-20">
											<div class="col-md-3">
												<form:label path="unitName" cssClass="marg-top-10">
													<spring:message code="label.businessUnitName" />
												</form:label>
											</div>
											<div class="col-md-5">
												<form:hidden path="id" id ="id" name="id" />
												<form:hidden path="budgetCheckOld" />
												<spring:message code="bu.enter.unitname.placeholder" var="unitnameplace" />
												<form:input path="unitName" type="text" data-validation-length="1-60" data-validation="required length" cssClass="form-control" placeholder="${unitnameplace}" />
											</div>
										</div>
										
										<div class="row marg-bottom-20">
											<div class="col-md-3">
												<label class="marg-top-10">
													<spring:message code="systemsetting.bu.unitcode" />
												</label>
											</div>
											<div class="col-md-5">
												<spring:message code="bu.enter.unitcode.placeholder" var="unitcodeplace" />
												<form:input path="unitCode" id="unitCode" type="text" data-validation-length="1-12" data-validation="${codeRequerd ? 'required' :'' }  length"  data-validation-optional="true" cssClass="form-control" placeholder="${unitcodeplace}" />
											</div>
										</div>
										
										<div class="row marg-bottom-20">
											<div class="col-md-3">
												<form:label path="displayName" cssClass="marg-top-10">
													<spring:message code="label.businessDisplayName" />
												</form:label>
											</div>
											<div class="col-md-5">
												<spring:message code="bu.enter.displayname.placeholder" var="displaynameplace" />
												<form:input path="displayName" type="text" data-validation-length="1-60" data-validation="required length" cssClass="form-control" placeholder="${displaynameplace}" />
											</div>
										</div>
										<div class="row marg-bottom-20">
											<div class="col-md-3">
												<form:label path="status" cssClass="marg-top-10">
													<spring:message code="label.parentBusinessUnit" />
												</form:label>
											</div>
											<div class="col-md-5">
												<form:select path="parent" cssClass="form-control chosen-select" id="idParent">
													<form:option value=""><spring:message code="pr.select.business.unit"/></form:option>
													<form:options items="${parentList}" itemValue="id" itemLabel="unitName" />
												</form:select>
											</div>
										</div>
										<div class="row marg-bottom-20">
											<div class="col-md-3">
												<form:label path="line1" cssClass="marg-top-10">
													<spring:message code="systemsetting.bu.line1" />
												</form:label>
											</div>
											<div class="col-md-5">
												<spring:message code="bu.enter.line1.placeholder" var="buline1"/>
												<form:input path="line1" type="text" data-validation-length="0-60" data-validation="length" cssClass="form-control" placeholder="${buline1}" />
											</div>
										</div>
										<div class="row marg-bottom-20">
											<div class="col-md-3">
												<form:label path="line2" cssClass="marg-top-10">
													<spring:message code="systemsetting.bu.line2" />
												</form:label>
											</div>
											<div class="col-md-5">
												<spring:message code="bu.enter.line2.placeholder" var="buline2"/>
												<form:input path="line2" type="text" data-validation-length="0-60" data-validation="length" cssClass="form-control" placeholder="${buline2}" />
											</div>
										</div>
										<div class="row marg-bottom-20">
											<div class="col-md-3">
												<form:label path="line3" cssClass="marg-top-10">
													<spring:message code="systemsetting.bu.line3" />
												</form:label>
											</div>
											<div class="col-md-5">
												<spring:message code="bu.enter.line3.placeholder" var="buline3"/>
												<form:input path="line3" type="text" data-validation-length="0-60" data-validation="length" cssClass="form-control" placeholder="${buline3}" />
											</div>
										</div>
										<div class="row marg-bottom-20">
											<div class="col-md-3">
												<form:label path="line4" cssClass="marg-top-10">
													<spring:message code="systemsetting.bu.line4" />
												</form:label>
											</div>
											<div class="col-md-5">
												<spring:message code="bu.enter.line4.placeholder" var="buline4"/>
												<form:input path="line4" type="text" data-validation-length="0-60" data-validation="length" cssClass="form-control" placeholder="${buline4}" />
											</div>
										</div>
										<div class="row marg-bottom-20">
											<div class="col-md-3">
												<form:label path="line5" cssClass="marg-top-10">
													<spring:message code="systemsetting.bu.line5" />
												</form:label>
											</div>
											<div class="col-md-5">
												<spring:message code="bu.enter.line5.placeholder" var="buline5"/>
												<form:input path="line5" type="text" data-validation-length="0-60" data-validation="length" cssClass="form-control" placeholder="${buline5}" />
											</div>
										</div>
										<div class="row marg-bottom-20">
											<div class="col-md-3">
												<form:label path="line6" cssClass="marg-top-10">
													<spring:message code="systemsetting.bu.line6" />
												</form:label>
											</div>
											<div class="col-md-5">
												<spring:message code="bu.enter.line6.placeholder" var="buline6"/>
												<form:input path="line6" type="text" data-validation-length="0-60" data-validation="length" cssClass="form-control" placeholder="${buline6}" />
											</div>
										</div>
										<div class="row marg-bottom-20">
											<div class="col-md-3">
												<form:label path="line7" cssClass="marg-top-10">
													<spring:message code="systemsetting.bu.line7" />
												</form:label>
											</div>
											<div class="col-md-5">
												<spring:message code="bu.enter.line7.placeholder" var="buline7"/>
												<form:input path="line7" type="text" data-validation-length="0-60" data-validation="length" cssClass="form-control" placeholder="${buline7}" />
											</div>
										</div>

										<div class="row marg-bottom-20">
											<div class="col-md-3">
												<label Class="marg-top-10"> <spring:message code="systemsetting.bu.logo" /> </label>
											</div>
											<div class="col-md-5">
												<div class="profile">
													<c:if test="${empty logoImg}">
														<img id="logoImageHolder" src="${pageContext.request.contextPath}/resources/images/logo-image.png" alt="Logo" onclick="$('#logoImg').click()" />
														<div class="col-md-8">
															<a href="javascript:" onclick="$('#logoImg').click()" ><spring:message code="systemsetting.bu.upload.logo" /></a>
														</div>
														<div class="col-md-4">
															<a href="javascript:" id="removeLogo" ><spring:message code="systemsetting.bu.remove.logo" /></a>
														</div>
													</c:if>
													<c:if test="${not empty logoImg}">
														<img id="logoImageHolder" src="data:image/jpeg;base64,${logoImg}" alt="Logo" onclick="$('#logoImg').click()" />
														<div class="col-md-8">
															<a href="javascript:" onclick="$('#logoImg').click()" ><spring:message code="systemsetting.bu.upload.logo" /></a>
														</div>
														<div class="col-md-4">
															<a href="javascript:" id="removeLogo" ><spring:message code="systemsetting.bu.remove.logo" /></a>
														</div>
														</c:if>
													<form:input type="file" accept="image/*" style="visibility: hidden" name="logoImg" id="logoImg" path="" />
													<input type="hidden" id="removeFile" name="removeFile" value="false">
												</div>

											</div>
										</div>
										<input type="hidden" id="recursive" path="recursive" value="FALSE" name="recursive" > 
										
										<div class="row marg-bottom-20">
											<div class="col-md-3">
												<form:label path="status" cssClass="marg-top-10">
													<spring:message code="systemsetting.bu.budget.check" />
												</form:label>
											</div>
											<div class="col-md-5 marg-top-10">
												<form:checkbox id="idBudgetCheck" value="budgetCheck" path="budgetCheck" class="custom-checkbox" />
											</div>
										</div>										
										<div class="row marg-bottom-20">
											<div class="col-md-3">
												<form:label path="status" cssClass="marg-top-10">
													<spring:message code="application.status" />
												</form:label>
											</div>
											<div class="col-md-5">
												<spring:message code="application.status.required" var="required" />
												<form:select path="status" cssClass="form-control chosen-select" id="idStatus" data-validation="required" data-validation-error-msg-required="${required}">
													<form:options items="${statusList}" />
												</form:select>
											</div>
										</div>
										<div class="clear"></div>
										<div class="row marg-bottom-20">
											<div class="col-md-3">
												<label class="marg-top-10"></label>
											</div>
											<div class="col-md-9 dd sky mar_b_15">
												<input type="button" value="${btnValue}" id="saveBusinessUnit" class=" ${ !canEdit ? 'disabled' : '' } btn btn-info ph_btn_midium hvr-pop hvr-rectangle-out">
												<c:url value="/buyer/listBusinessUnit" var="listUrl" />
												<a href="${listUrl}" class="btn btn-black hvr-pop hvr-rectangle-out1 ph_btn_midium button-previous">
													<spring:message code="application.cancel" />
												</a>
											</div>
										</div>
									</form:form>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="removeTemplateUserListPopupEnable" role="dialog">
	<div class="modal-dialog for-delete-all reminder">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h3>
					<spring:message code="tooltip.remove.bussiness.user" />
				</h3>
				<button class="close for-absulate" onclick="javascript:doCancel();" type="button" data-dismiss="modal">�</button>
			</div>
			<div class="modal-body body-font">
				<label><spring:message code="bussinessUnit.confirm.budget" />
				<span></span>
				<spring:message code="bussinessUnit.enable.budget" />
				<span></span> ?</label>
			</div>
			<div class="modal-footer pad_all_15 float-left width-100 border-top-width-1">
			    <button id="yesUnit" type="button" class="btn btn-info pull-left ph_btn_small hvr-pop hvr-rectangle-out widthset-100 setbackground">
					<spring:message code="bussiness.yes" />
				</button>
				<button id="noUnit" type="button" class="widthset-100 btn btn-black pull-right btn-default ph_btn_small hvr-pop hvr-rectangle-out1" data-dismiss="modal">
					<spring:message code="bussiness.no" />
				</button>
			</div>
		</div>
	</div>
</div>


<!--For Disable-->

<div class="modal fade" id="removeTemplateUserListPopupDisable" role="dialog">
	<div class="modal-dialog for-delete-all reminder">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h3>
					<spring:message code="tooltip.remove.bussiness.user" />
				</h3>
				<button class="close for-absulate" onclick="javascript:doCancel();" type="button" data-dismiss="modal">�</button>
			</div>
			<div class="modal-body body-font">
				<label><spring:message code="bussinessUnit.confirm.budget" />
				<span></span>
				<spring:message code="bussinessUnit.disable.budget" />
				<span></span> ?</label>
			</div>
			<div class="modal-footer pad_all_15 float-left width-100 border-top-width-1">
			    <button id="yesUnit" type="button" class="btn btn-info pull-left ph_btn_small hvr-pop hvr-rectangle-out widthset-100 setbackground">
					<spring:message code="bussiness.yes" />
				</button>
				<button id="noUnit" type="button" class="widthset-100 btn btn-black pull-right btn-default ph_btn_small hvr-pop hvr-rectangle-out1" data-dismiss="modal">
					<spring:message code="bussiness.no" />
				</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.form-validator.js"/>"></script>
<!-- <style>
.profile_padding {
padding: 5px 11px 8px !important;
}
</style> -->
<script>
	//for image select
	$(document).ready(function() {
		$("#image-holder").on('click', '.thumb-image', function() {
			$(this).toggleClass("selectedItem");
		});

		$("#selectedItem").on("click", function() {
			$(".selectedItem").remove();
		});

		$("#logoImg").on('change', function() {
			if (typeof (FileReader) == null) {
				var image_holder = document.getElementById("logoImageHolder").src;
				console.log("=====");
				image_holder.attr('src', getContextPath() + '/resources/images/logo-image.png');
			} else if (typeof (FileReader) != "undefined") {
				$("#removeFile").val(false);
				var image_holder = $("#logoImageHolder");
				image_holder.empty();
				var reader = new FileReader();
				reader.onload = function(e) {
					image_holder.attr('src', e.target.result);
				}
				//console.log("=====");
				image_holder.show();
				reader.readAsDataURL($(this)[0].files[0]);
			} else {
				//alert("This browser does not support FileReader.");
			}
		});

		$("#removeLogo").click(function() {
			$("#logoImg").val("");
			$("#removeFile").val(true);
			$('#logoImageHolder').attr('src', getContextPath() + '/resources/images/logo-image.png')
		});
	});
	<c:if test="${buyerReadOnlyAdmin}">
	$(window).bind('load', function() {
		var allowedFields = '.button-previous, #dashboardLink, #listLink';
		//var disableAnchers = ['#reloadMsg'];        
		disableFormFields(allowedFields);
		$('#page-content').find('select').not(allowedFields).parent('div').addClass('disabled');
	});
	</c:if>

	
	$.formUtils.addValidator({
		name : 'unitCode',
		validatorFunction : function(value, $el, config, language, $form) {
			var unitCode = $('#unitCode').val();
			var response = true;
			var header = $("meta[name='_csrf_header']").attr("content");
			var token = $("meta[name='_csrf']").attr("content");
			//alert(loginId);
			$.ajax({
				url : getContextPath() + "/buyer/checkUnitCode",
				data : {
					'unitCode' : unitCode
				},
				async : false,
				beforeSend : function(xhr) {
					xhr.setRequestHeader(header, token);
				},
				success : function(data) {
					console.log(data);
					response = true;
				},
				error : function(request, textStatus, errorThrown) {
					console.log(textStatus);
					response = false;
				}
			});
			return response;
		},
		errorMessage : 'Unit code already used in the system',
		errorMessageKey : 'Unit code'
	});
	
	
	$(document).delegate('#saveBusinessUnit', 'click', function(e) {
		e.preventDefault();
		
		var idParent = $("#idParent").val();
		var id = $("#id").val();
		var  budgetCheckOld = $("#budgetCheckOld").val().toString();
		var  idBudgetCheck  = $("#idBudgetCheck").prop('checked').toString() ;

		if(idParent == '' && id != '' && budgetCheckOld  != idBudgetCheck)
		{
			if(idBudgetCheck == "true"){
				$('#removeTemplateUserListPopupEnable').modal().show();
			}
			else{
				$('#removeTemplateUserListPopupDisable').modal().show();
			}
			
		} else {
			$("#idBusinessUnit").submit()
		}
		
	});
	
	$(document).delegate('#yesUnit', 'click', function(e){
		e.preventDefault();
		$("#recursive").val(true)
		$("#idBusinessUnit").submit()
	});
	

	$(document).delegate('#noUnit', 'click', function(e){
		e.preventDefault();
		$("#recursive").val(false)
		$("#idBusinessUnit").submit()
	});
	
	
	$.validate({
		lang : 'en'
	});
</script>
<style>
.widthset-100 {
	width: 100px;
}
.setbackground {
background: #0095d5 !important;
}
.body-font{
font-size: 16px !important;
}
</style>

	<script type="text/javascript" src="<c:url value="/resources/js/jquery.mask.min.js"/>"></script>