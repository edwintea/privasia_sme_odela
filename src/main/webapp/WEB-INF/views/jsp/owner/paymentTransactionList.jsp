<%@ page import="org.apache.velocity.runtime.parser.node.GetExecutor"%>
<%@ page import="org.w3c.dom.Document"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<sec:authentication property="principal.languageCode" var="languageCode" />
<div id="page-content" view-name="paymentTransaction">
	<div class="container col-md-12">
		<!-- pageging  block -->
		<ol class="breadcrumb">
			<li><a href="#"> <spring:message code="application.dashboard" />
			</a></li>
			<li class="active">Payment Transactions</li>
		</ol>
		<div class="Section-title title_border gray-bg">
			<h2 class="trans-cap manage_icon">Payment Transaction List</h2>
		</div>
		<div class="container-fluid col-md-12">
			<jsp:include page="/WEB-INF/views/jsp/templates/ajaxMessage.jsp" />
			<div class="row">
				<div class="col_12">
					<div class="white_box_brd pad_all_15">
						<section class="index_table_block">
							<jsp:include page="/WEB-INF/views/jsp/templates/message.jsp" />
							<div class="row">
								<div class="col-xs-12">
									<div class="form-group col-md-12 bordered-row">
										<div class="ph_tabel_wrapper scrolableTable_list">
											<table id="tableList" class=" display table table-bordered noarrow" cellspacing="0" width="100%">
												<thead>
													<tr class="tableHeaderWithSearch">
														<%-- 														<th search-type="text"><spring:message code="application.action" /></th>
 --%>
														<th search-type="text">Company Name</th>
														<th search-type="text" class="align-left">Country</th>
														<th search-type="text" class="align-left">Transaction Id</th>
														<!-- 														<th search-type="select" search-options="transactionTypeList">Transaction Type</th>
 -->
														<th search-type="text" class="align-right">Amount</th>
														<th search-type="text" class="align-left">Plan</th>
														<th search-type="" class="width_200 width_200_fix" class="align-left">Transaction Time</th>
														<th search-type="select" search-options="statusList" class="align-left">Status</th>
													</tr>
												</thead>
											</table>
										</div>
										<div id="morris-bar-yearly" class="graph" style="visibility: hidden"></div>
									</div>
								</div>
						</section>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%-- <script type="text/javascript" src="<c:url value="/resources/js/view/paymentTransaction.js"/>"></script>
 --%>
<script type="text/javascript">
	$('document').ready(function() {

		var table = $('#tableList').DataTable({
			"oLanguage":{
				"sUrl":  getContextPath() + "/resources/assets/widgets/datatable/datatable.${languageCode}.json"
		},
			"processing" : true,
			"deferRender" : true,
			"preDrawCallback" : function(settings) {
				$('div[id=idGlobalError]').hide();
				$('#loading').show();
				return true;
			},
			"drawCallback" : function() {
				// in case your overlay needs to be put away automatically you can put it here
				$('#loading').hide();
			},
			"serverSide" : true,
			"pageLength" : 10,
			"searching" : true,
			"ajax" : {
				"url" : getContextPath() + "/owner/paymentTransaction/paymentTransactionData",
				"data" : function(d) {
				},
				"error": function(request, textStatus, errorThrown) {
					var error = request.getResponseHeader('error');
					if (error != undefined) {
						$('p[id=idGlobalErrorMessage]').html(error != null ? error.split(",").join("<br/>") : "");
						$('div[id=idGlobalError]').show();
					}
					$('#loading').hide();
				}
			},
			"order" : [],
			"columns" : [ 
/* 			{
				"data" : "id",
				"searchable" : false,
				"orderable" : false,
				"render" : function(data, type, row) {
					var ret = '<a href="' + getContextPath() + '/owner/paymentTransaction/viewPaymentTransaction/' + row.id + '"><img src="${pageContext.request.contextPath}/resources/images/edit1.png"></a>';
					return ret;
				}
			}, 
 */			{
				"data" : "companyName",
				"className" : "align-left",
				"defaultContent" : ""
			}, {
				"data" : "country.countryName",
				"className" : "align-left",
				"defaultContent" : ""
			}, {
				"data" : "referenceTransactionId",
				"className" : "align-left",
				"defaultContent" : ""
/* 			}, {
				"data" : "type",
				"defaultContent" : ""
 */			}, {
				"data" : "amount",
				"defaultContent" : "",
				"className" : "align-right",
				"render" : function(data, type, row) {
					var ret =  row.currencyCode + ' ' + (row.amount ? row.amount : 'UNKNOWN');
					return ret;
				}
			}, {
				"data" : "plan.planName",
				"defaultContent" : "",
				"className" : "align-left",
				"render" : function(data, type, row) {
					var ret = '';
					if(row.buyerPlan != null) {
						ret = row.buyerPlan.planName;
					}
					if(row.supplierPlan != null) {
						ret = row.supplierPlan.planName;
					}
					return ret;
				}
			}, {
				"data" : "createdDate",
				"className" : "align-left",
				"searchable" : false,
				"defaultContent" : ""
			}, {
				"data" : "status",
				"className" : "align-left",
				"defaultContent" : "",
				"render" : function(data, type, row) {
					var ret =  row.status.replace('_', ' ');
					return ret;
				}
			} ],
		
		"initComplete": function(settings, json) {
		var htmlSearch = '<tr class="tableHeaderWithSearch">';
		$('#tableList thead tr:nth-child(1) th').each(function(i) {
			var title = $(this).text();
			if (!(title == "Actions") && $(this).attr('search-type') != '') {
				if($(this).attr('search-type') == 'select'){
					var optionsType = $(this).attr('search-options');
					htmlSearch += '<th class="align-left" style="'+$(this).attr("style")+'"><select data-index="'+i+'"><option value=""><spring:message code="application.search"/> '+title+'</option>';
					if(optionsType == 'transactionTypeList'){
						<c:forEach items="${transactionTypeList}" var="item">
						htmlSearch += '<option value="${item}">${item}</option>';
						</c:forEach>
					}
					if(optionsType == 'statusList'){
						<c:forEach items="${statusList}" var="item">
						htmlSearch += '<option value="${item}">${item}</option>';
						</c:forEach>
					}
					htmlSearch += '</select></th>';
				} else {
					htmlSearch += '<th class="align-left" style="'+$(this).attr("style")+'"><input type="text" placeholder="<spring:message code="application.search"/> '+title+'" data-index="'+i+'" /></th>';
				}
			} else {
				htmlSearch += '<th class="align-left" style="'+$(this).attr("style")+'"><div style="visibility:hidden;'+$(this).attr("style")+'"></div></th>';
			}
		});
		htmlSearch += '</tr>';
		$('#tableList thead').append(htmlSearch);
		$(table.table().container()).on('keyup', 'thead input', function() {
			if ($.trim(this.value).length > 2 || this.value.length == 0) {
				table.column($(this).data('index')).search(this.value).draw();
			}
		});
		$(table.table().container()).on('change', 'thead select', function() {
			table.column($(this).data('index')).search(this.value).draw();
		});
		}
		});
	});
</script>
