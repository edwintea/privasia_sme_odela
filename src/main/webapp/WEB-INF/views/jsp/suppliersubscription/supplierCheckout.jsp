<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="container margin-bottom-5">
	<c:url var="doInitiateSupplierPayment" value="doInitiateSupplierPayment" />
	<form:form action="${doInitiateSupplierPayment}" id="supplierCheckoutForm" method="post" modelAttribute="supplier" autocomplete="off">
		<div class="buyer-check-section">
			<p class="font-27">Fill in your details to start your subscription process</p>
			<p>&nbsp;</p>			
		</div>
		<div class="row">
			<div class="col-sm-1 col-md-1 col-lg-1 col-xs-0 margin-right-3"></div>
			<div class="col-sm-12 col-md-12 col-lg-4 col-xs-12 buyer-purchase-details">

				<div class="form-group">
					<label>Company Name</label>
					<!-- <input type="text" class="form-control" id="" name="companyname"> -->
					<form:input path="companyName" cssClass="form-control" id="idCompanyName" placeholder="Once submitted, cannot be changed" data-validation="required length alphanumeric company_name" data-validation-allowing="-_ &.()" data-validation-length="4-124" />
				</div>
				<div class="form-group">
					<label><spring:message code="supplier.registration.company.number" /></label>
					<form:input path="companyRegistrationNumber" cssClass="form-control" id="idCompRegNum" placeholder="Once submitted, cannot be changed" data-validation="required length alphanumeric crn_number" data-validation-allowing="-_ " data-validation-length="2-124" />
				</div>
				<div class="form-group">
					<label>Country of Registration</label>
					<form:select path="registrationOfCountry" id="idRegCountry" cssClass="form-control" data-validation="required crn_number company_name">
						<form:option value="">Select Country of Registration</form:option>
						<form:options items="${countryList}" itemValue="id" itemLabel="countryName" />
					</form:select>
				</div>
				<div class="form-group">
					<label>Full Name</label>
					<form:input path="fullName" cssClass="form-control" id="idFullName" placeholder="Once submitted, cannot be changed" data-validation="required length alphanumeric" data-validation-allowing="-_ ." data-validation-length="2-128" />
				</div>
				<div class="form-group">
					<label><spring:message code="supplier.designation" /></label>
					<form:input path="designation" cssClass="form-control" id="idDesignation" placeholder="Once submitted, cannot be changed" data-validation="required length alphanumeric" data-validation-allowing="- " data-validation-length="2-128" />
				</div>
				<div class="form-group">
					<label>Login Email</label>
					<form:input path="loginEmail" cssClass="form-control" id="idLoginEmail" placeholder="This email cannot be changed later" data-validation="required length email login_email" data-validation-length="max124" />
				</div>

				<div class="form-group">
					<label>Communication Email</label>
					<form:input path="communicationEmail" class="form-control" id="idCommunicationEmail" placeholder="Email will be sent to this address" data-validation="required length email" data-validation-length="max124" />
				</div>

				<div class="form-group">
					<label>Admin Mobile Number</label>
					<form:input path="mobileNumber" cssClass="form-control" id="idAdminMobileNo" placeholder="Country code and number e.g. +6017666666" data-validation="required length number" data-validation-ignore="+ " data-validation-length="6-14" />
				</div>
				<div class="form-group">
					<label>Contact Number</label>
					<form:input path="companyContactNumber" cssClass="form-control" id="idCompanyContactNumber" placeholder="+60345678906" data-validation="required length number" data-validation-ignore="+ " data-validation-length="6-14" />
				</div>
				<div class="form-group">
					<label>Password</label>
					<c:set var="rgx" value="^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{8,15}$"></c:set>
					<form:password path="password" cssClass="form-control pwd" id="exampleInputPass1" placeholder="Minimum 8 characters with one number, one capital letter" data-validation="custom" data-validation-regexp="${!empty regex ? regex.regx :rgx}" data-validation-error-msg="${!empty regex.message? regex.message :''}" />
					<p class="pass-desc" id="passwordPlaceHolder">${!empty regex.message?regex.message:''}</p>
				</div>
				<!--   <div class="form-group">
          <div id="g-recaptcha" data-sitekey="6LdG1iUTAAAAAJB9POQXAHttIeHra0NuqA1NwzBg"><div style="width: 304px; height: 78px;"><div><iframe src="https://www.google.com/recaptcha/api2/anchor?ar=1&amp;k=6LezmB8TAAAAAFfN-D8nEnnuGpPohFZn4JKUbdNg&amp;co=aHR0cHM6Ly9hcHAucHJvY3VyZWhlcmUuY29tOjQ0Mw..&amp;hl=en&amp;v=v1526338122299&amp;theme=light&amp;size=normal&amp;cb=r6ek5xxe09ir" width="304" height="78" role="presentation" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe></div><textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: none; "></textarea></div></div>
          <input id="recaptchaResponse" name="recaptchaResponse" type="hidden" value="" class="form-control">


        </div> -->
				<p class="ts-pera">
					*By proceeding, you agree to our <span> <a target="_blank" href="<c:url value="/resources/termsandcondition.pdf"/>">terms & conditions</a>
					</span>
				</p>

				<div id="paypalPayment">
					<span class="rfr_field proceedPayment" id="supplierCheckOutBtn"></span>
					<div>
						<img src="<c:url value="/resources/images/public/Visa.png"/>" alt="Visa"> <img src="<c:url value="/resources/images/public/mastercard-badge.png"/>" alt="Mastercard"> <img src="<c:url value="/resources/images/public/american-express.png"/>" alt="American Express"> <img src="<c:url value="/resources/images/public/discover.png"/>" alt="visa-badge"> <br> <img src="<c:url value="/resources/images/public/paypal.png"/>" alt="Paypal"> <img src="<c:url value="/resources/images/public/McAfee.png"/>" alt="McAfee">
					</div>
				</div>
				<div id="submitPayment" style="display: none;">
					<button type="submit" class="btn btn-primary btn-padding submitSuppButton">Submit</button>
					<div>
						<img src="<c:url value="/resources/images/public/McAfee.png"/>" alt="McAfee-badge" class="max-80 img-responsive" style="margin-top: 10px;">
					</div>
				</div>
			</div>
			<div class="col-sm-1 col-md-1 col-lg-1 col-xs-0"></div>
			<div class="col-sm-12 col-md-12 col-lg-4 col-xs-12 pack-amount-details">
				<input type="hidden" name="promoCodeId" id="promoCodeId">

				<h4 class="border-btm toptitle" id="toptitle">UNLIMITED BUYER</h4>
				<div class="pack-details">
					<p class="sub-heading">Supplier Subscription</p>
					<form>
						<input type="hidden" name="supplierPrice" id="supplierPrice" value="0.00">
						<!-- <div class="radio">
							<div class="pack-details">
								<label class="width-label-100"> <input type="radio" name="supplerPlan"  value="SINGLEBUYER" checked="checked"> <span class="left-span">Single
										Buyer Plan</span> <span class="pull-right width-label-100-span-2">
										0.00
								</span>
								</label>
								<p class="usd-details">US$30 per annum</p>
							</div>
						</div> -->
						<div class="radio">
							<div class="pack-details">
								<label class="width-label-100"> <input type="radio" name="supplerPlan" value="ALLBUYER" checked="checked"> <span class="left-span">Unlimited Buyer Plan</span> <span class="pull-right width-label-100-span-2">${plan.price}</span>
								</label>
								<p class="usd-details">US$${plan.price} per annum</p>
							</div>
						</div>
					</form>
				</div>
				<div class="pack-details">
					<div class="form-inline">
						<label class="margin-right-20">Promo Code</label> <input type="text" name="promocode" id="promocode" class="form-control"> <span class="pull-right " id="promoDiscountPrice"> <input type="hidden" name="promoDiscountPrice" value="0.00"> 0.00
						</span> <span id="promoError"></span>
					</div>
				</div>
				<h4 class="border-btm"></h4>
				<div>
					<h4 class="pull-left">TOTAL FEE</h4>
					<h4 class="pull-right" id="totalPrice">
						<input type="hidden" name="totalPrice" value="0" /> US$ 0.00
					</h4>
				</div>
			</div>
			<div class="col-sm-1 col-md-1 col-lg-1 col-xs-0"></div>
		</div>
	</form:form>
</div>
<script src="<c:url value="/resources/assets/js-core/jquery.form-validator.js"/>"></script>
<script src="//www.paypalobjects.com/api/checkout.js" async></script>
<script type="text/javascript">
	$.validate({
		lang : 'en',
		modules : 'date, security'
	});

	window.paypalCheckoutReady = function() {
		paypal.checkout.setup('${merchantId}', {
			environment : '${paypalEnvironment}',
			container : 'supplierCheckoutForm',
			condition : function() {
				return $('#supplierCheckoutForm').isValid();
			},
			//button: 'placeOrderBtn'
			buttons : [ {
				container : 'supplierCheckOutBtn',
				type : 'checkout',
				color : 'blue',
				size : 'medium',
				shape : 'rect'
			} ]
		});

	};
</script>
<style type="text/css">
.pass-desc {
	color: #7f7f7f;
	font-weight: normal;
	/* 	font-size: 13px; */
}
</style>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form-validator.js"/>"></script>
<script>
	$.validate({
		lang : 'en'
	});
</script>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.mask.min.js"/>"></script>
<script>
	$(document).ready(function() {
		$(".pwd").click(function() {
			$('#passwordPlaceHolder').hide();
		});

		$('#idAdminMobileNo').mask('+00 00000000000', {
			placeholder : "+60 122735465"
		});
		$('#idCompanyContactNumber').mask('+00 00000000000', {
			placeholder : "+60 322761533"
		});
	});
</script>

<script type="text/javascript" src="<c:url value="/resources/js/view/supplierCheckout.js"/>"></script>