<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<spring:message var="supplierPlanDesk" code="application.owner.supplier.subscription.plan" />
 
<div class="container margin-bottom-5">
  <div class="buyer-check-section">
    <p>Just one step left to subscribe to Procurehere's market leading simplified<br> e-procurement solution</p>
    <p>Complete your purchase through our secure payment system, and unlock access to the platform which<br> has saved users US$1.3 billion to date and counting</p>
  </div>
  <div class="row">
    <form:form commandName="buyer" action="${pageContext.request.contextPath}/buyerSubscription/userBasedBuyerCheckout" id="idUserBasedBuyerCheckOutForm" method="post">
    <div class="col-sm-1 col-md-1 col-lg-1 col-xs-0 margin-right-3">
    </div>
    <div class="col-sm-12 col-md-12 col-lg-4 col-xs-12 buyer-purchase-details">
        <div class="form-group">
          <label>Company Name</label>
          	<form:input type="text" id="idCompanyName" cssClass="form-control" placeholder="Enter your company name" data-validation="required length alphanumeric company_name" data-validation-allowing="-_ &.()" data-validation-length="4-124"
						path="buyer.companyName" />
        </div>
        <div class="form-group">
          <label>Company Registration Number</label>
			<form:input type="text" id="idCompRegNum" cssClass="form-control" placeholder="Enter your company registration number" data-validation="required length alphanumeric crn_number" data-validation-allowing="-_ " data-validation-length="2-124"
				path="buyer.companyRegistrationNumber" />
        </div>
        <div class="form-group">
          <label>Country of Registration</label>
			<form:select path="buyer.registrationOfCountry" id="idRegCountry" cssClass="form-control" data-validation="required country_change crn_number company_name">
				<form:option value="">Select Country of Registration</form:option>
				<form:options items="${countryList}" itemValue="id" itemLabel="countryName" />
			</form:select>
        </div>
		<div class="form-group">
			<label>Full Name</label>
			<!-- <input type="text" class="form-control" id="r" name="fname"> -->
			<form:input type="text" cssClass="form-control" placeholder="Company contact person name" data-validation="required length" path="buyer.fullName" data-validation-length="4-100" />
		</div>
		<div class="form-group">
			<label>Login Email</label>
			<!-- <input type="text" class="form-control" id="rr" name="lname"> -->
			<form:input type="text" id="idLoginEmail" cssClass="form-control" placeholder="e.g. contact@company.com" data-validation="required length email login_email" data-validation-length="6-128" path="buyer.loginEmail" />
		</div>
		<div class="form-group">
			<label>Communication Email</label>
			<!-- <input type="email" class="form-control" id="rrr" name="email"> -->
			<form:input type="text" cssClass="form-control" placeholder="All email communications will be sent to this address" data-validation="email" path="buyer.communicationEmail" />
		</div>
		<div class="form-group">
			<label>Contact Number</label>
			<!-- <input type="number" class="form-control" id="rrrr" name="companyname"> -->
			<form:input type="text" id="idCntNum" cssClass="form-control" placeholder="e.g. 0123456789" data-validation="number length" path="buyer.companyContactNumber" data-validation-length="6-14" />
		</div>
 
        <p class="ts-pera">*By proceeding, you agree to our <span><a href="<c:url value="/resources/termsandcondition.pdf"/>">terms & conditions</a></span></p>
        <span class="rfr_field" id="buyerUserBaseCheckOutBtn"></span>
        <div>
          <img src="<c:url value="/resources/images/public/Visa.png"/>" alt="Visa-badges">
          <img src="<c:url value="/resources/images/public/mastercard-badge.png"/>" alt="Mastercard-badge">
          <img src="<c:url value="/resources/images/public/american-express.png"/>" alt="American-Express">
          <img src="<c:url value="/resources/images/public/discover.png"/>" alt="visa-badge"><br>
          <img src="<c:url value="/resources/images/public/paypal.png"/>" alt="Paypal-badge">
          <img src="<c:url value="/resources/images/public/McAfee.png"/>" alt="McAfee-badge">
        </div>
       
    </div>
    <div class="col-sm-1 col-md-1 col-lg-1 col-xs-0">
    </div>
    
    <div class="col-sm-12 col-md-12 col-lg-4 col-xs-12 pack-amount-details" id="ubp" >
      <h4 class="border-btm">USER BASED</h4>
      <div class="pack-details">
      <input type="hidden" name="plan.id" id="planId" value="${buyerPlan.id}">
      <input type="hidden" id="userPlanId" value="${buyerPlan.id}">
      <input type="hidden" name="range.id" id="rangeUserId">
      <input type="hidden" name="promoCodeId" id="promoCodeUserId">
        <p><span class="">Starter Pack</span><span class="pull-right">${buyerPlan.basePrice}</span></p>
        <p>${buyerPlan.baseUsers} X Users + ${buyerPlan.baseUsers} X Approvers</p>
      </div>
      <div class="pack-details">
			<c:forEach items="${buyerPlan.rangeList}" var="range" varStatus="index" begin="1" end="1">
				<c:set var="endRange" value="${range.rangeEnd}" />
				<c:set var="addtionalUserPrice" value="${range.price}" />
			</c:forEach>
      
      
        <p>
        	<span class="form-inline">Additional Users
        		<form:input type="text" class="form-control" path="userQuantity" name="numberUser" id="additionalUsers" data-validation="length number" value="0" data-validation-allowing="range[0;${endRange}]" data-validation-optional="true" data-validation-length="1-3"/>
        	</span>
        	<span class="pull-right" id="additionalShowUserPrice">0.00</span>
        </p>
        <fmt:formatNumber groupingUsed="false" var="addtionalFmtUserPrice" type="number" minFractionDigits="2" maxFractionDigits="2" value="${addtionalUserPrice}" />
        <p class="pack-margin-neg-12">US$ ${addtionalFmtUserPrice} per user</p>
      </div>
      
      <div class="pack-details">
        <p><span class="">Subscription Method</p>

		<c:forEach items="${buyerPlan.planPeriodList}" var="period" varStatus="status">
			<div class="radio">
				<label>
					<c:if test="${period.planDuration == '1'}">
						<input type="radio" name="periodId" value="${period.id}" id="periodId" data-validation="required" checked="checked" >
						Monthly
					</c:if>
					<c:if test="${period.planDuration != '1'}">
						<input type="radio" name="periodId" value="${period.id}" id="periodId" data-validation="required" >
						${period.planDuration} Months &nbsp; &nbsp; ${period.planDiscount}% Discount
					</c:if>
				</label>
			</div>
		</c:forEach>
      </div>
      
      <div class="pack-details">
       <form class="form-group">
       <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        <div class="form-inline">
          <label class="margin-right-20">Promo Code</label>
          <input type="text" name="promocode" class="form-control" id="promoCodeUser">
        </div>
      </form>
    </div>
    <h4>TOTAL FEE</h4>

    <div class="pack-details">
        <p id="basePrice"><span class="left-span" id="basePriceDesc" >Starter Pack x 1 Month</span>
       		 <span class="pull-right" >
        		<input type="hidden" name="basePrice" value="${buyerPlan.basePrice}">
				${buyerPlan.basePrice}
        	</span>
        </p>
      </div>
      <div class="pack-details">
        <p>
        <span class="left-span">Additional Users</span><span class="pull-right" id="addtionalUserPrice"> 
        	<input type="hidden" name="addtionalUserPrice" value="0">0.00
        </span>
       </p>
      </div>
      <div class="pack-details">
        <p><span class="left-span">Subscription Discount</span><span class="pull-right"  id="subscriptionDiscountPrice"><input type="hidden" name="subscriptionDiscountPrice" value="0"> 0.00</span></p>
      </div>
      <div class="pack-details">
        <p><span class="left-span">Promotional Code</span><span class="pull-right" id="promoDiscountPrice"><input type="hidden" name="promoDiscountPrice" value="0">0.00</span></p>
      </div>
      <h4 class="border-btm"></h4>
      <h4 class="pull-right" id="totalPrice">US$ <input type="hidden" name="totalPrice" value="${buyerPlan.basePrice}">
			${buyerPlan.basePrice}
	  </h4>

   

  </div>
   <div class="col-sm-1 col-md-1 col-lg-1 col-xs-0">
    </div>
    </form:form>
    
</div>
</div>
<div><a href="#"><img src="./images/chat-icon.png" alt="chat-icon" class="chat-icon"></a></div>
 
<script src="<c:url value="/resources/assets/js-core/jquery.form-validator.js"/>"></script>
<script src="//www.paypalobjects.com/api/checkout.js" async></script>
<script type="text/javascript">
	$.validate({ lang : 'en', modules : 'date, security' });
	
	
	window.paypalCheckoutReady = function() {
		paypal.checkout.setup('${merchantId}', {
			environment : '${paypalEnvironment}',
			container : 'idUserBasedBuyerCheckOutForm',
			condition : function() {
				return $('#idUserBasedBuyerCheckOutForm').isValid();
			},
			//button: 'placeOrderBtn'
			buttons : [ {
				container : 'buyerUserBaseCheckOutBtn',
				type : 'checkout',
				color : 'blue',
				size : 'medium',
				shape : 'rect'
			} ]
		});

	};
	
	
</script>
<script type="text/javascript" src="<c:url value="/resources/js/view/freeTrialPlan.js?1"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/view/freeTrialSignup.js"/>"></script>