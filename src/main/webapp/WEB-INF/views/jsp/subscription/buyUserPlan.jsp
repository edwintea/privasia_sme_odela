<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/assets/elements/procurehere-public.css"/>">
<div class="select_pack_wrap">
	<div class="sp_inner">
		<div class="sp_box1">
			<jsp:include page="/WEB-INF/views/jsp/templates/ajaxMessage.jsp" />
			<jsp:include page="/WEB-INF/views/jsp/templates/message.jsp" />
			<div class="clear"></div>


			<c:forEach items="${planList}" var="plan" varStatus="status">
				<div id="tab-${status.index}" class="tab-content ${status.index == 0 ? 'current' : '' } doc-fir tab-main-inner" style="display: none;">
					<input type="hidden" id="${plan.planType == 'PER_USER' ? 'user': 'event'}currencyId" value="${plan.currency.currencyCode}">

					<%-- <div class="tpb_open_box">
						<c:out value="${plan.shortDescription}" escapeXml="false" />
					</div> --%>
					<c:set var="endRange" value="999" />
					<table id="${plan.planType == 'PER_USER' ? 'userRangeTable': 'eventRangeTable'}" class="table_align_center table1">
						<c:set var="tax" value="${not empty plan.tax ? plan.tax : 0}" />
						<fmt:formatNumber groupingUsed="false" var="taxFormt" type="number" minFractionDigits="0" maxFractionDigits="2" value="${tax}" />
						<fmt:formatNumber groupingUsed="false" var="base" type="number" minFractionDigits="0" maxFractionDigits="2" value="${plan.basePrice}" />
						<input type="hidden" id="taxFormt${plan.planType == 'PER_USER' ? 'User': 'Event'}" value="${tax}">
						<c:set var="basePriceWithTax" value="${not empty base ? base + ((base * taxFormt)/100) : 0}" />
						<fmt:formatNumber groupingUsed="false" var="basePriceWithTaxFormt" type="number" minFractionDigits="2" maxFractionDigits="2" value="${basePriceWithTax}" />
					</table>

					<fmt:formatNumber groupingUsed="false" type="number" var="basePrice" minFractionDigits="2" maxFractionDigits="2" value="${plan.basePrice}" />
					<div>
						<c:choose>
							<c:when test="${empty changePlan}">
								<c:url var="buyerSubs" value="/buyerSubscription/get/${plan.id}" />
							</c:when>
							<c:otherwise>
								<c:url var="buyerSubs" value="/buyer/billing/buyBuyerPlan/${plan.id}" />
							</c:otherwise>
						</c:choose>

						<form action="${buyerSubs}" id="idSubscribeForm" method="post">

							<div class="col-sm-12 col-md-12 col-lg-4 col-xs-12 pack-amount-details" id="ubp">
								<h4 class="border-btm">USER BASED</h4>
								<div class="pack-details">
									<input type="hidden" name="plan.id" id="planId" value="${plan.id}"> <input type="hidden" id="userPlanId" value="${plan.id}"> <input type="hidden" name="range.id" id="rangeUserId"> <input type="hidden" name="promoCodeId" id="promoCodeUserId">
									<p>
										<span class="">Starter Pack</span> <span class="pull-right">${basePrice}</span>
									</p>
									<p>${plan.baseUsers}XUsers+${plan.baseUsers}XApprovers</p>
								</div>
								<div class="pack-details">
									<c:forEach items="${plan.rangeList}" var="range" varStatus="index" begin="1" end="1">
										<c:set var="endRange" value="${range.rangeEnd}" />
										<c:set var="addtionalUserPrice" value="${range.price}" />
									</c:forEach>


									<p>
										<span class="form-inline"> Additional Users <input type="text" class="form-control" name="userQuantity" id="additionalUsers" data-validation="length number" value="0" data-validation-allowing="range[0;${endRange}]" data-validation-optional="true" data-validation-length="1-3" />
										</span> <span class="pull-right" id="additionalShowUserPrice">0.00</span>
									</p>
									<fmt:formatNumber groupingUsed="false" var="addtionalFmtUserPrice" type="number" minFractionDigits="2" maxFractionDigits="2" value="${addtionalUserPrice}" />
									<p class="pack-margin-neg-12">US$ ${addtionalFmtUserPrice} per user</p>
								</div>

								<div class="pack-details">
									<p>
										<span class="">Subscription Method</span>
									</p>

									<c:forEach items="${plan.planPeriodList}" var="period" varStatus="status">
										<div class="radio">
											<label> <c:if test="${period.planDuration == '1'}">
													<input type="radio" name="periodId" value="${period.id}" id="periodId" data-validation="required" checked="checked">
						Monthly
					</c:if> <c:if test="${period.planDuration != '1'}">
													<input type="radio" name="periodId" value="${period.id}" id="periodId" data-validation="required">
						${period.planDuration} Months &nbsp; &nbsp; ${period.planDiscount}% Discount
					</c:if>
											</label>
										</div>
									</c:forEach>
								</div>

								<div class="pack-details">
									<form class="form-group">
										<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
										<div class="form-inline">
											<label class="margin-right-20">Promo Code</label> <input type="text" name="promocode" class="form-control" id="promoCodeUser">
										</div>
									</form>
								</div>
								<h4>TOTAL FEE</h4>

								<div class="pack-details">
									<p>
										<span class="left-span">Starter Pack x 1 Month</span> <span class="pull-right" id="basePrice"> <input type="hidden" name="eventPrice" value="${basePrice}"> ${basePrice}
										</span>
									</p>
								</div>
								<div class="pack-details">
									<p>
										<span class="left-span">Additional Users</span> <span class="pull-right" id="addtionalUserPrice"> <input type="hidden" name="addtionalUserPrice" value="0"> 0.00
										</span>
									</p>
								</div>
								<div class="pack-details">
									<p>
										<span class="left-span">Subscription Discount</span> <span class="pull-right" id="subscriptionDiscountPrice"> <input type="hidden" name="subscriptionDiscountPrice" value="0"> 0.00
										</span>
									</p>
								</div>
								<div class="pack-details">
									<p>
										<span class="left-span">Promotional Code</span> <span class="pull-right" id="promoDiscountPrice"> <input type="hidden" name="promoDiscountPrice" value="0"> 0.00
										</span>
									</p>
								</div>

								<div class="pack-details">
									
									<div>
										<input type="checkbox" id="autoChargeSubscription" name="autoChargeSubscription" class="custom-checkbox" />
									</div>
									<span class="left-span"> <label style="line-height: 0px;">Auto charge subscription <i class="glyph-icon icon-question-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="This will enable auto-renewal of subscription."></i>
									</label>
									</span>
								



									<div class="pack-details">
										<h4 class="border-btm"></h4>
										<h4 class="pull-right" id="totalPrice">
											US$ <input type="hidden" name="totalFeeAmount" value="${basePrice}"> ${basePrice}
										</h4>
										<c:choose>
											<c:when test="${empty changePlan}">
												<input type="submit" class="btn btn-info ph_btn_small hvr-pop hvr-rectangle-out" value="Get ${plan.planName}">
											</c:when>
											<c:otherwise>
												<div class="rfr_field paypal1" id="idButtonHolder"></div>
											</c:otherwise>
										</c:choose>
									</div>

								</div>
							</div>
						</form>
					</div>
				</div>
			</c:forEach>
		</div>
		<!--
		<div class="planGroup">
			<c:forEach items="${planList}" var="plan">
				<div class="sp_box1">
					<div class="spb_heading1">
						<c:out value="${plan.planName}" />
					</div>
					<div class="tpb_open_box">
						<c:out value="${plan.description}" escapeXml="false" />
					</div>
					<div class="choose_bttn">
						<a href="${pageContext.request.contextPath}/subscription/get/${plan.id}" class="cb_style cb_grey hvr-pop">Get ${plan.planName}</a>
					</div>
				</div>
			</c:forEach>
		</div> -->
		<c:if test="${empty planList}">
			<div class="sp_box2">
				<div class="spb_heading4">NO PLANS DEFINED. LOGIN AS ADMIN AND DEFINE SOME PLANS.</div>
			</div>
		</c:if>
	</div>
	<div class="clear"></div>
</div>
<style>
div#idGlobalError {
	float: left;
	width: 100%;
}

.alert .alert-icon {
	line-height: 34px;
	float: left;
	width: 34px;
	height: 34px;
	margin: 5px 10px 0 0;
	text-align: center;
}

.alert .alert-title {
	font-size: 12px;
	font-weight: bold;
	margin: 4px 0 3px;
	padding: 0;
	text-transform: uppercase;
}

.table_align_center {
	width: 80%;
	margin-left: 10%;
	margin-right: 10%;
}

.spb_heading1 {
	font-size: 30px;
	text-transform: none;
}

.spb_heading1 {
	width: 100%;
	float: left;
	text-align: center;
	font-size: 35px;
	text-transform: uppercase;
	color: #758d94;
	padding: 0 0 20px 0;
}

.sp_box1 {
	width: 70%;
	border: 1px solid #ccc;
	margin-bottom: 10px;
}

<!--
for buyer subscription tab  -->.home_tab_wrap {
	width: 100%;
	float: left;
}

ul.tabs {
	margin: 0px;
	padding: 0px;
	list-style: none;
}

.pull-left, .float-left {
	float: left !important;
}

ul.tabs li.current {
	background: #fff;
	color: #636363;
	border-left: 1px solid #e8e8e8;
	border-right: 1px solid #e8e8e8;
	border-top: 1px solid #e8e8e8;
}

ul.tabs li {
	background: #a0a0a0;
	color: #fff;
	display: inline-block;
	padding: 10px 25px;
	cursor: pointer;
	font-family: 'open_sanssemibold';
	font-weight: normal;
	margin-right: 5px;
	border-radius: 3px 3px 0 0;
	position: relative;
	z-index: 2;
}

.tab-content.current {
	display: block !important;
}

.tab-main-inner {
	float: left;
	width: 100%;
	text-align: left;
	background: #fff;
	margin-top: -1px;
	border: 1px solid #e8e8e8;
	position: relative;
	z-index: 1;
}

.marg_top_30 {
	margin-top: 30px;
}

.marg_bottom_10 {
	margin-bottom: 10px;
}

.table1 td, th {
	text-align: center;
}

.td-border td {
	border: 2px solid #a1a1a1;
	padding-top: 10px;
	padding-bottom: 10px;
	padding-left: 10px;
	padding-right: 10px;
	font-size: 15px;
	padding-right: 10px;
}

.td-border {
	border: 2px solid #a1a1a1;
}

.total-td {
	text-align: right !important;
	padding-top: 0px !important;
	padding-bottom: 0px !important;
}

.flagvisibility {
	display: none;
}

.help {
	color: #fff;
	background-color: #feb22a;
	width: 12px;
	height: 12px;
	display: inline-block;
	border-radius: 100%;
	font-size: 10px;
	text-align: center;
	text-decoration: none;
	-webkit-box-shadow: inset -1px -1px 1px 0px rgba(0, 0, 0, 0.25);
	-moz-box-shadow: inset -1px -1px 1px 0px rgba(0, 0, 0, 0.25);
	box-shadow: inset -1px -1px 1px 0px rgba(0, 0, 0, 0.25);
}

.paypal1
{
    margin-left: 45px;
}
</style>
<script type="text/javascript" src="<c:url value="/resources/js/masonry.js"/>"></script>

<script type="text/javascript" src="<c:url value="/resources/js/numeral.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form-validator.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/view/selectPlan.js"/>"></script>
<script>
	$.validate({
		lang : 'en'
	});

	$(document)
			.ready(
					function(e) {
						var confirmPaypal = true;

						<c:if test="${not empty changePlan}">
						window.paypalCheckoutReady = function() {
							console.log("paypal");
							paypal.checkout
									.setup(
											'${merchantId}',
											{
												environment : '${paypalEnvironment}',
												container : 'idSubscribeForm',
												condition : function() {
													confirmPaypal = true;
													var immediateAlert = $(
															'#immediateEffect')
															.prop('checked');

													//console.log(""+ immediateAlert);

													if ($('#idSubscribeForm')
															.isValid()
															&& immediateAlert) {
														//Alert for immediate effect
														if (!confirm("Any unused event will be forfeited (No refund)")) {
															console
																	.log("cancel");
															confirmPaypal = false;
														} else {
															confirmPaypal = true;
															console.log("yes");
															$(
																	'#idSubscribeForm')
																	.submit();
														}
													}
													return $('#idSubscribeForm')
															.isValid()
															&& confirmPaypal;
												},
												//button: 'placeOrderBtn'
												buttons : [ {
													container : 'idButtonHolder',
													type : 'checkout',
													color : 'blue',
													size : 'medium',
													shape : 'rect'
												} ]
											});
						};
						</c:if>
						$("#idButtonHolder").click(function(e) {
							console.log("confirmPaypal :" + !confirmPaypal);
							if (!confirmPaypal) {
								e.preventDefault();
							}
						});
					});
</script>
<script src="//www.paypalobjects.com/api/checkout.js" async></script>
<script type="text/javascript" src="<c:url value="/resources/js/view/buyUserPlan.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/view/freeTrialSignup.js"/>"></script>