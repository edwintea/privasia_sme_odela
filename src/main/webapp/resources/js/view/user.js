jQuery(document).ready(function($) {
	// RFX Template
	$('.checkAllCheckbox').change(function(){
		$('.subCheckBoxes').prop('checked', $(this).prop('checked'));
		$.uniform.update();
	});
	$('.subCheckBoxes').change(function(){
		if($('.subCheckBoxes').length == $('.subCheckBoxes:checked').length){
			$('.checkAllCheckbox').prop('checked', true);
		} else {
			$('.checkAllCheckbox').prop('checked', false);
		}
		$.uniform.update();
	});
	
	// PR Checkboxes
	$('.prCheckAllCheckbox').change(function(){
		$('.prSubCheckBoxes').prop('checked', $(this).prop('checked'));
		$.uniform.update();
	});
	$('.prSubCheckBoxes').change(function(){
		if($('.prSubCheckBoxes').length == $('.prSubCheckBoxes:checked').length){
			$('.prCheckAllCheckbox').prop('checked', true);
		} else {
			$('.prCheckAllCheckbox').prop('checked', false);
		}
		$.uniform.update();
	});

	
	// request Checkboxes
	$('.requestCheckAllCheckbox').change(function(){
		$('.requestSubCheckBoxes').prop('checked', $(this).prop('checked'));
		$.uniform.update();
	});
	$('.requestSubCheckBoxes').change(function(){
		if($('.requestSubCheckBoxes').length == $('.requestSubCheckBoxes:checked').length){
			$('.requestCheckAllCheckbox').prop('checked', true);
		} else {
			$('.requestCheckAllCheckbox').prop('checked', false);
		}
		$.uniform.update();
	});
	

	
	$('#saveUser').click(function(){
		$(this).addClass('disabled');
		if(!$('#userRegistration').isValid()){
		$(this).removeClass('disabled');
		}
	});

	$("#userType").change(function(e){
		e.preventDefault();
		
		var header = $("meta[name='_csrf_header']").attr("content");
		var token = $("meta[name='_csrf']").attr("content");

		var userType = $(this).val();
		
		$.ajax({
			type : "POST",
			url : "getUserRole",
			data : {
				'userType' :userType
			},
			beforeSend : function(xhr) {
				$('#loading').show();
				xhr.setRequestHeader(header, token);
			},
			complete : function() {
				$('#loading').hide();
			},
			success : function(data) {
				console.log('data: ' + data);
				var userRoleTable = '<option value="">Select User Role</option>';
				var firstRoleId = '';
				$.each(data, function(i, item) {
					if(i == 0){
						firstRoleId = item.id ;
						$(".form-error").remove();
						$("#idUserRole").parent().removeClass("has-error");
						$("#idUserRole").removeClass("error");
						$('#idUserRole').css("border-color", "");
					}else{
						firstRoleId = '';
					}
					userRoleTable += '<option value="'+item.id+'">' + item.roleName+ '</option>'; '</li>';
				});
				$('#idUserRole').html(userRoleTable).trigger("chosen:updated");
				$('#idUserRole').val(firstRoleId).trigger("chosen:updated");
			},
			error : function(request, textStatus, errorThrown) {
				alert('Error: ' + request.getResponseHeader('error'));
			}
		});
		
		});
	
	
	
});