$("#switchToSupplierEngagementId").click(function () {
    $(this).removeClass('font-white');
    $(this).parent().addClass('current');
    $(this).addClass('font-gray');
    $("#switchToSupplierProfileId").parent().removeClass('current');
    $("#switchToSupplierProfileId").addClass('font-white');
    $("#switchToSupplierProfileId").removeClass('font-gray');
    $("#supplierProfileViewId").show();
    $("#supplierEngagementViewId").hide();
});

$("#switchToSupplierProfileId").click(function () {
    $(this).removeClass('font-white');
    $(this).parent().addClass('current');
    $(this).addClass('font-gray');
    $("#switchToSupplierEngagementId").parent().removeClass('current');
    $("#switchToSupplierEngagementId").addClass('font-white');
    $("#switchToSupplierEngagementId").removeClass('font-gray');
    $("#supplierProfileViewId").hide();
    $("#supplierEngagementViewId").show();
});

$(document).ready(function () {
    var selectedId="";

    var indCatSeleted = '';
    $(document).find('.token-input-list').prepend(indCatSeleted);
    indCatSeleted = []
    finalCategories = []
    indCatSeleted = $("#industryCategoryCodes").val() ? $("#industryCategoryCodes").val().replace('[', '').replace(']', '').split('*') : [];
    ids = [];
    ids = $("#industryCategoryCodes").val() ? $('#industryCategoryCodes').val().replace('[', '').replace(']', '').split('*') : [];
    for (var index = 0; index < indCatSeleted.length; index++) {
        data = {};
        if (indCatSeleted[index] != "") {
            data['code'] = indCatSeleted[index].split('-')[1].trim();
            data['name'] = indCatSeleted[index].split('-')[2].trim();
            data['id'] = ids[index].split('-')[0].replace(',', '').trim();
            finalCategories.push(data)
        }
    }
    $("#demo-input-local").tokenInput("searchIndustryCategories", {
        minChars: 1,
        method: 'GET',
        hintText: "Start typing to search industry categories...",
        noResultsText: "No results",
        searchingText: "Searching...",
        queryParam: "search",
        propertyToSearch: "name",
        propertyToSearchCode: "code",
        preventDuplicates: true,
        minChars: 3,
        prePopulate: finalCategories,
        readonly: true
    });
    $('#demo-input-local').attr('data-validation', 'required');
    $('#demo-input-local').attr('data-validation-error-msg-container', '#catValErr');
    $.validate({});
    if ($("#statusId").val() === 'APPROVED') {
        $('#approveRequest').hide();
        $('#rejectRequest').hide();
        $('#buyerRemarkId').prop('readonly', true);
        $('#buyerRemarkId').attr('data-validation', '');
        $('#pointerEventId').attr('style', 'pointer-events: none;');
    }
});

$("#approveRequest").click(function () {
    selectedId=$(this).data('id');

    $('#buyerRemarkId').attr('data-validation', 'length');
    $('#supplierRequestForm').isValid();
    var indCat = [];
    select = $("#token-input-demo-input-local").parent().parent().find(".token-input-token");
    select.each(function () {
        indCat.push($(this).find('input').val());
    });
    if (indCat.length <= 0) {
        event.preventDefault();
        $("#token-input-demo-input-local").closest(".token-input-list").css({
            'border-color': '#a94442'
        })
        return;
    }
    if ($('#supplierRequestForm').isValid()) {
        //var id = $("#requestId").val()
        var id = selectedId;
        $("#approveSupplierRequest").attr("request-id", id);
        $("#approveRequestModal").modal('show')
    }
});

$("#rejectRequest").click(function () {
    selectedId=$(this).data('id');

    var indCat = [];
    select = $("#token-input-demo-input-local").parent().parent().find(".token-input-token");
    select.each(function () {
        indCat.push($(this).find('input').val());
    });
    if (indCat.length <= 0) {
        event.preventDefault();
        $("#token-input-demo-input-local").closest(".token-input-list").css({
            'border-color': '#a94442'
        })
        return;
    }


    if ($('#buyerRemarkId').val() && $('#buyerRemarkId').val().length > 0) {

        $("#rejectSupplierRequest").attr("request-id", selectedId);
        $("#rejectRequestModal").modal('show')
    } else {
        $('#buyerRemarkId').attr('data-validation', 'length required');
        $('#supplierRequestForm').isValid();
    }

});

$("#closeRequest").click(function () {
    $("#closeView").click()
});

$("#rejectSupplierRequest").click(function () {
    $("#rejectRequestModal").modal('hide');
    //var requestId = $("#rejectSupplierRequest").attr("request-id");
    var requestId = selectedId;

    var header = $("meta[name='_csrf_header']").attr("content");
    var token = $("meta[name='_csrf']").attr("content");
    data = {};
    data['buyerRemark'] = $('#buyerRemarkId').val();
    data['requestId'] = requestId;
    var indCat = [];
    select = $("#token-input-demo-input-local").parent().parent().find(".token-input-token");
    select.each(function () {
        indCat.push($(this).find('input').val());
    });
    data['indCat'] = indCat;
    data['requestId']=requestId;

    $.ajax({
        url: getContextPath() + "/buyer/rejectSupplierRequest/" + requestId,
        type: "POST",
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(data),
        beforeSend: function (xhr) {
            $('#loading').show();
            xhr.setRequestHeader(header, token);
        },
        success: function (request, status, xhr) {
            var success = xhr.getResponseHeader('success');
            $('p[id=idGlobalSuccessMessage]').html(success);
            $('div[id=idGlobalSuccess]').show();
            $('div[id=idGlobalError]').hide();
            $('#supplierStatusId').html(request.status)
            $('#rejectRequest').hide();
            supplierFormsData1.ajax.reload();

            window.location=getContextPath() + "/buyer/importSupplier"
        },
        error: function (request, status, xhr) {
            $('p[id=idGlobalErrorMessage]').html(xhr.getResponseHeader('error'));
            $('div[id=idGlobalError]').show();
            $('div[id=idGlobalSuccess]').hide();
        },
        complete: function () {
            $('#loading').hide();
        }
    });

});

$("#approveSupplierRequest").click(function () {
    $("#approveRequestModal").modal('hide');
    var requestId = $("#approveSupplierRequest").attr("request-id");
    var header = $("meta[name='_csrf_header']").attr("content");
    var token = $("meta[name='_csrf']").attr("content");
    data = {};
    data['buyerRemark'] = $('#buyerRemarkId').val();
    var indCat = [];
    select = $("#token-input-demo-input-local").parent().parent().find(".token-input-token");
    select.each(function () {
        indCat.push($(this).find('input').val());
    });
    data['indCat'] = indCat;
    $.ajax({
        url: getContextPath() + "/buyer/acceptSupplierRequest/" + selectedId,
        type: "POST",
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(data),
        beforeSend: function (xhr) {
            $('#loading').show();
            xhr.setRequestHeader(header, token);
        },
        success: function (request, status, xhr) {
            var success = xhr.getResponseHeader('success');
            $('p[id=idGlobalSuccessMessage]').html(success);
            $('div[id=idGlobalSuccess]').show();
            $('div[id=idGlobalError]').hide();
            $('#supplierStatusId').html(request.status)
            $('#approveRequest').hide();
            $('#rejectRequest').hide();
            $('#buyerRemarkId').prop('readonly', true);
            $('#pointerEventId').attr('style', 'pointer-events: none;');
            $('#buyerRemarkId').attr('data-validation', '');

            //window.location.href = getContextPath() + "/supplierSignupList";
        },
        error: function (request, status, xhr) {
            $('p[id=idGlobalErrorMessage]').html(xhr.getResponseHeader('error'));
            $('div[id=idGlobalError]').show();
            $('div[id=idGlobalSuccess]').hide();
        },
        complete: function () {
            $('#loading').hide();
        }
    });
});
