$(document).ready(function () {

	$('#saveDir').hide();
	$('#cancelSaveDir').hide();
	if(document.getElementById("radioYes").value=="true"){
		document.getElementById("radioYes").checked=true;
	}else {
		document.getElementById("radioNo").checked=true;
	}
	$(document).on("change", "#icAttachment", function() {
		if($('#icAttachment') && $('#icAttachment')[0] && $('#icAttachment')[0].files && $('#icAttachment')[0].files[0]) {
			$(".show_name_icAttachment").html($('#icAttachment')[0].files[0].name);
		}
	});
});

$(document).delegate('#showConfirmDeletePopUp', 'click', function (e) {
    $('div[id=idGlobalError]').hide();
    $('div[id=idGlobalSuccess]').hide();
    $('#confirmDeleteDirector').removeClass('fade');
    $('#confirmDeleteDirector').removeClass('hideModal');
    $('#confirmDeleteDirector').addClass('showModal');
    var deleteId = $(this).attr('delete-id');
    var deleteName = $(this).attr('delete-name');
    $('#confirmDeleteDirector').attr('delete-id', deleteId);
    $('#confirmDeleteDirector').attr('delete-name', deleteName);

});

$('#confDelDir').click(function () {
    $('div[id=idGlobalError]').hide();
    $('div[id=idGlobalSuccess]').hide();
    var deleteId = $('#confirmDeleteDirector').attr('delete-id');
    var deleteName = $('#confirmDeleteDirector').attr('delete-name');
    $('#confirmDeleteDirector').removeClass('showModal');
    var header = $("meta[name='_csrf_header']").attr("content");
    var token = $("meta[name='_csrf']").attr("content");
    $.ajax({
        url: getContextPath() + "/removeDirector",
        type: "GET",
        data: {
            id: deleteId,
            name: deleteName
        },
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
            $('#loading').show();
        },
        success: function (data, textStatus, request) {
            var table = '';
            var info = request.getResponseHeader('error');
            if (info) {
                $('p[id=idGlobalSuccessMessage]').html(info.split(",").join("<br/>"));
                $('div[id=idGlobalSuccess]').show();
                if (document.querySelectorAll("#idGlobalSuccess")[0]) {
                    document.querySelectorAll("#idGlobalSuccess")[0].style.display = "none"
                }
            }
            $.each(data, function (i, item) {
                table += '<tr>' +
                    '<td><div><span class="col-sm-6 p-l-0 no-padding" id="showConfirmDeletePopUp" delete-id="' + item.id + '" delete-name="' + item.directorName + '"><a><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></a></span>' +
                    '<span class="col-sm-6 p-l-0 no-padding" id="editDirector" edit-id=' + item.id + '><a><i class="fa fa-edit fa-lg" aria-hidden="true"></i></a></span></div></td>' +
                    '<td>' + (i + 1) + '</td>' +
                    '<td>' + item.directorName + '</td>' +
                    '<td>' + item.idNumber + '</td>' +
                    '<td><a href="' + getContextPath() + '/downloadIcCopy/' + item.id + '">' + item.identificationCardFileName + '</a></td>' +
                    '<td>' + item.dirType + '</td>' +
                    '<td>' + (item.dirEmail != undefined ? item.dirEmail : item.dirEmail != null ? item.dirEmail : "") + '</td>' +
                    '<td>' + (item.dirContact != undefined ? item.dirContact : item.dirContact != null ? item.dirContact : "") + '</td>' +
                    '</tr>'
            });
            $('#directorsDisplay tbody').html(table);
        },
        error: function (request, textStatus, errorThrown) {
            $('p[id=idGlobalErrorMessage]').html(request.getResponseHeader('error').split(",").join("<br/>"));
            $('div[id=idGlobalError]').show(); if (document.querySelectorAll("#idGlobalError")[0]) {
                document.querySelectorAll("#idGlobalError")[0].style.display = "none"
            }
            $('#loading').hide();
        },
        complete: function () {
            $('#loading').hide();
        }
    });
})

$('#confirmDeleteDirectorClose').click(function () {
    $('#confirmDeleteDirector').removeClass('showModal');
    $('#confirmDeleteDirector').addClass('hideModal fade');
})

$('#confirmDeleteDirectorDismiss').click(function () {
    $('#confirmDeleteDirector').removeClass('showModal');
    $('#confirmDeleteDirector').addClass('hideModal fade');
})

$(document).delegate('#editDirector', 'click', function (e) {
    $('div[id=idGlobalError]').hide();
    $('div[id=idGlobalSuccess]').hide();
    var header = $("meta[name='_csrf_header']").attr("content");
    var token = $("meta[name='_csrf']").attr("content");
    var editId = $(this).attr('edit-id');
    $.ajax({
        url: getContextPath() + "/editDirector",
        type: "GET",
        data: {
            id: editId,
        },
        beforeSend: function (xhr) {
            xhr.setRequestHeader(header, token);
            $('#loading').show();
        },
        success: function (data, textStatus, request) {
            $('#directorId').val(data.id);
            $('#idDirectorName').val(data.directorName);
            // $('#idType').val(data.idType).trigger("chosen:updated");
            $('#idNumber').val(data.idNumber);
            $("#idDirType").val(data.dirType).trigger("chosen:updated");
            $('#idDirEmail').val(data.dirEmail);
            $('#idDirContact').val(data.dirContact);
            // $('#saveDir').prop("disabled", false);
            // $('#addNewDir').prop("disabled", true);
            // $('#saveDir').addClass('btn-primary');
            // $('#addNewDir').addClass('disabled')
            // $('#saveDir').removeClass('disabled');
            // $('#addNewDir').removeClass('btn-black');
            
            $('#boardOfDirectorForm').isValid();
            
			$('#saveDir').show();
			$('#cancelSaveDir').show();
			$('#addNewDir').hide();

        },
        error: function (request, textStatus, errorThrown) {
            $('p[id=idGlobalErrorMessage]').html(request.getResponseHeader('error').split(",").join("<br/>"));
            $('div[id=idGlobalError]').show();
            $('#loading').hide();
        },
        complete: function () {
            $('#loading').hide();
        }
    });

});

$('#cancelSaveDir').click(function () {
	$('#addNewDir').show();
	$('#addNewDir').removeClass('disabled');
	$('#saveDir').hide();
	$('#cancelSaveDir').hide();
	// $('#saveDir').addClass('disabled');
	$('#boardOfDirectorForm').trigger("reset");
	$("#idDirType").val(null).trigger("chosen:updated");
	$(".show_name_icAttachment").html('');
	$('#Load_File-error-icAttachment').removeClass("has-error");
	$('#Load_File-error-icAttachment').html('');
	$('#directorId').val('');
	
});



$('#addNewDir, #saveDir').on('click', function (e) {
	e.preventDefault();
	
	if (!$('#boardOfDirectorForm').isValid()) {
		return false;
	}
	if ($('#idType').val() && $('#idType').val().length === 0) {
		return false;
	}
	if ($('#idDirType').val()
		&& $('#idDirType').val().length === 0) {
		return false;
	}
	$('#boardOfDirectorForm').submit();
	$('#loading').show();
	$(this).attr('disabled', 'true');
	$(this).addClass('disabled');
	return true;
	
});

$('#addNewDirXX').click(function (e) {

	e.preventDefault();
	
	if (!$('#boardOfDirectorForm').isValid()) {
		$('#icAttachment').val('').change();
		$('div[id=Load_File-error-icAttachment]').show();
		return false;
	}

	if ($('#idType').val() && $('#idType').val().length === 0) {
		return false;
	}

	if ($('#idDirType').val()
		&& $('#idDirType').val().length === 0) {
		return false;
	}
	
	return true;
	
});



$('#mandatoryDocumentFormSSMUpload').click(function (e) {
	e.preventDefault();
	$('div[id=idGlobalError]').hide();
	$('div[id=idGlobalSuccess]').hide();
	if (!$('#mandatoryDocumentFormSSMForm').isValid()) {
		return false;
	}
	if ($('#mandatoryDocumentFormSSMFile').val().length == 0) {
		$('p[id=idGlobalErrorMessage]').html(
			"Please select a file for upload.");
		$('div[id=idGlobalError]').show();
		return;
	}
	var header = $("meta[name='_csrf_header']").attr("content");
	var token = $("meta[name='_csrf']").attr("content");
	var oMyForm = new FormData();
	oMyForm.append("mandatoryDocumentSSMFile", $('#mandatoryDocumentFormSSMFile')[0].files[0]);
	$.ajax({
		url: getContextPath()
			+ "/mandatoryDocumentSSMFileUpload",
		data: oMyForm,
		type: "POST",
		enctype: 'multipart/form-data',
		processData: false,
		contentType: false,
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
			$('#loading').show();
		},
		success: function (data, textStatus, request) {
			document.getElementById("mandatoryDocumentFormSSMFile").value = "";
			$("#idMandatoryDocumentFormSSMSpan").html('');
			var info = request.getResponseHeader('success');
			$('p[id=idGlobalSuccessMessage]').html(info.split(",").join("<br/>"));
			$('div[id=idGlobalSuccess]').show();
			$("#mandatoryDocumentFormSSMUpload").removeClass('btn-blue').addClass('btn-gray');
			var table = '<tr>'
				+ '<td><a class="removeMandatoryDocumentFormSSMFile" href="#" docType="FORM9" removeDocumentId="' + data.id + '" removeDocumentFileName="' + data.ssmFileName + '"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></a></i></td>'
				+ '<td><form:form method="GET">'
				+ '<a class="word-break" href="'
				+ getContextPath()
				+ '/downloadSupplierOrganizationDocuments/FORM9/'
				+ data.id
				+ '" mandatory-document-form9-id="' + data.id + '">'
				+ data.ssmFileName
				+ '</a>'
				+ '<form:form>'
				+ '</td>'
				+ '<td><span class="col-sm-10 no-padding">'
				+ data.ssmUploadDate
				+ '</span></td>'
				+ '</tr>';
			$('#mandatoryDocumentFormSSMDisplay tbody').html(table);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		},
		error: function (request, textStatus, errorThrown) {
			$('p[id=idGlobalErrorMessage]').html(request.getResponseHeader('error').split(",").join("<br/>"));
			$('div[id=idGlobalError]').show();
			$('#loading').hide();
		},
		complete: function () {
			$('#loading').hide();
		}
	});

});

$('#mandatoryDocumentForm9Upload').click(function (e) {
	e.preventDefault();
	$('div[id=idGlobalError]').hide();
	$('div[id=idGlobalSuccess]').hide();
	if (!$('#mandatoryDocumentForm9Form').isValid()) {
		return false;
	}
	if ($('#mandatoryDocumentForm9File').val().length == 0) {
		$('p[id=idGlobalErrorMessage]').html(
			"Please select a file for upload.");
		$('div[id=idGlobalError]').show();
		return;
	}
	var header = $("meta[name='_csrf_header']").attr("content");
	var token = $("meta[name='_csrf']").attr("content");
	var oMyForm = new FormData();
	oMyForm.append("mandatoryDocumentForm9File", $('#mandatoryDocumentForm9File')[0].files[0]);
	$.ajax({
		url: getContextPath()
			+ "/mandatoryDocumentForm9FileUpload",
		data: oMyForm,
		type: "POST",
		enctype: 'multipart/form-data',
		processData: false,
		contentType: false,
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
			$('#loading').show();
		},
		success: function (data, textStatus, request) {
			document.getElementById("mandatoryDocumentForm9File").value = "";
			$('#file-error-form9').removeClass('has-error');
			$('#file-error-form9').html('');
			$("#idMandatoryDocumentForm9Span").html('');
			var info = request.getResponseHeader('success');
			$('p[id=idGlobalSuccessMessage]').html(info.split(",").join("<br/>"));
			$('div[id=idGlobalSuccess]').show();
			$("#mandatoryDocumentForm9Upload").removeClass('btn-blue').addClass('btn-gray');
			var table = '<tr>'
				+ '<td><a class="removeMandatoryDocumentForm9File" href="#" docType="FORM9" removeDocumentId="' + data.id + '" removeDocumentFileName="' + data.form9FileName + '"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></a></i></td>'
				+ '<td><form:form method="GET">'
				+ '<a class="word-break" href="'
				+ getContextPath()
				+ '/downloadSupplierOrganizationDocuments/FORM9/'
				+ data.id
				+ '" mandatory-document-form9-id="' + data.id + '">'
				+ data.form9FileName
				+ '</a>'
				+ '<form:form>'
				+ '</td>'
				+ '<td><span class="col-sm-10 no-padding">'
				+ data.form9UploadDate
				+ '</span></td>'
				+ '</tr>';
			$('#mandatoryDocumentForm9Display tbody').html(table);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		},
		error: function (request, textStatus, errorThrown) {
			$('p[id=idGlobalErrorMessage]').html(request.getResponseHeader('error').split(",").join("<br/>"));
			$('div[id=idGlobalError]').show();
			$('#loading').hide();
		},
		complete: function () {
			$('#loading').hide();
		}
	});

});

$('#mandatoryDocumentForm24Upload').click(function (e) {
	e.preventDefault();
	$('div[id=idGlobalError]').hide();
	$('div[id=idGlobalSuccess]').hide();
	if (!$('#mandatoryDocumentForm24Form').isValid()) {
		return false;
	}
	if ($('#mandatoryDocumentForm24File').val().length == 0) {
		$('p[id=idGlobalErrorMessage]').html(
			"Please select a file for upload.");
		$('div[id=idGlobalError]').show();
		return;
	}
	var header = $("meta[name='_csrf_header']").attr("content");
	var token = $("meta[name='_csrf']").attr("content");
	var oMyForm = new FormData();
	oMyForm.append("mandatoryDocumentForm24File", $('#mandatoryDocumentForm24File')[0].files[0]);
	$.ajax({
		url: getContextPath()
			+ "/mandatoryDocumentForm24FileUpload",
		data: oMyForm,
		type: "POST",
		enctype: 'multipart/form-data',
		processData: false,
		contentType: false,
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
			$('#loading').show();
		},
		success: function (data, textStatus, request) {
			document.getElementById("mandatoryDocumentForm24File").value = "";
			$("#idMandatoryDocumentForm24Span").html('');
			$('#file-error-form24').removeClass('has-error');
			$('#file-error-form24').html('');
			var info = request.getResponseHeader('success');
			$('p[id=idGlobalSuccessMessage]').html(info.split(",").join("<br/>"));
			$('div[id=idGlobalSuccess]').show();
			$("#mandatoryDocumentForm24Upload").removeClass('btn-blue').addClass('btn-gray');
			var table = '<tr>'
				+ '<td><a class="removeMandatoryDocumentForm24File" href="#" docType="FORM24" removeDocumentId="' + data.id + '" removeDocumentFileName="' + data.form24FileName + '"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></a></i></td>'
				+ '<td><form:form method="GET">'
				+ '<a class="word-break" href="'
				+ getContextPath()
				+ '/downloadSupplierOrganizationDocuments/FORM24/'
				+ data.id
				+ '" mandatory-document-form24-id="' + data.id + '">'
				+ data.form24FileName
				+ '</a>'
				+ '<form:form>'
				+ '</td>'
				+ '<td><span class="col-sm-10 no-padding">'
				+ data.form24UploadDate
				+ '</span></td>'
				+ '</tr>';
			$('#mandatoryDocumentForm24Display tbody').html(table);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		},
		error: function (request, textStatus, errorThrown) {
			$('p[id=idGlobalErrorMessage]').html(request.getResponseHeader('error').split(",").join("<br/>"));
			$('div[id=idGlobalError]').show();
			$('#loading').hide();
		},
		complete: function () {
			$('#loading').hide();
		}
	});

});

$('#mandatoryDocumentForm49Upload').click(function (e) {
	e.preventDefault();
	$('div[id=idGlobalError]').hide();
	$('div[id=idGlobalSuccess]').hide();
	if (!$('#mandatoryDocumentForm49Form').isValid()) {
		return false;
	}
	if ($('#mandatoryDocumentForm49File').val().length == 0) {
		$('p[id=idGlobalErrorMessage]').html(
			"Please select a file for upload.");
		$('div[id=idGlobalError]').show();
		return;
	}
	var header = $("meta[name='_csrf_header']").attr("content");
	var token = $("meta[name='_csrf']").attr("content");
	var oMyForm = new FormData();
	oMyForm.append("mandatoryDocumentForm49File", $('#mandatoryDocumentForm49File')[0].files[0]);
	$.ajax({
		url: getContextPath()
			+ "/mandatoryDocumentForm49FileUpload",
		data: oMyForm,
		type: "POST",
		enctype: 'multipart/form-data',
		processData: false,
		contentType: false,
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
			$('#loading').show();
		},
		success: function (data, textStatus, request) {
			document.getElementById("mandatoryDocumentForm49File").value = "";
			$('#file-error-form49').removeClass('has-error');
			$('#file-error-form49').html('');
			$("#idMandatoryDocumentForm49Span").html('');
			var info = request.getResponseHeader('success');
			$('p[id=idGlobalSuccessMessage]').html(info.split(",").join("<br/>"));
			$('div[id=idGlobalSuccess]').show();
			$("#mandatoryDocumentForm49Upload").removeClass('btn-blue').addClass('btn-gray');
			var table = '<tr>'
				+ '<td><a class="removeMandatoryDocumentForm49File" href="#" docType="FORM49" removeDocumentId="' + data.id + '" removeDocumentFileName="' + data.form49FileName + '"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></a></i></td>'
				+ '<td><form:form method="GET">'
				+ '<a class="word-break" href="'
				+ getContextPath()
				+ '/downloadSupplierOrganizationDocuments/FORM49/'
				+ data.id
				+ '" mandatory-document-form49-id="' + data.id + '">'
				+ data.form49FileName
				+ '</a>'
				+ '<form:form>'
				+ '</td>'
				+ '<td><span class="col-sm-10 no-padding">'
				+ data.form49UploadDate
				+ '</span></td>'
				+ '</tr>';
			$('#mandatoryDocumentForm49Display tbody').html(table);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		},
		error: function (request, textStatus, errorThrown) {
			$('p[id=idGlobalErrorMessage]').html(request.getResponseHeader('error').split(",").join("<br/>"));
			$('div[id=idGlobalError]').show();
			$('#loading').hide();
		},
		complete: function () {
			$('#loading').hide();
		}
	});

});

$('#mandatoryDocumentFormMofUpload').click(function (e) {
	e.preventDefault();
	$('div[id=idGlobalError]').hide();
	$('div[id=idGlobalSuccess]').hide();
	if (!$('#mandatoryDocumentFormMofForm').isValid()) {
		return false;
	}
	if ($('#mandatoryDocumentFormMofFile').val().length == 0) {
		$('p[id=idGlobalErrorMessage]').html(
			"Please select a file for upload.");
		$('div[id=idGlobalError]').show();
		return;
	}
	var header = $("meta[name='_csrf_header']").attr("content");
	var token = $("meta[name='_csrf']").attr("content");
	var oMyForm = new FormData();
	oMyForm.append("mandatoryDocumentFormMofFile", $('#mandatoryDocumentFormMofFile')[0].files[0]);
	$.ajax({
		url: getContextPath()
			+ "/mandatoryDocumentFormMofFileUpload",
		data: oMyForm,
		type: "POST",
		enctype: 'multipart/form-data',
		processData: false,
		contentType: false,
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
			$('#loading').show();
		},
		success: function (data, textStatus, request) {
			document.getElementById("mandatoryDocumentFormMofFile").value = "";
			$("#idMandatoryDocumentFormMofSpan").html('');
			$('#file-error-formMof').removeClass('has-error');
			$('#file-error-formMof').html('');
			var info = request.getResponseHeader('success');
			$('p[id=idGlobalSuccessMessage]').html(info.split(",").join("<br/>"));
			$('div[id=idGlobalSuccess]').show();
			$("#mandatoryDocumentFormMofUpload").removeClass('btn-blue').addClass('btn-gray');
			var table = '<tr>'
				+ '<td><a class="removeMandatoryDocumentFormMofFile" href="#" docType="FORMMOF" removeDocumentId="' + data.id + '" removeDocumentFileName="' + data.mofFileName + '"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></a></i></td>'
				+ '<td><form:form method="GET">'
				+ '<a class="word-break" href="'
				+ getContextPath()
				+ '/downloadSupplierOrganizationDocuments/FORMMOF/'
				+ data.id
				+ '" mandatory-document-formMof-id="' + data.id + '">'
				+ data.mofFileName
				+ '</a>'
				+ '<form:form>'
				+ '</td>'
				+ '<td><span class="col-sm-10 no-padding">'
				+ data.mofUploadDate
				+ '</span></td>'
				+ '</tr>';
			$('#mandatoryDocumentFormMofDisplay tbody').html(table);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		},
		error: function (request, textStatus, errorThrown) {
			$('p[id=idGlobalErrorMessage]').html(request.getResponseHeader('error').split(",").join("<br/>"));
			$('div[id=idGlobalError]').show();
			$('#loading').hide();
		},
		complete: function () {
			$('#loading').hide();
		}
	});

});


$('#mandatoryDocumentFormVDFUpload').click(function (e) {
	e.preventDefault();
	$('div[id=idGlobalError]').hide();
	$('div[id=idGlobalSuccess]').hide();
	if (!$('#mandatoryDocumentFormVDFForm').isValid()) {
		return false;
	}
	if ($('#mandatoryDocumentFormVDFFile').val().length == 0) {
		$('p[id=idGlobalErrorMessage]').html(
			"Please select a file for upload.");
		$('div[id=idGlobalError]').show();
		return;
	}
	var header = $("meta[name='_csrf_header']").attr("content");
	var token = $("meta[name='_csrf']").attr("content");
	var oMyForm = new FormData();
	oMyForm.append("mandatoryDocumentVDFFile", $('#mandatoryDocumentFormVDFFile')[0].files[0]);
	$.ajax({
		url: getContextPath()
			+ "/mandatoryDocumentVDFFileUpload",
		data: oMyForm,
		type: "POST",
		enctype: 'multipart/form-data',
		processData: false,
		contentType: false,
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
			$('#loading').show();
		},
		success: function (data, textStatus, request) {
			document.getElementById("mandatoryDocumentFormVDFFile").value = "";
			$("#idMandatoryDocumentFormVDFSpan").html('');
			var info = request.getResponseHeader('success');
			$('p[id=idGlobalSuccessMessage]').html(info.split(",").join("<br/>"));
			$('div[id=idGlobalSuccess]').show();
			$("#mandatoryDocumentFormVDFUpload").removeClass('btn-blue').addClass('btn-gray');
			var table = '<tr>'
				+ '<td><a class="removeMandatoryDocumentFormVDFFile" href="#" docType="VDF" removeDocumentId="' + data.id + '" removeDocumentFileName="' + data.vdfFileName + '"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></a></i></td>'
				+ '<td><form:form method="GET">'
				+ '<a class="word-break" href="'
				+ getContextPath()
				+ '/downloadSupplierOrganizationDocuments/VDF/'
				+ data.id
				+ '" mandatory-document-vdf-id="' + data.id + '">'
				+ data.vdfFileName
				+ '</a>'
				+ '<form:form>'
				+ '</td>'
				+ '<td><span class="col-sm-10 no-padding">'
				+ data.vdfUploadDate
				+ '</span></td>'
				+ '</tr>';
			$('#mandatoryDocumentFormVDFDisplay tbody').html(table);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		},
		error: function (request, textStatus, errorThrown) {
			$('p[id=idGlobalErrorMessage]').html(request.getResponseHeader('error').split(",").join("<br/>"));
			$('div[id=idGlobalError]').show();
			$('#loading').hide();
		},
		complete: function () {
			$('#loading').hide();
		}
	});

});

$('#mandatoryDocumentFormCCDFUpload').click(function (e) {
	e.preventDefault();
	$('div[id=idGlobalError]').hide();
	$('div[id=idGlobalSuccess]').hide();
	if (!$('#mandatoryDocumentFormCCDFForm').isValid()) {
		return false;
	}
	if ($('#mandatoryDocumentFormCCDFFile').val().length == 0) {
		$('p[id=idGlobalErrorMessage]').html(
			"Please select a file for upload.");
		$('div[id=idGlobalError]').show();
		return;
	}
	var header = $("meta[name='_csrf_header']").attr("content");
	var token = $("meta[name='_csrf']").attr("content");
	var oMyForm = new FormData();
	oMyForm.append("mandatoryDocumentCCDFFile", $('#mandatoryDocumentFormCCDFFile')[0].files[0]);
	$.ajax({
		url: getContextPath()
			+ "/mandatoryDocumentCCDFFileUpload",
		data: oMyForm,
		type: "POST",
		enctype: 'multipart/form-data',
		processData: false,
		contentType: false,
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
			$('#loading').show();
		},
		success: function (data, textStatus, request) {
			document.getElementById("mandatoryDocumentFormCCDFFile").value = "";
			$("#idMandatoryDocumentFormCCDFSpan").html('');
			var info = request.getResponseHeader('success');
			$('p[id=idGlobalSuccessMessage]').html(info.split(",").join("<br/>"));
			$('div[id=idGlobalSuccess]').show();
			$("#mandatoryDocumentFormCCDFUpload").removeClass('btn-blue').addClass('btn-gray');
			var table = '<tr>'
				+ '<td><a class="removeMandatoryDocumentFormCCDF" href="#" docType="CCDF" removeDocumentId="' + data.id + '" removeDocumentFileName="' + data.ccdfFileName + '"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></a></i></td>'
				+ '<td><form:form method="GET">'
				+ '<a class="word-break" href="'
				+ getContextPath()
				+ '/downloadSupplierOrganizationDocuments/CCDF/'
				+ data.id
				+ '" mandatory-document-ccdf-id="' + data.id + '">'
				+ data.ccdfFileName
				+ '</a>'
				+ '<form:form>'
				+ '</td>'
				+ '<td><span class="col-sm-10 no-padding">'
				+ data.ccdfUploadDate
				+ '</span></td>'
				+ '</tr>';
			$('#mandatoryDocumentFormCCDFDisplay tbody').html(table);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		},
		error: function (request, textStatus, errorThrown) {
			$('p[id=idGlobalErrorMessage]').html(request.getResponseHeader('error').split(",").join("<br/>"));
			$('div[id=idGlobalError]').show();
			$('#loading').hide();
		},
		complete: function () {
			$('#loading').hide();
		}
	});

});

$(document).delegate('.radioYes', 'click',function (e) {

	let radioYes = document.getElementById("radioYes");
	radioYes.checked = true;
	let radioNo = document.getElementById("radioNo");
	radioNo.checked = false;
	$('#mandatoryDocumentFormODELAUpload').prop('required',true);
});
$(document).delegate('.radioNo', 'click',function (e) {

	$('div[id=idGlobalError]').hide();
	$('div[id=idGlobalSuccess]').hide();
	let radioYes = document.getElementById("radioYes");
	radioYes.checked = false;
	let radioNo = document.getElementById("radioNo");
	radioNo.checked = true;

	var header = $("meta[name='_csrf_header']").attr("content");
	var token = $("meta[name='_csrf']").attr("content");
	var oMyForm = new FormData();
	oMyForm.append("odelaMerchant",false); // need to wait for adib radio check
	$.ajax({
		url: getContextPath()
			+ "/mandatoryDocumentODELAFileUploadStatus",
		data: oMyForm,
		type: "POST",
		enctype: 'multipart/form-data',
		processData: false,
		contentType: false,
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
			$('#loading').show();
		},
		success: function (data, textStatus, request) {
			document.getElementById("mandatoryDocumentFormODELAFile").value = "";
			$("#idMandatoryDocumentFormODELASpan").html('');
			var info = request.getResponseHeader('success');
			$('p[id=idGlobalSuccessMessage]').html(info.split(",").join("<br/>"));
			$('div[id=idGlobalSuccess]').show();
			$("#mandatoryDocumentFormODELAUpload").removeClass('btn-blue').addClass('btn-gray');
			var table = '<tr></tr>';
			$('#mandatoryDocumentFormODELADisplay tbody').html(table);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		},
		error: function (request, textStatus, errorThrown) {
			$('p[id=idGlobalErrorMessage]').html(request.getResponseHeader('error').split(",").join("<br/>"));
			$('div[id=idGlobalError]').show();
			$('#loading').hide();
		},
		complete: function () {
			$('#loading').hide();
		}
	});
});
$('#mandatoryDocumentFormODELAUpload').click(function (e) {
	e.preventDefault();
	$('div[id=idGlobalError]').hide();
	$('div[id=idGlobalSuccess]').hide();
	if (!$('#mandatoryDocumentFormODELAForm').isValid()) {
		return false;
	}
	if ($('#mandatoryDocumentFormODELAFile').val().length == 0) {
		$('p[id=idGlobalErrorMessage]').html(
			"Please select a file for upload.");
		$('div[id=idGlobalError]').show();
		return;
	}
	var header = $("meta[name='_csrf_header']").attr("content");
	var token = $("meta[name='_csrf']").attr("content");
	var oMyForm = new FormData();
	oMyForm.append("odelaMerchant",true); // need to wait for adib radio check
	oMyForm.append("mandatoryDocumentODELAFile", $('#mandatoryDocumentFormODELAFile')[0].files[0]);
	$.ajax({
		url: getContextPath()
			+ "/mandatoryDocumentODELAFileUpload",
		data: oMyForm,
		type: "POST",
		enctype: 'multipart/form-data',
		processData: false,
		contentType: false,
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
			$('#loading').show();
		},
		success: function (data, textStatus, request) {
			document.getElementById("mandatoryDocumentFormODELAFile").value = "";
			$("#idMandatoryDocumentFormODELASpan").html('');
			var info = request.getResponseHeader('success');
			$('p[id=idGlobalSuccessMessage]').html(info.split(",").join("<br/>"));
			$('div[id=idGlobalSuccess]').show();
			$("#mandatoryDocumentFormODELAUpload").removeClass('btn-blue').addClass('btn-gray');
			var table = '<tr>'
				+ '<td><a class="removeMandatoryDocumentFormODELAFile" href="#" docType="ODELA" removeDocumentId="' + data.id + '" removeDocumentFileName="' + data.odelaFileName + '"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></a></i></td>'
				+ '<td><form:form method="GET">'
				+ '<a class="word-break" href="'
				+ getContextPath()
				+ '/downloadSupplierOrganizationDocuments/ODELA/'
				+ data.id
				+ '" mandatory-document-odela-id="' + data.id + '">'
				+ data.odelaFileName
				+ '</a>'
				+ '<form:form>'
				+ '</td>'
				+ '<td><span class="col-sm-10 no-padding">'
				+ data.odelaUploadDate
				+ '</span></td>'
				+ '</tr>';
			$('#mandatoryDocumentFormODELADisplay tbody').html(table);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		},
		error: function (request, textStatus, errorThrown) {
			$('p[id=idGlobalErrorMessage]').html(request.getResponseHeader('error').split(",").join("<br/>"));
			$('div[id=idGlobalError]').show();
			$('#loading').hide();
		},
		complete: function () {
			$('#loading').hide();
		}
	});

});

$('#mandatoryDocumentFormSSMFile').change(function () {
	if($('#mandatoryDocumentFormSSMFile') && $('#mandatoryDocumentFormSSMFile')[0] && $('#mandatoryDocumentFormSSMFile')[0].files && $('#mandatoryDocumentFormSSMFile')[0].files[0]) {
		$("#idMandatoryDocumentFormSSMSpan").html($('#mandatoryDocumentFormSSMFile')[0].files[0].name);
	}
	$("#mandatoryDocumentFormSSMUpload").removeClass('btn-gray').addClass('btn-blue');
});
$('#mandatoryDocumentForm9File').change(function () {
	if($('#mandatoryDocumentForm9File') && $('#mandatoryDocumentForm9File')[0] && $('#mandatoryDocumentForm9File')[0].files && $('#mandatoryDocumentForm9File')[0].files[0]) {
		$("#idMandatoryDocumentForm9Span").html($('#mandatoryDocumentForm9File')[0].files[0].name);
	}
	$("#mandatoryDocumentForm9Upload").removeClass('btn-gray').addClass('btn-blue');
});

$('#mandatoryDocumentForm24File').change(function () {
	if($('#mandatoryDocumentForm24File') && $('#mandatoryDocumentForm24File')[0] && $('#mandatoryDocumentForm24File')[0].files && $('#mandatoryDocumentForm24File')[0].files[0]) {
		$("#idMandatoryDocumentForm24Span").html($('#mandatoryDocumentForm24File')[0].files[0].name);
	}
	$("#mandatoryDocumentForm24Upload").removeClass('btn-gray').addClass('btn-blue');
});

$('#mandatoryDocumentForm49File').change(function () {
	if($('#mandatoryDocumentForm49File') && $('#mandatoryDocumentForm49File')[0] && $('#mandatoryDocumentForm49File')[0].files && $('#mandatoryDocumentForm49File')[0].files[0]) {
		$("#idMandatoryDocumentForm49Span").html($('#mandatoryDocumentForm49File')[0].files[0].name);
	}
	$("#mandatoryDocumentForm49Upload").removeClass('btn-gray').addClass('btn-blue');
});

$('#mandatoryDocumentFormMofFile').change(function () {
	if($('#mandatoryDocumentFormMofFile') && $('#mandatoryDocumentFormMofFile')[0] && $('#mandatoryDocumentFormMofFile')[0].files && $('#mandatoryDocumentFormMofFile')[0].files[0]) {
		$("#idMandatoryDocumentFormMofSpan").html($('#mandatoryDocumentFormMofFile')[0].files[0].name);
	}
	$("#mandatoryDocumentFormMofUpload").removeClass('btn-gray').addClass('btn-blue');
});
$('#mandatoryDocumentFormVDFFile').change(function () {
	if($('#mandatoryDocumentFormVDFFile') && $('#mandatoryDocumentFormVDFFile')[0] && $('#mandatoryDocumentFormVDFFile')[0].files && $('#mandatoryDocumentFormVDFFile')[0].files[0]) {
		$("#idMandatoryDocumentFormVDFSpan").html($('#mandatoryDocumentFormVDFFile')[0].files[0].name);
	}
	$("#mandatoryDocumentFormVDFUpload").removeClass('btn-gray').addClass('btn-blue');
});

$('#mandatoryDocumentFormCCDFFile').change(function () {
	if($('#mandatoryDocumentFormCCDFFile') && $('#mandatoryDocumentFormCCDFFile')[0] && $('#mandatoryDocumentFormCCDFFile')[0].files && $('#mandatoryDocumentFormCCDFFile')[0].files[0]) {
		$("#idMandatoryDocumentFormCCDFSpan").html($('#mandatoryDocumentFormCCDFFile')[0].files[0].name);
	}
	$("#mandatoryDocumentFormCCDFUpload").removeClass('btn-gray').addClass('btn-blue');
});

$('#mandatoryDocumentFormODELAFile').change(function () {
	if($('#mandatoryDocumentFormODELAFile') && $('#mandatoryDocumentFormODELAFile')[0] && $('#mandatoryDocumentFormODELAFile')[0].files && $('#mandatoryDocumentFormODELAFile')[0].files[0]) {
		$("#idMandatoryDocumentFormODELASpan").html($('#mandatoryDocumentFormODELAFile')[0].files[0].name);
	}
	$("#mandatoryDocumentFormODELAUpload").removeClass('btn-gray').addClass('btn-blue');
});


$(document).delegate('.removeMandatoryDocumentForm9File', 'click', function (e) {
	e.preventDefault();
	$('div[id=idGlobalError]').hide();
	$('div[id=idGlobalSuccess]').hide();

	var header = $("meta[name='_csrf_header']").attr("content");
	var token = $("meta[name='_csrf']").attr("content");
	var removeOtherId = $(this).attr('removeDocumentId');
	var otherCredFile = $(this).attr('removeDocumentFileName');
	var docType = $(this).attr('docType');
	$.ajax({
		url: getContextPath() + "/removeSupplierOrgDoc/" + docType + "",
		data: {
			id: removeOtherId,
			file: otherCredFile
		},
		type: "POST",
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
			$('#loading').show();
		},
		success: function (data, textStatus, request) {
			var info = request.getResponseHeader('success');
			$('p[id=idGlobalSuccessMessage]').html(info.split(",").join("<br/>"));
			$('div[id=idGlobalSuccess]').show();
			var table = '<tr>'
			if(data.form9FileName && data.form9FileName != '') {
				table += '<td><a class="removeMandatoryDocumentForm9File" href="#" docType="FORM9" removeDocumentId="' + data.id + '" removeDocumentFileName="' + data.form9FileName + '"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></a></i></td>'
				+ '<td><form:form method="GET">'
				+ '<a class="word-break" href="'
				+ getContextPath()
				+ '/downloadSupplierOrganizationDocuments/FORM9/'
				+ data.id
				+ '" mandatory-document-form9-id="' + data.id + '">'
				+ data.form9FileName
				+ '</a>'
				+ '<form:form>'
				+ '</td>'
				+ '<td><span class="col-sm-10 no-padding">'
				+ data.form9UploadDate
				+ '</span></td>'
			} else {
				table += '<td colspan="3">Document Not Uploaded</td>'
			}
			table += '</tr>';
			$('#mandatoryDocumentForm9Display tbody').html(table);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		},
		error: function (request, textStatus, errorThrown) {
			$('p[id=idGlobalErrorMessage]').html(request.getResponseHeader('error'));
			$('div[id=idGlobalError]').show();
			$('#loading').hide();
		},
		complete: function () {
			$('#loading').hide();
		}
	});

});

$(document).delegate('.removeMandatoryDocumentForm24File', 'click', function (e) {
	e.preventDefault();
	$('div[id=idGlobalError]').hide();
	$('div[id=idGlobalSuccess]').hide();

	var header = $("meta[name='_csrf_header']").attr("content");
	var token = $("meta[name='_csrf']").attr("content");
	var removeOtherId = $(this).attr('removeDocumentId');
	var otherCredFile = $(this).attr('removeDocumentFileName');
	var docType = $(this).attr('docType');
	$.ajax({
		url: getContextPath() + "/removeSupplierOrgDoc/" + docType + "",
		data: {
			id: removeOtherId,
			file: otherCredFile
		},
		type: "POST",
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
			$('#loading').show();
		},
		success: function (data, textStatus, request) {
			var info = request.getResponseHeader('success');
			$('p[id=idGlobalSuccessMessage]').html(info.split(",").join("<br/>"));
			$('div[id=idGlobalSuccess]').show();
			var table = '<tr>'
			if(data.form24FileName && data.form24FileName != '') {
				table += '<td><a class="removeMandatoryDocumentForm24File" href="#" docType="FORM24" removeDocumentId="' + data.id + '" removeDocumentFileName="' + data.form24FileName + '"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></a></i></td>'
				+ '<td><form:form method="GET">'
				+ '<a class="word-break" href="'
				+ getContextPath()
				+ '/downloadSupplierOrganizationDocuments/FORM24/'
				+ data.id
				+ '" mandatory-document-form24-id="' + data.id + '">'
				+ data.form24FileName
				+ '</a>'
				+ '<form:form>'
				+ '</td>'
				+ '<td><span class="col-sm-10 no-padding">'
				+ data.form24UploadDate
				+ '</span></td>'
			} else {
				table += '<td colspan="3">Document Not Uploaded</td>'
			}
			table += '</tr>';
			$('#mandatoryDocumentForm24Display tbody').html(table);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		},
		error: function (request, textStatus, errorThrown) {
			$('p[id=idGlobalErrorMessage]').html(request.getResponseHeader('error'));
			$('div[id=idGlobalError]').show();
			$('#loading').hide();
		},
		complete: function () {
			$('#loading').hide();
		}
	});

});

$(document).delegate('.removeMandatoryDocumentForm49File', 'click', function (e) {
	e.preventDefault();
	$('div[id=idGlobalError]').hide();
	$('div[id=idGlobalSuccess]').hide();

	var header = $("meta[name='_csrf_header']").attr("content");
	var token = $("meta[name='_csrf']").attr("content");
	var removeOtherId = $(this).attr('removeDocumentId');
	var otherCredFile = $(this).attr('removeDocumentFileName');
	var docType = $(this).attr('docType');
	$.ajax({
		url: getContextPath() + "/removeSupplierOrgDoc/" + docType + "",
		data: {
			id: removeOtherId,
			file: otherCredFile
		},
		type: "POST",
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
			$('#loading').show();
		},
		success: function (data, textStatus, request) {
			var info = request.getResponseHeader('success');
			$('p[id=idGlobalSuccessMessage]').html(info.split(",").join("<br/>"));
			$('div[id=idGlobalSuccess]').show();
			var table = '<tr>'
			if(data.form49FileName && data.form49FileName != '') {
				table += '<td><a class="removeMandatoryDocumentForm49File" href="#" docType="FORM49" removeDocumentId="' + data.id + '" removeDocumentFileName="' + data.form49FileName + '"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></a></i></td>'
				+ '<td><form:form method="GET">'
				+ '<a class="word-break" href="'
				+ getContextPath()
				+ '/downloadSupplierOrganizationDocuments/FORM49/'
				+ data.id
				+ '" mandatory-document-form49-id="' + data.id + '">'
				+ data.form49FileName
				+ '</a>'
				+ '<form:form>'
				+ '</td>'
				+ '<td><span class="col-sm-10 no-padding">'
				+ data.form49UploadDate
				+ '</span></td>'
			} else {
				table += '<td colspan="3">Document Not Uploaded</td>'
			}
			table += '</tr>';
			$('#mandatoryDocumentForm49Display tbody').html(table);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		},
		error: function (request, textStatus, errorThrown) {
			$('p[id=idGlobalErrorMessage]').html(request.getResponseHeader('error'));
			$('div[id=idGlobalError]').show();
			$('#loading').hide();
		},
		complete: function () {
			$('#loading').hide();
		}
	});

});


$(document).delegate('.removeMandatoryDocumentFormMofFile', 'click', function (e) {
	e.preventDefault();
	$('div[id=idGlobalError]').hide();
	$('div[id=idGlobalSuccess]').hide();

	var header = $("meta[name='_csrf_header']").attr("content");
	var token = $("meta[name='_csrf']").attr("content");
	var removeOtherId = $(this).attr('removeDocumentId');
	var otherCredFile = $(this).attr('removeDocumentFileName');
	var docType = $(this).attr('docType');
	$.ajax({
		url: getContextPath() + "/removeSupplierOrgDoc/" + docType + "",
		data: {
			id: removeOtherId,
			file: otherCredFile
		},
		type: "POST",
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
			$('#loading').show();
		},
		success: function (data, textStatus, request) {
			var info = request.getResponseHeader('success');
			$('p[id=idGlobalSuccessMessage]').html(info.split(",").join("<br/>"));
			$('div[id=idGlobalSuccess]').show();
			var table = '<tr>'
			if(data.mofFileName && data.mofFileName != '') {
				table += '<td><a class="removeMandatoryDocumentFormMofFile" href="#" docType="FORMMOF" removeDocumentId="' + data.id + '" removeDocumentFileName="' + data.mofFileName + '"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></a></i></td>'
				+ '<td><form:form method="GET">'
				+ '<a class="word-break" href="'
				+ getContextPath()
				+ '/downloadSupplierOrganizationDocuments/FORMMOF/'
				+ data.id
				+ '" mandatory-document-formMof-id="' + data.id + '">'
				+ data.mofFileName
				+ '</a>'
				+ '<form:form>'
				+ '</td>'
				+ '<td><span class="col-sm-10 no-padding">'
				+ data.mofUploadDate
				+ '</span></td>'
			} else {
				table += '<td colspan="3">Document Not Uploaded</td>'
			}
			table += '</tr>';
			$('#mandatoryDocumentFormMofDisplay tbody').html(table);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		},
		error: function (request, textStatus, errorThrown) {
			$('p[id=idGlobalErrorMessage]').html(request.getResponseHeader('error'));
			$('div[id=idGlobalError]').show();
			$('#loading').hide();
		},
		complete: function () {
			$('#loading').hide();
		}
	});

});



$(document).delegate('.removeMandatoryDocumentSSMFile', 'click', function (e) {
	e.preventDefault();
	$('div[id=idGlobalError]').hide();
	$('div[id=idGlobalSuccess]').hide();

	var header = $("meta[name='_csrf_header']").attr("content");
	var token = $("meta[name='_csrf']").attr("content");
	var removeOtherId = $(this).attr('removeDocumentId');
	var otherCredFile = $(this).attr('removeDocumentFileName');
	var docType = $(this).attr('docType');
	$.ajax({
		url: getContextPath() + "/removeSupplierOrgDoc/" + docType + "",
		data: {
			id: removeOtherId,
			file: otherCredFile
		},
		type: "POST",
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
			$('#loading').show();
		},
		success: function (data, textStatus, request) {
			var info = request.getResponseHeader('success');
			$('p[id=idGlobalSuccessMessage]').html(info.split(",").join("<br/>"));
			$('div[id=idGlobalSuccess]').show();
			var table = '<tr>'
			if(data.ssmFileName && data.ssmFileName != '') {
				table += '<td><a class="removeMandatoryDocumentSSMFile" href="#" docType="SSM" removeDocumentId="' + data.id + '" removeDocumentFileName="' + data.ssmFileName + '"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></a></i></td>'
					+ '<td><form:form method="GET">'
					+ '<a class="word-break" href="'
					+ getContextPath()
					+ '/downloadSupplierOrganizationDocuments/SSM/'
					+ data.id
					+ '" mandatory-document-ssm-id="' + data.id + '">'
					+ data.ssmFileName
					+ '</a>'
					+ '<form:form>'
					+ '</td>'
					+ '<td><span class="col-sm-10 no-padding">'
					+ data.ssmUploadDate
					+ '</span></td>'
			} else {
				table += '<td colspan="3">Document Not Uploaded</td>'
			}
			table += '</tr>';
			$('#mandatoryDocumentFormSSMDisplay tbody').html(table);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		},
		error: function (request, textStatus, errorThrown) {
			$('p[id=idGlobalErrorMessage]').html(request.getResponseHeader('error'));
			$('div[id=idGlobalError]').show();
			$('#loading').hide();
		},
		complete: function () {
			$('#loading').hide();
		}
	});

});



$(document).delegate('.removeMandatoryDocumentFormVDFFile', 'click', function (e) {
	e.preventDefault();
	$('div[id=idGlobalError]').hide();
	$('div[id=idGlobalSuccess]').hide();

	var header = $("meta[name='_csrf_header']").attr("content");
	var token = $("meta[name='_csrf']").attr("content");
	var removeOtherId = $(this).attr('removeDocumentId');
	var otherCredFile = $(this).attr('removeDocumentFileName');
	var docType = $(this).attr('docType');
	$.ajax({
		url: getContextPath() + "/removeSupplierOrgDoc/" + docType + "",
		data: {
			id: removeOtherId,
			file: otherCredFile
		},
		type: "POST",
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
			$('#loading').show();
		},
		success: function (data, textStatus, request) {
			var info = request.getResponseHeader('success');
			$('p[id=idGlobalSuccessMessage]').html(info.split(",").join("<br/>"));
			$('div[id=idGlobalSuccess]').show();
			var table = '<tr>'
			if(data.vdfFileName && data.vdfFileName != '') {
				table += '<td><a class="removeMandatoryDocumentFormVDFFile" href="#" docType="VDF" removeDocumentId="' + data.id + '" removeDocumentFileName="' + data.vdfFileName + '"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></a></i></td>'
					+ '<td><form:form method="GET">'
					+ '<a class="word-break" href="'
					+ getContextPath()
					+ '/downloadSupplierOrganizationDocuments/VDF/'
					+ data.id
					+ '" mandatory-document-vdf-id="' + data.id + '">'
					+ data.vdfFileName
					+ '</a>'
					+ '<form:form>'
					+ '</td>'
					+ '<td><span class="col-sm-10 no-padding">'
					+ data.vdfUploadDate
					+ '</span></td>'
			} else {
				table += '<td colspan="3">Document Not Uploaded</td>'
			}
			table += '</tr>';
			$('#mandatoryDocumentFormVDFDisplay tbody').html(table);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		},
		error: function (request, textStatus, errorThrown) {
			$('p[id=idGlobalErrorMessage]').html(request.getResponseHeader('error'));
			$('div[id=idGlobalError]').show();
			$('#loading').hide();
		},
		complete: function () {
			$('#loading').hide();
		}
	});

});


$(document).delegate('.removeMandatoryDocumentFormCCDFFile', 'click', function (e) {
	e.preventDefault();
	$('div[id=idGlobalError]').hide();
	$('div[id=idGlobalSuccess]').hide();

	var header = $("meta[name='_csrf_header']").attr("content");
	var token = $("meta[name='_csrf']").attr("content");
	var removeOtherId = $(this).attr('removeDocumentId');
	var otherCredFile = $(this).attr('removeDocumentFileName');
	var docType = $(this).attr('docType');
	$.ajax({
		url: getContextPath() + "/removeSupplierOrgDoc/" + docType + "",
		data: {
			id: removeOtherId,
			file: otherCredFile
		},
		type: "POST",
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
			$('#loading').show();
		},
		success: function (data, textStatus, request) {
			var info = request.getResponseHeader('success');
			$('p[id=idGlobalSuccessMessage]').html(info.split(",").join("<br/>"));
			$('div[id=idGlobalSuccess]').show();
			var table = '<tr>'
			if(data.ccdfFileName && data.ccdfFileName != '') {
				table += '<td><a class="removeMandatoryDocumentFormCCDFFile" href="#" docType="CCDF" removeDocumentId="' + data.id + '" removeDocumentFileName="' + data.ccdfFileName + '"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></a></i></td>'
					+ '<td><form:form method="GET">'
					+ '<a class="word-break" href="'
					+ getContextPath()
					+ '/downloadSupplierOrganizationDocuments/CCDF/'
					+ data.id
					+ '" mandatory-document-vdf-id="' + data.id + '">'
					+ data.ccdfFileName
					+ '</a>'
					+ '<form:form>'
					+ '</td>'
					+ '<td><span class="col-sm-10 no-padding">'
					+ data.ccdfUploadDate
					+ '</span></td>'
			} else {
				table += '<td colspan="3">Document Not Uploaded</td>'
			}
			table += '</tr>';
			$('#mandatoryDocumentFormCCDFDisplay tbody').html(table);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		},
		error: function (request, textStatus, errorThrown) {
			$('p[id=idGlobalErrorMessage]').html(request.getResponseHeader('error'));
			$('div[id=idGlobalError]').show();
			$('#loading').hide();
		},
		complete: function () {
			$('#loading').hide();
		}
	});

});

$(document).delegate('.removeMandatoryDocumentFormODELAFile', 'click', function (e) {
	e.preventDefault();
	$('div[id=idGlobalError]').hide();
	$('div[id=idGlobalSuccess]').hide();

	var header = $("meta[name='_csrf_header']").attr("content");
	var token = $("meta[name='_csrf']").attr("content");
	var removeOtherId = $(this).attr('removeDocumentId');
	var otherCredFile = $(this).attr('removeDocumentFileName');
	var docType = $(this).attr('docType');
	$.ajax({
		url: getContextPath() + "/removeSupplierOrgDoc/" + docType + "",
		data: {
			id: removeOtherId,
			file: otherCredFile
		},
		type: "POST",
		beforeSend: function (xhr) {
			xhr.setRequestHeader(header, token);
			$('#loading').show();
		},
		success: function (data, textStatus, request) {
			var info = request.getResponseHeader('success');
			$('p[id=idGlobalSuccessMessage]').html(info.split(",").join("<br/>"));
			$('div[id=idGlobalSuccess]').show();
			var table = '<tr>'
			if(data.odelaFileName && data.odelaFileName != '') {
				table += '<td><a class="removeMandatoryDocumentFormODELAFile" href="#" docType="ODELA" removeDocumentId="' + data.id + '" removeDocumentFileName="' + data.odelaFileName + '"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></a></i></td>'
					+ '<td><form:form method="GET">'
					+ '<a class="word-break" href="'
					+ getContextPath()
					+ '/downloadSupplierOrganizationDocuments/ODELA/'
					+ data.id
					+ '" mandatory-document-vdf-id="' + data.id + '">'
					+ data.odelaFileName
					+ '</a>'
					+ '<form:form>'
					+ '</td>'
					+ '<td><span class="col-sm-10 no-padding">'
					+ data.odelaUploadDate
					+ '</span></td>'
			} else {
				table += '<td colspan="3">Document Not Uploaded</td>'
			}
			table += '</tr>';
			$('#mandatoryDocumentFormODELADisplay tbody').html(table);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		},
		error: function (request, textStatus, errorThrown) {
			$('p[id=idGlobalErrorMessage]').html(request.getResponseHeader('error'));
			$('div[id=idGlobalError]').show();
			$('#loading').hide();
		},
		complete: function () {
			$('#loading').hide();
		}
	});

});