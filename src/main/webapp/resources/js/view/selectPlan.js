	$(document).ready(function() {

/*			$("#idButtonHolder").click(function(e) {
//					alert("karna h ya nhi ye bta");
				
					if (confirm("Save your work before leaving?") ) {
				        console.log("yes");
				    } else {
				        console.log("no");
				    }
			
			});*/

		
		//on ready default 1st radio check 
		$("#radio_0").attr('checked', true);

		//To calculate Total Fee on document ready
		// console.log("tab-link data-attr-tab :" +$('.tab-link-0').attr('data-att-tab'));
		if($('.tab-link-0').attr('data-att-tab') === "userTab"){
			calculateTotalFeeUser();
		}else{
			calculateTotalFeeEvent();
		}

	
		/*$('.sp_inner').masonry({
		  // options
		  itemSelector: '.sp_box1',
		  gutter: 10
		});*/
		
		/*
		
		$('.planGroup').each(function(i) {
			$(this).addClass('planGroupColor_' + i);
		}); //cb_grey 
		$('.sp_box1, .sp_box2').each(function(i) {
			$(this).find("div[class^='spb_heading']").addClass('planColor_' + (i + 1));
			$(this).find(".spa_left_bot").addClass('planColor_' + (i + 1));
			$(this).find(".cb_style").addClass('planColorBg_' + (i + 1));
		}); */
		
		// Use To switch Tabs
		$('ul.tabs li').click(function() {
			var tab_id = $(this).attr('data-tab');

			$('ul.tabs li').removeClass('current');
			$('.tab-content').removeClass('current');

			$(this).addClass('current');
			$("#" + tab_id).addClass('current');
		})
	});
	
$('.eventTab').on('click', function(event) {
	calculateTotalFeeEvent();
	});

$('.userTab').on('click', function(event) {
	calculateTotalFeeUser();
	});

$(document).on("keyup", ".noOfEvent", function(e) {
	var noOfEvent = parseInt($(this).val());
	var currencyCode = $('#eventcurrencyId').val();
	var promoCode = $('#promoCodeEvent').val();
	
	$('#userError').html('').removeClass('has-error');
	/*if($(this).val() === 'undefined' || $(this).val() === '0' ){
		console.log("noOfEvent :"+$(this).val());
		$('#eventError').html('<span class="help-block form-error">Event should be greater then 0</span>').addClass('has-error');
    return false;
	}*/
	
	ajaxCallOnChangeEventFields(noOfEvent, promoCode, currencyCode);
});


$( '#promoCodeEvent' ).on('change', function(e) {
	//alert($(this).val());
	var noOfEvent = parseInt($('.noOfEvent').val());
//	console.log("noOfUser :"+$(this).val());
	$('#promoErrorEvent').html('').removeClass('has-error');
	/*if($('.noOfEvent').val() === 'undefined' || $('.noOfEvent').val() === '0' ){
		console.log("noOfUser :"+$(this).val());
		$('#eventError').html('<span class="help-block form-error">Event should be greater then 0</span>').addClass('has-error');
		return false;
	}*/
	var promoCode = $(this).val();
	
	var currencyCode = $('#eventcurrencyId').val();
	
	ajaxCallOnChangeEventFields(noOfEvent, promoCode, currencyCode);
});


$(document).on("keyup", ".noOfuser", function(e) {
		//alert($(this).val());
		var noOfUser = parseInt($(this).val());
		//Show approver user
		if (isNaN(noOfUser)) {
			$('#userNo').text("each user");
			$('#appNo').text("1 approver user");
			var approvalUser = 'no approver users ';
			$("#approvalUser").html(approvalUser);
		} else {
			$('#userNo').text(noOfUser + " users");
			$('#appNo').text(noOfUser + " approver users");
			var approvalUser = noOfUser + ' approver users ';
			$("#approvalUser").html(approvalUser);
		}
		
	//	console.log("noOfUser :"+$(this).val());
		$('#userError').html('').removeClass('has-error');
		/*if($(this).val() === 'undefined' || $(this).val() === '0' ){
			console.log("noOfUser :"+$(this).val());
			$('#userError').html('<span class="help-block form-error">User should be greater then 0</span>').addClass('has-error');
	    return false;
		}*/
		var promoCode = $('#promoCodeUser').val();
		var selectedMonths = parseInt($("input[name=periodId]:checked").attr('data-duration'));
		
		var radioIndex = $("input[name=periodId]:checked").attr('data-index');
		//console.log($("input[name=periodId]:checked").attr('data-duration') +"selectedMonths ");
		var montlhyDiscount = parseInt($('#discountValue'+radioIndex).text());
		
		var currencyCode = $('#usercurrencyId').val();
		
		ajaxCallOnChangeUserFields(noOfUser, promoCode, selectedMonths, montlhyDiscount, currencyCode);
	});
	
	
$( 'input[name="periodId"]:radio' ).on('change', function(e) {
		//alert($(this).val());
		var noOfUser = parseInt($('.noOfuser').val());
	//	console.log("noOfUser :"+$(this).val());
		$('#userError').html('').removeClass('has-error');
		/*if($('.noOfuser').val() === 'undefined' || $('.noOfuser').val() === '0' ){
			console.log("noOfUser :"+$(this).val());
			$('#userError').html('<span class="help-block form-error">User should be greater then 0</span>').addClass('has-error');
			return false;
		}*/
		var promoCode = $('#promoCodeUser').val();
		var selectedMonths = parseInt($(this).attr('data-duration'));
		
		var radioIndex = $(this).attr('data-index');
		var montlhyDiscount = parseInt($('#discountValue'+radioIndex).text());
		
		var currencyCode = $('#usercurrencyId').val();
		
		ajaxCallOnChangeUserFields(noOfUser, promoCode, selectedMonths, montlhyDiscount, currencyCode);
	});
	
$( '#promoCodeUser' ).on('change', function(e) {
	//alert($(this).val());
	var noOfUser = parseInt($('.noOfuser').val());
//	console.log("noOfUser :"+$(this).val());
	$('#promoErrorUser').html('').removeClass('has-error');
	/*if($('.noOfuser').val() === 'undefined' || $('.noOfuser').val() === '0' ){
		console.log("noOfUser :"+$(this).val());
		$('#userError').html('<span class="help-block form-error">User should be greater then 0</span>').addClass('has-error');
		return false;
	}*/
	var promoCode = $(this).val();
	
	var selectedMonths = parseInt($("input[name=periodId]:checked").attr('data-duration'));
	
	var radioIndex = $("input[name=periodId]:checked").attr('data-index');
	var montlhyDiscount = parseInt($('#discountValue'+radioIndex).text());
	
	var currencyCode = $('#usercurrencyId').val();
	
	ajaxCallOnChangeUserFields(noOfUser, promoCode, selectedMonths, montlhyDiscount, currencyCode);
});
	
	
function calculateTotalFeeUser(){
	var noOfUser = parseInt($('.noOfuser').val());
	var taxFormt = parseFloat($('#taxFormtUser').val());
	console.log("taxFormt: "+ taxFormt);
	//for show approver users 
	$("#approvalUser").html(noOfUser + ' approver users ');

	var currencyCode = $('#usercurrencyId').val();
	var selectedMonths = $('#radio_0').attr('data-duration');
	$('#userRangeTable tr').each(function(){
		var rangeId = $(this).children('td:first').attr('data-id');
	    var start = parseInt($(this).children('td:first').attr('data-start'));
	    var end = parseInt($(this).children('td:first').attr('data-end'));
	    var price = parseInt($(this).children('td:first').attr('data-price'));
	    var basePrice = $('#basePrice').val();
	    var baseUsers = $('#baseUsers').val();
	    
	//    console.log("basePrice :" + basePrice);
	 //   console.log("start :" + start+ " End :"+ end + " Price :"+ price +" noOfUser :"+ noOfUser);
	    
	    if(noOfUser >= start && noOfUser <= end){
//	    	console.log("start1 :" + start+ " End1 :"+ end + " Price1 :"+ price +" noOfUser1 :"+ noOfUser);
	    	
	    	$('#rangeUserId').val(rangeId);
	    	
	    	var baseFeeValue = 0;
	    	if(basePrice !== undefined ){
	    	//	console.log("baseprice ppppp :" +basePrice);
	    		basePrice = parseInt($('#basePrice').val());
//	    		$('#totalFeeTableUser tr').eq(-1).before("<tr><td>new row</td></tr>")
	    		var baseFeeLabel = currencyCode + " "+ basePrice + " for " + baseUsers + " Users X " + selectedMonths + " Months";
	    		$('#totalFeeTableUser').find("#baseFeeLabel").html(baseFeeLabel);
	    		
	    		baseFeeValue = (basePrice * selectedMonths);
		    //	console.log("baseFeeValue :" + baseFeeValue);
		    	$('#totalFeeTableUser').find("#baseFeeValue").html(ReplaceNumberWithCommas((baseFeeValue).toFixed(2)));
	    		$('#baseFeeTr').removeClass('flagvisibility');
	    		
	    		if(baseUsers >= noOfUser ){
		    		var totalFeeValue = (price * noOfUser * selectedMonths);
		    //		console.log("totalFeeValue :" + totalFeeValue);
		    		var feeValue = '<input type="hidden" name="feeValue" value="'+totalFeeValue+'" >';
		    		$('#totalFeeTableUser').find("#totalFeeValue").html(feeValue +ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
		    		$('#totalFeeTr').addClass('flagvisibility');
		    	}else{
		    		noOfUser = noOfUser - baseUsers;
		    		var totalFeeLabel = currencyCode + " "+ price + " X " + noOfUser + " Users X " + selectedMonths + " Months";
		    		$('#totalFeeTableUser').find("#totalFeeLabel").html(totalFeeLabel);
		    		
		    		var totalFeeValue = (price * noOfUser * selectedMonths);
		    //		console.log("totalFeeValue :" + totalFeeValue);
		    		var feeValue = '<input type="hidden" name="feeValue" value="'+totalFeeValue+'" >';
		    		$('#totalFeeTableUser').find("#totalFeeValue").html(feeValue +ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
		    		$('#totalFeeTr').removeClass('flagvisibility');
		    	}
	    		totalFeeValue = (basePrice !== undefined ? basePrice : 0) + totalFeeValue;
	    	}else{
		    	
	    	var totalFeeLabel = currencyCode + " "+ price + " X " + noOfUser + " Users X " + selectedMonths + " Months";
	    	$('#totalFeeTableUser').find("#totalFeeLabel").html(totalFeeLabel);
	    	
	    	var totalFeeValue = (price * noOfUser * selectedMonths);
//	    	console.log("totalFeeValue :" + totalFeeValue);
	    	var feeValue = '<input type="hidden" name="feeValue" value="'+totalFeeValue+'" >';
	    	$('#totalFeeTableUser').find("#totalFeeValue").html(feeValue +ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
	    	$('#totalFeeTr').removeClass('flagvisibility');
	    	}
	    	
	    	
	    	
	    	var selectMonthDiscount =parseInt($('#discountValue0').text());
	    	var totalFeeDiscountLabel = "Subscription Discount "+ selectMonthDiscount +"%";
//	    	console.log("totalFeeDiscountLabel :" + totalFeeDiscountLabel);
	    	$('#totalFeeTableUser').find("#totalFeeDiscountLabel").html(totalFeeDiscountLabel);
	    	
	    	var totalFeeDiscountValue = (totalFeeValue / 100) * selectMonthDiscount;
//	    	console.log("totalFeeDiscountValue :"+ totalFeeDiscountValue);
	    	var feeDiscountValue = '<input type="hidden" name="feeDiscountValue" value="'+totalFeeDiscountValue+'" >';
	    	$('#totalFeeTableUser').find("#totalFeeDiscountValue").html(feeDiscountValue +"-"+ReplaceNumberWithCommas((totalFeeDiscountValue).toFixed(2)));
//	    	console.log("totalFeeValue :" + totalFeeValue);
	    	var totalFeeAmount = totalFeeValue - totalFeeDiscountValue;
//	    	console.log("totalFeeAmount : "+totalFeeAmount);
	    	var totalFeeAmountValue = '<input type="hidden" name="totalFeeAmount" value="'+totalFeeAmount+'" >';
	    	
	    	var totalTax = (totalFeeAmount * taxFormt)/100 ;
	    	totalFeeAmount = totalFeeAmount + totalTax;
	    	$('#totalFeeTableUser').find("#tax").html(ReplaceNumberWithCommas((totalTax).toFixed(2)));
	    	$('#totalFeeTableUser').find('#totalFeeAmount').html(totalFeeAmountValue+ReplaceNumberWithCommas((totalFeeAmount).toFixed(2)));
	    }
	});
}

function calculateTotalFeeEvent(){
var noOfEvent = $('.noOfEvent').val();
var currencyCode = $('#eventcurrencyId').val();
var taxFormt = parseFloat($('#taxFormtEvent').val());

$('#eventRangeTable tr').each(function(){
	var rangeId = $(this).children('td:first').attr('data-id');
    var start = parseInt($(this).children('td:first').attr('data-start'));
    var end = parseInt($(this).children('td:first').attr('data-end'));
    var price = parseInt($(this).children('td:first').attr('data-price'));
 //   console.log("start :" + start+ " End :"+ end + " Price :"+ price +" noOfEvent :"+ noOfEvent);
    
    if(noOfEvent >= start && noOfEvent <= end){
 //   	console.log("start1 :" + start+ " End1 :"+ end + " Price1 :"+ price +" noOfEvent1 :"+ noOfEvent);
    	$('#rangeEventId').val(rangeId);
    	
    	var totalFeeLabel = currencyCode + " "+ price + " X " + noOfEvent + " Events ";
    	$('#totalFeeTableEvent').find("#totalFeeLabel").html(totalFeeLabel);
    	var totalFeeValue = (price * noOfEvent);
    	var feeValue = '<input type="hidden" name="feeValue" value="'+totalFeeValue+'" >';
    	$('#totalFeeTableEvent').find("#totalFeeValue").html(feeValue +ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
    	
    	var totalFeeAmountValue = '<input type="hidden" name="totalFeeAmount" value="'+totalFeeValue+'" >';
    	var totalTax = (totalFeeValue * taxFormt)/100 ;
    	totalFeeValue = totalFeeValue + totalTax;
    	$('#totalFeeTableEvent').find("#tax").html(ReplaceNumberWithCommas((totalTax).toFixed(2)));
    	$('#totalFeeTableEvent').find('#totalFeeAmount').html(totalFeeAmountValue+ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
    	
    }
});

}

function ajaxCallOnChangeUserFields(noOfUser, promoCode, selectedMonths, montlhyDiscount, currencyCode){
	var taxFormt = parseFloat($('#taxFormtUser').val());
	if(promoCode !== 'undefined' && promoCode !== ''){
		var header = $("meta[name='_csrf_header']").attr("content");
		var token = $("meta[name='_csrf']").attr("content");
		var url = getContextPath() + "/buyerSubscription/getPromoCode";
		var changePlan = $('#changePlanId').val();
		var basePrice = $('#basePrice').val();
	    var baseUsers = $('#baseUsers').val();
		
//		console.log("changePlan : " + changePlan);
		if(changePlan !== 'undefined' && changePlan){
			url = getContextPath() + "/buyer/billing/getPromoCode";
		}
			$.ajax({
				type : "GET",
				url : url,
				data : {
					promoCode : promoCode,
				},
				beforeSend : function(xhr) {
					$('#loading').show();
					xhr.setRequestHeader(header, token);
					xhr.setRequestHeader("Accept", "application/json");
					xhr.setRequestHeader("Content-Type", "application/json");
				},
				complete : function() {
					$('#loading').hide();
				},
				success : function(data) {
					//console.log(data);
					$('#promoErrorUser').html('').removeClass('has-error');
					$('#userRangeTable tr').each(function(){
						var rangeId = $(this).children('td:first').attr('data-id');
					    var start = parseInt($(this).children('td:first').attr('data-start'));
					    var end = parseInt($(this).children('td:first').attr('data-end'));
					    var price = parseInt($(this).children('td:first').attr('data-price'));
					    var basePrice = $('#basePrice').val();
					    var baseUsers = $('#baseUsers').val();
					 //   console.log("start :" + start+ " End :"+ end + " Price :"+ price +" noOfUser :"+ noOfUser);
					    
					    if(noOfUser >= start && noOfUser <= end){
//					    	console.log("start1 :" + start+ " End1 :"+ end + " Price1 :"+ price +" noOfUser1 :"+ noOfUser);
					    	
					    	$('#rangeUserId').val(rangeId);
					    	
					    	$('#promoCodeUserId').val(data.id);
					    	
					    	var baseFeeValue = 0;
					    	if(basePrice !== undefined ){
					    //		console.log("baseprice ppppp :" +basePrice);
					    		basePrice = parseInt($('#basePrice').val());
//					    		$('#totalFeeTableUser tr').eq(-1).before("<tr><td>new row</td></tr>")
					    		var baseFeeLabel = currencyCode + " "+ basePrice + " for " + baseUsers + " Users X " + selectedMonths + " Months";
					    		$('#totalFeeTableUser').find("#baseFeeLabel").html(baseFeeLabel);
					    		
					    		baseFeeValue = (basePrice * selectedMonths);
						//    	console.log("baseFeeValue :" + baseFeeValue);
						    	$('#totalFeeTableUser').find("#baseFeeValue").html(ReplaceNumberWithCommas((baseFeeValue).toFixed(2)));
					    		$('#baseFeeTr').removeClass('flagvisibility');
					    		
					    		if(baseUsers >= noOfUser ){
						    		var totalFeeValue = (price * noOfUser * selectedMonths);
					//	    		console.log("totalFeeValue :" + totalFeeValue);
						    		var feeValue = '<input type="hidden" name="feeValue" value="'+totalFeeValue+'" >';
						    		$('#totalFeeTableUser').find("#totalFeeValue").html(feeValue +ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
						    		$('#totalFeeTr').addClass('flagvisibility');
						    	}else{
						    		noOfUser = noOfUser - baseUsers;
						    		var totalFeeLabel = currencyCode + " "+ price + " X " + noOfUser + " Users X " + selectedMonths + " Months";
						    		$('#totalFeeTableUser').find("#totalFeeLabel").html(totalFeeLabel);
						    		
						    		var totalFeeValue = (price * noOfUser * selectedMonths);
						//    		console.log("totalFeeValue :" + totalFeeValue);
						    		var feeValue = '<input type="hidden" name="feeValue" value="'+totalFeeValue+'" >';
						    		$('#totalFeeTableUser').find("#totalFeeValue").html(feeValue +ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
						    		$('#totalFeeTr').removeClass('flagvisibility');
						    	}
					    		totalFeeValue = (baseFeeValue !== undefined ? baseFeeValue : 0) + totalFeeValue;
					    	}else{
						    	
					    	var totalFeeLabel = currencyCode + " "+ price + " X " + noOfUser + " Users X " + selectedMonths + " Months";
					    	$('#totalFeeTableUser').find("#totalFeeLabel").html(totalFeeLabel);
					    	
					    	var totalFeeValue = (price * noOfUser * selectedMonths);
//					    	console.log("totalFeeValue :" + totalFeeValue);
					    	var feeValue = '<input type="hidden" name="feeValue" value="'+totalFeeValue+'" >';
					    	$('#totalFeeTableUser').find("#totalFeeValue").html(feeValue +ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
					    	$('#totalFeeTr').removeClass('flagvisibility');
					    	}
//					    	var totalFeeLabel = currencyCode + " "+ price + " X " + noOfUser + " Users X " + selectedMonths + " Months";
//					    	$('#totalFeeTableUser').find("#totalFeeLabel").html(totalFeeLabel);
//					    	
//					    	var totalFeeValue = (price * noOfUser * selectedMonths);
//					    	
//					    	var feeValue = '<input type="hidden" name="feeValue" value="'+totalFeeValue+'" >';
//					    	$('#totalFeeTableUser').find("#totalFeeValue").html(feeValue +ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
////					    	
					    	
					    	var totalFeeDiscountLabel = "Subscription Discount "+ montlhyDiscount +"%";
					    	$('#totalFeeTableUser').find("#totalFeeDiscountLabel").html(totalFeeDiscountLabel);
					    	
					    	var totalFeeDiscountValue = (totalFeeValue / 100) * montlhyDiscount;
					    	var feeDiscountValue = '<input type="hidden" name="feeDiscountValue" value="'+totalFeeDiscountValue+'" >';
					    	$('#totalFeeTableUser').find("#totalFeeDiscountValue").html(feeDiscountValue +"-"+ReplaceNumberWithCommas((totalFeeDiscountValue).toFixed(2)));
					    	
					    	var totalFeePromoValue = 0;
					    	var totalFeePromoLabel = data.promoName;
					    //	console.log(data.promoDiscount);
					    	if(data.discountType !== 'undefined' && data.discountType === 'PERCENTAGE'){
					    	totalFeePromoValue  = (totalFeeValue / 100) * data.promoDiscount;
					    	totalFeePromoLabel +=  "-"+ data.promoDiscount +" % OFF";
					    	}else{
					    		totalFeePromoValue  = data.promoDiscount;
						    	totalFeePromoLabel +=  "- "+currencyCode+" "+ data.promoDiscount +" OFF";
					    	}
					    	$('#totalFeeTableUser').find('#totalFeePromoLabel').html(totalFeePromoLabel);
					    	var promoCodeDiscount = '<input type="hidden" name="promoCodeDiscount" value="'+totalFeePromoValue+'" >';
					    	$('#totalFeeTableUser').find('#totalFeePromoValue').html(promoCodeDiscount+"-"+ ReplaceNumberWithCommas((totalFeePromoValue).toFixed(2)));
					    	
					    	var totalFeeAmount = (totalFeeValue - totalFeeDiscountValue) - totalFeePromoValue ;

					    	var totalFeeAmountValue = '<input type="hidden" name="totalFeeAmount" value="'+totalFeeAmount+'" >';
					    	var totalTax = (totalFeeAmount * taxFormt)/100 ;
					    	totalFeeAmount = totalFeeAmount + totalTax;
					    	$('#totalFeeTableUser').find("#tax").html(ReplaceNumberWithCommas((totalTax).toFixed(2)));
					    	$('#totalFeeTableUser').find('#totalFeeAmount').html(totalFeeAmountValue+ReplaceNumberWithCommas((totalFeeAmount).toFixed(2)));
					    	
					    }
					});
					
				},
				error : function(request, textStatus, errorThrown) {
					
					$('#userRangeTable tr').each(function(){
						var rangeId = $(this).children('td:first').attr('data-id');
					    var start = parseInt($(this).children('td:first').attr('data-start'));
					    var end = parseInt($(this).children('td:first').attr('data-end'));
					    var price = parseInt($(this).children('td:first').attr('data-price'));
					    var basePrice = $('#basePrice').val();
					    var baseUsers = $('#baseUsers').val();
					 //   console.log("start :" + start+ " End :"+ end + " Price :"+ price +" noOfUser :"+ noOfUser);
					    
					    if(noOfUser >= start && noOfUser <= end){
//					    	console.log("start1 :" + start+ " End1 :"+ end + " Price1 :"+ price +" noOfUser1 :"+ noOfUser);
					    	
					    	$('#rangeUserId').val(rangeId);
					    	
					    	
					    	var baseFeeValue = 0;
					    	if(basePrice !== undefined ){
					    //		console.log("baseprice ppppp :" +basePrice);
					    		basePrice = parseInt($('#basePrice').val());
//					    		$('#totalFeeTableUser tr').eq(-1).before("<tr><td>new row</td></tr>")
					    		var baseFeeLabel = currencyCode + " "+ basePrice + " for " + baseUsers + " Users X " + selectedMonths + " Months";
					    		$('#totalFeeTableUser').find("#baseFeeLabel").html(baseFeeLabel);
					    		
					    		baseFeeValue = (basePrice * selectedMonths);
						  //  	console.log("baseFeeValue :" + baseFeeValue);
						    	$('#totalFeeTableUser').find("#baseFeeValue").html(ReplaceNumberWithCommas((baseFeeValue).toFixed(2)));
					    		$('#baseFeeTr').removeClass('flagvisibility');
					    		
					    		if(baseUsers >= noOfUser ){
						    		var totalFeeValue = (price * noOfUser * selectedMonths);
						    //		console.log("totalFeeValue :" + totalFeeValue);
						    		var feeValue = '<input type="hidden" name="feeValue" value="'+totalFeeValue+'" >';
						    		$('#totalFeeTableUser').find("#totalFeeValue").html(feeValue +ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
						    		$('#totalFeeTr').addClass('flagvisibility');
						    	}else{
						    		noOfUser = noOfUser - baseUsers;
						    		var totalFeeLabel = currencyCode + " "+ price + " X " + noOfUser + " Users X " + selectedMonths + " Months";
						    		$('#totalFeeTableUser').find("#totalFeeLabel").html(totalFeeLabel);
						    		
						    		var totalFeeValue = (price * noOfUser * selectedMonths);
						    //		console.log("totalFeeValue :" + totalFeeValue);
						    		var feeValue = '<input type="hidden" name="feeValue" value="'+totalFeeValue+'" >';
						    		$('#totalFeeTableUser').find("#totalFeeValue").html(feeValue +ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
						    		$('#totalFeeTr').removeClass('flagvisibility');
						    	}
					    		totalFeeValue = (baseFeeValue !== undefined ? baseFeeValue : 0) + totalFeeValue;
					    	}else{
						    	
					    	var totalFeeLabel = currencyCode + " "+ price + " X " + noOfUser + " Users X " + selectedMonths + " Months";
					    	$('#totalFeeTableUser').find("#totalFeeLabel").html(totalFeeLabel);
					    	
					    	var totalFeeValue = (price * noOfUser * selectedMonths);
//					    	console.log("totalFeeValue :" + totalFeeValue);
					    	var feeValue = '<input type="hidden" name="feeValue" value="'+totalFeeValue+'" >';
					    	$('#totalFeeTableUser').find("#totalFeeValue").html(feeValue +ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
					    	$('#totalFeeTr').removeClass('flagvisibility');
					    	}
					    	
//					    	var totalFeeLabel = currencyCode + " "+ price + " X " + noOfUser + " Users X " + selectedMonths + " Months";
//					    	$('#totalFeeTableUser').find("#totalFeeLabel").html(totalFeeLabel);
//					    	
//					    	var totalFeeValue = (price * noOfUser * selectedMonths);
//					    	var feeValue = '<input type="hidden" name="feeValue" value="'+totalFeeValue+'" >';
//					    	$('#totalFeeTableUser').find("#totalFeeValue").html(feeValue +ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
//					    	
					    	
					    	var totalFeeDiscountLabel = "Subscription Discount "+ montlhyDiscount +"%";
					    	$('#totalFeeTableUser').find("#totalFeeDiscountLabel").html(totalFeeDiscountLabel);
					    	
					    	var totalFeeDiscountValue = (totalFeeValue / 100) * montlhyDiscount;
					    	var feeDiscountValue = '<input type="hidden" name="feeDiscountValue" value="'+totalFeeDiscountValue+'" >';
					    	$('#totalFeeTableUser').find("#totalFeeDiscountValue").html(feeDiscountValue +"-"+ReplaceNumberWithCommas((totalFeeDiscountValue).toFixed(2)));
					    	
					    	$('#totalFeeTableUser').find('#totalFeePromoLabel').html("No Promotional Code");
					    	$('#totalFeeTableUser').find('#totalFeePromoValue').html("0.00");
					    	
					    	var totalFeeAmount = (totalFeeValue - totalFeeDiscountValue);

					    	var totalFeeAmountValue = '<input type="hidden" name="totalFeeAmount" value="'+totalFeeAmount+'" >';
					    	var totalTax = (totalFeeAmount * taxFormt)/100 ;
					    	totalFeeAmount = totalFeeAmount + totalTax;
					    	$('#totalFeeTableUser').find("#tax").html(ReplaceNumberWithCommas((totalTax).toFixed(2)));
					    	$('#totalFeeTableUser').find('#totalFeeAmount').html(totalFeeAmountValue+ReplaceNumberWithCommas((totalFeeAmount).toFixed(2)));
					    	
					    }
					
					$('#promoErrorUser').html('<span class="help-block form-error">'+request.getResponseHeader('error')+'</span>').addClass('has-error');
				});
				}
				
			});
	}else{
		$('#userRangeTable tr').each(function(){
			var rangeId = $(this).children('td:first').attr('data-id');
		    var start = parseInt($(this).children('td:first').attr('data-start'));
		    var end = parseInt($(this).children('td:first').attr('data-end'));
		    var price = parseInt($(this).children('td:first').attr('data-price'));
		    var basePrice = $('#basePrice').val();
		    var baseUsers = $('#baseUsers').val();
		 //   console.log("start :" + start+ " End :"+ end + " Price :"+ price +" noOfUser :"+ noOfUser);
		    
		    if(noOfUser >= start && noOfUser <= end){
//		    	console.log("start1 :" + start+ " End1 :"+ end + " Price1 :"+ price +" noOfUser1 :"+ noOfUser);
		    	
		    	$('#rangeUserId').val(rangeId);
		    	
		    	
		    	var baseFeeValue = 0;
		    	if(basePrice !== undefined ){
		    	//	console.log("baseprice ppppp :" +basePrice);
		    		basePrice = parseInt($('#basePrice').val());
//		    		$('#totalFeeTableUser tr').eq(-1).before("<tr><td>new row</td></tr>")
		    		var baseFeeLabel = currencyCode + " "+ basePrice + " for " + baseUsers + " Users X " + selectedMonths + " Months";
		    		$('#totalFeeTableUser').find("#baseFeeLabel").html(baseFeeLabel);
		    		
		    		baseFeeValue = (basePrice * selectedMonths);
			 //   	console.log("baseFeeValue :" + baseFeeValue);
			    	$('#totalFeeTableUser').find("#baseFeeValue").html(ReplaceNumberWithCommas((baseFeeValue).toFixed(2)));
		    		$('#baseFeeTr').removeClass('flagvisibility');
		    		
		    		if(baseUsers >= noOfUser ){
			    		var totalFeeValue = (price * noOfUser * selectedMonths);
		//	    		console.log("totalFeeValue :" + totalFeeValue);
			    		var feeValue = '<input type="hidden" name="feeValue" value="'+totalFeeValue+'" >';
			    		$('#totalFeeTableUser').find("#totalFeeValue").html(feeValue +ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
			    		$('#totalFeeTr').addClass('flagvisibility');
			    	}else{
			    		noOfUser = noOfUser - baseUsers;
			    		var totalFeeLabel = currencyCode + " "+ price + " X " + noOfUser + " Users X " + selectedMonths + " Months";
			    		$('#totalFeeTableUser').find("#totalFeeLabel").html(totalFeeLabel);
			    		
			    		var totalFeeValue = (price * noOfUser * selectedMonths);
			 //   		console.log("totalFeeValue :" + totalFeeValue);
			    		var feeValue = '<input type="hidden" name="feeValue" value="'+totalFeeValue+'" >';
			    		$('#totalFeeTableUser').find("#totalFeeValue").html(feeValue +ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
			    		$('#totalFeeTr').removeClass('flagvisibility');
			    	}
		    		totalFeeValue = (baseFeeValue !== undefined ? baseFeeValue : 0) + totalFeeValue;
		    	}else{
			    	
		    	var totalFeeLabel = currencyCode + " "+ price + " X " + noOfUser + " Users X " + selectedMonths + " Months";
		    	$('#totalFeeTableUser').find("#totalFeeLabel").html(totalFeeLabel);
		    	
		    	var totalFeeValue = (price * noOfUser * selectedMonths);
//		    	console.log("totalFeeValue :" + totalFeeValue);
		    	var feeValue = '<input type="hidden" name="feeValue" value="'+totalFeeValue+'" >';
		    	$('#totalFeeTableUser').find("#totalFeeValue").html(feeValue +ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
		    	$('#totalFeeTr').removeClass('flagvisibility');
		    	}
		    	
//		    	var totalFeeLabel = currencyCode + " "+ price + " X " + noOfUser + " Users X " + selectedMonths + " Months";
//		    	$('#totalFeeTableUser').find("#totalFeeLabel").html(totalFeeLabel);
//		    	
//		    	var totalFeeValue = (price * noOfUser * selectedMonths);
//		    	var feeValue = '<input type="hidden" name="feeValue" value="'+totalFeeValue+'" >';
//		    	$('#totalFeeTableUser').find("#totalFeeValue").html(feeValue +ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
		    	
		    	
		    	var totalFeeDiscountLabel = "Subscription Discount "+ montlhyDiscount +"%";
		    	$('#totalFeeTableUser').find("#totalFeeDiscountLabel").html(totalFeeDiscountLabel);
		    	
		    	var totalFeeDiscountValue = (totalFeeValue / 100) * montlhyDiscount;
		    	var feeDiscountValue = '<input type="hidden" name="feeDiscountValue" value="'+totalFeeDiscountValue+'" >';
		    	$('#totalFeeTableUser').find("#totalFeeDiscountValue").html(feeDiscountValue +"-"+ReplaceNumberWithCommas((totalFeeDiscountValue).toFixed(2)));
		    	
		    	$('#totalFeeTableUser').find("#totalFeePromoLabel").html("No Promotional Code");
		    	$('#totalFeeTableUser').find("#totalFeePromoValue").html("0.00");
		    	
		    	var totalFeeAmount = (totalFeeValue - totalFeeDiscountValue);

		    	var totalFeeAmountValue = '<input type="hidden" name="totalFeeAmount" value="'+totalFeeAmount+'" >';
		    	var totalTax = (totalFeeAmount * taxFormt)/100 ;
		    	totalFeeAmount = totalFeeAmount + totalTax;
		    	$('#totalFeeTableUser').find("#tax").html(ReplaceNumberWithCommas((totalTax).toFixed(2)));
		    	$('#totalFeeTableUser').find('#totalFeeAmount').html(totalFeeAmountValue+ReplaceNumberWithCommas((totalFeeAmount).toFixed(2)));
		    	
		    }
		});
	}
}

function ajaxCallOnChangeEventFields(noOfEvent, promoCode, currencyCode){
	var taxFormt = parseFloat($('#taxFormtEvent').val());
	if(promoCode !== 'undefined' && promoCode !== ''){
		var header = $("meta[name='_csrf_header']").attr("content");
		var token = $("meta[name='_csrf']").attr("content");
		var url = getContextPath() + "/buyerSubscription/getPromoCode";
		var changePlan = $('#changePlanId').val();
	//	console.log("changePlan : " + changePlan);
		if(changePlan !== 'undefined' && changePlan){
			url = getContextPath() + "/buyer/billing/getPromoCode";
		}
			$.ajax({
				type : "GET",
				url : url,
				data : {
					promoCode : promoCode,
				},
				beforeSend : function(xhr) {
					$('#loading').show();
					xhr.setRequestHeader(header, token);
					xhr.setRequestHeader("Accept", "application/json");
					xhr.setRequestHeader("Content-Type", "application/json");
				},
				complete : function() {
					$('#loading').hide();
				},
				success : function(data) {
					//console.log(data);
					$('#promoErrorEvent').html('').removeClass('has-error');
					
					
					$('#eventRangeTable tr').each(function(){
						var rangeId = $(this).children('td:first').attr('data-id');
					    var start = parseInt($(this).children('td:first').attr('data-start'));
					    var end = parseInt($(this).children('td:first').attr('data-end'));
					    var price = parseInt($(this).children('td:first').attr('data-price'));
					 //   console.log("start :" + start+ " End :"+ end + " Price :"+ price +" noOfEvent :"+ noOfEvent);
					    
					    if(noOfEvent >= start && noOfEvent <= end){
				//	    	console.log("start1 :" + start+ " End1 :"+ end + " Price1 :"+ price +" noOfEvent1 :"+ noOfEvent);
					    	
					    	$('#rangeEventId').val(rangeId);
					    	
					    	$('#promoCodeEventId').val(data.id);
					    	
					    	var totalFeeLabel = currencyCode + " "+ price + " X " + noOfEvent + " Events ";
					    	$('#totalFeeTableEvent').find("#totalFeeLabel").html(totalFeeLabel);
					    	var totalFeeValue = (price * noOfEvent);
					    	var feeValue = '<input type="hidden" name="feeValue" value="'+totalFeeValue+'" >';
					    	$('#totalFeeTableEvent').find("#totalFeeValue").html(feeValue +ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));

					    	
					    	var totalFeePromoValue = (totalFeeValue / 100) * data.promoDiscount;
					    	var totalFeePromoLabel = data.promoName;
					    	if(data.discountType !== 'undefined' && data.discountType === 'PERCENTAGE'){
						    	totalFeePromoValue  = (totalFeeValue / 100) * data.promoDiscount;
						    	totalFeePromoLabel +=  "-"+ data.promoDiscount +" % OFF";
						    	}else{
						    		totalFeePromoValue  = data.promoDiscount;
							    	totalFeePromoLabel +=  "- "+currencyCode+" "+ data.promoDiscount +" OFF";
						    	}
					    	
					    	$('#totalFeeTableEvent').find('#totalFeePromoLabel').html(totalFeePromoLabel);
					    	
					    	var promoCodeDiscount = '<input type="hidden" name="promoCodeDiscount" value="'+totalFeePromoValue+'" >';
					    	$('#totalFeeTableEvent').find('#totalFeePromoValue').html(promoCodeDiscount+"-"+ ReplaceNumberWithCommas((totalFeePromoValue).toFixed(2)));
					    
					    	var totalFeeAmount = (totalFeeValue - totalFeePromoValue);
					    	var totalFeeAmountValue = '<input type="hidden" name="totalFeeAmount" value="'+totalFeeAmount+'" >';
					    	var totalTax = (totalFeeAmount * taxFormt)/100 ;
					    	totalFeeAmount = totalFeeAmount + totalTax;
					    	$('#totalFeeTableEvent').find("#tax").html(ReplaceNumberWithCommas((totalTax).toFixed(2)));
					    	$('#totalFeeTableEvent').find('#totalFeeAmount').html(totalFeeAmountValue+ReplaceNumberWithCommas((totalFeeAmount).toFixed(2)));
					    }
					});
					
				},
				error : function(request, textStatus, errorThrown) {
					
					$('#eventRangeTable tr').each(function(){
						var rangeId = $(this).children('td:first').attr('data-id');
					    var start = parseInt($(this).children('td:first').attr('data-start'));
					    var end = parseInt($(this).children('td:first').attr('data-end'));
					    var price = parseInt($(this).children('td:first').attr('data-price'));
					 //   console.log("start :" + start+ " End :"+ end + " Price :"+ price +" noOfEvent :"+ noOfEvent);
					    
					    if(noOfEvent >= start && noOfEvent <= end){
					  //  	console.log("start1 :" + start+ " End1 :"+ end + " Price1 :"+ price +" noOfEvent1 :"+ noOfEvent);
					    	
					    	$('#rangeEventId').val(rangeId);
					    	
					    	var totalFeeLabel = currencyCode + " "+ price + " X " + noOfEvent + " Events ";
					    	$('#totalFeeTableEvent').find("#totalFeeLabel").html(totalFeeLabel);
					    	var totalFeeValue = (price * noOfEvent);
					    	var feeValue = '<input type="hidden" name="feeValue" value="'+totalFeeValue+'" >';
					    	$('#totalFeeTableEvent').find("#totalFeeValue").html(feeValue +ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));

					    	
					    	$('#totalFeeTableEvent').find('#totalFeePromoLabel').html("No Promotional Code");
					    	$('#totalFeeTableEvent').find('#totalFeePromoValue').html("0.00");
					    	
					    	var totalFeeAmount = totalFeeValue;
					    	var totalFeeAmountValue = '<input type="hidden" name="totalFeeAmount" value="'+totalFeeAmount+'" >';
					    	var totalTax = (totalFeeAmount * taxFormt)/100 ;
					    	totalFeeAmount = totalFeeAmount + totalTax;
					    	$('#totalFeeTableEvent').find("#tax").html(ReplaceNumberWithCommas((totalTax).toFixed(2)));
					    	$('#totalFeeTableEvent').find('#totalFeeAmount').html(totalFeeAmountValue+ReplaceNumberWithCommas((totalFeeAmount).toFixed(2)));

					    }
					    $('#promoErrorEvent').html('<span class="help-block form-error">'+request.getResponseHeader('error')+'</span>').addClass('has-error');
					});
					
				}
				
			});
	}else{
		$('#eventRangeTable tr').each(function(){
			var rangeId = $(this).children('td:first').attr('data-id');
		    var start = parseInt($(this).children('td:first').attr('data-start'));
		    var end = parseInt($(this).children('td:first').attr('data-end'));
		    var price = parseInt($(this).children('td:first').attr('data-price'));
		 //   console.log("start :" + start+ " End :"+ end + " Price :"+ price +" noOfEvent :"+ noOfEvent);
		    
		    if(noOfEvent >= start && noOfEvent <= end){
		    //	console.log("start1 :" + start+ " End1 :"+ end + " Price1 :"+ price +" noOfEvent1 :"+ noOfEvent);
		    	
		    	$('#rangeEventId').val(rangeId);
		    	
		    	var totalFeeLabel = currencyCode + " "+ price + " X " + noOfEvent + " Events ";
		    	$('#totalFeeTableEvent').find("#totalFeeLabel").html(totalFeeLabel);
		    	var totalFeeValue = (price * noOfEvent);
		    	var feeValue = '<input type="hidden" name="feeValue" value="'+totalFeeValue+'" >';
		    	$('#totalFeeTableEvent').find("#totalFeeValue").html(feeValue +ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));

		    	
		    	$('#totalFeeTableEvent').find('#totalFeePromoLabel').html("No Promotional Code");
		    	$('#totalFeeTableEvent').find('#totalFeePromoValue').html("0.00");
		    	
		    	var totalFeeAmount = totalFeeValue;
		    	var totalFeeAmountValue = '<input type="hidden" name="totalFeeAmount" value="'+totalFeeAmount+'" >';
		    	var totalTax = (totalFeeAmount * taxFormt)/100 ;
		    	totalFeeAmount = totalFeeAmount + totalTax;
		    	$('#totalFeeTableEvent').find("#tax").html(ReplaceNumberWithCommas((totalTax).toFixed(2)));
		    	$('#totalFeeTableEvent').find('#totalFeeAmount').html(totalFeeAmountValue+ReplaceNumberWithCommas((totalFeeAmount).toFixed(2)));

		    }
		});
		
	}
}


function ReplaceNumberWithCommas(yourNumber) {
	// Seperates the components of the number
	var n = yourNumber.toString().split(".");
	// Comma-fies the first part
	n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	// Combines the two sections
	return n.join(".");
}