$('document').ready(function() {
	
var chargeMonths = 0;
updateFields(chargeMonths);	

$("#openRenew").click(function() {
		$("#idRenewDialog").modal();
		calculateTotalFee();
	});

$("#confirmPayment").click(function() {
	$("#idconfirmPayment").modal();
});
	$(".openEventCredits").click(function() {
		var subsId = $(this).attr('data-subscriptionId');
		console.log("subsId ==" + subsId);
		// var promoCode = 'PROMO_498_VAL';
		var header = $("meta[name='_csrf_header']").attr("content");
		var token = $("meta[name='_csrf']").attr("content");
		$.ajax({ type : "GET", url : getContextPath() + "/buyer/billing/getSubscription/" + subsId, beforeSend : function(xhr) {
			$('#loading').show();
			xhr.setRequestHeader(header, token);
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		}, complete : function() {
			$('#loading').hide();
			updateFields(chargeMonths);
			console.log("==complete==");
		}, success : function(data) {
			console.log("==success== Subs Id :"+ data.id);
			chargeMonths = data.chargeMonths;
			updateSubscription(data);
			calculateUpdateTotalFee(chargeMonths);
			$('#loading').hide();
		}, error : function(request, textStatus, errorThrown) {
			console.log("==error==");
			$('#loading').hide();
		}

		});
	});

	// on ready previous subscription radio check
	var lastSubsPeriodId = $('#selectPeriodId').val();
	$('.periodRadioList input').each(function() {
//		console.log(" ==" + $(this).val() + " lastSubsId period id====" + lastSubsPeriodId);
		if ($(this).val() === lastSubsPeriodId) {
			$(this).attr('checked', true);
		}
	});

	$(document).on("keyup", "#numberUserEvent", function(e) {
		var numberUserEvent = parseInt($(this).val());
		// console.log("numberUserEvent :"+numberUserEvent);
		$('#numberError').html('').removeClass('has-error');
		if ($(this).val() === 'undefined' || $(this).val() === '0') {
			// console.log("numberUserEvent :"+$(this).val());
			$('#numberError').html('<span class="help-block form-error">Number should be greater then 0</span>').addClass('has-error');
			return false;
		}
		if (isNaN(numberUserEvent)) {
			$('#numberError').html('<span class="help-block form-error">It should be correct number</span>').addClass('has-error');
			return false;
		}
		
		//Show approve user in note and table
		var planType = $('#planType').val();
		
		if (planType === 'PER_USER') {
			if (isNaN(numberUserEvent)) {
				$('#userNo').text("each user");
				$('#appNo').text("1 approver user");
				var approvalUser = numberUserEvent + ' approver users ';
				$("#approverUser").html(approvalUser);

			} else {
				$('#userNo').text(numberUserEvent + " users");
				$('#appNo').text(numberUserEvent + " approver users");
				var approvalUser = numberUserEvent + ' approver users ';
				$("#approverUser").html(approvalUser);
			}		
	//	
		}
		
		var promoCode = $('#promoCode').val();
		var selectedMonths = parseInt($("input[name=periodId]:checked").attr('data-duration'));
		var montlhyDiscount = parseInt($("input[name=periodId]:checked").attr('data-discount'));
		var planType = $('#planType').val();
		var currencyCode = $('#currencyId').val();
		ajaxCallOnChangeFields(numberUserEvent, promoCode, selectedMonths, montlhyDiscount, currencyCode, planType);
	});

	$('input[name="periodId"]:radio').on('change', function(e) {
		var numberUserEvent = parseInt($('#numberUserEvent').val());
		$('#promoError').html('').removeClass('has-error');
		$('#numberError').html('').removeClass('has-error');
		if ($('#numberUserEvent').val() === 'undefined' || $('#numberUserEvent').val() === '0') {
			// console.log("numberUserEvent :"+$(this).val());
			$('#numberError').html('<span class="help-block form-error">Number should be greater then 0</span>').addClass('has-error');
			return false;
		}
		if (isNaN(numberUserEvent)) {
			$('#numberError').html('<span class="help-block form-error">It should be correct number</span>').addClass('has-error');
			return false;
		}
		var promoCode = $('#promoCode').val();
		var selectedMonths = parseInt($(this).attr('data-duration'));
		var montlhyDiscount = parseInt($(this).attr('data-discount'));
		var planType = $('#planType').val();
		var currencyCode = $('#currencyId').val();

		ajaxCallOnChangeFields(numberUserEvent, promoCode, selectedMonths, montlhyDiscount, currencyCode, planType);
	});

	$('#promoCode').on('change', function(e) {
		// alert($(this).val());
		var numberUserEvent = parseInt($('#numberUserEvent').val());
		$('#promoError').html('').removeClass('has-error');
		$('#numberError').html('').removeClass('has-error');
		if ($('#numberUserEvent').val() === 'undefined' || $('#numberUserEvent').val() === '0') {
			// console.log("numberUserEvent :"+$(this).val());
			$('#numberError').html('<span class="help-block form-error">Number should be greater then 0</span>').addClass('has-error');
			return false;
		}
		if (isNaN(numberUserEvent)) {
			$('#numberError').html('<span class="help-block form-error">It should be correct number</span>').addClass('has-error');
			return false;
		}
		var promoCode = $(this).val();
		var selectedMonths = parseInt($("input[name=periodId]:checked").attr('data-duration'));
		var montlhyDiscount = parseInt($("input[name=periodId]:checked").attr('data-discount'));
		var planType = $('#planType').val();
		var currencyCode = $('#currencyId').val();

		ajaxCallOnChangeFields(numberUserEvent, promoCode, selectedMonths, montlhyDiscount, currencyCode, planType);
	});

});

function calculateTotalFee() {
	// alert();
	var numberUserEvent = parseInt($('#numberUserEvent').val());
	
	
	var selectedMonths = parseInt($("input[name=periodId]:checked").attr('data-duration'));
	var montlhyDiscount = parseInt($("input[name=periodId]:checked").attr('data-discount'));
	var planType = $('#planType').val();
	// shows no approver users
	if (planType === 'PER_USER') {
		if (isNaN(numberUserEvent)) {
			var approvalUser = 'no approver users ';
			$("#approverUser").html(approvalUser);
		} else {
			var approvalUser = numberUserEvent + ' approver users ';
			$("#approverUser").html(approvalUser);
		}		
	}
	
	console.log("numberUserEvent :" + numberUserEvent + " selectedMonths :" + selectedMonths + " montlhyDiscount :" + montlhyDiscount);
	var currencyCode = $('#currencyId').val();
	var promoCode = $('#promoCode').val();
	ajaxCallOnChangeFields(numberUserEvent, promoCode, selectedMonths, montlhyDiscount, currencyCode, planType);
}

function ajaxCallOnChangeFields(numberUserEvent, promoCode, selectedMonths, montlhyDiscount, currencyCode, planType) {
	var basePrice = $('#basePrice').val();
	var baseUsers = $('#baseUsers').val();
	var taxFormt = parseFloat($('#renewTaxFormt').val());
	if (promoCode !== 'undefined' && promoCode !== '') {
		var header = $("meta[name='_csrf_header']").attr("content");
		var token = $("meta[name='_csrf']").attr("content");
		$.ajax({ type : "GET", url : getContextPath() + "/buyer/billing/getPromoCode", data : { promoCode : promoCode, }, beforeSend : function(xhr) {
			$('#loading').show();
			xhr.setRequestHeader(header, token);
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		}, complete : function() {
			$('#loading').hide();
		}, success : function(data) {
			// console.log(data);
			$('#rangeTable tr').each(function() {
				var rangeId = $(this).children('td:first').attr('data-id');
				var start = parseInt($(this).children('td:first').attr('data-start'));
				var end = parseInt($(this).children('td:first').attr('data-end'));
				var price = parseInt($(this).children('td:first').attr('data-price'));
				// console.log("start :" + start+ " End :"+ end + " Price :"+ price +" noOfUser :"+ noOfUser);

				if (numberUserEvent >= start && numberUserEvent <= end) {
					// console.log("start1 :" + start+ " End1 :"+ end + " Price1 :"+ price +" noOfUser1 :"+ noOfUser);

					$('#rangeId').val(rangeId);
					$('#promoCodeId').val(data.id);
					
//					var baseFeeValue = 0;
			    	if(basePrice !== undefined ){
			    		if (planType === 'PER_USER') {
			    		
			    		basePrice = parseInt($('#basePrice').val());
			    		var baseFeeLabel = currencyCode + " "+ basePrice + " for " + baseUsers + " Users X " + selectedMonths + " Months";
			    		$("#baseFeeLabel").html(baseFeeLabel);
			    		
			    		baseFeeValue = (basePrice * selectedMonths);
				    	console.log("baseFeeValue :" + baseFeeValue);
				    	$("#baseFeeValue").html(ReplaceNumberWithCommas((baseFeeValue).toFixed(2)));
			    		$('#baseFeeTr').removeClass('flagvisibility');
			    		
			    		
			    		if(baseUsers >= numberUserEvent ){
				    		$('#totalFeeTr').addClass('flagvisibility');
				    	}else{
				    		numberUserEvent = numberUserEvent - baseUsers;
				    		var totalFeeLabel = currencyCode + " " + price + " X " + numberUserEvent +" Users X " + selectedMonths + " Months";
							$("#totalFeeLabel").html(totalFeeLabel);

				    		$('#totalFeeTr').removeClass('flagvisibility');
				    	}
			    		var totalFeeValue = (price * numberUserEvent * selectedMonths);
			    		var feeValue = '<input type="hidden" name="feeValue" value="' + totalFeeValue + '" >';
						$("#totalFeeValue").html(feeValue + ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
			    		var totalFeeAmount = baseFeeValue + totalFeeValue;
			    		}
			    	}else{
			    		
						var totalFeeLabel = currencyCode + " " + price + " X " + numberUserEvent;
						if (planType === 'PER_USER') {
							totalFeeLabel += " Users X " + selectedMonths + " Months";
						} else {
							totalFeeLabel += " Events ";
						}
						$("#totalFeeLabel").html(totalFeeLabel);

						var totalFeeValue = (price * numberUserEvent);
						if (planType === 'PER_USER') {
							totalFeeValue = totalFeeValue * selectedMonths;
						}

						// console.log("totalFeeValue :" + totalFeeValue);
						var feeValue = '<input type="hidden" name="feeValue" value="' + totalFeeValue + '" >';
						$("#totalFeeValue").html(feeValue + ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));

						var totalFeeAmount = totalFeeValue;
			    	}
			    	
					if (planType === 'PER_USER') {
						var totalFeeDiscountLabel = "Subscription Discount " + montlhyDiscount + "%";
						// console.log("totalFeeDiscountLabel :" + totalFeeDiscountLabel);
						$("#totalFeeDiscountLabel").html(totalFeeDiscountLabel);
						var totalFeeDiscountValue = (totalFeeAmount / 100) * montlhyDiscount;
						// console.log("totalFeeDiscountValue :"+ totalFeeDiscountValue);
						var feeDiscountValue = '<input type="hidden" name="feeDiscountValue" value="' + totalFeeDiscountValue + '" >';
						$("#totalFeeDiscountValue").html(feeDiscountValue + "-" + ReplaceNumberWithCommas((totalFeeDiscountValue).toFixed(2)));

//						totalFeeAmount = totalFeeAmount - totalFeeDiscountValue;
						
					} else {
						$('.subsDiscount').hide();
					}

					// console.log(data.promoDiscount);
					var totalFeePromoValue = 0;
					var totalFeePromoLabel = data.promoName;
					if (data.discountType !== 'undefined' && data.discountType === 'PERCENTAGE') {
						totalFeePromoValue = (totalFeeAmount / 100) * data.promoDiscount;
						totalFeePromoLabel += "-" + data.promoDiscount + " % OFF";
					} else {
						totalFeePromoValue = data.promoDiscount;
						totalFeePromoLabel += "- " + currencyCode + " " + data.promoDiscount + " OFF";
					}
					$('#totalFeePromoLabel').html(totalFeePromoLabel);
					var promoCodeDiscount = '<input type="hidden" name="promoCodeDiscount" value="' + totalFeePromoValue + '" >';
					$('#totalFeePromoValue').html(promoCodeDiscount + "-" + ReplaceNumberWithCommas((totalFeePromoValue).toFixed(2)));
					if (planType === 'PER_USER') {
						totalFeeAmount = (totalFeeAmount - totalFeeDiscountValue) - totalFeePromoValue;
					} else {
						totalFeeAmount = totalFeeAmount - totalFeePromoValue;
					}
					// console.log("totalFeeAmount : "+totalFeeAmount);
					var totalFeeAmountValue = '<input type="hidden" name="totalFeeAmount" value="' + totalFeeAmount + '" >';
					var totalTax = (totalFeeAmount * taxFormt)/100 ;
			    	totalFeeAmount = totalFeeAmount + totalTax;
			    	$('#renewTaxAmount').html(ReplaceNumberWithCommas((totalTax).toFixed(2)));
					
					$('#totalFeeAmount').html(totalFeeAmountValue + ReplaceNumberWithCommas((totalFeeAmount).toFixed(2)));
				}
			});

		}, error : function(request, textStatus, errorThrown) {
			$('#rangeTable tr').each(function() {
				var rangeId = $(this).children('td:first').attr('data-id');
				var start = parseInt($(this).children('td:first').attr('data-start'));
				var end = parseInt($(this).children('td:first').attr('data-end'));
				var price = parseInt($(this).children('td:first').attr('data-price'));
				// console.log("start :" + start+ " End :"+ end + " Price :"+ price +" noOfUser :"+ noOfUser);

				if (numberUserEvent >= start && numberUserEvent <= end) {
					// console.log("start1 :" + start+ " End1 :"+ end + " Price1 :"+ price +" noOfUser1 :"+ noOfUser);

					$('#rangeId').val(rangeId);
					var baseFeeValue = 0;
			    	if(basePrice !== undefined ){
			    		if (planType === 'PER_USER') {
			    		
			    		basePrice = parseInt($('#basePrice').val());
			    		var baseFeeLabel = currencyCode + " "+ basePrice + " for " + baseUsers + " Users X " + selectedMonths + " Months";
			    		$("#baseFeeLabel").html(baseFeeLabel);
			    		
			    		baseFeeValue = (basePrice * selectedMonths);
				    	console.log("baseFeeValue :" + baseFeeValue);
				    	$("#baseFeeValue").html(ReplaceNumberWithCommas((baseFeeValue).toFixed(2)));
			    		$('#baseFeeTr').removeClass('flagvisibility');
			    		
			    		
			    		if(baseUsers >= numberUserEvent ){
				    		$('#totalFeeTr').addClass('flagvisibility');
				    	}else{
				    		numberUserEvent = numberUserEvent - baseUsers;
				    		var totalFeeLabel = currencyCode + " " + price + " X " + numberUserEvent +" Users X " + selectedMonths + " Months";
							$("#totalFeeLabel").html(totalFeeLabel);

				    		$('#totalFeeTr').removeClass('flagvisibility');
				    	}
			    		var totalFeeValue = (price * numberUserEvent * selectedMonths);
			    		var feeValue = '<input type="hidden" name="feeValue" value="' + totalFeeValue + '" >';
						$("#totalFeeValue").html(feeValue + ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
			    		var totalFeeAmount = baseFeeValue + totalFeeValue;
			    		}
			    	}else{
			    		
						var totalFeeLabel = currencyCode + " " + price + " X " + numberUserEvent;
						if (planType === 'PER_USER') {
							totalFeeLabel += " Users X " + selectedMonths + " Months";
						} else {
							totalFeeLabel += " Events ";
						}
						$("#totalFeeLabel").html(totalFeeLabel);

						var totalFeeValue = (price * numberUserEvent);
						if (planType === 'PER_USER') {
							totalFeeValue = totalFeeValue * selectedMonths;
						}

						// console.log("totalFeeValue :" + totalFeeValue);
						var feeValue = '<input type="hidden" name="feeValue" value="' + totalFeeValue + '" >';
						$("#totalFeeValue").html(feeValue + ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));

						var totalFeeAmount = totalFeeValue;
			    	}
					
					
//					var totalFeeLabel = currencyCode + " " + price + " X " + numberUserEvent;
//					if (planType === 'PER_USER') {
//						totalFeeLabel += " Users X " + selectedMonths + " Months";
//					} else {
//						totalFeeLabel += " Events ";
//					}
//					$("#totalFeeLabel").html(totalFeeLabel);
//
//					var totalFeeValue = (price * numberUserEvent);
//					if (planType === 'PER_USER') {
//						totalFeeValue = totalFeeValue * selectedMonths;
//					}
//
//					// console.log("totalFeeValue :" + totalFeeValue);
//					var feeValue = '<input type="hidden" name="feeValue" value="' + totalFeeValue + '" >';
//					$("#totalFeeValue").html(feeValue + ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
//
//					var totalFeeAmount = totalFeeValue;
					if (planType === 'PER_USER') {
						var totalFeeDiscountLabel = "Subscription Discount " + montlhyDiscount + "%";
						// console.log("totalFeeDiscountLabel :" + totalFeeDiscountLabel);
						$("#totalFeeDiscountLabel").html(totalFeeDiscountLabel);
						var totalFeeDiscountValue = (totalFeeAmount / 100) * montlhyDiscount;
						// console.log("totalFeeDiscountValue :"+ totalFeeDiscountValue);
						var feeDiscountValue = '<input type="hidden" name="feeDiscountValue" value="' + totalFeeDiscountValue + '" >';
						$("#totalFeeDiscountValue").html(feeDiscountValue + "-" + ReplaceNumberWithCommas((totalFeeDiscountValue).toFixed(2)));

						totalFeeAmount = totalFeeAmount - totalFeeDiscountValue;
					} else {
						$('.subsDiscount').hide();
					}
					$('#totalFeePromoLabel').html("No Promotional Code");
					$('#totalFeePromoValue').html("0.00");

					// console.log("totalFeeAmount : "+totalFeeAmount);
					var totalFeeAmountValue = '<input type="hidden" name="totalFeeAmount" value="' + totalFeeAmount + '" >';
					
					var totalTax = (totalFeeAmount * taxFormt)/100 ;
			    	totalFeeAmount = totalFeeAmount + totalTax;
			    	$('#renewTaxAmount').html(ReplaceNumberWithCommas((totalTax).toFixed(2)));
					
					$('#totalFeeAmount').html(totalFeeAmountValue + ReplaceNumberWithCommas((totalFeeAmount).toFixed(2)));
				}
			});
			$('#promoError').html('<span class="help-block form-error">' + request.getResponseHeader('error') + '</span>').addClass('has-error');
		}

		});
	} else {
		$('#rangeTable tr').each(function() {
			var rangeId = $(this).children('td:first').attr('data-id');
			var start = parseInt($(this).children('td:first').attr('data-start'));
			var end = parseInt($(this).children('td:first').attr('data-end'));
			var price = parseInt($(this).children('td:first').attr('data-price'));
			// console.log("start :" + start+ " End :"+ end + " Price :"+ price +" noOfUser :"+ noOfUser);

			if (numberUserEvent >= start && numberUserEvent <= end) {
				// console.log("start1 :" + start+ " End1 :"+ end + " Price1 :"+ price +" noOfUser1 :"+ noOfUser);

				$('#rangeId').val(rangeId);
				
				
				var baseFeeValue = 0;
		    	if(basePrice !== undefined ){
		    		if (planType === 'PER_USER') {
		    		
		    		basePrice = parseInt($('#basePrice').val());
		    		var baseFeeLabel = currencyCode + " "+ basePrice + " for " + baseUsers + " Users X " + selectedMonths + " Months";
		    		$("#baseFeeLabel").html(baseFeeLabel);
		    		
		    		baseFeeValue = (basePrice * selectedMonths);
			    	console.log("baseFeeValue :" + baseFeeValue);
			    	$("#baseFeeValue").html(ReplaceNumberWithCommas((baseFeeValue).toFixed(2)));
		    		$('#baseFeeTr').removeClass('flagvisibility');
		    		
		    		
		    		if(baseUsers >= numberUserEvent ){
			    		$('#totalFeeTr').addClass('flagvisibility');
			    	}else{
			    		numberUserEvent = numberUserEvent - baseUsers;
			    		var totalFeeLabel = currencyCode + " " + price + " X " + numberUserEvent +" Users X " + selectedMonths + " Months";
						$("#totalFeeLabel").html(totalFeeLabel);

			    		$('#totalFeeTr').removeClass('flagvisibility');
			    	}
		    		var totalFeeValue = (price * numberUserEvent * selectedMonths);
		    		var feeValue = '<input type="hidden" name="feeValue" value="' + totalFeeValue + '" >';
					$("#totalFeeValue").html(feeValue + ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
		    		var totalFeeAmount = baseFeeValue + totalFeeValue;
		    		}
		    	}else{
		    		
					var totalFeeLabel = currencyCode + " " + price + " X " + numberUserEvent;
					if (planType === 'PER_USER') {
						totalFeeLabel += " Users X " + selectedMonths + " Months";
					} else {
						totalFeeLabel += " Events ";
					}
					$("#totalFeeLabel").html(totalFeeLabel);

					var totalFeeValue = (price * numberUserEvent);
					if (planType === 'PER_USER') {
						totalFeeValue = totalFeeValue * selectedMonths;
					}

					// console.log("totalFeeValue :" + totalFeeValue);
					var feeValue = '<input type="hidden" name="feeValue" value="' + totalFeeValue + '" >';
					$("#totalFeeValue").html(feeValue + ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));

					var totalFeeAmount = totalFeeValue;
		    	}

				if (planType === 'PER_USER') {
					var totalFeeDiscountLabel = "Subscription Discount " + montlhyDiscount + "%";
					// console.log("totalFeeDiscountLabel :" + totalFeeDiscountLabel);
					$("#totalFeeDiscountLabel").html(totalFeeDiscountLabel);
					var totalFeeDiscountValue = (totalFeeAmount / 100) * montlhyDiscount;
					// console.log("totalFeeDiscountValue :"+ totalFeeDiscountValue);
					var feeDiscountValue = '<input type="hidden" name="feeDiscountValue" value="' + totalFeeDiscountValue + '" >';
					$("#totalFeeDiscountValue").html(feeDiscountValue + "-" + ReplaceNumberWithCommas((totalFeeDiscountValue).toFixed(2)));

					totalFeeAmount = totalFeeAmount - totalFeeDiscountValue;
				} else {
					$('.subsDiscount').hide();
				}
				$('#totalFeePromoLabel').html("No Promotional Code");
				$('#totalFeePromoValue').html("0.00");

				// console.log("totalFeeAmount : "+totalFeeAmount);
				var totalFeeAmountValue = '<input type="hidden" name="totalFeeAmount" value="' + totalFeeAmount + '" >';
				
				var totalTax = (totalFeeAmount * taxFormt)/100 ;
		    	totalFeeAmount = totalFeeAmount + totalTax;
		    	$('#renewTaxAmount').html(ReplaceNumberWithCommas((totalTax).toFixed(2)));
				
				$('#totalFeeAmount').html(totalFeeAmountValue + ReplaceNumberWithCommas((totalFeeAmount).toFixed(2)));
			}
		});
	}
}

function updateSubscription(data) {
	var formHtml = '<h3>Update ' + data.plan.planName + ' Subscription</h3>';

	formHtml += '<button class="close for-absulate" type="button" data-dismiss="modal">x</button>';
	formHtml += '<input type="hidden" id="updateCurrencyId" value="' + data.plan.currencyCode + '">';
	formHtml += '<input type="hidden" id="updatePlanType" value="' + data.planType + '">';
	if (typeof data.planPeriod !== 'undefined' && data.planPeriod !== '') {
		formHtml += '<input type="hidden" id="updateSelectPeriodId" value="' + data.planPeriod.id + '">';
	}

	formHtml += '<table id="updateRangeTable" class="marg-top-20">';
var lastRange = 0;
	$(data.plan.rangeList).each(function(i, range) {
		lastRange = range.rangeEnd;
		formHtml += '<tr class="row">';
		formHtml += '<td class="col-md-6" data-id="' + range.id + '" data-start="' + range.rangeStart + '" data-end="' + range.rangeEnd + '" data-price="' + range.price + '">';
		formHtml += '<h4 style="font-weight: bold; color: #3f96d8;">' + range.displayLabel + '' + (data.plan.planType === 'PER_USER' ? " Users" : " Events") + '</h4>';
		formHtml += '</td>';
		formHtml += '<td class="col-md-6"> ' + data.plan.currencyCode;
		formHtml += '<span style="font-size: 23px;">';
		
		console.log("data plan Base price : "+ data.plan.basePrice);
		var itemTax = 0;
		if(data.plan.basePrice !== undefined && i == 0){
			formHtml += '<input type="hidden" id="updateBasePrice" value="'+ data.plan.basePrice +'">';
			formHtml += '<input type="hidden" id="updateBaseUsers" value="'+ data.plan.baseUsers +'">';
			formHtml += (data.plan.basePrice).toFixed(2);
			
			itemTax=parseFloat(((data.plan.basePrice).toFixed(2)*(data.plan.tax).toFixed(2))/100)+ parseFloat((data.plan.basePrice).toFixed(2));
		}else{
			formHtml += (range.price).toFixed(2);
			itemTax=parseFloat(((range.price).toFixed(2)*(data.plan.tax).toFixed(2))/100)+ parseFloat((range.price).toFixed(2));
		}
		formHtml += '</span>';
		if(data.plan.basePrice !== undefined && i == 0){
			formHtml += (data.plan.planType === 'PER_USER' ? " /month" : " /event");
		}else{
			formHtml += (data.plan.planType === 'PER_USER' ? " /user/month" : " /event");
		}

		
		formHtml += '<br><div">';
		
		formHtml += '('+ itemTax +' inclusive of '+(data.plan.tax).toFixed(2)+'% GST)';
		
		formHtml += '</div>';
		formHtml += '</td>';
		formHtml += '</tr>';
		

		
		
	});
	formHtml += '</table>';

	formHtml += '<form id="idUpdateSubscribeForm" action="' + getContextPath() + '/buyer/billing/updateSubscription/' + data.id + '" method="post">';
	formHtml += '<input type="hidden" id="lastRange" value="'+ lastRange +'">';
	formHtml += '<div class="row marg-top-20">';
	formHtml += '<div class="col-md-4">';
	formHtml += '<label>Additional ' + (data.plan.planType === 'PER_USER' ? "User" : "Event") + '</label>';
	formHtml += '</div>';
	formHtml += '<div class="col-md-8">';
	formHtml += '<input type="hidden" name="oldNumberUserEvent" id="oldNumberUserEvent" value="' + (data.plan.planType === 'PER_USER' ? data.userQuantity : data.eventQuantity) + '" >';
	formHtml += '<input type="text" class="form-control" name="updateNumberUserEvent" id="updateNumberUserEvent" value="' + (data.plan.planType === 'PER_USER' ? data.userQuantity : data.eventQuantity)
			+ '" data-validation="required length number" data-validation-length="1-3">';
	formHtml += '<input type="hidden" name="updateRangeId" id="updateRangeId">';
	formHtml += '<span id="updateNumberError"></span>';
	formHtml += '</div>';
	formHtml += '</div>';

	formHtml += '<div class="row marg-top-20">';
	formHtml += '<div class="col-md-4"><label>Promo Code</label></div>';
	formHtml += '<div class="col-md-8">';
	formHtml += '<input type="hidden" name="updatePromoCodeId" id="updatePromoCodeId">';
	formHtml += '<input type="text" class="form-control" id="updatePromoCode" name="updatePromoCode">';
	formHtml += '<span id="updatePromoError"></span>';
	formHtml += '</div></div>';

	formHtml += '<div class="row marg-top-20 flagvisibility" id="updateBaseFeeTr" >';
	formHtml += '<div class="col-md-6">	<label id="updateBaseFeeLabel"></label></div>';
	formHtml += '<div class="col-md-6"><label id="updateBaseFeeValue"></label></div>';
	formHtml += '</div>';
	
	formHtml += '<div class="row marg-top-20" id="updateTotalFeeTr" >';
	formHtml += '<div class="col-md-6">	<label id="updateTotalFeeLabel"></label></div>';
	formHtml += '<div class="col-md-6"><label id="updateTotalFeeValue"></label></div>';
	formHtml += '</div>';

	/*formHtml += '<div class="row marg-top-20 subsDiscount">';
	formHtml += '<div class="col-md-6"><label id="updateTotalFeeDiscountLabel"></label></div>';
	formHtml += '<div class="col-md-6"><label id="updateTotalFeeDiscountValue"> </label></div>';
	formHtml += '</div>';*/
	
	//label that shows approver user
	if(data.plan.planType === 'PER_USER'){
	formHtml += '<div class="row marg-top-20">';
	formHtml += '<div class="col-md-6"><label id="updateTotalFeeApproverLabel">'+data.userQuantity +' approver users </label></div>';
	formHtml += '<div class="col-md-6"><label > 0.00</label></div>';
	formHtml += '</div>';
	}
	formHtml += '<div class="row marg-top-20">';
	formHtml += '<div class="col-md-6"><label id="updateTotalFeePromoLabel"></label></div>';
	formHtml += '<div class="col-md-6"><label id="updateTotalFeePromoValue"> </label></div>';
	formHtml += '</div>';

	formHtml += '<div class="row marg-top-20">';
	formHtml += '<div class="col-md-6"><label>Tax '+data.plan.tax+'% GST</label></div>';
	formHtml += '<div class="col-md-6"><label id="updateTaxAmount"></label></div>';
	formHtml += '</div>';
	
	formHtml += '<div class="row marg-top-20">';
	formHtml += '<div class="col-md-6" style="padding-right: 0px;"><label>Total Fee</label><label class="pull-right">'+data.currencyCode+'</label></div>';
	formHtml += '<div class="col-md-6"><label id="updateTotalFeeAmount"></label></div>';
	formHtml += '</div>';

	formHtml += '<div class="row marg-top-20">';
	formHtml += '<div class="col-md-3">&nbsp;</div><div class="col-md-6"><div id="idUpdateButtonHolder"></div></div><div class="col-md-3">&nbsp;</div>';
	formHtml += '</div>';
	formHtml += '<div class="row">';
	formHtml += '<div class="col-md-2">&nbsp;</div><div class="col-md-10">';
	formHtml += '<img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/cc-badges-ppppcmcvdam.png" alt="Credit Card Badges">';
	formHtml += '</div>';
	formHtml += '</div>';
	formHtml += '</form>';
	// it shows note for approver user offer
	if(data.plan.planType === 'PER_USER'){
	formHtml += '<div class="col-md-12 " style="margin-top: 15px;">';
	formHtml += '<span style="color: red;"> * </span>';
	formHtml += 'Note : For	<span id="userNo1">'+data.userQuantity +' users</span> you will get	<span id="appNo1">'+data.userQuantity +' approver users</span>	free.';
	formHtml += '</div>';
	}
	$('#UpdateSubsDialogText').html(formHtml);

	$("#idUpdateSubsDialog").modal();

	window.paypalCheckoutReady = function() {
		paypal.checkout.setup('${merchantId}', { environment : '${paypalEnvironment}', container : 'idUpdateSubscribeForm', condition : function() {
			return $('#idUpdateSubscribeForm').isValid();
		},
		// button: 'placeOrderBtn'
		buttons : [ { container : 'idUpdateButtonHolder', type : 'checkout', color : 'blue', size : 'medium', shape : 'rect' } ] });
	};
}

function calculateUpdateTotalFee(chargeMonths) {
	var updateNumberUserEvent = parseInt($('#updateNumberUserEvent').val());
	var TotalNumberUserEvent = parseInt($('#oldNumberUserEvent').val()) + updateNumberUserEvent;
	var updatePlanType = $('#updatePlanType').val();
//	console.log("updateNumberUserEvent :" + updateNumberUserEvent + " updatePlanType :" + updatePlanType);
	var updateCurrencyCode = $('#updateCurrencyId').val();
	var updatePromoCode = $('#updatePromoCode').val();
//console.log("updateCurrencyCode :" + updateCurrencyCode + " updatePromoCode :" + updatePromoCode);
	var updateSelectedMonths = 0;
	
	if (updatePlanType === 'PER_USER') {
		updateSelectedMonths = parseInt(chargeMonths);
	}
//	console.log("updateSelectedMonths :" + updateSelectedMonths);
	ajaxCallOnChangeFieldsForUpdateSubs(updateNumberUserEvent, updatePromoCode, updateSelectedMonths, updateCurrencyCode, updatePlanType, TotalNumberUserEvent);
}

function ajaxCallOnChangeFieldsForUpdateSubs(updateNumberUserEvent, updatePromoCode, updateSelectedMonths, updateCurrencyCode, updatePlanType, TotalNumberUserEvent) {
	var basePrice = $('#updateBasePrice').val();
	var baseUsers = $('#updateBaseUsers').val();
	var taxFormt = parseFloat($('#taxFormt').val());
	console.log("basePrice :" + basePrice + "== baseUsers :" + baseUsers);
	var lastRange = parseInt($('#lastRange').val());
	if (updatePromoCode !== 'undefined' && updatePromoCode !== '') {
		var header = $("meta[name='_csrf_header']").attr("content");
		var token = $("meta[name='_csrf']").attr("content");
		$.ajax({ type : "GET", url : getContextPath() + "/buyer/billing/getPromoCode", data : { 'promoCode' : updatePromoCode }, beforeSend : function(xhr) {
			$('#loading').show();
			xhr.setRequestHeader(header, token);
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
		}, complete : function() {
			$('#loading').hide();
		}, success : function(data) {
			// console.log(data);
			$('#updateRangeTable tr').each(function() {
				var rangeId = $(this).children('td:first').attr('data-id');
				var start = parseInt($(this).children('td:first').attr('data-start'));
				var end = parseInt($(this).children('td:first').attr('data-end'));
				var price = parseInt($(this).children('td:first').attr('data-price'));
				 console.log("start :" + start+ " End :"+ end + " Price :"+ price +" noOfUser :"+ TotalNumberUserEvent);

				if (TotalNumberUserEvent >= start && TotalNumberUserEvent <= end) {
//					console.log("start1 :" + start+ " End1 :"+ end + " Price1 :"+ price +" noOfUser1 :"+ updateNumberUserEvent);

					$('#updateRangeId').val(rangeId);
					$('#updatePromoCodeId').val(data.id);
					
					var baseFeeValue = 0;
			    	if(basePrice !== undefined ){
			    		if (updatePlanType === 'PER_USER') {
			    		
//			    		basePrice = parseInt(basePrice);
//			    		var baseFeeLabel = updateCurrencyCode + " "+ basePrice + " for " + baseUsers + " Users X " + updateSelectedMonths + " Months";
//			    		$("#updateBaseFeeLabel").html(baseFeeLabel);
//			    		
//			    		baseFeeValue = (basePrice * updateSelectedMonths);
//				    	console.log("baseFeeValue :" + baseFeeValue);
//				    	$("#updateBaseFeeValue").html(ReplaceNumberWithCommas((baseFeeValue).toFixed(2)));
//			    		$('#updateBaseFeeTr').removeClass('flagvisibility');
//			    		
//			    		
//			    		if(baseUsers >= TotalNumberUserEvent ){
//				    		$('#updateTotalFeeTr').addClass('flagvisibility');
//				    	}else{
//				    		updateNumberUserEvent = TotalNumberUserEvent - baseUsers;
				    		var totalFeeLabel = updateCurrencyCode + " " + price + " X " + updateNumberUserEvent +" Users X " + updateSelectedMonths + " Months";
							$("#updateTotalFeeLabel").html(totalFeeLabel);

				    		$('#updateTotalFeeTr').removeClass('flagvisibility');
//				    	}
			    		var totalFeeValue = (price * updateNumberUserEvent * updateSelectedMonths);
			    		var feeValue = '<input type="hidden" name="updateFeeValue" value="' + totalFeeValue + '" >';
			    		var chargeMonths = '<input type="hidden" name="chargeMonths" value="' + updateSelectedMonths + '" > ';
						$("#updateTotalFeeValue").html(chargeMonths + feeValue + ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
			    		var totalFeeAmount = baseFeeValue + totalFeeValue;
			    		console.log("totalFeeAmount :" + totalFeeAmount );
			    		}
			    	}else{
			    		
						var totalFeeLabel = updateCurrencyCode + " " + price + " X " + updateNumberUserEvent;
						if (updatePlanType === 'PER_USER') {
							totalFeeLabel += " Users X " + updateSelectedMonths + " Months";
						} else {
							totalFeeLabel += " Events ";
						}
						$("#updateTotalFeeLabel").html(totalFeeLabel);

						var totalFeeValue = (price * updateNumberUserEvent);
						if (updatePlanType === 'PER_USER') {
							totalFeeValue = totalFeeValue * updateSelectedMonths;
						}

						var feeValue = '<input type="hidden" name="updateFeeValue" value="' + totalFeeValue + '" >';
						var chargeMonths = '<input type="hidden" name="chargeMonths" value="' + updateSelectedMonths + '" > ';
						$("#updateTotalFeeValue").html(chargeMonths + feeValue + ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));

						var totalFeeAmount = totalFeeValue;
						console.log("totalFeeAmount :" + totalFeeAmount);
			    	}
					
//					var totalFeeLabel = updateCurrencyCode + " " + price + " X " + updateNumberUserEvent;
//					if (updatePlanType === 'PER_USER') {
//						totalFeeLabel += " Users X " + updateSelectedMonths + " Months";
//					} else {
//						totalFeeLabel += " Events ";
//					}
//					$("#updateTotalFeeLabel").html(totalFeeLabel);
//
//					var totalFeeValue = (price * updateNumberUserEvent);
//					if (updatePlanType === 'PER_USER') {
//						totalFeeValue = totalFeeValue * updateSelectedMonths;
//					}
//
//					// console.log("totalFeeValue :" + totalFeeValue);
//					var feeValue = '<input type="hidden" name="updateFeeValue" value="' + totalFeeValue + '" >';
//					var chargeMonths = '<input type="hidden" name="chargeMonths" value="' + updateSelectedMonths + '" > ';
//					$("#updateTotalFeeValue").html(chargeMonths + feeValue + ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
//
//					var totalFeeAmount = totalFeeValue;
//					
					
					
					var totalFeePromoValue = 0;
					var totalFeePromoLabel = data.promoName;
					if (data.discountType !== 'undefined' && data.discountType === 'PERCENTAGE') {
						totalFeePromoValue = (totalFeeAmount / 100) * data.promoDiscount;
						totalFeePromoLabel += "-" + data.promoDiscount + " % OFF";
					} else {
						totalFeePromoValue = data.promoDiscount;
						totalFeePromoLabel += "- " + updateCurrencyCode + " " + data.promoDiscount + " OFF";
					}
					$('#updateTotalFeePromoLabel').html(totalFeePromoLabel);
					var promoCodeDiscount = '<input type="hidden" name="updatePromoCodeDiscount" value="' + totalFeePromoValue + '" >';
					$('#updateTotalFeePromoValue').html(promoCodeDiscount + "-" + ReplaceNumberWithCommas((totalFeePromoValue).toFixed(2)));
					if (planType === 'PER_USER') {
						totalFeeAmount = (totalFeeAmount - totalFeeDiscountValue) - totalFeePromoValue;
					} else {
						totalFeeAmount = totalFeeAmount - totalFeePromoValue;
					}
					
					// console.log("totalFeeAmount : "+totalFeeAmount);
					var totalFeeAmountValue = '<input type="hidden" name="updateTotalFeeAmount" value="' + totalFeeAmount + '" >';
					
					var totalTax = (totalFeeAmount * taxFormt)/100 ;
			    	totalFeeAmount = totalFeeAmount + totalTax;
			    	$('#updateTaxAmount').html(ReplaceNumberWithCommas((totalTax).toFixed(2)));
					
					$('#updateTotalFeeAmount').html(totalFeeAmountValue + ReplaceNumberWithCommas((totalFeeAmount).toFixed(2)));
				}else if(TotalNumberUserEvent >= lastRange){
					$('#updateNumberError').html('<span class="help-block form-error">Total user should be less then '+lastRange+'</span>').addClass('has-error');
					return false;
				}
			});

		}, error : function(request, textStatus, errorThrown) {
			$('#updateRangeTable tr').each(function() {
				var rangeId = $(this).children('td:first').attr('data-id');
				var start = parseInt($(this).children('td:first').attr('data-start'));
				var end = parseInt($(this).children('td:first').attr('data-end'));
				var price = parseInt($(this).children('td:first').attr('data-price'));
				// console.log("start :" + start+ " End :"+ end + " Price :"+ price +" noOfUser :"+ noOfUser);

				if (TotalNumberUserEvent >= start && TotalNumberUserEvent <= end) {
//					console.log("start1 :" + start+ " End1 :"+ end + " Price1 :"+ price +" noOfUser1 :"+ updateNumberUserEvent);

					$('#updateRangeId').val(rangeId);
					
					var baseFeeValue = 0;
			    	if(basePrice !== undefined ){
			    		if (updatePlanType === 'PER_USER') {
			    		
//			    		basePrice = parseInt(basePrice);
//			    		var baseFeeLabel = updateCurrencyCode + " "+ basePrice + " for " + baseUsers + " Users X " + updateSelectedMonths + " Months";
//			    		$("#updateBaseFeeLabel").html(baseFeeLabel);
//			    		
//			    		baseFeeValue = (basePrice * updateSelectedMonths);
//				    	console.log("baseFeeValue :" + baseFeeValue);
//				    	$("#updateBaseFeeValue").html(ReplaceNumberWithCommas((baseFeeValue).toFixed(2)));
//			    		$('#updateBaseFeeTr').removeClass('flagvisibility');
//			    		
//			    		
//			    		if(baseUsers >= TotalNumberUserEvent ){
//				    		$('#updateTotalFeeTr').addClass('flagvisibility');
//				    	}else{
//				    		updateNumberUserEvent = TotalNumberUserEvent - baseUsers;
				    		var totalFeeLabel = updateCurrencyCode + " " + price + " X " + updateNumberUserEvent +" Users X " + updateSelectedMonths + " Months";
							$("#updateTotalFeeLabel").html(totalFeeLabel);

				    		$('#updateTotalFeeTr').removeClass('flagvisibility');
//				    	}
			    		var totalFeeValue = (price * updateNumberUserEvent * updateSelectedMonths);
			    		var feeValue = '<input type="hidden" name="updateFeeValue" value="' + totalFeeValue + '" >';
			    		var chargeMonths = '<input type="hidden" name="chargeMonths" value="' + updateSelectedMonths + '" > ';
						$("#updateTotalFeeValue").html(chargeMonths + feeValue + ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
			    		var totalFeeAmount = baseFeeValue + totalFeeValue;
//			    		console.log("totalFeeAmount :" + totalFeeAmount );
			    		}
			    	}else{
			    		
						var totalFeeLabel = updateCurrencyCode + " " + price + " X " + updateNumberUserEvent;
						if (updatePlanType === 'PER_USER') {
							totalFeeLabel += " Users X " + updateSelectedMonths + " Months";
						} else {
							totalFeeLabel += " Events ";
						}
						$("#updateTotalFeeLabel").html(totalFeeLabel);

						var totalFeeValue = (price * updateNumberUserEvent);
						if (updatePlanType === 'PER_USER') {
							totalFeeValue = totalFeeValue * updateSelectedMonths;
						}

//						 console.log("totalFeeValue :" + totalFeeValue);
						var feeValue = '<input type="hidden" name="updateFeeValue" value="' + totalFeeValue + '" >';
						var chargeMonths = '<input type="hidden" name="chargeMonths" value="' + updateSelectedMonths + '" > ';
						$("#updateTotalFeeValue").html(chargeMonths + feeValue + ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));

						var totalFeeAmount = totalFeeValue;
			    	}
			    	
//					var totalFeeLabel = updateCurrencyCode + " " + price + " X " + updateNumberUserEvent;
//					if (updatePlanType === 'PER_USER') {
//						totalFeeLabel += " Users X " + updateSelectedMonths + " Months";
//					} else {
//						totalFeeLabel += " Events ";
//					}
//					$("#updateTotalFeeLabel").html(totalFeeLabel);
//
//					var totalFeeValue = (price * updateNumberUserEvent);
//					if (updatePlanType === 'PER_USER') {
//						totalFeeValue = totalFeeValue * updateSelectedMonths;
//					}
//
//					// console.log("totalFeeValue :" + totalFeeValue);
//					var feeValue = '<input type="hidden" name="updateFeeValue" value="' + totalFeeValue + '" >';
//					var chargeMonths = '<input type="hidden" name="chargeMonths" value="' + updateSelectedMonths + '" > ';
//					$("#updateTotalFeeValue").html(chargeMonths + feeValue + ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
//
//					var totalFeeAmount = totalFeeValue;
//					
					$('#updateTotalFeePromoLabel').html("No Promotional Code");
					$('#updateTotalFeePromoValue').html("0.00");

					// console.log("totalFeeAmount : "+totalFeeAmount);
					var totalFeeAmountValue = '<input type="hidden" name="updateTotalFeeAmount" value="' + totalFeeAmount + '" >';
					
					var totalTax = (totalFeeAmount * taxFormt)/100 ;
			    	totalFeeAmount = totalFeeAmount + totalTax;
			    	$('#updateTaxAmount').html(ReplaceNumberWithCommas((totalTax).toFixed(2)));
					
					$('#updateTotalFeeAmount').html(totalFeeAmountValue + ReplaceNumberWithCommas((totalFeeAmount).toFixed(2)));
				}else if(TotalNumberUserEvent >= lastRange){
					$('#updateNumberError').html('<span class="help-block form-error">Total user should be less then '+lastRange+'</span>').addClass('has-error');
					return false;
				}
			});
			$('#updatePromoError').html('<span class="help-block form-error">' + request.getResponseHeader('error') + '</span>').addClass('has-error');
		}

		});
	} else {
		$('#updateRangeTable tr').each(function() {
			var rangeId = $(this).children('td:first').attr('data-id');
			var start = parseInt($(this).children('td:first').attr('data-start'));
			var end = parseInt($(this).children('td:first').attr('data-end'));
			var price = parseInt($(this).children('td:first').attr('data-price'));
			 console.log("start :" + start+ " End :"+ end + " Price :"+ price +" TotalNumberUserEvent :"+ TotalNumberUserEvent);

			if (TotalNumberUserEvent >= start && TotalNumberUserEvent <= end) {
//				console.log("start1 :" + start+ " End1 :"+ end + " Price1 :"+ price +" noOfUser1 :"+ updateNumberUserEvent);

				$('#updateRangeId').val(rangeId);
				
				
				var baseFeeValue = 0;
		    	if(basePrice !== undefined ){
		    		if (updatePlanType === 'PER_USER') {
		    		
//		    		basePrice = parseInt(basePrice);
//		    		var baseFeeLabel = updateCurrencyCode + " "+ basePrice + " for " + baseUsers + " Users X " + updateSelectedMonths + " Months";
//		    		$("#updateBaseFeeLabel").html(baseFeeLabel);
//		    		
//		    		baseFeeValue = (basePrice * updateSelectedMonths);
//			    	console.log("baseFeeValue :" + baseFeeValue);
//			    	$("#updateBaseFeeValue").html(ReplaceNumberWithCommas((baseFeeValue).toFixed(2)));
//		    		$('#updateBaseFeeTr').removeClass('flagvisibility');
		    		
		    		
//		    		if(baseUsers >= TotalNumberUserEvent ){
//			    		$('#updateTotalFeeTr').addClass('flagvisibility');
//			    	}else{
			    		//updateNumberUserEvent = TotalNumberUserEvent - baseUsers;
			    		var totalFeeLabel = updateCurrencyCode + " " + price + " X " + updateNumberUserEvent +" Users X " + updateSelectedMonths + " Months";
						$("#updateTotalFeeLabel").html(totalFeeLabel);

			    		$('#updateTotalFeeTr').removeClass('flagvisibility');
//			    	}
			    		
			    		
		    		console.log("price :" + price );
		    		var totalFeeValue = (price * updateNumberUserEvent * updateSelectedMonths);
		    		var feeValue = '<input type="hidden" name="updateFeeValue" value="' + totalFeeValue + '" >';
		    		var chargeMonths = '<input type="hidden" name="chargeMonths" value="' + updateSelectedMonths + '" > ';
					$("#updateTotalFeeValue").html(chargeMonths + feeValue + ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));
		    		var totalFeeAmount = baseFeeValue + totalFeeValue;
		    		console.log("totalFeeAmount :" + totalFeeAmount );
		    		}
		    	}else{
		    		
					var totalFeeLabel = updateCurrencyCode + " " + price + " X " + updateNumberUserEvent;
					if (updatePlanType === 'PER_USER') {
						totalFeeLabel += " Users X " + updateSelectedMonths + " Months";
					} else {
						totalFeeLabel += " Events ";
					}
					$("#updateTotalFeeLabel").html(totalFeeLabel);

					var totalFeeValue = (price * updateNumberUserEvent);
					if (updatePlanType === 'PER_USER') {
						totalFeeValue = totalFeeValue * updateSelectedMonths;
					}

//					 console.log("totalFeeValue :" + totalFeeValue);
					var feeValue = '<input type="hidden" name="updateFeeValue" value="' + totalFeeValue + '" >';
					var chargeMonths = '<input type="hidden" name="chargeMonths" value="' + updateSelectedMonths + '" > ';
					$("#updateTotalFeeValue").html(chargeMonths + feeValue + ReplaceNumberWithCommas((totalFeeValue).toFixed(2)));

					var totalFeeAmount = totalFeeValue;
		    	}
				
				$('#updateTotalFeePromoLabel').html("No Promotional Code");
				$('#updateTotalFeePromoValue').html("0.00");

				
				
				var totalFeeAmountValue = '<input type="hidden" name="updateTotalFeeAmount" value="' + totalFeeAmount + '" >';
				
				var totalTax = (totalFeeAmount * taxFormt)/100 ;
		    	totalFeeAmount = totalFeeAmount + totalTax;
		    	$('#updateTaxAmount').html(ReplaceNumberWithCommas((totalTax).toFixed(2)));
		    		
				
				 console.log("totalFeeAmount : "+totalFeeAmount);
				$('#updateTotalFeeAmount').html(totalFeeAmountValue + ReplaceNumberWithCommas((totalFeeAmount).toFixed(2)));
			}
			else if(TotalNumberUserEvent >= lastRange){
				$('#updateNumberError').html('<span class="help-block form-error">Total user should be less then '+lastRange+'</span>').addClass('has-error');
				return false;
			}
		});
	}
}

function updateFields(chargeMonths){
	$('#updatePromoCode').on('change', function(e) {
		var updateNumberUserEvent = parseInt($('#updateNumberUserEvent').val());
		$('#updatePromoError').html('').removeClass('has-error');
		$('#updateNumberError').html('').removeClass('has-error');
		if ($('#updateNumberUserEvent').val() === 'undefined' || $('#updateNumberUserEvent').val() === '0') {
			// console.log("numberUserEvent :"+$(this).val());
			$('#updateNumberError').html('<span class="help-block form-error">Number should be greater then 0</span>').addClass('has-error');
			return false;
		}
		if (isNaN(updateNumberUserEvent)) {
			$('#updateNumberError').html('<span class="help-block form-error">It should be correct number</span>').addClass('has-error');
			return false;
		}

		var updateNumberUserEvent = parseInt($('#updateNumberUserEvent').val());
		var TotalNumberUserEvent = parseInt($('#oldNumberUserEvent').val()) + updateNumberUserEvent;
		var updatePlanType = $('#updatePlanType').val();
//		console.log("updateNumberUserEvent :" + updateNumberUserEvent + " updatePlanType :" + updatePlanType);
		var updateCurrencyCode = $('#updateCurrencyId').val();
		var updatePromoCode = $(this).val();
	//console.log("updateCurrencyCode :" + updateCurrencyCode + " updatePromoCode :" + updatePromoCode);
		var updateSelectedMonths = 0;
		
		if (updatePlanType === 'PER_USER') {
			updateSelectedMonths = parseInt(chargeMonths);
		}
//		console.log("updateSelectedMonths :" + updateSelectedMonths);
		ajaxCallOnChangeFieldsForUpdateSubs(updateNumberUserEvent, updatePromoCode, updateSelectedMonths, updateCurrencyCode, updatePlanType, TotalNumberUserEvent);
	
	});
	
	
	$(document).on("keyup", "#updateNumberUserEvent", function(e) {
		var updateNumberUserEvent = parseInt($(this).val());
		var TotalNumberUserEvent = parseInt($('#oldNumberUserEvent').val()) + updateNumberUserEvent;
		$('#updatePromoError').html('').removeClass('has-error');
		$('#updateNumberError').html('').removeClass('has-error');
		if ($(this).val() === 'undefined' || $(this).val() === '0') {
			// console.log("numberUserEvent :"+$(this).val());
			$('#updateNumberError').html('<span class="help-block form-error">Number should be greater then 0</span>').addClass('has-error');
			return false;
		}
		if (isNaN(updateNumberUserEvent)) {
			$('#updateNumberError').html('<span class="help-block form-error">It should be correct number</span>').addClass('has-error');
			return false;
		}
		
		var lastRange = parseInt($('#lastRange').val());
		console.log("last range: " + lastRange);
		if (updateNumberUserEvent > lastRange) {
			$('#updateNumberError').html('<span class="help-block form-error">It should be less then '+lastRange+'</span>').addClass('has-error');
			return false;
		}
		console.log("updateNumberUserEvent 1 : " + updateNumberUserEvent);
		var updateNumberUserEvent = parseInt($('#updateNumberUserEvent').val());
		console.log("updateNumberUserEvent 2 : " + updateNumberUserEvent);
		var updatePlanType = $('#updatePlanType').val();
//		console.log("updateNumberUserEvent :" + updateNumberUserEvent + " updatePlanType :" + updatePlanType);
		var updateCurrencyCode = $('#updateCurrencyId').val();
		var updatePromoCode = $('#updatePromoCode').val();
	//console.log("updateCurrencyCode :" + updateCurrencyCode + " updatePromoCode :" + updatePromoCode);
		var updateSelectedMonths = 0;
		
		if (updatePlanType === 'PER_USER') {
			// setting free approver user *Note
			$("#updateTotalFeeApproverLabel").html(updateNumberUserEvent + ' approver users ');
			$('#userNo1').text(updateNumberUserEvent + " users");
			$('#appNo1').text(updateNumberUserEvent + " approver users");
				
			updateSelectedMonths = parseInt(chargeMonths);
		}
//		console.log("updateSelectedMonths :" + updateSelectedMonths);
		ajaxCallOnChangeFieldsForUpdateSubs(updateNumberUserEvent, updatePromoCode, updateSelectedMonths, updateCurrencyCode, updatePlanType, TotalNumberUserEvent);
	});
}

function ReplaceNumberWithCommas(yourNumber) {
	// Seperates the components of the number
	var n = yourNumber.toString().split(".");
	// Comma-fies the first part
	n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	// Combines the two sections
	return n.join(".");
}
