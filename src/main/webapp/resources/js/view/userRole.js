// checkCheckedboxs();
$(document).delegate('.nvigator+input[type="checkbox"]', 'change', function() {
	if ($(this).prop('checked') == false) {
		//console.log('>> Unchecked : ' + $(this).val());
		//$(this).parent().removeClass('checked');
		//$(this).parents('div').siblings('ul').find('input[type="checkbox"]').prop('checked', false);
		$(this).siblings('ul').find('input[type="checkbox"]').prop('checked', false);
		/*$(this).parents('div').siblings('ul').find('input[type="checkbox"]').parent().removeClass('checked');
		if ($(this).parent().parent().parent().parent().find('input[type="checkbox"]:checked').length == 0) {
			$(this).parents('ul').siblings('div').children('span').removeClass('checked');
			$(this).parents('ul').siblings('div').children('span').children('input[type="checkbox"]').prop('checked', false);
		}*/

		if ($(this).closest('ul').siblings('ul').find('input[type="checkbox"]:checked').length == 0){
			$(this).siblings('ul').find('input[type="checkbox"]').prop('checked', false);
			$(this).closest('ul').siblings('input[type=checkbox]').prop('checked', false);
					
		} 
		
		if ($(this).parents('ul').siblings('ul').find('input[type="checkbox"]:checked').length == 0)
		{
		$(this).parents('ul').siblings('input[type=checkbox]').prop('checked', false);
		}
		
	} else {
		if($(this).val() == 'ROLE_ADMIN_READONLY') {
			console.log('ADMIN READONLY CHECKED !!! : ' + $(this).val());
			$(this).parents('ul').find('input[type=checkbox]').each(function(){
				if($(this).val() != 'ROLE_USER') {
					$(this).prop('checked', false)
				}
			});
			$(this).prop('checked', true);
		} else {
			
			$(this).siblings('ul').find('input[type="checkbox"]').prop('checked', true);
			$(this).parents('ul').siblings('input[type=checkbox]').prop('checked', true);
		}
		/*$(this).parent().addClass('checked');
		$(this).parents('div').siblings('ul').find('input[type="checkbox"]').prop('checked', true);*/
		/*$(this).parents('div').siblings('ul').find('input[type="checkbox"]').parent().addClass('checked');
		$(this).parents('ul').siblings('div').children('span').addClass('checked');
		$(this).parents('ul').siblings('div').children('span').children('input[type="checkbox"]').prop('checked', true);*/
	}
});

//function checkCheckedboxs() {
//	$('.checkboxesRole:checked').parent().addClass('checked');
//}

$(document).on('click',
		'.nvigator',
				function() {
					var obj1 = $(this);
					var appenHtm = $(this).parent();
					var parentvalue = $(this).next().val();
					var fieldLabelName = $(this).next().attr('name');
					//appenHtm.find('ul').remove();
					var header = $("meta[name='_csrf_header']").attr("content");
					var token = $("meta[name='_csrf']").attr("content");
					var aclValue = $(this).next().children('span').children('input[type="checkbox"]').val();
					$(this).next().children('span').children('input[type="checkbox"]').change();

					if ($(obj1).find('i').hasClass('fa-plus')) {
						$(obj1).find('i').removeClass('fa-plus fa-spinner').addClass('fa-minus');
						appenHtm.find('ul').show();
					} else {
						//$(obj1).parent('li').find('ul').slideToggle('slow');
						$(obj1).find('i').removeClass('fa-minus').addClass('fa-plus');
						appenHtm.find('ul').hide();
					}
				});

loadAllCheckboxValues();

function loadAllCheckboxValues() {
	$('.leftSideOfCheckbox').each(function() {
		var currentleftBlock = $(this);
		currentleftBlock.next('.rightSideOfCheckbox').html('');
		currentleftBlock.find('input[type="checkbox"]:checked').each(function() {
			var htmldata = '<div class="item" data-value="' + $(this).val() + '"><span class="remove-selected">×</span>' + $(this).siblings(".number").text() + '</div>';
			$(this).closest('.chk_scroll_box').find('.rightSideOfCheckbox').append(htmldata);
		});
	});
}

$(document).on('change', '.leftSideOfCheckbox input[type=checkbox]', function() {
	$(this).parents('ul').siblings('input[type=checkbox]').prop('checked', true);
	$(this).closest('.chk_scroll_box').find('.rightSideOfCheckbox').html('');
	$(this).closest('.leftSideOfCheckbox').find('input[type="checkbox"]:checked').each(function() {
		var htmldata = '<div class="item" data-value="' + $(this).val() + '"><span class="remove-selected">×</span>' + $(this).siblings(".number").text() + '</div>';
		$(this).closest('.chk_scroll_box').find('.rightSideOfCheckbox').append(htmldata);
	});
});