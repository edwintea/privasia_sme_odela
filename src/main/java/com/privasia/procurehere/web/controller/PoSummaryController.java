
package com.privasia.procurehere.web.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.privasia.procurehere.core.dao.ErpSetupDao;
import com.privasia.procurehere.core.dao.PoAuditDao;
import com.privasia.procurehere.core.dao.PoReportDao;
import com.privasia.procurehere.core.entity.ErpSetup;
import com.privasia.procurehere.core.entity.Po;
import com.privasia.procurehere.core.entity.PoAudit;
import com.privasia.procurehere.core.entity.PoItem;
import com.privasia.procurehere.core.entity.PoReport;
import com.privasia.procurehere.core.entity.Pr;
import com.privasia.procurehere.core.enums.PoAuditType;
import com.privasia.procurehere.core.enums.PoAuditVisibilityType;
import com.privasia.procurehere.core.enums.PoDocumentType;
import com.privasia.procurehere.core.enums.PoStatus;
import com.privasia.procurehere.core.exceptions.NotAuthorizedException;
import com.privasia.procurehere.core.pojo.DoSupplierPojo;
import com.privasia.procurehere.core.pojo.InvoiceSupplierPojo;
import com.privasia.procurehere.core.pojo.SearchFilterPoPojo;
import com.privasia.procurehere.core.pojo.TableData;
import com.privasia.procurehere.core.pojo.TableDataInput;
import com.privasia.procurehere.core.utils.CollectionUtil;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.core.utils.SecurityLibrary;
import com.privasia.procurehere.core.utils.StringUtils;
import com.privasia.procurehere.service.ApprovalService;
import com.privasia.procurehere.service.BuyerSettingsService;
import com.privasia.procurehere.service.DeliveryOrderService;
import com.privasia.procurehere.service.InvoiceService;
import com.privasia.procurehere.service.OwnerSettingsService;
import com.privasia.procurehere.service.PoAuditService;
import com.privasia.procurehere.service.PoService;
import com.privasia.procurehere.service.PrAuditService;
import com.privasia.procurehere.service.PrService;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

/**
 * @author parveen
 */
@Controller
@RequestMapping("/buyer")
public class PoSummaryController {

	protected static final Logger LOG = Logger.getLogger(Global.PR_LOG);

	@Autowired
	ServletContext context;

	@Autowired
	PoService poService;

	@Autowired
	OwnerSettingsService ownerSettingsService;

	@Autowired
	ErpSetupDao erpSetupDao;

	@Resource
	MessageSource messageSource;

	@Autowired
	PoAuditService poAuditService;

	@Autowired
	ApprovalService approvalService;

	@Autowired
	PrService prService;

	@Autowired
	PoAuditDao poAuditDao;

	@Autowired
	BuyerSettingsService buyerSettingsService;

	@Autowired
	PrAuditService prAuditService;

	@Autowired
	DeliveryOrderService deliveryOrderService;

	@Autowired
	InvoiceService invoiceService;

	@Autowired
	PoReportDao poReportDao;

	@ModelAttribute("poDocType")
	public List<PoDocumentType> getPoDocType() {
		return Arrays.asList(PoDocumentType.values());
	}

	@ModelAttribute("poStatusList")
	public List<PoStatus> getPoStatusList() {
		List<PoStatus> poStatusList = Arrays.asList(PoStatus.values());
		return poStatusList;
	}

	@InitBinder
	public void initBinder(WebDataBinder binder, HttpSession session) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
		TimeZone timeZone = TimeZone.getDefault();
		String strTimeZone = (String) session.getAttribute(Global.SESSION_TIME_ZONE_KEY);
		if (strTimeZone != null) {
			timeZone = TimeZone.getTimeZone(strTimeZone);
		}
		SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
		timeFormat.setTimeZone(timeZone);
		SimpleDateFormat deliveryDate = new SimpleDateFormat("dd/MM/yyyy");
		deliveryDate.setTimeZone(timeZone);
		binder.registerCustomEditor(Date.class, "deliveryTime", new CustomDateEditor(timeFormat, true));
		binder.registerCustomEditor(Date.class, "deliveryDate", new CustomDateEditor(deliveryDate, true));
	}

	@RequestMapping(path = "/poList", method = RequestMethod.GET)
	public String poList() {
		return "poList";
	}

	@RequestMapping(path = "/poListData", method = RequestMethod.GET)
	public ResponseEntity<TableData<Po>> poListData(TableDataInput input, @RequestParam(required = false) String dateTimeRange, HttpSession session, HttpServletResponse response) {
		try {
			LOG.info("Start : " + input.getStart() + " Length : " + input.getLength() + " Sort : " + input.getSort() + ", dateTimeRange :" + dateTimeRange);
			LOG.debug(">>>>>>>>>>>>> Table Data Input : " + input.toString());
			Date startDate = null;
			Date endDate = null;
			if (StringUtils.checkString(dateTimeRange).length() > 0) {
				TimeZone timeZone = TimeZone.getDefault();
				String strTimeZone = (String) session.getAttribute(Global.SESSION_TIME_ZONE_KEY);
				if (strTimeZone != null) {
					timeZone = TimeZone.getTimeZone(strTimeZone);
				}
				if (StringUtils.checkString(dateTimeRange).length() > 0) {
					String dateTimeArr[] = dateTimeRange.split("-");
					DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
					formatter.setTimeZone(timeZone);
					startDate = (Date) formatter.parse(dateTimeArr[0]);
					endDate = (Date) formatter.parse(dateTimeArr[1]);
					// LOG.info("Start date : " + startDate + " End Date : " + endDate);
				}
			}
			List<Po> poList = poService.findAllSearchFilterPo(SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), SecurityLibrary.getLoggedInUserTenantId(), input, startDate, endDate);
			TableData<Po> data = new TableData<Po>(poList);
			data.setDraw(input.getDraw());
			long recordFiltered = poService.findTotalSearchFilterPoCount(SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), SecurityLibrary.getLoggedInUserTenantId(), input, startDate, endDate);
			data.setRecordsFiltered(recordFiltered);
			data.setRecordsTotal(recordFiltered);
			return new ResponseEntity<TableData<Po>>(data, HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Error fetching po list : " + e.getMessage(), e);
			HttpHeaders headers = new HttpHeaders();
			headers.add("error", "Error fetching PO list : " + e.getMessage());
			return new ResponseEntity<TableData<Po>>(null, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(path = "/poView/{poId}", method = RequestMethod.GET)
	public String poView(@PathVariable String poId, Model model, HttpServletRequest request, RedirectAttributes redir) {
		LOG.info("getting po view id :" + poId);
		try {
			constructPoSummaryAttributes(poId, model);
		} catch (NotAuthorizedException e) {
			LOG.error("Error in view Po :" + e.getMessage(), e);
			redir.addFlashAttribute("requestedUrl", request.getRequestURL());
			return "redirect:/403_error";
		} catch (Exception e) {
			LOG.error("Error in view Po :" + e.getMessage(), e);
			model.addAttribute("error", messageSource.getMessage("posummary.error.view.po", new Object[] { e.getMessage() }, Global.LOCALE));
		}
		return "poView";
	}

	@RequestMapping(path = "/poSummary/{prId}", method = RequestMethod.GET)
	public String poSummary(@PathVariable String prId, Model model, RedirectAttributes redir) {
		LOG.info("getting po view by pr id :" + prId);
		try {
			Po po = poService.findByPrId(prId);
			if (po != null) {
				return "redirect:/buyer/poView/" + po.getId();
			} else {
				model.addAttribute("error", "No PO found for PR");
				return "redirect:/buyer/prView/" + prId;
			}
		} catch (Exception e) {
			LOG.error("Error in view Po :" + e.getMessage(), e);
			model.addAttribute("error", messageSource.getMessage("posummary.error.view.po", new Object[] { e.getMessage() }, Global.LOCALE));
			return "redirect:/buyer/prView/" + prId;
		}
	}

	public Po constructPoSummaryAttributes(String poId, Model model) throws NotAuthorizedException {
		Po po = poService.getLoadedPoById(poId);
		if (!(SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") || SecurityLibrary.getLoggedInUser().getId().equals(po.getCreatedBy().getId()))) {
			throw new NotAuthorizedException("Not Autherized");
		}

		po.setInvoiceCount((int) invoiceService.findTotalBuyerInvoiceForPo(poId));
		po.setDoCount((int) deliveryOrderService.findTotalDoForBuyerPo(poId));
		model.addAttribute("po", po);
		List<PoItem> poItemlist = poService.findAllPoItemByPoIdForSummary(poId);
		model.addAttribute("poItemlist", poItemlist);
		model.addAttribute("ownerSettings", ownerSettingsService.getOwnersettings());
		List<PoAudit> poAuditList = poAuditService.getPoAuditByPoIdForBuyer(po.getId());
		model.addAttribute("poAuditList", poAuditList);
		ErpSetup erpSetup = erpSetupDao.getErpConfigBytenantId(SecurityLibrary.getLoggedInUserTenantId());
		model.addAttribute("erpSetup", erpSetup);
		model.addAttribute("isAutoPublishPo", buyerSettingsService.isAutoPublishePoSettingsByTenantId(SecurityLibrary.getLoggedInUserTenantId()));
		List<DoSupplierPojo> dos = deliveryOrderService.getDosByPoIdForBuyer(poId);
		model.addAttribute("dos", dos);
		List<InvoiceSupplierPojo> invoices = invoiceService.getInvoicesByPoIdForBuyer(poId);
		model.addAttribute("invoices", invoices);

		return po;
	}

	@RequestMapping(path = "/exportPoItemTemplate/{poId}", method = RequestMethod.GET)
	public void downloader(@PathVariable String poId, HttpServletRequest request, HttpServletResponse response) {
		LOG.info("Download prItemTemplate... ");
		try {
			Po po = poService.findPoById(poId);
			String downloadFolder = context.getRealPath("/WEB-INF/");
			String fileName = "poItemTemplate.xlsx";
			Path file = Paths.get(downloadFolder, fileName);
			List<PoItem> poList = poService.findAllPoItemByPoId(poId);
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("PO Item List");

			// For Financial Standard
			DecimalFormat df = null;
			if (po.getDecimal().equals("1")) {
				df = new DecimalFormat("#,###,###,##0.0");
			} else if (po.getDecimal().equals("2")) {
				df = new DecimalFormat("#,###,###,##0.00");
			} else if (po.getDecimal().equals("3")) {
				df = new DecimalFormat("#,###,###,##0.000");
			} else if (po.getDecimal().equals("4")) {
				df = new DecimalFormat("#,###,###,##0.0000");
			} else if (po.getDecimal().equals("5")) {
				df = new DecimalFormat("#,###,###,##0.00000");
			} else if (po.getDecimal().equals("6")) {
				df = new DecimalFormat("#,###,###,##0.000000");
			} else {
				df = new DecimalFormat("#,###,###,##0.00");
			}

			// Style of Heading Cells
			CellStyle styleHeading = workbook.createCellStyle();
			Font font = workbook.createFont();
			font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
			styleHeading.setFont(font);
			styleHeading.setAlignment(CellStyle.ALIGN_CENTER);
			// Creating Headings
			Row rowHeading = sheet.createRow(0);
			int i = 0;
			for (String column : Global.PO_EXCEL_COLUMNS) {
				Cell cell = rowHeading.createCell(i++);
				if (column.equals("UNIT PRICE")) {
					cell.setCellValue(column + "(" + po.getCurrency().getCurrencyCode() + ")");
				} else {
					cell.setCellValue(column);
				}
				cell.setCellStyle(styleHeading);
			}
			if (po.getField1Label() != null) {
				Cell cell = rowHeading.createCell(i++);
				cell.setCellValue(po.getField1Label());
				cell.setCellStyle(styleHeading);
			}
			if (po.getField2Label() != null) {
				Cell cell = rowHeading.createCell(i++);
				cell.setCellValue(po.getField2Label());
				cell.setCellStyle(styleHeading);
			}
			if (po.getField3Label() != null) {
				Cell cell = rowHeading.createCell(i++);
				cell.setCellValue(po.getField3Label());
				cell.setCellStyle(styleHeading);
			}
			if (po.getField4Label() != null) {
				Cell cell = rowHeading.createCell(i++);
				cell.setCellValue(po.getField4Label());
				cell.setCellStyle(styleHeading);
			}

			if (po.getField5Label() != null) {
				Cell cell = rowHeading.createCell(i++);
				cell.setCellValue(po.getField5Label());
				cell.setCellStyle(styleHeading);
			}
			if (po.getField6Label() != null) {
				Cell cell = rowHeading.createCell(i++);
				cell.setCellValue(po.getField6Label());
				cell.setCellStyle(styleHeading);
			}
			if (po.getField7Label() != null) {
				Cell cell = rowHeading.createCell(i++);
				cell.setCellValue(po.getField7Label());
				cell.setCellStyle(styleHeading);
			}
			if (po.getField8Label() != null) {
				Cell cell = rowHeading.createCell(i++);
				cell.setCellValue(po.getField8Label());
				cell.setCellStyle(styleHeading);
			}

			if (po.getField9Label() != null) {
				Cell cell = rowHeading.createCell(i++);
				cell.setCellValue(po.getField9Label());
				cell.setCellStyle(styleHeading);
			}
			if (po.getField10Label() != null) {
				Cell cell = rowHeading.createCell(i++);
				cell.setCellValue(po.getField10Label());
				cell.setCellStyle(styleHeading);
			}

			Cell cell = rowHeading.createCell(i++);
			cell.setCellValue("TOTAT_AMOUNT(" + po.getCurrency().getCurrencyCode() + ")");
			cell.setCellStyle(styleHeading);

			cell = rowHeading.createCell(i++);
			cell.setCellValue("TAX_AMOUNT(" + po.getCurrency().getCurrencyCode() + ")");
			cell.setCellStyle(styleHeading);

			cell = rowHeading.createCell(i++);
			cell.setCellValue("TOTAT_AMOUNT_WITH_TAX(" + po.getCurrency().getCurrencyCode() + ")");
			cell.setCellStyle(styleHeading);

			int cellNumber = 0;
			int r = 1;
			// Write Data into Excel
			for (PoItem item : poList) {
				Row row = sheet.createRow(r++);
				int cellNum = 0;
				row.createCell(cellNum++).setCellValue(item.getLevel() + "." + item.getOrder());
				row.createCell(cellNum++).setCellValue(item.getItemName());
				row.createCell(cellNum++).setCellValue(item.getItemDescription() != null ? item.getItemDescription() : "");
				row.createCell(cellNum++).setCellValue(item.getProduct() != null ? (item.getProduct().getUom() != null ? item.getProduct().getUom().getUom() : "") : (item.getUnit() != null ? item.getUnit().getUom() : ""));
				int colNum = 6;
				if (StringUtils.checkString(po.getField1Label()).length() > 0 && po.getField1Label() != null)
					colNum++;
				if (StringUtils.checkString(po.getField2Label()).length() > 0 && po.getField2Label() != null)
					colNum++;
				if (StringUtils.checkString(po.getField3Label()).length() > 0 && po.getField3Label() != null)
					colNum++;
				if (StringUtils.checkString(po.getField4Label()).length() > 0 && po.getField4Label() != null)
					colNum++;
				if (StringUtils.checkString(po.getField5Label()).length() > 0 && po.getField5Label() != null)
					colNum++;
				if (StringUtils.checkString(po.getField6Label()).length() > 0 && po.getField6Label() != null)
					colNum++;
				if (StringUtils.checkString(po.getField7Label()).length() > 0 && po.getField7Label() != null)
					colNum++;
				if (StringUtils.checkString(po.getField8Label()).length() > 0 && po.getField8Label() != null)
					colNum++;
				if (StringUtils.checkString(po.getField9Label()).length() > 0 && po.getField9Label() != null)
					colNum++;
				if (StringUtils.checkString(po.getField10Label()).length() > 0 && po.getField10Label() != null)
					colNum++;

				sheet.addMergedRegion(new CellRangeAddress(r - 1, r - 1, cellNum - 1, colNum + 3));
				cellNumber = colNum + 4;
				if (CollectionUtil.isNotEmpty(item.getChildren())) {
					for (PoItem children : item.getChildren()) {
						Row childrow = sheet.createRow(r++);
						int childCellNum = 0;
						childrow.createCell(childCellNum++).setCellValue(children.getLevel() + "." + children.getOrder());
						childrow.createCell(childCellNum++).setCellValue(children.getProduct() != null ? children.getProduct().getProductName() : "");
						childrow.createCell(childCellNum++).setCellValue(children.getItemDescription() != null ? children.getItemDescription() : "");
						childrow.createCell(childCellNum++).setCellValue(children.getProduct() != null ? (children.getProduct().getUom() != null ? children.getProduct().getUom().getUom() : "") : (children.getUnit() != null ? children.getUnit().getUom() : ""));
						childrow.createCell(childCellNum++).setCellValue(children.getQuantity() != null ? String.valueOf(children.getQuantity()) : "");
						childrow.createCell(childCellNum++).setCellValue(children.getUnitPrice() != null ? df.format(children.getUnitPrice()) : "");
						childrow.createCell(childCellNum++).setCellValue(children.getItemTax() != null ? children.getItemTax() : "");
						if (StringUtils.checkString(po.getField1Label()).length() > 0 && po.getField1Label() != null)
							childrow.createCell(childCellNum++).setCellValue(children.getField1() != null ? children.getField1() : "");
						if (StringUtils.checkString(po.getField2Label()).length() > 0 && po.getField2Label() != null)
							childrow.createCell(childCellNum++).setCellValue(children.getField2() != null ? children.getField2() : "");
						if (StringUtils.checkString(po.getField3Label()).length() > 0 && po.getField3Label() != null)
							childrow.createCell(childCellNum++).setCellValue(children.getField3() != null ? children.getField3() : "");
						if (StringUtils.checkString(po.getField4Label()).length() > 0 && po.getField4Label() != null)
							childrow.createCell(childCellNum++).setCellValue(children.getField4() != null ? children.getField4() : "");

						if (StringUtils.checkString(po.getField5Label()).length() > 0 && po.getField5Label() != null)
							childrow.createCell(childCellNum++).setCellValue(children.getField5() != null ? children.getField5() : "");
						if (StringUtils.checkString(po.getField6Label()).length() > 0 && po.getField6Label() != null)
							childrow.createCell(childCellNum++).setCellValue(children.getField6() != null ? children.getField6() : "");
						if (StringUtils.checkString(po.getField7Label()).length() > 0 && po.getField7Label() != null)
							childrow.createCell(childCellNum++).setCellValue(children.getField7() != null ? children.getField7() : "");
						if (StringUtils.checkString(po.getField8Label()).length() > 0 && po.getField8Label() != null)
							childrow.createCell(childCellNum++).setCellValue(children.getField8() != null ? children.getField8() : "");
						if (StringUtils.checkString(po.getField9Label()).length() > 0 && po.getField9Label() != null)
							childrow.createCell(childCellNum++).setCellValue(children.getField9() != null ? children.getField9() : "");
						if (StringUtils.checkString(po.getField10Label()).length() > 0 && po.getField10Label() != null)
							childrow.createCell(childCellNum++).setCellValue(children.getField10() != null ? children.getField10() : "");

						childrow.createCell(childCellNum++).setCellValue(children.getTotalAmount() != null ? df.format(children.getTotalAmount()) : df.format(0));
						childrow.createCell(childCellNum++).setCellValue(children.getTaxAmount() != null ? df.format(children.getTaxAmount()) : df.format(0));
						childrow.createCell(childCellNum++).setCellValue(children.getTotalAmountWithTax() != null ? df.format(children.getTotalAmountWithTax()) : df.format(0));
						cellNumber = childCellNum;
					}
				}
			}
			r++;
			Row row = sheet.createRow(r++);
			row.createCell(cellNumber - 2).setCellValue("Total(" + po.getCurrency().getCurrencyCode() + ")");
			row.createCell(cellNumber - 1).setCellValue(df.format(po.getTotal()));
			if (po.getTaxDescription() != null) {
				row = sheet.createRow(r++);
				row.createCell(cellNumber - 4).setCellValue("Additional Charges");
				row.createCell(cellNumber - 3).setCellValue(po.getTaxDescription());
				row.createCell(cellNumber - 2).setCellValue("(" + po.getCurrency().getCurrencyCode() + ")");
				row.createCell(cellNumber - 1).setCellValue(df.format(po.getAdditionalTax()));
			}
			row = sheet.createRow(r++);
			Cell grandTotalCell = row.createCell(cellNumber - 2);
			grandTotalCell.setCellValue("Grand Total(" + po.getCurrency().getCurrencyCode() + ")");
			grandTotalCell.setCellStyle(styleHeading);
			grandTotalCell = row.createCell(cellNumber - 1);
			grandTotalCell.setCellValue(df.format(po.getGrandTotal()));
			grandTotalCell.setCellStyle(styleHeading);

			// Auto Fit
			for (int k = 0; k < 21; k++) {
				sheet.autoSizeColumn(k, true);
			}

			// Save Excel File
			FileOutputStream out = new FileOutputStream(downloadFolder + "/" + fileName);
			workbook.write(out);
			out.close();
			LOG.info("Successfully written in Excel");

			if (Files.exists(file)) {
				response.setContentType("application/vnd.ms-excel");
				response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
				try {
					Files.copy(file, response.getOutputStream());
					response.getOutputStream().flush();
				} catch (IOException e) {
					LOG.error("Error :- " + e.getMessage(), e);
				}
			}
		} catch (Exception e) {
			LOG.error("Error while downloading PO items  Excel : " + e.getMessage(), e);
		}
	}

	@RequestMapping(path = "/createPo", method = RequestMethod.POST)
	public String createPoManually(@RequestParam("prId") String prId, RedirectAttributes redir) {
		try {
			LOG.info("Creating PO manually for pr Id :" + prId + " ==User Name :" + SecurityLibrary.getLoggedInUser().getName());
			if (StringUtils.checkString(prId).length() > 0) {
				Pr pr = prService.getLoadedPrById(prId);
				if (pr != null) {
					Po po = prService.createPo(SecurityLibrary.getLoggedInUser(), pr);
					if (po != null && StringUtils.checkString(po.getId()).length() > 0) {
						LOG.info("po created succefully:" + po.getId());
						po = poService.getLoadedPoById(po.getId());
						pr.setPoNumber(po.getPoNumber());
						pr.setPoCreatedDate(new Date());
						pr.setIsPo(Boolean.TRUE);
						prService.updatePr(pr);
						try {
							approvalService.sendPoCreatedEmailToCreater(pr.getCreatedBy(), pr, SecurityLibrary.getLoggedInUser());
							if (po.getStatus() == PoStatus.ORDERED && po.getSupplier() != null) {
								approvalService.sendPoReceivedEmailNotificationToSupplier(po, SecurityLibrary.getLoggedInUser());
							}
						} catch (Exception e) {
							LOG.error("Error while sending received PO notification to supplier:" + e.getMessage(), e);
						}

						redir.addFlashAttribute("success", messageSource.getMessage("success.po.created", new Object[] { po.getPoNumber() }, Global.LOCALE));
					}
				}
			}
			return "redirect:prView/" + prId;
		} catch (Exception e) {
			LOG.error("Error while creating po manually :" + e.getMessage(), e);
			redir.addFlashAttribute("error", "Error while creating PO:" + e.getMessage());
			return "redirect:prView/" + prId;

		}

	}

	@RequestMapping(path = "/sendPo", method = RequestMethod.POST)
	public String sendPo(@RequestParam("poId") String poId, RedirectAttributes redir) {
		try {
			LOG.info("Send PO poId :" + poId + " ==User Name :" + SecurityLibrary.getLoggedInUser().getName());
			Po po = poService.getLoadedPoById(poId);
			po.setOrderedBy(SecurityLibrary.getLoggedInUser());
			po.setOrderedDate(new Date());
			po.setStatus(PoStatus.ORDERED);

			PoAudit buyerAudit = new PoAudit();
			buyerAudit.setAction(PoAuditType.ORDERED);
			buyerAudit.setActionBy(SecurityLibrary.getLoggedInUser());
			buyerAudit.setActionDate(new Date());
			buyerAudit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
			if (po.getSupplier() != null) {
				buyerAudit.setSupplier(po.getSupplier().getSupplier());
				buyerAudit.setDescription(messageSource.getMessage("po.audit.ordered", new Object[] { po.getPoNumber(), po.getSupplier().getSupplier() != null ? po.getSupplier().getSupplier().getCompanyName() : "" }, Global.LOCALE));
			} else {
				buyerAudit.setDescription(messageSource.getMessage("po.opensupplier.audit.ordered", new Object[] { po.getPoNumber(), StringUtils.checkString(po.getSupplierName()).length() > 0 ? po.getSupplierName() : "" }, Global.LOCALE));
			}
			buyerAudit.setVisibilityType(PoAuditVisibilityType.BUYER);
			buyerAudit.setPo(po);

			PoAudit supplierAudit = new PoAudit();
			supplierAudit.setAction(PoAuditType.RECEIVED);
			supplierAudit.setActionBy(SecurityLibrary.getLoggedInUser());
			supplierAudit.setActionDate(new Date());
			supplierAudit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
			if (po.getSupplier() != null) {
				supplierAudit.setSupplier(po.getSupplier().getSupplier());
			}
			supplierAudit.setDescription(messageSource.getMessage("po.audit.received", new Object[] { po.getPoNumber(), po.getBuyer().getCompanyName() }, Global.LOCALE));
			supplierAudit.setVisibilityType(PoAuditVisibilityType.SUPPLIER);
			supplierAudit.setPo(po);
			poService.updatePoStatus(po, buyerAudit, supplierAudit);
			LOG.info("PO " + po.getPoNumber() + " sent succefully to supplier");

			if (po.getSupplier() != null) {
				try {
					approvalService.sendPoReceivedEmailNotificationToSupplier(po, SecurityLibrary.getLoggedInUser());
				} catch (Exception e) {
					LOG.info("Error while sending cancellation PO notification to supplier:" + e.getMessage(), e);
				}
			}
			redir.addFlashAttribute("success", messageSource.getMessage("success.po.ordered", new Object[] { po.getPoNumber() }, Global.LOCALE));
		} catch (Exception e) {
			LOG.error("Error While sending PO to supplier :" + e.getMessage(), e);
			redir.addFlashAttribute("error", messageSource.getMessage("flasherror.while.ordered.po", new Object[] { e.getMessage() }, Global.LOCALE));
		}
		return "redirect:poView/" + poId;
	}

	@RequestMapping(path = "/cancelPo", method = RequestMethod.POST)
	public String cancelPo(@RequestParam("poId") String poId, @RequestParam(required = false) String poRemarks, RedirectAttributes redir) {
		try {
			LOG.info("Cancel PO poId :" + poId + " ==User Name :" + SecurityLibrary.getLoggedInUser().getName() + " ==poRemarks : " + poRemarks);
			Po po = poService.getLoadedPoById(poId);
			po.setStatus(PoStatus.CANCELLED);

			PoAudit buyerAudit = new PoAudit();
			buyerAudit.setAction(PoAuditType.CANCELLED);
			buyerAudit.setActionBy(SecurityLibrary.getLoggedInUser());
			buyerAudit.setActionDate(new Date());
			buyerAudit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
			if (po.getSupplier() != null) {
				buyerAudit.setSupplier(po.getSupplier().getSupplier());
			}
			buyerAudit.setDescription(messageSource.getMessage("po.buyeraudit.cancelled", new Object[] { po.getPoNumber(), poRemarks }, Global.LOCALE));
			buyerAudit.setVisibilityType(PoAuditVisibilityType.BUYER);
			buyerAudit.setPo(po);

			PoAudit supplierAudit = new PoAudit();
			supplierAudit.setAction(PoAuditType.CANCELLED);
			supplierAudit.setActionBy(SecurityLibrary.getLoggedInUser());
			supplierAudit.setActionDate(new Date());
			supplierAudit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
			if (po.getSupplier() != null) {
				supplierAudit.setSupplier(po.getSupplier().getSupplier());
			}
			supplierAudit.setDescription(messageSource.getMessage("po.audit.cancelled", new Object[] { po.getPoNumber(), po.getBuyer().getCompanyName(), poRemarks }, Global.LOCALE));
			supplierAudit.setVisibilityType(PoAuditVisibilityType.SUPPLIER);
			supplierAudit.setPo(po);
			poService.updatePoStatus(po, buyerAudit, supplierAudit);
			LOG.info("PO " + po.getPoNumber() + " cancelled succefully");

			if (po.getSupplier() != null) {
				try {
					approvalService.sendCancelPoEmailNotificationToSupplier(po, poRemarks, SecurityLibrary.getLoggedInUser());
				} catch (Exception e) {
					LOG.info("Error while sending cancellation PO notification to supplier:" + e.getMessage(), e);
				}
			}
			redir.addFlashAttribute("success", messageSource.getMessage("success.po.cancel", new Object[] { po.getPoNumber() }, Global.LOCALE));
		} catch (Exception e) {
			LOG.error("Error While Cancel PO :" + e.getMessage(), e);
			redir.addFlashAttribute("error", messageSource.getMessage("flasherror.while.cancel.po", new Object[] { e.getMessage() }, Global.LOCALE));
		}
		return "redirect:poView/" + poId;
	}

	@RequestMapping(path = "/poReports", method = RequestMethod.POST)
	public void downloadpoReports(HttpSession session, HttpServletRequest request, HttpServletResponse response, @RequestParam(required = false) String poIds, boolean select_all, @RequestParam String dateTimeRange, @ModelAttribute("searchFilterPoPojo") SearchFilterPoPojo searchFilterPoPojo) {
		try {
			String pOArr[] = null;
			if (StringUtils.checkString(poIds).length() > 0) {
				pOArr = poIds.split(",");
			}
			TableDataInput input = new TableDataInput();
			input.setStart(0);
			input.setLength(5000);

			String tenantId = SecurityLibrary.getLoggedInUserTenantId();

			LOG.info("dateTimeRange :" + dateTimeRange);
			Date startDate = null;
			Date endDate = null;

			if (StringUtils.checkString(dateTimeRange).length() > 0) {
				TimeZone timeZone = TimeZone.getDefault();
				String strTimeZone = (String) session.getAttribute(Global.SESSION_TIME_ZONE_KEY);
				if (strTimeZone != null) {
					timeZone = TimeZone.getTimeZone(strTimeZone);
				}

				String dateTimeArr[] = dateTimeRange.split("-");
				DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
				formatter.setTimeZone(timeZone);
				startDate = (Date) formatter.parse(dateTimeArr[0]);
				endDate = (Date) formatter.parse(dateTimeArr[1]);
				LOG.info("Start date : " + startDate + " End Date : " + endDate);
			}

			poService.downloadBuyerPoReports(tenantId, pOArr, response, session, select_all, startDate, endDate, searchFilterPoPojo, SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId());
		} catch (Exception e) {
			LOG.error("Error While Filter po list :" + e.getMessage(), e);
		}

	}

	@RequestMapping(path = "/downloadPoReport/{poId}", method = RequestMethod.GET)
	public void generatePoReport(@PathVariable("poId") String poId, HttpServletResponse response, HttpSession session) throws Exception {
		try {
			LOG.info(" PO REPORT : " + poId);
			String poFilename = "UnknownPO.pdf";
			Po po = poService.getLoadedPoById(poId);
			PoReport reportObj = poReportDao.findReportByPoId(po.getId(), SecurityLibrary.getLoggedInUserTenantId());

			if (reportObj != null) {
				LOG.info("PDF is Present in Database");
				response.setContentType("application/pdf");
				response.setContentLength(reportObj.getFileData().length);
				response.setHeader("Content-Disposition", "attachment; filename=\"" + reportObj.getFileName() + "\"");
				FileCopyUtils.copy(reportObj.getFileData(), response.getOutputStream());
				response.flushBuffer();
				response.setStatus(HttpServletResponse.SC_OK);
			} else {
				LOG.info("PDF is not Present in Database");
				if (po.getPoNumber() != null) {
					poFilename = (po.getPoNumber()).replace("/", "-") + ".pdf";
				}
				String filename = poFilename;

				JasperPrint jasperPrint = poService.getBuyerPoPdf(po, (String) session.getAttribute(Global.SESSION_TIME_ZONE_KEY));
				if (jasperPrint != null) {
					response.setContentType("application/pdf");
					response.addHeader("Content-Disposition", "attachment; filename=" + filename);
					JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());

					byte[] outputFile = JasperExportManager.exportReportToPdf(jasperPrint);
					PoReport attach = new PoReport();
					attach.setFileData(outputFile);
					attach.setFileName(filename);
					attach.setPoNumber(po.getPoNumber());
					attach.setTenantId(SecurityLibrary.getLoggedInUserTenantId());
					attach.setPo(po);
					poReportDao.saveOrUpdate(attach);

					response.getOutputStream().flush();
					response.getOutputStream().close();
				}
				try {
					PoAudit poAudit = new PoAudit();
					poAudit.setAction(PoAuditType.DOWNLOADED);
					poAudit.setActionBy(SecurityLibrary.getLoggedInUser());
					poAudit.setActionDate(new Date());
					poAudit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
					if (po.getSupplier() != null && po.getSupplier().getSupplier() != null) {
						poAudit.setSupplier(po.getSupplier().getSupplier());
					}
					poAudit.setDescription(messageSource.getMessage("po.audit.downloadPo", new Object[] { po.getPoNumber() }, Global.LOCALE));
					poAudit.setVisibilityType(PoAuditVisibilityType.BUYER);
					poAudit.setPo(po);
					poAuditService.save(poAudit);
				} catch (Exception e) {
					LOG.error("Error while saving po audit:" + e.getMessage(), e);
				}
			}
		} catch (Exception e) {
			LOG.error("Could not generate PR Summary Report. " + e.getMessage(), e);
		}
	}

}