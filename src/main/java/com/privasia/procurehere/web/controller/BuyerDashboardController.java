/**
 *  
 */
package com.privasia.procurehere.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.privasia.procurehere.core.entity.Po;
import com.privasia.procurehere.core.entity.Pr;
import com.privasia.procurehere.core.enums.PoStatus;
import com.privasia.procurehere.core.enums.PrStatus;
import com.privasia.procurehere.core.enums.RfxTypes;
import com.privasia.procurehere.core.pojo.ActiveEventPojo;
import com.privasia.procurehere.core.pojo.BudgetPojo;
import com.privasia.procurehere.core.pojo.DraftEventPojo;
import com.privasia.procurehere.core.pojo.DynamicColumnsData;
import com.privasia.procurehere.core.pojo.FinishedEventPojo;
import com.privasia.procurehere.core.pojo.OngoingEventPojo;
import com.privasia.procurehere.core.pojo.PendingEventPojo;
import com.privasia.procurehere.core.pojo.SourcingFormRequestPojo;
import com.privasia.procurehere.core.pojo.SupplierFormSubmissionPojo;
import com.privasia.procurehere.core.pojo.TableData;
import com.privasia.procurehere.core.pojo.TableDataInput;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.core.utils.SecurityLibrary;
import com.privasia.procurehere.service.BudgetService;
import com.privasia.procurehere.service.PoService;
import com.privasia.procurehere.service.PrService;
import com.privasia.procurehere.service.RfaEventSupplierService;
import com.privasia.procurehere.service.RftEventService;
import com.privasia.procurehere.service.SourcingFormRequestService;
import com.privasia.procurehere.service.SupplierFormSubmissionService;
import com.privasia.procurehere.service.supplier.SupplierService;

/**
 * @author Najeer
 */
@Controller
@RequestMapping(value = "/buyer")
public class BuyerDashboardController extends DashboardBase implements Serializable {

	private static final long serialVersionUID = 4723921253275442116L;

	private static final Logger LOG = Logger.getLogger(Global.BUYER_LOG);

	@Resource
	MessageSource messageSource;

	@Autowired
	SupplierService supplierService;

	@Autowired
	BudgetService budgetService;

	@Autowired
	RftEventService rftEventService;

	@Autowired
	PrService prService;

	@Autowired
	RfaEventSupplierService rfaEventSupplierService;

	@Autowired
	SourcingFormRequestService sourcingFormRequestService;

	@Autowired
	PoService poService;

	@Autowired
	SupplierFormSubmissionService supplierFormSubmissionService;

	@ModelAttribute("poStatusList")
	public List<PrStatus> getPoStatusList() {
		List<PrStatus> poStatusList = Arrays.asList(PrStatus.APPROVED, PrStatus.TRANSFERRED, PrStatus.CANCELED);
		return poStatusList;
	}

	@RequestMapping(value = "/buyerDashboard", method = RequestMethod.GET)
	public String buyerDashboard(Model model) throws JsonProcessingException {
		model.addAttribute("days", 30);

		long myApprovalsCount = rftEventService.findTotalMyPendingApprovals(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId());
		long myEvaluationCount = rftEventService.findTotalMyPendingEvaluation(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), null);

		LOG.info("Draft count started .................");
		long draftEventCount = rftEventService.findTotalDraftEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), null);
		LOG.info("Draft count End .................");
		LOG.info("Pending count started .................");
		long pendingEventCount = rftEventService.findTotalPendingEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), null);
		LOG.info("Pending count ended.................");
		LOG.info("Schedule count started .................");
		long scheduledEventCount = rftEventService.findTotalActiveEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), null);
		LOG.info("Schedule count ended.................");
		LOG.info("Ongoing count started .................");
		long ongoingEventCount = rftEventService.findTotalOngoingEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), null);
		LOG.info("Ongoing count ended .................");

		// regarding PH 69 we have use -1 for get all count for event to show on dash-board
		LOG.info("Finshed count started .................");
		long finishedEventCount = rftEventService.findTotalFinishedEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), -1, null);
		LOG.info("FInished count ended .................");

		long draftPrCount = prService.findTotalDraftPr(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), null);
		long pendingPrCount = prService.findTotalPendingPr(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId());

		long prCount = prService.findTotalApprovedPrs(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId());

		// long prCount = poService.findTotalPo(SecurityLibrary.getLoggedInUserTenantId());

		long prTransferCount = prService.findTotalPrTransfer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId());
		long suspendedEventCount = rftEventService.findTotalSuspendedEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), null);

		LOG.info("Completed count started .................");
		long completedEventCount = rftEventService.findTotalCompletedEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), null);
		LOG.info("Completed count ended.................");
		LOG.info("Cancelled count started .................");
		long cancelledEventCount = rftEventService.findTotalCancelledEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), null);
		LOG.info("Cancelled count end .................");
		LOG.info("Closed count started .................");
		long closedEventCount = rftEventService.findTotalClosedEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), null);
		LOG.info("Closed count started .................");

		long formDraftCount = sourcingFormRequestService.myDraftRequestListCount(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), null);
		long formPendingCount = sourcingFormRequestService.myPendingRequestListCount(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), null);
		long approvedRequestCount = sourcingFormRequestService.myApprovedRequestListCount(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), null);
		long finishedRequestCount = sourcingFormRequestService.finishedRequestCount(SecurityLibrary.getLoggedInUserTenantId(), null, SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId());
		long cancelRequestCount = sourcingFormRequestService.getCancelRequestCount(SecurityLibrary.getLoggedInUserTenantId(), null, SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId());
		long completeRequestCount = sourcingFormRequestService.myPendingRequestListCount(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), null);
		long readyPoCount = poService.findPoCountBasedOnStatusAndTenant(SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), SecurityLibrary.getLoggedInUserTenantId(), PoStatus.READY);
		long orderedPoCount = poService.findPoCountBasedOnStatusAndTenant(SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), SecurityLibrary.getLoggedInUserTenantId(), PoStatus.ORDERED);

		long totalFilteredBudgetCount = budgetService.findTotalBudgetForApproval(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), null);

		model.addAttribute("completeRequestCount", completeRequestCount);
		model.addAttribute("formDraftCount", formDraftCount);
		model.addAttribute("formPendingCount", formPendingCount);
		model.addAttribute("finishedRequestCount", finishedRequestCount);
		model.addAttribute("ongoingEventCount", ongoingEventCount);
		model.addAttribute("draftEventCount", draftEventCount);
		model.addAttribute("pendingEventCount", pendingEventCount);
		model.addAttribute("scheduledEventCount", scheduledEventCount);
		model.addAttribute("finishedEventCount", finishedEventCount + finishedRequestCount + approvedRequestCount);
		model.addAttribute("draftPrCount", draftPrCount + formDraftCount);
		model.addAttribute("pendingPrCount", pendingPrCount + formPendingCount);
		model.addAttribute("prCount", prCount);
		model.addAttribute("draftPr", draftPrCount);
		model.addAttribute("pendingPr", pendingPrCount);
		model.addAttribute("finishedEvent", finishedEventCount);

		model.addAttribute("prTransferCount", prTransferCount);

		model.addAttribute("suspendedEventCount", suspendedEventCount);
		model.addAttribute("cancelledEventCount", cancelledEventCount + cancelRequestCount);
		model.addAttribute("closedEventCount", closedEventCount);
		model.addAttribute("completedEventCount", completedEventCount);

		model.addAttribute("cancelledEvent", cancelledEventCount);
		model.addAttribute("myApprovalsCount", myApprovalsCount + totalFilteredBudgetCount);
		model.addAttribute("myEvaluationCount", myEvaluationCount);
		model.addAttribute("approvedRequestCount", approvedRequestCount);
		model.addAttribute("cancelRequestCount", cancelRequestCount);
		model.addAttribute("readyPoCount", readyPoCount);
		model.addAttribute("orderedPoCount", orderedPoCount);

		model.addAttribute("eventAppCount", rftEventService.findTotalEventMyPendingApprovals(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId()));
		model.addAttribute("prAppCount", rftEventService.findTotalPrMyPendingApprovals(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId()));
		model.addAttribute("rfsAppCount", rftEventService.findTotalRfsMyPendingApprovals(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId()));
		model.addAttribute("sfAppCount", rftEventService.findTotalSupplierFormMyPendingApprovals(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId()));
		model.addAttribute("budgetAppCount", totalFilteredBudgetCount);

		LOG.info("Total finished Request count " + finishedRequestCount);
		return "buyerDashboard";
	}

	@RequestMapping(value = "/extendSession", method = RequestMethod.GET)
	public ResponseEntity<String> extendSession(Model model) {
		return new ResponseEntity<String>("{\"id\" : \"Success\"}", HttpStatus.OK);
	}

	@RequestMapping(value = "/dynamic", method = RequestMethod.GET)
	public ResponseEntity<List<DynamicColumnsData>> dynamic() {

		List<DynamicColumnsData> tables = new ArrayList<DynamicColumnsData>();

		for (int i = 1; i <= 3; i++) {
			DynamicColumnsData data = new DynamicColumnsData();
			List<List<String>> columns = new ArrayList<List<String>>();
			for (int j = 1; j <= 3; j++) {
				List<String> cols = new ArrayList<String>();
				cols.add("Column " + j);
				columns.add(cols);
			}

			List<List<String>> values = new ArrayList<List<String>>();
			List<String> rowData = new ArrayList<String>();
			rowData.add("value 1");
			rowData.add("value 2");
			rowData.add("value 3");

			values.add(rowData);
			data.setColumns(columns);
			data.setData(values);
			data.setTableId("table" + i);

			tables.add(data);
		}

		return new ResponseEntity<List<DynamicColumnsData>>(tables, HttpStatus.OK);
	}

	@ModelAttribute("rfxList")
	public List<RfxTypes> getStatusList() {
		return Arrays.asList(RfxTypes.values());
	}

	@RequestMapping(value = "/suspendedList", method = RequestMethod.GET)
	public ResponseEntity<TableData<DraftEventPojo>> buyerSuspendedList(TableDataInput input) {
		TableData<DraftEventPojo> data = null;

		try {
			data = new TableData<DraftEventPojo>(rftEventService.getAllSuspendedEventsForBuyer(SecurityLibrary.getLoggedInUserTenantId(), input, SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId()));
			data.setDraw(input.getDraw());

			long totalCount = rftEventService.findTotalSuspendedEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), input);
			data.setRecordsTotal(totalCount);
			data.setRecordsFiltered(totalCount);

			LOG.info("  suspendedList  :" + totalCount);
		} catch (Exception e) {
			LOG.error("Error while loading draft list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<DraftEventPojo>>(data, HttpStatus.OK);
	}

	@RequestMapping(value = "/cancelledList", method = RequestMethod.GET)
	public ResponseEntity<TableData<DraftEventPojo>> buyerCancelledList(TableDataInput input) {
		TableData<DraftEventPojo> data = null;
		try {
			data = new TableData<DraftEventPojo>(rftEventService.getAllCancelledEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), input, SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId()));
			data.setDraw(input.getDraw());

			long totalCount = rftEventService.findTotalCancelledEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), input);
			data.setRecordsTotal(totalCount);
			data.setRecordsFiltered(totalCount);

			LOG.info(" cancelledList totalCount  :" + totalCount);
		} catch (Exception e) {
			LOG.error("Error while loading cancelled list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<DraftEventPojo>>(data, HttpStatus.OK);
	}

	@RequestMapping(value = "/closedList", method = RequestMethod.GET)
	public ResponseEntity<TableData<DraftEventPojo>> buyerClosedList(TableDataInput input) {
		TableData<DraftEventPojo> data = null;
		try {
			data = new TableData<DraftEventPojo>(rftEventService.getAllClosedEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), input, SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId()));
			data.setDraw(input.getDraw());

			long totalCount = rftEventService.findTotalClosedEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), input);
			data.setRecordsTotal(totalCount);
			data.setRecordsFiltered(totalCount);

			LOG.info(" closed List totalCount  :" + totalCount);
		} catch (Exception e) {
			LOG.error("Error while loading closed list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<DraftEventPojo>>(data, HttpStatus.OK);
	}

	@RequestMapping(value = "/completedList", method = RequestMethod.GET)
	public ResponseEntity<TableData<DraftEventPojo>> buyerCompletedList(TableDataInput input) {
		TableData<DraftEventPojo> data = null;
		try {
			data = new TableData<DraftEventPojo>(rftEventService.getAllCompletedEventsForBuyer(SecurityLibrary.getLoggedInUserTenantId(), input, SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId()));
			data.setDraw(input.getDraw());

			long totalCount = rftEventService.findTotalCompletedEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), input);
			data.setRecordsTotal(totalCount);
			data.setRecordsFiltered(totalCount);

			LOG.info(" Complete List totalCount  :" + totalCount);
		} catch (Exception e) {
			LOG.error("Error while loading Complete list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<DraftEventPojo>>(data, HttpStatus.OK);
	}

	@RequestMapping(value = "/draftList", method = RequestMethod.GET)
	public ResponseEntity<TableData<DraftEventPojo>> buyerDraftList(TableDataInput input) {
		TableData<DraftEventPojo> data = null;
		try {
			data = new TableData<DraftEventPojo>(rftEventService.getAllDraftEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), input, SecurityLibrary.getLoggedInUser().getId()));
			data.setDraw(input.getDraw());
			long totalCount = rftEventService.findTotalDraftEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), input);
			data.setRecordsTotal(totalCount);
			data.setRecordsFiltered(totalCount);
			// LOG.info(" totalCount :" + totalCount);
		} catch (Exception e) {
			LOG.error("Error while loading draft list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<DraftEventPojo>>(data, HttpStatus.OK);
	}

	@RequestMapping(value = "/ongoingEventList", method = RequestMethod.GET)
	public ResponseEntity<TableData<OngoingEventPojo>> buyerOngoingList(TableDataInput input) {
		List<OngoingEventPojo> ongoingEventList = null;
		TableData<OngoingEventPojo> data = null;
		long totalCount = 0;
		try {
			ongoingEventList = rftEventService.getAllOngoingEventsForBuyer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getLastLoginTime(), input, SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId());
			totalCount = rftEventService.findTotalOngoingEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), input);
			data = new TableData<OngoingEventPojo>(ongoingEventList, totalCount);
			data.setRecordsTotal(totalCount);
			data.setRecordsFiltered(totalCount);
		} catch (Exception e) {
			LOG.error("Error while loading draft list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<OngoingEventPojo>>(data, HttpStatus.OK);
	}

	@RequestMapping(value = "/finishedEventList", method = RequestMethod.GET)
	public ResponseEntity<TableData<FinishedEventPojo>> buyerFinishedList(@RequestParam int days, TableDataInput input) {
		List<FinishedEventPojo> finishedEventList = null;
		TableData<FinishedEventPojo> data = null;
		long totalCount = 0;
		try {
			finishedEventList = rftEventService.getAllFinishedEventsForBuyer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getLastLoginTime(), days, input, SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId());
			totalCount = rftEventService.findTotalFinishedEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), days, input);
			data = new TableData<FinishedEventPojo>(finishedEventList, totalCount);
			data.setRecordsTotal(totalCount);
			data.setRecordsFiltered(totalCount);
		} catch (Exception e) {
			LOG.error("Error while loading draft list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<FinishedEventPojo>>(data, HttpStatus.OK);
	}

	@RequestMapping(value = "/activeEventList", method = RequestMethod.GET)
	public ResponseEntity<TableData<ActiveEventPojo>> buyerActiveEventList(TableDataInput input) {
		TableData<ActiveEventPojo> data = null;
		try {
			data = new TableData<ActiveEventPojo>(rftEventService.getAllActiveEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getLastLoginTime(), input, SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId()));
			data.setDraw(input.getDraw());
			long totalCount = rftEventService.findTotalActiveEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), input);
			data.setRecordsTotal(totalCount);
			data.setRecordsFiltered(totalCount);
		} catch (Exception e) {
			LOG.error("Error while loading draft list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<ActiveEventPojo>>(data, HttpStatus.OK);
	}

	@RequestMapping(value = "/pendingPrList", method = RequestMethod.GET)
	public ResponseEntity<TableData<Pr>> buyerPendingPrList(TableDataInput input) {
		// LOG.info(" input : " + input + " SecurityLibrary.getLoggedInUserTenantId() :" +
		// SecurityLibrary.getLoggedInUserTenantId());
		TableData<Pr> data = null;
		try {
			data = new TableData<Pr>(prService.findAllPendingPr(SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), SecurityLibrary.getLoggedInUserTenantId(), input));
			data.setDraw(input.getDraw());
			long recordFiltered = prService.findTotalFilteredPendingPr(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), input);
			// long totalCount = prService.findTotalPr(SecurityLibrary.getLoggedInUserTenantId());
			data.setRecordsFiltered(recordFiltered);
			data.setRecordsTotal(recordFiltered);
		} catch (Exception e) {
			LOG.error("Error while loading Pr pending list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<Pr>>(data, HttpStatus.OK);
	}

	@RequestMapping(value = "/pendingEventList", method = RequestMethod.GET)
	public ResponseEntity<TableData<PendingEventPojo>> pendingEventList(TableDataInput input) {
		TableData<PendingEventPojo> data = null;
		try {
			data = new TableData<PendingEventPojo>(rftEventService.getAllPendingEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getLastLoginTime(), input, SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId()));
			data.setDraw(input.getDraw());
			long totalCount = rftEventService.findTotalPendingEventForBuyer(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), input);
			data.setRecordsTotal(totalCount);
			data.setRecordsFiltered(totalCount);
		} catch (Exception e) {
			LOG.error("Error while loading draft list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<PendingEventPojo>>(data, HttpStatus.OK);
	}

	@RequestMapping(value = "/myPendingEventApprovalList", method = RequestMethod.GET)
	public ResponseEntity<TableData<PendingEventPojo>> myPendingEventApprovalList(TableDataInput input) {
		TableData<PendingEventPojo> data = null;
		try {
			data = new TableData<PendingEventPojo>(rftEventService.findMyPendingRfxApprovals(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), input));
			data.setDraw(input.getDraw());
			long totalCount = rftEventService.findTotalMyPendingRfxApprovals(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), input);
			data.setRecordsTotal(totalCount);
			data.setRecordsFiltered(totalCount);
		} catch (Exception e) {
			LOG.error("Error while loading my pending event approval list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<PendingEventPojo>>(data, HttpStatus.OK);
	}

	@RequestMapping(value = "/myPendingPrApprovalList", method = RequestMethod.GET)
	public ResponseEntity<TableData<PendingEventPojo>> myPendingPrApprovalList(TableDataInput input) {
		TableData<PendingEventPojo> data = null;
		try {
			data = new TableData<PendingEventPojo>(rftEventService.findMyPendingPrApprovals(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), input));
			data.setDraw(input.getDraw());
			long totalCount = rftEventService.findTotalMyPendingPrApprovals(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), input);
			data.setRecordsTotal(totalCount);
			data.setRecordsFiltered(totalCount);
		} catch (Exception e) {
			LOG.error("Error while loading mt pending pr approval list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<PendingEventPojo>>(data, HttpStatus.OK);
	}

	@RequestMapping(value = "/myPendingEvaluationList", method = RequestMethod.GET)
	public ResponseEntity<TableData<PendingEventPojo>> myPendingEvaluationList(TableDataInput input) {
		TableData<PendingEventPojo> data = null;
		try {
			data = new TableData<PendingEventPojo>(rftEventService.findMyPendingEvaluation(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), input));
			data.setDraw(input.getDraw());
			long totalCount = rftEventService.findTotalMyPendingEvaluation(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), input);
			data.setRecordsTotal(totalCount);
			data.setRecordsFiltered(totalCount);
		} catch (Exception e) {
			LOG.error("Error while loading my pending evaluation list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<PendingEventPojo>>(data, HttpStatus.OK);
	}

	@RequestMapping(value = "/myPendingRequestApprovalList", method = RequestMethod.GET)
	public ResponseEntity<TableData<SourcingFormRequestPojo>> myPendingRequestApprovalList(TableDataInput input) {
		TableData<SourcingFormRequestPojo> data = null;
		try {
			data = new TableData<SourcingFormRequestPojo>(sourcingFormRequestService.findTotalMyPendingRequestList(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), input));
			data.setDraw(input.getDraw());
			long totalCount = sourcingFormRequestService.findTotalMyPendingRequestCount(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId());
			data.setRecordsTotal(totalCount);
			data.setRecordsFiltered(totalCount);
		} catch (Exception e) {
			LOG.error("Error while loading my pending Request list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<SourcingFormRequestPojo>>(data, HttpStatus.OK);
	}

	@RequestMapping(value = "/myDraftRequestList", method = RequestMethod.GET)
	public ResponseEntity<TableData<SourcingFormRequestPojo>> myDraftRequestList(TableDataInput input) {
		TableData<SourcingFormRequestPojo> data = null;
		try {

			List<SourcingFormRequestPojo> list = sourcingFormRequestService.myDraftRequestPojoList(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), input);
			data = new TableData<SourcingFormRequestPojo>(list);
			data.setDraw(input.getDraw());
			long totalCount = sourcingFormRequestService.myDraftRequestListCount(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), input);
			data.setRecordsTotal(totalCount);
			data.setRecordsFiltered(totalCount);
		} catch (Exception e) {
			LOG.error("Error while loading my Draft Request ListCount Request list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<SourcingFormRequestPojo>>(data, HttpStatus.OK);
	}

	@RequestMapping(value = "/myApprvedRequestList", method = RequestMethod.GET)
	public ResponseEntity<TableData<SourcingFormRequestPojo>> myApprvedRequestList(TableDataInput input) {
		TableData<SourcingFormRequestPojo> data = null;
		try {

			List<SourcingFormRequestPojo> list = sourcingFormRequestService.myApprvedRequestList(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), input);

			data = new TableData<SourcingFormRequestPojo>(list);
			data.setDraw(input.getDraw());
			long totalCount = sourcingFormRequestService.myApprovedRequestListCount(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), input);
			LOG.info("Total count +++++++++++++++++++++++++++ " + totalCount);

			data.setRecordsTotal(totalCount);
			data.setRecordsFiltered(totalCount);
		} catch (Exception e) {
			LOG.error("Error while loading my Draft Request ListCount Request list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<SourcingFormRequestPojo>>(data, HttpStatus.OK);
	}

	@RequestMapping(value = "/myPendingRequestAppList", method = RequestMethod.GET)
	public ResponseEntity<TableData<SourcingFormRequestPojo>> myPendingRequestList(TableDataInput input) {
		TableData<SourcingFormRequestPojo> data = null;
		try {
			data = new TableData<SourcingFormRequestPojo>(sourcingFormRequestService.myPendingRequestAppList(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), input));
			data.setDraw(input.getDraw());
			long totalCount = sourcingFormRequestService.myPendingRequestAppListCount(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), input);
			data.setRecordsTotal(totalCount);
			data.setRecordsFiltered(totalCount);
		} catch (Exception e) {
			LOG.error("Error while loading my Pending RequestList Request list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<SourcingFormRequestPojo>>(data, HttpStatus.OK);
	}

	@RequestMapping(value = "/myPendingRequestList", method = RequestMethod.GET)
	public ResponseEntity<TableData<SourcingFormRequestPojo>> myPendingApproveRequestList(TableDataInput input) {
		TableData<SourcingFormRequestPojo> data = null;
		try {
			data = new TableData<SourcingFormRequestPojo>(sourcingFormRequestService.myPendingRequestList(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), input));
			data.setDraw(input.getDraw());
			long totalCount = sourcingFormRequestService.myPendingRequestListCount(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), input);
			data.setRecordsTotal(totalCount);
			data.setRecordsFiltered(totalCount);
		} catch (Exception e) {
			LOG.error("Error while loading my Pending RequestList Request list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<SourcingFormRequestPojo>>(data, HttpStatus.OK);
	}

	@RequestMapping(value = "/myCompleteRequestList", method = RequestMethod.GET)
	public ResponseEntity<TableData<SourcingFormRequestPojo>> myCompleteRequestList(TableDataInput input) {
		TableData<SourcingFormRequestPojo> data = null;
		try {
			data = new TableData<SourcingFormRequestPojo>(sourcingFormRequestService.myCompletedRequestList(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_REQUEST_ALL") ? null : SecurityLibrary.getLoggedInUser().getId(), input));
			data.setDraw(input.getDraw());

			long totalCount = sourcingFormRequestService.myPendingRequestListCount(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_REQUEST_ALL") ? null : SecurityLibrary.getLoggedInUser().getId(), input);
			data.setRecordsTotal(totalCount);
			data.setRecordsFiltered(totalCount);
			LOG.info(totalCount);
		} catch (Exception e) {
			LOG.error("Error while loading my Pending RequestList Request list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<SourcingFormRequestPojo>>(data, HttpStatus.OK);
	}

	@RequestMapping(value = "/myCancelRequestList", method = RequestMethod.GET)
	public ResponseEntity<TableData<SourcingFormRequestPojo>> myCancelRequestList(TableDataInput input) {
		TableData<SourcingFormRequestPojo> data = null;
		try {

			List<SourcingFormRequestPojo> list = sourcingFormRequestService.myCancelRequestList(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), input);

			data = new TableData<SourcingFormRequestPojo>(list);
			data.setDraw(input.getDraw());
			long totalCount = sourcingFormRequestService.getCancelRequestCount(SecurityLibrary.getLoggedInUserTenantId(), input, SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId());
			data.setRecordsTotal(totalCount);
			data.setRecordsFiltered(totalCount);
		} catch (Exception e) {
			LOG.error("Error while loading my Draft Request ListCount Request list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<SourcingFormRequestPojo>>(data, HttpStatus.OK);
	}

	@RequestMapping(value = "/myFinishRequestList", method = RequestMethod.GET)
	public ResponseEntity<TableData<SourcingFormRequestPojo>> myFinishRequestList(TableDataInput input) {
		TableData<SourcingFormRequestPojo> data = null;
		try {

			List<SourcingFormRequestPojo> list = sourcingFormRequestService.myFinishRequestList(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), input);

			data = new TableData<SourcingFormRequestPojo>(list);
			data.setDraw(input.getDraw());
			long totalCount = sourcingFormRequestService.finishedRequestCount(SecurityLibrary.getLoggedInUserTenantId(), input, SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId());
			data.setRecordsTotal(totalCount);
			data.setRecordsFiltered(totalCount);
		} catch (Exception e) {
			LOG.error("Error while loading my Draft Request ListCount Request list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<SourcingFormRequestPojo>>(data, HttpStatus.OK);
	}

	@RequestMapping(path = "/budgetData", method = RequestMethod.GET)
	public ResponseEntity<TableData<BudgetPojo>> budgetTemplateData(TableDataInput input) {
		TableData<BudgetPojo> data = null;
		try {
			data = new TableData<BudgetPojo>(budgetService.getAllBudgetForApproval(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), input));
			data.setDraw(input.getDraw());
			LOG.info("***************budget query data" + data.getRecordsTotal());
			long totalFilteredCount = budgetService.findTotalBudgetForApproval(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), input);
			data.setRecordsFiltered(totalFilteredCount);
			LOG.info("***************totalFilteredCount" + totalFilteredCount);
			long totalCount = budgetService.findTotalCountBudgetForApproval(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), input);
			data.setRecordsTotal(totalFilteredCount);
			LOG.info(" totalCount :" + totalCount);
		} catch (Exception e) {
			LOG.error("Error while loading budget list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<BudgetPojo>>(data, HttpStatus.OK);
	}

	@RequestMapping(path = "/readyPoData", method = RequestMethod.GET)
	public ResponseEntity<TableData<Po>> readyPoData(TableDataInput input, HttpServletResponse response) {
		try {
			LOG.info("Start : " + input.getStart() + " Length : " + input.getLength() + " Sort : " + input.getSort());
			LOG.debug(">>>>>>>>>>>>> Table Data Input : " + input.toString());
			List<Po> poList = poService.findAllPoByStatus(SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), SecurityLibrary.getLoggedInUserTenantId(), input, PoStatus.READY);
			TableData<Po> data = new TableData<Po>(poList);
			data.setDraw(input.getDraw());
			long recordFiltered = poService.findTotalFilteredPoByStatus(SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), SecurityLibrary.getLoggedInUserTenantId(), input, PoStatus.READY);
			data.setRecordsFiltered(recordFiltered);
			data.setRecordsTotal(recordFiltered);
			return new ResponseEntity<TableData<Po>>(data, HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Error fetching ready po list : " + e.getMessage(), e);
			HttpHeaders headers = new HttpHeaders();
			headers.add("error", "Error fetching ready PO list : " + e.getMessage());
			return new ResponseEntity<TableData<Po>>(null, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(path = "/orderedPoData", method = RequestMethod.GET)
	public ResponseEntity<TableData<Po>> orderedPoData(TableDataInput input, HttpServletResponse response) {
		try {
			LOG.info("Start : " + input.getStart() + " Length : " + input.getLength() + " Sort : " + input.getSort());
			LOG.debug(">>>>>>>>>>>>> Table Data Input : " + input.toString());
			List<Po> poList = poService.findAllPoByStatus(SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), SecurityLibrary.getLoggedInUserTenantId(), input, PoStatus.ORDERED);
			TableData<Po> data = new TableData<Po>(poList);
			data.setDraw(input.getDraw());
			long recordFiltered = poService.findTotalFilteredPoByStatus(SecurityLibrary.ifAnyGranted("ROLE_ADMIN,ROLE_ADMIN_READONLY") ? null : SecurityLibrary.getLoggedInUser().getId(), SecurityLibrary.getLoggedInUserTenantId(), input, PoStatus.ORDERED);
			data.setRecordsFiltered(recordFiltered);
			data.setRecordsTotal(recordFiltered);
			return new ResponseEntity<TableData<Po>>(data, HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Error fetching orders po list : " + e.getMessage(), e);
			HttpHeaders headers = new HttpHeaders();
			headers.add("error", "Error fetching orders PO list : " + e.getMessage());
			return new ResponseEntity<TableData<Po>>(null, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/myPendingSupplierList", method = RequestMethod.GET)
	public ResponseEntity<TableData<SupplierFormSubmissionPojo>> myPendingSupplierFormList(TableDataInput input) {
		TableData<SupplierFormSubmissionPojo> data = null;
		try {
			data = new TableData<SupplierFormSubmissionPojo>(supplierFormSubmissionService.myPendingRequestList(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), input));
			data.setDraw(input.getDraw());
			long totalCount = supplierFormSubmissionService.myPendingSupplierFormListCount(SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser().getId(), input);
			data.setRecordsTotal(totalCount);
			data.setRecordsFiltered(totalCount);
		} catch (Exception e) {
			LOG.error("Error while loading my Pending Supplier form Request list : " + e.getMessage(), e);
		}
		return new ResponseEntity<TableData<SupplierFormSubmissionPojo>>(data, HttpStatus.OK);
	}

}