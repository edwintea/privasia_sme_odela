/**
 * 
 */
package com.privasia.procurehere.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import com.privasia.procurehere.core.dao.AccessRightsDao;
import com.privasia.procurehere.core.entity.*;
import com.privasia.procurehere.core.enums.AuditTypes;
import com.privasia.procurehere.core.enums.ModuleType;
import com.privasia.procurehere.core.enums.SupplierStatus;
import com.privasia.procurehere.core.enums.TenantType;
import com.privasia.procurehere.core.utils.*;
import com.privasia.procurehere.service.*;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

import com.privasia.procurehere.core.dao.SupplierSubscriptionDao;
import com.privasia.procurehere.core.exceptions.ApplicationException;
import com.privasia.procurehere.service.supplier.BuyerSupplierRegistrationService;
import com.privasia.procurehere.service.supplier.SupplierService;
import com.privasia.procurehere.web.editors.CountryEditor;
import com.privasia.procurehere.web.editors.IndustryCategoryEditor;
import com.privasia.procurehere.web.editors.ProductCategoryMaintenanceEditor;

/**
 * @author ravi
 */
@Controller
@RequestMapping(value = "/")
public class BuyerSupplierRegistrationController implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1284768145262445266L;

    private static final Logger LOG = Logger.getLogger(Global.SUPPLIER_LOG);

    @Resource
    MessageSource messageSource;

    @Autowired
    BuyerSupplierRegistrationService buyerSupplierRegistrationService;

    @Autowired
    SupplierService supplierService;

    @Autowired
    UserRoleService userRoleService;

    @Autowired
    AccessRightsDao accessRightsDao;

    @Autowired
    CountryService countryService;

    @Autowired
    CountryEditor countryEditor;

    @Autowired
    BuyerService buyerService;

    @Autowired
    SupplierSubscriptionDao supplierSubscriptionDao;

    @Autowired
    SupplierPlanService supplierPlanService;

    @Autowired
    PasswordSettingService passwordSettingService;

    @Autowired
    UserService userService;

    @Autowired
    BuyerSettingsService buyerSettingsService;

    @Autowired
    ProductCategoryMaintenanceEditor productCategoryMaintenanceEditor;

    @Autowired
    IndustryCategoryService industryCategoryService;

    @Autowired
    IndustryCategoryEditor industryCategoryEditor;

    @Autowired
    VelocityEngine velocityEngine;

    /**
     * @param binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Country.class, countryEditor);
        binder.registerCustomEditor(IndustryCategory.class, industryCategoryEditor);
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
        StringTrimmerEditor stringtrimmer = new StringTrimmerEditor(true);
        binder.registerCustomEditor(String.class, stringtrimmer);
    }

    @RequestMapping(path = "/register", method = { RequestMethod.GET })
    public String supplierRegistration(Model model) {
        LOG.info(" *********** supplierRegistration called *************** ");
            Buyer buyer = new Buyer();

            LOG.info("Buyer Details found .................");

            Supplier supplier = new Supplier();
            Country my = countryService.getCountryByCode("MY");
            Country country = new Country(my.getId(), my.getCountryCode(), my.getCountryName(), my.getStatus());
            supplier.setRegistrationOfCountry(country);
            model.addAttribute("countryList", countryService.getAllCountries());
            model.addAttribute("supplier", supplier);
            model.addAttribute("companyName", supplier.getCompanyName());
             SupplierSettings supplierSettings = new SupplierSettings();

            try {


            if (supplierSettings != null && supplierSettings.getFileAttatchment() != null) {
                byte[] encodeBase64 = Base64.encodeBase64(supplierSettings.getFileAttatchment());
                String base64Encoded = new String(encodeBase64, "UTF-8");
                model.addAttribute("companyLogo", base64Encoded);
              }
            } catch (Exception e) {
                LOG.error("Error encoding Image" + e.getMessage(), e);
            }

            List<IndustryCategory> icList = industryCategoryService.getAllIndustryCategoryByIds(null);
            model.addAttribute("icList", icList);




            LOG.info("Sending details...");
            return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String saveRegistration(@ModelAttribute("supplier") Supplier supplier, BindingResult result, Model model) {
          Buyer buyer = new Buyer();
     
        if (result.hasErrors()) {
            LOG.error("Page submitted With Errors ............................. ");
            List<String> errMessages = new ArrayList<String>();
            for (ObjectError field : result.getAllErrors()) {
                LOG.info("ERROR : " + field.getObjectName() + " : " + field.getDefaultMessage());
                errMessages.add(field.getDefaultMessage());
            }
            model.addAttribute("errors", errMessages);
            model.addAttribute("companyName", supplier.getCompanyName());
            SupplierSettings supplierSettings = new SupplierSettings();
            List<IndustryCategory> icList = industryCategoryService.getAllIndustryCategoryByIds(null);
             model.addAttribute("icList", icList);
           

            try {

                
                if (supplierSettings != null && supplierSettings.getFileAttatchment() != null) {
                    byte[] encodeBase64 = Base64.encodeBase64(supplierSettings.getFileAttatchment());
                    String base64Encoded = new String(encodeBase64, "UTF-8");
                    model.addAttribute("companyLogo", base64Encoded);
                }
            } catch (Exception e) {
                LOG.error("Error encoding Image" + e.getMessage(), e);
            }
            return "redirect:register";
        } else {
            try {
                if (validate(supplier, model)) {
                    LOG.info("Registration country : " + supplier.getRegistrationOfCountry().getCountryName());
                    model.addAttribute("supplier", supplier);
                    model.addAttribute("countryList", countryService.getAllCountries());
                    model.addAttribute("companyName", supplier.getCompanyName());
                    SupplierSettings supplierSettings = new SupplierSettings();
                    List<IndustryCategory> icList = industryCategoryService.getAllIndustryCategoryByIds(null);
                    model.addAttribute("icList", icList);
                   

                    try {

                        if (supplierSettings != null && supplierSettings.getFileAttatchment() != null) {
                            byte[] encodeBase64 = Base64.encodeBase64(supplierSettings.getFileAttatchment());
                            String base64Encoded = new String(encodeBase64, "UTF-8");
                            model.addAttribute("companyLogo", base64Encoded);
                        }
                    } catch (Exception e) {
                        LOG.error("Error encoding Image" + e.getMessage(), e);
                    }
                    LOG.info("Returning from error....");
                    return "register";
                }
                supplier = buyerSupplierRegistrationService.registerSupplier(supplier, buyer.getId());
                LOG.info("Supplier " + supplier.getCompanyName() + " Created successfully");
            } catch (ApplicationException e) {
                LOG.error("Error while storing Supplier : " + e.getMessage(), e);
                model.addAttribute("errors", e.getMessage());
                model.addAttribute("supplier", supplier);
                model.addAttribute("countryList", countryService.getAllCountries());
                model.addAttribute("companyName", supplier.getCompanyName());
                SupplierSettings supplierSettings = new SupplierSettings();
                List<IndustryCategory> icList = industryCategoryService.getAllIndustryCategoryByIds(null);
                model.addAttribute("icList", icList);

                try {

                    if (supplierSettings != null && supplierSettings.getFileAttatchment() != null) {
                                               byte[] encodeBase64 = Base64.encodeBase64(supplierSettings.getFileAttatchment());
                                               String base64Encoded = new String(encodeBase64, "UTF-8");
                                               model.addAttribute("companyLogo", base64Encoded);
                                           }
                } catch (Exception e1) {
                    LOG.error("Error encoding Image" + e1.getMessage(), e1);
                }
                LOG.info("Returning from error....");
                return "register";

            } catch (Exception e) {
                model.addAttribute("errors", messageSource.getMessage("supplier.error.detail", new Object[] { supplier.getCompanyName(), e.getMessage() }, Global.LOCALE));
                model.addAttribute("supplier", supplier);
                model.addAttribute("countryList", countryService.getAllCountries());
                model.addAttribute("companyName", supplier.getCompanyName());
                SupplierSettings supplierSettings = new SupplierSettings();
                List<IndustryCategory> icList = industryCategoryService.getAllIndustryCategoryByIds(null);
                model.addAttribute("icList", icList);
                

                try {

                     if (supplierSettings != null && supplierSettings.getFileAttatchment() != null) {
                        byte[] encodeBase64 = Base64.encodeBase64(supplierSettings.getFileAttatchment());
                        String base64Encoded = new String(encodeBase64, "UTF-8");
                        model.addAttribute("companyLogo", base64Encoded);
                        }
                } catch (Exception e1) {
                    LOG.error("Error encoding Image" + e1.getMessage(), e1);
                }
                LOG.info("Returning from error....");
                return "register";
            }
            //send email to ne supplier register
            return "redirect:buyerSupplierCreationThankyou";
        }

    }

    @RequestMapping(path = "/buyerSupplierCreationThankyou", method = RequestMethod.GET)
    public String buyerSupplierCreationThankyou() {
        LOG.info("supplier Checkout Thank you GET called");
        return "buyerSupplierCreationThankyou";
    }

    private boolean validate(Supplier supplier, Model model) {
        boolean isError = false;

        User user = userService.getPlainUserByLoginId(supplier.getLoginEmail());
        String regex = null;
        String msg = null;
        if (user != null) {
            PasswordSettings settings = passwordSettingService.getPasswordRegex(user.getTenantId());
            if (settings != null) {
                regex = settings.getRegx();
                msg = null;
            }
        }
        if (supplierService.isExistsLoginEmail(supplier.getLoginEmail())) {
            model.addAttribute("errors", messageSource.getMessage("supplier.login.email.exisit", new Object[] { supplier.getLoginEmail() }, Global.LOCALE));
            isError = true;
        } else if (userService.getUserByLoginName(supplier.getLoginEmail()) != null) {
            model.addAttribute("errors", messageSource.getMessage("supplier.login.email.exisit", new Object[] { supplier.getLoginEmail() }, Global.LOCALE));
            isError = true;
        }
        if (!StringUtils.validatePasswordWithRegx(supplier.getPassword(), regex)) {
            model.addAttribute("errors", StringUtils.checkString(msg).length() > 0 ? msg : messageSource.getMessage("user.password.week", new Object[] { supplier.getLoginEmail() }, Global.LOCALE));
            isError = true;
        }

        if (supplierService.isExists(supplier)) {
            model.addAttribute("errors", messageSource.getMessage("supplier.error.duplicate", new Object[] { supplier.getCompanyName(), supplier.getCompanyRegistrationNumber(), supplier.getRegistrationOfCountry() }, Global.LOCALE));
            isError = true;
        }

        return isError;
    }

    @RequestMapping(value = "/register1", method = RequestMethod.POST,consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<Object>  doRegister(@RequestBody MultiValueMap<String, String> mdl,Model model) {

        HttpHeaders headers = new HttpHeaders();
        User user = new User();
        Supplier supplier = new Supplier();

        try {

            supplier.setCompanyName(mdl.get("companyName").get(0));
            supplier.setCompanyRegistrationNumber(mdl.get("companyRegistrationNumber").get(0));

            System.out.println("!--REQUEST START--!" + countryService.searchCountryById(mdl.get("registrationOfCountry").get(0)));
            supplier.setRegistrationOfCountry(countryService.searchCountryById(mdl.get("registrationOfCountry").get(0)));
            supplier.setLoginEmail(mdl.get("loginEmail").get(0));
            supplier.setPassword(mdl.get("password").get(0));
            supplier.setCommunicationEmail(mdl.get("communicationEmail").get(0));
            supplier.setFullName(mdl.get("fullName").get(0));
            supplier.setCompanyContactNumber(mdl.get("companyContactNumber").get(0));
            supplier.setStatus(SupplierStatus.PENDING);
            supplier.setCreatedBy(null);

            //supplier.setIndustryCategories(mdl.get("_industryCategories").toString());


            if(supplierService.isExistsByCompanyNameRegistrationNoLoginEmail(supplier)){
                System.out.println("!--EXISTING--!" + supplierService.isExistsByCompanyNameRegistrationNoLoginEmail(supplier));
                return ResponseHandler.generateResponse("Account exist!", HttpStatus.BAD_REQUEST, new EmptyJsonResponse());

            }else{
                //send email to the supplier for register
                supplierService.saveSupplier(supplier, true);

                /**
                 * Create User Role
                 */
                UserRole userRole = new UserRole();
                userRole = new UserRole();
                userRole.setRoleName("Administrator".toUpperCase());
                userRole.setRoleDescription("Application Administrator");
                userRole.setCreatedDate(new Date());
                userRole.setTenantId(supplier.getId());

                userRole.setCreatedBy(SecurityLibrary.getLoggedInUser());
                userRoleService.saveUserRole(userRole, accessRightsDao.getAccessControlListForSupplier(false));

                //register as user system;
                user.setLoginId(supplier.getLoginEmail().toUpperCase());
                user.setCommunicationEmail(supplier.getCommunicationEmail());
                user.setTenantId(supplier.getId());
                user.setTenantType(TenantType.SUPPLIER);
                user.setName(supplier.getCompanyName());
                user.setPassword(supplier.getPassword());
                user.setSupplier(supplier);
                user.setCreatedDate(new Date());
                user.setBuyer(buyerService.findBuyerById(SecurityLibrary.getLoggedInUserTenantId()));
                SupplierSubscription supplierSubscription = supplierSubscriptionDao.getCurrentSubscriptionForSupplier(supplier.getId());
                if (supplierSubscription != null) {
                    if (supplierSubscription.getSupplierPlan() != null) {
                        user.setShowWizardTutorial(Boolean.FALSE);
                    } else {
                        user.setShowWizardTutorial(Boolean.TRUE);
                    }
                }

                user.setCreatedBy(SecurityLibrary.getLoggedInUser());
                user.setUserRole(userRole);
                userService.saveUser(user);
                 LOG.info("SUPPLIER CONFIRMATION USER CREATED FOR SUPPLIER [ " + supplier.getCompanyName() + "] LOGIN ID : [" + supplier.getLoginEmail() + " ]");

            }

        }catch (Exception e){
             return ResponseHandler.generateResponse("Error : "+e.getMessage(), HttpStatus.OK, new EmptyJsonResponse());

        }

        return ResponseHandler.generateResponse("Successfully retrieve data!", HttpStatus.OK, new EmptyJsonResponse());

    }

}
