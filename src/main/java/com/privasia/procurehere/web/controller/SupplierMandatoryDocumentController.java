package com.privasia.procurehere.web.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.privasia.procurehere.core.dao.OwnerAuditTrailDao;
import com.privasia.procurehere.core.dao.SupplierAuditTrailDao;
import com.privasia.procurehere.core.dao.SupplierMandatoryDocumentDao;
import com.privasia.procurehere.core.entity.*;
import com.privasia.procurehere.core.enums.AuditTypes;
import com.privasia.procurehere.core.enums.ModuleType;
import com.privasia.procurehere.core.exceptions.SecurityRuntimeException;
import com.privasia.procurehere.core.pojo.SupplierIntigrationPojo;
import com.privasia.procurehere.core.utils.*;
import com.privasia.procurehere.service.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;


import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.privasia.procurehere.service.OwnerSettingsService;
import com.privasia.procurehere.core.entity.SupplierMandatoryDocument;
import com.privasia.procurehere.core.pojo.SupplierMandatoryDocumentPojo;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Edwin DJava
 * @author edwin
 */

@Controller
@RequestMapping(path = "/buyer")
public class SupplierMandatoryDocumentController implements Serializable {
	private static final Logger LOG = Logger.getLogger(Global.SUPPLIER_LOG);


	@Autowired
	SupplierMandatoryDocumentService supplierMandatoryDocumentService;

	@Autowired
	SupplierMandatoryDocumentDao supplierMandatoryDocumentDao;

	@Autowired
	OwnerSettingsService ownerSettingsService;

	@Autowired
	BuyerAuditTrailService buyerAuditTrailService ;

	@Autowired
	OwnerAuditTrailService ownerAuditTrailService ;

	@Autowired
	OwnerAuditTrailDao ownerAuditTrailDao;

	@Autowired
	UserService userService;

    @Autowired
    private Environment env;



    @InitBinder
	public void initBinder(ServletRequestDataBinder binder) {

	}

	@RequestMapping(path = "/supplierMandatoryDocument", method = RequestMethod.GET)
	public String supplierMandatoryDocument1(Model model) throws JsonProcessingException {
		LOG.info("Listing  Supplier Mandatory Document Data: " );
		model.addAttribute("countrys", "MY");
		return "supplierMandatoryDocument"; //redirecting only
	}

	/**
	 * @param file
	 * @param title
	 * @param status
	 * @return
	 */
	@RequestMapping(value = "/createSupplierMandatoryDocuments", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<List<SupplierMandatoryDocument>> uploadDocument(@Validated @RequestParam("file") MultipartFile file, @RequestParam("title") String title, @RequestParam("status") Boolean status) {
		String fileName = null;
		HttpHeaders headers = new HttpHeaders();


		if (!file.isEmpty()) {

			List<String> fileTypes = ownerSettingsService.getOwnersettings().getFileTypes();
			if (!StringUtils.validateAttachmentFile(file.getOriginalFilename(), fileTypes)) {
				headers.add("error", "Only " + String.join(",", fileTypes) + " files are allowed.");
				return new ResponseEntity<List<SupplierMandatoryDocument>>(null, headers, HttpStatus.BAD_REQUEST);
			}

			try {



				fileName = file.getOriginalFilename();
				byte[] bytes = file.getBytes();
				LOG.info("FILE CONTENT" + bytes);

				SupplierMandatoryDocument document = new SupplierMandatoryDocument();
				document.setFileName(fileName);
				document.setContentType(file.getContentType());
				document.setDescription(title);
				document.setFileName(fileName);
				document.setFileData(bytes);
				document.setUploadDate(new Date());
				document.setCreatedDate(new Date());
                document.setStatus(status);

				document.setCreatedBy(SecurityLibrary.getLoggedInUser());
				document.setUpdatedDate(new Date());
				document.setUpdatedBy(SecurityLibrary.getLoggedInUser().getLoginId());
				document.setFileSize(bytes.length > 0 ? bytes.length / 1024 : 0);
				document.setFileSizeInKb(bytes.length > 0 ? bytes.length / 1024 : 0);

				if(document.getStatus()){
					if(supplierMandatoryDocumentService.isExistsByActiveTitle(document)) {
						return new ResponseEntity<List<SupplierMandatoryDocument>>(null, headers, HttpStatus.BAD_REQUEST);
					}
				}

				supplierMandatoryDocumentDao.saveOrUpdate(document);

				// INSERT INTO AUDIT TRAIL

				BuyerAuditTrail buyerAuditTrail = new BuyerAuditTrail(AuditTypes.CREATE, fileName + " Is Created ", SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser(), new Date(), ModuleType.SupplierMandatoryDocument);
				buyerAuditTrail.setActionBy(SecurityLibrary.getLoggedInUser());
				buyerAuditTrailService.save(buyerAuditTrail);

				OwnerAuditTrail ownerAuditTrail = new OwnerAuditTrail(AuditTypes.CREATE, fileName + " Is Created ", SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser(), new Date(), ModuleType.SupplierMandatoryDocument);
				ownerAuditTrail.setActionBy(SecurityLibrary.getLoggedInUser());
				ownerAuditTrailService.save(ownerAuditTrail);


				List<SupplierMandatoryDocument> docsList = supplierMandatoryDocumentService.getAll();
				headers.add("success", "File " + fileName + " upload successfully ");

				return new ResponseEntity<List<SupplierMandatoryDocument>>(docsList, headers, HttpStatus.OK);

			} catch (Exception e) {
				LOG.error("You failed to upload " + fileName + ": " + e.getMessage(), e);
				headers.add("error", "You failed to upload " + fileName);
				return new ResponseEntity<List<SupplierMandatoryDocument>>(null, headers, HttpStatus.BAD_REQUEST);
			}
		}



		return new ResponseEntity<List<SupplierMandatoryDocument>>(null, headers, HttpStatus.BAD_REQUEST);
	}

	/**
	 * @param file
	 * @param title
	 * @param status
	 * @return
	 */
	@RequestMapping(value = "/updateSupplierMandatoryDocuments", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<List<SupplierMandatoryDocument>> updateSupplierMandatoryDocuments(@Validated @RequestParam("file") MultipartFile file, @RequestParam("id") String id,@RequestParam("title") String title, @RequestParam("status") Boolean status) {
		String fileName = null;
		HttpHeaders headers = new HttpHeaders();


		if (!file.isEmpty()) {

			List<String> fileTypes = ownerSettingsService.getOwnersettings().getFileTypes();
			if (!StringUtils.validateAttachmentFile(file.getOriginalFilename(), fileTypes)) {
				headers.add("error", "Only " + String.join(",", fileTypes) + " files are allowed.");
				return new ResponseEntity<List<SupplierMandatoryDocument>>(null, headers, HttpStatus.BAD_REQUEST);
			}

			try {

				fileName = file.getOriginalFilename();
				byte[] bytes = file.getBytes();
				LOG.info("FILE CONTENT" + bytes);

				SupplierMandatoryDocument document = new SupplierMandatoryDocument();
				document.setId(id);
				document.setFileName(fileName);
				document.setContentType(file.getContentType());
				document.setDescription(title);
				document.setFileName(fileName);
				document.setFileData(bytes);
				document.setUploadDate(new Date());
				document.setCreatedDate(new Date());
				document.setStatus(status);

				document.setCreatedBy(SecurityLibrary.getLoggedInUser());
				document.setUpdatedDate(new Date());
				document.setUpdatedBy(SecurityLibrary.getLoggedInUser().getLoginId());
				document.setFileSize(bytes.length > 0 ? bytes.length / 1024 : 0);
				document.setFileSizeInKb(bytes.length > 0 ? bytes.length / 1024 : 0);

				if(document.getStatus()){
					if(supplierMandatoryDocumentService.isExistsByActiveTitle(document)) {
						return new ResponseEntity<List<SupplierMandatoryDocument>>(null, headers, HttpStatus.BAD_REQUEST);
					}
				}

				supplierMandatoryDocumentDao.saveOrUpdate(document);

				// INSERT INTO AUDIT TRAIL

				BuyerAuditTrail buyerAuditTrail = new BuyerAuditTrail(AuditTypes.UPDATE, fileName + " Is Created ", SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser(), new Date(), ModuleType.SupplierMandatoryDocument);
				buyerAuditTrail.setActionBy(SecurityLibrary.getLoggedInUser());
				buyerAuditTrailService.save(buyerAuditTrail);

				OwnerAuditTrail ownerAuditTrail = new OwnerAuditTrail(AuditTypes.UPDATE, fileName + " Is Created ", SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser(), new Date(), ModuleType.SupplierMandatoryDocument);
				ownerAuditTrail.setActionBy(SecurityLibrary.getLoggedInUser());
				ownerAuditTrailService.save(ownerAuditTrail);


				List<SupplierMandatoryDocument> docsList = supplierMandatoryDocumentService.getAll();
				headers.add("success", "File " + fileName + " upload successfully ");

				return new ResponseEntity<List<SupplierMandatoryDocument>>(docsList, headers, HttpStatus.OK);

			} catch (Exception e) {
				LOG.error("You failed to upload " + fileName + ": " + e.getMessage(), e);
				headers.add("error", "You failed to upload " + fileName);
				return new ResponseEntity<List<SupplierMandatoryDocument>>(null, headers, HttpStatus.BAD_REQUEST);
			}
		}



		return new ResponseEntity<List<SupplierMandatoryDocument>>(null, headers, HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/allSupplierMandatoryDocuments", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<SupplierMandatoryDocument>> allSupplierMandatoryDocuments() {
		String fileName = null;
		HttpHeaders headers = new HttpHeaders();


		try {

			List<SupplierMandatoryDocument> docsList = supplierMandatoryDocumentService.getAll();
			headers.add("success", "View All Files ");

			return new ResponseEntity<List<SupplierMandatoryDocument>>(docsList, headers, HttpStatus.OK);

		} catch (Exception e) {
			LOG.error("You failed to get datas" + e.getMessage(), e);
			headers.add("error", "You failed to get datas " + fileName);
			return new ResponseEntity<List<SupplierMandatoryDocument>>(null, headers, HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value = "/deleteDocument", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<SupplierMandatoryDocument>> deleteDocument(@RequestParam("id") String removeId, @RequestParam("name") String removeName) {

		String fileName = null;
		HttpHeaders headers = new HttpHeaders();

		try {
			supplierMandatoryDocumentService.deleteDocument(removeId);

			// INSERT INTO AUDIT TRAIL
			BuyerAuditTrail buyerAuditTrail = new BuyerAuditTrail(AuditTypes.DELETE, removeName + " Is Deleted ",SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser(), new Date(), ModuleType.SupplierMandatoryDocument);
			buyerAuditTrail.setActionBy(SecurityLibrary.getLoggedInUser());
			buyerAuditTrailService.save(buyerAuditTrail);

			OwnerAuditTrail ownerAuditTrail = new OwnerAuditTrail(AuditTypes.DELETE, removeName + " Is Deleted ",SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser(), new Date(), ModuleType.SupplierMandatoryDocument);
			ownerAuditTrail.setActionBy(SecurityLibrary.getLoggedInUser());
			ownerAuditTrailService.save(ownerAuditTrail);


			headers.add("success", "View All Files ");

			return new ResponseEntity<List<SupplierMandatoryDocument>>(supplierMandatoryDocumentService.getAll(), headers, HttpStatus.OK);

		} catch (Exception e) {
			LOG.error("You failed to upload " + fileName + ": " + e.getMessage(), e);
			headers.add("error", "You failed to upload " + fileName);
			return new ResponseEntity<List<SupplierMandatoryDocument>>(null, headers, HttpStatus.BAD_REQUEST);
		}

	}



}