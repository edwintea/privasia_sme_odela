package com.privasia.procurehere.web.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.privasia.procurehere.core.entity.User;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.core.utils.SecurityLibrary;
import com.privasia.procurehere.service.TransferOwnershipService;
import com.privasia.procurehere.service.UserService;
import com.privasia.procurehere.web.editors.UserEditor;

import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;

/**
 * @author Priyanka Singh
 */
@Controller
@RequestMapping("/buyer")
public class TransferOwnershipController {

	private static final Logger LOG = Logger.getLogger(Global.ADMIN_LOG);

	@Autowired
	UserService userService;

	@Autowired
	TransferOwnershipService transferOwnershipService;

	@Resource
	MessageSource messageSource;

	@Autowired
	UserEditor userEditor;

	@InitBinder
	public void initBinder(ServletRequestDataBinder binder) {
		binder.registerCustomEditor(User.class, userEditor);
	}

	@RequestMapping(path = "/transferOwnership", method = RequestMethod.GET)
	public ModelAndView transferOwnership(Model model) throws JsonProcessingException {
		LOG.info("transferOwnership create Called");
		List<User> userList = userService.fetchAllActiveUsersForTenant(SecurityLibrary.getLoggedInUserTenantId());
		model.addAttribute("userList", userList);
		return new ModelAndView("transferOwnership");
	}

	@RequestMapping(path = "/saveTransferOwnership", method = RequestMethod.POST)
	public String saveTransferOwnership(@RequestParam(name = "fromUser") String fromUser, @RequestParam(name = "toUser") String toUser, Model model, RedirectAttributes redir, HttpSession session) {
		User user = null;
		user = userService.findUserById(fromUser);
		String from = user.getName();
		user = userService.findUserById(toUser);
		String assignTo = user.getName();
		JRSwapFileVirtualizer virtualizer = null;
		try {
			virtualizer = new JRSwapFileVirtualizer(100, new JRSwapFile(System.getProperty("java.io.tmpdir"), 2048, 1024), false);
			transferOwnershipService.saveTransferOwnership(fromUser, toUser, session, virtualizer);
			redir.addFlashAttribute("success", messageSource.getMessage("event.audit.transfer", new Object[] { from, assignTo }, Global.LOCALE));
			return "redirect:transferOwnership";
		} catch (Exception e) {
			LOG.error("Error in saving TransferOwnership " + e.getMessage(), e);
			model.addAttribute("error", messageSource.getMessage("event.audit.transfer.error", new Object[] { from, assignTo }, Global.LOCALE));
			return "transferOwnership";
		} finally {
			if (virtualizer != null) {
				virtualizer.cleanup();

			}
		}
	}
}
