package com.privasia.procurehere.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.privasia.procurehere.core.entity.CostCenter;
import com.privasia.procurehere.core.enums.Status;
import com.privasia.procurehere.core.pojo.TableData;
import com.privasia.procurehere.core.pojo.TableDataInput;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.core.utils.SecurityLibrary;
import com.privasia.procurehere.core.utils.StringUtils;
import com.privasia.procurehere.service.BuyerService;
import com.privasia.procurehere.service.CostCenterService;

/**
 * @author RT-Kapil
 */
@Controller
@RequestMapping(path = "/buyer")
public class CostCenterController {

	private static final Logger LOG = Logger.getLogger(Global.ADMIN_LOG);

	@Autowired
	CostCenterService costCenterService;

	@Autowired
	BuyerService buyerService;

	@Resource
	MessageSource messageSource;

	@ModelAttribute("statusList")
	public List<Status> getStatusList() {
		return Arrays.asList(Status.values());
	}

	@RequestMapping(path = "/costCenter", method = RequestMethod.GET)
	public ModelAndView createCostCenter(@ModelAttribute CostCenter costCenter, Model model) {
		model.addAttribute("Buyer", buyerService.findAllBuyers());
		model.addAttribute("btnValue", messageSource.getMessage("application.create", new Object[] {}, LocaleContextHolder.getLocale()));
		return new ModelAndView("costCenter", "costCenter", new CostCenter());
	}

	@RequestMapping(path = "/costCenter", method = RequestMethod.POST)
	public ModelAndView saveCostCenter(@ModelAttribute CostCenter costCenter, BindingResult result, Model model, RedirectAttributes redir) {
		List<String> errMessages = new ArrayList<String>();
		try {
			LOG.info("Cost Center 1");

			if (result.hasErrors()) {
				for (ObjectError err : result.getAllErrors()) {
					errMessages.add(err.getDefaultMessage());

				}
				LOG.info("error..");
				model.addAttribute("btnValue", messageSource.getMessage("application.create", new Object[] {}, LocaleContextHolder.getLocale()));
				model.addAttribute("errors", errMessages);
				model.addAttribute("Buyer", buyerService.findAllBuyers());
				return new ModelAndView("costCenter", "costCenter", new CostCenter());
			} else {

				if (doValidate(costCenter)) {
					LOG.info("Buyer In cost Center" + SecurityLibrary.getLoggedInUser().getBuyer());
					if (StringUtils.checkString(costCenter.getId()).length() == 0) {
						costCenter.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
						costCenter.setCreatedBy(SecurityLibrary.getLoggedInUser());
						costCenter.setCreatedDate(new Date());
						costCenterService.saveCostCenter(costCenter);
						redir.addFlashAttribute("success", messageSource.getMessage("costcenter.save.success", new Object[] { costCenter.getCostCenter() }, Global.LOCALE));
					} else {
						CostCenter persistObj = costCenterService.getCostCenterById(costCenter.getId());
						LOG.info("cost Center :  " + persistObj);
						persistObj.setCostCenter(costCenter.getCostCenter());
						persistObj.setDescription(costCenter.getDescription());
						persistObj.setStatus(costCenter.getStatus());
						persistObj.setModifiedBy(SecurityLibrary.getLoggedInUser());
						persistObj.setModifiedDate(new Date());
						costCenterService.updateCostCenter(persistObj);
						redir.addFlashAttribute("success", messageSource.getMessage("costcenter.update.success", new Object[] { costCenter.getCostCenter() }, Global.LOCALE));
						LOG.info("cost Center 2 :  " + persistObj);
					}
				} else {

					model.addAttribute("errors", messageSource.getMessage("costCenter.error.duplicate", new Object[] { costCenter.getCostCenter() }, Global.LOCALE));
					model.addAttribute("btnValue", messageSource.getMessage("application.create", new Object[] {}, LocaleContextHolder.getLocale()));
					model.addAttribute("Buyer", buyerService.findAllBuyers());
					return new ModelAndView("costCenter", "costCenter", costCenter);

				}
			}
		} catch (Exception e) {
			LOG.error("Error While saving the cost center" + e.getMessage(), e);
			model.addAttribute("errors", messageSource.getMessage("costCenter.error.save", new Object[] { e.getMessage() }, Global.LOCALE));
			model.addAttribute("Buyer", buyerService.findAllBuyers());
			return new ModelAndView("costCenter", "costCenter", new CostCenter());
		}

		return new ModelAndView("redirect:listCostCenter");

	}

	private boolean doValidate(CostCenter costCenter) {
		boolean validate = true;
		if (costCenterService.isExists(costCenter, SecurityLibrary.getLoggedInUserTenantId())) {
			LOG.info("inside validation");

			validate = false;
		}
		return validate;
	}

	@RequestMapping(path = "/listCostCenter", method = RequestMethod.GET)
	public String listCostCenter(Model model) {
		return "listCostCenter";
	}

	@RequestMapping(path = "/editCostCenter", method = RequestMethod.GET)
	public ModelAndView editCostCenter(@RequestParam String id, Model model) {
		LOG.info("Edit CostCenter Called  " + id);
		CostCenter costCenter = costCenterService.getCostCenterById(id);
		model.addAttribute("btnValue", messageSource.getMessage("application.update", new Object[] {}, LocaleContextHolder.getLocale()));
		return new ModelAndView("costCenter", "costCenter", costCenter);
	}

	@RequestMapping(path = "/deleteCostCenter", method = RequestMethod.GET)
	public String deleteCostCenter(@RequestParam String id, Model model) {
		LOG.info("Delete CostCenter Called");
		CostCenter costCenter = costCenterService.getCostCenterById(id);
		try {
			costCenter.setModifiedBy(SecurityLibrary.getLoggedInUser());
			costCenter.setModifiedDate(new Date());
			costCenterService.deleteCostCenter(costCenter);
			model.addAttribute("success", messageSource.getMessage("costcenter.success.delete", new Object[] { costCenter.getCostCenter() }, Global.LOCALE));
		} catch (Exception e) {
			LOG.error("Error while deleting Cost Center [ " + e.getMessage(), e);
			model.addAttribute("errors", messageSource.getMessage("costcenter.error.delete", new Object[] { costCenter.getCostCenter() }, Global.LOCALE));
		}
		return "listCostCenter";
	}

	@RequestMapping(path = "/costCenterData", method = RequestMethod.GET)
	public ResponseEntity<TableData<CostCenter>> costCenterData(TableDataInput input) {
		try {
			LOG.info("Start : " + input.getStart() + " Length : " + input.getLength() + " Sort : " + input.getSort());
			LOG.info(">>>>>>>>>>>>> Table Data Input : " + input.toString());
			TableData<CostCenter> data = new TableData<CostCenter>(costCenterService.findCostCentersForTenant(SecurityLibrary.getLoggedInUserTenantId(), input));
			data.setDraw(input.getDraw());
			long totalFilterCount = costCenterService.findTotalFilteredCostCentersForTenant(SecurityLibrary.getLoggedInUserTenantId(), input);
			data.setRecordsFiltered(totalFilterCount);
			long totalCount = costCenterService.findTotalCostCentersForTenant(SecurityLibrary.getLoggedInUserTenantId());
			data.setRecordsTotal(totalCount);
			return new ResponseEntity<TableData<CostCenter>>(data, HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Error fetching Cost Center list : " + e.getMessage(), e);
			HttpHeaders headers = new HttpHeaders();
			headers.add("error", "Error fetching Cost Center list : " + e.getMessage());
			return new ResponseEntity<TableData<CostCenter>>(null, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(path = "/costCenterTemplate", method = RequestMethod.GET)
	public void downloadCostCenterExcel(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			costCenterService.costCenterDownloadTemplate(response, SecurityLibrary.getLoggedInUserTenantId());
		} catch (Exception e) {
			LOG.error("Error while downloading cost center  template :: " + e.getMessage(), e);
		}
	}

	@RequestMapping(path = "/uploadCostCenter", method = RequestMethod.POST)
	public ResponseEntity<String> uploadCostCenterExcel(@RequestParam("file") MultipartFile file, HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpHeaders headers = new HttpHeaders();
		try {
			validateUploadCostCenter(file);
			LOG.info("uploadCostCenterExcel method called" + file.getOriginalFilename());
			costCenterService.costCenterUploadFile(file, SecurityLibrary.getLoggedInUserTenantId(), SecurityLibrary.getLoggedInUser());
			headers.add("success", "Cost center list uploaded successfully");
			return new ResponseEntity<String>(headers, HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Error while Uploading cost center  template :: " + e.getMessage(), e);
			headers.add("error", e.getMessage());
			return new ResponseEntity<String>(headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	public void validateUploadCostCenter(MultipartFile file) {
		LOG.info("++++++++++++file.getContentType()++++++++++++++" + file.getContentType());
		if (!file.getContentType().equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") && !file.getContentType().equals("application/wps-office.xlsx") && !file.getContentType().equals("application/vnd.ms-excel"))
			throw new MultipartException("Only excel files accepted!");
	}

}
