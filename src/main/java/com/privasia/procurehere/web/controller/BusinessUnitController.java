package com.privasia.procurehere.web.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.privasia.procurehere.core.entity.BusinessUnit;
import com.privasia.procurehere.core.enums.Status;
import com.privasia.procurehere.core.pojo.TableData;
import com.privasia.procurehere.core.pojo.TableDataInput;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.core.utils.SecurityLibrary;
import com.privasia.procurehere.core.utils.StringUtils;
import com.privasia.procurehere.service.BusinessUnitService;
import com.privasia.procurehere.service.EventIdSettingsService;
import com.privasia.procurehere.web.editors.BusinessUnitEditor;

/**
 * @author parveen
 */
@Controller
@RequestMapping(path = "/buyer")
public class BusinessUnitController {

	private static final Logger LOG = Logger.getLogger(Global.BUYER_LOG);

	@Autowired
	BusinessUnitService businessUnitService;

	@Resource
	MessageSource messageSource;

	@Autowired
	EventIdSettingsService eventIdSettingsService;
	
	@Autowired
	BusinessUnitEditor businessUnitEditor;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(BusinessUnit.class, businessUnitEditor);
		StringTrimmerEditor stringtrimmer = new StringTrimmerEditor(true);
		binder.registerCustomEditor(String.class, stringtrimmer);
	}

	@RequestMapping(path = "/businessUnit", method = RequestMethod.GET)
	public ModelAndView createBusinessUnit(Model model) {
		LOG.info("Business Unit GET called");
		model.addAttribute("btnValue", messageSource.getMessage("application.create", new Object[] {}, LocaleContextHolder.getLocale()));
		model.addAttribute("codeRequerd", eventIdSettingsService.isRequiredCode(SecurityLibrary.getLoggedInUserTenantId()));
		model.addAttribute("parentList", businessUnitService.getPlainActiveBusinessUnitParentsForTenant(SecurityLibrary.getLoggedInUserTenantId()));
		return new ModelAndView("businessUnitCreate", "businessUnit", new BusinessUnit());
	}
	
	@RequestMapping(path = "/businessUnit", method = RequestMethod.POST)
	public ModelAndView saveBusinessUnit(@ModelAttribute BusinessUnit businessUnit, @RequestParam(value = "logoImg", required = false) MultipartFile logoImg, @RequestParam(value = "removeFile") boolean removeFile , @RequestParam(value = "recursive") boolean recursive , BindingResult result, Model model, RedirectAttributes redir) {
		try {
			LOG.info(recursive +"Business Unit POST called Code :" + businessUnit.getUnitCode());
			if (!businessUnitService.isExists(businessUnit, SecurityLibrary.getLoggedInUserTenantId())) {

				if (businessUnitService.isExistsUnitCode(businessUnit.getUnitCode(), SecurityLibrary.getLoggedInUserTenantId(), businessUnit.getId())) {
					model.addAttribute("error", messageSource.getMessage("businessUnitCode.error.duplicate", new Object[] { businessUnit.getUnitCode() }, Global.LOCALE));
					LOG.info("duplicate unit Code");
					if (StringUtils.checkString(businessUnit.getId()).length() == 0) {
						model.addAttribute("btnValue", messageSource.getMessage("application.create", new Object[] {}, LocaleContextHolder.getLocale()));
					} else {
						model.addAttribute("btnValue", messageSource.getMessage("application.update", new Object[] {}, LocaleContextHolder.getLocale()));
					}
					return new ModelAndView("businessUnitCreate", "businessUnit", businessUnit);
				}

				String fileName = null;
				
				if (logoImg != null && !logoImg.isEmpty()) {
					
					fileName = logoImg.getOriginalFilename();

					if (!StringUtils.validateAttachmentFile(fileName, Arrays.asList("jpg","jpeg", "png", "bmp"))) {
						model.addAttribute("error", "Only " + String.join(",", Arrays.asList("jpg","jpeg", "png", "bmp")) + " files are allowed.");
						if (StringUtils.checkString(businessUnit.getId()).length() == 0) {
							model.addAttribute("btnValue", messageSource.getMessage("application.create", new Object[] {}, LocaleContextHolder.getLocale()));
						} else {
							model.addAttribute("btnValue", messageSource.getMessage("application.update", new Object[] {}, LocaleContextHolder.getLocale()));
						}
						model.addAttribute("codeRequerd", eventIdSettingsService.isRequiredCode(SecurityLibrary.getLoggedInUserTenantId()));
						model.addAttribute("parentList", businessUnitService.getPlainActiveBusinessUnitParentsForTenant(SecurityLibrary.getLoggedInUserTenantId()));
						return new ModelAndView("businessUnitCreate", "businessUnit", businessUnit);
					}
				}

				
				if (StringUtils.checkString(businessUnit.getId()).length() == 0) {
					if (logoImg != null && !logoImg.isEmpty()) {
						
						LOG.info("fileName :" + fileName);
						byte[] bytes = logoImg.getBytes();
						businessUnit.setContentType(logoImg.getContentType());
						businessUnit.setFileName(fileName);
						businessUnit.setFileAttatchment(bytes);
						businessUnit.setFileSizeKb(bytes.length > 0 ? bytes.length / 1024 : 0);
					}
					businessUnit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
					businessUnit.setCreatedBy(SecurityLibrary.getLoggedInUser());
					businessUnit.setCreatedDate(new Date());
					businessUnitService.save(businessUnit);
					redir.addFlashAttribute("success", messageSource.getMessage("businessUnit.save.success", new Object[] { businessUnit.getUnitName() }, Global.LOCALE));
					LOG.info("Business Unit saved successfully");
				} else {
					BusinessUnit persistObj = businessUnitService.getBusinessUnitById(businessUnit.getId());
					if (logoImg != null && !logoImg.isEmpty()) {
						LOG.info("fileName :" + fileName);
						byte[] bytes = logoImg.getBytes();
						persistObj.setContentType(logoImg.getContentType());
						persistObj.setFileName(fileName);
						persistObj.setFileAttatchment(bytes);
						persistObj.setFileSizeKb(bytes.length > 0 ? bytes.length / 1024 : 0);
					}
					if (removeFile) {
						persistObj.setContentType(null);
						persistObj.setFileName(null);
						persistObj.setFileAttatchment(null);
						persistObj.setFileSizeKb(null);
					}
					persistObj.setRecursive(recursive);
					persistObj.setUnitName(businessUnit.getUnitName());
					persistObj.setDisplayName(businessUnit.getDisplayName());
					persistObj.setUnitCode(businessUnit.getUnitCode());
					persistObj.setLine1(businessUnit.getLine1());
					persistObj.setLine2(businessUnit.getLine2());
					persistObj.setLine3(businessUnit.getLine3());
					persistObj.setLine4(businessUnit.getLine4());
					persistObj.setLine5(businessUnit.getLine5());
					persistObj.setLine6(businessUnit.getLine6());
					persistObj.setLine7(businessUnit.getLine7());
					persistObj.setStatus(businessUnit.getStatus());
					persistObj.setBudgetCheck(businessUnit.getBudgetCheck());
					persistObj.setParent(businessUnit.getParent()); // Parent business unit
					persistObj.setModifiedBy(SecurityLibrary.getLoggedInUser());
					persistObj.setModifiedDate(new Date());
					persistObj.setBudgetCheck(businessUnit.getBudgetCheck());
					businessUnitService.update(persistObj);
					redir.addFlashAttribute("success", messageSource.getMessage("businessUnit.update.success", new Object[] { businessUnit.getUnitName() }, Global.LOCALE));
					LOG.info("Business Unit updated successfully");
				}
			} else {
				model.addAttribute("error", messageSource.getMessage("businessUnit.error.duplicate", new Object[] { businessUnit.getUnitName() }, Global.LOCALE));
				if (StringUtils.checkString(businessUnit.getId()).length() == 0) {
					model.addAttribute("btnValue", messageSource.getMessage("application.create", new Object[] {}, LocaleContextHolder.getLocale()));
				} else {
					model.addAttribute("btnValue", messageSource.getMessage("application.update", new Object[] {}, LocaleContextHolder.getLocale()));
				}
				model.addAttribute("codeRequerd", eventIdSettingsService.isRequiredCode(SecurityLibrary.getLoggedInUserTenantId()));
				model.addAttribute("parentList", businessUnitService.getPlainActiveBusinessUnitParentsForTenant(SecurityLibrary.getLoggedInUserTenantId()));
				return new ModelAndView("businessUnitCreate", "businessUnit", businessUnit);
			}
		} catch (Exception e) {
			LOG.error("Error While saving the Business Unit" + e.getMessage(), e);
			model.addAttribute("error", messageSource.getMessage("businessUnit.error.save", new Object[] { e.getMessage() }, Global.LOCALE));
			if (StringUtils.checkString(businessUnit.getId()).length() == 0) {
				model.addAttribute("btnValue", messageSource.getMessage("application.create", new Object[] {}, LocaleContextHolder.getLocale()));
			} else {
				model.addAttribute("btnValue", messageSource.getMessage("application.update", new Object[] {}, LocaleContextHolder.getLocale()));
			}
			model.addAttribute("parentList", businessUnitService.getPlainActiveBusinessUnitParentsForTenant(SecurityLibrary.getLoggedInUserTenantId()));
			return new ModelAndView("businessUnitCreate", "businessUnit", businessUnit);
		}
		model.addAttribute("statusList", Arrays.asList(Status.values()));
		return new ModelAndView("redirect:listBusinessUnit");

	}

	@RequestMapping(path = "/listBusinessUnit", method = RequestMethod.GET)
	public String listBusinessUnit(Model model) {
		model.addAttribute("statusList", Arrays.asList(Status.values()));
		return "listBusinessUnit";
	}

	@RequestMapping(path = "/editBusinessUnit", method = RequestMethod.GET)
	public ModelAndView editBusinessUnit(@RequestParam String id, Model model) {
		LOG.info("Edit Business Unit Called " + id);
		BusinessUnit businessUnit = null;
		try {
			model.addAttribute("btnValue", messageSource.getMessage("application.update", new Object[] {}, LocaleContextHolder.getLocale()));
			businessUnit = businessUnitService.getBusinessUnitById(id);
			businessUnit.setBudgetCheckOld(businessUnit.getBudgetCheck());
			try {
				if (businessUnit.getFileAttatchment() != null) {
					byte[] encodeBase64 = Base64.encodeBase64(businessUnit.getFileAttatchment());
					String base64Encoded = new String(encodeBase64, "UTF-8");
					model.addAttribute("logoImg", base64Encoded);
				}
			} catch (Exception e) {
				LOG.error("Error while encoded logo :" + e.getMessage(), e);
			}
			
			List<BusinessUnit> parentList = businessUnitService.getPlainActiveBusinessUnitParentsForTenant(SecurityLibrary.getLoggedInUserTenantId());
			// Remove self of its present in Parent List
			parentList.remove(businessUnit);
			model.addAttribute("parentList", parentList);

		} catch (Exception e) {
			LOG.error("Error While edit the Business Unit" + e.getMessage(), e);
			model.addAttribute("error", messageSource.getMessage("businessUnit.error.save", new Object[] { e.getMessage() }, Global.LOCALE));
		}
		return new ModelAndView("businessUnitCreate", "businessUnit", businessUnit != null ? businessUnit : new BusinessUnit());
	}

	@RequestMapping(path = "/deleteBusinessUnit", method = RequestMethod.GET)
	public String deleteBusinessUnit(@RequestParam String id, Model model) {
		LOG.info("Delete Business Unit Called");
		try {
			BusinessUnit businessUnit = businessUnitService.getBusinessUnitById(id);
			if (businessUnit != null) {
				businessUnit.setModifiedBy(SecurityLibrary.getLoggedInUser());
				businessUnit.setModifiedDate(new Date());
				businessUnit.setCreatedBy(SecurityLibrary.getLoggedInUser());
				businessUnitService.delete(businessUnit);
				model.addAttribute("success", messageSource.getMessage("businessUnit.success.delete", new Object[] { businessUnit.getUnitName() }, Global.LOCALE));
			} else {
				model.addAttribute("statusList", Arrays.asList(Status.values()));
				return "redirect:listBusinessUnit";
			}
		} catch (Exception e) {
			LOG.error("Error while deletingbusiness unit [ " + e.getMessage(), e);
			model.addAttribute("error", messageSource.getMessage("businessUnit.error.delete", new Object[] {}, Global.LOCALE));
		}
		model.addAttribute("statusList", Arrays.asList(Status.values()));
		return "redirect:listBusinessUnit";
	}

	@RequestMapping(path = "/businessUnitData", method = RequestMethod.GET)
	public ResponseEntity<TableData<BusinessUnit>> businessUnitData(TableDataInput input) {
		try {
			LOG.info("Start : " + input.getStart() + " Length : " + input.getLength() + " Sort : " + input.getSort());
			// LOG.info(">>>>>>>>>>>>> Table Data Input : " + input.toString());
			TableData<BusinessUnit> data = new TableData<BusinessUnit>(businessUnitService.findBusinessUnitsForTenant(SecurityLibrary.getLoggedInUserTenantId(), input));
			data.setDraw(input.getDraw());
			long totalFilterCount = businessUnitService.findTotalFilteredBusinessUnitsForTenant(SecurityLibrary.getLoggedInUserTenantId(), input);
			data.setRecordsFiltered(totalFilterCount);
			long totalCount = businessUnitService.findTotalBusinessUnitsForTenant(SecurityLibrary.getLoggedInUserTenantId());
			data.setRecordsTotal(totalCount);
			return new ResponseEntity<TableData<BusinessUnit>>(data, HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Error fetching Business Unit list : " + e.getMessage(), e);
			HttpHeaders headers = new HttpHeaders();
			headers.add("error", "Error fetching Business Unit list : " + e.getMessage());
			return new ResponseEntity<TableData<BusinessUnit>>(null, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(path = "/businessUnitTemplate", method = RequestMethod.GET)
	public void downloadBusinessUnitListExcel(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			businessUnitService.businessUnitDownloadTemplate(response, SecurityLibrary.getLoggedInUserTenantId());
		} catch (Exception e) {
			LOG.error("Error while downloading business Unit Service  template :: " + e.getMessage(), e);
		}

	}

}
