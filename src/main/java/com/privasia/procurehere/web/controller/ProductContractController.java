package com.privasia.procurehere.web.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.privasia.procurehere.core.entity.BusinessUnit;
import com.privasia.procurehere.core.entity.BuyerSettings;
import com.privasia.procurehere.core.entity.FavouriteSupplier;
import com.privasia.procurehere.core.entity.OwnerSettings;
import com.privasia.procurehere.core.entity.ProductCategory;
import com.privasia.procurehere.core.entity.ProductContract;
import com.privasia.procurehere.core.entity.Uom;
import com.privasia.procurehere.core.enums.Status;
import com.privasia.procurehere.core.pojo.BusinessUnitPojo;
import com.privasia.procurehere.core.pojo.ProductContractItemsPojo;
import com.privasia.procurehere.core.pojo.ProductContractPojo;
import com.privasia.procurehere.core.pojo.SupplierPojo;
import com.privasia.procurehere.core.pojo.TableData;
import com.privasia.procurehere.core.pojo.TableDataInput;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.core.utils.SecurityLibrary;
import com.privasia.procurehere.service.BusinessUnitService;
import com.privasia.procurehere.service.BuyerSettingsService;
import com.privasia.procurehere.service.FavoriteSupplierService;
import com.privasia.procurehere.service.OwnerSettingsService;
import com.privasia.procurehere.service.PrService;
import com.privasia.procurehere.service.ProductCategoryMaintenanceService;
import com.privasia.procurehere.service.ProductContractItemsService;
import com.privasia.procurehere.service.ProductContractService;
import com.privasia.procurehere.service.ProductListMaintenanceService;
import com.privasia.procurehere.service.UomService;
import com.privasia.procurehere.web.editors.BusinessUnitEditor;
import com.privasia.procurehere.web.editors.FavouriteSupplierEditor;
import com.privasia.procurehere.web.editors.ProductCategoryMaintenanceEditor;
import com.privasia.procurehere.web.editors.UomEditor;

/**
 * @author pavan
 */

@Controller
@RequestMapping(path = "/buyer")
public class ProductContractController {

	private static final Logger LOG = Logger.getLogger(Global.ADMIN_LOG);

	@Autowired
	UomService uomService;

	@Autowired
	ProductCategoryMaintenanceService productCategoryMaintenanceService;

	@Autowired
	UomEditor uomEditor;

	@Autowired
	ProductCategoryMaintenanceEditor pcmEditor;

	@Autowired
	FavouriteSupplierEditor fsEditor;

	@Autowired
	BusinessUnitEditor buEditor;

	@Autowired
	ProductContractService productListService;

	@Autowired
	ProductContractItemsService productContractItemsService;

	@Autowired
	BuyerSettingsService buyerSettingsService;

	@Autowired
	FavoriteSupplierService favoriteSupplierService;

	@Autowired
	BusinessUnitService businessUnitService;

	@Resource
	MessageSource messageSource;

	@Autowired
	PrService prService;

	@Autowired
	OwnerSettingsService ownerSettingsService;

	@Autowired
	ProductListMaintenanceService productListMaintenanceService;

	@InitBinder
	public void initBinder(WebDataBinder binder, HttpSession session) {
		binder.registerCustomEditor(Uom.class, uomEditor);
		binder.registerCustomEditor(ProductCategory.class, pcmEditor);
		binder.registerCustomEditor(FavouriteSupplier.class, fsEditor);
		binder.registerCustomEditor(BusinessUnit.class, buEditor);

		StringTrimmerEditor stringtrimmer = new StringTrimmerEditor(true);
		binder.registerCustomEditor(String.class, stringtrimmer);

		TimeZone timeZone = TimeZone.getDefault();
		String strTimeZone = (String) session.getAttribute(Global.SESSION_TIME_ZONE_KEY);
		if (strTimeZone != null) {
			timeZone = TimeZone.getTimeZone(strTimeZone);
		}
		SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy");
		timeFormat.setTimeZone(timeZone);

		binder.registerCustomEditor(Date.class, "contractStartDate", new CustomDateEditor(timeFormat, true));
		binder.registerCustomEditor(Date.class, "contractEndDate", new CustomDateEditor(timeFormat, true));

	}

	@ModelAttribute("statusList")
	public List<Status> getStatusList() {
		return Arrays.asList(Status.values());
	}

	@ModelAttribute("ownerSettings")
	public OwnerSettings getOwnersettings() {
		return ownerSettingsService.getOwnersettings();
	}

	@RequestMapping(path = "/productContractList", method = RequestMethod.GET)
	public String productContractList(Model model) throws JsonProcessingException {
		return "productContractList";
	}

	/*
	 * productListMaintenanceData
	 */
	@RequestMapping(path = "/productContractListData", method = RequestMethod.GET)
	public ResponseEntity<TableData<ProductContractPojo>> productContractListData(TableDataInput input) {
		try {
			LOG.info("Start : " + input.getStart() + " Length : " + input.getLength() + " Sort : " + input.getSort());
			LOG.debug(">>>>>>>>>>>>> Table Data Input : " + input.toString());
			TableData<ProductContractPojo> data = new TableData<ProductContractPojo>(productListService.findProductContractListForTenant(SecurityLibrary.getLoggedInUserTenantId(), input));
			data.setDraw(input.getDraw());
			long totalFilterCount = productListService.findTotalFilteredProductListForTenant(SecurityLibrary.getLoggedInUserTenantId(), input);
			data.setRecordsFiltered(totalFilterCount);
			long totalCount = productListService.findTotalProductListForTenant(SecurityLibrary.getLoggedInUserTenantId());
			data.setRecordsTotal(totalCount);
			return new ResponseEntity<TableData<ProductContractPojo>>(data, HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Error fetching productList Contract list : " + e.getMessage(), e);
			HttpHeaders headers = new HttpHeaders();
			headers.add("error", "Error fetching productList Contract  list : " + e.getMessage());
			return new ResponseEntity<TableData<ProductContractPojo>>(null, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(path = "/productContractListItem", method = RequestMethod.GET)
	public ResponseEntity<TableData<ProductContractItemsPojo>> productContractListItem(@RequestParam String id, TableDataInput input) {
		try {
			LOG.info("Start : " + input.getStart() + " Length : " + input.getLength() + " Sort : " + input.getSort() + "id :  " + id);
			LOG.debug(">>>>>>>>>>>>> Table Data Input : " + input.toString());
			TableData<ProductContractItemsPojo> data = new TableData<ProductContractItemsPojo>(productContractItemsService.findProductContractItemListForTenant(SecurityLibrary.getLoggedInUserTenantId(), input, id));
			data.setDraw(input.getDraw());
			long totalFilterCount = productContractItemsService.findTotalFilteredProductItemListForTenant(SecurityLibrary.getLoggedInUserTenantId(), input, id);
			data.setRecordsFiltered(totalFilterCount);
			long totalCount = productContractItemsService.findTotalProductItemListForTenant(SecurityLibrary.getLoggedInUserTenantId(), id);
			data.setRecordsTotal(totalCount);

			return new ResponseEntity<TableData<ProductContractItemsPojo>>(data, HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Error fetching productList Item Contract list : " + e.getMessage(), e);
			HttpHeaders headers = new HttpHeaders();
			headers.add("error", "Error fetching productList Item    list : " + e.getMessage());
			return new ResponseEntity<TableData<ProductContractItemsPojo>>(null, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(path = "/productContractListEdit", method = RequestMethod.GET)
	public ModelAndView productContractListEdit(@RequestParam String id, @ModelAttribute("productListMaintenance") ProductContract productListMaintenance, ModelMap model, HttpSession session) {
		ProductContract productListObj = productListService.findProductContractById(id, SecurityLibrary.getLoggedInUserTenantId());
		LOG.info("contract Refrence number  : " + productListObj.getContractReferenceNumber());
		TimeZone timeZone = TimeZone.getDefault();
		String strTimeZone = (String) session.getAttribute(Global.SESSION_TIME_ZONE_KEY);
		if (strTimeZone != null) {
			timeZone = TimeZone.getTimeZone(strTimeZone);
		}
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
		formatter.setTimeZone(timeZone);

		BuyerSettings buyerSettings = buyerSettingsService.getBuyerSettingsByTenantId(SecurityLibrary.getLoggedInUser().getBuyer().getId());
		String decimal = buyerSettings.getDecimal();
		int z = Integer.parseInt(decimal);
		StringBuffer decimals = new StringBuffer(0);
		for (int i = 0; i < z; i++) {
			decimals.append(0);
		}
		model.addAttribute("decimals", decimals);
		model.addAttribute("decimals", decimals);

		model.addAttribute("data", productListObj);
		ProductContract productObj = productListService.getProductContractById(id);
		List<SupplierPojo> supplierListData =favoriteSupplierService.searchFavouriteSupplier(SecurityLibrary.getLoggedInUserTenantId(), null, null);
		List<SupplierPojo> supplierList = new ArrayList<SupplierPojo>();
		if (productListObj.getSupplier() != null) {
			supplierList.add(new SupplierPojo(productListObj.getSupplier().getId(), productObj.getSupplierName()));
		}
		for (SupplierPojo supplier : supplierListData) {
			boolean isSupplierExist = false;
			if (productListObj.getSupplier() != null && productListObj.getSupplier().getId().equals(supplier.getId())) {
				isSupplierExist = true;
			}
			if (!isSupplierExist) {
				supplierList.add(supplier);
			}
		}
		model.addAttribute("favSupp", supplierList);
		
		List<BusinessUnitPojo>businessUnitData=businessUnitService.fetchBusinessUnitByTenantId(SecurityLibrary.getLoggedInUserTenantId(), null);
		List<BusinessUnitPojo> businessUnitList = new ArrayList<BusinessUnitPojo>();
		if (productListObj.getBusinessUnit() != null) {
			businessUnitList.add(new BusinessUnitPojo(productListObj.getBusinessUnit().getId(), productListObj.getBusinessUnit().getDisplayName()));
		}
		for (BusinessUnitPojo unit : businessUnitData) {
			boolean isUnitExist = false;
			if (productListObj.getBusinessUnit() != null && productListObj.getBusinessUnit().getId().equals(unit.getId())) {
				isUnitExist = true;
			}
			if (!isUnitExist) {
				businessUnitList.add(unit);
			}
		}
		model.addAttribute("businessUnit", businessUnitList);
		// model.addAttribute("favSupp",
		// favoriteSupplierService.favoriteSuppliersOfBuyer(SecurityLibrary.getLoggedInUser().getBuyer().getId(), null,
		// null, null));
		// model.addAttribute("businessUnit",
		// businessUnitService.getPlainActiveBusinessUnitForTenant(SecurityLibrary.getLoggedInUserTenantId()));

		model.addAttribute("btnValue2", messageSource.getMessage("application.update", new Object[] {}, LocaleContextHolder.getLocale()));
		return new ModelAndView("productContractUpdate", "productListMaintenance", productListObj);
	}

	@RequestMapping(path = "/saveProductContract", method = RequestMethod.POST)
	public ModelAndView saveProductList(@ModelAttribute("productListMaintenance") ProductContract productContract, Model model, RedirectAttributes redir) {
		try {
			LOG.info("inside update " + productContract.getId());
			ProductContract productListObj = productListService.findProductContractById(productContract.getId(), SecurityLibrary.getLoggedInUserTenantId());
			productListObj.setStatus(productContract.getStatus());
			productListService.createProductContract(productListObj);
			redir.addFlashAttribute("success", messageSource.getMessage("productContract.update.success", new Object[] { productListObj.getContractReferenceNumber() }, Global.LOCALE));
			return new ModelAndView("redirect:productContractList");

		}

		catch (Exception e) {
			LOG.error("Error While updating the product Item " + e.getMessage(), e);
			model.addAttribute("error", messageSource.getMessage("product.error.save", new Object[] { e.getMessage() }, Global.LOCALE));
			return new ModelAndView("redirect:productContractListData");
		}

	}

	@PostMapping("/searchFavouriteuppliers")
	public @ResponseBody ResponseEntity<List<SupplierPojo>> searchFavouriteuppliers(@RequestParam("searchSupplier") String searchSupplier) {
		List<SupplierPojo> favouriteSupplierList = favoriteSupplierService.searchFavouriteSupplier(SecurityLibrary.getLoggedInUserTenantId(), searchSupplier, null);
		return new ResponseEntity<List<SupplierPojo>>(favouriteSupplierList, HttpStatus.OK);
	}

	@PostMapping("/searchBusinessUnit")
	public @ResponseBody ResponseEntity<List<BusinessUnitPojo>> searchBusinessUnit(@RequestParam("searchUnit") String searchUnit) {
		List<BusinessUnitPojo> businessUnitList = businessUnitService.fetchBusinessUnitByTenantId(SecurityLibrary.getLoggedInUserTenantId(), searchUnit);
		return new ResponseEntity<List<BusinessUnitPojo>>(businessUnitList, HttpStatus.OK);
	}

}
