/**
 * 
 */
package com.privasia.procurehere.web.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.privasia.procurehere.core.entity.BuyerEmailSettings;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.core.utils.SecurityLibrary;
import com.privasia.procurehere.core.utils.StringUtils;
import com.privasia.procurehere.service.BuyerEmailSettingsService;

/**
 * @author jayshree
 *
 */

@Controller
@RequestMapping(value = "/emailSettings")
public class BuyerEmailSettingsController {
	
	private static final Logger LOG = Logger.getLogger(Global.BUYER_LOG);
	
	@Autowired
	BuyerEmailSettingsService buyerEmailSettingsService;
	
	@RequestMapping(value = "/buyerEmailSettings", method = RequestMethod.GET)
	public String createBuyerEmailSettings(@ModelAttribute BuyerEmailSettings emailSettings, Model model, RedirectAttributes redirectAttributes) {
		BuyerEmailSettings dbEmailSettings = buyerEmailSettingsService.getEmailSettingByTenantId(SecurityLibrary.getLoggedInUserTenantId());
		LOG.info("Buyer Email Settings create cont called >>>>>>>>>> "+dbEmailSettings.getId());
		model.addAttribute("buyerEmailSettings", dbEmailSettings);
		return "buyerEmailSettings";
	}

	@RequestMapping(value = "/saveEmailSettings", method = RequestMethod.POST)
	public String saveBuyerEmailSettings(@ModelAttribute("buyerEmailSettings") BuyerEmailSettings buyerEmailSettings, @RequestParam String emailPassword, Model model, RedirectAttributes redirect) {
		LOG.info("Save Buyer Email Settings called >>>>>>>>>> "+buyerEmailSettings.getId());
		try {
			buyerEmailSettingsService.saveBuyerEmailSettings(SecurityLibrary.getLoggedInUserTenantId(), buyerEmailSettings, emailPassword, SecurityLibrary.getLoggedInUser());
			
			if(StringUtils.checkString(buyerEmailSettings.getId()).length() == 0 ) {
				redirect.addFlashAttribute("success", "Email Server Settings saved successfully");
			}else {
				redirect.addFlashAttribute("success", "Email Server Settings updated successfully");
			}
			return "redirect:buyerEmailSettings";
		} catch (Exception e) {
			LOG.info("Error while Saving settings >>>>>>>>>> "+e.getMessage(), e);
			redirect.addFlashAttribute("error", "Error is "+e.getMessage());
			return "redirect:buyerEmailSettings";
		}
	}

}
