package com.privasia.procurehere.web.controller;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.privasia.procurehere.core.entity.PaymentTransaction;
import com.privasia.procurehere.core.enums.TransactionStatus;
import com.privasia.procurehere.core.enums.TransactionType;
import com.privasia.procurehere.core.pojo.TableData;
import com.privasia.procurehere.core.pojo.TableDataInput;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.service.PaymentTransactionService;

/**
 * @author Nitin Otageri
 */
@Controller
@RequestMapping("/owner/paymentTransaction")
public class PaymentTransactionController {

	private static final Logger LOG = Logger.getLogger(Global.BUYER_LOG);

	@Autowired
	PaymentTransactionService paymentTransactionService;

	@Resource
	MessageSource messageSource;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	@ModelAttribute("transactionTypeList")
	public List<TransactionType> getTransactionTypeList() {
		return Arrays.asList(TransactionType.values());
	}

	@ModelAttribute("statusList")
	public List<TransactionStatus> getStatusList() {
		return Arrays.asList(TransactionStatus.values());
	}

	@RequestMapping(path = "/viewPaymentTransaction/{id}", method = RequestMethod.GET)
	public String viewPaymentTransaction(@PathVariable(name = "id") String id, Model model, RedirectAttributes redir) {
		LOG.info("Getting the PaymentTransaction : " + id);
		PaymentTransaction paymentTransaction = paymentTransactionService.getPaymentTransactionById(id);
		// Error condition. Send the user back to listing screen.
		if (paymentTransaction == null) {
			redir.addFlashAttribute("error", messageSource.getMessage("common.no.records.found", new Object[] { id }, Global.LOCALE));
			return "redirect:paymentTransactionList";
		}
		model.addAttribute("paymentTransaction", paymentTransaction);

		return "paymentTransaction";
	}

	@RequestMapping(path = "/paymentTransactionList", method = RequestMethod.GET)
	public String paymentTransactionList(Model model) {
		return "paymentTransactionList";
	}

	@RequestMapping(path = "/paymentTransactionData", method = RequestMethod.GET)
	public ResponseEntity<TableData<PaymentTransaction>> paymentTransactionData(TableDataInput input) {
		try {
			TableData<PaymentTransaction> data = new TableData<PaymentTransaction>(paymentTransactionService.findPaymentTransactions(input));
			long totalFilterCount = paymentTransactionService.findTotalFilteredPaymentTransactions(input);
			data.setRecordsFiltered(totalFilterCount);
			long totalCount = paymentTransactionService.findTotalPaymentTransactions();
			data.setRecordsTotal(totalCount);
			return new ResponseEntity<TableData<PaymentTransaction>>(data, HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Error fetching Payment Transaction list : " + e.getMessage(), e);
			HttpHeaders headers = new HttpHeaders();
			headers.add("error", "Error fetching Payment Transaction list : " + e.getMessage());
			return new ResponseEntity<TableData<PaymentTransaction>>(null, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
