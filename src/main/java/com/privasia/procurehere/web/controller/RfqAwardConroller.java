package com.privasia.procurehere.web.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.privasia.procurehere.core.dao.ErpSetupDao;
import com.privasia.procurehere.core.entity.BuyerSettings;
import com.privasia.procurehere.core.entity.ErpSetup;
import com.privasia.procurehere.core.entity.FavouriteSupplier;
import com.privasia.procurehere.core.entity.NotificationMessage;
import com.privasia.procurehere.core.entity.OwnerSettings;
import com.privasia.procurehere.core.entity.PrTemplate;
import com.privasia.procurehere.core.entity.ProductCategory;
import com.privasia.procurehere.core.entity.ProductItem;
import com.privasia.procurehere.core.entity.RfqEvent;
import com.privasia.procurehere.core.entity.RfqEventAudit;
import com.privasia.procurehere.core.entity.RfqEventAward;
import com.privasia.procurehere.core.entity.RfqEventAwardDetails;
import com.privasia.procurehere.core.entity.RfqEventBq;
import com.privasia.procurehere.core.entity.RfqSupplierBq;
import com.privasia.procurehere.core.entity.RfqSupplierBqItem;
import com.privasia.procurehere.core.entity.Supplier;
import com.privasia.procurehere.core.entity.User;
import com.privasia.procurehere.core.enums.AuditActionType;
import com.privasia.procurehere.core.enums.AwardCriteria;
import com.privasia.procurehere.core.enums.NotificationType;
import com.privasia.procurehere.core.enums.RfxTypes;
import com.privasia.procurehere.core.enums.Status;
import com.privasia.procurehere.core.exceptions.ApplicationException;
import com.privasia.procurehere.core.utils.CollectionUtil;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.core.utils.SecurityLibrary;
import com.privasia.procurehere.core.utils.StringUtils;
import com.privasia.procurehere.service.BuyerSettingsService;
import com.privasia.procurehere.service.DashboardNotificationService;
import com.privasia.procurehere.service.EventAuditService;
import com.privasia.procurehere.service.EventAwardAuditService;
import com.privasia.procurehere.service.FavoriteSupplierService;
import com.privasia.procurehere.service.NotificationService;
import com.privasia.procurehere.service.OwnerSettingsService;
import com.privasia.procurehere.service.PrTemplateService;
import com.privasia.procurehere.service.ProductCategoryMaintenanceService;
import com.privasia.procurehere.service.RfqAwardService;
import com.privasia.procurehere.service.RfqBqService;
import com.privasia.procurehere.service.RfqEventService;
import com.privasia.procurehere.service.RfqEventSupplierService;
import com.privasia.procurehere.service.RfqSupplierBqItemService;
import com.privasia.procurehere.service.UserService;

import freemarker.template.Configuration;

@Controller
@RequestMapping("/buyer/RFQ")
public class RfqAwardConroller {

	protected static final Logger LOG = Logger.getLogger(Global.BUYER_LOG);

	@Autowired
	RfqEventService rfqEventService;

	@Autowired
	RfqEventSupplierService rfqEventSupplierService;

	@Autowired
	RfqAwardService eventAwardService;

	@Autowired
	RfqBqService rfqBqService;

	@Autowired
	EventAwardAuditService eventAwardAuditService;

	@Autowired
	ErpSetupDao erpSetupDao;
	@Autowired
	BuyerSettingsService buyerSettingsService;

	@Value("${app.url}")
	String APP_URL;

	@Autowired
	NotificationService notificationService;

	@Autowired
	DashboardNotificationService dashboardNotificationService;

	@Autowired
	Configuration freemarkerConfiguration;

	@Autowired
	MessageSource messageSource;
	@Autowired
	ProductCategoryMaintenanceService productCategoryMaintenanceService;

	@Autowired
	PrTemplateService prTemplateService;

	@Autowired
	FavoriteSupplierService favoriteSupplierService;

	@Autowired
	UserService userService;

	@Autowired
	RfqSupplierBqItemService rfqSupplierBqItemService;

	@Autowired
	OwnerSettingsService ownerSettingsService;

	@Autowired
	EventAuditService eventAuditService;

	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {
		dataBinder.setAutoGrowCollectionLimit(2000); // this is for by default only 256 array of object allowed here

	}

	@ModelAttribute("productCategory")
	public List<ProductCategory> getAllProductCategory() {
		List<ProductCategory> productList = productCategoryMaintenanceService.getAllProductCategoryByTenantId(SecurityLibrary.getLoggedInUserTenantId());
		if (CollectionUtil.isNotEmpty(productList)) {
			return productList;
		} else {
			return null;
		}
	}

	@ModelAttribute("prTemplate")
	public List<PrTemplate> getAllPrTemplate() {
		List<PrTemplate> allPrTemplate = prTemplateService.findAllPrTemplatesForTenantAndUser(SecurityLibrary.getLoggedInUserTenantId());
		LOG.info("================" + allPrTemplate.size());
		if (CollectionUtil.isNotEmpty(allPrTemplate)) {
			return allPrTemplate;
		} else {
			return null;
		}
	}

	@ModelAttribute("prCreator")
	public List<User> getPrCreator() {
		List<User> prCreatorUser = userService.getPrCreatorUser(SecurityLibrary.getLoggedInUserTenantId());
		if (CollectionUtil.isNotEmpty(prCreatorUser)) {
			return prCreatorUser;
		} else {
			return null;
		}
	}

	@ModelAttribute("ownerSettings")
	public OwnerSettings getOwnersettings() {
		return ownerSettingsService.getOwnersettings();
	}

	@RequestMapping(path = { "/eventAward/{eventId}/{bqId}", "/eventAward/{eventId}" }, method = RequestMethod.GET)
	public ModelAndView getEventAward(@PathVariable("eventId") String eventId, @PathVariable(value = "bqId", required = false) String bqId, Model model) {
		LOG.info("Hi this is event award Controller eventId  :" + eventId + "=== bqId :" + bqId);
		try {

			if (eventId != null) {
				RfqEvent event = rfqEventService.getRfqEventByeventId(eventId);
				rfqEventService.updateEventAward(eventId);
				model.addAttribute("event", event);
				model.addAttribute("eventType", RfxTypes.RFQ);
				AwardCriteria[] list = new AwardCriteria[3];
				list[0] = AwardCriteria.LOWEST_ITEMIZED_PRICE;
				list[1] = AwardCriteria.LOWEST_TOTAL_PRICE;
				list[2] = AwardCriteria.MANUAL;
				model.addAttribute("awardCriteria", list);

				// model.addAttribute("awardCriteria", AwardCriteria.values());

				List<RfqEventBq> rfqbq = rfqBqService.getAllBqListByEventId(eventId);
				if (StringUtils.checkString(bqId).length() == 0) {
					bqId = rfqbq.get(0).getId();
				}
				List<Supplier> suppliers = rfqEventSupplierService.getEventQualifiedSuppliersForEvaluation(event.getId());
				List<RfqSupplierBq> suppBqArr = new ArrayList<RfqSupplierBq>();
				RfqSupplierBq supplierBq = new RfqSupplierBq();

				if (rfqbq != null) {
					for (RfqEventBq bq : rfqbq) {
						RfqSupplierBq rfqSupplierBq = rfqEventService.getSupplierBQOfLeastTotalPrice(eventId, bq.getId(), SecurityLibrary.getLoggedInUserTenantId());
						suppBqArr.add(rfqSupplierBq);
					}
				}
				// RfqEventAward rfqEventAward = eventAwardService.rfqEventAwardByEventIdandBqId(eventId, bqId);
				RfqEventAward rfqEventAward = eventAwardService.rfqEventAwardDetailsByEventIdandBqId(eventId, bqId);
				if (rfqEventAward == null) {
					LOG.info("Event Award Null");
					RfqEventAward awd = new RfqEventAward();
					awd.setAwardCriteria(AwardCriteria.LOWEST_TOTAL_PRICE);
					supplierBq = rfqEventService.getSupplierBQOfLeastTotalPrice(eventId, bqId, SecurityLibrary.getLoggedInUserTenantId());
					model.addAttribute("supplierBq", supplierBq);
					model.addAttribute("eventAward", awd);
				} else {
					LOG.info("Event Award not Null" + rfqEventAward.getId());
					if (AwardCriteria.LOWEST_TOTAL_PRICE == rfqEventAward.getAwardCriteria()) {
						supplierBq = rfqEventService.getSupplierBQOfLeastTotalPrice(eventId, bqId, SecurityLibrary.getLoggedInUserTenantId());
					} else if (AwardCriteria.LOWEST_ITEMIZED_PRICE == rfqEventAward.getAwardCriteria()) {
						supplierBq = rfqEventService.getSupplierBQOfLowestItemisedPrice(eventId, bqId, SecurityLibrary.getLoggedInUserTenantId());
					} else if (AwardCriteria.MANUAL == rfqEventAward.getAwardCriteria()) {
						if (CollectionUtil.isNotEmpty(suppliers)) {
							supplierBq = rfqEventService.getSupplierBQwithSupplierId(eventId, bqId, suppliers.get(0).getId(), SecurityLibrary.getLoggedInUserTenantId());
						} else {
							LOG.info("Qualified suppliers is empty");
						}
					}
					model.addAttribute("supplierBq", supplierBq);
					model.addAttribute("eventAward", rfqEventAward);
				}

				model.addAttribute("eventSuppliers", suppliers);
				model.addAttribute("suppBqId", bqId);
				model.addAttribute("supplierBqs", suppBqArr);
				model.addAttribute("awardAuditList", eventAwardAuditService.findAllAwardAuditForTenantIdAndRfqEventId(SecurityLibrary.getLoggedInUserTenantId(), eventId));
				ErpSetup erpSetup = erpSetupDao.getErpConfigBytenantId(SecurityLibrary.getLoggedInUserTenantId());
				model.addAttribute("erpSetup", erpSetup);
				model.addAttribute("changeCriteria", Boolean.FALSE);
			}
		} catch (Exception e) {
			LOG.info(e);
		}

		return new ModelAndView("eventAwardDetailForSAP");
	}

	@RequestMapping(path = { "/saveEventAward", "/saveEventAward/{transfer}" }, method = RequestMethod.POST)
	private String saveEventAward(@ModelAttribute RfqEventAward rfqEventAward, @PathVariable(value = "transfer", required = false) Boolean transfer, HttpSession session, Model model, RedirectAttributes redir) {
		try {
			ErpSetup erpSetup = erpSetupDao.getErpConfigBytenantId(SecurityLibrary.getLoggedInUserTenantId());
			Boolean trasnferFlag = false;
			Boolean concludeFlag = false;
			if (transfer != null && erpSetup != null) {
				if (Boolean.TRUE == erpSetup.getIsErpEnable()) {
					trasnferFlag = true;
				} else {
					concludeFlag = true;
				}
			} else if (transfer != null && erpSetup == null) {
				concludeFlag = true;
			}
			saveProductItem(rfqEventAward);
			LOG.info(rfqEventAward.getRfxEvent().getId());

			if (rfqEventAward.getRfxEvent().getId() != null) {
				RfqEventAward eventAward = eventAwardService.saveEventAward(rfqEventAward, session, SecurityLibrary.getLoggedInUser(), trasnferFlag, concludeFlag);
				if (eventAward != null) {
					try {
						RfqEventAudit audit = new RfqEventAudit();
						audit.setActionDate(new Date());
						audit.setActionBy(SecurityLibrary.getLoggedInUser());
						if (Boolean.TRUE == concludeFlag) {
							audit.setDescription("Event ' " + eventAward.getRfxEvent().getEventName() + "'  is awarded and concluded");
						} else {
							audit.setDescription("Event ' " + eventAward.getRfxEvent().getEventName() + "'  awarded");
						}
						audit.setEvent(eventAward.getRfxEvent());
						if (Boolean.TRUE == concludeFlag) {
							audit.setAction(AuditActionType.Conclude);
						} else {
							audit.setAction(AuditActionType.Award);
						}
						eventAuditService.save(audit);
					} catch (Exception e) {
						LOG.error(e.getMessage(), e);
					}
				}

				if (Boolean.TRUE == trasnferFlag) {
					return "redirect:/buyer/RFQ/transferAward/" + rfqEventAward.getRfxEvent().getId() + "/" + eventAward.getBq().getId() + "/" + eventAward.getId() + "/" + trasnferFlag;
				}
				return "redirect:/buyer/RFQ/eventAward/" + rfqEventAward.getRfxEvent().getId() + "/" + eventAward.getBq().getId();
			}
		} catch (Exception e) {
			redir.addFlashAttribute("error", e.getMessage());
			LOG.info(e.getMessage(), e);
			return "redirect:eventAward/" + rfqEventAward.getRfxEvent().getId();
		}
		return "redirect:eventAward/" + rfqEventAward.getRfxEvent().getId();
	}

	@RequestMapping(path = "/criteria/{eventType}", method = RequestMethod.POST)
	public ModelAndView criteria(@PathVariable("eventType") RfxTypes eventType, @RequestParam("eventId") String eventId, @RequestParam("bqId") String bqId, @RequestParam("awardCriteria") AwardCriteria criteria, Model model) {
		LOG.info(eventId);
		LOG.info(bqId);
		LOG.info(eventType);
		LOG.info(criteria);
		try {

			if (eventId != null) {
				RfqEvent event = rfqEventService.getRfqEventByeventId(eventId);
				model.addAttribute("event", event);
				model.addAttribute("eventType", eventType);
				AwardCriteria[] list = new AwardCriteria[3];
				list[0] = AwardCriteria.LOWEST_ITEMIZED_PRICE;
				list[1] = AwardCriteria.LOWEST_TOTAL_PRICE;
				list[2] = AwardCriteria.MANUAL;
				model.addAttribute("awardCriteria", list);

				// model.addAttribute("awardCriteria", AwardCriteria.values());
				List<Supplier> suppliers = rfqEventSupplierService.getEventQualifiedSuppliersForEvaluation(event.getId());

				List<RfqEventBq> rfqbq = rfqBqService.getAllBqListByEventId(eventId);
				List<RfqSupplierBq> suppBqArr = new ArrayList<RfqSupplierBq>();

				if (rfqbq != null) {
					for (RfqEventBq bq : rfqbq) {
						LOG.info(bq.getId());
						RfqSupplierBq rfqSupplierBq = rfqEventService.getSupplierBQOfLeastTotalPrice(eventId, bq.getId(), SecurityLibrary.getLoggedInUserTenantId());
						suppBqArr.add(rfqSupplierBq);
					}
				}

				RfqSupplierBq rfqSupplierBq = new RfqSupplierBq();

				// RfqEventAward rfqEventAward = eventAwardService.rfqEventAwardByEventIdandBqId(eventId, bqId);
				RfqEventAward rfqEventAward = eventAwardService.rfqEventAwardDetailsByEventIdandBqId(eventId, bqId);
				if (AwardCriteria.LOWEST_TOTAL_PRICE == criteria) {
					LOG.info(eventId + "   " + bqId);
					rfqSupplierBq = rfqEventService.getSupplierBQOfLeastTotalPrice(eventId, bqId, SecurityLibrary.getLoggedInUserTenantId());
				} else if (AwardCriteria.LOWEST_ITEMIZED_PRICE == criteria) {
					rfqSupplierBq = rfqEventService.getSupplierBQOfLowestItemisedPrice(eventId, bqId, SecurityLibrary.getLoggedInUserTenantId());
				} else if (AwardCriteria.MANUAL == criteria) {
					// rfqSupplierBq = rfqEventService.getSupplierBQOfLeastTotalPrice(eventId, bqId);
					if (CollectionUtil.isNotEmpty(suppliers)) {
						rfqSupplierBq = rfqEventService.getSupplierBQwithSupplierId(eventId, bqId, suppliers.get(0).getId(), SecurityLibrary.getLoggedInUserTenantId());
						if (rfqEventAward != null && rfqEventAward.getAwardCriteria() != criteria) {
							if (CollectionUtil.isNotEmpty(rfqEventAward.getRfxAwardDetails())) {
								for (RfqEventAwardDetails detail : rfqEventAward.getRfxAwardDetails()) {
									detail.setSupplier(suppliers.get(0));
								}
							}
						}
					} else {
						LOG.info("Qualified suppliers is empty");
					}
				}

				if (rfqEventAward == null) {
					LOG.info("Event Award Null");
					model.addAttribute("changeCriteria", Boolean.TRUE);
					RfqEventAward eventAward = new RfqEventAward();
					eventAward.setAwardCriteria(criteria);
					model.addAttribute("eventAward", eventAward);
				} else {
					model.addAttribute("changeCriteria", rfqEventAward.getAwardCriteria() != criteria);
					LOG.info("Event Award not Null..." + rfqEventAward.getAwardCriteria() + "----" + criteria);
					rfqEventAward.setAwardCriteria(criteria);
					model.addAttribute("eventAward", rfqEventAward);
				}
				model.addAttribute("eventSuppliers", suppliers);
				model.addAttribute("supplierBq", rfqSupplierBq);
				model.addAttribute("supplierBqs", suppBqArr);
				model.addAttribute("suppBqId", bqId);
				model.addAttribute("awardAuditList", eventAwardAuditService.findAllAwardAuditForTenantIdAndRfqEventId(SecurityLibrary.getLoggedInUserTenantId(), eventId));
				ErpSetup erpSetup = erpSetupDao.getErpConfigBytenantId(SecurityLibrary.getLoggedInUserTenantId());
				model.addAttribute("erpSetup", erpSetup);
			}
		} catch (Exception e) {
			LOG.info(e.getMessage(), e);
		}
		return new ModelAndView("eventAwardDetailForSAP");
	}

	@RequestMapping(path = "/getSupplierData/{eventType}", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<RfqSupplierBqItem> criteria(@PathVariable("eventType") RfxTypes eventType, @RequestParam("eventId") String eventId, @RequestParam("bqItemId") String bqItemId, @RequestParam("supplierId") String supplierId) {

		HttpHeaders headers = new HttpHeaders();
		RfqSupplierBqItem bqItem = null;
		try {
			bqItem = eventAwardService.getBqItemByBqItemId(bqItemId, supplierId, SecurityLibrary.getLoggedInUserTenantId());
		} catch (Exception e) {
			headers.add("error", "Cannot fetch supplier Item details : " + e.getMessage());
			return new ResponseEntity<RfqSupplierBqItem>(bqItem, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<RfqSupplierBqItem>(bqItem, headers, HttpStatus.OK);
	}

	@RequestMapping(path = "/transferAward/{eventId}/{bqId}/{rfqEventAwardId}/{transfer}", method = RequestMethod.GET)
	public ModelAndView transferAward(@PathVariable("eventId") String eventId, @PathVariable("bqId") String bqId, @PathVariable("rfqEventAwardId") String rfqEventAwardId, @PathVariable("transfer") Boolean transfer, RedirectAttributes redir, HttpSession session) {
		LOG.info("eventId :" + eventId);
		try {
			if (eventId != null) {
				eventAwardService.transferRfqAward(eventId, SecurityLibrary.getLoggedInUserTenantId(), session, SecurityLibrary.getLoggedInUser(), rfqEventAwardId, transfer, RfxTypes.RFQ);
				// RfqEvent event = rfqEventService.getEventById(eventId);
				// if (event != null && event.getAwardDate() == null) {
				// event.setAwardDate(new Date());
				// rfqEventService.updateEvent(event);
				// }

				redir.addFlashAttribute("suCategoryccess", "Successfully transferred");
			} else {
				LOG.info("Event ID is Null");
			}
		} catch (Exception e) {
			LOG.info("Error while transfer Event Award :" + e.getMessage(), e);
			redir.addFlashAttribute("error", messageSource.getMessage("flasherror.while.transfer.award", new Object[] { e.getMessage() }, Global.LOCALE));
			try {

				RfqEvent event = rfqEventService.getRfqEventByeventId(eventId);
				if (event != null) {
					sendErrorNotificationWhileCreating(SecurityLibrary.getLoggedInUser(), event.getReferanceNumber(), e.getMessage());
				}
			} catch (Exception err) {
				LOG.info("Error while sending email for transferAward", err.getMessage(), err);
			}
		}
		return new ModelAndView("redirect:/buyer/RFQ/eventAward/" + eventId + "/" + bqId);
	}

	@RequestMapping(value = "/downloadAwardSnapShot/{id}", method = RequestMethod.GET)
	public void downloadAwardSnapShot(@PathVariable String id, HttpServletResponse response) throws IOException {
		try {
			LOG.info("Award Audid Id  :: :: " + id + "::::::");
			eventAwardService.downloadAwardAuditSnapshot(id, response);
		} catch (Exception e) {
			LOG.error("Error while downloading Award Audit snapshot: " + e.getMessage(), e);
		}
	}

	@RequestMapping(value = "/downloadAwardExcelSnapShot/{id}", method = RequestMethod.GET)
	public void downloadAwardExcelSnapShot(@PathVariable String id, HttpServletResponse response) throws IOException {
		try {
			LOG.info("Award Audid Id  :: :: " + id + "::::::");
			eventAwardService.downloadAwardAuditExcelSnapShot(id, response);
		} catch (Exception e) {
			LOG.error("Error while downloading Award Audit snapshot: " + e.getMessage(), e);
		}

	}

	@RequestMapping(value = "/downloadAwardAttachFile/{id}", method = RequestMethod.GET)
	public void downloadAwardAttachFile(@PathVariable String id, HttpServletResponse response) throws IOException {
		try {
			LOG.info("Event Award  Id  :: :: " + id + "::::::");
			eventAwardService.downloadAwardAttachFileSnapShot(id, response);
		} catch (Exception e) {
			LOG.error("Error while downloading Award Audit snapshot: " + e.getMessage(), e);
		}
	}

	@RequestMapping(value = "/checkProductItemExistOrNot", method = RequestMethod.POST)
	public ResponseEntity<String> checkProductItemExistOrNot(HttpServletResponse response, RedirectAttributes redir, @RequestParam(required = false) String itemAndSupplierId) {
		try {
			LOG.info("itemVal " + itemAndSupplierId);
			String[] itemandSupplier = itemAndSupplierId.split("-");
			String itemId = itemandSupplier[0];
			LOG.info("*****itemId*******        " + itemId);
			String supplierId = itemandSupplier[1];
			LOG.info("******Supplier Id************    " + supplierId);
			RfqSupplierBqItem supplierBqItem = rfqSupplierBqItemService.getBqItemByRfqBqItemId(itemId, supplierId);
			if ((StringUtils.checkString(supplierBqItem.getItemName()).length() > 0) && (StringUtils.checkString(supplierId).length() > 0)) {
				ProductItem productItem = productCategoryMaintenanceService.checkProductItemExistOrNot(supplierBqItem.getItemName(), supplierId, SecurityLibrary.getLoggedInUserTenantId());
				if (productItem != null) {
					return new ResponseEntity<String>("", HttpStatus.OK);

				}
			}

		} catch (Exception e) {
			LOG.error("Error while checkng item already exist or not:  " + e.getMessage(), e);
		}
		return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);

	}

	private String getErpNotifiactionEmailsByBuyerSettings(String tenantId) {
		try {
			if (StringUtils.checkString(tenantId).length() > 0) {
				LOG.info("fetching buyer setting-------------------");
				BuyerSettings buyerSettings = buyerSettingsService.getBuyerSettingsByTenantId(tenantId);
				if (buyerSettings != null) {
					return buyerSettings.getErpNotificationEmails();
				}
			}
		} catch (Exception e) {
			LOG.error("Error while fetching buyer setting :" + e.getMessage(), e);
		}
		return null;
	}

	private String getTimeZoneByBuyerSettings(String tenantId, String timeZone) {
		try {
			if (StringUtils.checkString(tenantId).length() > 0) {
				String time = buyerSettingsService.getBuyerTimeZoneByTenantId(tenantId);
				if (time != null) {
					timeZone = time;
				}
			}
		} catch (Exception e) {
			LOG.error("Error while fetching buyer time zone :" + e.getMessage(), e);
		}
		return timeZone;
	}

	private void sendEmail(String mailTo, String subject, Map<String, Object> map, String template) {
		if (StringUtils.checkString(mailTo).length() > 0) {
			try {
				LOG.info("Sending request email to : " + mailTo);
				String message = FreeMarkerTemplateUtils.processTemplateIntoString(freemarkerConfiguration.getTemplate(template), map);
				notificationService.sendEmail(mailTo, subject, message);
			} catch (Exception e) {
				LOG.error("ERROR while Sending mail :" + e.getMessage(), e);
			}
		} else {
			LOG.warn("No communication email configured for user... Not going to send email notification");
		}
	}

	private void sendErrorNotificationWhileCreating(User user, String prNo, String error) {
		String mailTo = "";
		String subject = "Error ERP event added in ERP Event List";
		String url = APP_URL + "/buyer/erpManualList";
		HashMap<String, Object> map = new HashMap<String, Object>();
		try {

			mailTo = getErpNotifiactionEmailsByBuyerSettings(user.getTenantId());
			map.put("userName", "");
			map.put("prNo", prNo);
			map.put("error", error);
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
			String timeZone = "GMT+8:00";
			timeZone = getTimeZoneByBuyerSettings(user.getTenantId(), timeZone);
			df.setTimeZone(TimeZone.getTimeZone(timeZone));
			map.put("date", df.format(new Date()));
			map.put("appUrl", url);
			map.put("loginUrl", APP_URL + "/login");
			sendEmail(mailTo, subject, map, Global.ERP_EVENT_ERROR_TEMPLATE);
		} catch (Exception e) {
			LOG.error("Error While sending pending mail For adding Erp audit into manual list :" + e.getMessage(), e);
		}
		try {
			String notificationMessage = messageSource.getMessage("erp.added.notification.message", new Object[] { prNo }, Global.LOCALE);
			sendDashboardNotification(user, url, subject, notificationMessage);
		} catch (Exception e) {
			LOG.error("Error While sending notification For Event CREATION :" + e.getMessage(), e);
		}

	}

	private void sendDashboardNotification(User messageTo, String url, String subject, String notificationMessage) {
		NotificationMessage message = new NotificationMessage();
		message.setCreatedBy(null);
		message.setCreatedDate(new Date());
		message.setMessage(notificationMessage);
		message.setNotificationType(NotificationType.EVENT_MESSAGE);
		message.setMessageTo(messageTo);
		message.setSubject(subject);
		message.setTenantId(messageTo.getTenantId());
		message.setUrl(url);
		dashboardNotificationService.save(message);
	}

	@RequestMapping(path = "/transferEventAwardToPr/{templateId}/{userId}/{eventId}", method = RequestMethod.POST)
	public String transferEventAwardToPr(@ModelAttribute RfqEventAward rfqEventAward, @PathVariable String templateId, @PathVariable("eventId") String eventId, @PathVariable String userId, Model model) {
		LOG.info("Template Id:" + templateId);
		LOG.info("Pr Creater Id" + userId);

		try {

			saveProductItem(rfqEventAward);
			rfqEventService.updatePrPushDate(eventId);
			String msg = rfqEventService.createPrFromAward(rfqEventAward, templateId, userId, SecurityLibrary.getLoggedInUser());

			RfqEventAudit audit = new RfqEventAudit();
			RfqEvent event = new RfqEvent();
			event.setId(eventId);
			audit.setEvent(event);
			audit.setAction(AuditActionType.Create);
			audit.setActionBy(SecurityLibrary.getLoggedInUser());
			audit.setActionDate(new Date());
			String prId = msg.substring(msg.indexOf(":") + 1);
			audit.setDescription("Event is pushed to PR : " + (StringUtils.checkString(prId).length() > 0 ? prId : ""));
			eventAuditService.save(audit);

			model.addAttribute("success", messageSource.getMessage("rfa.success.pr.created", new Object[] { msg != null ? msg : "" }, Global.LOCALE));

		} catch (ApplicationException e) {
			LOG.error("Error generating Auto PRs during Event Award : " + e.getMessage(), e);
			model.addAttribute("error", messageSource.getMessage("rfa.award.error.generating", new Object[] { e.getMessage() }, Global.LOCALE));
		} catch (Exception e) {
			LOG.error("Error generating Auto PRs during Event Award : " + e.getMessage(), e);
			// model.addAttribute("error", "Error generating PR : " + e.getMessage());
			model.addAttribute("error", messageSource.getMessage("rfa.award.error.generating", new Object[] { e.getMessage() }, Global.LOCALE));
		}

		return "redirect:/buyer/RFQ/eventAward/" + eventId;
	}

	private void saveProductItem(RfqEventAward rfqEventAward) throws ApplicationException {
		if (CollectionUtil.isNotEmpty(rfqEventAward.getRfxAwardDetails())) {
			for (RfqEventAwardDetails rfxAward : rfqEventAward.getRfxAwardDetails()) {
				LOG.info("UOM----" + rfxAward.getBqItem().getUom() + "----" + rfxAward.getAwardedPrice());

				if (StringUtils.checkString(rfxAward.getSelectItem()).length() > 0) {
					String value = rfxAward.getSelectItem();
					String[] itemNameAndSupplierId = value.split("-");
					String itemId = itemNameAndSupplierId[0];
					LOG.info("****Item Name in Save****" + itemId);
					String supplierId = itemNameAndSupplierId[1];

					RfqSupplierBqItem supplierBqItem = rfqSupplierBqItemService.getBqItemByRfqBqItemId(itemId, supplierId);

					String itemName = supplierBqItem.getItemName();
					LOG.info("****Supplier Id in Save******" + supplierId);
					LOG.info("UOM----" + rfxAward.getBqItem().getUom() + "----" + rfxAward.getAwardedPrice());
					if (StringUtils.checkString(itemName).length() > 0 && StringUtils.checkString(supplierId).length() > 0) {
						ProductItem productItem = productCategoryMaintenanceService.checkProductItemExistOrNot(itemName, supplierId, SecurityLibrary.getLoggedInUserTenantId());

						if (productItem != null) {
							LOG.info("Item Name Update Price--------" + productItem.getProductName());
							LOG.info("****Supplier Id Update Price******" + rfxAward.getSupplier().getId());

							BigDecimal ap = rfxAward.getAwardedPrice();
							productItem.setUnitPrice(ap.divide(supplierBqItem.getQuantity(), 6, BigDecimal.ROUND_DOWN));

							// supplierBqItem.getQuantity();

							// productItem.setUnitPrice(rfxAward.getAwardedPrice());
							LOG.info("UNIT PRICE------" + productItem.getUnitPrice());
							productCategoryMaintenanceService.updateProductItemPrice(productItem);
						} else {
							productItem = new ProductItem();
							productItem.setProductName(itemName);
							LOG.info("Item Name New-----" + productItem.getProductName());

							if (StringUtils.checkString(rfxAward.getProductCode()).length() > 0) {
								productItem.setProductCode(rfxAward.getProductCode());
							} else {
								throw new ApplicationException("Please Select Product Code for: " + itemName);
							}

							if (supplierBqItem.getUom() != null) {
								productItem.setUom(supplierBqItem.getUom());
							}
							productItem.setTax(rfxAward.getTax());
							productItem.setCreatedBy(SecurityLibrary.getLoggedInUser());
							productItem.setCreatedDate(new Date());

							BigDecimal ap = rfxAward.getAwardedPrice();
							productItem.setUnitPrice(ap.divide(supplierBqItem.getQuantity(), 6, BigDecimal.ROUND_DOWN));

							productItem.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
							String productCategory = rfxAward.getProductCategory();
							if (StringUtils.checkString(productCategory).length() > 0) {
								LOG.info("Product Category-----------" + productCategory);
								ProductCategory category = new ProductCategory();
								category.setId(productCategory);
								productItem.setProductCategory(category);
							} else {
								throw new ApplicationException("Please Select Product Category for: " + itemName);
							}
							FavouriteSupplier favouriteSupplier = favoriteSupplierService.getFavouriteSupplierBySupplierId(supplierId, SecurityLibrary.getLoggedInUserTenantId());
							productItem.setFavoriteSupplier(favouriteSupplier);
							productItem.setStatus(Status.ACTIVE);
							productCategoryMaintenanceService.saveNewProductItem(productItem);
						}
					}
				}
			}
		}
	}
}
