package com.privasia.procurehere.web.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.privasia.procurehere.core.dao.ErpSetupDao;
import com.privasia.procurehere.core.entity.BuyerSettings;
import com.privasia.procurehere.core.entity.ErpSetup;
import com.privasia.procurehere.core.entity.FavouriteSupplier;
import com.privasia.procurehere.core.entity.NotificationMessage;
import com.privasia.procurehere.core.entity.OwnerSettings;
import com.privasia.procurehere.core.entity.PrTemplate;
import com.privasia.procurehere.core.entity.ProductCategory;
import com.privasia.procurehere.core.entity.ProductItem;
import com.privasia.procurehere.core.entity.RfaEvent;
import com.privasia.procurehere.core.entity.RfaEventAudit;
import com.privasia.procurehere.core.entity.RfaEventAward;
import com.privasia.procurehere.core.entity.RfaEventAwardDetails;
import com.privasia.procurehere.core.entity.RfaEventBq;
import com.privasia.procurehere.core.entity.RfaSupplierBq;
import com.privasia.procurehere.core.entity.RfaSupplierBqItem;
import com.privasia.procurehere.core.entity.Supplier;
import com.privasia.procurehere.core.entity.User;
import com.privasia.procurehere.core.enums.AuctionType;
import com.privasia.procurehere.core.enums.AuditActionType;
import com.privasia.procurehere.core.enums.AwardCriteria;
import com.privasia.procurehere.core.enums.NotificationType;
import com.privasia.procurehere.core.enums.RfxTypes;
import com.privasia.procurehere.core.enums.Status;
import com.privasia.procurehere.core.exceptions.ApplicationException;
import com.privasia.procurehere.core.utils.CollectionUtil;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.core.utils.SecurityLibrary;
import com.privasia.procurehere.core.utils.StringUtils;
import com.privasia.procurehere.service.BuyerSettingsService;
import com.privasia.procurehere.service.DashboardNotificationService;
import com.privasia.procurehere.service.EventAuditService;
import com.privasia.procurehere.service.EventAwardAuditService;
import com.privasia.procurehere.service.FavoriteSupplierService;
import com.privasia.procurehere.service.NotificationService;
import com.privasia.procurehere.service.OwnerSettingsService;
import com.privasia.procurehere.service.PrTemplateService;
import com.privasia.procurehere.service.ProductCategoryMaintenanceService;
import com.privasia.procurehere.service.RfaAwardService;
import com.privasia.procurehere.service.RfaBqService;
import com.privasia.procurehere.service.RfaEventService;
import com.privasia.procurehere.service.RfaEventSupplierService;
import com.privasia.procurehere.service.RfaSupplierBqItemService;
import com.privasia.procurehere.service.UserService;

import freemarker.template.Configuration;

@Controller
@RequestMapping("/buyer/RFA")
public class RfaAwardConroller {

	protected static final Logger LOG = Logger.getLogger(Global.BUYER_LOG);

	// private RfxTypes eventType;

	@Autowired
	RfaEventService rfaEventService;

	@Autowired
	RfaEventSupplierService rfaEventSupplierService;

	@Autowired
	RfaAwardService eventAwardService;

	@Autowired
	RfaBqService rfaBqService;

	@Autowired
	EventAwardAuditService eventAwardAuditService;

	@Autowired
	ErpSetupDao erpSetupDao;

	@Autowired
	BuyerSettingsService buyerSettingsService;

	@Value("${app.url}")
	String APP_URL;

	@Autowired
	NotificationService notificationService;

	@Autowired
	DashboardNotificationService dashboardNotificationService;

	@Autowired
	Configuration freemarkerConfiguration;

	@Autowired
	MessageSource messageSource;

	@Autowired
	ProductCategoryMaintenanceService productCategoryMaintenanceService;

	@Autowired
	PrTemplateService prTemplateService;

	@Autowired
	FavoriteSupplierService favoriteSupplierService;

	@Autowired
	UserService userService;

	@Autowired
	RfaSupplierBqItemService rfaSupplierBqItemService;

	@Autowired
	OwnerSettingsService ownerSettingsService;

	@Autowired
	EventAuditService eventAuditService;

	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {
		dataBinder.setAutoGrowCollectionLimit(2000); // this is for by default only 256 array of object allowed here

	}

	@ModelAttribute("productCategory")
	public List<ProductCategory> getAllProductCategory() {
		List<ProductCategory> productList = productCategoryMaintenanceService.getAllProductCategoryByTenantId(SecurityLibrary.getLoggedInUserTenantId());
		if (CollectionUtil.isNotEmpty(productList)) {
			return productList;
		} else {
			return null;
		}
	}

	@ModelAttribute("prTemplate")
	public List<PrTemplate> getAllPrTemplate() {
		List<PrTemplate> allPrTemplate = prTemplateService.findAllPrTemplatesForTenantAndUser(SecurityLibrary.getLoggedInUserTenantId());
		LOG.info("================" + allPrTemplate.size());
		if (CollectionUtil.isNotEmpty(allPrTemplate)) {
			return allPrTemplate;
		} else {
			return null;
		}
	}

	@ModelAttribute("prCreator")
	public List<User> getPrCreator() {
		List<User> prCreatorUser = userService.getPrCreatorUser(SecurityLibrary.getLoggedInUserTenantId());
		if (CollectionUtil.isNotEmpty(prCreatorUser)) {
			return prCreatorUser;
		} else {
			return null;
		}
	}

	@ModelAttribute("ownerSettings")
	public OwnerSettings getOwnersettings() {
		return ownerSettingsService.getOwnersettings();
	}

	@RequestMapping(path = { "/eventAward/{eventId}/{bqId}", "/eventAward/{eventId}" }, method = RequestMethod.GET)
	public ModelAndView getEventAward(@PathVariable("eventId") String eventId, @PathVariable(value = "bqId", required = false) String bqId, Model model) {
		LOG.info("Hi this is event award Controller eventId  :" + eventId + "=== bqId :" + bqId);
		try {

			if (eventId != null) {
				RfaEvent event = rfaEventService.getRfaEventById(eventId);
				rfaEventService.updateEventAward(eventId);
				model.addAttribute("event", event);
				model.addAttribute("eventType", RfxTypes.RFA);

				if (event.getAuctionType() == AuctionType.FORWARD_ENGISH || event.getAuctionType() == AuctionType.FORWARD_SEALED_BID || event.getAuctionType() == AuctionType.FORWARD_DUTCH) {
					AwardCriteria[] list = new AwardCriteria[3];
					list[0] = AwardCriteria.HIGHEST_TOTAL_PRICE;
					list[1] = AwardCriteria.HIGHEST_ITEMIZED_PRICE;
					list[2] = AwardCriteria.MANUAL;
					model.addAttribute("awardCriteria", list);
				} else {
					AwardCriteria[] list = new AwardCriteria[3];
					list[0] = AwardCriteria.LOWEST_TOTAL_PRICE;
					list[1] = AwardCriteria.LOWEST_ITEMIZED_PRICE;
					list[2] = AwardCriteria.MANUAL;
					model.addAttribute("awardCriteria", list);
				}

				List<RfaEventBq> rfabq = rfaBqService.getAllBqListByEventId(eventId);
				if (StringUtils.checkString(bqId).length() == 0) {
					bqId = rfabq.get(0).getId();
				}
				List<Supplier> suppliers = rfaEventSupplierService.getEventQualifiedSuppliersForEvaluation(event.getId());
				List<RfaSupplierBq> suppBqArr = new ArrayList<RfaSupplierBq>();
				RfaSupplierBq supplierBq = new RfaSupplierBq();

				if (rfabq != null) {
					for (RfaEventBq bq : rfabq) {
						RfaSupplierBq rfaSupplierBq = rfaEventService.getSupplierBQOfLeastTotalPrice(eventId, bq.getId(), SecurityLibrary.getLoggedInUserTenantId());
						suppBqArr.add(rfaSupplierBq);
					}
				}
				// RfaEventAward rfaEventAward = eventAwardService.rfaEventAwardByEventIdandBqId(eventId, bqId);
				//PH-1304
				RfaEventAward rfaEventAward = eventAwardService.rfaEventAwardDetailsByEventIdandBqId(eventId, bqId);

				if (rfaEventAward == null) {
					if (event.getAuctionType() == AuctionType.FORWARD_ENGISH || event.getAuctionType() == AuctionType.FORWARD_SEALED_BID || event.getAuctionType() == AuctionType.FORWARD_DUTCH) {
						LOG.info("Event Award Null");
						RfaEventAward awd = new RfaEventAward();
						awd.setAwardCriteria(AwardCriteria.HIGHEST_TOTAL_PRICE);
						supplierBq = rfaEventService.getSupplierBQOfHighestTotalPrice(eventId, bqId, SecurityLibrary.getLoggedInUserTenantId());
						model.addAttribute("supplierBq", supplierBq);
						model.addAttribute("eventAward", awd);
					} else {
						LOG.info("Event Award Null");
						RfaEventAward awd = new RfaEventAward();
						awd.setAwardCriteria(AwardCriteria.LOWEST_TOTAL_PRICE);
						supplierBq = rfaEventService.getSupplierBQOfLeastTotalPrice(eventId, bqId, SecurityLibrary.getLoggedInUserTenantId());
						model.addAttribute("supplierBq", supplierBq);
						model.addAttribute("eventAward", awd);
					}

				} else {
					LOG.info("Event Award not Null" + rfaEventAward.getId());
					if (AwardCriteria.LOWEST_TOTAL_PRICE == rfaEventAward.getAwardCriteria()) {
						supplierBq = rfaEventService.getSupplierBQOfLeastTotalPrice(eventId, bqId, SecurityLibrary.getLoggedInUserTenantId());
					} else if (AwardCriteria.LOWEST_ITEMIZED_PRICE == rfaEventAward.getAwardCriteria()) {
						supplierBq = rfaEventService.getSupplierBQOfLowestItemisedPrice(eventId, bqId, SecurityLibrary.getLoggedInUserTenantId());
					} else if (AwardCriteria.HIGHEST_TOTAL_PRICE == rfaEventAward.getAwardCriteria()) {
						supplierBq = rfaEventService.getSupplierBQOfHighestTotalPrice(eventId, bqId, SecurityLibrary.getLoggedInUserTenantId());
					} else if (AwardCriteria.HIGHEST_ITEMIZED_PRICE == rfaEventAward.getAwardCriteria()) {
						supplierBq = rfaEventService.getSupplierBQOfHighestItemisedPrice(eventId, bqId, SecurityLibrary.getLoggedInUserTenantId());
					} else if (AwardCriteria.MANUAL == rfaEventAward.getAwardCriteria()) {
						// supplierBq = rfaEventService.getSupplierBQOfLeastTotalPrice(eventId, bqId);
						if (CollectionUtil.isNotEmpty(suppliers)) {
							supplierBq = rfaEventService.getSupplierBQwithSupplierId(eventId, bqId, suppliers.get(0).getId(), SecurityLibrary.getLoggedInUserTenantId());
						} else {
							LOG.info("Qualified suppliers is empty");
						}
					}
					model.addAttribute("supplierBq", supplierBq);
					model.addAttribute("eventAward", rfaEventAward);
				}

				model.addAttribute("eventSuppliers", suppliers);
				model.addAttribute("suppBqId", bqId);
				model.addAttribute("supplierBqs", suppBqArr);
				model.addAttribute("awardAuditList", eventAwardAuditService.findAllAwardAuditForTenantIdAndRfaEventId(SecurityLibrary.getLoggedInUserTenantId(), eventId));
				ErpSetup erpSetup = erpSetupDao.getErpConfigBytenantId(SecurityLibrary.getLoggedInUserTenantId());
				model.addAttribute("erpSetup", erpSetup);
				model.addAttribute("changeCriteria", Boolean.FALSE);
			}
		} catch (Exception e) {
			LOG.info("error while geting award " + e.getMessage(), e);
		}

		return new ModelAndView("eventAwardDetailForSAP");
	}

	@RequestMapping(path = { "/saveEventAward", "/saveEventAward/{transfer}" }, method = RequestMethod.POST)
	private String saveEventAward(@ModelAttribute RfaEventAward rfaEventAward, @PathVariable(value = "transfer", required = false) Boolean transfer, Model model, HttpSession session, RedirectAttributes redir) {
		try {
			ErpSetup erpSetup = erpSetupDao.getErpConfigBytenantId(SecurityLibrary.getLoggedInUserTenantId());
			Boolean trasnferFlag = false;
			Boolean concludeFlag = false;
			if (transfer != null && erpSetup != null) {
				if (Boolean.TRUE == erpSetup.getIsErpEnable()) {
					trasnferFlag = true;
				} else {
					concludeFlag = true;
				}
			} else if (transfer != null && erpSetup == null) {
				concludeFlag = true;
			}
			saveProductItem(rfaEventAward);
			LOG.info(rfaEventAward.getRfxEvent().getId());
			if (rfaEventAward.getRfxEvent().getId() != null) {
				LOG.info(rfaEventAward.getRfxEvent().getId());
				RfaEventAward eventAward = eventAwardService.saveEventAward(rfaEventAward, session, SecurityLibrary.getLoggedInUser(), trasnferFlag, concludeFlag);
				if (eventAward != null) {
					try {
						RfaEventAudit audit = new RfaEventAudit();
						audit.setActionDate(new Date());
						audit.setActionBy(SecurityLibrary.getLoggedInUser());
						if (Boolean.TRUE == concludeFlag) {
							audit.setDescription("Event ' " + eventAward.getRfxEvent().getEventName() + "'  is awarded and concluded");
						} else {
							audit.setDescription("Event ' " + eventAward.getRfxEvent().getEventName() + "'  awarded");
						}
						audit.setEvent(eventAward.getRfxEvent());
						if (Boolean.TRUE == concludeFlag) {
							audit.setAction(AuditActionType.Conclude);
						} else {
							audit.setAction(AuditActionType.Award);
						}
						eventAuditService.save(audit);
					} catch (Exception e) {
						LOG.error(e.getMessage(), e);
					}
				}
				if (Boolean.TRUE == trasnferFlag) {
					return "redirect:/buyer/RFA/transferAward/" + rfaEventAward.getRfxEvent().getId() + "/" + eventAward.getBq().getId() + "/" + eventAward.getId() + "/" + trasnferFlag;
				}
				return "redirect:/buyer/RFA/eventAward/" + rfaEventAward.getRfxEvent().getId() + "/" + eventAward.getBq().getId();
			}
		} catch (Exception e) {
			redir.addFlashAttribute("error", e.getMessage());
			LOG.info(e.getMessage(), e);
			return "redirect:eventAward/" + rfaEventAward.getRfxEvent().getId();
		}
		return "redirect:eventAward/" + rfaEventAward.getRfxEvent().getId();
	}

	@RequestMapping(path = "/criteria/{eventType}", method = RequestMethod.POST)
	public ModelAndView criteria(@PathVariable("eventType") RfxTypes eventType, @RequestParam("eventId") String eventId, @RequestParam("bqId") String bqId, @RequestParam("awardCriteria") AwardCriteria criteria, Model model) {
		LOG.info(eventId);
		LOG.info(bqId);
		LOG.info(eventType);
		LOG.info(criteria);
		try {

			if (eventId != null) {
				RfaEvent event = rfaEventService.getRfaEventById(eventId);
				model.addAttribute("event", event);
				model.addAttribute("eventType", eventType);
				if (event.getAuctionType() == AuctionType.FORWARD_ENGISH || event.getAuctionType() == AuctionType.FORWARD_SEALED_BID || event.getAuctionType() == AuctionType.FORWARD_DUTCH) {
					AwardCriteria[] list = new AwardCriteria[3];
					list[0] = AwardCriteria.HIGHEST_TOTAL_PRICE;
					list[1] = AwardCriteria.HIGHEST_ITEMIZED_PRICE;
					list[2] = AwardCriteria.MANUAL;
					model.addAttribute("awardCriteria", list);
				} else {
					AwardCriteria[] list = new AwardCriteria[3];
					list[0] = AwardCriteria.LOWEST_TOTAL_PRICE;
					list[1] = AwardCriteria.LOWEST_ITEMIZED_PRICE;
					list[2] = AwardCriteria.MANUAL;
					model.addAttribute("awardCriteria", list);
				}
				List<Supplier> suppliers = rfaEventSupplierService.getEventQualifiedSuppliersForEvaluation(event.getId());
				List<RfaEventBq> rfabq = rfaBqService.getAllBqListByEventId(eventId);
				List<RfaSupplierBq> suppBqArr = new ArrayList<RfaSupplierBq>();

				if (rfabq != null) {
					for (RfaEventBq bq : rfabq) {
						LOG.info(bq.getId());
						RfaSupplierBq rfaSupplierBq = rfaEventService.getSupplierBQOfLeastTotalPrice(eventId, bq.getId(), SecurityLibrary.getLoggedInUserTenantId());
						suppBqArr.add(rfaSupplierBq);
					}
				}

				RfaSupplierBq rfaSupplierBq = new RfaSupplierBq();

				//RfaEventAward rfaEventAward = eventAwardService.rfaEventAwardByEventIdandBqId(eventId, bqId);
				RfaEventAward rfaEventAward = eventAwardService.rfaEventAwardDetailsByEventIdandBqId(eventId, bqId);
				if (AwardCriteria.LOWEST_TOTAL_PRICE == criteria) {
					LOG.info(eventId + "   " + bqId);
					rfaSupplierBq = rfaEventService.getSupplierBQOfLeastTotalPrice(eventId, bqId, SecurityLibrary.getLoggedInUserTenantId());
				} else if (AwardCriteria.LOWEST_ITEMIZED_PRICE == criteria) {
					LOG.info("______________________________________________________");
					rfaSupplierBq = rfaEventService.getSupplierBQOfLowestItemisedPrice(eventId, bqId, SecurityLibrary.getLoggedInUserTenantId());
				} else if (AwardCriteria.HIGHEST_TOTAL_PRICE == criteria) {
					LOG.info(eventId + "   " + bqId);
					rfaSupplierBq = rfaEventService.getSupplierBQOfHighestTotalPrice(eventId, bqId, SecurityLibrary.getLoggedInUserTenantId());
				} else if (AwardCriteria.HIGHEST_ITEMIZED_PRICE == criteria) {
					LOG.info("______________________________________________________");
					rfaSupplierBq = rfaEventService.getSupplierBQOfHighestItemisedPrice(eventId, bqId, SecurityLibrary.getLoggedInUserTenantId());
				} else if (AwardCriteria.MANUAL == criteria) {
					if (CollectionUtil.isNotEmpty(suppliers)) {
						rfaSupplierBq = rfaEventService.getSupplierBQwithSupplierId(eventId, bqId, suppliers.get(0).getId(), SecurityLibrary.getLoggedInUserTenantId());
						if (rfaEventAward != null && rfaEventAward.getAwardCriteria() != criteria) {
							if (CollectionUtil.isNotEmpty(rfaEventAward.getRfxAwardDetails())) {
								for (RfaEventAwardDetails detail : rfaEventAward.getRfxAwardDetails()) {
									detail.setSupplier(suppliers.get(0));
								}
							}
						}
					} else {
						LOG.info("Qualified suppliers is empty");
					}
				}

				if (rfaEventAward == null) {
					LOG.info("Event Award Null");
					RfaEventAward eventAward = new RfaEventAward();
					eventAward.setAwardCriteria(criteria);
					model.addAttribute("eventAward", eventAward);
					model.addAttribute("changeCriteria", Boolean.TRUE);
				} else {
					LOG.info("Event Award not Null..." + rfaEventAward.getId());
					model.addAttribute("changeCriteria", rfaEventAward.getAwardCriteria() != criteria);
					rfaEventAward.setAwardCriteria(criteria);
					model.addAttribute("eventAward", rfaEventAward);
				}
				model.addAttribute("eventSuppliers", suppliers);
				model.addAttribute("supplierBq", rfaSupplierBq);
				model.addAttribute("supplierBqs", suppBqArr);
				model.addAttribute("suppBqId", bqId);
				model.addAttribute("awardAuditList", eventAwardAuditService.findAllAwardAuditForTenantIdAndRfaEventId(SecurityLibrary.getLoggedInUserTenantId(), eventId));
				ErpSetup erpSetup = erpSetupDao.getErpConfigBytenantId(SecurityLibrary.getLoggedInUserTenantId());
				model.addAttribute("erpSetup", erpSetup);
			}
		} catch (Exception e) {
			LOG.info(e.getMessage(), e);
		}
		return new ModelAndView("eventAwardDetailForSAP");
	}

	@RequestMapping(path = "/getSupplierData/{eventType}", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<RfaSupplierBqItem> criteria(@PathVariable("eventType") RfxTypes eventType, @RequestParam("eventId") String eventId, @RequestParam("bqItemId") String bqItemId, @RequestParam("supplierId") String supplierId) {

		HttpHeaders headers = new HttpHeaders();
		RfaSupplierBqItem bqItem = null;
		try {

			bqItem = eventAwardService.getBqItemByBqItemId(bqItemId, supplierId, SecurityLibrary.getLoggedInUserTenantId());
		} catch (Exception e) {
			LOG.error("Error : " + e.getMessage(), e);
			headers.add("error", "Cannot fetch supplier Item details : " + e.getMessage());
			return new ResponseEntity<RfaSupplierBqItem>(bqItem, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<RfaSupplierBqItem>(bqItem, headers, HttpStatus.OK);
	}

	@RequestMapping(path = "/transferAward/{eventId}/{bqId}/{rfaEventAwardId}/{transfer}", method = RequestMethod.GET)
	public ModelAndView transferAward(@PathVariable("eventId") String eventId, @PathVariable("bqId") String bqId, @PathVariable("rfaEventAwardId") String rfaEventAwardId, @PathVariable("transfer") Boolean transfer, RedirectAttributes redir, HttpSession session) {
		LOG.info("eventId :" + eventId);
		try {
			if (eventId != null) {
				eventAwardService.transferRfaAward(eventId, SecurityLibrary.getLoggedInUserTenantId(), rfaEventAwardId, session, SecurityLibrary.getLoggedInUser(), transfer, RfxTypes.RFA);
				// RfaEvent event = rfaEventService.getRfaEventById(eventId);
				// if (event != null && event.getAwardDate() == null) {
				// event.setAwardDate(new Date());
				// rfaEventService.updateRfaEvent(event);
				// }
				redir.addFlashAttribute("success", messageSource.getMessage("flashsuccess.successfully.transfered", new Object[] {}, Global.LOCALE));
			} else {
				LOG.info("Event ID is Null");
			}
		} catch (Exception e) {
			LOG.info("Error while transfer Event Award :" + e.getMessage(), e);
			redir.addFlashAttribute("error", messageSource.getMessage("flasherror.while.transfer.award", new Object[] { e.getMessage() }, Global.LOCALE));
			try {

				RfaEvent event = rfaEventService.getRfaEventByeventId(eventId);
				if (event != null) {
					sendErrorNotificationWhileCreating(SecurityLibrary.getLoggedInUser(), event.getReferanceNumber(), e.getMessage());
				}
			} catch (Exception err) {
				LOG.info("Error while sending email for transferAward", err.getMessage(), err);
			}
		}
		return new ModelAndView("redirect:/buyer/RFA/eventAward/" + eventId + "/" + bqId);
	}

	@RequestMapping(value = "/downloadAwardSnapShot/{id}", method = RequestMethod.GET)
	public void downloadAwardSnapShot(@PathVariable String id, HttpServletResponse response) throws IOException {
		try {
			LOG.info("Award Audid Id  :: :: " + id + "::::::");
			eventAwardService.downloadAwardAuditSnapshot(id, response);
		} catch (Exception e) {
			LOG.error("Error while downloading Award Audit snapshot: " + e.getMessage(), e);
		}
	}

	@RequestMapping(value = "/downloadAwardExcelSnapShot/{id}", method = RequestMethod.GET)
	public void downloadAwardExcelSnapShot(@PathVariable String id, HttpServletResponse response) throws IOException {
		try {
			LOG.info("Award Audid Id  :: :: " + id + "::::::");
			eventAwardService.downloadAwardAuditExcelSnapShot(id, response);
		} catch (Exception e) {
			LOG.error("Error while downloading Award Audit snapshot: " + e.getMessage(), e);
		}
	}

	@RequestMapping(value = "/downloadAwardAttachFile/{id}", method = RequestMethod.GET)
	public void downloadAwardAttachFile(@PathVariable String id, HttpServletResponse response) throws IOException {
		try {
			LOG.info("Event Award  Id  :: :: " + id + "::::::");
			eventAwardService.downloadAwardAttachFileSnapShot(id, response);
		} catch (Exception e) {
			LOG.error("Error while downloading Award Audit snapshot: " + e.getMessage(), e);
		}
	}

	@RequestMapping(value = "/checkProductItemExistOrNot", method = RequestMethod.POST)
	public ResponseEntity<String> checkProductItemExistOrNot(HttpServletResponse response, RedirectAttributes redir, @RequestParam(required = false) String itemAndSupplierId) {
		try {
			LOG.info("itemVal " + itemAndSupplierId);
			String[] itemandSupplier = itemAndSupplierId.split("-");
			String itemId = itemandSupplier[0];
			LOG.info("*****itemName*******        " + itemId);
			String supplierId = itemandSupplier[1];
			LOG.info("******Supplier Id************    " + supplierId);
			RfaSupplierBqItem supplierBqItem = rfaSupplierBqItemService.getBqItemByRfaBqItemId(itemId, supplierId);
			if ((StringUtils.checkString(supplierBqItem.getItemName()).length() > 0) && (StringUtils.checkString(supplierId).length() > 0)) {
				ProductItem productItem = productCategoryMaintenanceService.checkProductItemExistOrNot(supplierBqItem.getItemName(), supplierId, SecurityLibrary.getLoggedInUserTenantId());
				if (productItem != null) {
					return new ResponseEntity<String>("", HttpStatus.OK);

				}
			}

		} catch (Exception e) {
			LOG.error("Error while checkng item already exist or not:  " + e.getMessage(), e);
		}
		return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);

	}

	private String getErpNotifiactionEmailsByBuyerSettings(String tenantId) {
		try {
			if (StringUtils.checkString(tenantId).length() > 0) {
				LOG.info("fetching buyer setting-------------------");
				BuyerSettings buyerSettings = buyerSettingsService.getBuyerSettingsByTenantId(tenantId);
				if (buyerSettings != null) {
					return buyerSettings.getErpNotificationEmails();
				}
			}
		} catch (Exception e) {
			LOG.error("Error while fetching buyer setting :" + e.getMessage(), e);
		}
		return null;
	}

	private String getTimeZoneByBuyerSettings(String tenantId, String timeZone) {
		try {
			if (StringUtils.checkString(tenantId).length() > 0) {
				String time = buyerSettingsService.getBuyerTimeZoneByTenantId(tenantId);
				if (time != null) {
					timeZone = time;
				}
			}
		} catch (Exception e) {
			LOG.error("Error while fetching buyer time zone :" + e.getMessage(), e);
		}
		return timeZone;
	}

	private void sendEmail(String mailTo, String subject, Map<String, Object> map, String template) {
		if (StringUtils.checkString(mailTo).length() > 0) {
			try {
				LOG.info("Sending request email to : " + mailTo);
				String message = FreeMarkerTemplateUtils.processTemplateIntoString(freemarkerConfiguration.getTemplate(template), map);
				notificationService.sendEmail(mailTo, subject, message);
			} catch (Exception e) {
				LOG.error("ERROR while Sending mail :" + e.getMessage(), e);
			}
		} else {
			LOG.warn("No communication email configured for user... Not going to send email notification");
		}
	}

	private void sendErrorNotificationWhileCreating(User user, String prNo, String error) {
		String mailTo = "";
		String subject = "Error ERP event added in ERP Event List";
		String url = APP_URL + "/buyer/erpManualList";
		HashMap<String, Object> map = new HashMap<String, Object>();
		try {

			mailTo = getErpNotifiactionEmailsByBuyerSettings(user.getTenantId());
			map.put("userName", "");
			map.put("prNo", prNo);
			map.put("error", error);
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
			String timeZone = "GMT+8:00";
			timeZone = getTimeZoneByBuyerSettings(user.getTenantId(), timeZone);
			df.setTimeZone(TimeZone.getTimeZone(timeZone));
			map.put("date", df.format(new Date()));
			map.put("appUrl", url);
			map.put("loginUrl", APP_URL + "/login");
			sendEmail(mailTo, subject, map, Global.ERP_EVENT_ERROR_TEMPLATE);
		} catch (Exception e) {
			LOG.error("Error While sending pending mail For adding Erp audit into manual list :" + e.getMessage(), e);
		}
		try {
			String notificationMessage = messageSource.getMessage("erp.added.notification.message", new Object[] { prNo }, Global.LOCALE);
			sendDashboardNotification(user, url, subject, notificationMessage);
		} catch (Exception e) {
			LOG.error("Error While sending notification For Event CREATION :" + e.getMessage(), e);
		}

	}

	private void sendDashboardNotification(User messageTo, String url, String subject, String notificationMessage) {
		NotificationMessage message = new NotificationMessage();
		message.setCreatedBy(null);
		message.setCreatedDate(new Date());
		message.setMessage(notificationMessage);
		message.setNotificationType(NotificationType.EVENT_MESSAGE);
		message.setMessageTo(messageTo);
		message.setSubject(subject);
		message.setTenantId(messageTo.getTenantId());
		message.setUrl(url);
		dashboardNotificationService.save(message);
	}

	@RequestMapping(path = "/transferEventAwardToPr/{templateId}/{userId}/{eventId}", method = RequestMethod.POST)
	public String transferEventAwardToPr(@ModelAttribute RfaEventAward rfaEventAward, @PathVariable String templateId, @PathVariable("eventId") String eventId, @PathVariable String userId, Model model) {
		LOG.info("Template Id:" + templateId);
		LOG.info("Pr Creater Id" + userId);

		try {
			saveProductItem(rfaEventAward);
			String msg = rfaEventService.createPrFromAward(rfaEventAward, templateId, userId, SecurityLibrary.getLoggedInUser());
			rfaEventService.updatePrPushDate(eventId);

			RfaEventAudit audit = new RfaEventAudit();
			RfaEvent event = new RfaEvent();
			event.setId(eventId);
			audit.setEvent(event);
			audit.setAction(AuditActionType.Create);
			audit.setActionBy(SecurityLibrary.getLoggedInUser());
			audit.setActionDate(new Date());
			String prId = msg.substring(msg.indexOf(":") + 1);
			audit.setDescription("Event is pushed to PR : " + (StringUtils.checkString(prId).length() > 0 ? prId : ""));
			eventAuditService.save(audit);

			model.addAttribute("success", messageSource.getMessage("rfa.success.pr.created", new Object[] { msg != null ? msg : "" }, Global.LOCALE));
		} catch (ApplicationException e) {
			LOG.error("Error generating Auto PRs during Event Award : " + e.getMessage(), e);
			model.addAttribute("error", messageSource.getMessage("rfa.award.error.generating", new Object[] { e.getMessage() }, Global.LOCALE));
		} catch (Exception e) {
			LOG.error("Error generating Auto PRs during Event Award : " + e.getMessage(), e);
			// model.addAttribute("error", "Error generating PR : " + e.getMessage());
			model.addAttribute("error", messageSource.getMessage("rfa.award.error.generating", new Object[] { e.getMessage() }, Global.LOCALE));
		}

		return "redirect:/buyer/RFA/eventAward/" + eventId;
	}

	private void saveProductItem(RfaEventAward rfaEventAward) throws ApplicationException {
		if (CollectionUtil.isNotEmpty(rfaEventAward.getRfxAwardDetails())) {
			for (RfaEventAwardDetails rfxAward : rfaEventAward.getRfxAwardDetails()) {
				if (StringUtils.checkString(rfxAward.getSelectItem()).length() > 0) {
					String value = rfxAward.getSelectItem();
					String[] itemNameAndSupplierId = value.split("-");
					String itemId = itemNameAndSupplierId[0];
					String supplierId = itemNameAndSupplierId[1];

					RfaSupplierBqItem supplierBqItem = rfaSupplierBqItemService.getBqItemByRfaBqItemId(itemId, supplierId);

					String itemName = supplierBqItem.getItemName();
					if (StringUtils.checkString(itemName).length() > 0 && StringUtils.checkString(supplierId).length() > 0) {
						ProductItem productItem = productCategoryMaintenanceService.checkProductItemExistOrNot(itemName, supplierId, SecurityLibrary.getLoggedInUserTenantId());

						if (productItem != null) {

							BigDecimal ap = rfxAward.getAwardedPrice();
							productItem.setUnitPrice(ap.divide(supplierBqItem.getQuantity(), 6, BigDecimal.ROUND_DOWN));

							// supplierBqItem.getQuantity();

							// productItem.setUnitPrice(rfxAward.getAwardedPrice());
							productCategoryMaintenanceService.updateProductItemPrice(productItem);
						} else {
							productItem = new ProductItem();
							productItem.setProductName(itemName);
							LOG.info("Item Name New-----" + productItem.getProductName());

							if (StringUtils.checkString(rfxAward.getProductCode()).length() > 0) {
								productItem.setProductCode(rfxAward.getProductCode());
							} else {
								throw new ApplicationException("Please Select Product Code for: " + itemName);
							}

							if (supplierBqItem.getUom() != null) {
								productItem.setUom(supplierBqItem.getUom());
							}
							productItem.setTax(rfxAward.getTax());
							productItem.setCreatedBy(SecurityLibrary.getLoggedInUser());
							productItem.setCreatedDate(new Date());

							BigDecimal ap = rfxAward.getAwardedPrice();
							productItem.setUnitPrice(ap.divide(supplierBqItem.getQuantity(), 6, BigDecimal.ROUND_DOWN));

							productItem.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
							String productCategory = rfxAward.getProductCategory();
							if (StringUtils.checkString(productCategory).length() > 0) {
								ProductCategory category = new ProductCategory();
								category.setId(productCategory);
								productItem.setProductCategory(category);
							} else {
								throw new ApplicationException("Please Select Product Category for: " + itemName);
							}

							FavouriteSupplier favouriteSupplier = favoriteSupplierService.getFavouriteSupplierBySupplierId(supplierId, SecurityLibrary.getLoggedInUserTenantId());
							productItem.setFavoriteSupplier(favouriteSupplier);
							productItem.setStatus(Status.ACTIVE);
							productCategoryMaintenanceService.saveNewProductItem(productItem);
						}
					}
				}
			}
		}
	}
}
