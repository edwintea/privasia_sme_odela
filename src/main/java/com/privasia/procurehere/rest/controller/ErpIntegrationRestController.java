package com.privasia.procurehere.rest.controller;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.privasia.procurehere.core.dao.BuyerAddressDao;
import com.privasia.procurehere.core.dao.CurrencyDao;
import com.privasia.procurehere.core.dao.ErpSetupDao;
import com.privasia.procurehere.core.dao.RequestAuditDao;
import com.privasia.procurehere.core.dao.UserDao;
import com.privasia.procurehere.core.entity.BusinessUnit;
import com.privasia.procurehere.core.entity.Buyer;
import com.privasia.procurehere.core.entity.BuyerAddress;
import com.privasia.procurehere.core.entity.BuyerSettings;
import com.privasia.procurehere.core.entity.CostCenter;
import com.privasia.procurehere.core.entity.Country;
import com.privasia.procurehere.core.entity.Currency;
import com.privasia.procurehere.core.entity.ErpAudit;
import com.privasia.procurehere.core.entity.ErpAwardStaging;
import com.privasia.procurehere.core.entity.ErpSetup;
import com.privasia.procurehere.core.entity.Event;
import com.privasia.procurehere.core.entity.FavouriteSupplier;
import com.privasia.procurehere.core.entity.NotificationMessage;
import com.privasia.procurehere.core.entity.Po;
import com.privasia.procurehere.core.entity.PoItem;
import com.privasia.procurehere.core.entity.Pr;
import com.privasia.procurehere.core.entity.PrAudit;
import com.privasia.procurehere.core.entity.ProductCategory;
import com.privasia.procurehere.core.entity.ProductCategoryPojo;
import com.privasia.procurehere.core.entity.ProductContract;
import com.privasia.procurehere.core.entity.ProductContractItems;
import com.privasia.procurehere.core.entity.ProductItem;
import com.privasia.procurehere.core.entity.RequestAudit;
import com.privasia.procurehere.core.entity.RfaEvent;
import com.privasia.procurehere.core.entity.RfaEventAudit;
import com.privasia.procurehere.core.entity.RfpEvent;
import com.privasia.procurehere.core.entity.RfpEventAudit;
import com.privasia.procurehere.core.entity.RfqEvent;
import com.privasia.procurehere.core.entity.RfqEventAudit;
import com.privasia.procurehere.core.entity.RftEvent;
import com.privasia.procurehere.core.entity.RftEventAudit;
import com.privasia.procurehere.core.entity.SourcingFormRequest;
import com.privasia.procurehere.core.entity.State;
import com.privasia.procurehere.core.entity.Uom;
import com.privasia.procurehere.core.entity.User;
import com.privasia.procurehere.core.enums.AuditActionType;
import com.privasia.procurehere.core.enums.ErpAuditType;
import com.privasia.procurehere.core.enums.EventStatus;
import com.privasia.procurehere.core.enums.NotificationType;
import com.privasia.procurehere.core.enums.OperationType;
import com.privasia.procurehere.core.enums.PoStatus;
import com.privasia.procurehere.core.enums.PrAuditType;
import com.privasia.procurehere.core.enums.PrStatus;
import com.privasia.procurehere.core.enums.ProcurehereDocumentType;
import com.privasia.procurehere.core.enums.RequestAuditType;
import com.privasia.procurehere.core.enums.RfxTypes;
import com.privasia.procurehere.core.enums.SourcingFormStatus;
import com.privasia.procurehere.core.enums.Status;
import com.privasia.procurehere.core.exceptions.ApplicationException;
import com.privasia.procurehere.core.pojo.ErpPoPojo;
import com.privasia.procurehere.core.pojo.ErpRequestPojo;
import com.privasia.procurehere.core.pojo.PoItemsPojo;
import com.privasia.procurehere.core.pojo.PoPojo;
import com.privasia.procurehere.core.pojo.PrResponseErpPojo;
import com.privasia.procurehere.core.pojo.PrResponsePojo;
import com.privasia.procurehere.core.pojo.PrToAuctionErpPojo;
import com.privasia.procurehere.core.pojo.ProductContractItemsPojo;
import com.privasia.procurehere.core.pojo.ProductContractPojo;
import com.privasia.procurehere.core.pojo.ProductItemPojo;
import com.privasia.procurehere.core.pojo.PurchaseGroupsPojo;
import com.privasia.procurehere.core.pojo.RFQResponseErpPojo;
import com.privasia.procurehere.core.pojo.UomPojo;
import com.privasia.procurehere.core.utils.CollectionUtil;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.core.utils.StringUtils;
import com.privasia.procurehere.service.ApprovalService;
import com.privasia.procurehere.service.BusinessUnitService;
import com.privasia.procurehere.service.BuyerService;
import com.privasia.procurehere.service.BuyerSettingsService;
import com.privasia.procurehere.service.CostCenterService;
import com.privasia.procurehere.service.CountryService;
import com.privasia.procurehere.service.DashboardNotificationService;
import com.privasia.procurehere.service.ErpAuditService;
import com.privasia.procurehere.service.ErpAwardStaggingService;
import com.privasia.procurehere.service.ErpIntegrationService;
import com.privasia.procurehere.service.EventAuditService;
import com.privasia.procurehere.service.FavoriteSupplierService;
import com.privasia.procurehere.service.NotificationService;
import com.privasia.procurehere.service.PoService;
import com.privasia.procurehere.service.PrAuditService;
import com.privasia.procurehere.service.PrService;
import com.privasia.procurehere.service.ProductCategoryMaintenanceService;
import com.privasia.procurehere.service.ProductContractItemsService;
import com.privasia.procurehere.service.ProductContractService;
import com.privasia.procurehere.service.ProductListMaintenanceService;
import com.privasia.procurehere.service.RfaEventService;
import com.privasia.procurehere.service.RfpEventService;
import com.privasia.procurehere.service.RfqEventService;
import com.privasia.procurehere.service.RftEventService;
import com.privasia.procurehere.service.SourcingFormRequestService;
import com.privasia.procurehere.service.StateService;
import com.privasia.procurehere.service.UomService;
import com.privasia.procurehere.service.UserService;
import com.privasia.procurehere.web.editors.FavouriteSupplierEditor;
import com.privasia.procurehere.web.editors.ProductCategoryMaintenanceEditor;
import com.privasia.procurehere.web.editors.UomEditor;

import io.swagger.annotations.Api;

/**
 * @author parveen
 */
@RestController
@RequestMapping("/erpApi")
@Api(value = "Erp Integration", description = "Er Integration Api")
public class ErpIntegrationRestController {

	private static final Logger LOG = Logger.getLogger(Global.ERP_LOG);

	@Autowired
	ErpSetupDao erpSetupDao;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	ErpAuditService erpAuditService;

	@Autowired
	UserDao userDao;

	@Autowired
	UserService userService;

	@Autowired
	BuyerService buyerService;

	@Autowired
	ErpIntegrationService erpIntegrationService;

	@Autowired
	RfaEventService rfaEventService;

	@Autowired
	RfpEventService rfpEventService;

	@Autowired
	RftEventService rftEventService;

	@Autowired
	RfqEventService rfqEventService;

	@Autowired
	PrService prService;

	@Autowired
	NotificationService notificationService;

	@Autowired
	DashboardNotificationService dashboardNotificationService;

	@Resource
	MessageSource messageSource;

	@Value("${app.url}")
	String APP_URL;

	@Autowired
	ErpAwardStaggingService erpAwardStaggingService;

	@Autowired
	BuyerSettingsService buyerSettingsService;

	@Autowired
	UomService uomService;

	@Autowired
	ProductCategoryMaintenanceService productCategoryMaintenanceService;

	@Autowired
	ProductListMaintenanceService productListMaintenanceService;

	@Autowired
	ProductContractService productContractService;

	@Autowired
	UomEditor uomEditor;

	@Autowired
	ProductCategoryMaintenanceEditor pcmEditor;

	@Autowired
	FavouriteSupplierEditor fsEditor;

	@Autowired
	FavoriteSupplierService favoriteSupplierService;

	@Autowired
	BusinessUnitService businessUnitService;

	@Autowired
	ProductContractItemsService productContractItemsService;

	@Autowired
	PrAuditService prAuditService;

	@Autowired
	CostCenterService costCenterService;

	@Autowired
	SourcingFormRequestService sourcingFormRequestService;

	@Autowired
	ApprovalService approvalService;

	@Autowired
	RequestAuditDao requestAuditDao;

	@Autowired
	CountryService countryService;

	@Autowired
	StateService stateService;

	@Autowired
	PoService poService;

	@Autowired
	CurrencyDao currencyDao;

	@Autowired
	BuyerAddressDao buyerAddressDao;

	@Autowired
	EventAuditService eventAuditService;

	// @formatter:off
	/**
	 * @api {post} /erpApi/prResponseData PR Response
	 * @apiName PR Response Data
	 * @apiGroup ERP
	 * @apiHeader {String} Content-Type Should be application/json
	 * @apiHeader {String} X-Authorization X-AUTH-KEY
	 * @apiHeaderExample {json} Header-Example: { "Content-Type":"application/json",
	 *                   "X-AUTH-KEY":"284c6798089445cf444516eea037feff1ad25dbe27ef7800a22dd2888ba6aa34" }
	 * @apiParam {String{64}} prReqNo Reference Number for EPROC PR
	 * @apiParam {String{64}} status Message type (SUCCESS/ERROR)
	 * @apiParam {String} headerNote Header note contain PRID,Approver Name,GeneralRemarks,ApprovalRemarks with ','
	 *           separated
	 * @apiParam {String{1050}} message Message
	 * @apiParamExample {json} Request-Example: { "message":"Purchase requisition number 0010093788 created",
	 *                  "status":"SUCCESS", "headerNote":"PR2381,G.RAVINDRAN GUNASEKARAN", "prReqNo":"P190407001" }
	 * @apiExample {curl} Example usage: curl -X POST -H
	 *             "X-AUTH-KEY:284c6798089445cf444516eea037feff1ad25dbe27ef7800a22dd2888ba6aa34" -H "Cache-Control:
	 *             no-cache" -H "Content-Type: application/json" -d ' { "message":"Purchase requisition number
	 *             0010093788 created", "status":"SUCCESS", "headerNote":"PR2381,G.RAVINDRAN GUNASEKARAN",
	 *             "prReqNo":"P190407001" }' "https://testtwo.procurehere.com/erpApi/prResponseData"
	 * @apiSuccessExample Success-Response: HTTP/1.1 200 OK { "success" : "PR response updated in EPROC successfully" }
	 * @apiSuccess success Success Message
	 * @apiError 401 Unauthorized access to protected resource.
	 * @apiError 500 Internal Server error
	 * @apiErrorExample HTTP/1.1 401 Unauthorized { "error" : "Auth key not valid" }
	 * @apiErrorExample HTTP/1.1 500 Internal server error { "error" : "Error while getting PR response in EPROC" }
	 */
	// @formatter:on
	@RequestMapping(value = "/prResponseData", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> prResponseData(@RequestBody PrResponseErpPojo prResponseErpPojo, @RequestHeader(name = "X-AUTH-KEY", required = true) String authKey) {
		LOG.info("PR Response Data Api called..");
		Map<String, Object> map = new HashMap<>();
		try {
			ObjectMapper mapperObj = new ObjectMapper();
			String payload = mapperObj.writeValueAsString(prResponseErpPojo);
			LOG.info("Payload :" + payload);
			ErpSetup erpSetup = erpSetupDao.getErpConfigByAppId(authKey);
			if (erpSetup != null) {
				String prId = prResponseErpPojo.getHeaderNote().split(",")[0];
				prResponseErpPojo.setId(prId);
				Pr pr = prService.updatePrResponse(prResponseErpPojo, erpSetup);
				if (pr != null) {
					map.put("success", "PR response updated in EPROC successfully");
				} else {
					map.put("error", "Need attention something is wrong to update the pr which is null for pr id :" + prResponseErpPojo.getId());
					HttpHeaders headers = new HttpHeaders();
					headers.add("error", "Need attention something is wrong to update the pr which is null for pr id :" + prResponseErpPojo.getId());
					return new ResponseEntity<Map<String, Object>>(map, headers, HttpStatus.INTERNAL_SERVER_ERROR);
				}
			} else {
				LOG.info("Erp is null for app ID :" + prResponseErpPojo.getAppId());
				map.put("error", "Auth key not valid");
				HttpHeaders headers = new HttpHeaders();
				headers.add("error", "Auth key not valid");
				return new ResponseEntity<Map<String, Object>>(map, headers, HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			LOG.error("Error while PR response updated in procurehere :" + e.getMessage(), e);
			map.put("error", "Error while getting PR response in EPROC :" + e.getMessage());
			HttpHeaders headers = new HttpHeaders();
			headers.add("error", "Error while getting PR response in EPROC :" + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(map, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

	// @formatter:off
	/**
	 * @api {post} /erpApi/rfqResponseData RFX Response
	 * @apiName RFX Response Data
	 * @apiGroup ERP
	 * @apiHeader {String} Content-Type Should be application/json
	 * @apiHeader {String} X-Authorization X-AUTH-KEY
	 * @apiHeaderExample {json} Header-Example: { "Content-Type":"application/json",
	 *                   "X-AUTH-KEY":"284c6798089445cf444516eea037feff1ad25dbe27ef7800a22dd2888ba6aa34" }
	 * @apiParam {String{500}} rfxDocNo Reference Number for EPROC RFX
	 * @apiParam {String{10}} status Message type (SUCCESS/ERROR)
	 * @apiParam {String{3000}} message Message
	 * @apiParamExample {json} Request-Example: { "message":"RFQ created under the number P190407001",
	 *                  "status":"SUCCESS","rfxDocNo":"P190407001" }
	 * @apiExample {curl} Example usage: curl -X POST -H
	 *             "X-AUTH-KEY:284c6798089445cf444516eea037feff1ad25dbe27ef7800a22dd2888ba6aa34" -H "Cache-Control:
	 *             no-cache" -H "Content-Type: application/json" -d '{ "message":"RFQ created under the number
	 *             P190407001", "status":"SUCCESS","rfxDocNo":"P190407001" }'
	 *             "https://testtwo.procurehere.com/erpApi/rfqResponseData"
	 * @apiSuccessExample Success-Response: HTTP/1.1 200 OK { "success" : "RFx Response Get In EPROC Successfully" }
	 * @apiSuccess success Success Message
	 * @apiError 401 Unauthorized access to protected resource.
	 * @apiError 500 Internal Server error
	 * @apiErrorExample HTTP/1.1 401 Unauthorized { "error" : "Auth key not valid" }
	 * @apiErrorExample HTTP/1.1 500 Internal server error { "error" : "Error while getting RFX response in EPROC" }
	 */

	// @formatter:on
	@RequestMapping(value = "/rfqResponseData", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> rfqResponseData(@RequestBody RFQResponseErpPojo rfqResponseErpPojo, @RequestHeader(name = "X-AUTH-KEY", required = true) String authKey) {
		LOG.info("RFQ Response Data Api called..");
		Map<String, Object> map = new HashMap<>();
		try {
			ObjectMapper mapperObj = new ObjectMapper();
			String payload = mapperObj.writeValueAsString(rfqResponseErpPojo);
			LOG.info("Payload :" + payload);
			ErpSetup erpSetup = erpSetupDao.getErpConfigByAppId(authKey);
			if (erpSetup != null) {
				erpIntegrationService.updateRfqResponse(rfqResponseErpPojo, erpSetup);
			} else {
				map.put("error", "Auth key not valid");
				HttpHeaders headers = new HttpHeaders();
				headers.add("error", "Auth key not valid");
				return new ResponseEntity<Map<String, Object>>(map, headers, HttpStatus.UNAUTHORIZED);
			}
			map.put("success", "RFX Response Get In EPROC Successfully");
		} catch (Exception e) {
			LOG.error("Error while RFQ response updated in procurehere :" + e.getMessage(), e);
			map.put("error", "Error while getting RFX response in EPROC :" + e.getMessage());
			HttpHeaders headers = new HttpHeaders();
			headers.add("error", "Error while getting RFX response in EPROC :" + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(map, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

	// @formatter:off
	/**
	 * @api {post} /erpApi/erpAuctionData Create RFX
	 * @apiName Auction Response Data
	 * @apiGroup ERP
	 * @apiHeader {String} Content-Type Should be application/json
	 * @apiHeader {String} X-Authorization X-AUTH-KEY
	 * @apiHeaderExample {json} Header-Example: { "Content-Type":"application/json",
	 *                   "X-AUTH-KEY":"284c6798089445cf444516eea037feff1ad25dbe27ef7800a22dd2888ba6aa34" }
	 * @apiParam {String{64}} erpRefNo Reference Number for EPROC RFX
	 * @apiParam {String{64}} curr Currency Code
	 * @apiParam {String[]} itemList Item List
	 * @apiParam (Item List){String{100}} itemNo Item No
	 * @apiParam (Item List){String{100}} itemName Item Name
	 * @apiParam (Item List){String{250}} itemDesc Item Desc
	 * @apiParam (Item List){String{10}} qty Item Quantity
	 * @apiParam (Item List){String{64}} ordrUom Unit of Measurement
	 * @apiParam (Item List){String{100}} extSubItm Child Item No [optional]
	 * @apiParam (Item List){String{10}} extSvcQty Child Item quantity [optional]
	 * @apiParam (Item List){String{64}} extBaseUom Child Unit of measurement [optional]
	 * @apiParam (Item List){String{250}} extValItm Child Item Name [optional]
	 * @apiParam (Item List){String{100}} itemCategory Item Category
	 * @apiParam (Item List){String{100}} materialNo Material No
	 * @apiParam (Item List){String{100}} materialGroup Material Group
	 * @apiParam (Item List){String{100}} brandDesc Brand Description
	 * @apiParam (Item List){String{100}} mfr_PartNO Manufacturer Part No
	 * @apiParam (Item List){String{100}} purhaseGroup Purchase Group
	 * @apiParam (Item List){String{100}} deliveryDate Delivery Date
	 * @apiParamExample {json} Request-Example: { "erpRefNo":"0010090429", "curr":"MYR", "itemList":[ {
	 *                  "itemNo":"00010", "itemName":"item 1", "itemDesc":"To Supply Install 1 Unit LC Meter for",
	 *                  "qty":"1.0", "ordrUom":"AU", "extSubItm":null, "extSvcQty":null, "extBaseUom":null,
	 *                  "extValItm":null, "itemCategory":"9", "materialNo":null, "materialGroup":"ZZ001",
	 *                  "brandDesc":null, "mfr_PartNO":null, "purhaseGroup":"XUP", "deliveryDate":"20190114", }, {
	 *                  "itemNo":"00020", "itemName":"item 2", "itemDesc":"To Supply Install 1 Unit LC Meter for",
	 *                  "qty":"1.0", "ordrUom":"AU", "extSubItm":null, "extSvcQty":null, "extBaseUom":null,
	 *                  "extValItm":null, "itemCategory":"9", "materialNo":null, "materialGroup":"ZZ001",
	 *                  "brandDesc":null, "mfr_PartNO":null, "purhaseGroup":"XUP", "deliveryDate":"20190114", }, {
	 *                  "itemNo":"00030", "itemName":"item 3", "itemDesc":"To Supply Install 1 Unit LC Meter for",
	 *                  "qty":"1.0", "ordrUom":"AU", "extSubItm":null, "extSvcQty":null, "extBaseUom":null,
	 *                  "extValItm":null, "itemCategory":"9", "materialNo":null, "materialGroup":"ZZ001",
	 *                  "brandDesc":null, "mfr_PartNO":null, "purhaseGroup":"XUP", "deliveryDate":"20190114", } ] }
	 * @apiExample {curl} Example usage: curl -X POST -H "X-AUTH-KEY:
	 *             284c6798089445cf444516eea037feff1ad25dbe27ef7800a22dd2888ba6aa34" -H "Cache-Control: no-cache" -H
	 *             "Content-Type: application/json" -d '{ "erpRefNo":"0010090429", "curr":"MYR", "itemList":[ {
	 *             "itemNo":"00010", "itemName":"item 1", "itemDesc":"To Supply Install 1 Unit LC Meter for",
	 *             "qty":"1.0", "ordrUom":"AU", "extSubItm":null, "extSvcQty":null, "extBaseUom":null, "extValItm":null,
	 *             "itemCategory":"9", "materialNo":null, "materialGroup":"ZZ001", "brandDesc":null, "mfr_PartNO":null,
	 *             "purhaseGroup":"XUP", "deliveryDate":"20190114", }, { "itemNo":"00020", "itemName":"item 2",
	 *             "itemDesc":"To Supply Install 1 Unit LC Meter for", "qty":"1.0", "ordrUom":"AU", "extSubItm":null,
	 *             "extSvcQty":null, "extBaseUom":null, "extValItm":null, "itemCategory":"9", "materialNo":null,
	 *             "materialGroup":"ZZ001", "brandDesc":null, "mfr_PartNO":null, "purhaseGroup":"XUP",
	 *             "deliveryDate":"20190114", }, { "itemNo":"00030", "itemName":"item 3", "itemDesc":"To Supply Install
	 *             1 Unit LC Meter for", "qty":"1.0", "ordrUom":"AU", "extSubItm":null, "extSvcQty":null,
	 *             "extBaseUom":null, "extValItm":null, "itemCategory":"9", "materialNo":null, "materialGroup":"ZZ001",
	 *             "brandDesc":null, "mfr_PartNO":null, "purhaseGroup":"XUP", "deliveryDate":"20190114", } ] }'
	 *             "https://testtwo.procurehere.com/erpApi/erpAuctionData"
	 * @apiSuccessExample Success-Response: HTTP/1.1 200 OK { "success" : "Auction Data Get In EPROC Successfully" }
	 * @apiSuccess success Success Message
	 * @apiError 401 Unauthorized access to protected resource.
	 * @apiError 500 Internal Server error
	 * @apiErrorExample HTTP/1.1 401 Unauthorized { "error" : "Auth key not valid" }
	 * @apiErrorExample HTTP/1.1 500 Internal server error { "error" : "Error while getting auction data in EPROC" }
	 */

	// @formatter:on
	@RequestMapping(value = "/erpAuctionData", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> erpAuctionData(@RequestBody PrToAuctionErpPojo prToAuctionErpPojo, @RequestHeader(name = "X-AUTH-KEY", required = true) String authKey) {
		LOG.info("send auction Data Api called..");
		Map<String, Object> map = new HashMap<>();
		try {
			ObjectMapper mapperObj = new ObjectMapper();
			String payload = mapperObj.writeValueAsString(prToAuctionErpPojo);
			LOG.info("Payload From Sap=====================================>:" + payload);
			ErpSetup erpConfig = erpSetupDao.getErpConfigWithTepmlateByAppId(authKey);
			if (erpConfig != null) {
				prToAuctionErpPojo.setAppId(authKey);
				boolean sendCreationMail = false;
				boolean sendDuplicateMail = false;
				Event event = null;
				ErpAudit erpAudit = new ErpAudit();
				erpAudit.setPrNo(prToAuctionErpPojo.getPrNo());
				Buyer buyer = buyerService.findBuyerById(erpConfig.getTenantId());
				User adminUser = userService.getUserByLoginIdNoTouch(buyer.getLoginEmail());
				adminUser.setBuyer(buyer);
				if (!duplicate(prToAuctionErpPojo.getPrNo(), erpConfig.getTenantId())) {
					if (Boolean.TRUE == erpConfig.getCreateEventAuto()) {
						erpAudit.setAction(ErpAuditType.CREATED);
						erpAudit.setResponseMsg("Created");
						switch (erpConfig.getType()) {
						case RFA:
							if (erpConfig.getRfaTemplate() != null) {
								event = erpIntegrationService.copyFromRfaTemplateForErp(erpConfig.getRfaTemplate().getId(), adminUser, prToAuctionErpPojo);

								sendCreationMail = true;
							}
							break;
						case RFP:
							if (erpConfig.getRfpTemplate() != null) {
								event = erpIntegrationService.copyFromRfpTemplateForErp(erpConfig.getRfpTemplate().getId(), adminUser, prToAuctionErpPojo);
								sendCreationMail = true;
							}
							break;
						case RFQ:
							if (erpConfig.getRfqTemplate() != null) {
								event = erpIntegrationService.copyFromRfqTemplateForErp(erpConfig.getRfqTemplate().getId(), adminUser, prToAuctionErpPojo);
								sendCreationMail = true;
							}
							break;
						case RFT:
							if (erpConfig.getRftTemplate() != null) {
								event = erpIntegrationService.copyFromRftTemplateForErp(erpConfig.getRftTemplate().getId(), adminUser, prToAuctionErpPojo);
								sendCreationMail = true;
							}
							break;
						default: {
							erpAudit.setAction(ErpAuditType.PENDING);
							erpAudit.setResponseMsg("Pending");
						}
							break;
						}
					} else {
						erpAudit.setAction(ErpAuditType.PENDING);
						erpAudit.setResponseMsg("Pending");
					}
				} else {
					LOG.info("Duplicate event for sap PR NO :" + prToAuctionErpPojo.getPrNo());
					erpAudit.setAction(ErpAuditType.DUPLICATE);
					erpAudit.setResponseMsg("Duplicate No: " + prToAuctionErpPojo.getPrNo());
					LOG.info("Send duplicate mail for pr no: " + prToAuctionErpPojo.getPrNo());
					// sending duplicate email
					sendDuplicateRecordInAudit(adminUser, prToAuctionErpPojo.getPrNo());
					sendDuplicateMail = true;
				}
				// setting erp event data.
				try {
					erpAudit.setActionBy(adminUser);
					erpAudit.setActionDate(new Date());
					erpAudit.setTenantId(adminUser.getTenantId());
					erpAudit.setPayload(payload);
					erpAudit = erpAuditService.save(erpAudit);
				} catch (Exception e) {
					LOG.error("Error while saving audit create event :" + e.getMessage(), e);
				}
				if (sendCreationMail) {

					event.setNextEventType(erpConfig.getType());
					sendRfxCreatedEmails(event, adminUser, erpConfig.getType());
					LOG.info("Send creation mail for pr no: " + prToAuctionErpPojo.getPrNo());
				} else if (!sendDuplicateMail) {
					LOG.info("Send pending mail for pr no: " + prToAuctionErpPojo.getPrNo());
					sendAddedRecordInAudit(adminUser, prToAuctionErpPojo.getPrNo());
				}
			} else {
				LOG.info("config is null for \"" + prToAuctionErpPojo.getAppId() + "\" APP ID");
				map.put("error", "Auth key not valid");
				HttpHeaders headers = new HttpHeaders();
				headers.add("error", "Auth key not valid");
				return new ResponseEntity<Map<String, Object>>(map, headers, HttpStatus.UNAUTHORIZED);
			}
			map.put("success", "Auction Data Get In EPROC Successfully");
		} catch (Exception e) { // 500
			// TODO sending event error mail to buyer admin for now, will change it to ERP configurable emails.
			saveErrorAudit(prToAuctionErpPojo, e);
			LOG.error("Error while getting auction data in procurehere :" + e.getMessage(), e);
			map.put("error", "Error while getting auction data in EPROC :" + e.getMessage());
			HttpHeaders headers = new HttpHeaders();
			headers.add("error", "Error while getting auction data in EPROC :" + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(map, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

	/**
	 * @param prToAuctionErpPojo
	 * @param e
	 */
	private void saveErrorAudit(PrToAuctionErpPojo prToAuctionErpPojo, Exception e) {
		try {
			ObjectMapper mapperObj = new ObjectMapper();
			ErpSetup erpConfig = erpSetupDao.getErpConfigWithTepmlateByAppId(prToAuctionErpPojo.getAppId());
			if (erpConfig != null) {
				Buyer buyer = buyerService.findBuyerById(erpConfig.getTenantId());
				User adminUser = userService.getUserByLoginIdNoTouch(buyer.getLoginEmail());
				String payload = mapperObj.writeValueAsString(prToAuctionErpPojo);
				LOG.info("Error Payload :" + payload);
				ErpAudit erpAudit = new ErpAudit();
				erpAudit.setPrNo(prToAuctionErpPojo.getPrNo());
				erpAudit.setAction(ErpAuditType.ERROR);
				if (e.getMessage().equals("BUSINESS_UNIT_EMPTY")) {
					erpAudit.setResponseMsg("Id setting is based on business unit which is not present in template.");
				} else {
					erpAudit.setResponseMsg("Error while process event: " + e.getMessage());
				}
				erpAudit.setActionBy(adminUser);
				erpAudit.setActionDate(new Date());
				erpAudit.setTenantId(adminUser.getTenantId());
				erpAudit.setPayload(payload);
				erpAudit = erpAuditService.save(erpAudit);
				sendErrorNotificationWhileCreating(adminUser, prToAuctionErpPojo.getPrNo(), e.getMessage());
			} else {
				LOG.info("config is null for \"" + prToAuctionErpPojo.getAppId() + "\" APP ID");
			}
		} catch (Exception e1) {
			LOG.error("Error while parsing object to json: " + e1.getMessage(), e1);
		}
	}

	private void sendAddedRecordInAudit(User user, String prNo) {
		String mailTo = "";
		String subject = "ERP event added in ERP Event List";
		String url = APP_URL + "/buyer/erpManualList";
		HashMap<String, Object> map = new HashMap<String, Object>();
		try {
			mailTo = getErpNotifiactionEmailsByBuyerSettings(user.getTenantId());
			map.put("userName", " ");
			map.put("prNo", prNo);
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
			String timeZone = "GMT+8:00";
			timeZone = getTimeZoneByBuyerSettings(user.getTenantId(), timeZone);
			df.setTimeZone(TimeZone.getTimeZone(timeZone));
			map.put("date", df.format(new Date()));
			map.put("appUrl", url);
			map.put("loginUrl", APP_URL + "/login");
			sendEmail(mailTo, subject, map, Global.ERP_EVENT_PENDING_TEMPLATE);
		} catch (Exception e) {
			LOG.error("Error While sending pending mail For adding Erp audit into manual list :" + e.getMessage(), e);
		}
		try {
			String notificationMessage = messageSource.getMessage("erp.added.notification.message", new Object[] { prNo }, Global.LOCALE);
			sendDashboardNotification(user, url, subject, notificationMessage);
		} catch (Exception e) {
			LOG.error("Error While sending notification For Event CREATION :" + e.getMessage(), e);
		}

	}

	private void sendDuplicateRecordInAudit(User user, String prNo) {
		String mailTo = "";
		String subject = "Duplicate ERP event added in ERP Event List";
		String url = APP_URL + "/buyer/erpManualList";
		HashMap<String, Object> map = new HashMap<String, Object>();
		try {
			mailTo = getErpNotifiactionEmailsByBuyerSettings(user.getTenantId());
			map.put("userName", "");
			map.put("prNo", prNo);
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
			String timeZone = "GMT+8:00";
			timeZone = getTimeZoneByBuyerSettings(user.getTenantId(), timeZone);
			df.setTimeZone(TimeZone.getTimeZone(timeZone));
			map.put("date", df.format(new Date()));
			map.put("appUrl", url);
			map.put("loginUrl", APP_URL + "/login");
			sendEmail(mailTo, subject, map, Global.ERP_EVENT_DUPLICATE_TEMPLATE);
		} catch (Exception e) {
			LOG.error("Error While sending pending mail For adding Erp audit into manual list :" + e.getMessage(), e);
		}
		try {
			String notificationMessage = messageSource.getMessage("erp.added.notification.message", new Object[] { prNo }, Global.LOCALE);
			sendDashboardNotification(user, url, subject, notificationMessage);
		} catch (Exception e) {
			LOG.error("Error While sending notification For Event CREATION :" + e.getMessage(), e);
		}

	}

	private void sendErrorNotificationWhileCreating(User user, String prNo, String error) {
		String mailTo = "";
		String subject = "Error ERP event added in ERP Event List";
		String url = APP_URL + "/buyer/erpManualList";
		HashMap<String, Object> map = new HashMap<String, Object>();
		try {

			mailTo = getErpNotifiactionEmailsByBuyerSettings(user.getTenantId());
			map.put("userName", "");
			map.put("prNo", prNo);
			map.put("error", error);
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
			String timeZone = "GMT+8:00";
			timeZone = getTimeZoneByBuyerSettings(user.getTenantId(), timeZone);
			df.setTimeZone(TimeZone.getTimeZone(timeZone));
			map.put("date", df.format(new Date()));
			map.put("appUrl", url);
			map.put("loginUrl", APP_URL + "/login");
			sendEmail(mailTo, subject, map, Global.ERP_EVENT_ERROR_TEMPLATE);
		} catch (Exception e) {
			LOG.error("Error While sending pending mail For adding Erp audit into manual list :" + e.getMessage(), e);
		}
		try {
			String notificationMessage = messageSource.getMessage("erp.added.notification.message", new Object[] { prNo }, Global.LOCALE);
			sendDashboardNotification(user, url, subject, notificationMessage);
		} catch (Exception e) {
			LOG.error("Error While sending notification For Event CREATION :" + e.getMessage(), e);
		}

	}

	private boolean duplicate(String prNo, String tenantId) {
		if (StringUtils.checkString(prNo).length() == 0) {
			return false;
		}
		return erpAuditService.isExists(prNo, tenantId);

	}

	// Rest api to Generate PO
	@RequestMapping(value = "/erpPoResponseData", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> prResponseData(@RequestBody List<PrResponseErpPojo> poResponseErpList, @RequestHeader(name = "X-AUTH-KEY") String authKey) {
		LOG.info("PR Response Data Api called..");
		Map<String, Object> map = new HashMap<>();
		try {
			ObjectMapper mapperObj = new ObjectMapper();
			String payload = mapperObj.writeValueAsString(poResponseErpList);
			LOG.info("Payload :" + payload);
			prService.updatePoResponse(poResponseErpList);
			map.put("success", "PO response updated in procurehere successfully");
		} catch (Exception e) {
			LOG.error("Error while Generate PO in procurehere :" + e.getMessage(), e);
			map.put("error", "Error while Generate PO in procurehere :" + e.getMessage());
			HttpHeaders header = new HttpHeaders();
			header.add("error", "Error while Generate PO in procurehere :" + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(map, header, HttpStatus.INTERNAL_SERVER_ERROR);

		}
		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

	@RequestMapping(value = "/erpPoData", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> poData(@RequestBody ErpPoPojo erpPoList, @RequestHeader(name = "X-AUTH-KEY") String authKey) {
		LOG.info("PR Response Data Api called..");
		Map<String, Object> map = new HashMap<>();
		try {
			ErpSetup erpSetup = erpSetupDao.getErpConfigByAppId(authKey);
			if (erpSetup != null) {
				ObjectMapper mapperObj = new ObjectMapper();
				String payload = mapperObj.writeValueAsString(erpPoList);
				LOG.info("Payload :" + payload);
				prService.createPo(erpPoList, erpSetup.getTenantId());
				map.put("success", "PO created in procurehere successfully");
			} else {
				LOG.info("config is not found for " + authKey);
				map.put("error", "Auth key not valid");
				HttpHeaders headers = new HttpHeaders();
				headers.add("error", "Auth key not valid");
				return new ResponseEntity<Map<String, Object>>(map, headers, HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			LOG.error("Error while Generate PO in procurehere :" + e.getMessage(), e);
			map.put("error", "Error while Generate PO in procurehere :" + e.getMessage());
			HttpHeaders header = new HttpHeaders();
			header.add("error", "Error while Generate PO in procurehere :" + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(map, header, HttpStatus.INTERNAL_SERVER_ERROR);

		}
		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

	protected void sendRfxCreatedEmails(Event event, User user, RfxTypes type) {
		String mailTo = "";
		String subject = "Event Created";
		// String url = APP_URL + "/login";
		String url = APP_URL + "/buyer/" + event.getNextEventType().name() + "/createEventDetails/" + event.getId();
		HashMap<String, Object> map = new HashMap<String, Object>();
		try {
			boolean isContainMail = true;
			mailTo = getErpNotifiactionEmailsByBuyerSettings(user.getTenantId()).toUpperCase();
			if (StringUtils.checkString(mailTo).length() > 0) {
				String[] mails = mailTo.split(",");
				for (String mail : mails) {
					if (mail.equalsIgnoreCase(user.getCommunicationEmail())) {
						isContainMail = false;
					}
				}
			}
			if (isContainMail) {
				mailTo = user.getCommunicationEmail() + "," + mailTo;
			}

			LOG.info("-------------------Sending Emails to the --------------" + mailTo);
			map.put("userName", "");
			map.put("event", event);
			map.put("eventType", type.name());
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
			String timeZone = "GMT+8:00";
			timeZone = getTimeZoneByBuyerSettings(user.getTenantId(), timeZone);
			df.setTimeZone(TimeZone.getTimeZone(timeZone));
			map.put("date", df.format(new Date()));
			map.put("appUrl", url);
			map.put("loginUrl", APP_URL + "/login");
			sendEmail(mailTo, subject, map, Global.ERP_EVENT_CREATED_TEMPLATE);
		} catch (Exception e) {
			LOG.error("Error While sending mail For Event CREATION :" + e.getMessage(), e);
		}
		try {
			String notificationMessage = messageSource.getMessage("event.created.notification.message", new Object[] { event.getReferanceNumber() }, Global.LOCALE);
			sendDashboardNotification(user, url, subject, notificationMessage);
		} catch (Exception e) {
			LOG.error("Error While sending notification For Event CREATION :" + e.getMessage(), e);
		}
	}

	public void sendEmail(String mailTo, String subject, Map<String, Object> map, String template) {
		notificationService.sendEmail(mailTo, subject, map, template);
	}

	private void sendDashboardNotification(User messageTo, String url, String subject, String notificationMessage) {
		NotificationMessage message = new NotificationMessage();
		message.setCreatedBy(null);
		message.setCreatedDate(new Date());
		message.setMessage(notificationMessage);
		message.setNotificationType(NotificationType.EVENT_MESSAGE);
		message.setMessageTo(messageTo);
		message.setSubject(subject);
		message.setTenantId(messageTo.getTenantId());
		message.setUrl(url);
		dashboardNotificationService.save(message);
	}

	private String getTimeZoneByBuyerSettings(String tenantId, String timeZone) {
		try {
			if (StringUtils.checkString(tenantId).length() > 0) {
				String time = buyerSettingsService.getBuyerTimeZoneByTenantId(tenantId);
				if (time != null) {
					timeZone = time;
				}
			}
		} catch (Exception e) {
			LOG.error("Error while fetching buyer time zone :" + e.getMessage(), e);
		}
		return timeZone;
	}

	private String getErpNotifiactionEmailsByBuyerSettings(String tenantId) {
		try {
			if (StringUtils.checkString(tenantId).length() > 0) {
				LOG.info("fetching buyer setting-------------------");
				BuyerSettings buyerSettings = buyerSettingsService.getBuyerSettingsByTenantId(tenantId);
				if (buyerSettings != null) {
					return StringUtils.checkString(buyerSettings.getErpNotificationEmails());
				}
			}
		} catch (Exception e) {
			LOG.error("Error while fetching buyer setting :" + e.getMessage(), e);
		}
		return "";
	}

	// @formatter:off
	/**
	 * @api {get} /erpApi/awardList RFX Awards List
	 * @apiName RFX Awards List
	 * @apiGroup ERP
	 * @apiDescription API to fetch the RFX Award List containing the Awards that have not been sent to the caller yet
	 * @apiHeader {String} Content-Type Should be application/json
	 * @apiHeader {String} X-Authorization X-AUTH-KEY
	 * @apiHeaderExample {json} Header-Example: { "Content-Type":"application/json",
	 *                   "X-AUTH-KEY":"284c6798089445cf444516eea037feff1ad25dbe27ef7800a22dd2888ba6aa34" }
	 * @apiExample {curl} Example usage: curl -X GET -H
	 *             "X-AUTH-KEY:284c6798089445cf444516eea037feff1ad25dbe27ef7800a22dd2888ba6aa34" -H "Cache-Control:
	 *             no-cache" -H "Content-Type: application/json" "https://testtwo.procurehere.com/erpApi/awardList"
	 * @apiSuccessExample Success-Response: HTTP/1.1 200 OK { "awardList": [ { "eventId": "RFQ05205", "eventName":"TEST
	 *                    NAME", "eventReferenceNumber": "RFQ-002", "businessUnitName": "BU", "eventOwner": "TEST OWNER
	 *                    NAME", "awardRemark":"Test AwardRemark", "totalAwardPrice":"9999.00", "createdDate":
	 *                    "20/05/2019 10:24:15", "startDate": "20/05/2019 10:24:20", "endDate": "19/05/2019 23:24:59",
	 *                    "validityDays": 25, "currencyCode":"MYR" "deliveryDate": "21/05/2019 00:24:00", "paymentTerm":
	 *                    "Check", "deliveryAddress": { "title": "Office Address", "line1": "pune", "line2": "pune",
	 *                    "city": "pune", "zip": "21454", "state": "MLK", "country": "MY" }, "bqList": [ { "bqName":
	 *                    "Bill of Quantity 1", "bqItems": [ { "id": "ff8081816ad36fe0016ad39a6c470004", "level": 1,
	 *                    "order": 1, "itemName": "Item 1", "itemDescription": "Item 1 Description", "itemNo": "0010",
	 *                    "quantity": 12, "uom": "Centimeter", "totalAmount": 252, "unitPrice": 21, "itemCategory":
	 *                    "FIELD1", "bqItemCode": "FIELD2", "materialGroup": "MAT_G", "mfr_PartNO" :
	 *                    "CA-AC-BUS-NS-JP-01", "lnno": "", "supplier": { "companyName": "SupplierPrivaa1",
	 *                    "mobileNumber": "+45 126323", "companyContactNumber": "+94 21219326", "countryCode": "MY",
	 *                    "companyRegistrationNumber": "400", "fullName": "sudesha sanjay nikam", "communicationEmail":
	 *                    "sudesha.nikam@gmail.com", "designation": "software developer", "yearOfEstablished": 1985,
	 *                    "faxNumber": "+94 21219326", "taxNumber":"12345678", "line1": "pune", "line2": "PUNE", "city":
	 *                    "PUNE", "stateCode": "NSN" } }, { "id": "ff8081816ad36fe0016ad39a6c520006", "level": 1,
	 *                    "order": 2, "itemNo": "0020", "itemName": "Item 2", "itemDescription": "Item 2 Description",
	 *                    "quantity": 12, "uom": "Centimeter", "totalAmount": 252, "unitPrice": 21, "vendorCode":
	 *                    "1234", "itemCategory": "FIELD1", "bqItemCode": "FIELD2", "materialGroup": "MAT_G",
	 *                    "mfr_PartNO" : "CA-AC-BUS-NS-JP-01", "lnno": "", "supplier": { "companyName":
	 *                    "SupplierPrivaa1", "mobileNumber": "+45 126323", "companyContactNumber": "+94 21219326",
	 *                    "countryCode": "MY", "companyRegistrationNumber": "400", "fullName": "sudesha sanjay nikam",
	 *                    "communicationEmail": "sudesha.nikam@gmail.com", "designation": "software developer",
	 *                    "yearOfEstablished": 1985, "faxNumber": "+94 21219326" "taxNumber":"12345678", "line1":
	 *                    "pune", "line2": "PUNE", "city": "PUNE", "stateCode": "NSN" } } ] }, { "bqName": "Bill of
	 *                    Quantity 2", "bqItems": [ { "id": "ff8081816ad36fe0016ad39b2dc6000c", "level": 1, "order": 1,
	 *                    "itemNo": "0030", "itemName": "Item 1", "itemDescription": "Item Description", "quantity": 45,
	 *                    "totalAmount": 1564, "uom": "Fahrenheit", "unitPrice": 34, "vendorCode": "1234",
	 *                    "itemCategory": "FIELD1", "bqItemCode": "FIELD2", "materialGroup": "MAT_G", "mfr_PartNO" :
	 *                    "CA-AC-BUS-NS-JP-01", "lnno": "", "supplier": { "companyName": "SupplierPrivaa1",
	 *                    "mobileNumber": "+45 126323", "companyContactNumber": "+94 21219326", "countryCode": "MY",
	 *                    "companyRegistrationNumber": "400", "fullName": "sudesha sanjay nikam", "communicationEmail":
	 *                    "sudesha.nikam@gmail.com", "designation": "software developer", "yearOfEstablished": 1985,
	 *                    "faxNumber": "+94 21219326", "taxNumber":"12345678", "line1": "pune", "line2": "PUNE", "city":
	 *                    "PUNE", "stateCode": "NSN" } }, { "id": "ff8081816ad36fe0016ad39b2dcc000e", "level": 1,
	 *                    "order": 2, "itemName": "Item 2", "itemDescription": "Item 2 Description", "quantity": 23,
	 *                    "uom": "Foot/feet", "unitPrice": 32, "totalAmount": 736, "vendorCode": "1234", "itemCategory":
	 *                    "FIELD1", "bqItemCode": "FIELD2", "materialGroup": "MAT_G", "mfr_PartNO" :
	 *                    "CA-AC-BUS-NS-JP-01", "lnno": "", "supplier": { "companyName": "SupplierPrivaa1",
	 *                    "mobileNumber": "+45 126323", "companyContactNumber": "+94 21219326", "countryCode": "MY",
	 *                    "companyRegistrationNumber": "400", "fullName": "sudesha sanjay nikam", "communicationEmail":
	 *                    "sudesha.nikam@gmail.com", "designation": "software developer", "yearOfEstablished": 1985,
	 *                    "faxNumber": "+94 21219326", "taxNumber":"12345678", "line1": "pune", "line2": "PUNE", "city":
	 *                    "PUNE", "stateCode": "NSN" } } ] } ] } ] } @apiSuccess {Object} awardDetail Award from EPROC
	 * @apiSuccess (Award){String{64}} eventId EventId for RFX
	 * @apiSuccess (Award){String{250}} eventName Event Name for RFX
	 * @apiSuccess (Award){String{64}} eventReferenceNumber RFX Reference Number
	 * @apiSuccess (Award){String{64}} businessUnitName Business Unit Name
	 * @apiSuccess (Award){String{160}} eventOwner Event Owner Name
	 * @apiSuccess (Award){String{1000}} awardRemark Award Remark
	 * @apiSuccess (Award){String{64}} currencyCode Currency Code
	 * @apiSuccess (Award){BigDecimal{16,4}} totalAwardPrice Total Award Price
	 * @apiSuccess (Award){Date} createdDate RFX Created Date
	 * @apiSuccess (Award){Date} startDate RFX Start Date
	 * @apiSuccess (Award){Date} endDate RFX End Date
	 * @apiSuccess (Award){Date} deliveryDate RFX Delivery Date
	 * @apiSuccess (Award){Integer{3}} validityDays RFX Validity Days
	 * @apiSuccess (Award){String{550}} paymentTerm RFX Payment Term
	 * @apiSuccess (Award){Object} deliveryAddress RFX Delivery Address
	 * @apiSuccess (Delivery Address){String{128}} title Address Title
	 * @apiSuccess (Delivery Address){String{250}} line1 Address Line 1
	 * @apiSuccess (Delivery Address){String{250}} line2 Address Line 2
	 * @apiSuccess (Delivery Address){String{250}} city Address city
	 * @apiSuccess (Delivery Address){String{32}} zip Address Zip
	 * @apiSuccess (Delivery Address){String{64}} state Address State Code
	 * @apiSuccess (Delivery Address){String{64}} country Address Country Code
	 * @apiSuccess (Award){Object[]} bqList RFX Bill of Quantity(BQ List)
	 * @apiSuccess (BQ List){String{128}} bqName RFX Bill of Quantity Name
	 * @apiSuccess (BQ List){Object[]} bqItems RFX Bq Item
	 * @apiSuccess (Bq Item){Integer{2}} level Item level
	 * @apiSuccess (Bq Item){Integer{2}} order Item Order
	 * @apiSuccess (Bq Item){String{10}} itemNo Item Number
	 * @apiSuccess (Bq Item){String{250}} itemName Item Name
	 * @apiSuccess (Bq Item){String{1100}} itemDescription Item Description
	 * @apiSuccess (Bq Item){BigInteger{10}} quantity Item Quantity
	 * @apiSuccess (Bq Item){String{64}} uom Unit Of Measurement
	 * @apiSuccess (Bq Item){BigDecimal{16,4}} totalAmount Total Amount
	 * @apiSuccess (Bq Item){BigDecimal{16,4}} unitPrice Unit Price
	 * @apiSuccess (Bq Item){String{100}} itemCategory Item Category
	 * @apiSuccess (Bq Item){String{100}} bqItemCode Bq Item Code
	 * @apiSuccess (Bq Item){String{100}} materialGroup Material Group
	 * @apiSuccess (Bq Item){String{100}} mfr_PartNO MFR PartNO
	 * @apiSuccess (Bq Item){String{100}} itemNo Item No
	 * @apiSuccess (Bq Item){String{100}} lnno Line Item No
	 * @apiSuccess (Bq Item){String{100}} vendorCode Vendor Code
	 * @apiSuccess (Bq Item){Object} supplier Supplier
	 * @apiSuccess (Supplier){String{128}} companyName Supplier Company Name
	 * @apiSuccess (Supplier){String{16}} mobileNumber Mobile Number
	 * @apiSuccess (Supplier){String{16}} companyContactNumber Company Contact Number
	 * @apiSuccess (Supplier){String{64}} countryCode Country Code
	 * @apiSuccess (Supplier){String{128}} companyRegistrationNumber Company Registration Number
	 * @apiSuccess (Supplier){String{128}} fullName Supplier Full Name
	 * @apiSuccess (Supplier){String{128}} communicationEmail Communication Email
	 * @apiSuccess (Supplier){String{128}} designation Designation
	 * @apiSuccess (Supplier){String{4}} yearOfEstablished Year Of Established
	 * @apiSuccess (Supplier){String{16}} faxNumber FaxNumber
	 * @apiSuccess (Supplier){String{32}} taxNumber Supplier Tax Number
	 * @apiSuccess (Supplier){String{250}} line1 Address Line1
	 * @apiSuccess (Supplier){String{250}} line2 Address Line2
	 * @apiSuccess (Supplier){String{250}} city City
	 * @apiSuccess (Supplier){String{64}} stateCode State Code
	 * @apiError 401 Unauthorized access to protected resource.
	 * @apiError 500 Internal Server error
	 * @apiErrorExample HTTP/1.1 401 Unauthorized { "error" : "Auth key not valid" }
	 * @apiErrorExample HTTP/1.1 500 Internal server error { "error" : "Error while getting Award detail response in
	 *                  EPROC" }
	 */

	// @formatter:on
	@RequestMapping(value = "/awardList", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> getAwardDetails(@RequestHeader(name = "X-AUTH-KEY", required = true) String authKey) {
		LOG.info("RFQ Award Detail List Data Api called..");
		Map<String, Object> map = new HashMap<>();
		try {
			ObjectMapper mapperObj = new ObjectMapper();
			ErpSetup erpSetup = erpSetupDao.getErpConfigByAppId(authKey);
			if (erpSetup != null) {
				if (erpSetup.getAwardInterfaceTypePull()) {
					List<ErpAwardStaging> staggingList = erpAwardStaggingService.getStaggingData(erpSetup.getTenantId(), null);
					List<Object> list = new ArrayList<Object>();
					for (ErpAwardStaging erpAwardStaging : staggingList) {
						list.add(mapperObj.readValue(erpAwardStaging.getPayload(), Object.class));

					}
					map.put("awardList", list);
				} else {
					map.put("error", "Interface not active");
					HttpHeaders headers = new HttpHeaders();
					headers.add("error", "Interface not active");
					return new ResponseEntity<Map<String, Object>>(map, headers, HttpStatus.UNAUTHORIZED);
				}
			} else {
				map.put("error", "Auth key not valid");
				HttpHeaders headers = new HttpHeaders();
				headers.add("error", "Auth key not valid");
				return new ResponseEntity<Map<String, Object>>(map, headers, HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			LOG.error("Error while geting Award List " + e.getMessage(), e);
			map.put("error", "Error while geting Award List in EPROC :" + e.getMessage());
			HttpHeaders headers = new HttpHeaders();
			headers.add("error", "Error while geting Award List in EPROC :" + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(map, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

	// @formatter:off
	/**
	 * @api {post} /erpApi/awardDetail RFX Awards Detail
	 * @apiName RFX Awards Data
	 * @apiGroup ERP
	 * @apiDescription API to fetch the RFX Award individually based on Reference Number
	 * @apiHeader {String} Content-Type Should be application/json
	 * @apiHeader {String} X-Authorization X-AUTH-KEY
	 * @apiHeaderExample {json} Header-Example: { "Content-Type":"application/json",
	 *                   "X-AUTH-KEY":"284c6798089445cf444516eea037feff1ad25dbe27ef7800a22dd2888ba6aa34" }
	 * @apiParam referenceNo eventId
	 * @apiParamExample {json} Request-Example: { "referenceNo":"RFQ05205" }
	 * @apiExample {curl} Example usage: curl -X POST -H
	 *             "X-AUTH-KEY:284c6798089445cf444516eea037feff1ad25dbe27ef7800a22dd2888ba6aa34" -H "Cache-Control:
	 *             no-cache" -H "Content-Type: application/json" -d '{ "referenceNo" : "RFQ05205" }'
	 *             "https://testtwo.procurehere.com/erpApi/awardDetail"
	 * @apiSuccessExample Success-Response: HTTP/1.1 200 OK { "awardDetail": { "eventId": "RFQ05205", "eventName":"TEST
	 *                    NAME", "eventReferenceNumber": "RFQ-002", "businessUnitName": "BU", "eventOwner": "TEST OWNER
	 *                    NAME", "awardRemark":"Test AwardRemark", "totalAwardPrice":"9999.00", "createdDate":
	 *                    "20/05/2019 10:24:15", "startDate": "20/05/2019 10:24:20", "endDate": "19/05/2019 23:24:59",
	 *                    "validityDays": 25, "deliveryDate": "21/05/2019 00:24:00", "paymentTerm": "Check",
	 *                    "currencyCode":"MYR" "deliveryAddress": { "title": "Office Address", "line1": "pune", "line2":
	 *                    "pune", "city": "pune", "zip": "21454", "state": "MLK", "country": "MY" }, "bqList": [ {
	 *                    "bqName": "Bill of Quantity 1", "bqItems": [ { "id": "ff8081816ad36fe0016ad39a6c470004",
	 *                    "level": 1, "order": 1, "itemName": "Item 1", "itemDescription": "Item 1 Description",
	 *                    "itemNo": "0010", "quantity": 12, "uom": "Centimeter", "totalAmount": 252, "unitPrice": 21,
	 *                    "vendorCode": "1234", "itemCategory": "FIELD1", "bqItemCode": "FIELD2", "materialGroup":
	 *                    "MAT_G", "mfr_PartNO" : "CA-AC-BUS-NS-JP-01", "lnno": "", "supplier": { "companyName":
	 *                    "SupplierPrivaa1", "mobileNumber": "+45 126323", "companyContactNumber": "+94 21219326",
	 *                    "countryCode": "MY", "companyRegistrationNumber": "400", "fullName": "sudesha sanjay nikam",
	 *                    "communicationEmail": "sudesha.nikam@gmail.com", "designation": "software developer",
	 *                    "yearOfEstablished": 1985, "faxNumber": "+94 21219326", "taxNumber":"12345678", "line1":
	 *                    "pune", "line2": "PUNE", "city": "PUNE", "stateCode": "NSN" } }, { "id":
	 *                    "ff8081816ad36fe0016ad39a6c520006", "level": 1, "order": 2, "itemNo": "0020", "itemName":
	 *                    "Item 2", "itemDescription": "Item 2 Description", "quantity": 12, "uom": "Centimeter",
	 *                    "totalAmount": 252, "unitPrice": 21, "vendorCode": "1234", "itemCategory": "FIELD1",
	 *                    "bqItemCode": "FIELD2", "materialGroup": "MAT_G", "mfr_PartNO" : "CA-AC-BUS-NS-JP-01", "lnno":
	 *                    "", "supplier": { "companyName": "SupplierPrivaa1", "mobileNumber": "+45 126323",
	 *                    "companyContactNumber": "+94 21219326", "countryCode": "MY", "companyRegistrationNumber":
	 *                    "400", "fullName": "sudesha sanjay nikam", "communicationEmail": "sudesha.nikam@gmail.com",
	 *                    "designation": "software developer", "yearOfEstablished": 1985, "faxNumber": "+94 21219326",
	 *                    "taxNumber":"12345678", "line1": "pune", "line2": "PUNE", "city": "PUNE", "stateCode": "NSN" }
	 *                    } ] }, { "bqName": "Bill of Quantity 2", "bqItems": [ { "id":
	 *                    "ff8081816ad36fe0016ad39b2dc6000c", "level": 1, "order": 1, "itemNo": "0030", "itemName":
	 *                    "Item 1", "itemDescription": "Item Description", "quantity": 45, "totalAmount": 1564, "uom":
	 *                    "Fahrenheit", "unitPrice": 34, "vendorCode": "1234", "itemCategory": "FIELD1", "bqItemCode":
	 *                    "FIELD2", "materialGroup": "MAT_G", "mfr_PartNO" : "CA-AC-BUS-NS-JP-01", "lnno": "",
	 *                    "supplier": { "companyName": "SupplierPrivaa1", "mobileNumber": "+45 126323",
	 *                    "companyContactNumber": "+94 21219326", "countryCode": "MY", "companyRegistrationNumber":
	 *                    "400", "fullName": "sudesha sanjay nikam", "communicationEmail": "sudesha.nikam@gmail.com",
	 *                    "designation": "software developer", "yearOfEstablished": 1985, "faxNumber": "+94 21219326",
	 *                    "taxNumber":"12345678", "line1": "pune", "line2": "PUNE", "city": "PUNE", "stateCode": "NSN" }
	 *                    }, { "id": "ff8081816ad36fe0016ad39b2dcc000e", "level": 1, "order": 2, "itemName": "Item 2",
	 *                    "itemDescription": "Item 2 Description", "quantity": 23, "uom": "Foot/feet", "unitPrice": 32,
	 *                    "totalAmount": 736, "vendorCode": "1234", "itemCategory": "FIELD1", "bqItemCode": "FIELD2",
	 *                    "materialGroup": "MAT_G", "lnno": "", "supplier": { "companyName": "SupplierPrivaa1",
	 *                    "mobileNumber": "+45 126323", "companyContactNumber": "+94 21219326", "countryCode": "MY",
	 *                    "companyRegistrationNumber": "400", "fullName": "sudesha sanjay nikam", "communicationEmail":
	 *                    "sudesha.nikam@gmail.com", "designation": "software developer", "yearOfEstablished": 1985,
	 *                    "faxNumber": "+94 21219326", "taxNumber":"12345678", "line1": "pune", "line2": "PUNE", "city":
	 *                    "PUNE", "stateCode": "NSN" } } ] } ] } }
	 * @apiSuccess {Object} awardDetail Award from EPROC
	 * @apiSuccess (Award){String{64}} eventId EventId for RFX
	 * @apiSuccess (Award){String{250}} eventName Event Name for RFX
	 * @apiSuccess (Award){String{64}} eventReferenceNumber RFX Reference Number
	 * @apiSuccess (Award){String{64}} businessUnitName Business Unit Name
	 * @apiSuccess (Award){String{160}} eventOwner Event Owner Name
	 * @apiSuccess (Award){String{64}} currencyCode Currency Code
	 * @apiSuccess (Award){String{1000}} awardRemark Award Remark
	 * @apiSuccess (Award){BigDecimal{16,4}} totalAwardPrice Total Award Price
	 * @apiSuccess (Award){Date} createdDate RFX Created Date
	 * @apiSuccess (Award){Date} startDate RFX Start Date
	 * @apiSuccess (Award){Date} endDate RFX End Date
	 * @apiSuccess (Award){Date} deliveryDate RFX Delivery Date
	 * @apiSuccess (Award){Integer{3}} validityDays RFX Validity Days
	 * @apiSuccess (Award){String{550}} paymentTerm RFX Payment Term
	 * @apiSuccess (Award){Object} deliveryAddress RFX Delivery Address
	 * @apiSuccess (Delivery Address){String{128}} title Address Title
	 * @apiSuccess (Delivery Address){String{250}} line1 Address Line 1
	 * @apiSuccess (Delivery Address){String{250}} line2 Address Line 2
	 * @apiSuccess (Delivery Address){String{250}} city Address city
	 * @apiSuccess (Delivery Address){String{32}} zip Address Zip
	 * @apiSuccess (Delivery Address){String{64}} state Address State Code
	 * @apiSuccess (Delivery Address){String{64}} country Address Country Code
	 * @apiSuccess (Award){Object[]} bqList RFX Bill of Quantity(BQ List)
	 * @apiSuccess (BQ List){String{128}} bqName RFX Bill of Quantity Name
	 * @apiSuccess (BQ List){Object[]} bqItems RFX Bq Item
	 * @apiSuccess (Bq Item){Integer{2}} level Item level
	 * @apiSuccess (Bq Item){Integer{2}} order Item Order
	 * @apiSuccess (Bq Item){String{10}} itemNo Item Number
	 * @apiSuccess (Bq Item){String{250}} itemName Item Name
	 * @apiSuccess (Bq Item){String{1100}} itemDescription Item Description
	 * @apiSuccess (Bq Item){BigInteger{10}} quantity Item Quantity
	 * @apiSuccess (Bq Item){String{64}} uom Unit Of Measurement
	 * @apiSuccess (Bq Item){BigDecimal{16,4}} totalAmount Total Amount
	 * @apiSuccess (Bq Item){BigDecimal{16,4}} unitPrice Unit Price
	 * @apiSuccess (Bq Item){String{100}} itemCategory Item Category
	 * @apiSuccess (Bq Item){String{100}} bqItemCode Bq Item Code
	 * @apiSuccess (Bq Item){String{100}} materialGroup Material Group
	 * @apiSuccess (Bq Item){String{100}} mfr_PartNO MFR PartNO
	 * @apiSuccess (Bq Item){String{100}} itemNo Item No
	 * @apiSuccess (Bq Item){String{100}} lnno Line Item No
	 * @apiSuccess (Bq Item){String{100}} vendorCode Vendor Code
	 * @apiSuccess (Bq Item){Object} supplier Supplier
	 * @apiSuccess (Supplier){String{128}} companyName Supplier Company Name
	 * @apiSuccess (Supplier){String{16}} mobileNumber Mobile Number
	 * @apiSuccess (Supplier){String{16}} companyContactNumber Company Contact Number
	 * @apiSuccess (Supplier){String{64}} countryCode Country Code
	 * @apiSuccess (Supplier){String{128}} companyRegistrationNumber Company Registration Number
	 * @apiSuccess (Supplier){String{128}} fullName Supplier Full Name
	 * @apiSuccess (Supplier){String{128}} communicationEmail Communication Email
	 * @apiSuccess (Supplier){String{128}} designation Designation
	 * @apiSuccess (Supplier){String{4}} yearOfEstablished Year Of Established
	 * @apiSuccess (Supplier){String{16}} faxNumber FaxNumber
	 * @apiSuccess (Supplier){String{32}} taxNumber Supplier Tax Number
	 * @apiSuccess (Supplier){String{250}} line1 Address Line1
	 * @apiSuccess (Supplier){String{250}} line2 Address Line2
	 * @apiSuccess (Supplier){String{250}} city City
	 * @apiSuccess (Supplier){String{64}} stateCode State Code
	 * @apiError 401 Unauthorized access to protected resource.
	 * @apiError 500 Internal Server error
	 * @apiErrorExample HTTP/1.1 401 Unauthorized { "error" : "Auth key not valid" }
	 * @apiErrorExample HTTP/1.1 500 Internal server error { "error" : "Error while getting Award detail in EPROC" }
	 */

	// @formatter:on

	@RequestMapping(value = "/awardDetail", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> getAwardDetail(@RequestHeader(name = "X-AUTH-KEY", required = true) String authKey, @RequestBody(required = true) ErpRequestPojo erpRequest) {
		LOG.info("RFQ Award Detail Data Api called.." + erpRequest);
		Map<String, Object> map = new HashMap<>();
		try {
			ObjectMapper mapperObj = new ObjectMapper();
			ErpSetup erpSetup = erpSetupDao.getErpConfigByAppId(authKey);
			if (erpSetup != null) {
				if (erpSetup.getAwardInterfaceTypePull()) {
					List<ErpAwardStaging> staggingList = erpAwardStaggingService.getStaggingData(erpSetup.getTenantId(), erpRequest.getReferenceNo());
					for (ErpAwardStaging erpAwardStaging : staggingList) {
						map.put("awardDetail", mapperObj.readValue(erpAwardStaging.getPayload(), Object.class));
					}
				} else {
					map.put("error", "Interface not active");
					HttpHeaders headers = new HttpHeaders();
					headers.add("error", "Interface not active");
					return new ResponseEntity<Map<String, Object>>(map, headers, HttpStatus.UNAUTHORIZED);
				}
			} else {
				map.put("error", "Auth key not valid");
				HttpHeaders headers = new HttpHeaders();
				headers.add("error", "Auth key not valid");
				return new ResponseEntity<Map<String, Object>>(map, headers, HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			LOG.error("Error while getting Award detail procurehere :" + e.getMessage(), e);
			map.put("error", "Error while getting Award detail in EPROC :" + e.getMessage());
			HttpHeaders headers = new HttpHeaders();
			headers.add("error", "Error while getting Award detail in EPROC :" + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(map, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

	// @formatter:off
	/**
	 * @api {post} /erpApi/markAwardAsProcessed Mark Award As Processed
	 * @apiName Mark Award As Processed
	 * @apiGroup ERP
	 * @apiDescription API to Mark Award As Processed individually based on Reference Number
	 * @apiHeader {String} Content-Type Should be application/json
	 * @apiHeader {String} X-Authorization X-AUTH-KEY
	 * @apiHeaderExample {json} Header-Example: { "Content-Type":"application/json",
	 *                   "X-AUTH-KEY":"284c6798089445cf444516eea037feff1ad25dbe27ef7800a22dd2888ba6aa34" }
	 * @apiParam referenceNo eventId
	 * @apiParamExample {json} Request-Example: { "referenceNo":"RFQ05205" }
	 * @apiExample {curl} Example usage: curl -X POST -H
	 *             "X-AUTH-KEY:284c6798089445cf444516eea037feff1ad25dbe27ef7800a22dd2888ba6aa34" -H "Cache-Control:
	 *             no-cache" -H "Content-Type: application/json" -d '{ "referenceNo" : "RFQ05205" }'
	 *             "https://testtwo.procurehere.com/erpApi/markAwardAsProcessed"
	 * @apiSuccessExample Success-Response: HTTP/1.1 200 OK { "success" : "Award marked as processed successfully" }
	 * @apiSuccess success Success Message
	 * @apiError 401 Unauthorized access to protected resource.
	 * @apiError 500 Internal Server error
	 * @apiErrorExample HTTP/1.1 401 Unauthorized { "error" : "Auth key not valid" }
	 * @apiErrorExample HTTP/1.1 500 Internal server error { "error" : "Error while processing request" }
	 */
	// @formatter:on
	@RequestMapping(value = "/markAwardAsProcessed", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> markAwardAsProcessed(@RequestHeader(name = "X-AUTH-KEY", required = true) String authKey, @RequestBody(required = true) ErpRequestPojo erpRequest) {
		LOG.info("RFQ Award Detail Data Api called.." + erpRequest);
		Map<String, Object> map = new HashMap<>();
		HttpHeaders headers = new HttpHeaders();
		try {
			ErpSetup erpSetup = erpSetupDao.getErpConfigByAppId(authKey);
			if (erpSetup != null) {
				if (erpSetup.getAwardInterfaceTypePull()) {
					List<ErpAwardStaging> staggingList = erpAwardStaggingService.getStaggingData(erpSetup.getTenantId(), erpRequest.getReferenceNo());
					if (CollectionUtil.isEmpty(staggingList)) {
						throw new ApplicationException("Invalid reference number");
					}
					for (ErpAwardStaging erpAwardStaging : staggingList) {
						erpAwardStaging.setSentFlag(true);
						erpAwardStaging.setSentDate(new Date());
						erpAwardStaggingService.update(erpAwardStaging);

						map.put("success", "Award marked as processed successfully");
						headers.add("success", "Award marked as processed successfully");

					}
				} else {
					map.put("error", "Interface not active");
					headers.add("error", "Interface not active");
					return new ResponseEntity<Map<String, Object>>(map, headers, HttpStatus.UNAUTHORIZED);
				}
			} else {
				map.put("error", "Auth key not valid");
				headers.add("error", "Auth key not valid");
				return new ResponseEntity<Map<String, Object>>(map, headers, HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			LOG.error("Error while processing request :" + e.getMessage(), e);
			map.put("error", "Error while processing request :" + e.getMessage());
			headers.add("error", "Error while processing request:" + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(map, headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

	@RequestMapping(value = "/uom", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> syncUomData(@RequestHeader(name = "X-AUTH-KEY", required = true) String authkey, @RequestBody UomPojo uomPojo) {
		LOG.info("UOM Response Data Api called...");
		Map<String, Object> map = new HashMap<>();
		try {

			ErpSetup erpConfig = erpSetupDao.getErpConfigWithTepmlateByAppId(authkey);
			if (erpConfig == null) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.erpconfig.empty", new Object[] {}, Global.LOCALE));
			}
			if (Boolean.FALSE == erpConfig.getEnableUomApi()) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.erpconfig.notallowed", new Object[] {}, Global.LOCALE));
			}

			Buyer buyer = buyerService.findBuyerById(erpConfig.getTenantId());
			if (buyer == null) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.buyer.empty", new Object[] {}, Global.LOCALE));
			}

			User user = userService.getUserByLoginIdNoTouch(buyer.getLoginEmail());
			if (user == null) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.user.empty", new Object[] {}, Global.LOCALE));
			}

			Uom uom = null;

			if (StringUtils.checkString(uomPojo.getUom()).length() == 0) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.uom.notEmpty", new Object[] {}, Global.LOCALE));
			}
			if (StringUtils.checkString(uomPojo.getUomDescription()).length() == 0) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.uom.uomDesc.notEmpty", new Object[] {}, Global.LOCALE));
			}

			uom = uomService.getUombyCode(uomPojo.getUom(), buyer.getId());

			switch (uomPojo.getOperation()) {
			case C: { // CREATE
				if (uom != null) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.uom.exists", new Object[] { uomPojo.getUom() }, Global.LOCALE));
				} else {
					uom = new Uom();
					createUom(uomPojo, buyer, user, uom);
				}
				map.put("errorcode", 0);
				map.put("errorMessage", "SUCCESS");
				break;
			}
			case D: { // DELETE
				if (uom != null) {
					uom = uomService.getUombyCode(uomPojo.getUom(), buyer.getId());
					uom.setStatus(Status.INACTIVE);
					uom.setModifiedBy(user);
					uom.setModifiedDate(new Date());
					uomService.updateUom(uom, Boolean.TRUE);

					map.put("errorcode", 0);
					map.put("errorMessage", "SUCCESS");
				} else {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.uom.notExists", new Object[] {}, Global.LOCALE));
				}
				break;
			}
			case U: { // UPDATE
				if (uom != null) {
					updateUom(uomPojo, user, uom);

					map.put("errorcode", 0);
					map.put("errorMessage", "SUCCESS");
				} else {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.uom.notExists", new Object[] {}, Global.LOCALE));
				}
				break;
			}
			case CU: { // CREATE OR UPDATE
				if (uom != null) {
					updateUom(uomPojo, user, uom);
				} else {
					uom = new Uom();
					createUom(uomPojo, buyer, user, uom);
				}

				map.put("errorcode", 0);
				map.put("errorMessage", "SUCCESS");
				break;
			}
			default:
				break;
			}
		} catch (Exception e) {
			LOG.error("Error storing UOM : " + e.getMessage(), e);
			map.put("errorCode", 1001);
			map.put("errorMessage", messageSource.getMessage("erpIntegration.uom.error", new Object[] { e.getMessage() }, Global.LOCALE));
			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		}

		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

	private void updateUom(UomPojo uomPojo, User user, Uom uom) {
		uom.setUomDescription(uomPojo.getUomDescription());
		uom.setStatus(Status.ACTIVE);
		uom.setModifiedBy(user);
		uom.setModifiedDate(new Date());
		uomService.updateUom(uom, Boolean.TRUE);
	}

	private void createUom(UomPojo uomPojo, Buyer buyer, User user, Uom uom) {
		uom.setUom(uomPojo.getUom());
		uom.setUomDescription(uomPojo.getUomDescription());
		uom.setBuyer(buyer);
		uom.setStatus(Status.ACTIVE);
		uom.setCreatedDate(new Date());
		uom.setCreatedBy(user);
		uomService.createUom(uom, Boolean.TRUE);
	}

	@RequestMapping(value = "/productCategory", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> syncProductCategory(@RequestHeader(name = "X-AUTH-KEY", required = true) String authkey, @RequestBody ProductCategoryPojo productCategoryPojo) {
		LOG.info("Product Category Data Api called...");
		Map<String, Object> map = new HashMap<>();
		try {

			ErpSetup erpConfig = erpSetupDao.getErpConfigWithTepmlateByAppId(authkey);
			if (erpConfig == null) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.erpconfig.empty", new Object[] {}, Global.LOCALE));
			}
			if (Boolean.FALSE == erpConfig.getEnableProductCategoryApi()) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.erpconfig.notallowed", new Object[] {}, Global.LOCALE));
			}
			Buyer buyer = buyerService.findBuyerById(erpConfig.getTenantId());
			if (buyer == null) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.buyer.empty", new Object[] {}, Global.LOCALE));
			}

			User user = userService.getUserByLoginIdNoTouch(buyer.getLoginEmail());
			if (user == null) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.user.empty", new Object[] {}, Global.LOCALE));
			}

			if (StringUtils.checkString(productCategoryPojo.getProductCode()).length() == 0) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.productCode.notEmpty", new Object[] {}, Global.LOCALE));
			}
			if (StringUtils.checkString(productCategoryPojo.getProductName()).length() == 0) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.productName.notEmpty", new Object[] {}, Global.LOCALE));
			}

			ProductCategory productCatgory = productCategoryMaintenanceService.findProductCategoryByCode(productCategoryPojo.getProductCode(), buyer.getId());

			switch (productCategoryPojo.getOperation()) {
			case C: { // CREATE
				if (productCatgory != null) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.productCategory.exists", new Object[] { productCategoryPojo.getProductCode() }, Global.LOCALE));
				} else {
					productCatgory = new ProductCategory();
					createProductCategory(productCategoryPojo, buyer, user, productCatgory);
				}
				map.put("errorcode", 0);
				map.put("errorMessage", "SUCCESS");
				break;
			}
			case D: { // DELETE
				if (productCatgory != null) {
					productCatgory.setStatus(Status.INACTIVE);
					productCatgory.setModifiedBy(user);
					productCatgory.setModifiedDate(new Date());
					productCategoryMaintenanceService.updateProductCategory(productCatgory, Boolean.TRUE);

					map.put("errorcode", 0);
					map.put("errorMessage", "SUCCESS");
				} else {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.productCategory.notExists", new Object[] { productCategoryPojo.getProductCode() }, Global.LOCALE));
				}
				break;
			}
			case U: { // UPDATE
				if (productCatgory != null) {
					updateProductCategory(productCategoryPojo, user, productCatgory);

					map.put("errorcode", 0);
					map.put("errorMessage", "SUCCESS");
				} else {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.productCategory.notExists", new Object[] { productCategoryPojo.getProductCode() }, Global.LOCALE));
				}
				break;
			}
			case CU: { // CREATE OR UPDATE
				if (productCatgory != null) {
					updateProductCategory(productCategoryPojo, user, productCatgory);
				} else {
					productCatgory = new ProductCategory();
					createProductCategory(productCategoryPojo, buyer, user, productCatgory);
				}
				map.put("errorcode", 0);
				map.put("errorMessage", "SUCCESS");
				break;
			}
			default:
				break;
			}
		} catch (Exception e) {
			LOG.error("Error storing Product category : " + e.getMessage(), e);
			map.put("errorCode", 1001);
			map.put("errorMessage", messageSource.getMessage("erpIntegration.productCategory.error", new Object[] { e.getMessage() }, Global.LOCALE));
			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		}

		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

	private void updateProductCategory(ProductCategoryPojo productCategoryPojo, User user, ProductCategory productCatgory) {
		productCatgory.setProductName(productCategoryPojo.getProductName());
		productCatgory.setModifiedBy(user);
		productCatgory.setModifiedDate(new Date());
		productCatgory.setStatus(Status.ACTIVE);
		productCategoryMaintenanceService.updateProductCategory(productCatgory, Boolean.TRUE);
	}

	private void createProductCategory(ProductCategoryPojo productCategoryPojo, Buyer buyer, User user, ProductCategory productCatgory) {
		productCatgory.setProductCode(productCategoryPojo.getProductCode());
		productCatgory.setProductName(productCategoryPojo.getProductName());
		productCatgory.setCreatedDate(new Date());
		productCatgory.setCreatedBy(user);
		productCatgory.setBuyer(buyer);
		productCatgory.setStatus(Status.ACTIVE);
		productCategoryMaintenanceService.createProductCategory(productCatgory, Boolean.TRUE);
	}

	@RequestMapping(value = "/productItem", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> syncProductItem(@RequestHeader(name = "X-AUTH-KEY", required = true) String authkey, @RequestBody ProductItemPojo productItemPojo) {
		LOG.info("Product Item Data Api called..." + productItemPojo);
		Map<String, Object> map = new HashMap<>();
		try {

			ErpSetup erpConfig = erpSetupDao.getErpConfigWithTepmlateByAppId(authkey);
			if (erpConfig == null) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.erpconfig.empty", new Object[] {}, Global.LOCALE));
			}
			if (Boolean.FALSE == erpConfig.getEnableProductItemApi()) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.erpconfig.notallowed", new Object[] {}, Global.LOCALE));
			}
			Buyer buyer = buyerService.findBuyerById(erpConfig.getTenantId());
			if (buyer == null) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.buyer.empty", new Object[] {}, Global.LOCALE));
			}

			User user = userService.getUserByLoginIdNoTouch(buyer.getLoginEmail());
			if (user == null) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.user.empty", new Object[] {}, Global.LOCALE));
			}

			ProductCategory productCategory = null;
			FavouriteSupplier favouriteSupplier = null;
			Uom uom = null;

			if (StringUtils.checkString(productItemPojo.getItemCode()).length() == 0) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.productCode.notEmpty", new Object[] {}, Global.LOCALE));
			}

			if (StringUtils.checkString(productItemPojo.getItemName()).length() == 0) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.productName.notEmpty", new Object[] {}, Global.LOCALE));
			}

			if (StringUtils.checkString(productItemPojo.getItemCategory()).length() > 0) {
				productCategory = productCategoryMaintenanceService.getProductCategoryAndTenantId(productItemPojo.getItemCategory(), buyer.getId());
				if (productCategory == null) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.productCategory.notExists", new Object[] { productItemPojo.getItemCategory() }, Global.LOCALE));
				}
			} else {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.productCategory.notEmpty", new Object[] {}, Global.LOCALE));
			}

			if (StringUtils.checkString(productItemPojo.getUom()).length() > 0) {
				uom = uomService.getUomByUomAndTenantId(productItemPojo.getUom(), buyer.getId());
				if (uom == null) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.uom.notExists", new Object[] {}, Global.LOCALE));
				}
			} else {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.uom.notEmpty", new Object[] {}, Global.LOCALE));
			}

			if (StringUtils.checkString(productItemPojo.getVendorCode()).length() > 0) {
				favouriteSupplier = favoriteSupplierService.getFavouriteSupplierByVendorCode(StringUtils.checkString(productItemPojo.getVendorCode()), buyer.getId());
				if (favouriteSupplier == null) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.supplier.notExists", new Object[] { productItemPojo.getVendorCode() }, Global.LOCALE));
				}
			}

			if (Boolean.TRUE == productItemPojo.getContractItem()) {
				if (productItemPojo.getValidityDate() == null) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.validity.notEmpty", new Object[] {}, Global.LOCALE));
				}
			}
			productItemPojo.setInterfaceCode(productItemPojo.getItemCode());

			List<ProductItemPojo> requestList = new ArrayList<ProductItemPojo>();
			
			// If there are multiple Purchase Group codes present, then split them into multiple product items

			if (CollectionUtil.isNotEmpty(productItemPojo.getPurchaseGroups())) {
				for(PurchaseGroupsPojo pgCode : productItemPojo.getPurchaseGroups()) {
					LOG.info("Group Code : " + pgCode.getPurchaseGroupCodes());
					ProductItemPojo pojo = productItemPojo.createCopy();
					pojo.setItemCode(pojo.getItemCode() + pgCode.getPurchaseGroupCodes());
					pojo.setPurchaseGroupCode(pgCode.getPurchaseGroupCodes());
					requestList.add(pojo);
				}
			} else {
				if(StringUtils.checkString(productItemPojo.getPurchaseGroupCode()).length() > 0) {
					productItemPojo.setItemCode(productItemPojo.getItemCode() + StringUtils.checkString(productItemPojo.getPurchaseGroupCode()));
				}
				requestList.add(productItemPojo);
			}
			
			for(ProductItemPojo productItemRequest : requestList) {
				LOG.info("ProductItemRequest : " + productItemRequest.toString());
				// Ignore ProductType for item sync
				ProductItem productItem = productListMaintenanceService.findProductItemByCode(productItemRequest.getItemCode(), buyer.getId(), null);

				switch (productItemRequest.getOperation()) {
				case C: { // CREATE
					if (productItem != null) {
						throw new ApplicationException(messageSource.getMessage("erpIntegration.productItem.exists", new Object[] { productItemRequest.getItemCode() }, Global.LOCALE));
					} else {
						productItem = new ProductItem();
						createProductItem(productItem, productItemRequest, buyer, user, productCategory, favouriteSupplier, uom);
					}

					map.put("errorcode", 0);
					map.put("errorMessage", "SUCCESS");
					break;
				}
				case D: { // DELETE
					if (productItem != null) {
						productItem.setStatus(Status.INACTIVE);
						productItem.setModifiedBy(user);
						productItem.setModifiedDate(new Date());
						productListMaintenanceService.updateProductItem(productItem, Boolean.TRUE);

						map.put("errorcode", 0);
						map.put("errorMessage", "SUCCESS");
					} else {
						throw new ApplicationException(messageSource.getMessage("erpIntegration.productItem.notExists", new Object[] { productItemRequest.getItemCode() }, Global.LOCALE));
					}
					break;
				}
				case U: { // UPDATE
					if (productItem != null) {
						updateProductItem(productItem, productItemRequest, user, productCategory, favouriteSupplier, uom);

						map.put("errorcode", 0);
						map.put("errorMessage", "SUCCESS");
					} else {
						throw new ApplicationException(messageSource.getMessage("erpIntegration.productItem.notExists", new Object[] { productItemRequest.getItemCode() }, Global.LOCALE));
					}
					break;
				}
				case CU: { // CREATE OR UPDATE
					if (productItem != null) {
						updateProductItem(productItem, productItemRequest, user, productCategory, favouriteSupplier, uom);
					} else {
						productItem = new ProductItem();
						createProductItem(productItem, productItemRequest, buyer, user, productCategory, favouriteSupplier, uom);
					}
					map.put("errorcode", 0);
					map.put("errorMessage", "SUCCESS");
					break;
				}
				default:
					break;
				}
			}


		} catch (Exception e) {
			LOG.error("Error Processing Product Item Request : " + e.getMessage(), e);
			map.put("errorCode", 1001);
			map.put("errorMessage", messageSource.getMessage("erpIntegration.productItem.error", new Object[] { e.getMessage() }, Global.LOCALE));
			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		}
		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

	private void updateProductItem(ProductItem productItem, ProductItemPojo productItemPojo, User user, ProductCategory productCategory, FavouriteSupplier favouriteSupplier, Uom uom) {
		productItem.setProductName(productItemPojo.getItemName());
		productItem.setTax(productItemPojo.getTax());
		productItem.setContractItem(Boolean.TRUE == productItemPojo.getContractItem() ? true : false);
		productItem.setUnitPrice(productItemPojo.getUnitPrice());
		productItem.setUom(uom);
		productItem.setProductCategory(productCategory);
		productItem.setFavoriteSupplier(favouriteSupplier);
		productItem.setValidityDate(productItemPojo.getValidityDate());
		productItem.setRemarks(productItemPojo.getRemarks());
		productItem.setBrand(productItemPojo.getBrand());
		productItem.setGlCode(productItemPojo.getGlCode());
		productItem.setPurchaseGroupCode(productItemPojo.getPurchaseGroupCode());
		// productItem.setContractReferenceNumber(productItemPojo.getContractReferenceNumber());
		productItem.setStatus(Status.ACTIVE);
		productItem.setProductItemType(productItemPojo.getItemType());
		productItem.setModifiedBy(user);
		productItem.setModifiedDate(new Date());
		productListMaintenanceService.updateProductItem(productItem, Boolean.TRUE);
	}

	private void createProductItem(ProductItem productItem, ProductItemPojo productItemPojo, Buyer buyer, User user, ProductCategory productCategory, FavouriteSupplier favouriteSupplier, Uom uom) {
		productItem.setInterfaceCode(productItemPojo.getInterfaceCode());
		productItem.setProductCode(productItemPojo.getItemCode());
		productItem.setProductName(productItemPojo.getItemName());
		productItem.setTax(productItemPojo.getTax());
		productItem.setContractItem(Boolean.TRUE == productItemPojo.getContractItem() ? true : false);
		productItem.setUnitPrice(productItemPojo.getUnitPrice());
		productItem.setUom(uom);
		productItem.setProductCategory(productCategory);
		productItem.setFavoriteSupplier(favouriteSupplier);
		productItem.setValidityDate(productItemPojo.getValidityDate());
		productItem.setRemarks(productItemPojo.getRemarks());
		productItem.setBrand(productItemPojo.getBrand());
		productItem.setGlCode(productItemPojo.getGlCode());
		productItem.setPurchaseGroupCode(productItemPojo.getPurchaseGroupCode());
		// productItem.setContractReferenceNumber(productItemPojo.getContractReferenceNumber());
		productItem.setStatus(Status.ACTIVE);
		productItem.setProductItemType(productItemPojo.getItemType());
		productItem.setBuyer(buyer);
		productItem.setCreatedDate(new Date());
		productItem.setCreatedBy(user);
		productListMaintenanceService.createProductItem(productItem, Boolean.TRUE);
	}

	@RequestMapping(value = "/productContract", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> syncItemContracts(@RequestHeader(name = "X-AUTH-KEY", required = true) String authkey, @RequestBody ProductContractPojo productContractPojo) {
		LOG.info("Product contract Data Api called..." + productContractPojo);
		Map<String, Object> map = new HashMap<>();
		try {
			ErpSetup erpConfig = erpSetupDao.getErpConfigWithTepmlateByAppId(authkey);
			if (erpConfig == null) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.erpconfig.empty", new Object[] {}, Global.LOCALE));
			}

			if (Boolean.FALSE == erpConfig.getEnableContractApi()) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.erpconfig.notallowed", new Object[] {}, Global.LOCALE));
			}

			Buyer buyer = buyerService.findBuyerById(erpConfig.getTenantId());
			if (buyer == null) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.buyer.empty", new Object[] {}, Global.LOCALE));
			}

			User user = userService.getUserByLoginIdNoTouch(buyer.getLoginEmail());
			if (user == null) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.user.empty", new Object[] {}, Global.LOCALE));
			}

			BusinessUnit businessUnit = businessUnitService.findBusinessUnitForTenantByUnitCode(erpConfig.getTenantId(), productContractPojo.getBusinessUnit());
			if (businessUnit == null) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.unitCode.invalid", new Object[] { productContractPojo.getBusinessUnit() }, Global.LOCALE));
			}

			if (StringUtils.checkString(productContractPojo.getContractReferenceNumber()).length() == 0) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.refrence.notEmpty", new Object[] {}, Global.LOCALE));
			}
			if (StringUtils.checkString(productContractPojo.getGroupCode()).length() == 0) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.groupCode.notEmpty", new Object[] {}, Global.LOCALE));
			}
			if (productContractPojo.getContractEndDate() == null) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.endDate.notEmpty", new Object[] {}, Global.LOCALE));
			}
			if (productContractPojo.getContractStartDate() == null) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.startDate.notEmpty", new Object[] {}, Global.LOCALE));
			}

			if (productContractPojo.getOperation() == null) {
				throw new ApplicationException("Operation is null");
			}

			FavouriteSupplier favouriteSupplier = null;
			if (StringUtils.checkString(productContractPojo.getVendorCode()).length() > 0) {
				favouriteSupplier = favoriteSupplierService.getFavouriteSupplierByVendorCode(StringUtils.checkString(productContractPojo.getVendorCode()), buyer.getId());
				if (favouriteSupplier == null) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.supplier.notExists", new Object[] { productContractPojo.getVendorCode() }, Global.LOCALE));
				}
			}

			ProductContract productContract = productContractService.findProductContractByReferenceNumber(productContractPojo.getContractReferenceNumber(), buyer.getId());

			switch (productContractPojo.getOperation()) {
			case C: { // CREATE
				if (productContract != null) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.productContract.exists", new Object[] { productContractPojo.getContractReferenceNumber() }, Global.LOCALE));
				} else {
					productContract = new ProductContract();
					saveProductContract(productContract, productContractPojo, buyer, user, favouriteSupplier, businessUnit);
				}
				map.put("errorcode", 0);
				map.put("errorMessage", "SUCCESS");
				break;
			}
			case D: { // DELETE
				if (productContract != null) {
					productContract.setStatus(Status.INACTIVE);
					productContract.setModifiedBy(user);
					productContract.setModifiedDate(new Date());
					productContractService.update(productContract);
					map.put("errorcode", 0);
					map.put("errorMessage", "SUCCESS");
				} else {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.productContract.notExists", new Object[] { productContractPojo.getContractReferenceNumber() }, Global.LOCALE));
				}
				break;
			}
			case U: { // UPDATE
				if (productContract != null) {
					updateProductContract(productContract, productContractPojo, user, buyer, favouriteSupplier, businessUnit);

					map.put("errorcode", 0);
					map.put("errorMessage", "SUCCESS");
				} else {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.productContract.notExists", new Object[] { productContractPojo.getContractReferenceNumber() }, Global.LOCALE));
				}
				break;
			}
			case CU: { // CREATE OR UPDATE
				if (productContract != null) {
					updateProductContract(productContract, productContractPojo, user, buyer, favouriteSupplier, businessUnit);
				} else {
					productContract = new ProductContract();
					saveProductContract(productContract, productContractPojo, buyer, user, favouriteSupplier, businessUnit);
				}
				map.put("errorcode", 0);
				map.put("errorMessage", "SUCCESS");
				break;
			}
			default:
				break;
			}
		} catch (Exception e) {
			LOG.error("Error Processing Product Contract Request : " + e.getMessage(), e);
			map.put("errorCode", 1001);
			map.put("errorMessage", messageSource.getMessage("erpIntegration.productItem.error", new Object[] { e.getMessage() }, Global.LOCALE));
			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		}
		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

	private void updateProductContract(ProductContract productContract, ProductContractPojo productContractPojo, User user, Buyer buyer, FavouriteSupplier favouriteSupplier, BusinessUnit businessUnit) throws ApplicationException {
		productContract.setBusinessUnit(businessUnit);
		productContract.setContractReferenceNumber(productContractPojo.getContractReferenceNumber());
		productContract.setGroupCode(productContractPojo.getGroupCode());
		productContract.setContractStartDate(productContractPojo.getContractStartDate());
		productContract.setContractEndDate(productContractPojo.getContractEndDate());
		productContract.setContractValue(productContractPojo.getContractValue());
		productContract.setSupplier(favouriteSupplier);
		productContract.setStatus(productContractPojo.getStatus());
		productContract.setModifiedDate(new Date());
		productContract.setModifiedBy(user);
		// for (ProductContractItems Data : productContract.getProductContractItem()) {
		// LOG.info("delete*******..." + Data.getContractItemNumber());
		// productContractItemsService.deleteBycontractItemNumber(Data.getContractItemNumber());
		// }

		if (CollectionUtil.isNotEmpty(productContractPojo.getItemList())) {
			for (ProductContractItemsPojo itemPojo : productContractPojo.getItemList()) {

				if (itemPojo.getQuantity().floatValue() <= 0.0f) {
					throw new ApplicationException("Quantity should be more than zero. Contract Item No: " + itemPojo.getContractItemNumber());
				}

				// Check with existing item in db
				ProductContractItems dbItem = null;
				for (ProductContractItems item : productContract.getProductContractItem()) {
					// Find matching using the itemCode and Storage Location and item contract number
					if (//
					item.getProductItem().getProductCode().equals(itemPojo.getItemCode()) //
							&& StringUtils.checkString(item.getStorageLocation()).equals(StringUtils.checkString(itemPojo.getStorageLoc())) //
							&& StringUtils.checkString(item.getContractItemNumber()).equals(StringUtils.checkString(itemPojo.getContractItemNumber()))) {
						dbItem = item;
						break;
					}
				}

				CostCenter costCenter = null;
				if (StringUtils.checkString(itemPojo.getCostCenter()).length() > 0) {
					costCenter = costCenterService.getActiveCostCenterForTenantByCostCenterName(itemPojo.getCostCenter(), buyer.getId());
					if (costCenter == null) {
						throw new ApplicationException(messageSource.getMessage("erpIntegration.cost.center.invalid", new Object[] { itemPojo.getCostCenter() }, Global.LOCALE));
					}
				}

				// new Item
				if (dbItem == null) {
					if (itemPojo.getItemType() == null) {
						throw new ApplicationException(messageSource.getMessage("itemType.missing", new Object[] {}, Global.LOCALE));
					}

					ProductItem productItem = productListMaintenanceService.findProductItemByCode(itemPojo.getItemCode(), buyer.getId(), itemPojo.getItemType());
					if (productItem == null) {
						throw new ApplicationException(messageSource.getMessage("erpIntegration.itemCode.notFound", new Object[] { itemPojo.getItemCode(), itemPojo.getItemType() }, Global.LOCALE));
					}

					Uom uom = uomService.getUomByUomAndTenantId(itemPojo.getUom(), buyer.getId());
					if (uom == null) {
						throw new ApplicationException(messageSource.getMessage("erpIntegration.uom.invalid", new Object[] { itemPojo.getUom() }, Global.LOCALE));
					}

					ProductCategory productCategory = productCategoryMaintenanceService.getProductCategoryAndTenantId(itemPojo.getItemCategory(), buyer.getId());
					if (productCategory == null) {
						throw new ApplicationException(messageSource.getMessage("erpIntegration.category.invalid", new Object[] { itemPojo.getItemCategory() }, Global.LOCALE));
					}

					if (productItem.getProductCategory() != null && !productCategory.getId().equals(productItem.getProductCategory().getId())) {
						throw new ApplicationException(messageSource.getMessage("erpIntegration.category.mismatch", new Object[] { itemPojo.getItemCategory() }, Global.LOCALE));
					}

					if (productItem.getProductCategory() == null && productCategory != null) {
						throw new ApplicationException(messageSource.getMessage("erpIntegration.category.notFound", new Object[] { itemPojo.getItemCategory() }, Global.LOCALE));
					}

					if (productItem.getProductCategory() != null && productCategory == null) {
						throw new ApplicationException(messageSource.getMessage("erpIntegration.category.linked", new Object[] { productItem.getProductCategory().getProductCode() }, Global.LOCALE));
					}

					BusinessUnit businessItem = businessUnitService.findBusinessUnitForTenantByUnitCode(buyer.getId(), itemPojo.getBusinessUnit());
					if (businessItem == null) {
						throw new ApplicationException(messageSource.getMessage("erpIntegration.unitCode.invalid", new Object[] { itemPojo.getBusinessUnit() }, Global.LOCALE));
					}

					dbItem = new ProductContractItems();
					dbItem.setProductContract(productContract);
					dbItem.setContractItemNumber(itemPojo.getContractItemNumber());
					dbItem.setProductItem(productItem);
					dbItem.setQuantity(itemPojo.getQuantity());
					dbItem.setBalanceQuantity(itemPojo.getQuantity());
					dbItem.setUom(uom);
					dbItem.setUnitPrice(itemPojo.getUnitPrice());
					dbItem.setProductCategory(productCategory);
					dbItem.setBusinessUnit(businessItem);
					dbItem.setStorageLocation(itemPojo.getStorageLoc());
					dbItem.setCostCenter(costCenter);
					productContract.getProductContractItem().add(dbItem);
				} else {
					LOG.info("Updating existing item - Item Code : " + dbItem.getProductItem().getProductCode() + ", Storage Loc: " + dbItem.getStorageLocation() + ", Bal Qty : " + dbItem.getBalanceQuantity());
					// old = 100, new = 20, diff = 80 means qty reducted by 80. Then subtract that from balance qty.
					// old = 100, new = 120, diff 20 means qty increased by 20. Then add that to balance.
					BigDecimal diff = dbItem.getQuantity().subtract(itemPojo.getQuantity());
					dbItem.setQuantity(itemPojo.getQuantity());
					dbItem.setBalanceQuantity(dbItem.getBalanceQuantity().subtract(diff));
					if (dbItem.getBalanceQuantity().floatValue() < 0) {
						throw new ApplicationException("Reduction in item quantity rejected as balance is becoming negative. Item Code : " + dbItem.getProductItem().getProductCode() + ", Storage Loc: " + dbItem.getStorageLocation());
					}
					dbItem.setUnitPrice(itemPojo.getUnitPrice());
					dbItem.setCostCenter(costCenter);
				}

			}
		} else {
			throw new ApplicationException(messageSource.getMessage("erpIntegration.notEmpty", new Object[] {}, Global.LOCALE));
		}

		productContractService.update(productContract);
	}

	private void saveProductContract(ProductContract productContract, ProductContractPojo productContractPojo, Buyer buyer, User user, FavouriteSupplier favouriteSupplier, BusinessUnit businessUnit) throws ApplicationException {
		productContract.setBusinessUnit(businessUnit);
		productContract.setContractReferenceNumber(productContractPojo.getContractReferenceNumber());
		productContract.setGroupCode(productContractPojo.getGroupCode());
		productContract.setContractStartDate(productContractPojo.getContractStartDate());
		productContract.setContractEndDate(productContractPojo.getContractEndDate());
		productContract.setContractValue(productContractPojo.getContractValue());
		productContract.setSupplier(favouriteSupplier);
		productContract.setStatus(Status.ACTIVE);
		productContract.setBuyer(buyer);
		productContract.setCreatedDate(new Date());
		productContract.setCreatedBy(user);

		List<ProductContractItems> listItem = new ArrayList<>();
		Set<ProductCategory> productCategoryIdList = new HashSet<ProductCategory>();

		if (CollectionUtil.isNotEmpty(productContractPojo.getItemList())) {
			for (ProductContractItemsPojo itemPojo : productContractPojo.getItemList()) {

				if (itemPojo.getQuantity().floatValue() <= 0.0f) {
					throw new ApplicationException("Quantity should be more than zero. Contract Item No: " + itemPojo.getContractItemNumber());
				}

				if (itemPojo.getItemType() == null) {
					throw new ApplicationException(messageSource.getMessage("itemType.missing", new Object[] {}, Global.LOCALE));
				}

				ProductItem productItem = productListMaintenanceService.findProductItemByCode(itemPojo.getItemCode(), buyer.getId(), itemPojo.getItemType());

				if (productItem == null) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.itemCode.notFound", new Object[] { itemPojo.getItemCode(), itemPojo.getItemType() }, Global.LOCALE));
				}

				Uom uom = uomService.getUomByUomAndTenantId(itemPojo.getUom(), buyer.getId());
				if (uom == null) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.uom.invalid", new Object[] { itemPojo.getUom() }, Global.LOCALE));
				}

				CostCenter costCenter = null;
				if (StringUtils.checkString(itemPojo.getCostCenter()).length() > 0) {
					costCenter = costCenterService.getActiveCostCenterForTenantByCostCenterName(itemPojo.getCostCenter(), buyer.getId());
					if (costCenter == null) {
						throw new ApplicationException(messageSource.getMessage("erpIntegration.cost.center.invalid", new Object[] { itemPojo.getCostCenter() }, Global.LOCALE));
					}
				}

				ProductCategory productCategory = productCategoryMaintenanceService.getProductCategoryAndTenantId(itemPojo.getItemCategory(), buyer.getId());
				if (productCategory == null) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.category.invalid", new Object[] { itemPojo.getItemCategory() }, Global.LOCALE));
				}

				if (productItem.getProductCategory() != null && !productCategory.getId().equals(productItem.getProductCategory().getId())) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.category.mismatch", new Object[] { itemPojo.getItemCategory() }, Global.LOCALE));
				}

				if (productItem.getProductCategory() == null && productCategory != null) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.category.notFound", new Object[] { itemPojo.getItemCategory() }, Global.LOCALE));
				}

				if (productItem.getProductCategory() != null && productCategory == null) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.category.linked", new Object[] { productItem.getProductCategory().getProductCode() }, Global.LOCALE));
				}

				BusinessUnit businessItem = businessUnitService.findBusinessUnitForTenantByUnitCode(buyer.getId(), itemPojo.getBusinessUnit());
				if (businessItem == null) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.unitCode.invalid", new Object[] { itemPojo.getBusinessUnit() }, Global.LOCALE));
				}
				productCategoryIdList.add(productItem.getProductCategory());

				ProductContractItems item = new ProductContractItems();
				item.setProductContract(productContract);
				item.setContractItemNumber(itemPojo.getContractItemNumber());
				item.setProductItem(productItem);
				item.setQuantity(itemPojo.getQuantity());
				item.setBalanceQuantity(itemPojo.getQuantity());
				item.setUom(uom);
				item.setUnitPrice(itemPojo.getUnitPrice());
				item.setProductCategory(productCategory);
				item.setBusinessUnit(businessItem);
				item.setStorageLocation(itemPojo.getStorageLoc());
				item.setCostCenter(costCenter);
				listItem.add(item);
			}
		} else {
			throw new ApplicationException(messageSource.getMessage("erpIntegration.notEmpty", new Object[] {}, Global.LOCALE));
		}

		// persistObj.setProductContractItem(listItem);
		// productContractService.update(persistObj);
		productContract.setProductContractItem(listItem);
		productContractService.createProductContract(productContract);

		List<ProductCategory> favprdctCategoryList = new ArrayList<ProductCategory>();
		List<ProductCategory> tempList = new ArrayList<ProductCategory>();

		if (CollectionUtil.isNotEmpty(favouriteSupplier.getProductCategory())) {
			LOG.info("favouriteSupplier  category not empty if");

			favprdctCategoryList = favouriteSupplier.getProductCategory();
			for (ProductCategory category : productCategoryIdList) {
				LOG.info("category  : " + category.getId());

				if (!favprdctCategoryList.contains(category)) {
					LOG.info(" not Contain : ");
					ProductCategory productCat = new ProductCategory();
					productCat.setId(category.getId());
					tempList.add(productCat);
				}

			}
		} else {
			LOG.info("else");
			for (ProductCategory category : productCategoryIdList) {
				LOG.info("add direct " + category.getId());
				ProductCategory productCat = new ProductCategory();
				productCat.setId(category.getId());
				tempList.add(productCat);

			}

		}
		favprdctCategoryList.addAll(tempList);
		favouriteSupplier.setProductCategory(favprdctCategoryList);
		favoriteSupplierService.updateFavoriteSupplier(favouriteSupplier);

	}

	@RequestMapping(value = "/sapPrResponse", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> sapPrResponse(@RequestHeader(name = "X-AUTH-KEY", required = true) String authkey, @RequestBody PrResponsePojo prResponse) {
		LOG.info("PR Response Api called..." + prResponse);
		Map<String, Object> map = new HashMap<>();
		map.put("responseId", prResponse.getTransactionId());
		map.put("resTransactionId", prResponse.getConsumerReferenceId());
		map.put("providerId", "Procurehere");
		try {
			ErpSetup erpConfig = erpSetupDao.getErpConfigWithTepmlateByAppId(authkey);
			if (erpConfig == null) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.erpconfig.empty", new Object[] {}, Global.LOCALE));
			}

			if (Boolean.FALSE == erpConfig.getEnableSapPrResponseApi()) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.erpconfig.notallowed", new Object[] {}, Global.LOCALE));
			}

			Buyer buyer = buyerService.findBuyerById(erpConfig.getTenantId());
			if (buyer == null) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.buyer.empty", new Object[] {}, Global.LOCALE));
			}

			if (prResponse.getTransactionType() == ProcurehereDocumentType.PR) {

				String prId = prResponse.getTransactionId();
				Pr pr = prService.findPrBySystemGeneratedPrIdAndTenantId(prId, buyer.getId());

				if (pr == null) {
					throw new ApplicationException("Invalid Procurehere PR No received from ERP : " + prId);
				}

				User user = userService.getUserByLoginIdNoTouch(buyer.getLoginEmail());
				if (user == null) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.user.empty", new Object[] {}, Global.LOCALE));
				}
				PrAudit audit = new PrAudit();
				audit.setActionBy(user);
				audit.setActionDate(new Date());
				audit.setBuyer(buyer);
				if (StringUtils.checkString(prResponse.getStatus()).equalsIgnoreCase("FAIL")) {

					// If Failed at ERP, then reset the status of PR to APPROVED so that it can be transferred again.
					pr.setErpDocNo(null);
					pr.setErpMessage(prResponse.getMessage());
					pr.setErpPrTransferred(Boolean.FALSE);
					pr.setErpStatus(null);
					pr.setStatus(PrStatus.APPROVED);
					prService.updatePr(pr);

					audit.setAction(PrAuditType.TRANSFERRED);
					audit.setDescription("Received failed response from ERP : " + prResponse.getMessage() + " Ref: " + prResponse.getSapRefNo());
					audit.setPr(pr);
					prAuditService.save(audit);
				} else {
					pr.setErpDocNo(prResponse.getSapRefNo());
					pr.setErpMessage(prResponse.getMessage());
					pr.setErpPrTransferred(Boolean.TRUE);
					pr.setErpStatus(prResponse.getStatus());

					audit.setAction(PrAuditType.DELIVERED);
					audit.setDescription(prResponse.getSapDocType() + " created in ERP with Reference : " + prResponse.getSapRefNo() + " Response: " + prResponse.getMessage());
					audit.setPr(pr);
					prAuditService.save(audit);

					// Create PO
					Boolean isAutoCreatePo = buyerSettingsService.isAutoCreatePoSettingsByTenantId(buyer.getId());
					if (Boolean.TRUE == erpConfig.getIsGeneratePo() && isAutoCreatePo) {
						Po po = prService.createPo(pr.getCreatedBy(), pr);
						pr.setIsPo(Boolean.TRUE);
						approvalService.sendPoCreatedEmailToCreater(pr.getCreatedBy(), pr, pr.getCreatedBy());
						try {
							if (po.getStatus() == PoStatus.ORDERED) {
								po = poService.getLoadedPoById(po.getId());
								if (po.getSupplier() != null) {
									approvalService.sendPoReceivedEmailNotificationToSupplier(po, po.getCreatedBy());
								}
							}
						} catch (Exception e) {
							LOG.error("Error while sending PO email notification to supplier:" + e.getMessage(), e);
						}
					}
					prService.updatePr(pr);

				}

			} else if (prResponse.getTransactionType() == ProcurehereDocumentType.RFS) {

				String rfsId = prResponse.getTransactionId();

				SourcingFormRequest sourcingFormRequest = sourcingFormRequestService.getSourcingFormByFormIdAndTenant(rfsId, buyer.getId());

				if (sourcingFormRequest == null) {
					throw new ApplicationException("Invalid Procurehere SR No received from ERP : " + rfsId);
				}

				User user = userService.getUserByLoginIdNoTouch(buyer.getLoginEmail());
				if (user == null) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.user.empty", new Object[] {}, Global.LOCALE));
				}

				RequestAudit audit = new RequestAudit();
				audit.setActionBy(user);
				audit.setActionDate(new Date());
				audit.setBuyer(buyer);
				audit.setReq(sourcingFormRequest);
				if (StringUtils.checkString(prResponse.getStatus()).equalsIgnoreCase("FAIL")) {

					if (prResponse.getOperation() == OperationType.D) {
						// If Failed at ERP, then reset the status of PR to APPROVED so that it can be transferred
						// again.
						audit.setAction(RequestAuditType.ERROR);
						audit.setDescription("Received failed response from ERP for Delete : " + prResponse.getMessage() + " Ref: " + prResponse.getSapRefNo());

					} else {
						// If Failed at ERP, then reset the status of PR to APPROVED so that it can be transferred
						// again.
						audit.setAction(RequestAuditType.ERROR);
						audit.setDescription("Received failed response from ERP : " + prResponse.getMessage() + " Ref: " + prResponse.getSapRefNo());

						sourcingFormRequest.setStatus(SourcingFormStatus.DRAFT);
						sourcingFormRequest.setSummaryCompleted(false);
						sourcingFormRequest.setErpDocNo(null);
						sourcingFormRequest.setErpMessage(prResponse.getMessage());
						sourcingFormRequest.setErpTransferred(Boolean.FALSE);
						sourcingFormRequest.setErpStatus(prResponse.getStatus());
						sourcingFormRequestService.update(sourcingFormRequest);
					}

				} else {

					audit.setAction(RequestAuditType.ERP);

					if (prResponse.getOperation() != OperationType.D) {
						audit.setDescription(prResponse.getSapDocType() + " created in ERP with Reference : " + prResponse.getSapRefNo() + " Response: " + prResponse.getMessage());
						// Must call Approval method before saving the SAP Response into Request. There is logic
						// attached to
						// it
						sourcingFormRequest = approvalService.doRequestApproval(sourcingFormRequest, sourcingFormRequest.getFormOwner());

						sourcingFormRequest.setErpDocNo(prResponse.getSapRefNo());
						sourcingFormRequest.setErpMessage(prResponse.getMessage());
						sourcingFormRequest.setErpTransferred(Boolean.TRUE);
						sourcingFormRequest.setErpStatus(prResponse.getStatus());
						sourcingFormRequest = sourcingFormRequestService.update(sourcingFormRequest);
					} else {
						audit.setDescription(prResponse.getSapDocType() + " deleted in ERP with Reference : " + prResponse.getSapRefNo() + " Response: " + prResponse.getMessage());
					}
				}
				requestAuditDao.save(audit);

			} else if (prResponse.getTransactionType() == ProcurehereDocumentType.RFQ) {

				User user = userService.getUserByLoginIdNoTouch(buyer.getLoginEmail());
				if (user == null) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.user.empty", new Object[] {}, Global.LOCALE));
				}

				String eventId = prResponse.getTransactionId();

				if (StringUtils.checkString(eventId).length() == 0) {
					throw new ApplicationException("Received empty Transaction ID for RFQ Response");
				}

				RfqEvent event = rfqEventService.getPlainEventByFormattedEventIdAndTenantId(eventId, buyer.getId());
				if (event == null) {
					throw new ApplicationException("Invalid Procurehere RFQ ID received from ERP : " + eventId);
				}

				// PH-1178 - Revert status of event to COMPLETE so that the save and transfer of award can be done again
				if (StringUtils.checkString(prResponse.getStatus()).equalsIgnoreCase("FAIL")) {
					event.setStatus(EventStatus.COMPLETE);
					event = rfqEventService.updateEvent(event);
				}

				String message = "";
				message = "ERP replied " + prResponse.getStatus() + " for " + prResponse.getSapDocType() + " with Reference : " + prResponse.getSapRefNo() + " Response: " + prResponse.getMessage();
				RfqEventAudit audit = new RfqEventAudit(buyer, event, user, new Date(), AuditActionType.Transfer, message, null);
				eventAuditService.save(audit);

			} else if (prResponse.getTransactionType() == ProcurehereDocumentType.RFT) {

				User user = userService.getUserByLoginIdNoTouch(buyer.getLoginEmail());
				if (user == null) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.user.empty", new Object[] {}, Global.LOCALE));
				}

				String eventId = prResponse.getTransactionId();
				if (StringUtils.checkString(eventId).length() == 0) {
					throw new ApplicationException("Received empty Transaction ID for RFQ Response");
				}

				RftEvent event = rftEventService.getPlainEventByFormattedEventIdAndTenantId(eventId, buyer.getId());
				if (event == null) {
					throw new ApplicationException("Invalid Procurehere RFT ID received from ERP : " + eventId);
				}

				// PH-1178 - Revert status of event to COMPLETE so that the save and transfer of award can be done again
				if (StringUtils.checkString(prResponse.getStatus()).equalsIgnoreCase("FAIL")) {
					event.setStatus(EventStatus.COMPLETE);
					event = rftEventService.updateEventSimple(event);
				}

				String message = "";
				message = "ERP replied " + prResponse.getStatus() + " for " + prResponse.getSapDocType() + " with Reference : " + prResponse.getSapRefNo() + " Response: " + prResponse.getMessage();
				RftEventAudit audit = new RftEventAudit(buyer, event, user, new Date(), AuditActionType.Transfer, message, null);
				eventAuditService.save(audit);

			} else if (prResponse.getTransactionType() == ProcurehereDocumentType.RFP) {

				User user = userService.getUserByLoginIdNoTouch(buyer.getLoginEmail());
				if (user == null) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.user.empty", new Object[] {}, Global.LOCALE));
				}

				String eventId = prResponse.getTransactionId();
				if (StringUtils.checkString(eventId).length() == 0) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.transection.empty", new Object[] {}, Global.LOCALE));

				}

				RfpEvent event = rfpEventService.getPlainEventByFormattedEventIdAndTenantId(eventId, buyer.getId());
				if (event == null) {
					throw new ApplicationException(messageSource.getMessage("invalid.rfp.id", new Object[] { eventId }, Global.LOCALE));
				}

				// PH-1178 - Revert status of event to COMPLETE so that the save and transfer of award can be done again
				if (StringUtils.checkString(prResponse.getStatus()).equalsIgnoreCase("FAIL")) {
					event.setStatus(EventStatus.COMPLETE);
					event = rfpEventService.updateEvent(event);
				}

				String message = "";
				message = "ERP replied " + prResponse.getStatus() + " for " + prResponse.getSapDocType() + " with Reference : " + prResponse.getSapRefNo() + " Response: " + prResponse.getMessage();
				RfpEventAudit audit = new RfpEventAudit(buyer, event, user, new Date(), AuditActionType.Transfer, message, null);
				eventAuditService.save(audit);

			} else if (prResponse.getTransactionType() == ProcurehereDocumentType.RFA) {

				User user = userService.getUserByLoginIdNoTouch(buyer.getLoginEmail());
				if (user == null) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.user.empty", new Object[] {}, Global.LOCALE));
				}

				String eventId = prResponse.getTransactionId();
				if (StringUtils.checkString(eventId).length() == 0) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.transection.empty", new Object[] {}, Global.LOCALE));
				}

				RfaEvent event = rfaEventService.getPlainEventByFormattedEventIdAndTenantId(eventId, buyer.getId());
				if (event == null) {
					throw new ApplicationException(messageSource.getMessage("invalid.rfa.id", new Object[] { eventId }, Global.LOCALE));
				}

				// PH-1178 - Revert status of event to COMPLETE so that the save and transfer of award can be done again
				if (StringUtils.checkString(prResponse.getStatus()).equalsIgnoreCase("FAIL")) {
					event.setStatus(EventStatus.COMPLETE);
					event = rfaEventService.updateRfaEvent(event);
				}

				String message = "";
				message = "ERP replied " + prResponse.getStatus() + " for " + prResponse.getSapDocType() + " with Reference : " + prResponse.getSapRefNo() + " Response: " + prResponse.getMessage();
				RfaEventAudit audit = new RfaEventAudit(buyer, event, user, new Date(), AuditActionType.Transfer, message, null);
				eventAuditService.save(audit);

			} else {
				LOG.info("Bhad mein ja Transaction Type : " + prResponse.getTransactionType() + " ko lekar.");
			}

			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

			map.put("resTimestamp", df.format(new Date()));
			map.put("resStatus", "SUCC");
			map.put("msgCode", "IM-001");
			map.put("msgDesc", "Message received successfully");
		} catch (Exception e) {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
			LOG.error("Error Processing ERP PR Response : " + e.getMessage(), e);
			map.put("resTimestamp", df.format(new Date()));
			map.put("resStatus", "FAIL");
			map.put("msgCode", "IM-002");
			map.put("msgDesc", "Error Processing ERP Response : " + e.getMessage());
			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		}
		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

	@RequestMapping(value = "/createPo", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> createPO(@RequestHeader(name = "X-AUTH-KEY", required = true) String authkey, @RequestBody PoPojo poPojo) {
		LOG.info("Create PO Api called..." + poPojo);
		Map<String, Object> map = new HashMap<>();
		try {
			ErpSetup erpConfig = erpSetupDao.getErpConfigWithTepmlateByAppId(authkey);
			if (erpConfig == null) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.erpconfig.empty", new Object[] {}, Global.LOCALE));
			}

			if (Boolean.FALSE == erpConfig.getEnablePoCreateApi()) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.erpconfig.notallowed", new Object[] {}, Global.LOCALE));
			}

			Buyer buyer = buyerService.findBuyerById(erpConfig.getTenantId());
			if (buyer == null) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.buyer.empty", new Object[] {}, Global.LOCALE));
			}

			User user = userService.getUserByLoginIdNoTouch(buyer.getLoginEmail());
			if (user == null) {
				throw new ApplicationException(messageSource.getMessage("erpIntegration.user.empty", new Object[] {}, Global.LOCALE));
			}

			CostCenter costCenter = null;
			if (StringUtils.checkString(poPojo.getCostCenter()).length() > 0) {
				costCenter = costCenterService.getActiveCostCenterForTenantByCostCenterName(poPojo.getCostCenter(), buyer.getId());
				if (costCenter == null) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.cost.center.invalid", new Object[] { poPojo.getCostCenter() }, Global.LOCALE));
				}
			}

			BusinessUnit businessUnit = null;
			if (StringUtils.checkString(poPojo.getBusinessUnit()).length() > 0) {
				businessUnit = businessUnitService.findBusinessUnitForTenantByUnitCode(buyer.getId(), poPojo.getBusinessUnit());
				if (businessUnit == null) {
					throw new ApplicationException(messageSource.getMessage("erpIntegration.unitCode.invalid", new Object[] { poPojo.getBusinessUnit() }, Global.LOCALE));
				}
			}

			BuyerAddress correspondenceAddress = null;

			if (poPojo.getCorrespondenceAddress() == null) {
				// throw new ApplicationException("Missing correspondence address.");
			} else {
				correspondenceAddress = new BuyerAddress();
				if (StringUtils.checkString(poPojo.getCorrespondenceAddress().getTitle()).length() == 0) {
					throw new ApplicationException("Missing Correspondence Address Title.");
				}
				if (StringUtils.checkString(poPojo.getCorrespondenceAddress().getLine1()).length() == 0) {
					throw new ApplicationException("Missing Correspondence Address Line 1.");
				}
				if (StringUtils.checkString(poPojo.getCorrespondenceAddress().getLine2()).length() == 0) {
					throw new ApplicationException("Missing Correspondence Address Line 2.");
				}
				if (StringUtils.checkString(poPojo.getCorrespondenceAddress().getCity()).length() == 0) {
					throw new ApplicationException("Missing Correspondence Address City.");
				}
				if (StringUtils.checkString(poPojo.getCorrespondenceAddress().getZipCode()).length() == 0) {
					throw new ApplicationException("Missing Correspondence Address zipCode.");
				}

				correspondenceAddress.setBuyer(buyer);
				correspondenceAddress.setTitle(poPojo.getCorrespondenceAddress().getTitle());
				correspondenceAddress.setLine1(poPojo.getCorrespondenceAddress().getLine1());
				correspondenceAddress.setLine2(poPojo.getCorrespondenceAddress().getLine2());
				correspondenceAddress.setCity(poPojo.getCorrespondenceAddress().getCity());
				correspondenceAddress.setZip(poPojo.getCorrespondenceAddress().getZipCode());
				String countryCode = poPojo.getCorrespondenceAddress().getCountryCode();
				if (StringUtils.checkString(countryCode).length() == 0) {
					throw new ApplicationException("Missing Country.");
				}
				Country country = countryService.getCountryByCode(countryCode);
				if (country == null) {
					throw new ApplicationException("Invalid country code : " + countryCode);
				}
				correspondenceAddress.setCountry(country);
				String stateCode = poPojo.getCorrespondenceAddress().getState();
				if (StringUtils.checkString(stateCode).length() == 0) {
					throw new ApplicationException("Missing State.");
				}

				List<State> stateList = stateService.searchStatesByNameOrCode(stateCode);
				if (CollectionUtil.isEmpty(stateList)) {
					throw new ApplicationException("Invalid State Name or Code : " + stateCode);
				}

				// Find matching state within the given country ( TODO: Or write a query to fetch it based on country so
				// you dont have to loop to a list)
				State state = null;
				for (State st : stateList) {
					if (st.getCountry().getCountryCode().equals(countryCode)) {
						state = st;
						break;
					}
				}
				if (state == null) {
					throw new ApplicationException("Invalid State Name or Code for given country: " + stateCode);
				}

				correspondenceAddress.setState(state);
				correspondenceAddress.setStatus(Status.ACTIVE);
			}

			BuyerAddress deliveryAddress = null;
			if (poPojo.getDeliveryAddress() == null) {
				// throw new ApplicationException("Missing delivery address.");
			} else {
				deliveryAddress = new BuyerAddress();
				deliveryAddress.setBuyer(buyer);

				if (StringUtils.checkString(poPojo.getDeliveryAddress().getTitle()).length() == 0) {
					throw new ApplicationException("Missing Delivery Address Title.");
				}
				if (StringUtils.checkString(poPojo.getDeliveryAddress().getLine1()).length() == 0) {
					throw new ApplicationException("Missing Delivery Address Line 1.");
				}
				if (StringUtils.checkString(poPojo.getDeliveryAddress().getLine2()).length() == 0) {
					throw new ApplicationException("Missing Delivery Address Line 2.");
				}
				if (StringUtils.checkString(poPojo.getDeliveryAddress().getCity()).length() == 0) {
					throw new ApplicationException("Missing Delivery Address City.");
				}
				if (StringUtils.checkString(poPojo.getDeliveryAddress().getZipCode()).length() == 0) {
					throw new ApplicationException("Missing Delivery Address zipCode.");
				}

				deliveryAddress.setTitle(poPojo.getDeliveryAddress().getTitle());
				deliveryAddress.setLine1(poPojo.getDeliveryAddress().getLine1());
				deliveryAddress.setLine2(poPojo.getDeliveryAddress().getLine2());
				deliveryAddress.setCity(poPojo.getDeliveryAddress().getCity());
				deliveryAddress.setZip(poPojo.getDeliveryAddress().getZipCode());
				String countryCode = poPojo.getDeliveryAddress().getCountryCode();
				if (StringUtils.checkString(countryCode).length() == 0) {
					throw new ApplicationException("Missing Country.");
				}
				Country country = countryService.getCountryByCode(countryCode);
				if (country == null) {
					throw new ApplicationException("Invalid country code : " + countryCode);
				}
				deliveryAddress.setCountry(country);
				String stateCode = poPojo.getDeliveryAddress().getState();
				if (StringUtils.checkString(stateCode).length() == 0) {
					throw new ApplicationException("Missing State.");
				}

				List<State> stateList = stateService.searchStatesByNameOrCode(stateCode);
				if (CollectionUtil.isEmpty(stateList)) {
					throw new ApplicationException("Invalid State Name or Code : " + stateCode);
				}

				// Find matching state within the given country ( TODO: Or write a query to fetch it based on country so
				// you dont have to loop to a list)
				State state = null;
				for (State st : stateList) {
					if (st.getCountry().getCountryCode().equals(countryCode)) {
						state = st;
						break;
					}
				}
				if (state == null) {
					throw new ApplicationException("Invalid State Name or Code for given country: " + stateCode);
				}

				deliveryAddress.setState(state);
				deliveryAddress.setStatus(Status.ACTIVE);
			}

			Currency currency = null;
			if (StringUtils.checkString(poPojo.getCurrency()).length() == 0) {
				throw new ApplicationException("Invalid Currency Code: " + poPojo.getCurrency());
			}

			currency = currencyDao.findByCurrencyCode(poPojo.getCurrency());
			if (currency == null) {
				throw new ApplicationException("Invalid Currency Code: " + poPojo.getCurrency());
			}

			if (poPojo.getDecimal() == null || (poPojo.getDecimal() != null && poPojo.getDecimal().intValue() == 0)) {
				throw new ApplicationException("Invalid Decimal Value: " + poPojo.getDecimal());
			}

			FavouriteSupplier supplier = null;
			if (StringUtils.checkString(poPojo.getSupplierCode()).length() > 0) {
				supplier = favoriteSupplierService.getFavouriteSupplierByVendorCode(StringUtils.checkString(poPojo.getSupplierCode()), buyer.getId());
				if (supplier == null) {
					throw new ApplicationException("Invalid Supplier Code: " + poPojo.getSupplierCode());
				}
			}

			Po po = new Po();
			po.setBuyer(buyer);
			po.setBusinessUnit(businessUnit);
			po.setCorrespondenceAddress(correspondenceAddress);
			po.setCostCenter(costCenter);
			po.setCreatedBy(user);
			po.setCreatedDate(poPojo.getCreatedDate());
			po.setCurrency(currency);
			po.setDecimal(String.valueOf(poPojo.getDecimal()));
			po.setDeliveryAddress(deliveryAddress);
			po.setDeliveryDate(poPojo.getDeliveryDate());
			po.setDeliveryReceiver(poPojo.getDeliveryReceiver());
			// po.setDescription(poPojo.getDe);
			po.setErpDocNo(poPojo.getReferenceNumber());
			// po.setErpMessage(poPojo.get);
			po.setErpPrTransferred(Boolean.FALSE);
			// po.setErpStatus(erpStatus);
			po.setGrandTotal(poPojo.getGrandTotal());
			po.setIsPoReportSent(Boolean.FALSE);
			po.setName(poPojo.getName());
			po.setPaymentTerm(poPojo.getPaymentTerm());
			po.setPoNumber(poPojo.getReferenceNumber());
			po.setReferenceNumber(poPojo.getReferenceNumber());
			po.setRemarks(poPojo.getRemarks());
			po.setRequester(poPojo.getRequester());
			po.setStatus(PoStatus.ORDERED);
			po.setSupplier(supplier);
			if (supplier == null) {
				po.setSupplierName(poPojo.getSupplierName());
				// po.setSupplierAddress(poPojo.get);
				// po.setSupplierTelNumber(supplier.getCompanyContactNumber());
				// po.setSupplierAddress(supplierAddress);
				// po.setSupplierTaxNumber(supplier.getFavouriteSupplierTaxNumber());
				// po.setSupplierFaxNumber(supplier.getFavouriteSupplierTaxNumber());
			}
			// po.setTaxDescription(taxDescription);
			po.setTermsAndConditions(poPojo.getTermsAndConditions());
			po.setTotal(poPojo.getGrandTotal()); // as there is no additional tax coming from SAP
			po.setUrgentPo(Boolean.FALSE);

			List<PoItem> poItems = new ArrayList<PoItem>();

			if (CollectionUtil.isNotEmpty(poPojo.getItemList())) {
				PoItem parent = new PoItem();
				parent.setItemName("Bill of Items");
				parent.setLevel(1);
				parent.setOrder(0);

				for (PoItemsPojo itemPojo : poPojo.getItemList()) {
					PoItem item = new PoItem();

					if (itemPojo.getQuantity().floatValue() <= 0.0f) {
						throw new ApplicationException("Quantity should be more than zero. Item No: " + itemPojo.getItemNo());
					}

					if (itemPojo.getItemType() == null) {
						throw new ApplicationException(messageSource.getMessage("itemType.missing", new Object[] {}, Global.LOCALE));
					}

					ProductItem productItem = productListMaintenanceService.findProductItemByCode(itemPojo.getItemCode(), buyer.getId(), itemPojo.getItemType());

					if (productItem == null) {
						throw new ApplicationException(messageSource.getMessage("erpIntegration.itemCode.notFound", new Object[] { itemPojo.getItemCode(), itemPojo.getItemType() }, Global.LOCALE));
					}

					Uom uom = uomService.getUomByUomAndTenantId(itemPojo.getUom(), buyer.getId());
					if (uom == null) {
						throw new ApplicationException(messageSource.getMessage("erpIntegration.uom.invalid", new Object[] { itemPojo.getUom() }, Global.LOCALE));
					}

					ProductCategory productCategory = productCategoryMaintenanceService.getProductCategoryAndTenantId(itemPojo.getItemCategory(), buyer.getId());
					if (productCategory == null) {
						throw new ApplicationException(messageSource.getMessage("erpIntegration.category.invalid", new Object[] { itemPojo.getItemCategory() }, Global.LOCALE));
					}

					if (productItem.getProductCategory() != null && !productCategory.getId().equals(productItem.getProductCategory().getId())) {
						throw new ApplicationException(messageSource.getMessage("erpIntegration.category.mismatch", new Object[] { itemPojo.getItemCategory() }, Global.LOCALE));
					}

					if (productItem.getProductCategory() == null && productCategory != null) {
						throw new ApplicationException(messageSource.getMessage("erpIntegration.category.notFound", new Object[] { itemPojo.getItemCategory() }, Global.LOCALE));
					}

					if (productItem.getProductCategory() != null && productCategory == null) {
						throw new ApplicationException(messageSource.getMessage("erpIntegration.category.linked", new Object[] { productItem.getProductCategory().getProductCode() }, Global.LOCALE));
					}

					if (itemPojo.getDeliveryAddress() == null) {
						// throw new ApplicationException("Missing delivery address.");
					} else {

						deliveryAddress = new BuyerAddress();

						deliveryAddress.setBuyer(buyer);

						if (StringUtils.checkString(itemPojo.getDeliveryAddress().getTitle()).length() == 0) {
							throw new ApplicationException("Missing Delivery Address Title.");
						}
						if (StringUtils.checkString(itemPojo.getDeliveryAddress().getLine1()).length() == 0) {
							throw new ApplicationException("Missing Delivery Address Line 1.");
						}
						if (StringUtils.checkString(itemPojo.getDeliveryAddress().getLine2()).length() == 0) {
							throw new ApplicationException("Missing Delivery Address Line 2.");
						}
						if (StringUtils.checkString(itemPojo.getDeliveryAddress().getCity()).length() == 0) {
							throw new ApplicationException("Missing Delivery Address City.");
						}
						if (StringUtils.checkString(itemPojo.getDeliveryAddress().getZipCode()).length() == 0) {
							throw new ApplicationException("Missing Delivery Address zipCode.");
						}

						deliveryAddress.setTitle(itemPojo.getDeliveryAddress().getTitle());
						deliveryAddress.setLine1(itemPojo.getDeliveryAddress().getLine1());
						deliveryAddress.setLine2(itemPojo.getDeliveryAddress().getLine2());
						deliveryAddress.setCity(itemPojo.getDeliveryAddress().getCity());
						deliveryAddress.setZip(itemPojo.getDeliveryAddress().getZipCode());
						String countryCode = itemPojo.getDeliveryAddress().getCountryCode();
						if (StringUtils.checkString(countryCode).length() == 0) {
							throw new ApplicationException("Missing Country.");
						}
						Country country = countryService.getCountryByCode(countryCode);
						if (country == null) {
							throw new ApplicationException("Invalid country code : " + countryCode);
						}
						deliveryAddress.setCountry(country);

						String stateCode = itemPojo.getDeliveryAddress().getState();
						if (StringUtils.checkString(stateCode).length() == 0) {
							throw new ApplicationException("Missing State.");
						}
						List<State> stateList = stateService.searchStatesByNameOrCode(stateCode);
						if (CollectionUtil.isEmpty(stateList)) {
							throw new ApplicationException("Invalid State Name or Code : " + stateCode);
						}

						// Find matching state within the given country ( TODO: Or write a query to fetch it based on
						// country so you dont have to loop to a list)
						State state = null;
						for (State st : stateList) {
							if (st.getCountry().getCountryCode().equals(countryCode)) {
								state = st;
								break;
							}
						}
						if (state == null) {
							throw new ApplicationException("Invalid State Name or Code for given country: " + stateCode);
						}

						deliveryAddress.setState(state);
						deliveryAddress.setStatus(Status.ACTIVE);

					}

					if (StringUtils.checkString(itemPojo.getCostCenter()).length() > 0) {
						costCenter = costCenterService.getActiveCostCenterForTenantByCostCenterName(itemPojo.getCostCenter(), buyer.getId());
						if (costCenter == null) {
							throw new ApplicationException(messageSource.getMessage("erpIntegration.cost.center.invalid", new Object[] { poPojo.getCostCenter() }, Global.LOCALE));
						}
					}

					if (StringUtils.checkString(itemPojo.getBusinessUnit()).length() > 0) {
						businessUnit = businessUnitService.findBusinessUnitForTenantByUnitCode(buyer.getId(), itemPojo.getBusinessUnit());
						if (businessUnit == null) {
							throw new ApplicationException(messageSource.getMessage("erpIntegration.unitCode.invalid", new Object[] { poPojo.getBusinessUnit() }, Global.LOCALE));
						}
					}

					item.setBusinessUnit(businessUnit);
					item.setBuyer(buyer);
					item.setCostCenter(costCenter);
					item.setDeliveryAddress(deliveryAddress);
					item.setDeliveryDate(itemPojo.getDeliveryDate());
					item.setDeliveryReceiver(itemPojo.getDeliveryReceiver());
					item.setFreeTextItemEntered(Boolean.FALSE);
					item.setItemDescription(itemPojo.getItemDescription());
					item.setItemName(itemPojo.getItemName());
					item.setItemTax(itemPojo.getItemTax());
					item.setLevel(1);
					item.setOrder((Integer.parseInt(itemPojo.getItemNo()) / 10));
					item.setParent(parent);
					item.setPo(po);
					item.setProduct(productItem);
					item.setProductCategory(productCategory);
					// item.setProductContractItem(productContractItem);
					item.setQuantity(itemPojo.getQuantity());
					item.setTaxAmount(itemPojo.getTaxAmount());
					item.setTotalAmount(itemPojo.getTotalAmount());
					item.setTotalAmountWithTax(itemPojo.getTotalAmountWithTax());
					item.setUnit(uom);
					item.setUnitPrice(itemPojo.getUnitPrice());
				}
			} else {
				throw new ApplicationException("Missing PO Items");
			}

			po.setPoItems(poItems);

			poService.savePo(po);

			map.put("errorcode", 0);
			map.put("errorMessage", "SUCCESS");

		} catch (Exception e) {
			LOG.error("Error Processing Create PO Request : " + e.getMessage(), e);
			map.put("errorCode", 1001);
			map.put("errorMessage", messageSource.getMessage("erpIntegration.createPo.error", new Object[] { e.getMessage() }, Global.LOCALE));
			return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		}
		return new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
	}

}
