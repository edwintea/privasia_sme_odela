package com.privasia.procurehere.core.dao.impl;

import java.util.Arrays;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.privasia.procurehere.core.dao.PoItemDao;
import com.privasia.procurehere.core.entity.PoItem;
import com.privasia.procurehere.core.entity.PrItem;
import com.privasia.procurehere.core.exceptions.NotAllowedException;
import com.privasia.procurehere.core.utils.CollectionUtil;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.core.utils.StringUtils;

/**
 * @author parveen
 */
@Repository
public class PoItemDaoImpl extends GenericDaoImpl<PoItem, String> implements PoItemDao {

	private static final Logger LOG = Logger.getLogger(Global.PR_LOG);

	@SuppressWarnings("unchecked")
	@Override
	public List<PoItem> getAllPoItemByPoId(String poId) {
		StringBuilder hsql = new StringBuilder("select distinct pi from PoItem pi left outer join fetch pi.unit left outer join fetch pi.product pd left outer join fetch pd.uom  left outer join fetch pi.po p where p.id = :poId order by pi.level, pi.order");
		final Query query = getEntityManager().createQuery(hsql.toString());
		query.setParameter("poId", poId);
		List<PoItem> prList = query.getResultList();
		return prList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrItem> getPrItemLevelOrder(String prId) {
		final Query query = getEntityManager().createQuery("select distinct a from PrItem a inner join fetch a.pr c where a.parent is null and c.id = :id order by a.level, a.order");
		query.setParameter("id", prId);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public PrItem getPrItembyPrIdAndPrItemId(String prId, String prItemId) {
		final Query query = getEntityManager().createQuery("select distinct a from PrItem a left outer join fetch a.productCategory pcc left outer join fetch pcc.createdBy  cb left outer join fetch cb.buyer left outer join fetch a.unit uomm inner join fetch a.pr c left outer join fetch a.product p  left outer join fetch a.productContractItem pci left outer join fetch p.uom where c.id = :prId and a.id = :prItemId");
		query.setParameter("prId", prId);
		query.setParameter("prItemId", prItemId);
		List<PrItem> uList = query.getResultList();
		if (CollectionUtil.isNotEmpty(uList)) {
			return uList.get(0);
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
	public String deletePrItems(String[] prItemIds, String prId) {

		for (String id : prItemIds) {
			Query query = getEntityManager().createQuery("select a.level, a.order, p.id from PrItem a left outer join a.parent p where a.id = :id");
			query.setParameter("id", id);
			List<Object[]> result = query.getResultList();
			if (CollectionUtil.isNotEmpty(result) && result.get(0).length > 1) {
				int level = ((Number) result.get(0)[0]).intValue();
				int order = ((Number) result.get(0)[1]).intValue();
				String parentId = ((String) result.get(0)[2]);
				LOG.info("Reordering Level :" + level + " order :" + order + " parentId : " + parentId);

				// If Parent
				if (StringUtils.checkString(parentId).length() == 0) {
					StringBuilder hql = new StringBuilder("update PrItem i set i.level = (i.level - 1) where i.level > :level and i.pr.id = :prId");
					query = getEntityManager().createQuery(hql.toString());
					query.setParameter("prId", prId);
					query.setParameter("level", level);
					query.executeUpdate();
				} else {
					// If Child
					StringBuilder hql = new StringBuilder("update PrItem i set i.order = (i.order - 1) where i.level = :level and i.order >= :order and i.parent.id = :parentId and i.pr.id = :prId");
					query = getEntityManager().createQuery(hql.toString());
					query.setParameter("prId", prId);
					query.setParameter("level", level);
					query.setParameter("order", order);
					query.setParameter("parentId", parentId);
					query.executeUpdate();
				}
			}
		}

		LOG.info("Going to Delete the selected Pr Items");
		Query query = getEntityManager().createQuery("delete from PrItem item where item.id in (:id) and  item.parent is not null and item.pr.id = :prId");
		query.setParameter("id", Arrays.asList(prItemIds));
		query.setParameter("prId", prId);
		query.executeUpdate();

		query = getEntityManager().createQuery("delete from PrItem item where item.id in (:id) and item.parent is null and item.pr.id = :prId");
		query.setParameter("id", Arrays.asList(prItemIds));
		query.setParameter("prId", prId);
		query.executeUpdate();
		LOG.info("COMPLETED TO DELETE the Selected pr ITems");

		String updateResult = updateOnDeletePrItems(prId);
		LOG.info("updateResult :" + updateResult);
		return prId;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
	public String deletePrItemsByPrId(String prId) {

		/*
		 * for (String id : prItemIds) { Query query = getEntityManager().
		 * createQuery("select a.level, a.order, p.id from PrItem a left outer join a.parent p where a.id = :id");
		 * query.setParameter("id", id); List<Object[]> result = query.getResultList(); if
		 * (CollectionUtil.isNotEmpty(result) && result.get(0).length > 1) { int level = ((Number)
		 * result.get(0)[0]).intValue(); int order = ((Number) result.get(0)[1]).intValue(); String parentId = ((String)
		 * result.get(0)[2]); LOG.info("Reordering Level :" + level + " order :" + order + " parentId : " + parentId);
		 * // If Parent if (StringUtils.checkString(parentId).length() == 0) { StringBuilder hql = new
		 * StringBuilder("update PrItem i set i.level = (i.level - 1) where i.level > :level and i.pr.id = :prId");
		 * query = getEntityManager().createQuery(hql.toString()); query.setParameter("prId", prId);
		 * query.setParameter("level", level); query.executeUpdate(); } else { // If Child StringBuilder hql = new
		 * StringBuilder("update PrItem i set i.order = (i.order - 1) where i.level = :level and i.order >= :order and i.parent.id = :parentId and i.pr.id = :prId"
		 * ); query = getEntityManager().createQuery(hql.toString()); query.setParameter("prId", prId);
		 * query.setParameter("level", level); query.setParameter("order", order); query.setParameter("parentId",
		 * parentId); query.executeUpdate(); } }
		 */

		LOG.info("Going to Delete the selected Pr Items");
		Query query = getEntityManager().createQuery("delete from PrItem item where  item.parent is not null and item.pr.id = :prId");
		// query.setParameter("id", Arrays.asList(prItemIds));
		query.setParameter("prId", prId);

		query.executeUpdate();

		Query query1 = getEntityManager().createQuery("delete from PrItem item where  item.parent is null and item.pr.id = :prId");
		// query.setParameter("id", Arrays.asList(prItemIds));
		query1.setParameter("prId", prId);
		query1.executeUpdate();

		LOG.info("COMPLETED TO DELETE the Selected pr ITems");

		// String updateResult = updateOnDeletePrItems(prId);
		// LOG.info("updateResult :" + updateResult);
		return prId;
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
	public String updateOnDeletePrItems(String prId) {
		Query query = getEntityManager().createQuery("update Pr p set p.total =case when (select sum(b.totalAmountWithTax) from PrItem b where b.pr.id = p.id) is null then 0 else (select sum(b.totalAmountWithTax) from PrItem b where b.pr.id = p.id) end where p.id= :prId");
		query.setParameter("prId", prId);
		int result1 = query.executeUpdate();

		query = getEntityManager().createQuery("update Pr p set p.grandTotal = (p.total + p.additionalTax) where p.id= :prId");
		query.setParameter("prId", prId);
		int result2 = query.executeUpdate();

		return result1 + "==" + result2;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrItem> getPrItemsbyId(String prId) {
		final Query query = getEntityManager().createQuery("select distinct a from PrItem a inner join fetch a.pr pr left outer join fetch a.children c where pr.id =:id order by a.level, a.order");
		query.setParameter("id", prId);
		return query.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public PrItem getPrItembyPrItemId(String itemId) {
		try {
			final Query query = getEntityManager().createQuery("select distinct a from PrItem a inner join fetch a.pr c left outer join fetch a.children left outer join fetch a.parent ch where a.id = :id");
			query.setParameter("id", itemId);
			List<PrItem> uList = query.getResultList();
			if (CollectionUtil.isNotEmpty(uList)) {
				LOG.info(" success ");
				return uList.get(0);
			} else {
				return null;
			}
		} catch (NoResultException nr) {
			LOG.info("Error while getting pr Items : " + nr.getMessage(), nr);
			return null;
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void updateItemOrder(String prId, PrItem prItem, String oldParent, String newParent, int oldOrder, int newOrder, int oldLevel, int newLevel) {
		LOG.info("UPdateing ITEM ORDERE ==> oldParent " + oldParent + " newParent " + newParent + " oldOrder " + oldOrder + " newOrder " + newOrder + " oldLevel " + oldLevel + " newLevel " + newLevel);
		// Top Level
		// Rearrange its new place - Push down
		if (newOrder == 0) {
			LOG.info("Enter 1 :: prId :: " + prId + " newLevel :: " + newLevel);
			StringBuilder hql = new StringBuilder("update PrItem i set i.level = (i.level + 1) where i.pr.id = :prId ");

			if (newLevel > oldLevel && oldOrder == 0) {
				hql.append(" and i.level > :level");
			} else {
				hql.append(" and i.level >= :level");
			}

			Query query = getEntityManager().createQuery(hql.toString());
			query.setParameter("prId", prId);
			query.setParameter("level", newLevel);
			query.executeUpdate();
		}
		// Top Level
		// Rearrange its new place - Pull up
		if (oldOrder == 0) {
			LOG.info("Enter 2 :: prId :: " + prId + " oldLevel :: " + oldLevel);
			StringBuilder hql = new StringBuilder("update PrItem i set i.level = (i.level - 1) where i.level > :level and i.pr.id = :prId");
			Query query = getEntityManager().createQuery(hql.toString());
			query.setParameter("prId", prId);
			query.setParameter("level", oldLevel);
			query.executeUpdate();
		}
		// Child Level
		// Push down
		if (newParent != null) {
			LOG.info("Enter 3 :: bqId :: " + prId + " newParent :: " + newParent + " newOrder " + newOrder);
			StringBuilder hql = new StringBuilder("update PrItem i set i.order = (i.order + 1) where i.parent.id = :parent and i.order >= :order and i.pr.id = :prId");
			if (newOrder > oldOrder && oldParent.equals(newParent)) {
				hql.append(" and i.order > :order");
			} else {
				hql.append("  and i.order >= :order ");
			}
			// Rearrange its new place
			Query query = getEntityManager().createQuery(hql.toString());
			query.setParameter("prId", prId);
			query.setParameter("parent", newParent);
			query.setParameter("order", newOrder);
			query.executeUpdate();
		}
		// Child Level
		// Pull Up
		if (oldParent != null) {
			LOG.info("Enter 4 :: prId :: " + prId + " oldParent :: " + oldParent + " oldOrder " + oldOrder);
			StringBuilder hql = new StringBuilder("update PrItem i set i.order = (i.order - 1) where i.parent.id = :parent and i.order > :order and i.pr.id = :prId");
			Query query = getEntityManager().createQuery(hql.toString());
			query.setParameter("prId", prId);
			query.setParameter("parent", oldParent);
			query.setParameter("order", oldOrder);
			query.executeUpdate();
		}

		// Fetch the parent object again as its position might have changed during above updates.
		if (newParent != null) {
			PrItem newDbParent = getPrItembyPrItemId(newParent);
			LOG.info("newDbParent.getLevel() :" + newDbParent.getLevel());
			prItem.setLevel(newDbParent.getLevel());
		}

		LOG.info("Updating object : " + prItem.toLogString());

		StringBuilder hql = new StringBuilder("update PrItem i set i.order = :order, i.level = :level, i.parent = :newParent where i.id = :prItemId and i.pr.id = :prId");
		Query query = getEntityManager().createQuery(hql.toString());
		query.setParameter("prId", prId);
		query.setParameter("prItemId", prItem.getId());
		query.setParameter("newParent", prItem.getParent());
		query.setParameter("order", prItem.getOrder());
		query.setParameter("level", prItem.getLevel());
		query.executeUpdate();
		LOG.info("Updated successfully");
	}

	@Override
	public void deleteNewFieldPr(String label, String prId) {
		try {
			LOG.info("Pr ID :: " + prId + " label :: " + label);
			StringBuilder hql = new StringBuilder("update PrItem i set i." + label + " = null where i.pr.id = :prId");
			Query query = getEntityManager().createQuery(hql.toString());
			query.setParameter("prId", prId);
			query.executeUpdate();
			LOG.info("pr items columns set deleted succesfully...");

			// delete additional column
			hql = new StringBuilder("update Pr p set p." + label + "Label = null where p.id = :prId");
			query = getEntityManager().createQuery(hql.toString());
			query.setParameter("prId", prId);
			query.executeUpdate();
			LOG.info("pr columns deleted succesfully...");
		} catch (NoResultException nr) {
			LOG.info("Error while deleting new Fields: " + nr.getMessage(), nr);
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public PrItem getParentbyLevelId(String prId, Integer level) {
		try {
			final Query query = getEntityManager().createQuery("from PrItem a inner join fetch a.pr as b where b.id = :prId and a.level = :level and a.order = 0 ");
			query.setParameter("prId", prId);
			query.setParameter("level", level);
			List<PrItem> uList = query.getResultList();
			if (CollectionUtil.isNotEmpty(uList)) {
				return uList.get(0);
			} else {
				return null;
			}
		} catch (NoResultException nr) {
			LOG.info("Error while getting PR Items : " + nr.getMessage(), nr);
			return null;
		}
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, rollbackFor = { NotAllowedException.class, Exception.class })
	public void deletePrItemsbyPrid(String prId) {
		try {
			Query query = getEntityManager().createQuery("delete from PrItem item where item.pr.id = :id and item.parent is not null");
			query.setParameter("id", prId);
			query.executeUpdate();

			query = getEntityManager().createQuery("delete from PrItem item where item.pr.id = :id and item.parent is null");
			query.setParameter("id", prId);
			query.executeUpdate();

			StringBuilder hql = new StringBuilder("update Pr p set p.taxDescription = null, p.total = 0,p.additionalTax = 0,p.grandTotal = 0  where p.id = :prId");
			query = getEntityManager().createQuery(hql.toString());
			query.setParameter("prId", prId);
			query.executeUpdate();

		} catch (NoResultException nr) {
			LOG.info("Error while getting BQ Items : " + nr.getMessage(), nr);
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrItem> findAllChildPrItemByPrId(String prId) {
		final Query query = getEntityManager().createQuery("select distinct a from PrItem a inner join fetch a.pr c where a.parent is not null and c.id = :id order by a.level, a.order");
		query.setParameter("id", prId);
		return query.getResultList();
	}

	@Override
	public boolean checkProductInUse(String productId) {
		LOG.info("proDuctId :" + productId);
		final Query query = getEntityManager().createQuery("select count(pi) from PrItem pi join pi.product p where p.id = :productId");
		query.setParameter("productId", "productId");
		return ((Number) query.getSingleResult()).intValue() > 0;
	}

	@Override
	public Long findProductCategoryCountByPrId(String prId) {
		final Query query = getEntityManager().createQuery("select count(distinct a.productCategory.id) from PrItem a inner join a.pr c where a.productCategory is not null and c.id = :id ");
		query.setParameter("id", prId);
		Long count = query.getSingleResult() != null ? ((Number) query.getSingleResult()).longValue() : 0l;
		return count;
	}
}
