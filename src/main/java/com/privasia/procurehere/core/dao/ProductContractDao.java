package com.privasia.procurehere.core.dao;

import java.util.List;

import com.privasia.procurehere.core.entity.ProductContract;
import com.privasia.procurehere.core.pojo.ProductContractPojo;
import com.privasia.procurehere.core.pojo.TableDataInput;

public interface ProductContractDao extends GenericDao<ProductContract, String> {

	ProductContract findByContractByReferenceNumber(String contractReferenceNumber, String tenantId);

	/**
	 * @param loggedInUserTenantId
	 * @param input
	 * @return
	 */

	List<ProductContractPojo> findProductContractListForTenant(String loggedInUserTenantId, TableDataInput input);

	/**
	 * @param loggedInUserTenantId
	 * @param input
	 * @return
	 */
	long findTotalFilteredProductListForTenant(String loggedInUserTenantId, TableDataInput input);

	/**
	 * @param loggedInUserTenantId
	 * @return
	 */
	long findTotalProductListForTenant(String loggedInUserTenantId);

	/**
	 * @param id
	 * @param loggedInUser
	 * @return
	 */
	ProductContract findProductContractById(String id, String loggedInUser);

	void deleteProductContractbyTenanatId(String tenantId);

}
