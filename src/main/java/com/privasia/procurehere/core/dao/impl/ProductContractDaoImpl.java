package com.privasia.procurehere.core.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.privasia.procurehere.core.dao.ProductContractDao;
import com.privasia.procurehere.core.entity.ProductContract;
import com.privasia.procurehere.core.enums.Status;
import com.privasia.procurehere.core.pojo.ColumnParameter;
import com.privasia.procurehere.core.pojo.OrderParameter;
import com.privasia.procurehere.core.pojo.ProductContractPojo;
import com.privasia.procurehere.core.pojo.TableDataInput;
import com.privasia.procurehere.core.utils.CollectionUtil;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.core.utils.StringUtils;

@Repository
public class ProductContractDaoImpl extends GenericDaoImpl<ProductContract, String> implements ProductContractDao {

	private static final Logger LOG = Logger.getLogger(Global.ADMIN_LOG);

	@SuppressWarnings("unchecked")
	@Override
	public ProductContract findByContractByReferenceNumber(String contractReferenceNumber, String tenantId) {
		LOG.info("contractReferenceNumber" + contractReferenceNumber);
		StringBuilder hsql = new StringBuilder("select distinct t from ProductContract as t left outer join fetch t.productContractItem pci join fetch pci.productItem pi where t.status = :status and t.contractReferenceNumber = :contractReferenceNumber and t.buyer.id = :tenantId");
		final Query query = getEntityManager().createQuery(hsql.toString());
		query.setParameter("status", Status.ACTIVE);
		query.setParameter("contractReferenceNumber", contractReferenceNumber);
		query.setParameter("tenantId", tenantId);
		try {
			List<ProductContract> list = query.getResultList();
			if (CollectionUtil.isNotEmpty(list)) {
				return list.get(0);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProductContractPojo> findProductContractListForTenant(String tenantId, TableDataInput tableParams) {
		final Query query = constructProductListForTenantQuery(tenantId, tableParams, false);
		query.setFirstResult(tableParams.getStart());
		query.setMaxResults(tableParams.getLength());
		return query.getResultList();
	}

	private Query constructProductListForTenantQuery(String tenantId, TableDataInput tableParams, boolean isCount) {

		String hql = "";

		// If count query is enabled, then add the select count(*) clause
		if (isCount) {
			hql += "select count(distinct t) ";
		}
		if (!isCount) {
			hql += "select distinct new com.privasia.procurehere.core.pojo.ProductContractPojo(t.id, t.contractReferenceNumber, t.groupCode, t.contractStartDate, t.contractEndDate, t.contractValue, t.status, t.createdDate, t.modifiedDate, cb.loginId, mb.loginId, s.companyName) from ProductContract t ";
		} else {
			hql += " from ProductContract t ";
		}
		// If this is not a count query, only then add the join fetch. Count query does not require its
		if (!isCount) {
			hql += " left outer join t.createdBy cb left outer join t.modifiedBy mb left outer join t.supplier fs left outer join fs.supplier s ";
		} else {
			hql += " left outer join t.createdBy cb left outer join t.modifiedBy mb left outer join t.supplier fs left outer join fs.supplier s ";
		}

		hql += " where t.buyer.id = :tenantId ";
		boolean isStatusFilterOn = false;

		// Add on search filter conditions
		for (ColumnParameter cp : tableParams.getColumns()) {
			if (Boolean.TRUE == cp.getSearchable() && cp.getSearch() != null && StringUtils.checkString(cp.getSearch().getValue()).length() > 0) {
				if (cp.getData().equals("status")) {
					hql += " and t.status = (:" + cp.getData() + ")";
				} else if (cp.getData().equals("typeString")) {
					hql += " and t.type = (:type)";
				} else if (cp.getData().equals("vendorCode")) {
					hql += " and upper(s.companyName) like (:" + cp.getData() + ")";
				} else if (cp.getData().equals("createBy")) {
					hql += " and upper(cb.loginId) like (:" + cp.getData() + ")";
				} else if (cp.getData().equals("modifiedBy")) {
					hql += " and upper(mb.loginId) like (:" + cp.getData() + ")";
				} else {
					hql += " and upper(t." + cp.getData() + ") like (:" + cp.getData().replace(".", "") + ")";
				}
			}
		}

		if (!isStatusFilterOn) {
			hql += " and t.status = :status ";
		}

		// If it is not a count query then add order by clause
		if (!isCount) {
			List<OrderParameter> orderList = tableParams.getOrder();
			if (CollectionUtil.isNotEmpty(orderList)) {
				hql += " order by ";
				for (OrderParameter order : orderList) {
					String orderColumn = tableParams.getColumns().get(order.getColumn()).getData();
					String dir = order.getDir();
					if (orderColumn.equals("vendorCode")) {
						hql += " s.companyName " + dir + ",";
					} else if (orderColumn.equals("createBy")) {
						hql += " cb.loginId " + dir + ",";
					} else if (orderColumn.equals("modifiedBy")) {
						hql += " mb.loginId " + dir + ",";
					} else {
						hql += " t." + orderColumn + " " + dir + ",";
					}
				}
				if (hql.lastIndexOf(",") == hql.length() - 1) {
					hql = hql.substring(0, hql.length() - 1);
				}
			} else {
				hql += " order by t.contractReferenceNumber ";
			}
		}

		LOG.info("HQL : " + hql + "  Tenant Id : " + tenantId);

		final Query query = getEntityManager().createQuery(hql.toString());
		query.setParameter("tenantId", tenantId);

		// Apply search filter values
		for (ColumnParameter cp : tableParams.getColumns()) {
			if (Boolean.TRUE == cp.getSearchable() && cp.getSearch() != null && StringUtils.checkString(cp.getSearch().getValue()).length() > 0) {
				if (cp.getData().equals("status")) {
					isStatusFilterOn = true;
					query.setParameter("status", Status.valueOf(cp.getSearch().getValue()));
				} else {
					LOG.info("Search by : " + cp.getData() + " value : " + cp.getSearch().getValue().toUpperCase());
					query.setParameter(cp.getData().replace(".", ""), "%" + cp.getSearch().getValue().toUpperCase() + "%");
				}
			}
		}
		// If status search filter is not ON then by default return only active records.
		if (!isStatusFilterOn) {
			query.setParameter("status", Status.ACTIVE);
		}
		return query;
	}

	@Override
	public long findTotalFilteredProductListForTenant(String loggedInUserTenantId, TableDataInput input) {
		final Query query = constructProductListForTenantQuery(loggedInUserTenantId, input, true);
		return ((Number) query.getSingleResult()).longValue();
	}

	@Override
	public long findTotalProductListForTenant(String tenantId) {
		StringBuilder hql = new StringBuilder("select count (t) from ProductContract t where t.status = :status and t.buyer.id = :tenantId ");
		final Query query = getEntityManager().createQuery(hql.toString());
		query.setParameter("status", Status.ACTIVE);
		query.setParameter("tenantId", tenantId);
		return ((Number) query.getSingleResult()).longValue();
	}

	@Override
	public ProductContract findProductContractById(String id, String tenantId) {
		StringBuilder hsql = new StringBuilder("select distinct t from ProductContract as t left outer join fetch t.productContractItem left outer join fetch t.businessUnit bu left outer join fetch t.supplier where t.id = :id and t.buyer.id = :tenantId");
		final Query query = getEntityManager().createQuery(hsql.toString());
		query.setParameter("id", id);
		query.setParameter("tenantId", tenantId);
		return (ProductContract) query.getSingleResult();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void deleteProductContractbyTenanatId(String tenantId) {

		StringBuilder hql = new StringBuilder("select t.id from ProductContract t where t.buyer.id = :tenantId ");
		final Query query = getEntityManager().createQuery(hql.toString());
		query.setParameter("tenantId", tenantId);

		List<String> list = query.getResultList();
		for (String id : list) {
			StringBuilder hsql = new StringBuilder("delete PROC_PRODUCT_CONTRACT_ITEM pc where pc.CONTRACT_ID = :id");
			Query query1 = getEntityManager().createNativeQuery(hsql.toString());
			query1.setParameter("id", id);
			query1.executeUpdate();
		}

		StringBuilder hsql = new StringBuilder("delete PROC_PRODUCT_CONTRACT pc where pc.TENANT_ID= :tenantId");
		Query query1 = getEntityManager().createNativeQuery(hsql.toString());
		query1.setParameter("tenantId", tenantId);
		query1.executeUpdate();

	}

}