/**
 * 
 */
package com.privasia.procurehere.core.dao;

import com.privasia.procurehere.core.entity.BuyerEmailSettings;

/**
 * @author jayshree
 *
 */
public interface BuyerEmailSettingsDao extends GenericDao<BuyerEmailSettings, String>{

	/**
	 * @return
	 */
	BuyerEmailSettings getEmailSetting();

	/**
	 * @param tenantId
	 * @param id
	 * @return
	 */
	BuyerEmailSettings getBuyerEmailSettingsByIdAndTenantId(String tenantId, String id);

	BuyerEmailSettings getEmailSettingByTenantId(String tenantId);

}
