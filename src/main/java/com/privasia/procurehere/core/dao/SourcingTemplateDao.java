package com.privasia.procurehere.core.dao;

import java.util.List;

import com.privasia.procurehere.core.entity.SourcingFormTemplate;
import com.privasia.procurehere.core.pojo.TableDataInput;

/**
 * @author pooja
 */

public interface SourcingTemplateDao extends GenericDao<SourcingFormTemplate, String> {
	/**
	 * @param tenantId
	 * @param input
	 * @param userId
	 * @return List<SourcingFormTemplate>
	 */
	List<SourcingFormTemplate> findAllActiveSourcingTemplateForTenant(String tenantId, TableDataInput input, String userId);

	/**
	 * @param tenantId
	 * @param tableParams
	 * @param userId
	 * @return long
	 */
	long findTotalFilteredTemplatesForTenant(String tenantId, TableDataInput tableParams, String userId);

	/**
	 * @param tenantId
	 * @return long
	 */
	long findTotalTemplatesForTenant(String tenantId);

	/**
	 * @param formId
	 * @param formName
	 * @return boolean
	 */
	boolean isExists(String formId, String formName);

	/**
	 * @param formName
	 * @param tenantId
	 * @return boolean
	 */
	boolean isTemplateExists(String formName, String tenantId);

	/**
	 * @param loggedInUserTenantId
	 * @return List<SourcingFormTemplate>
	 */
	List<SourcingFormTemplate> getSourcingFormbyteantId(String loggedInUserTenantId);

	/**
	 * @param loggedInUserTenantId
	 * @param input
	 * @param id
	 * @return List<SourcingFormTemplate>
	 */

	List<SourcingFormTemplate> getAllTemplate(String loggedInUserTenantId, TableDataInput input, String id);

	/**
	 * @param searchValue
	 * @param loggedInUserTenantId
	 * @param id
	 * @param pageNo
	 * @return
	 */
	List<SourcingFormTemplate> findByTemplateNameForTenant(String searchValue, String tenantId, String pageNo);

	/**
	 * filter list on Sourcing Template Screen
	 * 
	 * @param tenantId
	 * @param input
	 * @param userId
	 * @return
	 */
	List<SourcingFormTemplate> findAllActiveSourcTemplateForTenant(String tenantId, TableDataInput input, String userId);

	/**
	 * @param tenantId
	 * @return
	 */
	List<SourcingFormTemplate> findAllActiveSourcingTemplateForTenantId(String tenantId);

	/**
	 * @param tenantId
	 * @param input
	 * @param userId
	 * @return
	 */
	List<SourcingFormTemplate> findAllAssignActiveSourcingTemplateForTenant(String tenantId, TableDataInput input, String userId);

	/**
	 * @param formId
	 * @return
	 */
	long getBqCount(String formId);

	List<String> getTemplateByUserIdAndTemplateId(String tempId, String loggedInUserTenantId);

}
