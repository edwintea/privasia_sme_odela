/**
 * 
 */
package com.privasia.procurehere.core.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.privasia.procurehere.core.dao.BuyerEmailSettingsDao;
import com.privasia.procurehere.core.entity.BuyerEmailSettings;
import com.privasia.procurehere.core.utils.CollectionUtil;

/**
 * @author jayshree
 *
 */
@Repository
public class BuyerEmailSettingsDaoImpl extends GenericDaoImpl<BuyerEmailSettings, String> implements BuyerEmailSettingsDao {

	@SuppressWarnings("unchecked")
	@Override
	public BuyerEmailSettings getEmailSetting() {
		Query query = getEntityManager().createQuery("select e from BuyerEmailSettings e left outer join e.buyer b ");
//		query.setParameter("tenantId", tenantId);
		List<BuyerEmailSettings> list = query.getResultList();
		if(CollectionUtil.isNotEmpty(list)) {
			return list.get(0);
		}else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public BuyerEmailSettings getBuyerEmailSettingsByIdAndTenantId(String tenantId, String id) {
		Query query = getEntityManager().createQuery("select e from BuyerEmailSettings e left outer join e.buyer b where e.id =:id and b.id =:tenantId ");
		query.setParameter("id", id);
		query.setParameter("tenantId", tenantId);
		List<BuyerEmailSettings> list = query.getResultList();
		if(CollectionUtil.isNotEmpty(list)) {
			return list.get(0);
		}else {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public BuyerEmailSettings getEmailSettingByTenantId(String tenantId) {
		Query query = getEntityManager().createQuery("select e from BuyerEmailSettings e left outer join e.buyer b where b.id =:tenantId");
		query.setParameter("tenantId", tenantId);
		List<BuyerEmailSettings> list = query.getResultList();
		if(CollectionUtil.isNotEmpty(list)) {
			return list.get(0);
		}else {
			return null;
		}
	}

}
