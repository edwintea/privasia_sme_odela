package com.privasia.procurehere.core.dao;

import java.util.List;

import com.privasia.procurehere.core.entity.RfqEventAward;

public interface RfqEventAwardDao extends GenericDao<RfqEventAward, String> {

	RfqEventAward rfqEventAwardByEventIdandBqId(String eventId, String bqId);

	/**
	 * @param eventId
	 * @return
	 */
	List<RfqEventAward> getRfqEventAwardsByEventId(String eventId);

	Double getSumOfAwardedPrice(String id);

	/**
	 * @param eventId
	 * @param bqId
	 * @return
	 */
	RfqEventAward rfqEventAwardDetailsByEventIdandBqId(String eventId, String bqId);

}
