package com.privasia.procurehere.core.dao;

import com.privasia.procurehere.core.entity.SourcingFormCqOption;

/**
 * @author sarang
 */
public interface SourcingFormCqOptionDao extends GenericDao<SourcingFormCqOption, String> {


}
