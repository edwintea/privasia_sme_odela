package com.privasia.procurehere.core.dao;

import java.util.List;

import com.privasia.procurehere.core.entity.CostCenter;
import com.privasia.procurehere.core.pojo.TableDataInput;

public interface CostCenterDao extends GenericDao<CostCenter, String> {

	/**
	 * @param costCenter
	 * @param tenantId TODO
	 * @return
	 */
	boolean isExists(CostCenter costCenter, String tenantId);

	/**
	 * @param tenantId
	 * @return
	 */
	List<CostCenter> getAllActiveCostCentersForTenant(String tenantId);

	/**
	 * @param tenantId
	 * @param tableParams
	 * @return
	 */
	public List<CostCenter> findCostCentersForTenant(String tenantId, TableDataInput tableParams);

	/**
	 * @param tenantId
	 * @param tableParams
	 * @return
	 */
	public long findTotalFilteredCostCentersForTenant(String tenantId, TableDataInput tableParams);

	/**
	 * @param tenantId
	 * @return
	 */
	public long findTotalCostCentersForTenant(String tenantId);

	/**
	 * @param tenantId
	 * @return
	 */

	public List<CostCenter> getAllCostCentersByTenantId(String tenantId);

	/**
	 * @param costCenter
	 * @param tenantId TODO
	 * @return
	 */
	CostCenter getByCostCenter(String costCenter, String tenantId);

	/**
	 * @param tenantId
	 * @return
	 */
	List<CostCenter> getActiveCostCentersForTenant(String tenantId);

	/**
	 * @param costCenterName
	 * @param tenantId
	 * @return
	 */
	CostCenter getActiveCostCenterForTenantByCostCenterName(String costCenterName, String tenantId);

}