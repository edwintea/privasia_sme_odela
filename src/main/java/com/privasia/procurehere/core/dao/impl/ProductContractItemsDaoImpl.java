package com.privasia.procurehere.core.dao.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Query;

import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import com.privasia.procurehere.core.dao.ProductContractItemsDao;
import com.privasia.procurehere.core.entity.ProductCategory;
import com.privasia.procurehere.core.entity.ProductContractItems;
import com.privasia.procurehere.core.pojo.ColumnParameter;
import com.privasia.procurehere.core.pojo.OrderParameter;
import com.privasia.procurehere.core.pojo.ProductContractItemsPojo;
import com.privasia.procurehere.core.pojo.TableDataInput;
import com.privasia.procurehere.core.utils.CollectionUtil;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.core.utils.StringUtils;

@Repository
public class ProductContractItemsDaoImpl extends GenericDaoImpl<ProductContractItems, String> implements ProductContractItemsDao {

	private static final Logger LOG = Logger.getLogger(Global.ADMIN_LOG);

	@Override
	public void deleteBycontractItemNumber(String contractItemNumber) {
		StringBuilder hsql = new StringBuilder("delete from ProductContractItems pc where pc.contractItemNumber = :contractItemNumber");
		Query query = getEntityManager().createQuery(hsql.toString());
		query.setParameter("contractItemNumber", contractItemNumber);
		query.executeUpdate();

	}

	@SuppressWarnings("unchecked")
	@Override
	public ProductContractItems findProductContractItemByItemId(String itemId, String contractItemId) {

		StringBuilder hql = new StringBuilder(" select distinct(t) from ProductContractItems as t inner join t.productContract pc left outer join pc.supplier as fs left outer join fs.supplier as sss where ");

		if (StringUtils.checkString(itemId).length() != 0) {
			hql.append(" t.productItem.id = :productItem ");
		}
		if (StringUtils.checkString(contractItemId).length() != 0) {
			hql.append(" and t.id = :contractItemId ");
		}

		final Query query = getEntityManager().createQuery(hql.toString());
		if (StringUtils.checkString(itemId).length() != 0) {
			query.setParameter("productItem", itemId);
		}
		if (StringUtils.checkString(contractItemId).length() != 0) {
			query.setParameter("contractItemId", contractItemId);
		}
		List<ProductContractItems> uList = query.getResultList();
		if (CollectionUtil.isNotEmpty(uList)) {
			return uList.get(0);
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public ProductContractItems findProductContractItemById(String itemId) {

		StringBuilder hql = new StringBuilder(" select distinct(t) from ProductContractItems as t inner join fetch t.productContract pc left outer join fetch pc.supplier as fs left outer join fetch fs.supplier as sss left outer join fetch t.uom uoms left outer join fetch t.productCategory pct where ");

		if (StringUtils.checkString(itemId).length() != 0) {
			hql.append(" t.id = :productItem ");
		}

		final Query query = getEntityManager().createQuery(hql.toString());
		if (StringUtils.checkString(itemId).length() != 0) {
			query.setParameter("productItem", itemId);

		}
		List<ProductContractItems> uList = query.getResultList();
		if (CollectionUtil.isNotEmpty(uList)) {
			return uList.get(0);
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProductCategory> findProductContractByTenantIDSupplierID(String tenantId, String favSupplierId) {

		String hql = "select  distinct(t.productCategory) from FavouriteSupplier as t inner join t.productCategory where t.buyer.id = :tenantId ";
		if (StringUtils.checkString(favSupplierId).length() > 0) {
			hql += " AND t.id = :favSupplierId ";
		} else {
			hql = "select distinct(t.productCategory) from ProductContractItems as t inner join t.productContract pc left outer join t.productCategory where pc.buyer.id = :tenantId ";
		}

		final Query query = getEntityManager().createQuery(hql.toString());
		query.setParameter("tenantId", tenantId);
		if (StringUtils.checkString(favSupplierId).length() > 0) {
			query.setParameter("favSupplierId", favSupplierId);
		}
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProductContractItemsPojo> findProductContractItemListForTenant(String loggedInUserTenantId, TableDataInput tableParams, String id) {
		final Query query = constructProductListForTenantQuery(loggedInUserTenantId, tableParams, false, id);
		query.setFirstResult(tableParams.getStart());
		query.setMaxResults(tableParams.getLength());
		return query.getResultList();
	}

	private Query constructProductListForTenantQuery(String tenantId, TableDataInput tableParams, boolean isCount, String id) {

		String hql = "";

		// If count query is enabled, then add the select count(*) clause
		if (isCount) {
			hql += "select count(distinct t) ";
		}
		if (!isCount) {
			hql += "select distinct new com.privasia.procurehere.core.pojo.ProductContractItemsPojo(t.id, t.contractItemNumber, i.productName, i.productCode, t.quantity, t.balanceQuantity, t.unitPrice, t.storageLocation, um.uom, bu.unitName, c.costCenter) from ProductContractItems t left outer join t.productContract pc ";
		} else {
			hql += " from ProductContractItems t left outer join t.productContract pc";
		}
		// If this is not a count query, only then add the join fetch. Count query does not require its
		hql += " left outer join t.uom um left outer join t.businessUnit bu left outer join t.costCenter c join t.productItem i ";
		hql += " where  pc.id = :id ";

		// Add on search filter conditions
		for (ColumnParameter cp : tableParams.getColumns()) {
			if (Boolean.TRUE == cp.getSearchable() && cp.getSearch() != null && StringUtils.checkString(cp.getSearch().getValue()).length() > 0) {
				if (cp.getData().equals("typeString")) {
					hql += " and t.type = (:type)";
				} else if (cp.getData().equals("uom")) {
					hql += " and upper(um.uom) like (:" + cp.getData() + ")";
				} else if (cp.getData().equals("storageLoc")) {
					hql += " and upper(t.storageLocation) like (:" + cp.getData() + ")";
				} else if (cp.getData().equals("businessUnit")) {
					hql += " and upper(bu.unitName) like (:" + cp.getData() + ")";
				} else if (cp.getData().equals("itemName")) {
					hql += " and upper(i.productName) like (:" + cp.getData() + ")";
				} else if (cp.getData().equals("itemCode")) {
					hql += " and upper(i.productCode) like (:" + cp.getData() + ")";
				} else if (cp.getData().equals("costCenter")) {
					hql += " and upper(c.costCenter) like (:" + cp.getData() + ")";
				} else if (cp.getData().equals("storageLoc")) {
					hql += " and upper(t.storageLocation) like (:" + cp.getData() + ")";
				} else if (cp.getData().equals("quantity")) {
					try {
						new BigDecimal(cp.getSearch().getValue());
						hql += " and t." + cp.getData() + " = (:" + cp.getData().replace(".", "") + ")";
					} catch (Exception e) {
					}
				} else if (cp.getData().equals("balanceQuantity")) {
					try {
						new BigDecimal(cp.getSearch().getValue());
						hql += " and t." + cp.getData() + " = (:" + cp.getData().replace(".", "") + ")";
					} catch (Exception e) {
					}
				} else if (cp.getData().equals("unitPrice")) {
					try {
						new BigDecimal(cp.getSearch().getValue());
						hql += " and t." + cp.getData() + " = (:" + cp.getData().replace(".", "") + ")";
					} catch (Exception e) {
					}
				} else {
					hql += " and upper(t." + cp.getData() + ") like (:" + cp.getData().replace(".", "") + ")";
				}
			}
		}

		// If it is not a count query then add order by clause
		if (!isCount) {
			List<OrderParameter> orderList = tableParams.getOrder();
			if (CollectionUtil.isNotEmpty(orderList)) {
				hql += " order by ";
				for (OrderParameter order : orderList) {
					String orderColumn = tableParams.getColumns().get(order.getColumn()).getData();
					String dir = order.getDir();
					if (orderColumn.equals("uom")) {
						hql += " um.uom " + dir + ",";
					} else if (orderColumn.equals("itemName")) {
						hql += " i.productName " + dir + ",";
					} else if (orderColumn.equals("itemCode")) {
						hql += " i.productCode " + dir + ",";
					} else if (orderColumn.equals("storageLoc")) {
						hql += " t.storageLocation " + dir + ",";
					} else {
						hql += " t." + orderColumn + " " + dir + ",";
					}
				}
				if (hql.lastIndexOf(",") == hql.length() - 1) {
					hql = hql.substring(0, hql.length() - 1);
				}
			} else {
				hql += " order by t.contractItemNumber ";
			}
		}

		LOG.info("HQL : " + hql + "  Tenant Id : " + tenantId);

		final Query query = getEntityManager().createQuery(hql.toString());
		query.setParameter("id", id);

		// Apply search filter values
		for (ColumnParameter cp : tableParams.getColumns()) {
			if (Boolean.TRUE == cp.getSearchable() && cp.getSearch() != null && StringUtils.checkString(cp.getSearch().getValue()).length() > 0) {
				// String columnName = cp.getData().equals("status");
				if (cp.getData().equals("quantity")) {
					try {
						new BigDecimal(cp.getSearch().getValue());
						query.setParameter(cp.getData().replace(".", ""), new BigDecimal(cp.getSearch().getValue()));
					} catch (Exception e) {
					}
				} else if (cp.getData().equals("balanceQuantity")) {
					try {
						new BigDecimal(cp.getSearch().getValue());
						query.setParameter(cp.getData().replace(".", ""), new BigDecimal(cp.getSearch().getValue()));
					} catch (Exception e) {
					}
				} else if (cp.getData().equals("unitPrice")) {
					try {
						new BigDecimal(cp.getSearch().getValue());
						query.setParameter(cp.getData().replace(".", ""), new BigDecimal(cp.getSearch().getValue()));
					} catch (Exception e) {
					}
				} else {
					query.setParameter(cp.getData().replace(".", ""), "%" + cp.getSearch().getValue().toUpperCase() + "%");
				}
			}
		}
		return query;
	}

	@Override
	public long findTotalFilteredProductItemListForTenant(String loggedInUserTenantId, TableDataInput input, String id) {
		final Query query = constructProductListForTenantQuery(loggedInUserTenantId, input, true, id);
		return ((Number) query.getSingleResult()).longValue();
	}

	@Override
	public long findTotalProductItemListForTenant(String tenantId, String id) {
		StringBuilder hql = new StringBuilder("select count(t) from ProductContractItems t where t.productContract.id = :id");
		final Query query = getEntityManager().createQuery(hql.toString());
		query.setParameter("id", id);
		return ((Number) query.getSingleResult()).longValue();
	}

}