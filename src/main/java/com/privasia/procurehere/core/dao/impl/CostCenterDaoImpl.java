package com.privasia.procurehere.core.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.privasia.procurehere.core.dao.CostCenterDao;
import com.privasia.procurehere.core.entity.CostCenter;
import com.privasia.procurehere.core.enums.Status;
import com.privasia.procurehere.core.pojo.ColumnParameter;
import com.privasia.procurehere.core.pojo.OrderParameter;
import com.privasia.procurehere.core.pojo.TableDataInput;
import com.privasia.procurehere.core.utils.CollectionUtil;
import com.privasia.procurehere.core.utils.StringUtils;

@Repository("costCenterDao")
public class CostCenterDaoImpl extends GenericDaoImpl<CostCenter, String> implements CostCenterDao {

	private static final Logger LOG = Logger.getLogger(CostCenterDaoImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public List<CostCenter> getAllActiveCostCentersForTenant(String tenantId) {
		final Query query = getEntityManager().createQuery("from CostCenter cc left outer join fetch cc.createdBy as cb left outer join fetch cc.modifiedBy as mb where cc.buyer.id = :tenantId and cc.status = :status order by cc.costCenter");
		query.setParameter("tenantId", tenantId);
		query.setParameter("status", Status.ACTIVE);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CostCenter> getActiveCostCentersForTenant(String tenantId) {
		final Query query = getEntityManager().createQuery("select new com.privasia.procurehere.core.entity.CostCenter(cc.id, cc.costCenter,cc.description,cc.status) from CostCenter cc where cc.buyer.id = :tenantId and cc.status = :status order by cc.costCenter");
		query.setParameter("tenantId", tenantId);
		query.setParameter("status", Status.ACTIVE);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public CostCenter getActiveCostCenterForTenantByCostCenterName(String costCenterName, String tenantId) {
		final Query query = getEntityManager().createQuery("select new com.privasia.procurehere.core.entity.CostCenter(cc.id, cc.costCenter, cc.description, cc.status) from CostCenter cc where cc.buyer.id = :tenantId and cc.status = :status and cc.costCenter = :costCenterName ");
		query.setParameter("tenantId", tenantId);
		query.setParameter("status", Status.ACTIVE);
		query.setParameter("costCenterName", costCenterName);
		List<CostCenter> costList = query.getResultList();
		if(CollectionUtil.isNotEmpty(costList)) {
			return costList.get(0);
		}
		return null;
	}


	
	@SuppressWarnings("unchecked")
	@Override
	public boolean isExists(CostCenter costCenter, String tenantId) {
		StringBuilder hsql = new StringBuilder("from CostCenter as cc where upper(cc.costCenter) = upper(:costCenter) and cc.buyer.id = :tenantId ");
		if (StringUtils.checkString(costCenter.getId()).length() > 0) {
			hsql.append(" and cc.id <> :id");
		}
		final Query query = getEntityManager().createQuery(hsql.toString());
		query.setParameter("costCenter", costCenter.getCostCenter());
		query.setParameter("tenantId", tenantId);
		if (StringUtils.checkString(costCenter.getId()).length() > 0) {
			query.setParameter("id", costCenter.getId());
		}
		List<CostCenter> stList = query.getResultList();
		return CollectionUtil.isNotEmpty(stList);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CostCenter> findCostCentersForTenant(String tenantId, TableDataInput tableParams) {
		final Query query = constructCostCenterForTenantQuery(tenantId, tableParams, false);
		query.setFirstResult(tableParams.getStart());
		query.setMaxResults(tableParams.getLength());
		return query.getResultList();
	}

	@Override
	public long findTotalFilteredCostCentersForTenant(String tenantId, TableDataInput tableParams) {
		final Query query = constructCostCenterForTenantQuery(tenantId, tableParams, true);
		return ((Number) query.getSingleResult()).longValue();
	}

	@Override
	public long findTotalCostCentersForTenant(String tenantId) {
		StringBuilder hql = new StringBuilder("select count (c) from CostCenter c where c.status =:status and c.buyer.id = :tenantId ");
		final Query query = getEntityManager().createQuery(hql.toString());
		query.setParameter("status", Status.ACTIVE);
		query.setParameter("tenantId", tenantId);
		return ((Number) query.getSingleResult()).longValue();
	}

	/**
	 * @param tenantId
	 * @param tableParams
	 * @param isCount
	 * @return
	 */
	private Query constructCostCenterForTenantQuery(String tenantId, TableDataInput tableParams, boolean isCount) {

		String hql = "";

		// If count query is enabled, then add the select count(*) clause
		if (isCount) {
			hql += "select count(c) ";
		}

		hql += " from CostCenter c ";

		// If this is not a count query, only then add the join fetch. Count query does not require its
		if (!isCount) {
			hql += " left outer join fetch c.createdBy as cb left outer join fetch c.modifiedBy as mb ";
		}

		hql += " where c.buyer.id = :tenantId ";
		boolean isStatusFilterOn = false;

		// Add on search filter conditions
		for (ColumnParameter cp : tableParams.getColumns()) {
			if (Boolean.TRUE == cp.getSearchable() && cp.getSearch() != null && StringUtils.checkString(cp.getSearch().getValue()).length() > 0) {
				LOG.info("INPUT ::  " + cp.getData() + "VALUE :: " + cp.getSearch().getValue());
				if (cp.getData().equals("status")) {
					hql += " and c.status = (:" + cp.getData() + ")";
				} else {
					hql += " and upper(c." + cp.getData() + ") like (:" + cp.getData().replace(".", "") + ")";
				}
			}
		}
		if (!isStatusFilterOn) {
			hql += " and c.status = :status ";
		}

		// If it is not a count query then add order by clause
		if (!isCount) {
			List<OrderParameter> orderList = tableParams.getOrder();
			if (CollectionUtil.isNotEmpty(orderList)) {
				hql += " order by ";
				for (OrderParameter order : orderList) {
					String orderColumn = tableParams.getColumns().get(order.getColumn()).getData();
					String dir = order.getDir();
					hql += " c." + orderColumn + " " + dir + ",";
				}
				if (hql.lastIndexOf(",") == hql.length() - 1) {
					hql = hql.substring(0, hql.length() - 1);
				}
			}
		}

		// LOG.info("HQL : " + hql);

		final Query query = getEntityManager().createQuery(hql.toString());
		query.setParameter("tenantId", tenantId);

		// Apply search filter values
		for (ColumnParameter cp : tableParams.getColumns()) {
			if (Boolean.TRUE == cp.getSearchable() && cp.getSearch() != null && StringUtils.checkString(cp.getSearch().getValue()).length() > 0) {
				LOG.info("INPUT 1 ::  " + cp.getData() + "VALUE 1 :: " + cp.getSearch().getValue());
				if (cp.getData().equals("status")) {
					isStatusFilterOn = true;
					query.setParameter("status", Status.valueOf(cp.getSearch().getValue()));
				} else {
					query.setParameter(cp.getData().replace(".", ""), "%" + cp.getSearch().getValue().toUpperCase() + "%");
				}
			}
		}
		// If status search filter is not ON then by default return only active records.
		if (!isStatusFilterOn) {
			query.setParameter("status", Status.ACTIVE);
		}
		return query;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CostCenter> getAllCostCentersByTenantId(String tenantId) {
		final Query query = getEntityManager().createQuery("from CostCenter cc where cc.buyer.id = :tenantId order by cc.costCenter");
		LOG.info("Query ======>>>>>" + query);
		query.setParameter("tenantId", tenantId);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public CostCenter getByCostCenter(String costCenter, String tenantId) {
		final Query query = getEntityManager().createQuery("from CostCenter cc where cc.buyer.id = :tenantId and upper(cc.costCenter) = :costCenter ");
		query.setParameter("tenantId", tenantId);
		query.setParameter("costCenter", costCenter.toUpperCase());
		List<CostCenter> list = query.getResultList();
		if (CollectionUtil.isNotEmpty(list)) {
			return list.get(0);
		} else {
			return null;
		}

	}

}
