/**
 * 
 */
package com.privasia.procurehere.core.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.jboss.logging.Logger;
import org.springframework.stereotype.Component;

import com.privasia.procurehere.core.dao.RfqEventAwardAuditDao;
import com.privasia.procurehere.core.entity.RfqEventAwardAudit;

/**
 * @author priyanka
 */
@Component
public class RfqEventAwardAuditDaoImpl extends GenericDaoImpl<RfqEventAwardAudit, String> implements RfqEventAwardAuditDao {

	public static final Logger LOG = Logger.getLogger(RftEventAwardAuditDaoImpl.class);

	@Override
	@SuppressWarnings("unchecked")
	public List<RfqEventAwardAudit> findAllAwardAuditForTenantIdAndEventId(String loggedInUserTenantId, String eventId) {
		final Query query = getEntityManager().createQuery("select distinct New com.privasia.procurehere.core.entity.RfqEventAwardAudit(r.id, ab, r.actionDate, r.description,r.fileName) from RfqEventAwardAudit r left outer join r.actionBy ab where r.event.id =:eventId and r.buyer.id=:tenantId order by r.actionDate desc");
		query.setParameter("eventId", eventId);
		query.setParameter("tenantId", loggedInUserTenantId);
		return query.getResultList();
	}

}
