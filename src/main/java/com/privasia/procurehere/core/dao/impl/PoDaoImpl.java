package com.privasia.procurehere.core.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.hibernate.engine.spi.SessionImplementor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.privasia.procurehere.core.dao.PoDao;
import com.privasia.procurehere.core.dao.UserDao;
import com.privasia.procurehere.core.entity.Po;
import com.privasia.procurehere.core.entity.Pr;
import com.privasia.procurehere.core.enums.PoStatus;
import com.privasia.procurehere.core.enums.PrStatus;
import com.privasia.procurehere.core.pojo.ColumnParameter;
import com.privasia.procurehere.core.pojo.OrderParameter;
import com.privasia.procurehere.core.pojo.PoSupplierPojo;
import com.privasia.procurehere.core.pojo.SearchFilterPoPojo;
import com.privasia.procurehere.core.pojo.TableDataInput;
import com.privasia.procurehere.core.utils.CollectionUtil;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.core.utils.StringUtils;

/**
 * @author Nitin Otageri
 */
@Repository
public class PoDaoImpl extends GenericDaoImpl<Po, String> implements PoDao {

	private static final Logger LOG = Logger.getLogger(Global.PR_LOG);

	@Autowired
	UserDao userDao;

	@Override
	public Po findByPoId(String poId) {
		StringBuilder hsql = new StringBuilder("from Po p left outer join fetch p.supplier sp join fetch p.createdBy cb left outer join fetch p.modifiedBy mb left outer join fetch p.currency c left outer join fetch p.costCenter cc left outer join fetch p.deliveryAddress as da left outer join fetch da.state as dst left outer join fetch dst.country left outer join fetch p.pr pi where p.id = :id");
		Query query = getEntityManager().createQuery(hsql.toString());
		query.setParameter("id", poId);
		Po po = (Po) query.getSingleResult();
		return po;
	}

	@Override
	public Po findPoSupplierByPoId(String poId) {
		StringBuilder hsql = new StringBuilder("select distinct p from Po p left outer join fetch p.supplier sp left outer join fetch p.modifiedBy mb left outer join fetch p.poItems where p.id= :id");
		Query query = getEntityManager().createQuery(hsql.toString());
		query.setParameter("id", poId);
		Po po = (Po) query.getSingleResult();
		po = (Po) getEntityManager().unwrap(SessionImplementor.class).getPersistenceContext().unproxy(po);
		return po;
	}

	@Override
	public long findCountOfPoForSupplierBasedOnStatus(String tenantId, PoStatus status) {
		StringBuilder hsql = new StringBuilder("select count(distinct p) from Po p  left outer join p.supplier as fs where fs.supplier.id = :tenantId and p.status = :status");
		Query query = getEntityManager().createQuery(hsql.toString());
		query.setParameter("tenantId", tenantId);
		query.setParameter("status", status);
		return ((Number) query.getSingleResult()).longValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PoSupplierPojo> findAllSearchFilterPoForSupplierByStatus(String tenantId, TableDataInput input, PoStatus status) {
		final Query query = constructPoForTenantQueryForSupplierByStatus(tenantId, input, false, status);
		query.setFirstResult(input.getStart());
		query.setMaxResults(input.getLength());
		return query.getResultList();

	}

	@Override
	public long findTotalSearchFilterPoForSupplierByStatus(String tenantId, TableDataInput input, PoStatus status) {
		final Query query = constructPoForTenantQueryForSupplierByStatus(tenantId, input, true, status);
		return ((Number) query.getSingleResult()).longValue();
	}

	private Query constructPoForTenantQueryForSupplierByStatus(String tenantId, TableDataInput tableParams, boolean isCount, PoStatus status) {
		String hql = "";

		// If count query is enabled, then add the select count(*) clause
		if (isCount) {
			hql += "select count(distinct p) ";
		} else {
			hql += "select distinct new com.privasia.procurehere.core.pojo.PoSupplierPojo(p.id, p.name, p.modifiedDate, p.grandTotal, p.createdDate,p.decimal, p.referenceNumber ,p.description , p.poNumber, p.status, bu.unitName, c.currencyCode,b.companyName,p.actionDate) ";
		}

		hql += " from Po p ";

		hql += " left outer join p.supplier as fs";

		hql += " left outer join p.businessUnit as bu";

		hql += " left outer join p.currency as c";

		hql += " left outer join p.buyer b";

		hql += " where fs.supplier.id = :tenantId";

		// Add on search filter conditions
		for (ColumnParameter cp : tableParams.getColumns()) {
			if (Boolean.TRUE == cp.getSearchable() && cp.getSearch() != null && StringUtils.checkString(cp.getSearch().getValue()).length() > 0) {
				if (cp.getData().equals("businessUnit")) {
					hql += " and upper(p.businessUnit.unitName) like (:" + cp.getData().replace(".", "") + ")";
				} else if (cp.getData().equals("currency")) {
					hql += " and upper(p.currency.currencyCode) like (:" + cp.getData().replace(".", "") + ")";
				} else if (cp.getData().equals("buyerCompanyName")) {
					hql += " and upper(p.buyer.companyName) like (:" + cp.getData().replace(".", "") + ")";
				} else {
					hql += " and upper(p." + cp.getData() + ") like (:" + cp.getData().replace(".", "") + ") ";
				}
			}
		}

		hql += " and p.status = :status ";

		// If it is not a count query then add order by clause
		if (!isCount) {
			List<OrderParameter> orderList = tableParams.getOrder();
			if (CollectionUtil.isNotEmpty(orderList)) {
				hql += " order by ";
				for (OrderParameter order : orderList) {
					String orderColumn = tableParams.getColumns().get(order.getColumn()).getData();
					String dir = order.getDir();
					if (orderColumn.equalsIgnoreCase("businessUnit")) {
						hql += " p.businessUnit.unitName " + dir + ",";
					} else if (orderColumn.equalsIgnoreCase("currency")) {
						hql += " p.currency.currencyCode " + dir + ",";
					} else if (orderColumn.equalsIgnoreCase("buyerCompanyName")) {
						hql += " p.buyer.companyName " + dir + ",";
					} else if (orderColumn.equalsIgnoreCase("acceptRejectDate")) {
						hql += " p.actionDate " + dir + ",";
					} else {
						hql += " p." + orderColumn + " " + dir + ",";
					}
				}
				if (hql.lastIndexOf(",") == hql.length() - 1) {
					hql = hql.substring(0, hql.length() - 1);
				}
			} else {
				// by default order by created date

				hql += " order by p.createdDate desc ";
			}
		}

		LOG.info("HQL : " + hql);

		final Query query = getEntityManager().createQuery(hql.toString());

		query.setParameter("tenantId", tenantId);

		// Apply search filter values
		for (ColumnParameter cp : tableParams.getColumns()) {
			if (Boolean.TRUE == cp.getSearchable() && cp.getSearch() != null && StringUtils.checkString(cp.getSearch().getValue()).length() > 0) {
				// LOG.info("INPUT 1 :: " + cp.getData() + "VALUE 1 :: " + cp.getSearch().getValue());
				query.setParameter(cp.getData().replace(".", ""), "%" + cp.getSearch().getValue().toUpperCase() + "%");
			}
		}
		query.setParameter("status", status);
		return query;

	}

	@Override
	public long findTotalPo(String tenantId) {
		// StringBuilder hsql = new StringBuilder("select count(distinct p) from Po p where p.buyer.id = :buyerId and
		// p.status = :status and p.erpPrTransferred = :erpPrTransferred");
		StringBuilder hsql = new StringBuilder("select count(distinct p) from Po p where p.buyer.id = :buyerId and p.erpPrTransferred = :erpPrTransferred");
		Query query = getEntityManager().createQuery(hsql.toString());
		query.setParameter("buyerId", tenantId);
		// query.setParameter("status", PrStatus.APPROVED);
		query.setParameter("erpPrTransferred", Boolean.FALSE);
		return ((Number) query.getSingleResult()).longValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Po> findAllPo(String tenantId, TableDataInput input, Date startDate, Date endDate) {
		final Query query = constructPoForTenantQuery(tenantId, input, false, startDate, endDate);
		query.setFirstResult(input.getStart());
		query.setMaxResults(input.getLength());
		return query.getResultList();
	}

	private Query constructPoForTenantQuery(String tenantId, TableDataInput tableParams, boolean isCount, Date startDate, Date endDate) {

		String hql = "";

		// If count query is enabled, then add the select count(*) clause
		if (isCount) {
			hql += "select count(distinct p) ";
		} else {
			hql += "select distinct NEW Po(p.id, p.name, p.modifiedDate, p.grandTotal, cb, mb, p.createdDate, cb.name, mb.name, p.decimal, p.referenceNumber, p.description, p.poNumber,  bu.unitName, sup.companyName, p.supplierName, p.status,p.isPoReportSent,o,p.orderedDate,c.currencyCode,p.actionDate) ";
		}

		hql += " from Po p ";

		// If this is not a count query, only then add the join fetch. Count
		// query does not require its
		if (!isCount) {
			hql += " join p.createdBy as cb left outer join p.modifiedBy mb left outer join p.buyer b left outer join p.orderedBy as o ";
		}

		hql += " left outer join p.businessUnit as bu left outer join p.supplier as fs  left outer join fs.supplier as sup left outer join p.currency as c ";

		hql += " where p.buyer.id = :tenantId and p.erpPrTransferred = :erpPrTransferred ";

		// Add on search filter conditions
		for (ColumnParameter cp : tableParams.getColumns()) {
			if (Boolean.TRUE == cp.getSearchable() && cp.getSearch() != null && StringUtils.checkString(cp.getSearch().getValue()).length() > 0) {
				// LOG.info("INPUT :: " + cp.getData() + "VALUE :: " +
				// cp.getSearch().getValue());
				if (cp.getData().equals("status")) {
					hql += " and p.status in (:status) ";
				} else {
					if (cp.getData().replace(".", "").equalsIgnoreCase("supplierfullName")) {
						hql += " and (upper(p.supplierName) like (:" + cp.getData().replace(".", "") + ") or  upper(sup.companyName) like (:" + cp.getData().replace(".", "") + ")) ";
					} else {
						hql += " and upper(p." + cp.getData() + ") like (:" + cp.getData().replace(".", "") + ") ";

					}
				}
			}
		}

		// search with Date range
		if (startDate != null && endDate != null) {
			hql += " and  p.createdDate between :startDate and :endDate ";
		}

		// If it is not a count query then add order by clause
		if (!isCount) {
			List<OrderParameter> orderList = tableParams.getOrder();
			if (CollectionUtil.isNotEmpty(orderList)) {
				hql += " order by ";
				for (OrderParameter order : orderList) {
					String orderColumn = tableParams.getColumns().get(order.getColumn()).getData();
					String dir = order.getDir();

					if (orderColumn.equalsIgnoreCase("supplier.fullName")) {
						hql += " sup.companyName " + dir + ", p.supplierName " + dir;
					} else {

						hql += " p." + orderColumn + " " + dir + ",";
					}
				}
				if (hql.lastIndexOf(",") == hql.length() - 1) {
					hql = hql.substring(0, hql.length() - 1);
				}
			} else {
				// by default order by created date
				hql += " order by p.createdDate desc ";
			}
		}

		LOG.info("*********prdraftsQuery*********** " + hql);

		final Query query = getEntityManager().createQuery(hql.toString());
		query.setParameter("tenantId", tenantId);
		// Apply search filter values
		for (ColumnParameter cp : tableParams.getColumns()) {
			if (Boolean.TRUE == cp.getSearchable() && cp.getSearch() != null && StringUtils.checkString(cp.getSearch().getValue()).length() > 0) {
				// LOG.info("INPUT 1 :: " + cp.getData() + "VALUE 1 :: " +
				// cp.getSearch().getValue());
				if (cp.getData().equals("status")) {
					if (cp.getSearch().getValue().equalsIgnoreCase("ALL")) {
						List<PoStatus> statuses = Arrays.asList(PoStatus.values());
						query.setParameter("status", statuses);
					} else {
						query.setParameter("status", PrStatus.fromString(cp.getSearch().getValue().toUpperCase()));
					}
				} else {
					query.setParameter(cp.getData().replace(".", ""), "%" + cp.getSearch().getValue().toUpperCase() + "%");
				}
			}
		}

		// set parameter Date range
		if (startDate != null && endDate != null) {
			query.setParameter("startDate", startDate);
			query.setParameter("endDate", endDate);
		}
		query.setParameter("erpPrTransferred", Boolean.FALSE);
		return query;
	}

	@Override
	public long findTotalFilteredPo(String tenantId, TableDataInput input, Date startDate, Date endDate) {
		final Query query = constructPoForTenantQuery(tenantId, input, true, startDate, endDate);
		return ((Number) query.getSingleResult()).longValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PoSupplierPojo> findSearchPoByIds(String tenantId, String[] poArr, boolean select_all, Date startDate, Date endDate, SearchFilterPoPojo searchFilterPoPojo) {
		String hql = "select distinct new com.privasia.procurehere.core.pojo.PoSupplierPojo(p.id, p.name, p.modifiedDate, p.grandTotal, p.createdDate,p.decimal, p.referenceNumber ,p.description , p.poNumber, p.status, bu.unitName, c.currencyCode,b.companyName,p.actionDate) ";

		hql += " from Po p ";

		hql += " left outer join p.supplier as fs";

		hql += " left outer join p.businessUnit as bu";

		hql += " left outer join p.currency as c";

		hql += " left outer join p.buyer b";

		hql += " where fs.supplier.id = :tenantId ";
		
//		hql += "and p.status in(:status) ";

		if (!(select_all)) {
			if (poArr != null && poArr.length > 0) {
				hql += " and p.id in (:poArr)";
				LOG.info("select_all" + select_all);
			}
		}
		List<PoStatus> list = new ArrayList<PoStatus>();
		if (searchFilterPoPojo != null) {
			if (StringUtils.checkString(searchFilterPoPojo.getPoname()).length() > 0) {
				hql += " and upper(p.name) like :name"; 
			}
			if (StringUtils.checkString(searchFilterPoPojo.getBuyer()).length() > 0) {
				hql += " and upper(b.companyName) like :buyerCompanyName";
				LOG.info("buyerCompanyName" + searchFilterPoPojo.getBuyer());
			}
			if (StringUtils.checkString(searchFilterPoPojo.getPonumber()).length() > 0) {
				hql += " and upper(p.poNumber) like :poNumber";
			}
			if (StringUtils.checkString(searchFilterPoPojo.getCurrency()).length() > 0) {
				hql += " and upper(c.currencyCode) like :currency";
			}
			if (StringUtils.checkString(searchFilterPoPojo.getBusinessunit()).length() > 0) {
				hql += " and upper(bu.unitName) like :businessunit";
			}
			if (StringUtils.checkString(searchFilterPoPojo.getDescription()).length() > 0) {
				hql += " and upper(p.description) like :description";
			}
			if (searchFilterPoPojo.getPostatus() != null ) {
				hql += " and upper(p.status) like :postatus";
				
			} else {
				hql += " and upper(p.status) in :postatus)";
			}
		}

		// search with Date range
		if (startDate != null && endDate != null) {
			hql += " and p.createdDate between :startDate and :endDate ";
		}

		hql += " order by p.createdDate desc";

		final Query query = getEntityManager().createQuery(hql);

		if (!(select_all)) {
			if (poArr != null && poArr.length > 0) {
				query.setParameter("poArr", Arrays.asList(poArr));
			}
		}

		if (searchFilterPoPojo != null) {
			if (StringUtils.checkString(searchFilterPoPojo.getPoname()).length() > 0) {
				query.setParameter("name", "%" + searchFilterPoPojo.getPoname().toUpperCase() + "%");
			}
			if (StringUtils.checkString(searchFilterPoPojo.getBuyer()).length() > 0) {
				query.setParameter("buyerCompanyName", "%" + searchFilterPoPojo.getBuyer().toUpperCase() + "%");
			}
			if (StringUtils.checkString(searchFilterPoPojo.getPonumber()).length() > 0) {
				query.setParameter("poNumber", "%" + searchFilterPoPojo.getPonumber().toUpperCase() + "%");
			}
			if (StringUtils.checkString(searchFilterPoPojo.getCurrency()).length() > 0) {
				query.setParameter("currency", "%" + searchFilterPoPojo.getCurrency().toUpperCase() + "%");
			}

			if (StringUtils.checkString(searchFilterPoPojo.getBusinessunit()).length() > 0) {
				query.setParameter("businessunit", "%" + searchFilterPoPojo.getBusinessunit().toUpperCase() + "%");
			}
			if (StringUtils.checkString(searchFilterPoPojo.getDescription()).length() > 0) {
				query.setParameter("description", "%" + searchFilterPoPojo.getDescription().toUpperCase() + "%");
			}
			if (searchFilterPoPojo.getPostatus() != null ) {
				LOG.info("getPostatus" + searchFilterPoPojo.getPostatus());
					query.setParameter("postatus", searchFilterPoPojo.getPostatus());
			} else {
				query.setParameter("postatus", Arrays.asList(PoStatus.ORDERED, PoStatus.ACCEPTED, PoStatus.DECLINED, PoStatus.CANCELLED));
			}

		}

		if (startDate != null && endDate != null) {
			query.setParameter("startDate", startDate);
			query.setParameter("endDate", endDate);
		}
		query.setParameter("tenantId", tenantId);
//		query.setParameter("status", Arrays.asList(PoStatus.ORDERED, PoStatus.ACCEPTED, PoStatus.DECLINED, PoStatus.CANCELLED));
		return query.getResultList();

	}

	@Override
	public long findPoCountBasedOnStatusAndTenant(String loggedInUser, String tenantId, PoStatus status) {
		String hql = "select count(distinct p) from Po p where p.buyer.id = :tenantId and p.erpPrTransferred = :erpPrTransferred and p.status =:status ";
		if (StringUtils.checkString(loggedInUser).length() > 0) {
			hql += " and p.createdBy.id =:createdBy ";
		}
		final Query query = getEntityManager().createQuery(hql.toString());
		query.setParameter("tenantId", tenantId);
		query.setParameter("erpPrTransferred", Boolean.FALSE);
		query.setParameter("status", status);
		if (StringUtils.checkString(loggedInUser).length() > 0) {
			query.setParameter("createdBy", loggedInUser);
		}

		return ((Number) query.getSingleResult()).longValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Po> findAllPoByStatus(String loggedInUser, String tenantId, TableDataInput input, PoStatus status) {
		final Query query = constructPoForTenantQueryByStatus(loggedInUser, tenantId, input, false, status);
		query.setFirstResult(input.getStart());
		query.setMaxResults(input.getLength());
		return query.getResultList();
	}

	private Query constructPoForTenantQueryByStatus(String loggedInUser, String tenantId, TableDataInput tableParams, boolean isCount, PoStatus status) {

		String hql = "";

		// If count query is enabled, then add the select count(*) clause
		if (isCount) {
			hql += "select count(distinct p) ";
		} else {
			hql += "select distinct NEW Po(p.id, p.name, p.modifiedDate, p.grandTotal, cb, mb, p.createdDate, cb.name, mb.name, p.decimal, p.referenceNumber, p.description, p.poNumber,  bu.unitName, sup.companyName, p.supplierName, p.status,p.isPoReportSent,o,p.orderedDate,c.currencyCode,p.actionDate) ";
		}

		hql += " from Po p ";

		// If this is not a count query, only then add the join fetch. Count
		// query does not require its
		if (!isCount) {
			hql += " join p.createdBy as cb left outer join p.modifiedBy mb left outer join p.buyer b  left outer join p.orderedBy as o";
		}

		hql += " left outer join p.businessUnit as bu left outer join p.supplier as fs  left outer join fs.supplier as sup left outer join p.currency as c ";

		hql += " where p.buyer.id = :tenantId and p.erpPrTransferred = :erpPrTransferred and p.status =:status ";
		if (StringUtils.checkString(loggedInUser).length() > 0) {
			hql += " and p.createdBy.id =:createdBy ";
		}

		// Add on search filter conditions
		for (ColumnParameter cp : tableParams.getColumns()) {
			if (Boolean.TRUE == cp.getSearchable() && cp.getSearch() != null && StringUtils.checkString(cp.getSearch().getValue()).length() > 0) {
				if (cp.getData().replace(".", "").equalsIgnoreCase("supplierfullName")) {
					hql += " and (upper(p.supplierName) like (:" + cp.getData().replace(".", "") + ") or  upper(sup.companyName) like (:" + cp.getData().replace(".", "") + ")) ";
				} else {
					hql += " and upper(p." + cp.getData() + ") like (:" + cp.getData().replace(".", "") + ") ";

				}
			}
		}

		// If it is not a count query then add order by clause
		if (!isCount) {
			List<OrderParameter> orderList = tableParams.getOrder();
			if (CollectionUtil.isNotEmpty(orderList)) {
				hql += " order by ";
				for (OrderParameter order : orderList) {
					String orderColumn = tableParams.getColumns().get(order.getColumn()).getData();
					String dir = order.getDir();

					if (orderColumn.equalsIgnoreCase("supplier.fullName")) {
						hql += " sup.companyName " + dir + ", p.supplierName " + dir;
					} else {

						hql += " p." + orderColumn + " " + dir + ",";
					}
				}
				if (hql.lastIndexOf(",") == hql.length() - 1) {
					hql = hql.substring(0, hql.length() - 1);
				}
			} else {
				// by default order by created date
				hql += " order by p.createdDate desc ";
			}
		}

		LOG.info("*********prdraftsQuery*********** " + hql);

		final Query query = getEntityManager().createQuery(hql.toString());
		query.setParameter("tenantId", tenantId);
		// Apply search filter values
		for (ColumnParameter cp : tableParams.getColumns()) {
			if (Boolean.TRUE == cp.getSearchable() && cp.getSearch() != null && StringUtils.checkString(cp.getSearch().getValue()).length() > 0) {
				query.setParameter(cp.getData().replace(".", ""), "%" + cp.getSearch().getValue().toUpperCase() + "%");
			}
		}

		query.setParameter("erpPrTransferred", Boolean.FALSE);
		query.setParameter("status", status);
		if (StringUtils.checkString(loggedInUser).length() > 0) {
			query.setParameter("createdBy", loggedInUser);
		}
		return query;
	}

	@Override
	public long findTotalFilteredPoByStatus(String loggedInUserId, String tenantId, TableDataInput input, PoStatus status) {
		final Query query = constructPoForTenantQueryByStatus(loggedInUserId, tenantId, input, true, status);
		return ((Number) query.getSingleResult()).longValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Po> findAllSearchFilterPo(String loggedInUserId, String loggedInUserTenantId, TableDataInput input, Date startDate, Date endDate) {
		Query query = constructAllPoForTenantQuery(loggedInUserId, loggedInUserTenantId, input, false, startDate, endDate);
		query.setFirstResult(input.getStart());
		query.setMaxResults(input.getLength());
		return query.getResultList();
	}

	@Override
	public long findTotalSearchFilterPoCount(String loggedInUserId, String loggedInUserTenantId, TableDataInput input, Date startDate, Date endDate) {
		final Query query = constructAllPoForTenantQuery(loggedInUserId, loggedInUserTenantId, input, true, startDate, endDate);
		return ((Number) query.getSingleResult()).longValue();
	}

	private Query constructAllPoForTenantQuery(String loggedInUserId, String tenantId, TableDataInput tableParams, boolean isCount, Date startDate, Date endDate) {

		String hql = "";

		// If count query is enabled, then add the select count(*) clause
		if (isCount) {
			hql += "select count(distinct p) ";
		} else {
			hql += "select distinct NEW Po(p.id, p.name, p.modifiedDate, p.grandTotal, cb, mb, p.createdDate, cb.name, mb.name, p.decimal, p.referenceNumber, p.description, p.poNumber,  bu.unitName, sup.companyName, p.supplierName, p.status,p.isPoReportSent,o,p.orderedDate,c.currencyCode,p.actionDate) ";
		}

		hql += " from Po p ";

		// If this is not a count query, only then add the join fetch. Count
		// query does not require its
		if (!isCount) {
			hql += " join p.createdBy as cb left outer join p.modifiedBy mb left outer join p.buyer b  left outer join p.orderedBy as o";
		}

		hql += " left outer join p.businessUnit as bu left outer join p.supplier as fs  left outer join fs.supplier as sup left outer join p.currency as c ";

		hql += " where p.buyer.id = :tenantId ";
		if (StringUtils.checkString(loggedInUserId).length() > 0) {
			hql += " and p.createdBy.id =:createdBy";
		}
		boolean isSelectFilterOn = false;

		// Add on search filter conditions
		for (ColumnParameter cp : tableParams.getColumns()) {
			if (Boolean.TRUE == cp.getSearchable() && cp.getSearch() != null && StringUtils.checkString(cp.getSearch().getValue()).length() > 0) {
				if (cp.getData().equals("status")) {
					hql += " and p.status in (:status) ";
					isSelectFilterOn = true;
				} else if (cp.getData().replace(".", "").equalsIgnoreCase("supplierfullName")) {
					hql += " and (upper(p.supplierName) like (:" + cp.getData().replace(".", "") + ") or  upper(sup.companyName) like (:" + cp.getData().replace(".", "") + ")) ";
				} else {
					hql += " and upper(p." + cp.getData() + ") like (:" + cp.getData().replace(".", "") + ") ";

				}
			}
		}

		if (!isSelectFilterOn) {
			hql += " and p.status in (:statuses) ";
		}

		// search with Date range
		if (startDate != null && endDate != null) {
			hql += " and  p.createdDate between :startDate and :endDate ";
		}

		// If it is not a count query then add order by clause
		if (!isCount) {
			List<OrderParameter> orderList = tableParams.getOrder();
			if (CollectionUtil.isNotEmpty(orderList)) {
				hql += " order by ";
				for (OrderParameter order : orderList) {
					String orderColumn = tableParams.getColumns().get(order.getColumn()).getData();
					String dir = order.getDir();

					if (orderColumn.equalsIgnoreCase("supplier.fullName")) {
						hql += " sup.companyName " + dir + ", p.supplierName " + dir;
					} else {

						hql += " p." + orderColumn + " " + dir + ",";
					}
				}
				if (hql.lastIndexOf(",") == hql.length() - 1) {
					hql = hql.substring(0, hql.length() - 1);
				}
			} else {
				hql += " order by p.createdDate desc ";
			}
		}

		LOG.info("*********posQuery*********** " + hql);

		final Query query = getEntityManager().createQuery(hql.toString());
		query.setParameter("tenantId", tenantId);
		// Apply search filter values
		for (ColumnParameter cp : tableParams.getColumns()) {
			if (Boolean.TRUE == cp.getSearchable() && cp.getSearch() != null && StringUtils.checkString(cp.getSearch().getValue()).length() > 0) {
				if (cp.getData().equals("status")) {
					if (cp.getSearch().getValue().equalsIgnoreCase("ALL")) {
						List<PoStatus> statuss = Arrays.asList(PoStatus.READY, PoStatus.ORDERED, PoStatus.ACCEPTED, PoStatus.DECLINED, PoStatus.CANCELLED);
						query.setParameter("status", statuss);
					} else {
						query.setParameter("status", PoStatus.fromString(cp.getSearch().getValue().toUpperCase()));
					}
				} else {
					query.setParameter(cp.getData().replace(".", ""), "%" + cp.getSearch().getValue().toUpperCase() + "%");
				}
			}
		}
		if (!isSelectFilterOn) {
			query.setParameter("statuses", Arrays.asList(PoStatus.READY, PoStatus.ORDERED, PoStatus.ACCEPTED, PoStatus.DECLINED, PoStatus.CANCELLED));
		}
		// set parameter Date range
		if (startDate != null && endDate != null) {
			query.setParameter("startDate", startDate);
			query.setParameter("endDate", endDate);
		}
		if (StringUtils.checkString(loggedInUserId).length() > 0) {
			query.setParameter("createdBy", loggedInUserId);
		}
		return query;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Po> findSearchBuyerPoByIds(String tenantId, String[] poArr, boolean select_all, Date startDate, Date endDate, SearchFilterPoPojo searchFilterPoPojo, String loggedInUser) {
		String hql = "select p from Po p inner join p.createdBy cr left outer join p.orderedBy as o left outer join p.businessUnit as bu left outer join p.supplier as fs left outer join fs.supplier as sup left outer join p.currency c";
		
//		hql += "where p.status in (:status) and";         //commented bcoz we r setting status in searchFilterPoPojo for both conditions
		
		hql	+= " where p.buyer.id = :tenantId";
		
		if (!(select_all)) {
			if (poArr != null && poArr.length > 0) {
				hql += " and p.id in (:poArr)";
				LOG.info("select_all" + select_all);
			}
		}
		if (StringUtils.checkString(loggedInUser).length() > 0) {
			hql += " and p.createdBy.id = :createdBy ";
		}

		List<PoStatus> list = new ArrayList<PoStatus>();
		if (searchFilterPoPojo != null) {
			if (StringUtils.checkString(searchFilterPoPojo.getPoname()).length() > 0) {
				hql += " and upper(p.name) like :poName";
			}
			if (StringUtils.checkString(searchFilterPoPojo.getSupplier()).length() > 0) {
				hql += " and upper(sup.companyName) like :suppliername";
				LOG.info("getSuppliername" + searchFilterPoPojo.getSupplier());
			}
			if (StringUtils.checkString(searchFilterPoPojo.getPonumber()).length() > 0) {
				hql += " and upper(p.poNumber) like :ponumber";
			}
			if (StringUtils.checkString(searchFilterPoPojo.getPocreatedby()).length() > 0) {
				hql += " and upper(cr.name) like :pocreatedby";
			}
			if (StringUtils.checkString(searchFilterPoPojo.getBusinessunit()).length() > 0) {
				hql += " and upper(bu.unitName) like :businessunit";
			}
			if (StringUtils.checkString(searchFilterPoPojo.getCurrency()).length() > 0) {
				hql += " and upper(c.currencyCode) like :currency";
			}
			if (StringUtils.checkString(searchFilterPoPojo.getPoorderedby()).length() > 0) {
				hql += " and upper(o.name) like :orderedby";
			}
			if (searchFilterPoPojo.getPostatus() != null) {
				hql += " and upper(p.status) like :postatus";
			} else {
				hql += " and upper(p.status) in(:postatus)";
			}
		}

		// search with Date range
		if (startDate != null && endDate != null) {
			hql += " and p.createdDate between :startDate and :endDate ";
		}
		
		hql += " order by p.createdDate desc ";

		final Query query = getEntityManager().createQuery(hql);

		if (!(select_all)) {
			if (poArr != null && poArr.length > 0) {
				query.setParameter("poArr", Arrays.asList(poArr));
			}
		}
		if (StringUtils.checkString(loggedInUser).length() > 0) {
			query.setParameter("createdBy", loggedInUser);
		}

		if (searchFilterPoPojo != null) {
			LOG.info("---------name -----"+searchFilterPoPojo.getPoname());
			if (StringUtils.checkString(searchFilterPoPojo.getPoname()).length() > 0) {
				query.setParameter("poName", "%" + searchFilterPoPojo.getPoname().toUpperCase() + "%");
			}
			if (StringUtils.checkString(searchFilterPoPojo.getSupplier()).length() > 0) {
				query.setParameter("suppliername", "%" + searchFilterPoPojo.getSupplier().toUpperCase() + "%");
				LOG.info("getSuppliername" + searchFilterPoPojo.getSupplier().toUpperCase());
			}
			if (StringUtils.checkString(searchFilterPoPojo.getPonumber()).length() > 0) {
				query.setParameter("ponumber", "%" + searchFilterPoPojo.getPonumber().toUpperCase() + "%");
			}
			if (StringUtils.checkString(searchFilterPoPojo.getPocreatedby()).length() > 0) {
				query.setParameter("pocreatedby", "%" + searchFilterPoPojo.getPocreatedby().toUpperCase() + "%");
			}
			if (StringUtils.checkString(searchFilterPoPojo.getBusinessunit()).length() > 0) {
				query.setParameter("businessunit", "%" + searchFilterPoPojo.getBusinessunit().toUpperCase() + "%");
			}
			if (StringUtils.checkString(searchFilterPoPojo.getCurrency()).length() > 0) {
				query.setParameter("currency", searchFilterPoPojo.getCurrency());
			}
			if (StringUtils.checkString(searchFilterPoPojo.getPoorderedby()).length() > 0) {
				query.setParameter("orderedby", "%" + searchFilterPoPojo.getPoorderedby().toUpperCase() + "%");
			}
			if (searchFilterPoPojo.getPostatus() != null) {
				LOG.info("getPostatus" + searchFilterPoPojo.getPostatus());
				query.setParameter("postatus", searchFilterPoPojo.getPostatus());
			}else {
				query.setParameter("postatus", Arrays.asList(PoStatus.READY, PoStatus.ORDERED, PoStatus.ACCEPTED, PoStatus.DECLINED, PoStatus.CANCELLED));
			}

		}

		if (startDate != null && endDate != null) {
			query.setParameter("startDate", startDate);
			query.setParameter("endDate", endDate);
		}
//		List<PoStatus> statuses = Arrays.asList(PoStatus.READY, PoStatus.ORDERED, PoStatus.ACCEPTED, PoStatus.DECLINED, PoStatus.CANCELLED);
		query.setParameter("tenantId", tenantId);
//		query.setParameter("status", statuses);
		return query.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Po> findSupplierAllPo(String tenantId) {

		String hql = new String("select distinct p from Po p left outer join fetch p.createdBy cb left outer join fetch p.costCenter cc left outer join fetch p.currency pc left outer join fetch p.supplier fs left outer join fetch p.poItems pi left outer join fetch p.buyer b ");
		hql += " where fs.supplier.id = :tenantId and p.status not in (:status)  and p.isPo = true and p.createdDate is not null and  p.poNumber is not null";
		List<PrStatus> statuses = Arrays.asList(PrStatus.CANCELED, PrStatus.DRAFT);
		Query query = getEntityManager().createQuery(hql);
		query.setParameter("status", statuses);
		query.setParameter("tenantId", tenantId);
		return query.getResultList();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Po> findAllPoforSharingAll(String supplierId, String buyerId) {

		String hql = "select p from Po p left outer join p.supplier as fs  where fs.supplier.id = :tenantId and p.status= :status ";

		if (StringUtils.checkString(buyerId).length() > 0) {
			hql += " and p.buyer.id =:buyerId";
		}
		final Query query = getEntityManager().createQuery(hql);
		query.setParameter("tenantId", supplierId);
		query.setParameter("status", PoStatus.ACCEPTED);
		if (StringUtils.checkString(buyerId).length() > 0) {
			query.setParameter("buyerId", buyerId);
		}

		return query.getResultList();
	}

	@Override
	public String getBusineessUnitname(String poId) {
		StringBuilder hsql = new StringBuilder("select bu.displayName from Po p left outer join p.businessUnit as bu where p.id =:poId ");
		final Query query = getEntityManager().createQuery(hsql.toString());
		query.setParameter("poId", poId);
		return (String) query.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Po> getAllPoFromGlobalSearch(String searchVal, String tenantId, String status, String type, Date startDate, Date endDate) {
		LOG.info("searchValue:" + searchVal + " tenantId : " + tenantId);

		List<PoStatus> statusList = new ArrayList<PoStatus>();

		if (StringUtils.checkString(status).length() > 0) {
			String[] statusArray = status.split(",");
			if (statusArray != null && statusArray.length > 0) {
				for (String ss : statusArray) {
					statusList.add(PoStatus.valueOf(ss));
				}
			}
		}
		StringBuilder hsql;
		hsql = new StringBuilder("select p from Po p left outer join fetch p.businessUnit as bu left outer join fetch p.supplier as fs where fs.supplier.id = :tenantId ");

		if (StringUtils.checkString(searchVal).length() > 0) {
			LOG.info(searchVal);
			hsql.append(" and ((upper(p.poNumber) like :searchVal) or (upper(p.referenceNumber) like :searchVal) or (upper(p.name) like :searchVal) or (upper(p.description) like :searchVal) or (upper(bu.unitName) like :searchVal)) ");
		}
		hsql.append(" and (p.status in (:status)) ");
		if (startDate != null && endDate != null) {
			hsql.append(" and (p.createdDate between :startDate and :endDate) ");
		}
		LOG.info("HSQL :" + hsql);
		Query query = getEntityManager().createQuery(hsql.toString());

		query.setParameter("tenantId", tenantId);
		if (StringUtils.checkString(searchVal).length() > 0) {
			query.setParameter("searchVal", "%" + searchVal.toUpperCase() + "%");
		}
		if (StringUtils.checkString(status).length() > 0) {
			query.setParameter("status", statusList);
		} else {
			query.setParameter("status", Arrays.asList(PoStatus.ORDERED, PoStatus.ACCEPTED, PoStatus.DECLINED, PoStatus.CANCELLED));
		}
		if (startDate != null && endDate != null) {
			query.setParameter("startDate", startDate);
			query.setParameter("endDate", endDate);
		}

		List<Po> list = query.getResultList();
		LinkedHashSet<Po> resultList = new LinkedHashSet<Po>();
		for (Po po : list) {
			resultList.add(po);
		}
		return new ArrayList<Po>(resultList);
	}

	@Override
	public Po findByPrId(String prId) {
		StringBuilder hsql = new StringBuilder("from Po p where p.pr.id = :prId");
		Query query = getEntityManager().createQuery(hsql.toString());
		query.setParameter("prId", prId);
		Po po = (Po) query.getSingleResult();
		return po;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Po findSupplierByFavSupplierId(String id) {
		StringBuilder hsql = new StringBuilder("from Po p left outer join fetch p.supplier as fs where fs.id = :id");
		final Query query = getEntityManager().createQuery(hsql.toString());
		query.setParameter("id", id);
		List<Po> uList = query.getResultList();
		if (CollectionUtil.isNotEmpty(uList)) {
			return uList.get(0);
		}
		return null;
	}
}