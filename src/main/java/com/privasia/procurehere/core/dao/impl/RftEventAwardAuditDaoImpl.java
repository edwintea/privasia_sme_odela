/**
 * 
 */
package com.privasia.procurehere.core.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.jboss.logging.Logger;
import org.springframework.stereotype.Component;

import com.privasia.procurehere.core.dao.RftEventAwardAuditDao;
import com.privasia.procurehere.core.entity.RftEventAwardAudit;

/**
 * @author priyanka
 */
@Component
public class RftEventAwardAuditDaoImpl extends GenericDaoImpl<RftEventAwardAudit, String> implements RftEventAwardAuditDao {

	public static final Logger LOG = Logger.getLogger(RftEventAwardAuditDaoImpl.class);

	@Override
	@SuppressWarnings("unchecked")
	public List<RftEventAwardAudit> findAllAwardAuditForTenantIdAndEventId(String loggedInUserTenantId, String eventId) {
		final Query query = getEntityManager().createQuery("select distinct New com.privasia.procurehere.core.entity.RftEventAwardAudit(r.id, ab, r.actionDate, r.description,r.fileName) from RftEventAwardAudit r left outer join r.actionBy ab where r.event.id =:eventId and r.buyer.id=:tenantId order by r.actionDate desc");
		query.setParameter("eventId", eventId);
		query.setParameter("tenantId", loggedInUserTenantId);
		return query.getResultList();
	}

}
