package com.privasia.procurehere.core.dao.impl;

import com.privasia.procurehere.core.dao.SupplierMandatoryDocumentDao;
import com.privasia.procurehere.core.entity.SupplierCompanyProfile;
import com.privasia.procurehere.core.entity.SupplierMandatoryDocument;
import com.privasia.procurehere.core.utils.CollectionUtil;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class SupplierMandatoryDocumentDaoImpl extends GenericDaoImpl<SupplierMandatoryDocument, String> implements SupplierMandatoryDocumentDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<SupplierMandatoryDocument> findAll() {
		final Query query = getEntityManager().createQuery("from SupplierMandatoryDocument a inner join fetch a.user u order by a.createdDate desc ");
		return query.getResultList();
	}

	@Override
	public  SupplierMandatoryDocument findByIds(String id) {
		final Query query = getEntityManager().createQuery("from SupplierMandatoryDocument a where a.id= :id");
		query.setParameter("id", id);
		return (SupplierMandatoryDocument) query.getSingleResult();
	}

	@Override
	public boolean  isExistsByActiveTitle(SupplierMandatoryDocument document) {
		final Query query = getEntityManager().createQuery("from SupplierMandatoryDocument a where a.description= :title AND a.status= :status");
		query.setParameter("title", document.getDescription());
		query.setParameter("status", true);
		return CollectionUtil.isNotEmpty(query.getResultList());
	}

	@Override
	public SupplierMandatoryDocument saveOrUpdate(SupplierMandatoryDocument document){
		return super.saveOrUpdate(document);
	}

	public void deleteById(String id){
		final Query query = getEntityManager().createQuery("delete from SupplierMandatoryDocument a where a.id= :id ");
		query.setParameter("id", id);
		query.executeUpdate();
	}
}
