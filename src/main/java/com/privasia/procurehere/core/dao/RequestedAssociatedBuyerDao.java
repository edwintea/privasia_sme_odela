package com.privasia.procurehere.core.dao;

import com.privasia.procurehere.core.entity.Buyer;
import com.privasia.procurehere.core.entity.RequestedAssociatedBuyer;

import java.util.List;

public interface RequestedAssociatedBuyerDao extends GenericDao<RequestedAssociatedBuyer, String> {

    List<RequestedAssociatedBuyer> findAll();

    RequestedAssociatedBuyer findByIds(String id);

    public void deleteById(String id);

}
