package com.privasia.procurehere.core.dao;

import com.privasia.procurehere.core.entity.SupplierMandatoryDocument;

import java.util.List;

public interface SupplierMandatoryDocumentDao extends GenericDao<SupplierMandatoryDocument, String> {

    List<SupplierMandatoryDocument> findAll();

    SupplierMandatoryDocument findByIds(String id);

    boolean isExistsByActiveTitle(SupplierMandatoryDocument document);

    public void deleteById(String id);

}
