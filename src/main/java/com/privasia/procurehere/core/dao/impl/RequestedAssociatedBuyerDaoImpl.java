package com.privasia.procurehere.core.dao.impl;

import com.privasia.procurehere.core.dao.RequestedAssociatedBuyerDao;
import com.privasia.procurehere.core.entity.Buyer;
import com.privasia.procurehere.core.entity.RequestedAssociatedBuyer;
import com.privasia.procurehere.core.entity.SupplierMandatoryDocument;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class RequestedAssociatedBuyerDaoImpl extends GenericDaoImpl<RequestedAssociatedBuyer, String> implements RequestedAssociatedBuyerDao {

    @Override
    public List<RequestedAssociatedBuyer> findAll() {
        final Query query = getEntityManager().createQuery("from RequestedAssociatedBuyer a inner join fetch a.user u ");
        return query.getResultList();
    }

    @Override
    public RequestedAssociatedBuyer findByIds(String id) {
        final Query query = getEntityManager().createQuery("from RequestedAssociatedBuyer a where a.id= :id");
        query.setParameter("id", id);
        return (RequestedAssociatedBuyer) query.getSingleResult();
    }

    @Override
    public RequestedAssociatedBuyer saveOrUpdate(RequestedAssociatedBuyer requestedAssociatedBuyer){
        return super.saveOrUpdate(requestedAssociatedBuyer);
    }

    @Override
    public void deleteById(String id){
        final Query query = getEntityManager().createQuery("delete from RequestedAssociatedBuyer a where a.id= :id ");
        query.setParameter("id", id);
        query.executeUpdate();
    }

}
