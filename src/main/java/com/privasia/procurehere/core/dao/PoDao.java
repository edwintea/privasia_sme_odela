package com.privasia.procurehere.core.dao;

import java.util.Date;
import java.util.List;

import com.privasia.procurehere.core.entity.Po;
import com.privasia.procurehere.core.enums.PoStatus;
import com.privasia.procurehere.core.pojo.PoSupplierPojo;
import com.privasia.procurehere.core.pojo.SearchFilterPoPojo;
import com.privasia.procurehere.core.pojo.TableDataInput;

/**
 * @author Nitin Otageri
 */
public interface PoDao extends GenericDao<Po, String> {

	/**
	 * @param poId
	 * @return
	 */
	Po findByPoId(String poId);

	/**
	 * @param poId
	 * @return
	 */
	Po findPoSupplierByPoId(String poId);

	/**
	 * @param tenantId
	 * @param status TODO
	 * @return
	 */
	long findCountOfPoForSupplierBasedOnStatus(String tenantId, PoStatus status);

	/**
	 * @param tenantId
	 * @param input
	 * @param status
	 * @return
	 */
	List<PoSupplierPojo> findAllSearchFilterPoForSupplierByStatus(String tenantId, TableDataInput input, PoStatus status);

	/**
	 * @param tenantId
	 * @param input
	 * @param status
	 * @return
	 */
	long findTotalSearchFilterPoForSupplierByStatus(String tenantId, TableDataInput input, PoStatus status);

	/**
	 * @param tenantId
	 * @return
	 */
	long findTotalPo(String tenantId);

	/**
	 * @param tenantId
	 * @param input
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<Po> findAllPo(String tenantId, TableDataInput input, Date startDate, Date endDate);

	/**
	 * @param tenantId
	 * @param input
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	long findTotalFilteredPo(String tenantId, TableDataInput input, Date startDate, Date endDate);

	/**
	 * @param tenantId
	 * @param poArr
	 * @param select_all
	 * @param startDate
	 * @param endDate
	 * @param searchFilterPoPojo
	 * @return
	 */
	List<PoSupplierPojo> findSearchPoByIds(String tenantId, String[] poArr, boolean select_all, Date startDate, Date endDate, SearchFilterPoPojo searchFilterPoPojo);

	/**
	 * @param loggedInUser TODO
	 * @param tenantId
	 * @param status
	 * @return
	 */
	long findPoCountBasedOnStatusAndTenant(String loggedInUser, String tenantId, PoStatus status);

	/**
	 * @param loggedInUser TODO
	 * @param tenantId
	 * @param input
	 * @param status
	 * @return
	 */
	List<Po> findAllPoByStatus(String loggedInUser, String tenantId, TableDataInput input, PoStatus status);

	/**
	 * @param loggedInUserId TODO
	 * @param tenantId
	 * @param input
	 * @param status
	 * @return
	 */
	long findTotalFilteredPoByStatus(String loggedInUserId, String tenantId, TableDataInput input, PoStatus status);

	/**
	 * @param loggedInUserId TODO
	 * @param loggedInUserTenantId
	 * @param input
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<Po> findAllSearchFilterPo(String loggedInUserId, String loggedInUserTenantId, TableDataInput input, Date startDate, Date endDate);

	/**
	 * @param loggedInUserId TODO
	 * @param loggedInUserTenantId
	 * @param input
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	long findTotalSearchFilterPoCount(String loggedInUserId, String loggedInUserTenantId, TableDataInput input, Date startDate, Date endDate);

	/**
	 * @param tenantId
	 * @param poArr
	 * @param select_all
	 * @param startDate
	 * @param endDate
	 * @param searchFilterPoPojo
	 * @param loggedInUser TODO
	 * @return
	 */
	List<Po> findSearchBuyerPoByIds(String tenantId, String[] poArr, boolean select_all, Date startDate, Date endDate, SearchFilterPoPojo searchFilterPoPojo, String loggedInUser);

	/**
	 * @param tenantId
	 * @return
	 */
	List<Po> findSupplierAllPo(String tenantId);

	/**
	 * @param supplierId
	 * @param buyerId
	 * @return
	 */
	List<Po> findAllPoforSharingAll(String supplierId, String buyerId);

	/**
	 * @param poId
	 * @return
	 */
	String getBusineessUnitname(String poId);

	/**
	 * @param searchVal
	 * @param tenantId
	 * @param status
	 * @param type
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<Po> getAllPoFromGlobalSearch(String searchVal, String tenantId, String status, String type, Date startDate, Date endDate);

	/**
	 * @param prId
	 * @return
	 */
	Po findByPrId(String prId);

	/**
	 * @param id
	 * @return
	 */
	Po findSupplierByFavSupplierId(String id);

}
