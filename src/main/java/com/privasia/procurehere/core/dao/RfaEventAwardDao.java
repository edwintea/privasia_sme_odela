package com.privasia.procurehere.core.dao;

import java.util.List;

import com.privasia.procurehere.core.entity.RfaEventAward;

public interface RfaEventAwardDao extends GenericDao<RfaEventAward, String> {

	RfaEventAward rfaEventAwardByEventIdandBqId(String eventId, String bqId);

	/**
	 * @param eventId
	 * @return
	 */
	List<RfaEventAward> getRfaEventAwardsByEventId(String eventId);

	Double getSumOfAwardedPrice(String eventId);

	/**
	 * @param eventId
	 * @param bqId
	 * @return
	 */
	RfaEventAward rfaEventAwardDetailsByEventIdandBqId(String eventId, String bqId);

}
