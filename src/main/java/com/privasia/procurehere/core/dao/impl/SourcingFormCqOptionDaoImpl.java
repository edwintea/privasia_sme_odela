package com.privasia.procurehere.core.dao.impl;

import org.jboss.logging.Logger;
import org.springframework.stereotype.Repository;

import com.privasia.procurehere.core.dao.SourcingFormCqOptionDao;
import com.privasia.procurehere.core.entity.SourcingFormCqOption;
import com.privasia.procurehere.core.utils.Global;

/**
 * @author sarang
 */
@Repository
public class SourcingFormCqOptionDaoImpl extends GenericDaoImpl<SourcingFormCqOption, String> implements SourcingFormCqOptionDao {
	private static final Logger LOG = Logger.getLogger(Global.PR_LOG);
	
}
