/**
 * 
 */
package com.privasia.procurehere.core.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * @author ravi
 */
public class ErpPoPojo implements Serializable {

	private static final long serialVersionUID = -5293785785202614619L;

	private String businesUnit;

	private String poDate;

	private String erpPoDocNo;

	private String supplierCode;

	private String supplierName;

	private String supplierAddress1;

	private String supplierAddress2;

	private String faxNumber;

	private String telephoneNumber;

	private String supplierRegistrationNo;

	private String currency;

	private String paymentTerm;

	private List<ErpPoItemPojo> itemList;

	/**
	 * @return the businesUnit
	 */
	public String getBusinesUnit() {
		return businesUnit;
	}

	/**
	 * @param businesUnit the businesUnit to set
	 */
	public void setBusinesUnit(String businesUnit) {
		this.businesUnit = businesUnit;
	}

	/**
	 * @return the poDate
	 */
	public String getPoDate() {
		return poDate;
	}

	/**
	 * @param poDate the poDate to set
	 */
	public void setPoDate(String poDate) {
		this.poDate = poDate;
	}

	/**
	 * @return the erpPoDocNo
	 */
	public String getErpPoDocNo() {
		return erpPoDocNo;
	}

	/**
	 * @param erpPoDocNo the erpPoDocNo to set
	 */
	public void setErpPoDocNo(String erpPoDocNo) {
		this.erpPoDocNo = erpPoDocNo;
	}

	/**
	 * @return the supplierCode
	 */
	public String getSupplierCode() {
		return supplierCode;
	}

	/**
	 * @param supplierCode the supplierCode to set
	 */
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	/**
	 * @return the supplierName
	 */
	public String getSupplierName() {
		return supplierName;
	}

	/**
	 * @param supplierName the supplierName to set
	 */
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	/**
	 * @return the supplierAddress1
	 */
	public String getSupplierAddress1() {
		return supplierAddress1;
	}

	/**
	 * @param supplierAddress1 the supplierAddress1 to set
	 */
	public void setSupplierAddress1(String supplierAddress1) {
		this.supplierAddress1 = supplierAddress1;
	}

	/**
	 * @return the supplierAddress2
	 */
	public String getSupplierAddress2() {
		return supplierAddress2;
	}

	/**
	 * @param supplierAddress2 the supplierAddress2 to set
	 */
	public void setSupplierAddress2(String supplierAddress2) {
		this.supplierAddress2 = supplierAddress2;
	}

	/**
	 * @return the faxNumber
	 */
	public String getFaxNumber() {
		return faxNumber;
	}

	/**
	 * @param faxNumber the faxNumber to set
	 */
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	/**
	 * @return the telephoneNumber
	 */
	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	/**
	 * @param telephoneNumber the telephoneNumber to set
	 */
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	/**
	 * @return the supplierRegistrationNo
	 */
	public String getSupplierRegistrationNo() {
		return supplierRegistrationNo;
	}

	/**
	 * @param supplierRegistrationNo the supplierRegistrationNo to set
	 */
	public void setSupplierRegistrationNo(String supplierRegistrationNo) {
		this.supplierRegistrationNo = supplierRegistrationNo;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the paymentTerm
	 */
	public String getPaymentTerm() {
		return paymentTerm;
	}

	/**
	 * @param paymentTerm the paymentTerm to set
	 */
	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	/**
	 * @return the itemList
	 */
	public List<ErpPoItemPojo> getItemList() {
		return itemList;
	}

	/**
	 * @param itemList the itemList to set
	 */
	public void setItemList(List<ErpPoItemPojo> itemList) {
		this.itemList = itemList;
	}

}
