/**
 * 
 */
package com.privasia.procurehere.core.pojo;

/**
 * @author ravi
 */
public class HrmsIcDetails {

	private String employeeNo;
	private String staffName;
	private String newICNO;
	private String email;
	private String nextOfKinName;
	private String nextOfKinStaffNo;
	private String nextOfKinICNo;
	private String relationship;
	private String connectedParty;
	private String businessRegNo;

	/**
	 * @return the employeeNo
	 */
	public String getEmployeeNo() {
		return employeeNo;
	}

	/**
	 * @param employeeNo the employeeNo to set
	 */
	public void setEmployeeNo(String employeeNo) {
		this.employeeNo = employeeNo;
	}

	/**
	 * @return the staffName
	 */
	public String getStaffName() {
		return staffName;
	}

	/**
	 * @param staffName the staffName to set
	 */
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	/**
	 * @return the newICNO
	 */
	public String getNewICNO() {
		return newICNO;
	}

	/**
	 * @param newICNO the newICNO to set
	 */
	public void setNewICNO(String newICNO) {
		this.newICNO = newICNO;
	}

	/**
	 * @return the nextOfKinICNo
	 */
	public String getNextOfKinICNo() {
		return nextOfKinICNo;
	}

	/**
	 * @param nextOfKinICNo the nextOfKinICNo to set
	 */
	public void setNextOfKinICNo(String nextOfKinICNo) {
		this.nextOfKinICNo = nextOfKinICNo;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the nextOfKinName
	 */
	public String getNextOfKinName() {
		return nextOfKinName;
	}

	/**
	 * @param nextOfKinName the nextOfKinName to set
	 */
	public void setNextOfKinName(String nextOfKinName) {
		this.nextOfKinName = nextOfKinName;
	}

	/**
	 * @return the nextOfKinStaffNo
	 */
	public String getNextOfKinStaffNo() {
		return nextOfKinStaffNo;
	}

	/**
	 * @param nextOfKinStaffNo the nextOfKinStaffNo to set
	 */
	public void setNextOfKinStaffNo(String nextOfKinStaffNo) {
		this.nextOfKinStaffNo = nextOfKinStaffNo;
	}

	/**
	 * @return the relationship
	 */
	public String getRelationship() {
		return relationship;
	}

	/**
	 * @param relationship the relationship to set
	 */
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	/**
	 * @return the connectedParty
	 */
	public String getConnectedParty() {
		return connectedParty;
	}

	/**
	 * @param connectedParty the connectedParty to set
	 */
	public void setConnectedParty(String connectedParty) {
		this.connectedParty = connectedParty;
	}

	/**
	 * @return the businessRegNo
	 */
	public String getBusinessRegNo() {
		return businessRegNo;
	}

	/**
	 * @param businessRegNo the businessRegNo to set
	 */
	public void setBusinessRegNo(String businessRegNo) {
		this.businessRegNo = businessRegNo;
	}

}
