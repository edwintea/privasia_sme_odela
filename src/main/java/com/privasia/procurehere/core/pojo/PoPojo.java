package com.privasia.procurehere.core.pojo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author Nitin Otageri
 */
public class PoPojo implements Serializable {

	private static final long serialVersionUID = 1678519231573639563L;

	@ApiModelProperty(notes = "Document Type", required = true)
	private String docType;

	@ApiModelProperty(notes = "Reference No", required = true)
	private String referenceNumber;

	@ApiModelProperty(notes = "Created Date in YYYYMMDD format", required = false)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
	private Date createdDate;

	@ApiModelProperty(notes = "Name", required = true)
	private String name;

	@ApiModelProperty(notes = "Created By", required = true)
	private String createdBy;

	@ApiModelProperty(notes = "Requester", required = true)
	private String requester;

	@ApiModelProperty(notes = "Correspondence Address", required = false)
	private AddressPojo correspondenceAddress;

	@ApiModelProperty(notes = "Currency", required = true)
	private String currency;

	@ApiModelProperty(notes = "Decimal", required = true)
	private Integer decimal;

	@ApiModelProperty(notes = "Cost Center Code", required = false)
	private String costCenter;

	@ApiModelProperty(notes = "Business Unit Code", required = false)
	private String businessUnit;

	@ApiModelProperty(notes = "Available Budget", required = false)
	private BigDecimal availableBudget;

	@ApiModelProperty(notes = "Payment Term", required = true)
	private String paymentTerm;

	@ApiModelProperty(notes = "Delivery Address", required = false)
	private AddressPojo deliveryAddress;

	@ApiModelProperty(notes = "Delivery Date in YYYYMMDD format", required = false)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
	private Date deliveryDate;

	@ApiModelProperty(notes = "Delivery Receiver", required = false)
	private String deliveryReceiver;

	@ApiModelProperty(notes = "Supplier Name", required = false)
	private String supplierName;

	@ApiModelProperty(notes = "Supplier Code", required = true)
	private String supplierCode;

	@ApiModelProperty(notes = "Grand Total", required = true)
	private BigDecimal grandTotal;

	@ApiModelProperty(notes = "remarks", required = false)
	private String remarks;

	@ApiModelProperty(notes = "Terms And Conditions", required = false)
	private String termsAndConditions;

	@ApiModelProperty(notes = "List of Items", required = true)
	private List<PoItemsPojo> itemList;

	/**
	 * @return the docType
	 */
	public String getDocType() {
		return docType;
	}

	/**
	 * @param docType the docType to set
	 */
	public void setDocType(String docType) {
		this.docType = docType;
	}

	/**
	 * @return the referenceNumber
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * @param referenceNumber the referenceNumber to set
	 */
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the requester
	 */
	public String getRequester() {
		return requester;
	}

	/**
	 * @param requester the requester to set
	 */
	public void setRequester(String requester) {
		this.requester = requester;
	}

	/**
	 * @return the correspondenceAddress
	 */
	public AddressPojo getCorrespondenceAddress() {
		return correspondenceAddress;
	}

	/**
	 * @param correspondenceAddress the correspondenceAddress to set
	 */
	public void setCorrespondenceAddress(AddressPojo correspondenceAddress) {
		this.correspondenceAddress = correspondenceAddress;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the decimal
	 */
	public Integer getDecimal() {
		return decimal;
	}

	/**
	 * @param decimal the decimal to set
	 */
	public void setDecimal(Integer decimal) {
		this.decimal = decimal;
	}

	/**
	 * @return the costCenter
	 */
	public String getCostCenter() {
		return costCenter;
	}

	/**
	 * @param costCenter the costCenter to set
	 */
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}

	/**
	 * @return the businessUnit
	 */
	public String getBusinessUnit() {
		return businessUnit;
	}

	/**
	 * @param businessUnit the businessUnit to set
	 */
	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	/**
	 * @return the availableBudget
	 */
	public BigDecimal getAvailableBudget() {
		return availableBudget;
	}

	/**
	 * @param availableBudget the availableBudget to set
	 */
	public void setAvailableBudget(BigDecimal availableBudget) {
		this.availableBudget = availableBudget;
	}

	/**
	 * @return the paymentTerm
	 */
	public String getPaymentTerm() {
		return paymentTerm;
	}

	/**
	 * @param paymentTerm the paymentTerm to set
	 */
	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	/**
	 * @return the deliveryAddress
	 */
	public AddressPojo getDeliveryAddress() {
		return deliveryAddress;
	}

	/**
	 * @param deliveryAddress the deliveryAddress to set
	 */
	public void setDeliveryAddress(AddressPojo deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	/**
	 * @return the deliveryDate
	 */
	public Date getDeliveryDate() {
		return deliveryDate;
	}

	/**
	 * @param deliveryDate the deliveryDate to set
	 */
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	/**
	 * @return the deliveryReceiver
	 */
	public String getDeliveryReceiver() {
		return deliveryReceiver;
	}

	/**
	 * @param deliveryReceiver the deliveryReceiver to set
	 */
	public void setDeliveryReceiver(String deliveryReceiver) {
		this.deliveryReceiver = deliveryReceiver;
	}

	/**
	 * @return the supplierName
	 */
	public String getSupplierName() {
		return supplierName;
	}

	/**
	 * @param supplierName the supplierName to set
	 */
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	/**
	 * @return the supplierCode
	 */
	public String getSupplierCode() {
		return supplierCode;
	}

	/**
	 * @param supplierCode the supplierCode to set
	 */
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	/**
	 * @return the grandTotal
	 */
	public BigDecimal getGrandTotal() {
		return grandTotal;
	}

	/**
	 * @param grandTotal the grandTotal to set
	 */
	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the termsAndConditions
	 */
	public String getTermsAndConditions() {
		return termsAndConditions;
	}

	/**
	 * @param termsAndConditions the termsAndConditions to set
	 */
	public void setTermsAndConditions(String termsAndConditions) {
		this.termsAndConditions = termsAndConditions;
	}

	/**
	 * @return the itemList
	 */
	public List<PoItemsPojo> getItemList() {
		return itemList;
	}

	/**
	 * @param itemList the itemList to set
	 */
	public void setItemList(List<PoItemsPojo> itemList) {
		this.itemList = itemList;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PoPojo [docType=" + docType + ", referenceNumber=" + referenceNumber + ", createdDate=" + createdDate + ", name=" + name + ", createdBy=" + createdBy + ", requester=" + requester + ", correspondenceAddress=" + correspondenceAddress + ", currency=" + currency + ", decimal=" + decimal + ", costCenter=" + costCenter + ", businessUnit=" + businessUnit + ", availableBudget=" + availableBudget + ", paymentTerm=" + paymentTerm + ", deliveryAddress=" + deliveryAddress + ", deliveryDate=" + deliveryDate + ", deliveryReceiver=" + deliveryReceiver + ", supplierName=" + supplierName + ", supplierCode=" + supplierCode + ", grandTotal=" + grandTotal + ", remarks=" + remarks + ", termsAndConditions=" + termsAndConditions + ", itemList=" + itemList + "]";
	}

}
