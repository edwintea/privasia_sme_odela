package com.privasia.procurehere.core.pojo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;

import com.privasia.procurehere.core.enums.ProductItemType;

public class ProductContractItemsPojo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1139685009212583850L;

	@ApiModelProperty(required = false, hidden = true)
	private String id;

	@ApiModelProperty(notes = "Item Contract Number", allowableValues = "range[1, 5]", required = true)
	private String contractItemNumber;

	@ApiModelProperty(notes = "Item Code", allowableValues = "range[1, 15]", required = true)
	private String itemCode;

	@ApiModelProperty(notes = "Item Name", allowableValues = "range[1, 32]", required = false)
	private String itemName;

	@ApiModelProperty(notes = "Unit Price", allowableValues = "range[1, 5]")
	private BigDecimal unitPrice;

	@ApiModelProperty(notes = "Item Category", required = true)
	private String itemCategory;

	@ApiModelProperty(notes = "Plant Code", required = true, hidden = false)
	private String businessUnit;

	@ApiModelProperty(notes = "Uom Code", required = true)
	private String uom;

	@ApiModelProperty(notes = "Item Type - MATERIAL or SERVICE", required = false, hidden = false)
	private ProductItemType itemType;

	@ApiModelProperty(notes = "Quantity", allowableValues = "range[1, 5]", required = true)
	private BigDecimal quantity;

	@ApiModelProperty(notes = "Balance Quantity", allowableValues = "range[1, 5]", hidden = true)
	private BigDecimal balanceQuantity;

	@ApiModelProperty(notes = "Tax Code", allowableValues = "range[1, 15]", required = false)
	private String taxCode;

	@ApiModelProperty(notes = "Remarks", allowableValues = "range[1, 200]", required = false)
	private String remarks;

	@ApiModelProperty(notes = "Storage Location", required = false)
	private String storageLoc;

	@ApiModelProperty(notes = "Cost Center", required = false)
	private String costCenter;

	public ProductContractItemsPojo() {

	}

	/**
	 * @param id
	 * @param contractItemNumber
	 * @param unitPrice
	 * @param quantity
	 */
	public ProductContractItemsPojo(String id, String contractItemNumber, String itemName, String itemCode, BigDecimal quantity, BigDecimal balanceQuantity, BigDecimal unitPrice, String storageLocation, String uom, String businessUnit, String costCenter) {
		this.id = id;
		this.contractItemNumber = contractItemNumber;
		this.balanceQuantity = balanceQuantity;
		this.quantity = quantity;
		this.unitPrice = unitPrice;
		this.storageLoc = storageLocation;
		this.uom = uom;
		this.businessUnit = businessUnit;
		this.costCenter = costCenter;
		this.itemCode = itemCode;
		this.itemName = itemName;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the contractItemNumber
	 */
	public String getContractItemNumber() {
		return contractItemNumber;
	}

	/**
	 * @param contractItemNumber the contractItemNumber to set
	 */
	public void setContractItemNumber(String contractItemNumber) {
		this.contractItemNumber = contractItemNumber;
	}

	/**
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the balanceQuantity
	 */
	public BigDecimal getBalanceQuantity() {
		return balanceQuantity;
	}

	/**
	 * @param balanceQuantity the balanceQuantity to set
	 */
	public void setBalanceQuantity(BigDecimal balanceQuantity) {
		this.balanceQuantity = balanceQuantity;
	}

	/**
	 * @return the unitPrice
	 */
	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	/**
	 * @param unitPrice the unitPrice to set
	 */
	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	/**
	 * @return the itemCategory
	 */
	public String getItemCategory() {
		return itemCategory;
	}

	/**
	 * @param itemCategory the itemCategory to set
	 */
	public void setItemCategory(String itemCategory) {
		this.itemCategory = itemCategory;
	}

	/**
	 * @return the businessUnit
	 */
	public String getBusinessUnit() {
		return businessUnit;
	}

	/**
	 * @param businessUnit the businessUnit to set
	 */
	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	/**
	 * @return the uom
	 */
	public String getUom() {
		return uom;
	}

	/**
	 * @param uom the uom to set
	 */
	public void setUom(String uom) {
		this.uom = uom;
	}

	/**
	 * @return the itemCode
	 */
	public String getItemCode() {
		return itemCode;
	}

	/**
	 * @param itemCode the itemCode to set
	 */
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	/**
	 * @return the itemType
	 */
	public ProductItemType getItemType() {
		return itemType;
	}

	/**
	 * @param itemType the itemType to set
	 */
	public void setItemType(ProductItemType itemType) {
		this.itemType = itemType;
	}

	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return itemName;
	}

	/**
	 * @param itemName the itemName to set
	 */
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	/**
	 * @return the taxCode
	 */
	public String getTaxCode() {
		return taxCode;
	}

	/**
	 * @param taxCode the taxCode to set
	 */
	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the storageLoc
	 */
	public String getStorageLoc() {
		return storageLoc;
	}

	/**
	 * @param storageLoc the storageLoc to set
	 */
	public void setStorageLoc(String storageLoc) {
		this.storageLoc = storageLoc;
	}

	/**
	 * @return the costCenter
	 */
	public String getCostCenter() {
		return costCenter;
	}

	/**
	 * @param costCenter the costCenter to set
	 */
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProductContractItemsPojo [id=" + id + ", contractItemNumber=" + contractItemNumber + ", itemCode=" + itemCode + ", itemName=" + itemName + ", unitPrice=" + unitPrice + ", itemCategory=" + itemCategory + ", businessUnit=" + businessUnit + ", uom=" + uom + ", itemType=" + itemType + ", quantity=" + quantity + ", balanceQuantity=" + balanceQuantity + ", taxCode=" + taxCode + ", remarks=" + remarks + ", storageLoc=" + storageLoc + "]";
	}

}
