package com.privasia.procurehere.core.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * @author ravi
 */
public class TechOnePrCancelPojo implements Serializable {

	private static final long serialVersionUID = -9093033940754489556L;

	private String pridNumber;
	private String poStatus;
	private Date timestamp;

	/**
	 * @return the pridNumber
	 */
	public String getPridNumber() {
		return pridNumber;
	}

	/**
	 * @param pridNumber the pridNumber to set
	 */
	public void setPridNumber(String pridNumber) {
		this.pridNumber = pridNumber;
	}

	/**
	 * @return the poStatus
	 */
	public String getPoStatus() {
		return poStatus;
	}

	/**
	 * @param poStatus the poStatus to set
	 */
	public void setPoStatus(String poStatus) {
		this.poStatus = poStatus;
	}

	/**
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String toLogString() {
		return "TechOnePrCancelPojo [pridNumber=" + pridNumber + ", poStatus=" + poStatus + ", timestamp=" + timestamp + "]";
	}

	
}
