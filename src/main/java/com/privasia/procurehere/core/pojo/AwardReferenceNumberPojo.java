package com.privasia.procurehere.core.pojo;
import java.io.Serializable;

/**
 * 
 * @author Priyanka Ghadage
 *
 */
public class AwardReferenceNumberPojo implements Serializable {

	private static final long serialVersionUID = 7307586918621103651L;

	private Integer level;

	private Integer order;

	private String referenceNumber;

	public AwardReferenceNumberPojo() {
	}

	public AwardReferenceNumberPojo(Integer level, Integer order, String referenceNumber) {
		this.level = level;
		this.order = order;
		this.referenceNumber = referenceNumber;
	}

	/**
	 * @return the level
	 */
	public Integer getLevel() {
		return level;
	}

	/**
	 * @param level the level to set
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}

	/**
	 * @return the order
	 */
	public Integer getOrder() {
		return order;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder(Integer order) {
		this.order = order;
	}

	/**
	 * @return the referenceNumber
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * @param referenceNumber the referenceNumber to set
	 */
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String toLogString() {
		return "AwardReferenceNumber [level=" + level + ", order=" + order + ", referenceNumber=" + referenceNumber + "]";
	}
}

