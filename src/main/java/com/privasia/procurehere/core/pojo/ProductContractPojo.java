package com.privasia.procurehere.core.pojo;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.privasia.procurehere.core.enums.OperationType;
import com.privasia.procurehere.core.enums.Status;
import com.privasia.procurehere.core.utils.CustomDateTimeSerializer;

/**
 * @author pavan
 */
public class ProductContractPojo implements Serializable {

	private static final long serialVersionUID = -7094290956150607806L;

	@ApiModelProperty(required = false, hidden = true)
	private String id;

	@ApiModelProperty(notes = "Contract Reference Number", allowableValues = "range[1, 10]", required = true)
	private String contractReferenceNumber;

	@ApiModelProperty(notes = "Purchasing Group Code", allowableValues = "range[1, 9]", required = true)
	private String groupCode;

	@ApiModelProperty(notes = "Start Date in YYYYMMDD format", required = true, hidden = false)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone="GMT+08:00")
	private Date contractStartDate;

	@ApiModelProperty(notes = "End Date in YYYYMMDD  format", required = true, hidden = false)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone="GMT+08:00")
	private Date contractEndDate;

	@ApiModelProperty(notes = "Contract Value", allowableValues = "range[1, 15]", required = true)
	private BigDecimal contractValue;

	@ApiModelProperty(notes = "Status", required = true)
	private Status status;

	@ApiModelProperty(required = false, hidden = true)
	private String createdBy;

	@ApiModelProperty(notes = "Operation", required = true)
	private OperationType operation;

	@ApiModelProperty(notes = "Vendor Code", required = false, hidden = false)
	private String vendorCode;

	@ApiModelProperty(required = false, hidden = true)
	private String modifiedBy;

	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@ApiModelProperty(required = false, hidden = true)
	private Date createdDate;

	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@ApiModelProperty(required = false, hidden = true)
	private Date modifiedDate;

	@ApiModelProperty(notes = "Business Unit Code", required = false, hidden = false)
	private String businessUnit;

	@ApiModelProperty(notes = "List of Items", required = true, hidden = false)
	private List<ProductContractItemsPojo> itemList;

	/**
	 * @param id
	 * @param contractReferenceNumber
	 * @param groupCode
	 * @param contractStartDate
	 * @param contractEndDate
	 * @param contractValue
	 * @param status
	 * @param createdBy
	 * @param vendorCode
	 * @param modifiedBy
	 * @param createdDate
	 * @param modifiedDate
	 */
	public ProductContractPojo(String id, String contractReferenceNumber, String groupCode, Date contractStartDate, Date contractEndDate, BigDecimal contractValue, Status status, Date createdDate, Date modifiedDate, String createBy, String modifiedBy, String companyName) {
		this.id = id;
		this.contractReferenceNumber = contractReferenceNumber;
		this.groupCode = groupCode;
		this.contractStartDate = contractStartDate;
		this.contractEndDate = contractEndDate;
		this.contractValue = contractValue;
		this.status = status;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.createdBy = createBy;
		this.modifiedBy = modifiedBy;
		this.vendorCode = companyName;

	}

	/**
	 * @return the itemList
	 */
	public List<ProductContractItemsPojo> getItemList() {
		return itemList;
	}

	/**
	 * @param itemList the itemList to set
	 */
	public void setItemList(List<ProductContractItemsPojo> itemList) {
		this.itemList = itemList;
	}

	/**
	 * @return the businessUnit
	 */
	public String getBusinessUnit() {
		return businessUnit;
	}

	/**
	 * @param businessUnit the businessUnit to set
	 */
	public void setBusinessUnit(String businessUnit) {
		this.businessUnit = businessUnit;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the modifiedDate
	 */
	public Date getModifiedDate() {
		return modifiedDate;
	}

	/**
	 * @param modifiedDate the modifiedDate to set
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 * @return the vendorCode
	 */
	public String getVendorCode() {
		return vendorCode;
	}

	/**
	 * @param vendorCode the vendorCode to set
	 */
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	/**
	 * @return the operation
	 */
	public OperationType getOperation() {
		return operation;
	}

	/**
	 * @param operation the operation to set
	 */
	public void setOperation(OperationType operation) {
		this.operation = operation;
	}

	public ProductContractPojo() {
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	/**
	 * @return the contractReferenceNumber
	 */
	public String getContractReferenceNumber() {
		return contractReferenceNumber;
	}

	/**
	 * @param contractReferenceNumber the contractReferenceNumber to set
	 */
	public void setContractReferenceNumber(String contractReferenceNumber) {
		this.contractReferenceNumber = contractReferenceNumber;
	}

	/**
	 * @return the groupCode
	 */
	public String getGroupCode() {
		return groupCode;
	}

	/**
	 * @return the contractStartDate
	 */
	public Date getContractStartDate() {
		return contractStartDate;
	}

	/**
	 * @param contractStartDate the contractStartDate to set
	 */
	public void setContractStartDate(Date contractStartDate) {
		this.contractStartDate = contractStartDate;
	}

	/**
	 * @return the contractEndDate
	 */
	public Date getContractEndDate() {
		return contractEndDate;
	}

	/**
	 * @param contractEndDate the contractEndDate to set
	 */
	public void setContractEndDate(Date contractEndDate) {
		this.contractEndDate = contractEndDate;
	}

	/**
	 * @param groupCode the groupCode to set
	 */
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	/**
	 * @return the contractValue
	 */
	public BigDecimal getContractValue() {
		return contractValue;
	}

	/**
	 * @param contractValue the contractValue to set
	 */
	public void setContractValue(BigDecimal contractValue) {
		this.contractValue = contractValue;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProductContractPojo [id=" + id + ", contractReferenceNumber=" + contractReferenceNumber + ", groupCode=" + groupCode + ", contractStartDate=" + contractStartDate + ", contractEndDate=" + contractEndDate + ", contractValue=" + contractValue + ", status=" + status + ", createdBy=" + createdBy + ", operation=" + operation + ", vendorCode=" + vendorCode + ", modifiedBy=" + modifiedBy + ", createdDate=" + createdDate + ", modifiedDate=" + modifiedDate + ", businessUnit=" + businessUnit + ", itemList=" + itemList + "]";
	}

}
