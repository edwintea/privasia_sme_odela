package com.privasia.procurehere.core.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.privasia.procurehere.core.entity.NaicsCodes;
import com.privasia.procurehere.core.entity.SupplierBoardOfDirectors;
import com.privasia.procurehere.core.entity.SupplierProjects;
import com.privasia.procurehere.core.enums.FavouriteSupplierStatus;
import com.privasia.procurehere.core.enums.HrmsStatus;
import com.privasia.procurehere.core.enums.Status;
import com.privasia.procurehere.core.enums.SupplierStatus;
import com.privasia.procurehere.core.utils.CustomDateTimeSerializer;

/**
 * @author yogesh created for supplier profile pdf
 */

public class SupplierPojo implements Serializable {

	private static final long serialVersionUID = -4081304002746630127L;

	private String id;

	private String companyName;

	private String mobileNumber;

	private String companyContactNumber;

	private String countryName;

	private String companyRegistrationNumber;

	private String fullName;

	private String fsFullName;

	private String loginEmail;

	private String communicationEmail;

	private String designation;

	private String remarks;

	private String status;

	private String fsStatus;

	private Integer yearOfEstablished;

	private String faxNumber;
	private String companyWebsite;

	private String companystatus;

	private String line1;
	private String line2;

	private String city;
	private String state;

	@JsonSerialize(using = CustomDateTimeSerializer.class)
	private Date registrationCompleteDate;

	@JsonSerialize(using = CustomDateTimeSerializer.class)
	private Date registrationDate;

	private String registrationCompleteDateS;

	private List<NaicsCodes> naicsCodeslist;

	private List<Coverage> coverages;

	private List<SupplierProjects> supplierProjects;

	private boolean registrationComplete;

	private boolean favourite;

	private String taxRegistrationNumber;

	private String currencyCode;

	private String paidUpCapital;

	private List<SupplierFinanicalDocuments> supplierFinancialDocuments;

	private List<SupplierBoardOfDirectors> supplierBoardOfDirectors;

	private Date createdDate;

	private String hrmsStatus;
	
	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	private String companyAddress;

	private Boolean profileComplet = Boolean.FALSE;

	public SupplierPojo() {
	}

	public SupplierPojo(String id, String companyName) {
		this.id = id;
		this.companyName = companyName;
	}

	public SupplierPojo(String id, String fFullName, FavouriteSupplierStatus fStatus) {
		this.id = id;
		this.fsFullName = fFullName;
		this.fsStatus = fStatus.name();
	}

	public SupplierPojo(String id, String companyName, String countryName, Integer yearOfEstablished, Date registrationDate, FavouriteSupplierStatus fStatus, SupplierStatus status, String fFullName, String fullName, boolean registrationComplete) {
		this.id = id;
		this.companyName = companyName;
		this.countryName = countryName;
		this.yearOfEstablished = yearOfEstablished;
		this.registrationCompleteDate = registrationDate;
		this.fullName = fFullName;
		this.status = fStatus.name();
		// this.fullName = fullName;
		// this.status = status.name();
		this.favourite = true;
		this.registrationComplete = registrationComplete;
	}

	public SupplierPojo(String id, String companyName, String countryName, Integer yearOfEstablished, Date registrationDate, FavouriteSupplierStatus fStatus, SupplierStatus status, String fFullName, String fullName, boolean registrationComplete, Date createdDate) {
		this.id = id;
		this.companyName = companyName;
		this.countryName = countryName;
		this.yearOfEstablished = yearOfEstablished;
		this.registrationCompleteDate = registrationDate;
		this.fullName = fFullName;
		this.status = fStatus.name();
		// this.fullName = fullName;
		// this.status = status.name();
		this.favourite = true;
		this.registrationComplete = registrationComplete;
		this.createdDate = createdDate;
	}

	public SupplierPojo(String id, String companyName, String countryName, Integer yearOfEstablished, Date registrationDate, SupplierStatus status, String fullName, String communicationEmail, String designation, String companyContactNumber, String faxNumber, boolean registrationComplete) {
		this.id = id;
		this.companyName = companyName;
		this.countryName = countryName;
		this.yearOfEstablished = yearOfEstablished;
		this.registrationCompleteDate = registrationDate;
		this.fullName = fullName;
		this.status = Status.ACTIVE.name();
		this.communicationEmail = communicationEmail;
		this.designation = designation;
		this.companyContactNumber = companyContactNumber;
		this.faxNumber = faxNumber;
		this.registrationComplete = registrationComplete;

	}

	public SupplierPojo(String companyName, String countryName, Date registrationDate, String companyRegistrationNumber) {
		this.companyName = companyName;
		this.countryName = countryName;
		this.registrationCompleteDate = registrationDate;
		this.companyRegistrationNumber = companyRegistrationNumber;
	}

	public SupplierPojo(String id, String companyName, String countryName, SupplierStatus status, Date registrationDate, String companyRegistrationNumber, String fullName, String mobileNumber, String line1, String line2) {
		this.id = id;
		this.companyName = companyName;
		this.countryName = countryName;
		this.status = status.name();
		this.registrationDate = registrationDate;
		this.companyRegistrationNumber = companyRegistrationNumber;
		this.fullName = fullName;
		this.mobileNumber = mobileNumber;
		this.line1 = line1;
		this.line2 = line2;

	}

	public SupplierPojo(String id, String companyName, String countryName, Integer yearOfEstablished, Date registrationDate, HrmsStatus status, String fFullName, boolean registrationComplete, Date createdDate, Boolean profileComplet) {
		this.id = id;
		this.companyName = companyName;
		this.countryName = countryName;
		this.yearOfEstablished = yearOfEstablished;
		this.registrationCompleteDate = registrationDate;
		this.fullName = fFullName;
		if (Boolean.TRUE == profileComplet) {
			this.hrmsStatus = status != null ? status.getValue() : HrmsStatus.INCOMPLETE.getValue();
		} else {
			this.hrmsStatus = HrmsStatus.INCOMPLETE.getValue();
		}
		this.favourite = true;
		this.registrationComplete = registrationComplete;
		this.createdDate = createdDate;
		this.profileComplet = profileComplet;
	}

	public SupplierPojo(String id, String companyName, String countryName, Integer yearOfEstablished, Date registrationDate, HrmsStatus status, String fFullName, boolean registrationComplete, Date createdDate) {
		this.id = id;
		this.companyName = companyName;
		this.countryName = countryName;
		this.yearOfEstablished = yearOfEstablished;
		this.registrationCompleteDate = registrationDate;
		this.fullName = fFullName;
		this.hrmsStatus = status != null ? status.getValue() : HrmsStatus.INCOMPLETE.getValue();
		this.favourite = true;
		this.registrationComplete = registrationComplete;
		this.createdDate = createdDate;
	}
	
	public SupplierPojo(String communicationEmail,String companyContactNumber, String companyName,String companyRegistrationNumber, String fullName,String mobileNumber,String companyAddress) {
		this.communicationEmail = communicationEmail;
		this.companyContactNumber = companyContactNumber;
		this.companyName = companyName;
		this.companyRegistrationNumber = companyRegistrationNumber;
		this.fullName = fullName;
		this.mobileNumber = mobileNumber;
		this.companyAddress = companyAddress;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getCompanyContactNumber() {
		return companyContactNumber;
	}

	public void setCompanyContactNumber(String companyContactNumber) {
		this.companyContactNumber = companyContactNumber;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCompanyRegistrationNumber() {
		return companyRegistrationNumber;
	}

	public void setCompanyRegistrationNumber(String companyRegistrationNumber) {
		this.companyRegistrationNumber = companyRegistrationNumber;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getLoginEmail() {
		return loginEmail;
	}

	public void setLoginEmail(String loginEmail) {
		this.loginEmail = loginEmail;
	}

	public String getCommunicationEmail() {
		return communicationEmail;
	}

	public void setCommunicationEmail(String communicationEmail) {
		this.communicationEmail = communicationEmail;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getYearOfEstablished() {
		return yearOfEstablished;
	}

	public void setYearOfEstablished(Integer yearOfEstablished) {
		this.yearOfEstablished = yearOfEstablished;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getCompanyWebsite() {
		return companyWebsite;
	}

	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}

	public String getCompanystatus() {
		return companystatus;
	}

	public void setCompanystatus(String companystatus) {
		this.companystatus = companystatus;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getRegistrationCompleteDate() {
		return registrationCompleteDate;
	}

	public void setRegistrationCompleteDate(Date registrationCompleteDate) {
		this.registrationCompleteDate = registrationCompleteDate;
	}

	public List<NaicsCodes> getNaicsCodeslist() {
		return naicsCodeslist;
	}

	public void setNaicsCodeslist(List<NaicsCodes> naicsCodeslist) {
		this.naicsCodeslist = naicsCodeslist;
	}

	public List<Coverage> getCoverages() {
		return coverages;
	}

	public void setCoverages(List<Coverage> coverages) {
		this.coverages = coverages;
	}

	public List<SupplierProjects> getSupplierProjects() {
		return supplierProjects;
	}

	public void setSupplierProjects(List<SupplierProjects> supplierProjects) {
		this.supplierProjects = supplierProjects;
	}

	public String getRegistrationCompleteDateS() {
		return registrationCompleteDateS;
	}

	public void setRegistrationCompleteDateS(String registrationCompleteDateS) {
		this.registrationCompleteDateS = registrationCompleteDateS;
	}

	/**
	 * @return the fsFullName
	 */
	public String getFsFullName() {
		return fsFullName;
	}

	/**
	 * @param fsFullName the fsFullName to set
	 */
	public void setFsFullName(String fsFullName) {
		this.fsFullName = fsFullName;
	}

	/**
	 * @return the fsStatus
	 */
	public String getFsStatus() {
		return fsStatus;
	}

	/**
	 * @param fsStatus the fsStatus to set
	 */
	public void setFsStatus(String fsStatus) {
		this.fsStatus = fsStatus;
	}

	/**
	 * @return the favourite
	 */
	public boolean isFavourite() {
		return favourite;
	}

	/**
	 * @param favourite the favourite to set
	 */
	public void setFavourite(boolean favourite) {
		this.favourite = favourite;
	}

	/**
	 * @return the registrationDate
	 */
	public Date getRegistrationDate() {
		return registrationDate;
	}

	/**
	 * @param registrationDate the registrationDate to set
	 */
	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	/**
	 * @return the registrationComplete
	 */
	public boolean isRegistrationComplete() {
		return registrationComplete;
	}

	/**
	 * @param registrationComplete the registrationComplete to set
	 */
	public void setRegistrationComplete(boolean registrationComplete) {
		this.registrationComplete = registrationComplete;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SupplierPojo other = (SupplierPojo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getTaxRegistrationNumber() {
		return taxRegistrationNumber;
	}

	public void setTaxRegistrationNumber(String taxRegistrationNumber) {
		this.taxRegistrationNumber = taxRegistrationNumber;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getPaidUpCapital() {
		return paidUpCapital;
	}

	public void setPaidUpCapital(String paidUpCapital) {
		this.paidUpCapital = paidUpCapital;
	}

	public List<SupplierFinanicalDocuments> getSupplierFinancialDocuments() {
		return supplierFinancialDocuments;
	}

	public void setSupplierFinancialDocuments(List<SupplierFinanicalDocuments> supplierFinancialDocuments) {
		this.supplierFinancialDocuments = supplierFinancialDocuments;
	}

	public List<SupplierBoardOfDirectors> getSupplierBoardOfDirectors() {
		return supplierBoardOfDirectors;
	}

	public void setSupplierBoardOfDirectors(List<SupplierBoardOfDirectors> supplierBoardOfDirectors) {
		this.supplierBoardOfDirectors = supplierBoardOfDirectors;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the hrmsStatus
	 */
	public String getHrmsStatus() {
		return hrmsStatus;
	}

	/**
	 * @param hrmsStatus the hrmsStatus to set
	 */
	public void setHrmsStatus(String hrmsStatus) {
		this.hrmsStatus = hrmsStatus;
	}

	/**
	 * @return the profileComplet
	 */
	public Boolean getProfileComplet() {
		return profileComplet;
	}

	/**
	 * @param profileComplet the profileComplet to set
	 */
	public void setProfileComplet(Boolean profileComplet) {
		this.profileComplet = profileComplet;
	}

}
