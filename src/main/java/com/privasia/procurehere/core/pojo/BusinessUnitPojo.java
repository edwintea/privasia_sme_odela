package com.privasia.procurehere.core.pojo;

import java.io.Serializable;

/**
 * @author Sana
 */

public class BusinessUnitPojo implements Serializable {

	private static final long serialVersionUID = 676020086530363797L;

	private String id;

	private String displayName;

	public BusinessUnitPojo() {
	}

	public BusinessUnitPojo(String id, String displayName) {
		this.id = id;
		this.displayName = displayName;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

}
