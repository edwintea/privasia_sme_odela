package com.privasia.procurehere.core.pojo;

import com.privasia.procurehere.core.enums.Status;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * @author Yogesh
 */
public class SupplierMandatoryDocumentPojo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9209619501887324007L;

	/**
	 * -- GETTER --
	 *
	 * @return the documentId
	 */
	@Getter
	@ApiModelProperty(notes = "Supplier  Mandatory Document ID", required = true)
	private String documentId;

	@ApiModelProperty(notes = "Supplier  Mandatory Document File Title", required = true)
	private String documentTitle;
	@ApiModelProperty(notes = "Supplier Company Contact fILE", required = true)
	private String documentFile;

	/**
	 * @param documentId the companyName to set
	 */
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	/**
	 * @return documentTitle the documentTitle to set
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * @param documentTitle the companyName to set
	 */
	public void setDocumentTitle(String documentTitle) {
		this.documentTitle = documentTitle;
	}


	/**
	 * @return documentTitle the documentTitle to set
	 */
	public String getDocumentFile() {
		return documentFile;
	}

	/**
	 * @param documentFile the companyName to set
	 */
	public void setDocumentFile(String documentFile) {
		this.documentFile = documentFile;
	}
}

