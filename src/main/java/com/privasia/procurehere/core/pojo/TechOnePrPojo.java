package com.privasia.procurehere.core.pojo;

import java.io.Serializable;

/**
 * @author ravi
 */
public class TechOnePrPojo implements Serializable {

	private static final long serialVersionUID = -1193061922135083438L;

	private String purchasingLocationCode = "SME";
	private String subLocationCode = "PHP";
	private String requisitionNbr;
	private String processingGroupName;
	private String orderPrinted = "N";
	private String orderNbr;
	private String reference;
	private String requisitionDate;
	private String dueDate;
	private String requisitionType = "STD";
	private String requisitionComment;
	private String userFieldh1;
	private String userFieldh2;
	private String userFieldh3;
	private String userFieldh4;
	private String userFieldh5;
	private String userFieldh6;
	private String userFieldh7;
	private String userFieldh8;
	private String supplierLedgerCode = "AP";
	private String supplierAccountNbri;
	private String requisitionStatus = "U";
	private String lineId;
	private String lineType;
	private String units1;
	private String vatrateCodel = "NA";
	private String unitPrice;
	private String unitpriceinc;
	private String amtinc;
	private String description;
	private String lineComment;
	private String textLine1;
	private String textLine2;
	private String dissid = "1";
	private String ledgerCode = "GL";
	private String accountNbri = "01I99999999999999999";

	/**
	 * @return the purchasingLocationCode
	 */
	public String getPurchasingLocationCode() {
		return purchasingLocationCode;
	}

	/**
	 * @param purchasingLocationCode the purchasingLocationCode to set
	 */
	public void setPurchasingLocationCode(String purchasingLocationCode) {
		this.purchasingLocationCode = purchasingLocationCode;
	}

	/**
	 * @return the subLocationCode
	 */
	public String getSubLocationCode() {
		return subLocationCode;
	}

	/**
	 * @param subLocationCode the subLocationCode to set
	 */
	public void setSubLocationCode(String subLocationCode) {
		this.subLocationCode = subLocationCode;
	}

	/**
	 * @return the requisitionNbr
	 */
	public String getRequisitionNbr() {
		return requisitionNbr;
	}

	/**
	 * @param requisitionNbr the requisitionNbr to set
	 */
	public void setRequisitionNbr(String requisitionNbr) {
		this.requisitionNbr = requisitionNbr;
	}

	/**
	 * @return the processingGroupName
	 */
	public String getProcessingGroupName() {
		return processingGroupName;
	}

	/**
	 * @param processingGroupName the processingGroupName to set
	 */
	public void setProcessingGroupName(String processingGroupName) {
		this.processingGroupName = processingGroupName;
	}

	/**
	 * @return the orderPrinted
	 */
	public String getOrderPrinted() {
		return orderPrinted;
	}

	/**
	 * @param orderPrinted the orderPrinted to set
	 */
	public void setOrderPrinted(String orderPrinted) {
		this.orderPrinted = orderPrinted;
	}

	/**
	 * @return the orderNbr
	 */
	public String getOrderNbr() {
		return orderNbr;
	}

	/**
	 * @param orderNbr the orderNbr to set
	 */
	public void setOrderNbr(String orderNbr) {
		this.orderNbr = orderNbr;
	}

	/**
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}

	/**
	 * @param reference the reference to set
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}

	/**
	 * @return the requisitionDate
	 */
	public String getRequisitionDate() {
		return requisitionDate;
	}

	/**
	 * @param requisitionDate the requisitionDate to set
	 */
	public void setRequisitionDate(String requisitionDate) {
		this.requisitionDate = requisitionDate;
	}

	/**
	 * @return the dueDate
	 */
	public String getDueDate() {
		return dueDate;
	}

	/**
	 * @param dueDate the dueDate to set
	 */
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	/**
	 * @return the requisitionType
	 */
	public String getRequisitionType() {
		return requisitionType;
	}

	/**
	 * @param requisitionType the requisitionType to set
	 */
	public void setRequisitionType(String requisitionType) {
		this.requisitionType = requisitionType;
	}

	/**
	 * @return the requisitionComment
	 */
	public String getRequisitionComment() {
		return requisitionComment;
	}

	/**
	 * @param requisitionComment the requisitionComment to set
	 */
	public void setRequisitionComment(String requisitionComment) {
		this.requisitionComment = requisitionComment;
	}

	/**
	 * @return the userFieldh1
	 */
	public String getUserFieldh1() {
		return userFieldh1;
	}

	/**
	 * @param userFieldh1 the userFieldh1 to set
	 */
	public void setUserFieldh1(String userFieldh1) {
		this.userFieldh1 = userFieldh1;
	}

	/**
	 * @return the userFieldh2
	 */
	public String getUserFieldh2() {
		return userFieldh2;
	}

	/**
	 * @param userFieldh2 the userFieldh2 to set
	 */
	public void setUserFieldh2(String userFieldh2) {
		this.userFieldh2 = userFieldh2;
	}

	/**
	 * @return the userFieldh3
	 */
	public String getUserFieldh3() {
		return userFieldh3;
	}

	/**
	 * @param userFieldh3 the userFieldh3 to set
	 */
	public void setUserFieldh3(String userFieldh3) {
		this.userFieldh3 = userFieldh3;
	}

	/**
	 * @return the userFieldh4
	 */
	public String getUserFieldh4() {
		return userFieldh4;
	}

	/**
	 * @param userFieldh4 the userFieldh4 to set
	 */
	public void setUserFieldh4(String userFieldh4) {
		this.userFieldh4 = userFieldh4;
	}

	/**
	 * @return the userFieldh5
	 */
	public String getUserFieldh5() {
		return userFieldh5;
	}

	/**
	 * @param userFieldh5 the userFieldh5 to set
	 */
	public void setUserFieldh5(String userFieldh5) {
		this.userFieldh5 = userFieldh5;
	}

	/**
	 * @return the userFieldh6
	 */
	public String getUserFieldh6() {
		return userFieldh6;
	}

	/**
	 * @param userFieldh6 the userFieldh6 to set
	 */
	public void setUserFieldh6(String userFieldh6) {
		this.userFieldh6 = userFieldh6;
	}

	/**
	 * @return the userFieldh7
	 */
	public String getUserFieldh7() {
		return userFieldh7;
	}

	/**
	 * @param userFieldh7 the userFieldh7 to set
	 */
	public void setUserFieldh7(String userFieldh7) {
		this.userFieldh7 = userFieldh7;
	}

	/**
	 * @return the userFieldh8
	 */
	public String getUserFieldh8() {
		return userFieldh8;
	}

	/**
	 * @param userFieldh8 the userFieldh8 to set
	 */
	public void setUserFieldh8(String userFieldh8) {
		this.userFieldh8 = userFieldh8;
	}

	/**
	 * @return the supplierLedgerCode
	 */
	public String getSupplierLedgerCode() {
		return supplierLedgerCode;
	}

	/**
	 * @param supplierLedgerCode the supplierLedgerCode to set
	 */
	public void setSupplierLedgerCode(String supplierLedgerCode) {
		this.supplierLedgerCode = supplierLedgerCode;
	}

	/**
	 * @return the supplierAccountNbri
	 */
	public String getSupplierAccountNbri() {
		return supplierAccountNbri;
	}

	/**
	 * @param supplierAccountNbri the supplierAccountNbri to set
	 */
	public void setSupplierAccountNbri(String supplierAccountNbri) {
		this.supplierAccountNbri = supplierAccountNbri;
	}

	/**
	 * @return the requisitionStatus
	 */
	public String getRequisitionStatus() {
		return requisitionStatus;
	}

	/**
	 * @param requisitionStatus the requisitionStatus to set
	 */
	public void setRequisitionStatus(String requisitionStatus) {
		this.requisitionStatus = requisitionStatus;
	}

	/**
	 * @return the lineId
	 */
	public String getLineId() {
		return lineId;
	}

	/**
	 * @param lineId the lineId to set
	 */
	public void setLineId(String lineId) {
		this.lineId = lineId;
	}

	/**
	 * @return the lineType
	 */
	public String getLineType() {
		return lineType;
	}

	/**
	 * @param lineType the lineType to set
	 */
	public void setLineType(String lineType) {
		this.lineType = lineType;
	}

	/**
	 * @return the units1
	 */
	public String getUnits1() {
		return units1;
	}

	/**
	 * @param units1 the units1 to set
	 */
	public void setUnits1(String units1) {
		this.units1 = units1;
	}

	/**
	 * @return the vatrateCodel
	 */
	public String getVatrateCodel() {
		return vatrateCodel;
	}

	/**
	 * @param vatrateCodel the vatrateCodel to set
	 */
	public void setVatrateCodel(String vatrateCodel) {
		this.vatrateCodel = vatrateCodel;
	}

	/**
	 * @return the unitpriceinc
	 */
	public String getUnitpriceinc() {
		return unitpriceinc;
	}

	/**
	 * @param unitpriceinc the unitpriceinc to set
	 */
	public void setUnitpriceinc(String unitpriceinc) {
		this.unitpriceinc = unitpriceinc;
	}

	/**
	 * @return the unitPrice
	 */
	public String getUnitPrice() {
		return unitPrice;
	}

	/**
	 * @param unitPrice the unitPrice to set
	 */
	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}

	/**
	 * @return the amtinc
	 */
	public String getAmtinc() {
		return amtinc;
	}

	/**
	 * @param amtinc the amtinc to set
	 */
	public void setAmtinc(String amtinc) {
		this.amtinc = amtinc;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the lineComment
	 */
	public String getLineComment() {
		return lineComment;
	}

	/**
	 * @param lineComment the lineComment to set
	 */
	public void setLineComment(String lineComment) {
		this.lineComment = lineComment;
	}

	/**
	 * @return the textLine1
	 */
	public String getTextLine1() {
		return textLine1;
	}

	/**
	 * @param textLine1 the textLine1 to set
	 */
	public void setTextLine1(String textLine1) {
		this.textLine1 = textLine1;
	}

	/**
	 * @return the textLine2
	 */
	public String getTextLine2() {
		return textLine2;
	}

	/**
	 * @param textLine2 the textLine2 to set
	 */
	public void setTextLine2(String textLine2) {
		this.textLine2 = textLine2;
	}

	/**
	 * @return the dissid
	 */
	public String getDissid() {
		return dissid;
	}

	/**
	 * @param dissid the dissid to set
	 */
	public void setDissid(String dissid) {
		this.dissid = dissid;
	}

	/**
	 * @return the ledgerCode
	 */
	public String getLedgerCode() {
		return ledgerCode;
	}

	/**
	 * @param ledgerCode the ledgerCode to set
	 */
	public void setLedgerCode(String ledgerCode) {
		this.ledgerCode = ledgerCode;
	}

	/**
	 * @return the accountNbri
	 */
	public String getAccountNbri() {
		return accountNbri;
	}

	/**
	 * @param accountNbr the accountNbr to set
	 */
	public void setAccountNbri(String accountNbri) {
		this.accountNbri = accountNbri;
	}

	public String toLogString() {
		return "TechOnePrPojo [purchasingLocationCode=" + purchasingLocationCode + ", subLocationCode=" + subLocationCode + ", requisitionNbr=" + requisitionNbr + ", processingGroupName=" + processingGroupName + ", orderPrinted=" + orderPrinted + ", orderNbr=" + orderNbr + ", reference=" + reference + ", requisitionDate=" + requisitionDate + ", dueDate=" + dueDate + ", requisitionType=" + requisitionType + ", requisitionComment=" + requisitionComment + ", userFieldh1=" + userFieldh1 + ", userFieldh2=" + userFieldh2 + ", userFieldh3=" + userFieldh3 + ", userFieldh4=" + userFieldh4 + ", userFieldh5=" + userFieldh5 + ", userFieldh6=" + userFieldh6 + ", userFieldh7=" + userFieldh7 + ", userFieldh8=" + userFieldh8 + ", supplierLedgerCode=" + supplierLedgerCode + ", supplierAccountNbri=" + supplierAccountNbri + ", requisitionStatus=" + requisitionStatus + ", lineId=" + lineId + ", lineType=" + lineType + ", units1=" + units1 + ", vatrateCodel=" + vatrateCodel + ", unitPrice=" + unitPrice + ", amtinc=" + amtinc + ", description=" + description + ", lineComment=" + lineComment + ", textLine1=" + textLine1 + ", textLine2=" + textLine2 + ", ledgerCode=" + ledgerCode + ", accountNbr=" + accountNbri + "]";
	}

}
