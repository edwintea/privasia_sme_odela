/**
 * 
 */
package com.privasia.procurehere.core.enums;

/**
 * @author Ravi
 */
public enum AuditTypes {
	CREATE, UPDATE, DELETE,REQUEST
}
