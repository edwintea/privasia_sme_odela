/**
 * 
 */
package com.privasia.procurehere.core.enums;

/**
 * @author Nitin Otageri
 *
 */
public enum TransactionStatus {
	IN_PROGRESS, SUCCESS, VOIDED, FAILURE, TIMEOUT, NEEDS_ATTENTION, _UNKNOWN;
}
