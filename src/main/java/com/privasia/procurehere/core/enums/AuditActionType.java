/**
 * 
 */
package com.privasia.procurehere.core.enums;

/**
 * @author Teja
 */
public enum AuditActionType {
	General, Create, Update, Cancel, Suspend, Resume, Approve, Close, Active, Finish, Complete, Submitted, Accepted, Reject, Extension, Transfer, Start,
	Disqualified, Open, View, Award, Conclude, Download, Requalified, Previewed, Remark,Review,Evaluate,Reminder,Paid;
}
