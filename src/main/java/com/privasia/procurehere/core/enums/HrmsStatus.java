/**
 * 
 */
package com.privasia.procurehere.core.enums;

/**
 * @author ravi
 */
public enum HrmsStatus {
	MATCHED("Matched"), NOTMATCHED("Not Matched"), INCOMPLETE("Incomplete");

	private String value;

	/**
	 * @param value as type
	 */
	HrmsStatus(String value) {
		this.value = value;
	}

	/**
	 * @return value as number
	 */
	public String toString() {
		return value;
	}

	public static HrmsStatus convertFromString(String value) {
		for (HrmsStatus status : HrmsStatus.values()) {
			if (value.equals(status.getValue())) {
				return status;
			}
		}
		return null;
	}

	public static String convertToString(HrmsStatus type) {
		if (type == null)
			return null;

		switch (type) {
		case MATCHED:
			return MATCHED.value;
		case NOTMATCHED:
			return NOTMATCHED.value;
		case INCOMPLETE:
			return INCOMPLETE.value;
		default:
			return null;
		}
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
}
