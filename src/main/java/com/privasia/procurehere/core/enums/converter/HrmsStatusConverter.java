package com.privasia.procurehere.core.enums.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.privasia.procurehere.core.enums.HrmsStatus;

/**
 * @author ravi
 */

@Converter
public class HrmsStatusConverter implements AttributeConverter<HrmsStatus, String> {

	@Override
	public String convertToDatabaseColumn(HrmsStatus value) {
		if (value == null)
			return null;
		return HrmsStatus.convertToString(value);
	}

	@Override
	public HrmsStatus convertToEntityAttribute(String dbData) {
		if (dbData != null)
			return HrmsStatus.convertFromString(dbData);
		throw new IllegalArgumentException("Unknown" + dbData);
	}

}
