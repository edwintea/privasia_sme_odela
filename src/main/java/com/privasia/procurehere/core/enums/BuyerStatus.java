/**
 * 
 */
package com.privasia.procurehere.core.enums;

/**
 * @author Arc
 */
public enum BuyerStatus {
	UNPAID, PENDING, ACTIVE, SUSPENDED, ALL,CLOSED;
}
