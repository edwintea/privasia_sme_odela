package com.privasia.procurehere.core.enums;

/**
 * @author sudesha
 */
public enum SourcingTemplateFieldName {
	COST_CENTER, BUSINESS_UNIT, BUDGET_AMOUNT, HISTORIC_AMOUNT, MINIMUM_SUPPLIER_RATING, MAXIMUM_SUPPLIER_RATING, BASE_CURRENCY
}
