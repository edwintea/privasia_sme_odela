package com.privasia.procurehere.core.enums;

import java.util.Arrays;
import java.util.List;

import com.privasia.procurehere.core.utils.StringUtils;

/**
 * @author pooja
 */
public enum ModuleType {
	Supplier("Supplier"), Buyer("Buyer"), Country("Country"), IndustryCategory("Industry Category"), BuyerSettings("Buyer Settings"),
	ProductCategory("Product Category"), ProductItem("Product Item"), PromotionalCode("Promotional Code"), OwnerSettings("Owner Settings"),
	CompanyStatus("Company Type"), BuyerAddress("Buyer Address"), CostCenter("Cost Center"), Currency("Currency"), EmailSettings("Email Settings"),
	NaicsCodes("Naics Codes"), SupplierSettings("Supplier Settings"), TimeZone("TimeZone"), Uom("UOM"), State("State"), FavouriteSupplier("Favourite Supplier"),
	User("User"), UserRole("User Role"), BusinessUnit("Business Unit"), FinanceCompanySettings("Finance Company Settings"), PasswordSetting("Password Setting"),
	ERP("ERP Configration") , PublishedProfile("Published Profile"), BudgetPlanner("Budget Planner"),SupplierMandatoryDocument("Supplier Mandatory Document");

	private String value;

	/**
	 * @param value as Type
	 */
	private ModuleType(String value) {
		this.value = value;
	}

	/**
	 * @return value as number
	 */
	@Override
	public String toString() {
		return value;
	}

	public static List<ModuleType> getModuleTypeForSupplier() {
		return Arrays.asList(ModuleType.SupplierSettings, ModuleType.User, ModuleType.UserRole,ModuleType.SupplierMandatoryDocument);

	}

	public static List<ModuleType> getModuleTypeForBuyer() {
		return Arrays.asList(ModuleType.IndustryCategory, ModuleType.BuyerSettings, ModuleType.ProductCategory, ModuleType.ProductItem, ModuleType.BuyerAddress, ModuleType.CostCenter, ModuleType.FavouriteSupplier, ModuleType.User, ModuleType.UserRole, ModuleType.NaicsCodes, ModuleType.Uom, ModuleType.BusinessUnit , ModuleType.PublishedProfile ,ModuleType.BudgetPlanner, ModuleType.EmailSettings,ModuleType.SupplierMandatoryDocument);
	}

	public static List<ModuleType> getModuleTypeForOwner() {
		return Arrays.asList(ModuleType.UserRole, ModuleType.User, ModuleType.OwnerSettings, ModuleType.Uom, ModuleType.Currency, ModuleType.State, ModuleType.NaicsCodes, ModuleType.TimeZone, ModuleType.Country, ModuleType.CompanyStatus, ModuleType.PromotionalCode,ModuleType.SupplierMandatoryDocument);

	}

	public static ModuleType fromString(String value) {
		try {
			if (StringUtils.checkString(value).equals(Supplier.toString())) {
				return ModuleType.Supplier;
			} else if (StringUtils.checkString(value).equals(Buyer.toString())) {
				return ModuleType.Buyer;
			} else if (StringUtils.checkString(value).equals(Country.toString())) {
				return ModuleType.Country;
			} else if (StringUtils.checkString(value).equals(IndustryCategory.toString())) {
				return ModuleType.IndustryCategory;
			} else if (StringUtils.checkString(value).equals(BuyerSettings.toString())) {
				return ModuleType.BuyerSettings;
			} else if (StringUtils.checkString(value).equals(ProductCategory.toString())) {
				return ModuleType.ProductCategory;
			} else if (StringUtils.checkString(value).equals(ProductItem.toString())) {
				return ModuleType.ProductItem;
			} else if (StringUtils.checkString(value).equals(PromotionalCode.toString())) {
				return ModuleType.PromotionalCode;
			} else if (StringUtils.checkString(value).equals(OwnerSettings.toString())) {
				return ModuleType.OwnerSettings;
			} else if (StringUtils.checkString(value).equals(CompanyStatus.toString())) {
				return ModuleType.CompanyStatus;
			} else if (StringUtils.checkString(value).equals(BuyerAddress.toString())) {
				return ModuleType.BuyerAddress;
			} else if (StringUtils.checkString(value).equals(CostCenter.toString())) {
				return ModuleType.CostCenter;
			} else if (StringUtils.checkString(value).equals(Currency.toString())) {
				return ModuleType.Currency;
			} else if (StringUtils.checkString(value).equals(EmailSettings.toString())) {
				return ModuleType.EmailSettings;
			} else if (StringUtils.checkString(value).equals(NaicsCodes.toString())) {
				return ModuleType.NaicsCodes;
			} else if (StringUtils.checkString(value).equals(SupplierSettings.toString())) {
				return ModuleType.SupplierSettings;
			} else if (StringUtils.checkString(value).equals(TimeZone.toString())) {
				return ModuleType.TimeZone;
			} else if (StringUtils.checkString(value).equals(Uom.toString())) {
				return ModuleType.Uom;
			} else if (StringUtils.checkString(value).equals(State.toString())) {
				return ModuleType.State;
			} else if (StringUtils.checkString(value).equals(FavouriteSupplier.toString())) {
				return ModuleType.FavouriteSupplier;
			} else if (StringUtils.checkString(value).equals(User.toString())) {
				return ModuleType.User;
			} else if (StringUtils.checkString(value).equals(UserRole.toString())) {
				return ModuleType.UserRole;
			} else if (StringUtils.checkString(value).equals(BusinessUnit.toString())) {
				return ModuleType.BusinessUnit;
			}else if (StringUtils.checkString(value).equals(PublishedProfile.toString())) {
				return ModuleType.PublishedProfile;
			} else if (StringUtils.checkString(value).equals(BudgetPlanner.toString())) {
				return ModuleType.BudgetPlanner;
			} else if (StringUtils.checkString(value).equals(SupplierMandatoryDocument.toString())) {
				return ModuleType.SupplierMandatoryDocument;
			}
			return null;
		}

		catch (Exception e) {
			throw new IllegalArgumentException("Invalid value specified for Module Type : " + value);
		}
	}

	public static String getValue(ModuleType type) {
		switch (type) {
		case Supplier:
			return Supplier.value;
		case Buyer:
			return Buyer.value;
		case Country:
			return Country.value;
		case IndustryCategory:
			return IndustryCategory.value;
		case BuyerSettings:
			return BuyerSettings.value;
		case ProductCategory:
			return ProductCategory.value;
		case ProductItem:
			return ProductItem.value;
		case PromotionalCode:
			return PromotionalCode.value;
		case OwnerSettings:
			return OwnerSettings.value;
		case CompanyStatus:
			return CompanyStatus.value;
		case BuyerAddress:
			return BuyerAddress.value;
		case CostCenter:
			return CostCenter.value;
		case Currency:
			return Currency.value;
		case EmailSettings:
			return EmailSettings.value;
		case NaicsCodes:
			return NaicsCodes.value;
		case SupplierSettings:
			return SupplierSettings.value;
		case TimeZone:
			return TimeZone.value;
		case Uom:
			return Uom.value;
		case State:
			return State.value;
		case FavouriteSupplier:
			return FavouriteSupplier.value;
		case User:
			return User.value;
		case UserRole:
			return UserRole.value;
		case BusinessUnit:
			return BusinessUnit.value;
		case PublishedProfile:
			return PublishedProfile.value;
		case BudgetPlanner:
			return BudgetPlanner.value;
			case SupplierMandatoryDocument:
			return SupplierMandatoryDocument.value;
		default:
			return null;
		}
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
}
