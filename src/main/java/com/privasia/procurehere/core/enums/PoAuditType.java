package com.privasia.procurehere.core.enums;

/**
 * @author ravi
 */
public enum PoAuditType {
	CREATE, UPDATE, ACCEPTED, DECLINED, CANCELLED, READY, ORDERED, RECEIVED, DOWNLOADED, SHARED, REQUESTED
}
