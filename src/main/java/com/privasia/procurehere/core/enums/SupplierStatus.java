/**
 * 
 */
package com.privasia.procurehere.core.enums;

/**
 * @author Arc
 */
public enum SupplierStatus {
	PENDING, APPROVED, REJECTED, SUSPPENDED, CLOSED, _UNKNOWN,ALL;
}
