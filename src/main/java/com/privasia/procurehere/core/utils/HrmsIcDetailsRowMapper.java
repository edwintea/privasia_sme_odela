/**
 * 
 */
package com.privasia.procurehere.core.utils;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.privasia.procurehere.core.pojo.HrmsIcDetails;

/**
 * @author ravi
 */
public class HrmsIcDetailsRowMapper implements RowMapper<HrmsIcDetails> {

	@Override
	public HrmsIcDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
		HrmsIcDetails pojo = new HrmsIcDetails();
		pojo.setNewICNO(StringUtils.checkString(rs.getString("NewICNO")));
		pojo.setNextOfKinICNo(StringUtils.checkString(rs.getString("NextOfKinICNo")));
		return pojo;
	}

}
