package com.privasia.procurehere.core.utils;

import java.util.HashMap;
import java.util.Map;
//import java.util.Base64;


import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;



public class OdelaConstant {
	
	public final static String STATUS_CODE = "status_code";
	public final static String STATUS_MESSAGE = "status_msg";
	
	public final static String STATUS_CODE_A1E002 = "A1E002";
	public final static String STATUS_CODE_A1E003 = "A1E003";
	public final static String STATUS_CODE_A1E004 = "A1E004";
	public final static String STATUS_CODE_A1E005 = "A1E005";
	public final static String STATUS_CODE_A1E007 = "A1E007";
	public final static String STATUS_CODE_A1E008 = "A1E008";
	public final static String STATUS_CODE_A1E009 = "A1E009";
	public final static String STATUS_CODE_A1E010 = "A1E010";
	public final static String STATUS_CODE_A2E002 = "A2E002";
	public final static String STATUS_CODE_A3E001 = "A3E001";
	public final static String STATUS_CODE_A3E002 = "A3E002";
	public final static String STATUS_CODE_A3E003 = "A3E003";
	public final static String STATUS_CODE_A3E004 = "A3E004";
	public final static String STATUS_CODE_A3E005 = "A3E005";
	public final static String STATUS_CODE_A1E012 = "A1E012";
	public final static String STATUS_CODE_A3E006 = "A3E006";
	public final static String STATUS_CODE_S001 = "S001";
	public final static String STATUS_CODE_F001 = "F001";
	
	public final static String ODELA_MAIL_ACCEPTED = "Request Accepted";
	

	
public final static Map<String,String> RESPONSE_CODE_MSG= new HashMap<String,String>(){{
		
	put("A1E002","failed register");
	put("A1E003","missing parameter(s)");
	put("A1E004","invalid parameter(s)");
	put("A1E005","already exists");
	put("A1E007","not exists");
	put("A1E008","failed updated");
	put("A1E009","success updated");
	put("A1E010","null parameter(s)");
	put("A2E002","failed get data");
	put("A3E001","invalid");
	put("A3E002","minimum");
	put("A3E003","maximum");
	put("A3E004","min");
	put("A3E005","max");
	put("A1E012","failed insert");
	put("A3E006","wrong");
	put("S001","Successful");
	put("F001","Unsuccessful , refer error code");
		
	}};
		


}
