package com.privasia.procurehere.core.utils;

public class MaskUtils {

	public static String maskName(String preFix, String supplierID, String envoploeId) {
		long suplierMillis = countChar(supplierID);
		long envoploeMillis = countChar(envoploeId);
		return StringUtils.checkString(preFix) + StringUtils.lpad(String.valueOf(((suplierMillis % 100000) + (envoploeMillis % 100000) % 100000)), 6, '0');

	}

	static long countChar(String value) {
		long count = 0;
		if (StringUtils.checkString(value).length() > 0) {
			for (int i = 0; i < value.length(); i++) {
				count = count + (value.charAt(i) * 31 * i);
			}
		}
		return count;
	}

	public static void main(String[] args) {
		
		String sup[] = { "2c9fde456afeffc3016b06a5f89b3f7a","2c9fde456afeffc3016b02e763a87fcf","2c9fde456afeffc3016aff4240832b87" };
		
		for(String supId : sup) {
			System.out.println(maskName("M", supId, "2c9fde456c059b52016c102654f23149"));
		}
	}
}
