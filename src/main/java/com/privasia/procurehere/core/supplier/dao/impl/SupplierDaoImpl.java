/**
 * 
 */
package com.privasia.procurehere.core.supplier.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.privasia.procurehere.core.dao.impl.GenericDaoImpl;
import com.privasia.procurehere.core.entity.Country;
import com.privasia.procurehere.core.entity.FinancePo;
import com.privasia.procurehere.core.entity.Pr;
import com.privasia.procurehere.core.entity.RequestedAssociatedBuyer;
import com.privasia.procurehere.core.entity.Supplier;
import com.privasia.procurehere.core.entity.SupplierBoardOfDirectors;
import com.privasia.procurehere.core.enums.HrmsStatus;
import com.privasia.procurehere.core.enums.PoStatus;
import com.privasia.procurehere.core.enums.PrStatus;
import com.privasia.procurehere.core.enums.SubscriptionStatus;
import com.privasia.procurehere.core.enums.SupplierStatus;
import com.privasia.procurehere.core.pojo.ColumnParameter;
import com.privasia.procurehere.core.pojo.HrmsIcDetails;
import com.privasia.procurehere.core.pojo.OrderParameter;
import com.privasia.procurehere.core.pojo.PendingEventPojo;
import com.privasia.procurehere.core.pojo.PoSupplierPojo;
import com.privasia.procurehere.core.pojo.SearchSortFilterPojo;
import com.privasia.procurehere.core.pojo.SupplierPojo;
import com.privasia.procurehere.core.pojo.TableDataInput;
import com.privasia.procurehere.core.supplier.dao.SupplierDao;
import com.privasia.procurehere.core.utils.CollectionUtil;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.core.utils.HrmsIcDetailsRowMapper;
import com.privasia.procurehere.core.utils.StringUtils;

/**
 * @author Arc
 */
@Repository()
public class SupplierDaoImpl extends GenericDaoImpl<Supplier, String> implements SupplierDao {

	private static final Logger LOG = Logger.getLogger(Global.SUPPLIER_LOG);

	@Autowired
	private Environment environment;

	@Autowired
	@Qualifier("jdbcTemplate")
	private NamedParameterJdbcTemplate jdbcTemplate;

	@SuppressWarnings("unchecked")
	@Override
	public List<Supplier> findAllactiveSuppliers() {
		final Query query = getEntityManager().createQuery("from Supplier a  join fetch  a.registrationOfCountry as rc left outer join fetch a.companyStatus cs left outer join fetch a.state st  order by a.companyName asc");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Supplier> findPendingSuppliers() {
		final Query query = getEntityManager().createQuery("from Supplier a  join fetch  a.registrationOfCountry as rc left outer join fetch a.companyStatus cs left outer join fetch  a.state st  where a.status =:status order by a.registrationDate desc");
		query.setParameter("status", SupplierStatus.PENDING);
		return query.getResultList();
	}

	@Override
	public boolean isExists(Supplier supplier) {
		final Query query = getEntityManager().createQuery("from Supplier a where (a.companyRegistrationNumber = :companyRegistrationNumber and a.registrationOfCountry = :registrationOfCountry) or (upper(a.companyName) = :companyName and a.registrationOfCountry = :registrationOfCountry)");
		query.setParameter("companyRegistrationNumber", StringUtils.checkString(supplier.getCompanyRegistrationNumber()));
		query.setParameter("registrationOfCountry", supplier.getRegistrationOfCountry());
		query.setParameter("companyName", StringUtils.checkString(supplier.getCompanyName()).toUpperCase());
		query.setParameter("registrationOfCountry", supplier.getRegistrationOfCountry());
		return CollectionUtil.isNotEmpty(query.getResultList());
	}

	@Override
	public boolean isExistsLoginEmail(String loginEmail) {
		final Query query = getEntityManager().createQuery("from Supplier a where  upper(a.loginEmail) = :loginEmail and a.status <> :status");
		query.setParameter("loginEmail", StringUtils.checkString(loginEmail).toUpperCase());
		query.setParameter("status", SupplierStatus.REJECTED);
		return CollectionUtil.isNotEmpty(query.getResultList());
	}

	@Override
	public Supplier findById(String id) {
		final Query query = getEntityManager().createQuery("from Supplier a left outer join fetch a.registrationOfCountry as rc left outer join fetch a.companyStatus cs left outer join fetch  a.state st where a.id =:id");
		query.setParameter("id", id);
		return (Supplier) query.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Supplier> searchSupplier(String status, String order, String globalSearch) {
		StringBuilder hql = new StringBuilder(" from Supplier a  join fetch  a.registrationOfCountry rc  where 1 =1 ");
		if (StringUtils.checkString(status).length() > 0 && SupplierStatus.valueOf(StringUtils.checkString(status).toUpperCase()) != SupplierStatus.ALL) {
			hql.append("and a.status =:status ");
		}
		if (StringUtils.checkString(globalSearch).length() > 0) {
			hql.append("and (upper(a.companyName) like :companyName or upper(a.fullName) like :fullName or upper(a.supplierTrackDesc) like :supplierTrackDesc)");
		}

		if (StringUtils.checkString(order).length() > 0 && (StringUtils.checkString(order).equals("Newest"))) {
			hql.append("order by a.registrationDate desc");
		} else {
			hql.append("order by a.registrationDate asc");
		}

		final Query query = getEntityManager().createQuery(hql.toString());
		if (StringUtils.checkString(status).length() > 0 && SupplierStatus.valueOf(StringUtils.checkString(status).toUpperCase()) != SupplierStatus.ALL) {
			query.setParameter("status", SupplierStatus.valueOf(StringUtils.checkString(status).toUpperCase()));
		}
		if (StringUtils.checkString(globalSearch).length() > 0) {
			query.setParameter("companyName", "%" + globalSearch.toUpperCase() + "%");
			query.setParameter("fullName", "%" + globalSearch.toUpperCase() + "%");
			query.setParameter("supplierTrackDesc", "%" + globalSearch.toUpperCase() + "%");

		}
		return query.getResultList();
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public Supplier update(Supplier e) {
		return getEntityManager().merge(e);
	}

	@Override
	public Supplier findSupplierSubscriptionDetailsBySupplierId(String id) {
		final Query query = getEntityManager().createQuery("from Supplier a left outer join fetch a.associatedBuyers as b left outer join fetch a.registrationOfCountry as rc left outer join fetch a.supplierSubscription s left outer join fetch s.supplierPlan p where a.id =:id");
		query.setParameter("id", id);
		return (Supplier) query.getSingleResult();
	}

	@Override
	@Transactional
	public Supplier findSupplierSubscriptionDetailsBySupplierIdExcludedExpiredBuyers(String id) {
		final Query query = getEntityManager().createQuery("from Supplier s left outer join fetch s.associatedBuyers as ab left outer join fetch s.registrationOfCountry as rc left outer join fetch s.supplierSubscription ss left outer join fetch ss.supplierPlan sp where s.id =:id ");
		query.setParameter("id", id);
		try {
			Supplier supplier = (Supplier) query.getSingleResult();
			return supplier;
		} catch (NoResultException nre) {
			return null;
		}
	}

	@Override
	public Supplier findSupplierByIdForAssignedCountries(String id) {
		final Query query = getEntityManager().createQuery("from Supplier a left outer join fetch a.countries as rc  where a.id =:id");
		query.setParameter("id", id);
		return (Supplier) query.getSingleResult();
	}

	@Override
	public Supplier findSupplierByIdForAssignedStates(String id) {
		final Query query = getEntityManager().createQuery("from Supplier a left outer join fetch a.states as rc  where a.id =:id");
		query.setParameter("id", id);
		return (Supplier) query.getSingleResult();
	}

	@Override
	public Supplier findSupplierForProjectTrackById(String id) {
		final Query query = getEntityManager().createQuery("from Supplier a left outer join fetch a.supplierProjects as sp  where a.id =:id");
		query.setParameter("id", id);
		return (Supplier) query.getSingleResult();
	}

	@Override
	public boolean isExistsRegistrationNumber(Supplier supplier) {
		final Query query = getEntityManager().createQuery("from Supplier a where a.companyRegistrationNumber = :companyRegistrationNumber and a.registrationOfCountry = :registrationOfCountry ");
		query.setParameter("companyRegistrationNumber", supplier.getCompanyRegistrationNumber());
		query.setParameter("registrationOfCountry", supplier.getRegistrationOfCountry());
		return CollectionUtil.isNotEmpty(query.getResultList());
	}

	@Override
	public boolean isExistsCompanyName(Supplier supplier) {
		final Query query = getEntityManager().createQuery("from Supplier a where upper(a.companyName) = :companyName and a.registrationOfCountry = :registrationOfCountry ");
		query.setParameter("companyName", supplier.getCompanyName());
		query.setParameter("registrationOfCountry", supplier.getRegistrationOfCountry());
		return CollectionUtil.isNotEmpty(query.getResultList());
	}

	@Override
	public void updateSupplierCommunicationEmail(String supplierId, String oldCommunicationEmail, String newCommunicationEmail) {
		Query query = getEntityManager().createQuery("update Supplier a set a.communicationEmail = :newCommunicationEmail where a.id = :supplierId");
		query.setParameter("newCommunicationEmail", newCommunicationEmail);
		query.setParameter("supplierId", supplierId);
		query.executeUpdate();
		// Update its primary user as well
		query = getEntityManager().createQuery("update User a set a.communicationEmail = :newCommunicationEmail where a.communicationEmail = :oldCommunicationEmail and a.tenantId = :supplierId");
		query.setParameter("newCommunicationEmail", newCommunicationEmail);
		query.setParameter("oldCommunicationEmail", oldCommunicationEmail);
		query.setParameter("supplierId", supplierId);
		query.executeUpdate();
	}

	@Override
	public Supplier findSuppById(String id) {
		final Query query = getEntityManager().createQuery("from Supplier a left outer join fetch a.registrationOfCountry as rc left outer join fetch a.companyStatus cs left outer join fetch  a.state st left outer join fetch a.associatedBuyers as b where a.id =:id");
		query.setParameter("id", id);
		return (Supplier) query.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Supplier> findSuppliersOfNaicsCode(String ncid) {
		final Query query = getEntityManager().createQuery("from Supplier a  join fetch  a.registrationOfCountry as rc left outer join fetch a.companyStatus cs left outer join fetch  a.state st left outer join fetch a.naicsCodes nc where nc.id =:ncid");
		query.setParameter("ncid", ncid);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SupplierPojo> findListOfSupplierForDateRange(Date start, Date end, Country country) {
		StringBuilder hsql = new StringBuilder("select distinct new com.privasia.procurehere.core.pojo.SupplierPojo(s.companyName, rc.countryName, s.registrationCompleteDate,s.companyRegistrationNumber) from Supplier s left outer join s.registrationOfCountry as rc where s.registrationCompleteDate between :start and :end ");
		if (country != null) {
			hsql.append(" and rc = :country");
		}
		hsql.append(" order by s.registrationCompleteDate");
		Query query = getEntityManager().createQuery(hsql.toString());
		query.setParameter("start", start, TemporalType.TIMESTAMP);
		query.setParameter("end", end, TemporalType.TIMESTAMP);
		if (country != null) {
			query.setParameter("country", country);
		}
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Supplier> getAllSupplierFromGlobalSearch(String searchVal) {
		final Query query = getEntityManager().createQuery("select distinct s from Supplier s left outer join fetch s.registrationOfCountry rc where (upper(s.companyName) like :searchVal) or (upper(s.companyRegistrationNumber) like :searchVal)");
		query.setParameter("searchVal", "%" + searchVal.toUpperCase() + "%");
		List<Supplier> list = query.getResultList();
		LOG.info("  Supplier  " + list.size());
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Supplier> getAllSupplierFromIds(List<String> supplierIds) {
		final Query query = getEntityManager().createQuery("select  s from Supplier s  where s.id in (:supplierIds)");
		query.setParameter("supplierIds", supplierIds);
		List<Supplier> list = query.getResultList();
		return list;
	}

	@Override
	public Supplier findSupplierAndAssocitedBuyersById(String suppId) {
		final Query query = getEntityManager().createQuery("select distinct s from Supplier s left outer join fetch s.associatedBuyers ab where s.id = :id");
		query.setParameter("id", suppId);
		return (Supplier) query.getSingleResult();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Supplier> findSuppliersForSubscriptionExpireOrExtend() {
		final Query query = getEntityManager().createQuery("select distinct s from Supplier s left outer join fetch s.supplierSubscription ss where s.status = :status and s.supplierPackage is not null and s.supplierSubscription is not null and ss.endDate is not null and ss.endDate < :now and ss.subscriptionStatus not in (:subscriptionStatus)");
		query.setParameter("status", SupplierStatus.APPROVED);
		query.setParameter("now", new Date());
		List<SubscriptionStatus> list = new ArrayList<SubscriptionStatus>();
		list.add(SubscriptionStatus.EXPIRED);
		list.add(SubscriptionStatus.CANCELLED);
		query.setParameter("subscriptionStatus", list);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Supplier> findSuppliersForExpiryNotificationReminder(Date remindDate) {
		final Query query = getEntityManager().createQuery("select distinct s from Supplier s left outer join fetch s.supplierSubscription ss where s.status = :status and s.supplierPackage is not null and s.supplierSubscription is not null and ss.endDate is not null and ss.endDate <= :remindDate and ss.subscriptionStatus = :subscriptionStatus and (ss.reminderSent is null or ss.reminderSent = :reminderSent)");
		query.setParameter("status", SupplierStatus.APPROVED);
		query.setParameter("remindDate", remindDate);
		query.setParameter("reminderSent", Boolean.FALSE);
		query.setParameter("subscriptionStatus", SubscriptionStatus.ACTIVE);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PoSupplierPojo> findAllSearchFilterPoForSupplier(String tenantId, TableDataInput input, Date startDate, Date endDate) {
		final Query query = constructPrSearchFilterForTenantQueryForSupplier(tenantId, input, false, startDate, endDate);
		query.setFirstResult(input.getStart());
		query.setMaxResults(input.getLength());
		return query.getResultList();
	}

	@Override
	public long findTotalSearchFilterPoForSupplier(String tenantId, TableDataInput input, Date startDate, Date endDate) {
		final Query query = constructPrSearchFilterForTenantQueryForSupplier(tenantId, input, true, startDate, endDate);
		return ((Number) query.getSingleResult()).longValue();
	}

	@Override
	public long findTotalPoForSupplier(String tenantId) {
		StringBuilder hsql = new StringBuilder("select count(distinct p) from Po p left outer join p.supplier as fs where fs.supplier.id = :supplierId and p.status in (:status) and p.orderedDate is not null ");

		Query query = getEntityManager().createQuery(hsql.toString());
		query.setParameter("supplierId", tenantId);

		query.setParameter("status", Arrays.asList(PoStatus.ORDERED, PoStatus.ACCEPTED, PoStatus.DECLINED, PoStatus.CANCELLED));
		return ((Number) query.getSingleResult()).longValue();
	}

	private Query constructPrSearchFilterForTenantQueryForSupplier(String tenantId, TableDataInput tableParams, boolean isCount, Date startDate, Date endDate) {

		String hql = "";

		// If count query is enabled, then add the select count(*) clause
		if (isCount) {
			hql += "select count(distinct p) ";
		} else {
			hql += "select distinct new com.privasia.procurehere.core.pojo.PoSupplierPojo(p.id, p.name, p.modifiedDate, p.grandTotal, p.createdDate,p.decimal, p.referenceNumber ,p.description , p.poNumber, p.status, bu.unitName, c.currencyCode,b.companyName,p.actionDate) ";
		}

		hql += " from Po p ";

		hql += " left outer join p.supplier as fs";

		hql += " left outer join p.businessUnit as bu";

		hql += " left outer join p.currency as c";

		hql += " left outer join p.buyer b";

		hql += " where fs.supplier.id = :tenantId and p.orderedDate is not null ";

		Boolean selectStatusFilter = false;
		// Add on search filter conditions
		for (ColumnParameter cp : tableParams.getColumns()) {
			if (Boolean.TRUE == cp.getSearchable() && cp.getSearch() != null && StringUtils.checkString(cp.getSearch().getValue()).length() > 0) {
				if (cp.getData().equals("status")) {
					selectStatusFilter = true;
					hql += " and p.status in (:status) ";
				} else if (cp.getData().equals("businessUnit")) {
					hql += " and upper(p.businessUnit.unitName) like (:" + cp.getData().replace(".", "") + ")";
				} else if (cp.getData().equals("buyerCompanyName")) {
					hql += " and upper(p.buyer.companyName) like (:" + cp.getData().replace(".", "") + ")";
				} else if (cp.getData().equals("currency")) {
					hql += " and upper(p.currency.currencyCode) like (:" + cp.getData().replace(".", "") + ")";
				} else {
					hql += " and upper(p." + cp.getData() + ") like (:" + cp.getData().replace(".", "") + ") ";

				}
			}
		}

		if (!selectStatusFilter) {
			hql += " and p.status in(:status)";
		}
		// search with Date range
		if (startDate != null && endDate != null) {
			hql += " and  p.createdDate between :startDate and :endDate ";
		}

		// If it is not a count query then add order by clause
		if (!isCount) {
			List<OrderParameter> orderList = tableParams.getOrder();
			if (CollectionUtil.isNotEmpty(orderList)) {
				hql += " order by ";
				for (OrderParameter order : orderList) {
					String orderColumn = tableParams.getColumns().get(order.getColumn()).getData();
					String dir = order.getDir();
					if (orderColumn.equalsIgnoreCase("businessUnit")) {
						hql += " p.businessUnit.unitName " + dir + ",";
					} else if (orderColumn.equalsIgnoreCase("currency")) {
						hql += " p.currency.currencyCode " + dir + ",";
					} else if (orderColumn.equalsIgnoreCase("buyerCompanyName")) {
						hql += " p.buyer.companyName " + dir + ",";
					} else if (orderColumn.equalsIgnoreCase("acceptRejectDate")) {
						hql += " p.actionDate " + dir + ",";
					} else {
						hql += " p." + orderColumn + " " + dir + ",";
					}
				}
				if (hql.lastIndexOf(",") == hql.length() - 1) {
					hql = hql.substring(0, hql.length() - 1);
				}
			} else {
				// by default order by created date

				hql += " order by p.createdDate desc ";
			}
		}

		LOG.info("HQL : " + hql);

		final Query query = getEntityManager().createQuery(hql.toString());

		query.setParameter("tenantId", tenantId);

		// Apply search filter values
		for (ColumnParameter cp : tableParams.getColumns()) {
			if (Boolean.TRUE == cp.getSearchable() && cp.getSearch() != null && StringUtils.checkString(cp.getSearch().getValue()).length() > 0) {
				// LOG.info("INPUT 1 :: " + cp.getData() + "VALUE 1 :: " + cp.getSearch().getValue());
				if (cp.getData().equals("status")) {
					if (cp.getSearch().getValue().equalsIgnoreCase("ALL")) {
						query.setParameter("status", Arrays.asList(PoStatus.ORDERED, PoStatus.ACCEPTED, PoStatus.DECLINED, PoStatus.CANCELLED));
					} else {
						query.setParameter("status", PoStatus.fromString(cp.getSearch().getValue().toUpperCase()));
					}
				} else {
					query.setParameter(cp.getData().replace(".", ""), "%" + cp.getSearch().getValue().toUpperCase() + "%");
				}
			}
		}
		// set parameter Date range
		if (startDate != null && endDate != null) {
			query.setParameter("startDate", startDate);
			query.setParameter("endDate", endDate);
		}

		if (!selectStatusFilter) {
			query.setParameter("status", Arrays.asList(PoStatus.ORDERED, PoStatus.ACCEPTED, PoStatus.DECLINED, PoStatus.CANCELLED));
		}
		return query;
	}

	@Override
	public long findCountOfAllPOForSupplier(String tenantId, String userId) {
		StringBuilder hsql = new StringBuilder("select count(distinct p) from Pr p  left outer join p.supplier as fs where fs.supplier.id = :tenantId and p.status = :status ");
		// if not admin
		// if (userId != null) {
		// hsql.append(" and (tmu.id = :userId or p.createdBy.id = :userId or au.id = :userId)");
		// }
		LOG.info("HQL : " + hsql);

		Query query = getEntityManager().createQuery(hsql.toString());
		query.setParameter("tenantId", tenantId);
		// if (userId != null) {
		// query.setParameter("userId", userId);
		// }
		query.setParameter("status", PrStatus.APPROVED);
		return ((Number) query.getSingleResult()).longValue();
	}

	@Override
	public boolean isExistsRegistrationNumberWithId(Supplier supplier) {
		final Query query = getEntityManager().createQuery("from Supplier a where a.companyRegistrationNumber = :companyRegistrationNumber and a.registrationOfCountry = :registrationOfCountry and a.id <> :id");
		query.setParameter("companyRegistrationNumber", supplier.getCompanyRegistrationNumber());
		query.setParameter("registrationOfCountry", supplier.getRegistrationOfCountry());
		query.setParameter("id", supplier.getId());
		return CollectionUtil.isNotEmpty(query.getResultList());
	}

	@Override
	public boolean isExistsCompanyNameWithId(Supplier supplier) {
		final Query query = getEntityManager().createQuery("from Supplier a where upper(a.companyName) = :companyName and a.registrationOfCountry = :registrationOfCountry and a.id <> :id");
		query.setParameter("companyName", supplier.getCompanyName());
		query.setParameter("registrationOfCountry", supplier.getRegistrationOfCountry());
		query.setParameter("id", supplier.getId());
		return CollectionUtil.isNotEmpty(query.getResultList());
	}

	@Override
	public void updateSupplierCommunicationEmailForSupplierOnly(String supplierId, String oldCommunicationEmail, String newCommunicationEmail) {
		Query query = getEntityManager().createQuery("update Supplier a set a.communicationEmail = :newCommunicationEmail where a.id = :supplierId");
		query.setParameter("newCommunicationEmail", newCommunicationEmail);
		query.setParameter("supplierId", supplierId);
		query.executeUpdate();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FinancePo> findFinanceSuppliers(String tanentid) {

		final Query query = getEntityManager().createQuery("select distinct new com.privasia.procurehere.core.entity.FinancePo(s.id, s.companyName,s.loginEmail,s.communicationEmail,s.companyRegistrationNumber,s.fullName,s.mobileNumber,s.registrationDate) from FinancePo fp left outer join  fp.supplier s where  fp.financeCompany.id = :tanentid");
		LOG.info("----" + tanentid + "----" + query.toString());
		query.setParameter("tanentid", tanentid);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FinancePo> serchFinanceSuppliers(String status, String order, String globalSreach, String tanentid) {
		StringBuilder hql = new StringBuilder("select distinct new com.privasia.procurehere.core.entity.FinancePo(s.id, s.companyName,s.loginEmail,s.communicationEmail,s.companyRegistrationNumber,s.fullName,s.mobileNumber,s.registrationDate) from FinancePo fp left outer join  fp.supplier s where  fp.financeCompany.id = :tanentid");
		/*
		 * if (StringUtils.checkString(status).length() > 0 &&
		 * SupplierStatus.valueOf(StringUtils.checkString(status).toUpperCase()) != SupplierStatus.ALL) {
		 * hql.append("and a.status =:status "); }
		 */
		if (StringUtils.checkString(globalSreach).length() > 0) {
			hql.append(" and (upper(s.companyName) like :companyName or upper(s.fullName) like :fullName or upper(s.supplierTrackDesc) like :supplierTrackDesc)");
		}

		if (StringUtils.checkString(order).length() > 0 && (StringUtils.checkString(order).equals("Descending"))) {
			hql.append(" order by s.companyName desc");
		} else {
			hql.append(" order by s.companyName asc");
		}

		final Query query = getEntityManager().createQuery(hql.toString());
		/*
		 * if (StringUtils.checkString(status).length() > 0 &&
		 * SupplierStatus.valueOf(StringUtils.checkString(status).toUpperCase()) != SupplierStatus.ALL) {
		 * query.setParameter("status", SupplierStatus.valueOf(StringUtils.checkString(status).toUpperCase())); }
		 */
		if (StringUtils.checkString(globalSreach).length() > 0) {
			query.setParameter("companyName", "%" + globalSreach.toUpperCase() + "%");
			query.setParameter("fullName", "%" + globalSreach.toUpperCase() + "%");
			query.setParameter("supplierTrackDesc", "%" + globalSreach.toUpperCase() + "%");

		}
		query.setParameter("tanentid", tanentid);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pr> findAllSearchFilterPoForFinance(String tenantId, TableDataInput input, Date startDate, Date endDate, PrStatus status, String selectedSupplier) {
		final Query query = constructPrSearchFilterForTenantQueryForSupplier(tenantId, input, false, startDate, endDate, status, selectedSupplier);
		query.setFirstResult(input.getStart());
		query.setMaxResults(input.getLength());
		return query.getResultList();
	}

	@Override
	public long findTotalSearchFilterPoForFinance(String tenantId, TableDataInput input, Date startDate, Date endDate, PrStatus status, String selectedSupplier) {
		final Query query = constructPrSearchFilterForTenantQueryForSupplier(tenantId, input, true, startDate, endDate, status, selectedSupplier);
		return ((Number) query.getSingleResult()).longValue();
	}

	@Override
	public long findTotalPoForFinance(String tenantId) {
		StringBuilder hsql = new StringBuilder("select count(distinct p) from Pr p left outer join p.supplier as fs where p.status = :status ");

		Query query = getEntityManager().createQuery(hsql.toString());
		// query.setParameter("supplierId", tenantId);

		query.setParameter("status", PrStatus.APPROVED);
		return ((Number) query.getSingleResult()).longValue();
	}

	private Query constructPrSearchFilterForTenantQueryForSupplier(String tenantId, TableDataInput tableParams, boolean isCount, Date startDate, Date endDate, PrStatus status, String selectedSupplier) {

		String hql = "";
		LOG.info("cp.getData() :" + selectedSupplier);
		// If count query is enabled, then add the select count(*) clause
		if (isCount) {
			hql += "select count(distinct p) ";
		} else {
			hql += "select distinct NEW com.privasia.procurehere.core.entity.Pr(p.id, p.name, p.modifiedDate, p.grandTotal, p.createdBy, p.modifiedBy, p.poCreatedDate, p.createdBy.name, p.modifiedBy.name, p.decimal, p.referenceNumber, p.description, p.poNumber, p.businessUnit.unitName, s.companyName, p.supplierName) ";
		}

		hql += " from Pr p ";
		hql += " left outer join p.supplier as fs ";
		hql += " left outer join fs.supplier s ";

		hql += " where 1=1 ";

		boolean isSelectOn = false;
		// Add on search filter conditions
		for (ColumnParameter cp : tableParams.getColumns()) {
			if (Boolean.TRUE == cp.getSearchable() && cp.getSearch() != null && StringUtils.checkString(cp.getSearch().getValue()).length() > 0) {
				if (cp.getData().equals("status")) {
					hql += " and p.status in (:status) ";
					isSelectOn = true;
				} else if (cp.getData().equalsIgnoreCase("supplier.fullName")) {
					LOG.info("cp.getData() :" + cp.getData());
					hql += " and upper(s.companyName) like (:" + cp.getData().replace(".", "") + ") ";
				}

				else {
					hql += " and upper(p." + cp.getData() + ") like (:" + cp.getData().replace(".", "") + ") ";
				}
			}
		}

		if (StringUtils.checkString(selectedSupplier).length() > 0) {
			hql += " and s.id = '" + selectedSupplier + "' ";
		}

		if (!isSelectOn)

		{
			hql += " and p.status in (:status) ";
		}

		// search with Date range
		if (startDate != null && endDate != null) {
			hql += " and  p.poCreatedDate between :startDate and :endDate ";
		}

		// If it is not a count query then add order by clause
		if (!isCount) {
			List<OrderParameter> orderList = tableParams.getOrder();
			if (CollectionUtil.isNotEmpty(orderList)) {
				hql += " order by ";
				for (OrderParameter order : orderList) {
					String orderColumn = tableParams.getColumns().get(order.getColumn()).getData();
					String dir = order.getDir();
					hql += " p." + orderColumn + " " + dir + ",";
				}
				if (hql.lastIndexOf(",") == hql.length() - 1) {
					hql = hql.substring(0, hql.length() - 1);
				}
			} else {
				// by default order by created date

				hql += " order by p.poCreatedDate desc ";
			}
		}

		LOG.info("HQL : " + hql);

		final Query query = getEntityManager().createQuery(hql.toString());

		// query.setParameter("tenantId", tenantId);

		// Apply search filter values
		for (ColumnParameter cp : tableParams.getColumns()) {
			if (Boolean.TRUE == cp.getSearchable() && cp.getSearch() != null && StringUtils.checkString(cp.getSearch().getValue()).length() > 0) {
				// LOG.info("INPUT 1 :: " + cp.getData() + "VALUE 1 :: " + cp.getSearch().getValue());
				if (cp.getData().equals("status")) {
					if (cp.getSearch().getValue().equalsIgnoreCase("ALL")) {
						query.setParameter("status", PrStatus.APPROVED);
					} else {
						query.setParameter("status", PrStatus.fromString(cp.getSearch().getValue().toUpperCase()));
					}
				} else {
					query.setParameter(cp.getData().replace(".", ""), "%" + cp.getSearch().getValue().toUpperCase() + "%");
				}
			}
		}
		if (!isSelectOn) {
			query.setParameter("status", status);
		}
		// set parameter Date range
		if (startDate != null && endDate != null) {
			query.setParameter("startDate", startDate);
			query.setParameter("endDate", endDate);
		}
		return query;
	}

	@Override
	public long findCountOfAllPOForFinance(String tenantId, String userId) {
		StringBuilder hsql = new StringBuilder("select count(distinct p) from Pr p  left outer join p.supplier as fs where p.status = :status ");
		// if not admin
		// if (userId != null) {
		// hsql.append(" and (tmu.id = :userId or p.createdBy.id = :userId or au.id = :userId)");
		// }
		LOG.info("HQL : " + hsql);

		Query query = getEntityManager().createQuery(hsql.toString());
		// query.setParameter("tenantId", tenantId);
		// if (userId != null) {
		// query.setParameter("userId", userId);
		// }
		query.setParameter("status", PrStatus.APPROVED);
		return ((Number) query.getSingleResult()).longValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pr> findAllSearchFilterPoForOwner(TableDataInput input, Date startDate, Date endDate, PrStatus status, String selectedSupplier) {
		final Query query = constructPrSearchFilterForTenantQueryForOwner(input, false, startDate, endDate, status, selectedSupplier);
		query.setFirstResult(input.getStart());
		query.setMaxResults(input.getLength());
		return query.getResultList();
	}

	private Query constructPrSearchFilterForTenantQueryForOwner(TableDataInput tableParams, boolean isCount, Date startDate, Date endDate, PrStatus status, String selectedSupplier) {

		String hql = "";
		LOG.info("cp.getData() :" + selectedSupplier);
		// If count query is enabled, then add the select count(*) clause
		if (isCount) {
			hql += "select count(distinct p) ";
		} else {
			hql += "select distinct NEW com.privasia.procurehere.core.entity.Pr(p.id, p.name, p.modifiedDate, p.grandTotal, p.createdBy, p.modifiedBy, p.poCreatedDate, p.createdBy.name, p.modifiedBy.name, p.decimal, p.referenceNumber, p.description, p.poNumber, p.businessUnit.unitName, s.companyName, p.supplierName) ";
		}

		hql += " from Pr p ";
		hql += " left outer join p.supplier as fs ";
		hql += " left outer join fs.supplier s ";

		hql += " where 1=1 ";

		boolean isSelectOn = false;
		// Add on search filter conditions
		for (ColumnParameter cp : tableParams.getColumns()) {
			if (Boolean.TRUE == cp.getSearchable() && cp.getSearch() != null && StringUtils.checkString(cp.getSearch().getValue()).length() > 0) {
				if (cp.getData().equals("status")) {
					hql += " and p.status in (:status) ";
					isSelectOn = true;
				} else if (cp.getData().equalsIgnoreCase("supplier.fullName")) {
					LOG.info("cp.getData() :" + cp.getData());
					hql += " and upper(s.companyName) like (:" + cp.getData().replace(".", "") + ") ";
				}

				else {
					hql += " and upper(p." + cp.getData() + ") like (:" + cp.getData().replace(".", "") + ") ";
				}
			}
		}

		if (StringUtils.checkString(selectedSupplier).length() > 0) {
			hql += " and s.id = '" + selectedSupplier + "' ";
		}

		if (!isSelectOn)

		{
			hql += " and p.status in (:status) ";
		}

		// search with Date range
		if (startDate != null && endDate != null) {
			hql += " and  p.poCreatedDate between :startDate and :endDate ";
		}

		// If it is not a count query then add order by clause
		if (!isCount) {
			List<OrderParameter> orderList = tableParams.getOrder();
			if (CollectionUtil.isNotEmpty(orderList)) {
				hql += " order by ";
				for (OrderParameter order : orderList) {
					String orderColumn = tableParams.getColumns().get(order.getColumn()).getData();
					String dir = order.getDir();
					hql += " p." + orderColumn + " " + dir + ",";
				}
				if (hql.lastIndexOf(",") == hql.length() - 1) {
					hql = hql.substring(0, hql.length() - 1);
				}
			} else {
				// by default order by created date

				hql += " order by p.poCreatedDate desc ";
			}
		}

		LOG.info("HQL : " + hql);

		final Query query = getEntityManager().createQuery(hql.toString());

		// query.setParameter("tenantId", tenantId);

		// Apply search filter values
		for (ColumnParameter cp : tableParams.getColumns()) {
			if (Boolean.TRUE == cp.getSearchable() && cp.getSearch() != null && StringUtils.checkString(cp.getSearch().getValue()).length() > 0) {
				// LOG.info("INPUT 1 :: " + cp.getData() + "VALUE 1 :: " + cp.getSearch().getValue());
				if (cp.getData().equals("status")) {
					if (cp.getSearch().getValue().equalsIgnoreCase("ALL")) {
						query.setParameter("status", PrStatus.APPROVED);
					} else {
						query.setParameter("status", PrStatus.fromString(cp.getSearch().getValue().toUpperCase()));
					}
				} else {
					query.setParameter(cp.getData().replace(".", ""), "%" + cp.getSearch().getValue().toUpperCase() + "%");
				}
			}
		}
		if (!isSelectOn) {
			query.setParameter("status", status);
		}
		// set parameter Date range
		if (startDate != null && endDate != null) {
			query.setParameter("startDate", startDate);
			query.setParameter("endDate", endDate);
		}
		return query;
	}

	@Override
	public long findTotalSearchFilterPoForOwner(TableDataInput input, Date startDate, Date endDate, PrStatus status, String selectedSupplier) {
		final Query query = constructPrSearchFilterForTenantQueryForOwner(input, true, startDate, endDate, status, selectedSupplier);
		return ((Number) query.getSingleResult()).longValue();
	}

	@Override
	public long findTotalPoForOwner() {
		StringBuilder hsql = new StringBuilder("select count(distinct p) from Pr p left outer join p.supplier as fs where p.status = :status ");

		Query query = getEntityManager().createQuery(hsql.toString());
		// query.setParameter("supplierId", tenantId);

		query.setParameter("status", PrStatus.APPROVED);
		return ((Number) query.getSingleResult()).longValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PendingEventPojo> findAllPOForSupplierMobile(String tenantId, SearchSortFilterPojo search) {

		StringBuilder hsql = new StringBuilder("select distinct new com.privasia.procurehere.core.pojo.PendingEventPojo( p.id, p.name, p.poCreatedDate, p.referenceNumber, p.businessUnit, p.status) from Pr p left outer join p.supplier as fs inner join p.buyer as buy where fs.supplier.id = :tenantId and p.status = :status ");
		if (StringUtils.checkString(search.getSearchValue()).length() > 0) {
			hsql.append(" and (upper(p.poNumber) like :search or  upper(p.name) like :search )");
		}
		if (StringUtils.checkString(search.getBuyerName()).length() > 0) {
			hsql.append(" and upper(buy.companyName) like :companyName");
		}
		Query query = getEntityManager().createQuery(hsql.toString());
		query.setParameter("tenantId", tenantId);
		query.setParameter("status", PrStatus.APPROVED);
		if (StringUtils.checkString(search.getSearchValue()).length() > 0) {
			query.setParameter("search", "%" + search.getSearchValue().toUpperCase() + "%");
		}
		if (StringUtils.checkString(search.getBuyerName()).length() > 0) {
			query.setParameter("companyName", "%" + search.getBuyerName().toUpperCase() + "%");
		}
		if (search != null && search.getStart() != null && search.getLength() != null) {
			query.setFirstResult(search.getStart());
			query.setMaxResults(search.getLength());
		}
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SupplierPojo> searchSupplierForPagination(String status, String order, String globalSearch, String pageNo) {
		StringBuilder hql = new StringBuilder("select distinct new com.privasia.procurehere.core.pojo.SupplierPojo(a.id, a.companyName, rc.countryName, a.status, a.registrationDate, a.companyRegistrationNumber, a.fullName, a.mobileNumber, a.line1, a.line2) from Supplier a  join a.registrationOfCountry rc  where 1 =1 ");
		if (StringUtils.checkString(status).length() > 0 && SupplierStatus.valueOf(StringUtils.checkString(status).toUpperCase()) != SupplierStatus.ALL) {
			hql.append("and a.status =:status ");
		}
		if (StringUtils.checkString(globalSearch).length() > 0) {
			hql.append("and (upper(a.companyName) like :companyName or upper(a.fullName) like :fullName or upper(a.supplierTrackDesc) like :supplierTrackDesc)");
		}

		if (StringUtils.checkString(order).length() > 0 && (StringUtils.checkString(order).equals("Newest"))) {
			hql.append("order by a.registrationDate desc");
		} else {
			hql.append("order by a.registrationDate asc");
		}

		final Query query = getEntityManager().createQuery(hql.toString());
		if (StringUtils.checkString(status).length() > 0 && SupplierStatus.valueOf(StringUtils.checkString(status).toUpperCase()) != SupplierStatus.ALL) {
			query.setParameter("status", SupplierStatus.valueOf(StringUtils.checkString(status).toUpperCase()));
		}
		if (StringUtils.checkString(globalSearch).length() > 0) {
			query.setParameter("companyName", "%" + globalSearch.toUpperCase() + "%");
			query.setParameter("fullName", "%" + globalSearch.toUpperCase() + "%");
			query.setParameter("supplierTrackDesc", "%" + globalSearch.toUpperCase() + "%");

		}
		query.setFirstResult(Integer.parseInt(pageNo) * 10);
		query.setMaxResults(10);
		return query.getResultList();
	}

	@Override
	public long findTotalAssocitedBuyersById(String tenantId) {
		final Query query = getEntityManager().createQuery("select count(distinct b.id) from Supplier a left outer join a.associatedBuyers as b where a.id =:tenantId");
		query.setParameter("tenantId", tenantId);
		return ((Number) query.getSingleResult()).longValue();
	}

	@Override
	public long getTotalSupplierCount() {
		final Query query = getEntityManager().createQuery("select count(distinct s.id) from Supplier s");
		return ((Number) query.getSingleResult()).longValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public RequestedAssociatedBuyer findSupplierRequestByIds(String id, String buyerId) {
		final Query query = getEntityManager().createQuery("from RequestedAssociatedBuyer a left outer join fetch a.supplier sp left outer join fetch a.industryCategory ic left outer join fetch a.buyer b where sp.id=:id and b.id=:buyerId");
		query.setParameter("id", id);
		query.setParameter("buyerId", buyerId);
		List<RequestedAssociatedBuyer> list = query.getResultList();
		if (CollectionUtil.isNotEmpty(list)) {
			return list.get(0);
		} else {
			return null;
		}
	}

	@Override
	public Supplier findPlainSupplierById(String id) {
		final Query query = getEntityManager().createQuery("from Supplier a where a.id =:id");
		query.setParameter("id", id);
		return (Supplier) query.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SupplierBoardOfDirectors> findPendingICMatchedSuppliers() {
		final Query query = getEntityManager().createQuery("select a from SupplierBoardOfDirectors a join a.supplier s where (s.hrmsStatus =:hrmsstatus or  s.hrmsStatus is null) and s.status =:status order by s.registrationDate desc");
		query.setParameter("hrmsstatus", HrmsStatus.NOTMATCHED);
		query.setParameter("status", SupplierStatus.APPROVED);
		List<SupplierBoardOfDirectors> list = query.getResultList();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> findPendingICMatchedSupplierIds(List<String> ics) {
		StringBuilder hql = new StringBuilder("select a.id from SupplierBoardOfDirectors a join a.supplier s where s.profileComplet =:profileComplet and  (s.hrmsStatus is null or s.hrmsStatus = :hrmsStatus) and s.status =:status ");
		hql.append(" and a.idNumber in (:idNumber) ");
		hql.append(" order by s.registrationDate desc ");
		final Query query = getEntityManager().createQuery(hql.toString());
		query.setParameter("profileComplet", Boolean.TRUE);
		query.setParameter("hrmsStatus", HrmsStatus.NOTMATCHED);
		query.setParameter("status", SupplierStatus.APPROVED);
		query.setParameter("idNumber", ics);
		List<String> list = query.getResultList();
		return list;
	}

	@Override
	@Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
	public int findICDetailsFromHrms(String icNumber) {
		LOG.info("Checking IC Number " + icNumber + " In HRMS");
		String sql = "SELECT count(*) FROM " + environment.getRequiredProperty("hrms.db.link") + " [vFOSNextOfKin] where ( NewICNO like :icNumber or  NextOfKinICNo  like :icNumber) ";
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("icNumber", "%" + StringUtils.checkString(icNumber) + "%");

		int count = jdbcTemplate.queryForObject(sql, paramSource, Integer.class);
		if (count > 0) {
			LOG.info("Record found in HRMS for IcNumber " + icNumber);
		} else {
			LOG.info("Record Not found in HRMS for IcNumber " + icNumber);
		}
		return count;
	}

	@Override
	@Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
	public List<HrmsIcDetails> getHrmsDetails() {
		String sql = "SELECT NewICNO,NextOfKinICNo FROM " + environment.getRequiredProperty("hrms.db.link") + " [vFOSNextOfKin]";
		return jdbcTemplate.query(sql, new HrmsIcDetailsRowMapper());
	}

	@Override
	public void updateHrmsStatusForSupplier(String supplierId, HrmsStatus hrmsStatus) {
		Query query = getEntityManager().createQuery("update Supplier a set a.hrmsStatus = :hrmsStatus where a.id = :supplierId");
		LOG.info("Supplier Id : " + supplierId + " Status " + hrmsStatus);
		query.setParameter("hrmsStatus", hrmsStatus);
		query.setParameter("supplierId", supplierId);
		query.executeUpdate();

	}

	@Override
	public void updateHrmsStatusForDirector(List<String> ids) {
		Query query = getEntityManager().createQuery("update SupplierBoardOfDirectors a set a.hrmsStatus = :hrmsStatus where a.id in ( :ids) ");
		query.setParameter("hrmsStatus", Boolean.TRUE);
		query.setParameter("ids", ids);
		query.executeUpdate();

		query = getEntityManager().createQuery("update Supplier a set a.hrmsStatus = :hrmsStatus1 where a.id in (select s.id from SupplierBoardOfDirectors bd join bd.supplier s where bd.id in ( :directorsIds) ) and profileComplet = :profileComplet");
		query.setParameter("hrmsStatus1", HrmsStatus.MATCHED);
		query.setParameter("directorsIds", ids);
		query.setParameter("profileComplet", Boolean.TRUE);
		query.executeUpdate();

	}

	@Override
	public void updateProfileComplet(String id, Boolean profileComplet) {
		Query query = getEntityManager().createQuery("update Supplier a set a.profileComplet = :profileComplet where a.id =:id");
		query.setParameter("profileComplet", profileComplet);
		query.setParameter("id", id);
		query.executeUpdate();

	}

	@Override
	public boolean isExistsByCompanyNameRegistrationNoLoginEmail(Supplier supplier) {
		final Query query = getEntityManager().createQuery("from Supplier a where (a.companyRegistrationNumber = :companyRegistrationNumber and (upper(a.companyName) = :companyName) or a.loginEmail = :loginEmail) ");
		query.setParameter("companyRegistrationNumber", StringUtils.checkString(supplier.getCompanyRegistrationNumber()));
		query.setParameter("loginEmail", supplier.getLoginEmail());
		query.setParameter("companyName", StringUtils.checkString(supplier.getCompanyName()).toUpperCase());
		return CollectionUtil.isNotEmpty(query.getResultList());
	}

	@Override
	public long findMyPendingRequest(String tenantId) {
		StringBuilder hsql = new StringBuilder("select count(1) from supplier s where s.supplierStatus = :status AND  ");

		Query query = getEntityManager().createQuery(hsql.toString());
		query.setParameter("supplierId", tenantId);
		query.setParameter("status", PrStatus.PENDING);
		return ((Number) query.getSingleResult()).longValue();
	}

	@Override
	public long findMyRejectedRequest(String tenantId) {
		StringBuilder hsql = new StringBuilder("select count(1) from supplier s where s.supplierStatus = :status AND  ");

		Query query = getEntityManager().createQuery(hsql.toString());
		query.setParameter("supplierId", tenantId);
		query.setParameter("status", PrStatus.REJECTED);
		return ((Number) query.getSingleResult()).longValue();
	}
}
