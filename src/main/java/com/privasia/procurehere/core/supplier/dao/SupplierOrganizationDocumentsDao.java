/**
 * 
 */
package com.privasia.procurehere.core.supplier.dao;

import com.privasia.procurehere.core.dao.GenericDao;
import com.privasia.procurehere.core.entity.SupplierOrganizationDocuments;

/**
 * @author nitin
 */
public interface SupplierOrganizationDocumentsDao extends GenericDao<SupplierOrganizationDocuments, String> {

	/**
	 * @return
	 */
	SupplierOrganizationDocuments getSupplierOrganizationDocuments(String supplierId);

}
