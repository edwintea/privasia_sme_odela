package com.privasia.procurehere.core.supplier.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.privasia.procurehere.core.dao.impl.GenericDaoImpl;
import com.privasia.procurehere.core.entity.SupplierBoardOfDirectors;
import com.privasia.procurehere.core.supplier.dao.SupplierBoardOfDirectorsDao;

@Repository
public class SupplierBoardOfDirectorsDaoImpl extends GenericDaoImpl<SupplierBoardOfDirectors, String> implements SupplierBoardOfDirectorsDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<SupplierBoardOfDirectors> findAllBySupplierId(String id) {
		final Query query = getEntityManager().createQuery("select new com.privasia.procurehere.core.entity.SupplierBoardOfDirectors(a.id, a.directorName, a.idType, a.idNumber,a.citizenship, a.dirType, a.dirEmail, a.dirContact, a.identificationCardFileName) from SupplierBoardOfDirectors a inner join a.supplier sp where sp.id =:id");
		query.setParameter("id", id);
		return query.getResultList();
	}

	@Override
	public void deleteById(String id) {
		final Query query = getEntityManager().createQuery("delete from SupplierBoardOfDirectors a where a.id =:id");
		query.setParameter("id", id);
		query.executeUpdate();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SupplierBoardOfDirectors> findIfRecordExistsWithDuplicateIdnumber(String idNumber) {
		final Query query = getEntityManager().createQuery("from SupplierBoardOfDirectors a where a.idNumber =:idNumber");
		query.setParameter("idNumber", idNumber);
		return query.getResultList();
	}

	@Override
	public Long countByMatchedDirectors(String supplierId) {
		final Query query = getEntityManager().createQuery("select count(a.id) from SupplierBoardOfDirectors a join a.supplier s where s.id =:supplierId and a.hrmsStatus = true");
		query.setParameter("supplierId", supplierId);
		return ((Number) query.getSingleResult()).longValue();
	}

	@Override
	public void updateHrmsStatusById(String id, Boolean status) {
		final Query query = getEntityManager().createQuery("update SupplierBoardOfDirectors a set a.hrmsStatus = :hrmsStatus where a.id =:id");
		query.setParameter("id", id);
		query.setParameter("hrmsStatus", status);
		query.executeUpdate();

	}
}
