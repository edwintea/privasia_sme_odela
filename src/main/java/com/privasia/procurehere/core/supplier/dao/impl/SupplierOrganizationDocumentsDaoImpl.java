/**
 * 
 */
package com.privasia.procurehere.core.supplier.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.privasia.procurehere.core.dao.impl.GenericDaoImpl;
import com.privasia.procurehere.core.entity.SupplierOrganizationDocuments;
import com.privasia.procurehere.core.supplier.dao.SupplierOrganizationDocumentsDao;
import com.privasia.procurehere.core.utils.CollectionUtil;
import com.privasia.procurehere.core.utils.Global;

/**
 * @author nitin
 */
@Repository
public class SupplierOrganizationDocumentsDaoImpl extends GenericDaoImpl<SupplierOrganizationDocuments, String> implements SupplierOrganizationDocumentsDao {

	private static final Logger LOG = Logger.getLogger(Global.SUPPLIER_LOG);

	@SuppressWarnings("unchecked")
	@Override
	public SupplierOrganizationDocuments getSupplierOrganizationDocuments(String supplierId) {
		if(LOG.isDebugEnabled()) {
			LOG.debug("Fetching supplier org document for Supplier : " + supplierId);
		}
		final Query query = getEntityManager().createQuery("from SupplierOrganizationDocuments s where s.supplier.id = :supplierId ");
		query.setParameter("supplierId", supplierId);
		List<SupplierOrganizationDocuments> list = query.getResultList();
		if(CollectionUtil.isNotEmpty(list)) {
			return list.get(0);
		}
		return null;
	}
	
}
