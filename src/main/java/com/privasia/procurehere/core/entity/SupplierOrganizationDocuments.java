package com.privasia.procurehere.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.privasia.procurehere.core.utils.CustomDateTimeSerializer;

@Entity
@Table(name = "PROC_SUPPLIER_ORG_DOCUMENTS")
public class SupplierOrganizationDocuments implements Serializable {

	private static final long serialVersionUID = 215461204916040365L;

	@Id
	@GenericGenerator(name = "idGen", strategy = "uuid.hex")
	@GeneratedValue(generator = "idGen")
	@Column(name = "ID", length = 64)
	private String id;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "SUPPLIER_ID", nullable = false, foreignKey = @ForeignKey(name = "FK_SUP_ORG_DOCS"))
	private Supplier supplier;

	@Lob
	@Column(name = "FORM9_FILE_DATA")
	private byte[] form9FileData;

	@Column(name = "FORM9_FILE_NAME", length = 500)
	private String form9FileName;

	@Column(name = "FORM9_CONTENT_TYPE", length = 160)
	private String form9ContentType;

	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FORM9_UPLOAD_DATE")
	private Date form9UploadDate;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FORM9_UPLOAD_BY", nullable = true, foreignKey = @ForeignKey(name = "FK_SUP_FORM9_DOC_UPLD_BY"))
	private User form9UploadBy;

	@Lob
	@Column(name = "FORM24_FILE_DATA")
	private byte[] form24FileData;

	@Column(name = "FORM24_FILE_NAME", length = 500)
	private String form24FileName;

	@Column(name = "FORM24_CONTENT_TYPE", length = 160)
	private String form24ContentType;

	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FORM24_UPLOAD_DATE")
	private Date form24UploadDate;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FORM24_UPLOAD_BY", nullable = true, foreignKey = @ForeignKey(name = "FK_SUP_FORM24_DOC_UPLD_BY"))
	private User form24UploadBy;

	@Lob
	@Column(name = "FORM49_FILE_DATA")
	private byte[] form49FileData;

	@Column(name = "FORM49_FILE_NAME", length = 500)
	private String form49FileName;

	@Column(name = "FORM49_CONTENT_TYPE", length = 160)
	private String form49ContentType;

	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FORM49_UPLOAD_DATE")
	private Date form49UploadDate;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FORM49_UPLOAD_BY", nullable = true, foreignKey = @ForeignKey(name = "FK_SUP_FORM49_DOC_UPLD_BY"))
	private User form49UploadBy;

	@Lob
	@Column(name = "MOF_FILE_DATA")
	private byte[] mofFileData;

	@Column(name = "MOF_FILE_NAME", length = 500)
	private String mofFileName;

	@Column(name = "MOF_CONTENT_TYPE", length = 160)
	private String mofContentType;

	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MOF_UPLOAD_DATE")
	private Date mofUploadDate;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MOF_UPLOAD_BY", nullable = true, foreignKey = @ForeignKey(name = "FK_SUP_MOF_DOC_UPLD_BY"))
	private User mofUploadBy;

	@Lob
	@Column(name = "SSM_FILE_DATA")
	private byte[] ssmFileData;

	@Column(name = "SSM_FILE_NAME", length = 500)
	private String ssmFileName;

	@Column(name = "SSM_CONTENT_TYPE", length = 160)
	private String ssmContentType;

	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "SSM_UPLOAD_DATE")
	private Date ssmUploadDate;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SSM_UPLOAD_BY", nullable = true, foreignKey = @ForeignKey(name = "FK_SUP_SSM_DOC_UPLD_BY"))
	private User ssmUploadBy;

	@Lob
	@Column(name = "VDF_FILE_DATA")
	private byte[] vdfFileData;

	@Column(name = "VDF_FILE_NAME", length = 500)
	private String vdfFileName;

	@Column(name = "VDF_CONTENT_TYPE", length = 160)
	private String vdfContentType;

	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "VDF_UPLOAD_DATE")
	private Date vdfUploadDate;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "VDF_UPLOAD_BY", nullable = true, foreignKey = @ForeignKey(name = "FK_SUP_VDF_DOC_UPLD_BY"))
	private User vdfUploadBy;

	@Lob
	@Column(name = "CCDF_FILE_DATA")
	private byte[] ccdfFileData;

	@Column(name = "CCDF_FILE_NAME", length = 500)
	private String ccdfFileName;

	@Column(name = "CCDF_CONTENT_TYPE", length = 160)
	private String ccdfContentType;

	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CCDF_UPLOAD_DATE")
	private Date ccdfUploadDate;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CCDF_UPLOAD_BY", nullable = true, foreignKey = @ForeignKey(name = "FK_SUP_CCDF_DOC_UPLD_BY"))
	private User ccdfUploadBy;


	@Column(name="ODELA_MERCHANT", nullable = false)
	private Boolean odelaMerchant=Boolean.FALSE;
	@Lob
	@Column(name = "ODELA_FILE_DATA")
	private byte[] odelaFileData;

	@Column(name = "ODELA_FILE_NAME", length = 500)
	private String odelaFileName;

	@Column(name = "ODELA_CONTENT_TYPE", length = 160)
	private String odelaContentType;

	@JsonSerialize(using = CustomDateTimeSerializer.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ODELA_UPLOAD_DATE")
	private Date odelaUploadDate;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ODELA_UPLOAD_BY", nullable = true, foreignKey = @ForeignKey(name = "FK_SUP_ODELA_DOC_UPLD_BY"))
	private User odelaUploadBy;

	public SupplierOrganizationDocuments() {
	}

	public SupplierOrganizationDocuments(String id, String form9FileName, String form9ContentType, String form24FileName, String form24ContentType, String form49FileName, String form49ContentType, String mofFileName, String mofContentType,String ssmFileName,String ssmContentType,String vdfFileName,String vdfContentType,String ccdfFileName,String ccdfContentType,String odelaFileName,String odelaFileType) {
		super();
		this.id = id;
		this.form9FileName = form9FileName;
		this.form9ContentType = form9ContentType;
		this.form24FileName = form24FileName;
		this.form24ContentType = form24ContentType;
		this.form49FileName = form49FileName;
		this.form49ContentType = form49ContentType;
		this.mofFileName = mofFileName;
		this.mofContentType = mofContentType;
		this.ssmFileName=ssmFileName;
		this.ssmContentType=ssmContentType;
		this.vdfFileName=vdfFileName;
		this.vdfContentType=vdfContentType;
		this.ccdfFileName=ccdfFileName;
		this.ccdfContentType=ccdfContentType;
		this.odelaFileName=odelaFileName;
		this.odelaContentType=getOdelaContentType();
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the supplier
	 */
	public Supplier getSupplier() {
		return supplier;
	}

	/**
	 * @param supplier the supplier to set
	 */
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	/**
	 * @return the form9FileData
	 */
	public byte[] getForm9FileData() {
		return form9FileData;
	}

	/**
	 * @param form9FileData the form9FileData to set
	 */
	public void setForm9FileData(byte[] form9FileData) {
		this.form9FileData = form9FileData;
	}

	/**
	 * @return the form9FileName
	 */
	public String getForm9FileName() {
		return form9FileName;
	}

	/**
	 * @param form9FileName the form9FileName to set
	 */
	public void setForm9FileName(String form9FileName) {
		this.form9FileName = form9FileName;
	}

	/**
	 * @return the form9ContentType
	 */
	public String getForm9ContentType() {
		return form9ContentType;
	}

	/**
	 * @param form9ContentType the form9ContentType to set
	 */
	public void setForm9ContentType(String form9ContentType) {
		this.form9ContentType = form9ContentType;
	}

	/**
	 * @return the form9UploadDate
	 */
	public Date getForm9UploadDate() {
		return form9UploadDate;
	}

	/**
	 * @param form9UploadDate the form9UploadDate to set
	 */
	public void setForm9UploadDate(Date form9UploadDate) {
		this.form9UploadDate = form9UploadDate;
	}

	/**
	 * @return the form24FileData
	 */
	public byte[] getForm24FileData() {
		return form24FileData;
	}

	/**
	 * @param form24FileData the form24FileData to set
	 */
	public void setForm24FileData(byte[] form24FileData) {
		this.form24FileData = form24FileData;
	}

	/**
	 * @return the form24FileName
	 */
	public String getForm24FileName() {
		return form24FileName;
	}

	/**
	 * @param form24FileName the form24FileName to set
	 */
	public void setForm24FileName(String form24FileName) {
		this.form24FileName = form24FileName;
	}

	/**
	 * @return the form24ContentType
	 */
	public String getForm24ContentType() {
		return form24ContentType;
	}

	/**
	 * @param form24ContentType the form24ContentType to set
	 */
	public void setForm24ContentType(String form24ContentType) {
		this.form24ContentType = form24ContentType;
	}

	/**
	 * @return the form24UploadDate
	 */
	public Date getForm24UploadDate() {
		return form24UploadDate;
	}

	/**
	 * @param form24UploadDate the form24UploadDate to set
	 */
	public void setForm24UploadDate(Date form24UploadDate) {
		this.form24UploadDate = form24UploadDate;
	}

	/**
	 * @return the form49FileData
	 */
	public byte[] getForm49FileData() {
		return form49FileData;
	}

	/**
	 * @param form49FileData the form49FileData to set
	 */
	public void setForm49FileData(byte[] form49FileData) {
		this.form49FileData = form49FileData;
	}

	/**
	 * @return the form49FileName
	 */
	public String getForm49FileName() {
		return form49FileName;
	}

	/**
	 * @param form49FileName the form49FileName to set
	 */
	public void setForm49FileName(String form49FileName) {
		this.form49FileName = form49FileName;
	}

	/**
	 * @return the form49ContentType
	 */
	public String getForm49ContentType() {
		return form49ContentType;
	}

	/**
	 * @param form49ContentType the form49ContentType to set
	 */
	public void setForm49ContentType(String form49ContentType) {
		this.form49ContentType = form49ContentType;
	}

	/**
	 * @return the form49UploadDate
	 */
	public Date getForm49UploadDate() {
		return form49UploadDate;
	}

	/**
	 * @param form49UploadDate the form49UploadDate to set
	 */
	public void setForm49UploadDate(Date form49UploadDate) {
		this.form49UploadDate = form49UploadDate;
	}

	/**
	 * @return the mofFileData
	 */
	public byte[] getMofFileData() {
		return mofFileData;
	}

	/**
	 * @param mofFileData the mofFileData to set
	 */
	public void setMofFileData(byte[] mofFileData) {
		this.mofFileData = mofFileData;
	}

	/**
	 * @return the mofFileName
	 */
	public String getMofFileName() {
		return mofFileName;
	}

	/**
	 * @param mofFileName the mofFileName to set
	 */
	public void setMofFileName(String mofFileName) {
		this.mofFileName = mofFileName;
	}

	/**
	 * @return the mofContentType
	 */
	public String getMofContentType() {
		return mofContentType;
	}

	/**
	 * @param mofContentType the mofContentType to set
	 */
	public void setMofContentType(String mofContentType) {
		this.mofContentType = mofContentType;
	}

	/**
	 * @return the mofUploadDate
	 */
	public Date getMofUploadDate() {
		return mofUploadDate;
	}

	/**
	 * @param mofUploadDate the mofUploadDate to set
	 */
	public void setMofUploadDate(Date mofUploadDate) {
		this.mofUploadDate = mofUploadDate;
	}

	/**
	 * @return the mofUploadBy
	 */
	public User getMofUploadBy() {
		return mofUploadBy;
	}

	/**
	 * @param mofUploadBy the mofUploadBy to set
	 */
	public void setMofUploadBy(User mofUploadBy) {
		this.mofUploadBy = mofUploadBy;
	}

	/**
	 * @return the form9UploadBy
	 */
	public User getForm9UploadBy() {
		return form9UploadBy;
	}

	/**
	 * @param form9UploadBy the form9UploadBy to set
	 */
	public void setForm9UploadBy(User form9UploadBy) {
		this.form9UploadBy = form9UploadBy;
	}

	/**
	 * @return the form24UploadBy
	 */
	public User getForm24UploadBy() {
		return form24UploadBy;
	}

	/**
	 * @param form24UploadBy the form24UploadBy to set
	 */
	public void setForm24UploadBy(User form24UploadBy) {
		this.form24UploadBy = form24UploadBy;
	}

	/**
	 * @return the form49UploadBy
	 */
	public User getForm49UploadBy() {
		return form49UploadBy;
	}

	/**
	 * @param form49UploadBy the form49UploadBy to set
	 */
	public void setForm49UploadBy(User form49UploadBy) {
		this.form49UploadBy = form49UploadBy;
	}

	

	public byte[] getSsmFileData() {
		return ssmFileData;
	}

	public void setSsmFileData(byte[] ssmFileData) {
		this.ssmFileData = ssmFileData;
	}

	public String getSsmFileName() {
		return ssmFileName;
	}

	public void setSsmFileName(String ssmFileName) {
		this.ssmFileName = ssmFileName;
	}

	public String getSsmContentType() {
		return ssmContentType;
	}

	public void setSsmContentType(String ssmContentType) {
		this.ssmContentType = ssmContentType;
	}

	public Date getSsmUploadDate() {
		return ssmUploadDate;
	}

	public void setSsmUploadDate(Date ssmUploadDate) {
		this.ssmUploadDate = ssmUploadDate;
	}

	public User getSsmUploadBy() {
		return ssmUploadBy;
	}

	public void setSsmUploadBy(User ssmUploadBy) {
		this.ssmUploadBy = ssmUploadBy;
	}

	public byte[] getVdfFileData() {
		return vdfFileData;
	}

	public void setVdfFileData(byte[] vdfFileData) {
		this.vdfFileData = vdfFileData;
	}

	public String getVdfFileName() {
		return vdfFileName;
	}

	public void setVdfFileName(String vdfFileName) {
		this.vdfFileName = vdfFileName;
	}

	public String getVdfContentType() {
		return vdfContentType;
	}

	public void setVdfContentType(String vdfContentType) {
		this.vdfContentType = vdfContentType;
	}

	public Date getVdfUploadDate() {
		return vdfUploadDate;
	}

	public void setVdfUploadDate(Date vdfUploadDate) {
		this.vdfUploadDate = vdfUploadDate;
	}

	public User getVdfUploadBy() {
		return vdfUploadBy;
	}

	public void setVdfUploadBy(User vdfUploadBy) {
		this.vdfUploadBy = vdfUploadBy;
	}

	public byte[] getCcdfFileData() {
		return ccdfFileData;
	}

	public void setCcdfFileData(byte[] ccdfFileData) {
		this.ccdfFileData = ccdfFileData;
	}

	public String getCcdfFileName() {
		return ccdfFileName;
	}

	public void setCcdfFileName(String ccdfFileName) {
		this.ccdfFileName = ccdfFileName;
	}

	public String getCcdfContentType() {
		return ccdfContentType;
	}

	public void setCcdfContentType(String ccdfContentType) {
		this.ccdfContentType = ccdfContentType;
	}

	public Date getCcdfUploadDate() {
		return ccdfUploadDate;
	}

	public void setCcdfUploadDate(Date ccdfUploadDate) {
		this.ccdfUploadDate = ccdfUploadDate;
	}

	public User getCcdfUploadBy() {
		return ccdfUploadBy;
	}

	public void setCcdfUploadBy(User ccdfUploadBy) {
		this.ccdfUploadBy = ccdfUploadBy;
	}

	public Boolean getOdelaMerchant() {
		return odelaMerchant;
	}

	public void setOdelaMerchant(Boolean odelaMerchant) {
		this.odelaMerchant = odelaMerchant;
	}

	public byte[] getOdelaFileData() {
		return odelaFileData;
	}

	public void setOdelaFileData(byte[] odelaFileData) {
		this.odelaFileData = odelaFileData;
	}

	public String getOdelaFileName() {
		return odelaFileName;
	}

	public void setOdelaFileName(String odelaFileName) {
		this.odelaFileName = odelaFileName;
	}

	public String getOdelaContentType() {
		return odelaContentType;
	}

	public void setOdelaContentType(String odelaContentType) {
		this.odelaContentType = odelaContentType;
	}

	public Date getOdelaUploadDate() {
		return odelaUploadDate;
	}

	public void setOdelaUploadDate(Date odelaUploadDate) {
		this.odelaUploadDate = odelaUploadDate;
	}

	public User getOdelaUploadBy() {
		return odelaUploadBy;
	}

	public void setOdelaUploadBy(User odelaUploadBy) {
		this.odelaUploadBy = odelaUploadBy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SupplierOrganizationDocuments other = (SupplierOrganizationDocuments) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
