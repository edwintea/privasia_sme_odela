package com.privasia.procurehere.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Getter
@Entity
@Table(name = "PROC_SUPPLIER_MANDATORY_DOCUMENTS")
public class SupplierMandatoryDocument implements Serializable {

	private static final long serialVersionUID = 8117893729848219882L;

	@Id
	@GenericGenerator(name = "idGen", strategy = "uuid.hex")
	@GeneratedValue(generator = "idGen")
	@Column(name = "ID", length = 64)
	private String id;

	@Column(name = "CONTENT_TYPE", length = 64, nullable = false)
	private String contentType;

	@Column(name = "DESCRIPTION", nullable = false, length = 64)
	private String description;

	@Column(name = "FILE_DATA", length = 64, nullable = false)
	private byte[] fileData;

	@Column(name = "FILE_NAME", length = 64, nullable = false)
	private String fileName;

	@Column(name = "FILE_SIZE", length = 64, nullable = false)
	private int fileSize;

	public void setFileSizeInKb(int fileSizeInKb) {
		this.fileSizeInKb = fileSizeInKb;
	}

	@Column(name = "FILE_SIZE_IN_KB", length = 64, nullable = false)
	private int fileSizeInKb;

	@Column(name = "UPLOAD_DATE", length = 64, nullable = false)
	private Date uploadDate;

	@Column(name = "CREATED_DATE", length = 64, nullable = false)
	private Date createdDate;


	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.REFRESH })
	@JoinColumn(name = "CREATED_BY", nullable = true)
	private User user;

	@Column(name = "UPDATED_DATE", length = 64, nullable = false)
	private Date updatedDate;

	@Column(name = "UPDATED_BY", nullable = true)
	private String updatedBy;

	@Column(name = "STATUS ")
	private Boolean status = Boolean.TRUE;


	public String getId() {
		return id;
	}

	public String getContentType() {
		return contentType;
	}

	public String getDescription() {
		return description;
	}

	public byte[] getFileData() {
		return fileData;
	}

	public String getFileName() {
		return fileName;
	}

	public int getFileSize() {
		return fileSize;
	}

	public int getFileSizeInKb() {
		return fileSizeInKb;
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public User getCreatedBy() {
		return user;
	}

	public void setCreatedBy(User user) {
		this.user = user;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}


	public Boolean getStatus() {
		return status;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setFileData(byte[] fileData) {
		this.fileData = fileData;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public SupplierMandatoryDocument() {super();}

}
