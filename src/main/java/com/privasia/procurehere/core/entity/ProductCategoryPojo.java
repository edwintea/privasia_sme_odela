package com.privasia.procurehere.core.entity;

import java.io.Serializable;

import com.privasia.procurehere.core.enums.OperationType;
import com.privasia.procurehere.core.enums.Status;

import io.swagger.annotations.ApiModelProperty;

public class ProductCategoryPojo implements Serializable {
	/**
	 * @author jayshree
	 */
	private static final long serialVersionUID = 7082227558864073592L;

	@ApiModelProperty(required = false, hidden = true)
	private String id;

	@ApiModelProperty(notes = "Category Code", allowableValues = "range[1, 15]", required = true)
	private String productCode;

	@ApiModelProperty(notes = "Category Name", allowableValues = "range[1, 128]", required = true)
	private String productName;

	@ApiModelProperty(notes = "Status", required = true)
	private Status status;

	@ApiModelProperty(required = false, hidden = true)
	private String createdBy;
	
	@ApiModelProperty(notes = "Operation", required = true)
	private OperationType operation;

	/**
	 * @return the operation
	 */
	public OperationType getOperation() {
		return operation;
	}

	/**
	 * @param operation the operation to set
	 */
	public void setOperation(OperationType operation) {
		this.operation = operation;
	}

	public ProductCategoryPojo() {
	}

	public ProductCategoryPojo(String productCode, String productName, Status status) {
		this.productCode = productCode;
		this.productName = productName;
		this.status = status;
	}
	
	public ProductCategoryPojo(String id, String productName) {
		this.id = id;
		this.productName = productName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
