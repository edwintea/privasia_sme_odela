package com.privasia.procurehere.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "PROC_RFI_EVENT_DOCUMENTS")
public class RfiEventDocument extends EventDocument implements Serializable {

	private static final long serialVersionUID = -8244693372603916612L;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "EVENT_ID", foreignKey = @ForeignKey(name = "FK_RFI_EVENT_DOC"))
	private RfiEvent rfxEvent;

	@Column(name = "IS_INTERNAL ")
	private Boolean internal = Boolean.FALSE;

	public RfiEventDocument() {
	}

	public RfiEventDocument(String id, String fileName, String description, Date uploadDate, String credContentType, RfiEvent rfxEvent, byte[] fileData) {
		this.rfxEvent = rfxEvent;
		if (rfxEvent != null) {
			rfxEvent.getEventName();
		}
		setCredContentType(credContentType);
		setDescription(description);
		setFileName(fileName);
		setFileData(fileData);
		setId(id);
		setUploadDate(uploadDate);
		setFileSize((fileData.length) / 1024);
	}

	public RfiEventDocument(String id, String fileName, String description, Date uploadDate, String credContentType, RfiEvent rfxEvent, Integer fileSizeInKb) {
		this.rfxEvent = rfxEvent;
		if (rfxEvent != null) {
			rfxEvent.getEventName();
		}
		setCredContentType(credContentType);
		setDescription(description);
		setFileName(fileName);
		setId(id);
		setUploadDate(uploadDate);
		setFileSizeInKb(fileSizeInKb);
	}

	public RfiEventDocument copyFrom(RfiEvent oldEvent) {
		RfiEventDocument newDocs = new RfiEventDocument();
		newDocs.setFileData(getFileData());
		newDocs.setFileName(getFileName());
		newDocs.setDescription(getDescription());
		newDocs.setCredContentType(getCredContentType());
		newDocs.setUploadDate(getUploadDate());
		newDocs.setRfxEvent(oldEvent);
		return newDocs;
	}
	
	public RfiEventDocument(String id, String fileName, String description, Date uploadDate, String credContentType, RfiEvent rfxEvent, Integer fileSizeInKb,Boolean internal) {
		this.rfxEvent = rfxEvent;
		if (rfxEvent != null) {
			rfxEvent.getEventName();
		}
		setCredContentType(credContentType);
		setDescription(description);
		setFileName(fileName);
		setId(id);
		setUploadDate(uploadDate);
		setFileSizeInKb(fileSizeInKb);
		setInternal(internal);
	}
	
	/**
	 * @return the rfxEvent
	 */
	public RfiEvent getRfxEvent() {
		return rfxEvent;
	}

	/**
	 * @param rfxEvent the rfxEvent to set
	 */
	public void setRfxEvent(RfiEvent rfxEvent) {
		this.rfxEvent = rfxEvent;
	}

	public Boolean getInternal() {
		return internal;
	}

	public void setInternal(Boolean internal) {
		this.internal = internal;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return super.hashCode();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	public String toLogString() {
		return "RfiEventDocument [ " + toLogString() + "]";
	}

}
