package com.privasia.procurehere.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "PROC_PRODUCT_CONTRACT_ITEM")
public class ProductContractItems implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5660896686718692837L;

	public interface ProductContractItemInt {
	}

	@Id
	@GenericGenerator(name = "idGen", strategy = "uuid.hex")
	@GeneratedValue(generator = "idGen")
	@Column(name = "ID", length = 64)
	private String id;

	@ManyToOne(fetch = FetchType.LAZY, optional = false, cascade = { CascadeType.REFRESH })
	@JoinColumn(name = "CONTRACT_ID", foreignKey = @ForeignKey(name = "FK_CONT_ITM_CONTRACT_ID"), nullable = false)
	private ProductContract productContract;

	@NotEmpty(message = "{product.contract.itemnumber.empty}", groups = ProductContractItemInt.class)
	@Size(min = 1, max = 16, message = "{product.contract.itemnumber.length}", groups = ProductContractItemInt.class)
	@Column(name = "CONTRACT_ITEM_NUMBER", length = 16, nullable = false)
	private String contractItemNumber;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.REFRESH })
	@JoinColumn(name = "PRODUCT_ITEM_ID", nullable = true, foreignKey = @ForeignKey(name = "FK_CON_ITM_CON_ID"))
	private ProductItem productItem;

	@Digits(integer = 16, fraction = 4, message = "{product.quantity.length}", groups = ProductContractItemInt.class)
	@Column(name = "QUANTITY", length = 20, nullable = true)
	private BigDecimal quantity;

	@Digits(integer = 16, fraction = 4, message = "{product.balance.quantity.length}", groups = ProductContractItemInt.class)
	@Column(name = "BALANCE_QUANTITY", length = 20, nullable = true)
	private BigDecimal balanceQuantity;

	/*
	 * @NotNull(message = "{uom.empty}", groups = ProductContractItemInt.class)
	 */
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.REFRESH })
	@JoinColumn(name = "UOM_ID", nullable = true, foreignKey = @ForeignKey(name = "FK_CON_ITM_UOM_ID"))
	private Uom uom;

	@Digits(integer = 16, fraction = 4, message = "{product.unitPrice.length}", groups = ProductContractItemInt.class)
	@Column(name = "UNIT_PRICE", length = 20, nullable = false)
	private BigDecimal unitPrice;

	/*
	 * @NotNull(message = "{productCategory.empty}", groups = ProductItemInt.class)
	 */
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.REFRESH })
	@JoinColumn(name = "PRODUCT_CATEGORY_ID", nullable = true, foreignKey = @ForeignKey(name = "FK_CON_ITM_ITEM_CAT_ID"))
	private ProductCategory productCategory;

	/*
	 * @NotNull(message = "{business.unit.empty}", groups = ProductItemInt.class)
	 */
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.REFRESH })
	@JoinColumn(name = "BUSINESS_UNIT_ID", nullable = true, foreignKey = @ForeignKey(name = "FK_CON_ITM_BUS_UNIT_ID"))
	private BusinessUnit businessUnit;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.REFRESH })
	@JoinColumn(name = "COST_CENTER_ID", nullable = true, foreignKey = @ForeignKey(name = "FK_CON_ITM_COST_CENTR_ID"))
	private CostCenter costCenter;

	@Column(name = "STORAGE_LOCATION", length = 32, nullable = true)
	private String storageLocation;

	@Transient
	private String productName;

	@Transient
	private FavouriteSupplier supplier;

	public ProductContractItems() {
		super();
	}

	public ProductContractItems(String id, String itemId, String productName, BigDecimal unitPrice, Uom uom, BigDecimal balanceQuantity, String storageLocation, FavouriteSupplier favouriteSupplier, ProductCategory productCategory, BusinessUnit businessUnit) {
		super();
		ProductItem user = new ProductItem();
		user.setProductName(productName);
		this.productName = user.getProductName();
		this.id = id;
		this.unitPrice = unitPrice;
		this.uom = uom;
		this.balanceQuantity = balanceQuantity;
		this.supplier = favouriteSupplier;
		this.productCategory = productCategory;
		this.storageLocation = storageLocation;
		this.businessUnit = businessUnit;
		this.productItem = new ProductItem(itemId);
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the productContract
	 */
	public ProductContract getProductContract() {
		return productContract;
	}

	/**
	 * @param productContract the productContract to set
	 */
	public void setProductContract(ProductContract productContract) {
		this.productContract = productContract;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the contractItemNumber
	 */
	public String getContractItemNumber() {
		return contractItemNumber;
	}

	/**
	 * @param contractItemNumber the contractItemNumber to set
	 */
	public void setContractItemNumber(String contractItemNumber) {
		this.contractItemNumber = contractItemNumber;
	}

	/**
	 * @return the uom
	 */
	public Uom getUom() {
		return uom;
	}

	/**
	 * @param uom the uom to set
	 */
	public void setUom(Uom uom) {
		this.uom = uom;
	}

	/**
	 * @return the unitPrice
	 */
	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	/**
	 * @param unitPrice the unitPrice to set
	 */
	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	/**
	 * @return the productCategory
	 */
	public ProductCategory getProductCategory() {
		return productCategory;
	}

	/**
	 * @param productCategory the productCategory to set
	 */
	public void setProductCategory(ProductCategory productCategory) {
		this.productCategory = productCategory;
	}

	/**
	 * @return the businessUnit
	 */
	public BusinessUnit getBusinessUnit() {
		return businessUnit;
	}

	/**
	 * @param businessUnit the businessUnit to set
	 */
	public void setBusinessUnit(BusinessUnit businessUnit) {
		this.businessUnit = businessUnit;
	}

	/**
	 * @return the productItem
	 */
	public ProductItem getProductItem() {
		return productItem;
	}

	/**
	 * @param productItem the productItem to set
	 */
	public void setProductItem(ProductItem productItem) {
		this.productItem = productItem;
	}

	/**
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the balanceQuantity
	 */
	public BigDecimal getBalanceQuantity() {
		return balanceQuantity;
	}

	/**
	 * @param balanceQuantity the balanceQuantity to set
	 */
	public void setBalanceQuantity(BigDecimal balanceQuantity) {
		this.balanceQuantity = balanceQuantity;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public FavouriteSupplier getSupplier() {
		return supplier;
	}

	public void setSupplier(FavouriteSupplier supplier) {
		this.supplier = supplier;
	}

	/**
	 * @return the storageLocation
	 */
	public String getStorageLocation() {
		return storageLocation;
	}

	/**
	 * @param storageLocation the storageLocation to set
	 */
	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}

	/**
	 * @return the costCenter
	 */
	public CostCenter getCostCenter() {
		return costCenter;
	}

	/**
	 * @param costCenter the costCenter to set
	 */
	public void setCostCenter(CostCenter costCenter) {
		this.costCenter = costCenter;
	}

	@Override
	public String toString() {
		return "ProductContractItems [contractItemNumber=" + contractItemNumber + ", quantity=" + quantity + ", balanceQuantity=" + balanceQuantity + ", uom=" + uom + ", unitPrice=" + unitPrice + ", productName=" + productName + "]";
	}

}
