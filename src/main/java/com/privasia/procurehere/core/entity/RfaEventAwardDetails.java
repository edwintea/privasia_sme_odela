package com.privasia.procurehere.core.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "PROC_RFA_EVENT_AWARD_DETAILS")
public class RfaEventAwardDetails extends AwardDetails implements Serializable {

	private static final long serialVersionUID = 2380243016865784373L;

	@ManyToOne(optional = true, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(name = "EVENT_AWARD", nullable = true, foreignKey = @ForeignKey(name = "FK_RFA_EVNT_AWD_DT"))
	private RfaEventAward eventAward;

	@ManyToOne(optional = false, cascade = { CascadeType.REFRESH })
	@JoinColumn(name = "BQ_ITEM_ID", nullable = false, foreignKey = @ForeignKey(name = "FK_RFA_EVNT_AWD_BQ_ITEM"))
	private RfaBqItem bqItem;

	@Transient
	private String selectItem;

	@Transient
	private String productCategory;

	@Transient
	private String productCode;

	/**
	 * @return the eventAward
	 */
	public RfaEventAward getEventAward() {
		return eventAward;
	}

	/**
	 * @param eventAward the eventAward to set
	 */
	public void setEventAward(RfaEventAward eventAward) {
		this.eventAward = eventAward;
	}

	/**
	 * @return the bqItem
	 */
	public RfaBqItem getBqItem() {
		return bqItem;
	}

	/**
	 * @param bqItem the bqItem to set
	 */
	public void setBqItem(RfaBqItem bqItem) {
		this.bqItem = bqItem;
	}

	/**
	 * @return the selectItem
	 */
	public String getSelectItem() {
		return selectItem;
	}

	/**
	 * @param selectItem the selectItem to set
	 */
	public void setSelectItem(String selectItem) {
		this.selectItem = selectItem;
	}

	/**
	 * @return the productCategory
	 */
	public String getProductCategory() {
		return productCategory;
	}

	/**
	 * @param productCategory the productCategory to set
	 */
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/*
	 * @Override public String toString() { return "RfaEventAwardDetails [eventAward=" + eventAward + ", bqItem=" +
	 * bqItem + "]"; }
	 */

}
