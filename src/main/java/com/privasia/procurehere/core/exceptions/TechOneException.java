/**
 * 
 */
package com.privasia.procurehere.core.exceptions;

/**
 * @author Arc
 */
public class TechOneException extends Exception {

	private static final long serialVersionUID = 5435589551025145389L;

	public TechOneException() {
		super();
	}

	public TechOneException(String message) {
		super(message);
	}

	public TechOneException(Throwable cause) {
		super(cause);
	}

	public TechOneException(String message, Throwable cause) {
		super(message, cause);
	}

}
