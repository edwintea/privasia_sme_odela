package com.privasia.procurehere.job;

import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.supercsv.cellprocessor.ParseDate;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

import com.privasia.procurehere.core.dao.BuyerSettingsDao;
import com.privasia.procurehere.core.dao.PrDao;
import com.privasia.procurehere.core.dao.UserDao;
import com.privasia.procurehere.core.entity.BuyerSettings;
import com.privasia.procurehere.core.entity.Pr;
import com.privasia.procurehere.core.entity.PrAudit;
import com.privasia.procurehere.core.enums.PrAuditType;
import com.privasia.procurehere.core.pojo.TechOnePrCancelPojo;
import com.privasia.procurehere.core.utils.CollectionUtil;
import com.privasia.procurehere.core.utils.StringUtils;
import com.privasia.procurehere.service.PrAuditService;

/**
 * @author Ravi
 */
@Component
public class TechOnePrCancelJob implements BaseSchedulerJob {

	private static final Logger LOG = Logger.getLogger(TechOnePrCancelJob.class);

 
	@Autowired
	PrDao prDao;

	@Autowired
	PrAuditService prAuditService;

	@Autowired
	UserDao userDao;

	@Autowired
	BuyerSettingsDao buyerSettingsDao;

	@Override
	@Transactional(readOnly = false)
	public void execute(JobExecutionContext ctx) {
		readCancelFile();
	}

	private void readCancelFile() {
		try {
			List<BuyerSettings> buyerSettings = buyerSettingsDao.findAll(BuyerSettings.class);

			BuyerSettings buyerSetting = null;
			if (buyerSettings != null && buyerSettings.size() > 0)
				buyerSetting = buyerSettings.get(0);

			if (buyerSetting == null || (buyerSetting != null && StringUtils.checkString(buyerSetting.getPrOutBoundPath()).length() == 0)) {
				return;
			}
			File sourceFolder = new File(buyerSetting.getPrOutBoundPath());
			LOG.info("SourceFolder : " + sourceFolder.getAbsolutePath());
			File processedFolder = new File(sourceFolder, ".processed");

			if (!sourceFolder.exists()) {
				// throw error.
				LOG.error("Path Invalid Outbound folder for TechOne : " + buyerSetting.getPrOutBoundPath());
				return;
			}

			Files.createDirectories(processedFolder.toPath());
			LOG.info("ProcessedFolder : " + processedFolder.getAbsolutePath());

			for (File file : sourceFolder.listFiles()) {
				if (file.isFile()) {
					LOG.info("Input File : " + file.getAbsolutePath());
					Files.move(file.toPath(), processedFolder.toPath().resolve(file.getName()));
				}
			}
			List<TechOnePrCancelPojo> prCancelList = new ArrayList<TechOnePrCancelPojo>();
			for (File file : processedFolder.listFiles()) {
				LOG.info("Processed File : " + file.getAbsolutePath());
				try (ICsvBeanReader beanReader = new CsvBeanReader(new FileReader(file), CsvPreference.STANDARD_PREFERENCE)) {
					// the header elements are used to map the values to the bean
					String[] headers = beanReader.getHeader(true);
					headers = new String[] { "pridNumber", "poStatus", "timestamp" };
					CellProcessor[] processors = getProcessors();

					TechOnePrCancelPojo cancelPojo;
					while ((cancelPojo = beanReader.read(TechOnePrCancelPojo.class, headers, processors)) != null) {
						LOG.info("Cancel Pojo " + cancelPojo.toLogString());
						prCancelList.add(cancelPojo);
					}
				}
				file.delete();
			}
			if (CollectionUtil.isNotEmpty(prCancelList)) {
				for (TechOnePrCancelPojo pojo : prCancelList) {
					prDao.techOneCancelPrByPrId(pojo.getPridNumber());
					try {
						Pr pr = prDao.getPrByPrId(pojo.getPridNumber());
						PrAudit audit = new PrAudit();
						audit.setAction(PrAuditType.CANCELLED);
						audit.setActionBy(userDao.getAdminUserForBuyer(pr.getBuyer().getId()));
						audit.setActionDate(new Date());
						audit.setBuyer(pr.getBuyer());
						audit.setDescription("ERP PO " + pojo.getPridNumber() + " Cancelled");
						audit.setPr(pr);
						prAuditService.save(audit);
					} catch (Exception e) {
						LOG.error("Error While saving Audit Trail : " + e.getMessage(), e);
					}
					LOG.info("Pr " + pojo.getPridNumber() + " Recived Cancel request, Updated status to cancel in PH");
				}
			}

		} catch (Exception e) {
			LOG.error("Error : " + e.getMessage(), e);
		}
	}

	private CellProcessor[] getProcessors() {

		CellProcessor[] processors = new CellProcessor[] { new NotNull(), // PRIDNUMBER
				new NotNull(), // POSTATUS
				new NotNull(new ParseDate("dd/MM/yyyy HHmmss", true)) // TIMESTAMP
		};
		return processors;
	}

}
