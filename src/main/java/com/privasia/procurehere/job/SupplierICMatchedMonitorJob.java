/**
 * 
 */
package com.privasia.procurehere.job;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.privasia.procurehere.core.pojo.HrmsIcDetails;
import com.privasia.procurehere.core.utils.CollectionUtil;
import com.privasia.procurehere.core.utils.StringUtils;
import com.privasia.procurehere.service.supplier.SupplierService;

/**
 * @author ravi
 */
@Component
public class SupplierICMatchedMonitorJob implements BaseSchedulerJob {

	private static final Logger LOG = Logger.getLogger(SupplierICMatchedMonitorJob.class);

	@Autowired
	SupplierService supplierService;

	@Autowired
	MessageSource messageSource;

	@Override
	@Transactional(readOnly = false)
	public void execute(JobExecutionContext ctx) {
		LOG.info("IC Match Job");
		Set<String> icList = new HashSet<String>();
		try {
			List<HrmsIcDetails> hrmsList = supplierService.getHrmsDetails();
			if (CollectionUtil.isNotEmpty(hrmsList)) {
				for (HrmsIcDetails hrms : hrmsList) {
					if (StringUtils.checkString(hrms.getNewICNO()).length() > 0) {
						icList.add(StringUtils.checkString(hrms.getNewICNO()));
					}
					if (StringUtils.checkString(hrms.getNextOfKinICNo()).length() > 0) {
						icList.add(StringUtils.checkString(hrms.getNextOfKinICNo()));
					}
				}

				List<String> updateBatch = new ArrayList<String>();
				for (String ic : icList) {
					updateBatch.add(ic);
					if (updateBatch.size() == 999) {
						List<String> directorsIds = supplierService.findPendingICMatchedSupplierIds(updateBatch);
						if (CollectionUtil.isNotEmpty(directorsIds)) {
							supplierService.updateHrmsStatusForDirector(directorsIds);
						}
						updateBatch = new ArrayList<String>();
					}
				}
				if (updateBatch.size() > 0) {
					List<String> directorsIds = supplierService.findPendingICMatchedSupplierIds(updateBatch);
					if (CollectionUtil.isNotEmpty(directorsIds)) {
						supplierService.updateHrmsStatusForDirector(directorsIds);
					}
					updateBatch = new ArrayList<String>();
				}
			}
		} catch (Exception e) {
			LOG.error("Error while HRMS integration...." + e.getMessage(), e);
		}

	}
}
