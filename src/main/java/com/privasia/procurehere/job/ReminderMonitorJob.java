package com.privasia.procurehere.job;

import java.util.List;

import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.privasia.procurehere.core.entity.RfaEventMeetingReminder;
import com.privasia.procurehere.core.entity.RfaReminder;
import com.privasia.procurehere.core.entity.RfiEventMeetingReminder;
import com.privasia.procurehere.core.entity.RfiReminder;
import com.privasia.procurehere.core.entity.RfpEventMeetingReminder;
import com.privasia.procurehere.core.entity.RfpReminder;
import com.privasia.procurehere.core.entity.RfqEventMeetingReminder;
import com.privasia.procurehere.core.entity.RfqReminder;
import com.privasia.procurehere.core.entity.RftEventMeetingReminder;
import com.privasia.procurehere.core.entity.RftReminder;
import com.privasia.procurehere.core.enums.RfxTypes;
import com.privasia.procurehere.service.RfaMeetingService;
import com.privasia.procurehere.service.RfaReminderService;
import com.privasia.procurehere.service.RfiMeetingService;
import com.privasia.procurehere.service.RfiReminderService;
import com.privasia.procurehere.service.RfpMeetingService;
import com.privasia.procurehere.service.RfpReminderService;
import com.privasia.procurehere.service.RfqMeetingService;
import com.privasia.procurehere.service.RfqReminderService;
import com.privasia.procurehere.service.RftMeetingService;
import com.privasia.procurehere.service.RftReminderService;

/**
 * @author Nitin Otageri
 */
@Component
public class ReminderMonitorJob implements BaseSchedulerJob {

	private static final Logger LOG = Logger.getLogger(ReminderMonitorJob.class);

	@Autowired
	RftMeetingService rftMeetingService;

	@Autowired
	RfpMeetingService rfpMeetingService;

	@Autowired
	RfqMeetingService rfqMeetingService;

	@Autowired
	RfiMeetingService rfiMeetingService;

	@Autowired
	RfaMeetingService rfaMeetingService;

	@Autowired
	RftReminderService rftReminderService;

	@Autowired
	RfpReminderService rfpReminderService;

	@Autowired
	RfqReminderService rfqReminderService;

	@Autowired
	RfiReminderService rfiReminderService;

	@Autowired
	RfaReminderService rfaReminderService;

	@Autowired
	JmsTemplate jmsTemplate;

	@Override
	@Transactional(readOnly = false)
	public void execute(JobExecutionContext ctx) {
		// LOG.info("Running ReminderMonitorJob.... :D ");
		try {
			checkMeetingReminders();
			checkEventReminders();
		} catch (Exception e) {
			LOG.error("Error while Executing reminder job :" + e.getMessage(), e);
		}
	}

	private void checkEventReminders() {
		// Sending Mails and Notifications for RFT Events reminders
		// String timeZone = "GMT+8:00";

		List<RftReminder> rftReminders = rftReminderService.getEventRemindersForNotification();
		for (RftReminder reminder : rftReminders) {
			LOG.info("Rft Sending notifocation for Event Reminder Date and time : " + reminder.getReminderDate() + " Event Name : " + reminder.getRftEvent().getEventName());
			rftReminderService.updateImmediately(reminder.getId());

			// Event event = new Event();
			// event.setId(reminder.getRftEvent().getId());
			// event.setEventName(reminder.getRftEvent().getEventName());
			// event.setReferanceNumber(reminder.getRftEvent().getReferanceNumber());
			// event.setEventStart(reminder.getRftEvent().getEventStart());
			// event.setEventEnd(reminder.getRftEvent().getEventEnd());
			// String businessUnit = reminder.getRftEvent().getBusinessUnit() != null ?
			// reminder.getRftEvent().getBusinessUnit().getDisplayName() : "";
			// BusinessUnit bu = new BusinessUnit();
			// bu.setDisplayName(businessUnit);
			// event.setBusinessUnit(bu);
			//
			// User eventOwner = new User();
			// eventOwner.setId(reminder.getRftEvent().getCreatedBy().getId());
			// eventOwner.setName(reminder.getRftEvent().getCreatedBy().getName());
			// eventOwner.setTenantId(reminder.getRftEvent().getCreatedBy().getTenantId());
			// eventOwner.setCommunicationEmail(reminder.getRftEvent().getCreatedBy().getCommunicationEmail());
			// event.setEventOwner(eventOwner);
			//

			try {
				jmsTemplate.setDefaultDestinationName("QUEUE.EVENT.REMINDER");
				jmsTemplate.setDeliveryMode(DeliveryMode.PERSISTENT);
				jmsTemplate.send(new MessageCreator() {
					@Override
					public Message createMessage(Session session) throws JMSException {
						TextMessage objectMessage = session.createTextMessage();
						objectMessage.setText(RfxTypes.RFT.name() + reminder.getId());
						return objectMessage;
					}
				});
			} catch (Exception e) {
				LOG.error("Error sending message to queue : " + e.getMessage(), e);
			}
			// eventNotificationService.sendEventReminder(event, eventOwner, RfxTypes.RFT, businessUnit,
			// reminder.getStartReminder());

		}

		// Sending Mails and Notifications for RFP Events reminders
		List<RfpReminder> rfpReminders = rfpReminderService.getEventRemindersForNotification();
		for (RfpReminder reminder : rfpReminders) {
			LOG.info("Rfp Sending notifocation for Event Reminder Date and time : " + reminder.getReminderDate() + " Event Name : " + reminder.getRfxEvent().getEventName());
			rfpReminderService.updateImmediately(reminder.getId());

			// Event event = new Event();
			// event.setId(reminder.getRfxEvent().getId());
			// event.setEventName(reminder.getRfxEvent().getEventName());
			// event.setReferanceNumber(reminder.getRfxEvent().getReferanceNumber());
			// event.setEventStart(reminder.getRfxEvent().getEventStart());
			// event.setEventEnd(reminder.getRfxEvent().getEventEnd());
			// String businessUnit = reminder.getRfxEvent().getBusinessUnit() != null ?
			// reminder.getRfxEvent().getBusinessUnit().getDisplayName() : "";
			// BusinessUnit bu = new BusinessUnit();
			// bu.setDisplayName(businessUnit);
			// event.setBusinessUnit(bu);
			//
			// User eventOwner = new User();
			// eventOwner.setId(reminder.getRfxEvent().getCreatedBy().getId());
			// eventOwner.setName(reminder.getRfxEvent().getCreatedBy().getName());
			// eventOwner.setTenantId(reminder.getRfxEvent().getCreatedBy().getTenantId());
			// eventOwner.setCommunicationEmail(reminder.getRfxEvent().getCreatedBy().getCommunicationEmail());
			// event.setEventOwner(eventOwner);
			//

			try {
				jmsTemplate.setDefaultDestinationName("QUEUE.EVENT.REMINDER");
				jmsTemplate.setDeliveryMode(DeliveryMode.PERSISTENT);
				jmsTemplate.send(new MessageCreator() {
					@Override
					public Message createMessage(Session session) throws JMSException {
						TextMessage objectMessage = session.createTextMessage();
						objectMessage.setText(RfxTypes.RFP.name() + reminder.getId());
						return objectMessage;
					}
				});
			} catch (Exception e) {
				LOG.error("Error sending message to queue : " + e.getMessage(), e);
			}

			// eventNotificationService.sendEventReminder(event, eventOwner, RfxTypes.RFP, businessUnit,
			// reminder.getStartReminder());

		}

		// Sending Mails and Notifications for RFQ Events reminders
		List<RfqReminder> rfqReminders = rfqReminderService.getEventRemindersForNotification();
		for (RfqReminder reminder : rfqReminders) {
			LOG.info("Rfq Sending notifocation for Event Reminder Date and time : " + reminder.getReminderDate() + " Event Name : " + reminder.getRfxEvent().getEventName());
			rfqReminderService.updateImmediately(reminder.getId());

			// Event event = new Event();
			// event.setId(reminder.getRfxEvent().getId());
			// event.setEventName(reminder.getRfxEvent().getEventName());
			// event.setReferanceNumber(reminder.getRfxEvent().getReferanceNumber());
			// event.setEventStart(reminder.getRfxEvent().getEventStart());
			// event.setEventEnd(reminder.getRfxEvent().getEventEnd());
			// String businessUnit = reminder.getRfxEvent().getBusinessUnit() != null ?
			// reminder.getRfxEvent().getBusinessUnit().getDisplayName() : "";
			// BusinessUnit bu = new BusinessUnit();
			// bu.setDisplayName(businessUnit);
			// event.setBusinessUnit(bu);
			//
			// User eventOwner = new User();
			// eventOwner.setId(reminder.getRfxEvent().getCreatedBy().getId());
			// eventOwner.setName(reminder.getRfxEvent().getCreatedBy().getName());
			// eventOwner.setTenantId(reminder.getRfxEvent().getCreatedBy().getTenantId());
			// eventOwner.setCommunicationEmail(reminder.getRfxEvent().getCreatedBy().getCommunicationEmail());
			// event.setEventOwner(eventOwner);
			//

			try {
				jmsTemplate.setDefaultDestinationName("QUEUE.EVENT.REMINDER");
				jmsTemplate.setDeliveryMode(DeliveryMode.PERSISTENT);
				jmsTemplate.send(new MessageCreator() {
					@Override
					public Message createMessage(Session session) throws JMSException {
						TextMessage objectMessage = session.createTextMessage();
						objectMessage.setText(RfxTypes.RFQ.name() + reminder.getId());
						return objectMessage;
					}
				});
			} catch (Exception e) {
				LOG.error("Error sending message to queue : " + e.getMessage(), e);
			}

			// eventNotificationService.sendEventReminder(event, eventOwner, RfxTypes.RFQ, businessUnit,
			// reminder.getStartReminder());

		}

		// Sending Mails and Notifications for RFI Events reminders
		List<RfiReminder> rfiReminders = rfiReminderService.getEventRemindersForNotification();
		for (RfiReminder reminder : rfiReminders) {
			LOG.info("Rfi Sending notifocation for Event Reminder Date and time : " + reminder.getReminderDate() + " Event Name : " + reminder.getRfiEvent().getEventName());

			rfiReminderService.updateImmediately(reminder.getId());

			// Event event = new Event();
			// event.setId(reminder.getRfiEvent().getId());
			// event.setEventName(reminder.getRfiEvent().getEventName());
			// event.setReferanceNumber(reminder.getRfiEvent().getReferanceNumber());
			// event.setEventStart(reminder.getRfiEvent().getEventStart());
			// event.setEventEnd(reminder.getRfiEvent().getEventEnd());
			// String businessUnit = reminder.getRfiEvent().getBusinessUnit() != null ?
			// reminder.getRfiEvent().getBusinessUnit().getDisplayName() : "";
			// BusinessUnit bu = new BusinessUnit();
			// bu.setDisplayName(businessUnit);
			// event.setBusinessUnit(bu);
			//
			// User eventOwner = new User();
			// eventOwner.setId(reminder.getRfiEvent().getCreatedBy().getId());
			// eventOwner.setName(reminder.getRfiEvent().getCreatedBy().getName());
			// eventOwner.setTenantId(reminder.getRfiEvent().getCreatedBy().getTenantId());
			// eventOwner.setCommunicationEmail(reminder.getRfiEvent().getCreatedBy().getCommunicationEmail());
			// event.setEventOwner(eventOwner);
			// event.setType(RfxTypes.RFI);
			// event.setStartReminder(reminder.getStartReminder());

			try {
				jmsTemplate.setDefaultDestinationName("QUEUE.EVENT.REMINDER");
				jmsTemplate.setDeliveryMode(DeliveryMode.PERSISTENT);
				jmsTemplate.send(new MessageCreator() {
					@Override
					public Message createMessage(Session session) throws JMSException {
						TextMessage objectMessage = session.createTextMessage();
						objectMessage.setText(RfxTypes.RFI.name() + reminder.getId());
						return objectMessage;
					}
				});
			} catch (Exception e) {
				LOG.error("Error sending message to queue : " + e.getMessage(), e);
			}

			// eventNotificationService.sendEventReminder(event, eventOwner, RfxTypes.RFI, businessUnit,
			// reminder.getStartReminder());

		}

		// Sending Mails and Notifications for RFA Events reminders
		List<RfaReminder> rfaReminders = rfaReminderService.getEventRemindersForNotification();
		for (RfaReminder reminder : rfaReminders) {
			LOG.info("Rfa Sending notifocation for Event Reminder Date and time : " + reminder.getReminderDate() + " Event Name : " + reminder.getRfaEvent().getEventName());

			rfaReminderService.updateImmediately(reminder.getId());

			// Event event = new Event();
			// event.setId(reminder.getRfaEvent().getId());
			// event.setEventName(reminder.getRfaEvent().getEventName());
			// event.setReferanceNumber(reminder.getRfaEvent().getReferanceNumber());
			// event.setEventStart(reminder.getRfaEvent().getEventStart());
			// event.setEventEnd(reminder.getRfaEvent().getEventEnd());
			// String businessUnit = reminder.getRfaEvent().getBusinessUnit() != null ?
			// reminder.getRfaEvent().getBusinessUnit().getDisplayName() : "";
			// BusinessUnit bu = new BusinessUnit();
			// bu.setDisplayName(businessUnit);
			// event.setBusinessUnit(bu);
			//
			// User eventOwner = new User();
			// eventOwner.setId(reminder.getRfaEvent().getCreatedBy().getId());
			// eventOwner.setName(reminder.getRfaEvent().getCreatedBy().getName());
			// eventOwner.setTenantId(reminder.getRfaEvent().getCreatedBy().getTenantId());
			// eventOwner.setCommunicationEmail(reminder.getRfaEvent().getCreatedBy().getCommunicationEmail());
			// event.setEventOwner(eventOwner);
			try {
				jmsTemplate.setDefaultDestinationName("QUEUE.EVENT.REMINDER");
				jmsTemplate.setDeliveryMode(DeliveryMode.PERSISTENT);
				jmsTemplate.send(new MessageCreator() {
					@Override
					public Message createMessage(Session session) throws JMSException {
						TextMessage objectMessage = session.createTextMessage();
						objectMessage.setText(RfxTypes.RFA.name() + reminder.getId());
						return objectMessage;
					}
				});
			} catch (Exception e) {
				LOG.error("Error sending message to queue : " + e.getMessage(), e);
			}
			// eventNotificationService.sendEventReminder(event, eventOwner, RfxTypes.RFA, businessUnit,
			// reminder.getStartReminder());

		}
	}

	private void checkMeetingReminders() {

		// Sending Mails and Notifications for RFT meeting reminders
		List<RftEventMeetingReminder> rftReminders = rftMeetingService.getMeetingRemindersForNotification();
		for (RftEventMeetingReminder reminder : rftReminders) {
			LOG.info("Rft Sending notifocation for Reminder Date and time : " + reminder.getReminderDate() + " Meeting : " + reminder.getRfxEventMeeting().getTitle());
			reminder.setReminderSent(Boolean.TRUE);
			rftMeetingService.updateImmediately(reminder.getId());

			try {
				jmsTemplate.setDefaultDestinationName("QUEUE.MEETING.REMINDER");
				jmsTemplate.setDeliveryMode(DeliveryMode.PERSISTENT);
				jmsTemplate.send(new MessageCreator() {
					@Override
					public Message createMessage(Session session) throws JMSException {
						TextMessage objectMessage = session.createTextMessage();
						objectMessage.setText(RfxTypes.RFT.name() + reminder.getId());
						return objectMessage;
					}
				});
			} catch (Exception e) {
				LOG.error("Error sending message to queue : " + e.getMessage(), e);
			}
			//
			// List<EventMeetingContact> contactList = null;
			// if (CollectionUtil.isNotEmpty(reminder.getRfxEventMeeting().getRfxEventMeetingContacts())) {
			// contactList = new ArrayList<EventMeetingContact>();
			// for (RftEventMeetingContact contact : reminder.getRfxEventMeeting().getRfxEventMeetingContacts()) {
			// contactList.add(contact);
			// }
			// }
			//
			// Event event = new Event();
			// event.setId(reminder.getRftEvent().getId());
			// event.setEventName(reminder.getRftEvent().getEventName());
			// event.setEventId(reminder.getRftEvent().getEventId());
			// event.setReferanceNumber(reminder.getRftEvent().getReferanceNumber());
			// String businesUnit = reminder.getRftEvent().getBusinessUnit() != null ?
			// reminder.getRftEvent().getBusinessUnit().getDisplayName() : "";
			//
			// EventMeeting meeting = new EventMeeting();
			// meeting.setId(reminder.getRfxEventMeeting().getId());
			// meeting.setMeetingType(reminder.getRfxEventMeeting().getMeetingType());
			// meeting.setVenue(reminder.getRfxEventMeeting().getVenue());
			// meeting.setAppointmentDateTime(reminder.getRfxEventMeeting().getAppointmentDateTime());
			// meeting.setTitle(reminder.getRfxEventMeeting().getTitle());
			//
			// User eventOwner = new User();
			// eventOwner.setId(reminder.getRftEvent().getCreatedBy().getId());
			// eventOwner.setName(reminder.getRftEvent().getCreatedBy().getName());
			// eventOwner.setTenantId(reminder.getRftEvent().getCreatedBy().getTenantId());
			// eventOwner.setCommunicationEmail(reminder.getRftEvent().getCreatedBy().getCommunicationEmail());
			//
			// eventNotificationService.sendMeetingReminder(event, meeting, contactList, eventOwner, RfxTypes.RFT,
			// businesUnit);

		}

		// Sending Mails and Notifications for RFP meeting reminders
		List<RfpEventMeetingReminder> rfpReminders = rfpMeetingService.getMeetingRemindersForNotification();
		for (RfpEventMeetingReminder reminder : rfpReminders) {
			LOG.info("Rfp Sending notifocation for Reminder Date and time : " + reminder.getReminderDate() + " Meeting : " + reminder.getRfxEventMeeting().getTitle());
			rfpMeetingService.updateImmediately(reminder.getId());

			try {
				jmsTemplate.setDefaultDestinationName("QUEUE.MEETING.REMINDER");
				jmsTemplate.setDeliveryMode(DeliveryMode.PERSISTENT);
				jmsTemplate.send(new MessageCreator() {
					@Override
					public Message createMessage(Session session) throws JMSException {
						TextMessage objectMessage = session.createTextMessage();
						objectMessage.setText(RfxTypes.RFP.name() + reminder.getId());
						return objectMessage;
					}
				});
			} catch (Exception e) {
				LOG.error("Error sending message to queue : " + e.getMessage(), e);
			}

			// List<EventMeetingContact> contactList = null;
			// if (CollectionUtil.isNotEmpty(reminder.getRfxEventMeeting().getRfxEventMeetingContacts())) {
			// contactList = new ArrayList<EventMeetingContact>();
			// for (RfpEventMeetingContact contact : reminder.getRfxEventMeeting().getRfxEventMeetingContacts()) {
			// contactList.add(contact);
			// }
			// }
			//
			// Event event = new Event();
			// event.setId(reminder.getRfxEvent().getId());
			// event.setEventName(reminder.getRfxEvent().getEventName());
			// event.setEventId(reminder.getRfxEvent().getEventId());
			// event.setReferanceNumber(reminder.getRfxEvent().getReferanceNumber());
			// String businesUnit = reminder.getRfxEvent().getBusinessUnit() != null ?
			// reminder.getRfxEvent().getBusinessUnit().getDisplayName() : "";
			//
			// EventMeeting meeting = new EventMeeting();
			// meeting.setId(reminder.getRfxEventMeeting().getId());
			// meeting.setMeetingType(reminder.getRfxEventMeeting().getMeetingType());
			// meeting.setVenue(reminder.getRfxEventMeeting().getVenue());
			// meeting.setAppointmentDateTime(reminder.getRfxEventMeeting().getAppointmentDateTime());
			// meeting.setTitle(reminder.getRfxEventMeeting().getTitle());
			//
			// User eventOwner = new User();
			// eventOwner.setId(reminder.getRfxEvent().getCreatedBy().getId());
			// eventOwner.setName(reminder.getRfxEvent().getCreatedBy().getName());
			// eventOwner.setTenantId(reminder.getRfxEvent().getCreatedBy().getTenantId());
			// eventOwner.setCommunicationEmail(reminder.getRfxEvent().getCreatedBy().getCommunicationEmail());
			//
			// eventNotificationService.sendMeetingReminder(event, meeting, contactList, eventOwner, RfxTypes.RFP,
			// businesUnit);

		}

		// Sending Mails and Notifications for RFQ meeting reminders
		List<RfqEventMeetingReminder> rfqReminders = rfqMeetingService.getMeetingRemindersForNotification();
		for (RfqEventMeetingReminder reminder : rfqReminders) {
			LOG.info("Rfq Sending notifocation for Reminder Date and time : " + reminder.getReminderDate() + " Meeting : " + reminder.getRfxEventMeeting().getTitle());
			// reminder.setReminderSent(Boolean.TRUE);
			rfqMeetingService.updateImmediately(reminder.getId());

			try {
				jmsTemplate.setDefaultDestinationName("QUEUE.MEETING.REMINDER");
				jmsTemplate.setDeliveryMode(DeliveryMode.PERSISTENT);
				jmsTemplate.send(new MessageCreator() {
					@Override
					public Message createMessage(Session session) throws JMSException {
						TextMessage objectMessage = session.createTextMessage();
						objectMessage.setText(RfxTypes.RFQ.name() + reminder.getId());
						return objectMessage;
					}
				});
			} catch (Exception e) {
				LOG.error("Error sending message to queue : " + e.getMessage(), e);
			}
			//
			// List<EventMeetingContact> contactList = null;
			// if (CollectionUtil.isNotEmpty(reminder.getRfxEventMeeting().getRfxEventMeetingContacts())) {
			// contactList = new ArrayList<EventMeetingContact>();
			// for (RfqEventMeetingContact contact : reminder.getRfxEventMeeting().getRfxEventMeetingContacts()) {
			// contactList.add(contact);
			// }
			// }
			//
			// Event event = new Event();
			// event.setId(reminder.getRfxEvent().getId());
			// event.setEventName(reminder.getRfxEvent().getEventName());
			// event.setEventId(reminder.getRfxEvent().getEventId());
			// event.setReferanceNumber(reminder.getRfxEvent().getReferanceNumber());
			// String businesUnit = reminder.getRfxEvent().getBusinessUnit() != null ?
			// reminder.getRfxEvent().getBusinessUnit().getDisplayName() : "";
			//
			// EventMeeting meeting = new EventMeeting();
			// meeting.setId(reminder.getRfxEventMeeting().getId());
			// meeting.setMeetingType(reminder.getRfxEventMeeting().getMeetingType());
			// meeting.setVenue(reminder.getRfxEventMeeting().getVenue());
			// meeting.setAppointmentDateTime(reminder.getRfxEventMeeting().getAppointmentDateTime());
			// meeting.setTitle(reminder.getRfxEventMeeting().getTitle());
			//
			// User eventOwner = new User();
			// eventOwner.setId(reminder.getRfxEvent().getCreatedBy().getId());
			// eventOwner.setName(reminder.getRfxEvent().getCreatedBy().getName());
			// eventOwner.setTenantId(reminder.getRfxEvent().getCreatedBy().getTenantId());
			// eventOwner.setCommunicationEmail(reminder.getRfxEvent().getCreatedBy().getCommunicationEmail());
			//
			// eventNotificationService.sendMeetingReminder(event, meeting, contactList, eventOwner, RfxTypes.RFQ,
			// businesUnit);

		}

		// Sending Mails and Notifications for RFI meeting reminders
		List<RfiEventMeetingReminder> rfiReminders = rfiMeetingService.getMeetingRemindersForNotification();
		for (RfiEventMeetingReminder reminder : rfiReminders) {
			LOG.info("Rfi Sending notifocation for Reminder Date and time : " + reminder.getReminderDate() + " Meeting : " + reminder.getRfxEventMeeting().getTitle());
			rfiMeetingService.updateImmediately(reminder.getId());

			try {
				jmsTemplate.setDefaultDestinationName("QUEUE.MEETING.REMINDER");
				jmsTemplate.setDeliveryMode(DeliveryMode.PERSISTENT);
				jmsTemplate.send(new MessageCreator() {
					@Override
					public Message createMessage(Session session) throws JMSException {
						TextMessage objectMessage = session.createTextMessage();
						objectMessage.setText(RfxTypes.RFI.name() + reminder.getId());
						return objectMessage;
					}
				});
			} catch (Exception e) {
				LOG.error("Error sending message to queue : " + e.getMessage(), e);
			}

			// List<EventMeetingContact> contactList = null;
			// if (CollectionUtil.isNotEmpty(reminder.getRfxEventMeeting().getRfxEventMeetingContacts())) {
			// contactList = new ArrayList<EventMeetingContact>();
			// for (RfiEventMeetingContact contact : reminder.getRfxEventMeeting().getRfxEventMeetingContacts()) {
			// contactList.add(contact);
			// }
			// }
			//
			// Event event = new Event();
			// event.setId(reminder.getRfiEvent().getId());
			// event.setEventName(reminder.getRfiEvent().getEventName());
			// event.setEventId(reminder.getRfiEvent().getEventId());
			// event.setReferanceNumber(reminder.getRfiEvent().getReferanceNumber());
			// String businesUnit = reminder.getRfiEvent().getBusinessUnit() != null ?
			// reminder.getRfiEvent().getBusinessUnit().getDisplayName() : "";
			//
			// EventMeeting meeting = new EventMeeting();
			// meeting.setId(reminder.getRfxEventMeeting().getId());
			// meeting.setMeetingType(reminder.getRfxEventMeeting().getMeetingType());
			// meeting.setVenue(reminder.getRfxEventMeeting().getVenue());
			// meeting.setAppointmentDateTime(reminder.getRfxEventMeeting().getAppointmentDateTime());
			// meeting.setTitle(reminder.getRfxEventMeeting().getTitle());
			//
			// User eventOwner = new User();
			// eventOwner.setId(reminder.getRfiEvent().getCreatedBy().getId());
			// eventOwner.setName(reminder.getRfiEvent().getCreatedBy().getName());
			// eventOwner.setTenantId(reminder.getRfiEvent().getCreatedBy().getTenantId());
			// eventOwner.setCommunicationEmail(reminder.getRfiEvent().getCreatedBy().getCommunicationEmail());
			//
			// eventNotificationService.sendMeetingReminder(event, meeting, contactList, eventOwner, RfxTypes.RFI,
			// businesUnit);

		}

		// Sending Mails and Notifications for RFA meeting reminders
		List<RfaEventMeetingReminder> rfaReminders = rfaMeetingService.getMeetingRemindersForNotification();
		for (RfaEventMeetingReminder reminder : rfaReminders) {
			LOG.info("Rfa Sending notifocation for Reminder Date and time : " + reminder.getReminderDate() + " Meeting : " + reminder.getRfaEventMeeting().getTitle());
			rfaMeetingService.updateImmediately(reminder.getId());

			try {
				jmsTemplate.setDefaultDestinationName("QUEUE.MEETING.REMINDER");
				jmsTemplate.setDeliveryMode(DeliveryMode.PERSISTENT);
				jmsTemplate.send(new MessageCreator() {
					@Override
					public Message createMessage(Session session) throws JMSException {
						TextMessage objectMessage = session.createTextMessage();
						objectMessage.setText(RfxTypes.RFA.name() + reminder.getId());
						return objectMessage;
					}
				});
			} catch (Exception e) {
				LOG.error("Error sending message to queue : " + e.getMessage(), e);
			}

			// List<EventMeetingContact> contactList = null;
			// if (CollectionUtil.isNotEmpty(reminder.getRfaEventMeeting().getRfxEventMeetingContacts())) {
			// contactList = new ArrayList<EventMeetingContact>();
			// for (RfaEventMeetingContact contact : reminder.getRfaEventMeeting().getRfxEventMeetingContacts()) {
			// contactList.add(contact);
			// }
			// }
			//
			// Event event = new Event();
			// event.setId(reminder.getRfaEvent().getId());
			// event.setEventName(reminder.getRfaEvent().getEventName());
			// event.setEventId(reminder.getRfaEvent().getEventId());
			// event.setReferanceNumber(reminder.getRfaEvent().getReferanceNumber());
			// String businesUnit = reminder.getRfaEvent().getBusinessUnit() != null ?
			// reminder.getRfaEvent().getBusinessUnit().getDisplayName() : "";
			//
			// EventMeeting meeting = new EventMeeting();
			// meeting.setId(reminder.getRfaEventMeeting().getId());
			// meeting.setMeetingType(reminder.getRfaEventMeeting().getMeetingType());
			// meeting.setVenue(reminder.getRfaEventMeeting().getVenue());
			// meeting.setAppointmentDateTime(reminder.getRfaEventMeeting().getAppointmentDateTime());
			// meeting.setTitle(reminder.getRfaEventMeeting().getTitle());
			//
			// User eventOwner = new User();
			// eventOwner.setId(reminder.getRfaEvent().getCreatedBy().getId());
			// eventOwner.setName(reminder.getRfaEvent().getCreatedBy().getName());
			// eventOwner.setTenantId(reminder.getRfaEvent().getCreatedBy().getTenantId());
			// eventOwner.setCommunicationEmail(reminder.getRfaEvent().getCreatedBy().getCommunicationEmail());
			//
			// eventNotificationService.sendMeetingReminder(event, meeting, contactList, eventOwner, RfxTypes.RFA,
			// businesUnit);
		}
	}

}