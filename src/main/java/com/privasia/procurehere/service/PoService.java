package com.privasia.procurehere.service;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.privasia.procurehere.core.entity.Po;
import com.privasia.procurehere.core.entity.PoAudit;
import com.privasia.procurehere.core.entity.PoItem;
import com.privasia.procurehere.core.enums.PoStatus;
import com.privasia.procurehere.core.pojo.SearchFilterPoPojo;
import com.privasia.procurehere.core.pojo.TableDataInput;

import net.sf.jasperreports.engine.JasperPrint;

/**
 * @author Nitin Otageri
 */
public interface PoService {

	/**
	 * @return
	 */
	Po createPo(Po persistObj);

	/**
	 * @return
	 */
	Po savePo(Po persistObj);

	/**
	 * @param persistObj
	 * @return
	 */
	Po updatePo(Po persistObj);

	/**
	 * @param poId
	 * @return
	 */
	List<PoItem> findAllPoItemByPoIdForSummary(String poId);

	/**
	 * @param tenantId
	 * @return
	 */
	long findTotalPo(String tenantId);

	/**
	 * @param tenantId
	 * @param input
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<Po> findAllPo(String tenantId, TableDataInput input, Date startDate, Date endDate);

	/**
	 * @param tenantId
	 * @param input
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	long findTotalFilteredPo(String tenantId, TableDataInput input, Date startDate, Date endDate);

	/**
	 * @param poId
	 * @return
	 */
	Po findPoById(String poId);

	/**
	 * @param tenantId
	 * @param poArr
	 * @param response
	 * @param session
	 * @param select_all
	 * @param startDate
	 * @param endDate
	 * @param searchFilterPoPojo
	 */
	void downloadPoReports(String tenantId, String[] poArr, HttpServletResponse response, HttpSession session, boolean select_all, Date startDate, Date endDate, SearchFilterPoPojo searchFilterPoPojo);

	/**
	 * @param loggedInUser TODO
	 * @param tenantId
	 * @param status
	 * @return
	 */
	long findPoCountBasedOnStatusAndTenant(String loggedInUser, String tenantId, PoStatus status);

	/**
	 * @param loggedInUser TODO
	 * @param tenantId
	 * @param input
	 * @param status
	 * @return
	 */
	List<Po> findAllPoByStatus(String loggedInUser, String tenantId, TableDataInput input, PoStatus status);

	/**
	 * @param loggedInUserId TODO
	 * @param tenantId
	 * @param input
	 * @param status
	 * @return
	 */
	long findTotalFilteredPoByStatus(String loggedInUserId, String tenantId, TableDataInput input, PoStatus status);

	/**
	 * @param poId
	 * @return
	 */
	Po getLoadedPoById(String poId);

	/**
	 * @param poId
	 * @return
	 */
	List<PoItem> findAllPoItemByPoId(String poId);

	/**
	 * @param po
	 * @param buyerAudit
	 * @param supplierAudit TODO
	 */
	void updatePoStatus(Po po, PoAudit buyerAudit, PoAudit supplierAudit);

	/**
	 * @param loggedInUserId TODO
	 * @param loggedInUserTenantId
	 * @param input
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<Po> findAllSearchFilterPo(String loggedInUserId, String loggedInUserTenantId, TableDataInput input, Date startDate, Date endDate);

	/**
	 * @param loggedInuserId TODO
	 * @param loggedInUserTenantId
	 * @param input
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	long findTotalSearchFilterPoCount(String loggedInuserId, String loggedInUserTenantId, TableDataInput input, Date startDate, Date endDate);

	void downloadBuyerPoReports(String tenantId, String[] poArr, HttpServletResponse response, HttpSession session, boolean select_all, Date startDate, Date endDate, SearchFilterPoPojo searchFilterPoPojo, String loggedInUser);

	/**
	 * @param poId
	 * @return
	 */
	Po loadPoById(String poId);

	/**
	 * @param supplierId
	 * @param buyerId
	 * @return
	 */
	List<Po> findAllPoforSharingAll(String supplierId, String buyerId);

	/**
	 * @param poId
	 * @return
	 */
	String getBusinessUnitname(String poId);

	/**
	 * @param po
	 * @param strTimeZone
	 * @return
	 */
	JasperPrint getBuyerPoPdf(Po po, String strTimeZone);

	/**
	 * @param prId
	 * @return
	 */
	Po findByPrId(String prId);

	/**
	 * @param id
	 * @return
	 */
	Po findSupplierByFavSupplierId(String id);

}
