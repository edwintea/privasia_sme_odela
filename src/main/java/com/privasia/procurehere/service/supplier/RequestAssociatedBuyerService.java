package com.privasia.procurehere.service.supplier;

import com.privasia.procurehere.core.entity.RequestedAssociatedBuyer;

import java.util.List;

/**
 * @author Edwin
 * @author Edwin
 */

public interface RequestAssociatedBuyerService {

    /**
     * @param
     * @return
     */
    List<RequestedAssociatedBuyer> getAll();

    /**
     * @param id
     */

    RequestedAssociatedBuyer findByIds(String id);

}
