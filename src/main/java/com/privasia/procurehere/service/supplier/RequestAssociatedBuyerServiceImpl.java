package com.privasia.procurehere.service.supplier;

import com.privasia.procurehere.core.dao.SupplierMandatoryDocumentDao;
import com.privasia.procurehere.core.entity.RequestedAssociatedBuyer;
import com.privasia.procurehere.core.entity.SupplierMandatoryDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = false)
public class RequestAssociatedBuyerServiceImpl implements RequestAssociatedBuyerService {


    @Override
    public List<RequestedAssociatedBuyer> getAll(){
        return null;
    };

    @Override
    public RequestedAssociatedBuyer findByIds(String id){
        return null;
    };
}
