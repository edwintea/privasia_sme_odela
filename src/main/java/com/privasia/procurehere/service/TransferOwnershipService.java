package com.privasia.procurehere.service;

import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;

public interface TransferOwnershipService {

	void saveTransferOwnership(String fromUser,String toUser, HttpSession session, JRSwapFileVirtualizer virtualizer);

}
