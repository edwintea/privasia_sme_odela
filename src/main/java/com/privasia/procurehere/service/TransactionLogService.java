package com.privasia.procurehere.service;

import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.privasia.procurehere.core.entity.TransactionLog;
import com.privasia.procurehere.core.pojo.TableDataInput;
import com.privasia.procurehere.core.pojo.TransactionLogPojo;

public interface TransactionLogService {

	TransactionLog saveTransactionLog(TransactionLog transactionLog);

	List<TransactionLogPojo> getAlltransactionLogsForTenantId(String loggedInUserTenantId, TableDataInput input);

	long findfilteredTotalCountTransactionLogForTenantId(String loggedInUserTenantId, TableDataInput input);

	long findTotalTransactionLogForTenantId(String loggedInUserTenantId, TableDataInput input);

	void buildHeader(XSSFWorkbook workbook, XSSFSheet sheet);

	/**
	 * @param loggedInUserTenantId
	 * @return
	 */
	List<TransactionLogPojo> getAlltransactionLogsForTenantId(String loggedInUserTenantId);
}
