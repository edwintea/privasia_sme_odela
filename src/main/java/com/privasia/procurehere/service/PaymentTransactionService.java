/**
 * 
 */
package com.privasia.procurehere.service;

import java.util.List;

import com.privasia.procurehere.core.entity.PaymentTransaction;
import com.privasia.procurehere.core.pojo.TableDataInput;

/**
 * @author Nitin Otageri
 */
public interface PaymentTransactionService {

	/**
	 * @param tableParams
	 * @return
	 */
	List<PaymentTransaction> findPaymentTransactions(TableDataInput tableParams);

	/**
	 * @param paymentTransaction
	 * @return
	 */
	PaymentTransaction save(PaymentTransaction paymentTransaction);

	/**
	 * @param paymentTransaction
	 */
	PaymentTransaction update(PaymentTransaction paymentTransaction);

	/**
	 * @param id
	 * @return
	 */
	PaymentTransaction getPaymentTransactionById(String id);

	/**
	 * @return
	 */
	long findTotalPaymentTransactions();

	/**
	 * @param tableParams
	 * @return
	 */
	long findTotalFilteredPaymentTransactions(TableDataInput tableParams);

	/**
	 * @param buyerId
	 * @return
	 */
	long findTotalPaymentTransactionForBuyer(String buyerId);

	/**
	 * @param buyerId
	 * @param tableParams
	 * @return
	 */
	long findTotalFilteredPaymentTransactionsForBuyer(String buyerId, TableDataInput tableParams);

	/**
	 * @param buyerId
	 * @param tableParams
	 * @return
	 */
	List<PaymentTransaction> findPaymentTransactionsForBuyer(String buyerId, TableDataInput tableParams);

	/**
	 * @param buyerId
	 * @return
	 */
	long findTotalSuccessfulPaymentTransactionForBuyer(String buyerId);

	/**
	 * @param buyerId
	 * @param tableParams
	 * @return
	 */
	long findTotalSuccessfulFilteredPaymentTransactionsForBuyer(String buyerId, TableDataInput tableParams);

	/**
	 * @param buyerId
	 * @param tableParams
	 * @return
	 */
	List<PaymentTransaction> findSuccessfulPaymentTransactionsForBuyer(String buyerId, TableDataInput tableParams);

	/**
	 * @param loggedInUserTenantId
	 * @param input
	 * @return
	 */
	List<PaymentTransaction> findSuccessfulPaymentTransactionsForSupplier(String loggedInUserTenantId, TableDataInput input);

	/**
	 * @param loggedInUserTenantId
	 * @param input
	 * @return
	 */
	long findTotalSuccessfulFilteredPaymentTransactionsForSupplier(String loggedInUserTenantId, TableDataInput input);

	/**
	 * @param loggedInUserTenantId
	 * @return
	 */
	long findTotalSuccessfulPaymentTransactionForSupplier(String loggedInUserTenantId);

	/**
	 * @param paymentTransactionId
	 * @return
	 */
	PaymentTransaction getPaymentTransactionWithSupplierPlanByPaymentTransactionId(String paymentTransactionId);

	/**
	 * @param paymentTransactionId
	 * @return
	 */
	PaymentTransaction getPaymentTransactionWithBuyerPlanById(String paymentTransactionId);

}
