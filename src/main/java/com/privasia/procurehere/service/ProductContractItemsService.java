package com.privasia.procurehere.service;

import java.util.List;

import com.privasia.procurehere.core.entity.ProductCategory;
import com.privasia.procurehere.core.entity.ProductContractItems;
import com.privasia.procurehere.core.pojo.ProductContractItemsPojo;
import com.privasia.procurehere.core.pojo.TableDataInput;

public interface ProductContractItemsService {

	void deleteBycontractItemNumber(String contractItemNumber);

	/**
	 * @param itemId
	 * @return
	 */

	ProductContractItems findProductContractItemById(String itemId);

	/**
	 * @param loggedInUserTenantId
	 * @param id
	 * @return
	 */

	List<ProductCategory> findProductContractByTenantIDSupplierID(String loggedInUserTenantId, String id);

	/**
	 * @param loggedInUserTenantId
	 * @param input
	 * @param id
	 * @return
	 */

	List<ProductContractItemsPojo> findProductContractItemListForTenant(String loggedInUserTenantId, TableDataInput input, String id);

	/**
	 * @param loggedInUserTenantId
	 * @param input
	 * @param id TODO
	 * @return
	 */
	long findTotalFilteredProductItemListForTenant(String loggedInUserTenantId, TableDataInput input, String id);

	/**
	 * @param loggedInUserTenantId
	 * @param id
	 * @return
	 */
	long findTotalProductItemListForTenant(String loggedInUserTenantId, String id);

	ProductContractItems findProductContractItemByItemId(String itemId);

}
