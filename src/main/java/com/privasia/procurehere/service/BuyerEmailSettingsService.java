/**
 * 
 */
package com.privasia.procurehere.service;

import com.privasia.procurehere.core.entity.BuyerEmailSettings;
import com.privasia.procurehere.core.entity.User;

/**
 * @author jayshree
 *
 */
public interface BuyerEmailSettingsService {

	/**
	 * @return
	 */
	BuyerEmailSettings getEmailSetting();

	/**
	 * @param tenantId
	 * @param buyerEmailSettings
	 * @param emailPassword
	 * @param user
	 */
	void saveBuyerEmailSettings(String tenantId, BuyerEmailSettings buyerEmailSettings, String emailPassword, User user);

	BuyerEmailSettings getEmailSettingByTenantId(String tenantId);

}
