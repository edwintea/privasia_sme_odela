package com.privasia.procurehere.service;

import java.util.List;

import com.privasia.procurehere.core.entity.ErpSetup;
import com.privasia.procurehere.core.entity.RfaEvent;
import com.privasia.procurehere.core.entity.RfpEvent;
import com.privasia.procurehere.core.entity.RfqEvent;
import com.privasia.procurehere.core.entity.RftEvent;
import com.privasia.procurehere.core.entity.User;
import com.privasia.procurehere.core.exceptions.ApplicationException;
import com.privasia.procurehere.core.enums.RfxTypes;
import com.privasia.procurehere.core.pojo.AwardErpPojo;
import com.privasia.procurehere.core.pojo.AwardResponsePojo;
import com.privasia.procurehere.core.pojo.MobileEventPojo;
import com.privasia.procurehere.core.pojo.PrToAuctionErpPojo;
import com.privasia.procurehere.core.pojo.RFQResponseErpPojo;

/**
 * @author parveen
 */

public interface ErpIntegrationService {

	/**
	 * @param templateId
	 * @param adminUser
	 * @param prToAuctionErpPojo
	 * @return
	 * @throws Exception
	 */
	RftEvent copyFromRftTemplateForErp(String templateId, User adminUser, PrToAuctionErpPojo prToAuctionErpPojo) throws Exception;

	/**
	 * @param templateId
	 * @param adminUser
	 * @param prToAuctionErpPojo
	 * @return
	 * @throws Exception
	 */
	RfpEvent copyFromRfpTemplateForErp(String templateId, User adminUser, PrToAuctionErpPojo prToAuctionErpPojo) throws Exception;

	/**
	 * @param templateId
	 * @param adminUser
	 * @param prToAuctionErpPojo
	 * @return
	 * @throws Exception
	 */
	RfqEvent copyFromRfqTemplateForErp(String templateId, User adminUser, PrToAuctionErpPojo prToAuctionErpPojo) throws Exception;

	/**
	 * @param templateId
	 * @param createdBy
	 * @param prToAuctionErpPojo
	 * @return
	 * @throws Exception
	 */
	RfaEvent copyFromRfaTemplateForErp(String templateId, User createdBy, PrToAuctionErpPojo prToAuctionErpPojo) throws Exception;

	/**
	 * @param awardErpPojoList
	 * @return
	 * @throws Exception
	 */
	AwardResponsePojo sendAwardPage(List<AwardErpPojo> awardErpPojoList, String eventId, RfxTypes eventType) throws Exception;

	/**
	 * @param prId
	 * @throws Exception
	 */
	void transferPrToErp(String prId) throws Exception;

	List<MobileEventPojo> getEventTypeFromPrNo(String prNo, String tanentId);

	RfaEvent overwriteFromRfaTemplateForErp(User loggedInUser, PrToAuctionErpPojo prToAuctionErpPojo, MobileEventPojo mobileEventPojo) throws ApplicationException;

	RfqEvent overwriteRfqTemplateForErp(User loggedInUser, PrToAuctionErpPojo prToAuctionErpPojo, MobileEventPojo draftEventPojo) throws ApplicationException;

	RfpEvent overwriteRfpTemplateForErp(User loggedInUser, PrToAuctionErpPojo prToAuctionErpPojo, MobileEventPojo draftEventPojo) throws ApplicationException;

	RftEvent overwriteRftTemplateForErp(User loggedInUser, PrToAuctionErpPojo prToAuctionErpPojo, MobileEventPojo draftEventPojo) throws ApplicationException;

	/**
	 * @param rfqResponseErpPojo
	 * @param erpSetup
	 */
	void updateRfqResponse(RFQResponseErpPojo rfqResponseErpPojo, ErpSetup erpSetup);

	/**
	 * @param rfsId
	 * @param erpConfig
	 * @throws Exception
	 */
	void transferRfsToErp(String rfsId, ErpSetup erpConfig) throws Exception;

	/**
	 * @param rfsId
	 * @param erpSetup
	 * @throws Exception
	 */
	void transferRejectRfsToErp(String rfsId, ErpSetup erpSetup) throws Exception;

	/**
	 * @param event
	 * @param erpSetup
	 * @throws Exception
	 */
	void transferRejectRfsToErp(RfqEvent event, ErpSetup erpSetup) throws Exception;

	/**
	 * @param event
	 * @param erpSetup
	 * @throws Exception
	 */
	void transferRejectRfsToErp(RftEvent event, ErpSetup erpSetup) throws Exception;

	/**
	 * @param event
	 * @param erpSetup
	 * @throws Exception
	 */
	void transferRejectRfsToErp(RfaEvent event, ErpSetup erpSetup) throws Exception;

	/**
	 * @param event
	 * @param erpSetup
	 * @throws Exception
	 */
	void transferRejectRfsToErp(RfpEvent event, ErpSetup erpSetup) throws Exception;

}
