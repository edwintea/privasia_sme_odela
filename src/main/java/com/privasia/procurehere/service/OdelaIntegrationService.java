package com.privasia.procurehere.service;

import com.privasia.procurehere.core.entity.Supplier;

public interface OdelaIntegrationService {
	
	
	/**
	 * @param supplier
	 * @return
	 * @throws Exception
	 */
	boolean integrateSupplierIntoOdela(Supplier supplier) throws Exception ;
	
	

}
