package com.privasia.procurehere.service.impl;

import javax.servlet.http.HttpSession;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import com.privasia.procurehere.core.entity.RfaEvent;
import com.privasia.procurehere.core.entity.RfaEventAudit;
import com.privasia.procurehere.core.entity.RfiEvent;
import com.privasia.procurehere.core.entity.RfiEventAudit;
import com.privasia.procurehere.core.entity.RfpEvent;
import com.privasia.procurehere.core.entity.RfpEventAudit;
import com.privasia.procurehere.core.entity.RfqEvent;
import com.privasia.procurehere.core.entity.RfqEventAudit;
import com.privasia.procurehere.core.entity.RftEvent;
import com.privasia.procurehere.core.entity.RftEventAudit;
import com.privasia.procurehere.core.entity.User;
import com.privasia.procurehere.core.enums.ActionType;
import com.privasia.procurehere.core.enums.AuditActionType;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.service.EventAuditService;
import com.privasia.procurehere.service.RfaEventService;
import com.privasia.procurehere.service.RfiEventService;
import com.privasia.procurehere.service.RfpEventService;
import com.privasia.procurehere.service.RfqEventService;
import com.privasia.procurehere.service.RftEventService;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;

/**
 * @author Sarang
 */

@Service
@EnableAsync
public class SnapShotAuditServiceImpl implements SnapShotAuditService {

	@Autowired
	RfqEventService rfqEventService;

	@Autowired
	RfaEventService rfaEventService;

	@Autowired
	RfpEventService rfpEventService;

	@Autowired
	RftEventService rftEventService;

	@Autowired
	RfiEventService rfiEventService;

	@Autowired
	EventAuditService eventAuditService;

	@Autowired
	MessageSource messageSource;

	private static final Logger LOG = Logger.getLogger(Global.BUYER_LOG);

	@Override
	@Async
	public void doRfqAudit(RfqEvent event, HttpSession session, RfqEvent persistObj, User loginUser, AuditActionType type, String message, JRSwapFileVirtualizer virtualizer) {
		byte[] summarySnapshot = null;
		try {
			JasperPrint eventSummary = rfqEventService.getEvaluationSummaryPdf(event, loginUser, (String) session.getAttribute(Global.SESSION_TIME_ZONE_KEY), virtualizer);
			summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);

			RfqEventAudit audit = new RfqEventAudit(loginUser.getBuyer(), persistObj, loginUser, new java.util.Date(), type, messageSource.getMessage(message, new Object[] { persistObj.getEventName(), event.getSuspensionType() }, Global.LOCALE), summarySnapshot);
			eventAuditService.save(audit);
		} catch (Exception e) {
			LOG.error("Error while Store summary PDF as byte : " + e.getMessage(), e);
		}

	}

	@Override
	@Async
	public void doRfaAudit(RfaEvent event, HttpSession session, RfaEvent persistObj, User loginUser, AuditActionType type, String message, JRSwapFileVirtualizer virtualizer) {

		byte[] summarySnapshot = null;
		try {
			JasperPrint eventSummary = rfaEventService.getEvaluationSummaryPdf(event, loginUser, (String) session.getAttribute(Global.SESSION_TIME_ZONE_KEY), virtualizer);
			summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
			RfaEventAudit audit = new RfaEventAudit(loginUser.getBuyer(), persistObj, loginUser, new java.util.Date(), type, messageSource.getMessage(message, new Object[] { persistObj.getEventName(), event.getSuspendRemarks() }, Global.LOCALE), summarySnapshot);
			eventAuditService.save(audit);
		} catch (Exception e) {
			LOG.error("Error while Store summary PDF as byte : " + e.getMessage(), e);
		}

	}

	@Override
	@Async
	public void doRfpAudit(RfpEvent event, HttpSession session, RfpEvent persistObj, User loginUser, AuditActionType type, String message, JRSwapFileVirtualizer virtualizer) {

		byte[] summarySnapshot = null;
		try {
			JasperPrint eventSummary = rfpEventService.getEvaluationSummaryPdf(event, loginUser, (String) session.getAttribute(Global.SESSION_TIME_ZONE_KEY), virtualizer);
			summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
			RfpEventAudit audit = new RfpEventAudit(loginUser.getBuyer(), persistObj, loginUser, new java.util.Date(), type, messageSource.getMessage(message, new Object[] { persistObj.getEventName(), event.getSuspensionType() }, Global.LOCALE), summarySnapshot);
			eventAuditService.save(audit);
		} catch (Exception e) {
			LOG.error("Error while Store summary PDF as byte : " + e.getMessage(), e);
		}

	}

	@Override
	@Async
	public void doRftAudit(RftEvent event, HttpSession session, RftEvent persistObj, User loginUser, AuditActionType type, String message, JRSwapFileVirtualizer virtualizer) {

		byte[] summarySnapshot = null;
		try {
			JasperPrint eventSummary = rftEventService.getEvaluationSummaryPdf(event, loginUser, (String) session.getAttribute(Global.SESSION_TIME_ZONE_KEY), virtualizer);
			summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
			RftEventAudit audit = new RftEventAudit(loginUser.getBuyer(), persistObj, loginUser, new java.util.Date(), type, messageSource.getMessage(message, new Object[] { persistObj.getEventName(), event.getSuspensionType() }, Global.LOCALE), summarySnapshot);
			eventAuditService.save(audit);
		} catch (Exception e) {
			LOG.error("Error while Store summary PDF as byte : " + e.getMessage(), e);
		}

	}

	@Override
	@Async
	public void doRfiAudit(RfiEvent event, HttpSession session, RfiEvent persistObj, User loginUser, AuditActionType type, String message, JRSwapFileVirtualizer virtualizer) {

		byte[] summarySnapshot = null;
		try {
			JasperPrint eventSummary = rfiEventService.getEvaluationSummaryPdf(event, loginUser, (String) session.getAttribute(Global.SESSION_TIME_ZONE_KEY), virtualizer);
			summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
			RfiEventAudit audit = new RfiEventAudit(loginUser.getBuyer(), persistObj, loginUser, new java.util.Date(), type, messageSource.getMessage(message, new Object[] { persistObj.getEventName(), event.getSuspensionType() }, Global.LOCALE), summarySnapshot);
			eventAuditService.save(audit);
		} catch (Exception e) {
			LOG.error("Error while Store summary PDF as byte : " + e.getMessage(), e);
		}

	}

}
