package com.privasia.procurehere.service.impl;

import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibm.icu.math.BigDecimal;
import com.privasia.procurehere.core.dao.CurrencyDao;
import com.privasia.procurehere.core.dao.ErpSetupDao;
import com.privasia.procurehere.core.dao.ProductContractItemsDao;
import com.privasia.procurehere.core.dao.ProductListMaintenanceDao;
import com.privasia.procurehere.core.dao.RequestAuditDao;
import com.privasia.procurehere.core.dao.RfaBqDao;
import com.privasia.procurehere.core.dao.RfaBqItemDao;
import com.privasia.procurehere.core.dao.RfaEventAuditDao;
import com.privasia.procurehere.core.dao.RfaEventDao;
import com.privasia.procurehere.core.dao.RfpBqDao;
import com.privasia.procurehere.core.dao.RfpBqItemDao;
import com.privasia.procurehere.core.dao.RfpEventAuditDao;
import com.privasia.procurehere.core.dao.RfpEventDao;
import com.privasia.procurehere.core.dao.RfqBqDao;
import com.privasia.procurehere.core.dao.RfqBqItemDao;
import com.privasia.procurehere.core.dao.RfqEventAuditDao;
import com.privasia.procurehere.core.dao.RfqEventDao;
import com.privasia.procurehere.core.dao.RftBqDao;
import com.privasia.procurehere.core.dao.RftBqItemDao;
import com.privasia.procurehere.core.dao.RftEventAuditDao;
import com.privasia.procurehere.core.dao.RftEventDao;
import com.privasia.procurehere.core.dao.UserDao;
import com.privasia.procurehere.core.entity.Buyer;
import com.privasia.procurehere.core.entity.Currency;
import com.privasia.procurehere.core.entity.ErpAwardStaging;
import com.privasia.procurehere.core.entity.ErpSetup;
import com.privasia.procurehere.core.entity.NotificationMessage;
import com.privasia.procurehere.core.entity.Pr;
import com.privasia.procurehere.core.entity.PrAudit;
import com.privasia.procurehere.core.entity.PrItem;
import com.privasia.procurehere.core.entity.ProductContractItems;
import com.privasia.procurehere.core.entity.ProductItem;
import com.privasia.procurehere.core.entity.RequestAudit;
import com.privasia.procurehere.core.entity.RfaBqItem;
import com.privasia.procurehere.core.entity.RfaEvent;
import com.privasia.procurehere.core.entity.RfaEventAudit;
import com.privasia.procurehere.core.entity.RfaEventAwardAudit;
import com.privasia.procurehere.core.entity.RfaEventBq;
import com.privasia.procurehere.core.entity.RfpBqItem;
import com.privasia.procurehere.core.entity.RfpEvent;
import com.privasia.procurehere.core.entity.RfpEventAudit;
import com.privasia.procurehere.core.entity.RfpEventAwardAudit;
import com.privasia.procurehere.core.entity.RfpEventBq;
import com.privasia.procurehere.core.entity.RfqBqItem;
import com.privasia.procurehere.core.entity.RfqEvent;
import com.privasia.procurehere.core.entity.RfqEventAudit;
import com.privasia.procurehere.core.entity.RfqEventAwardAudit;
import com.privasia.procurehere.core.entity.RfqEventBq;
import com.privasia.procurehere.core.entity.RftBqItem;
import com.privasia.procurehere.core.entity.RftEvent;
import com.privasia.procurehere.core.entity.RftEventAudit;
import com.privasia.procurehere.core.entity.RftEventAwardAudit;
import com.privasia.procurehere.core.entity.RftEventBq;
import com.privasia.procurehere.core.entity.SourcingFormRequest;
import com.privasia.procurehere.core.entity.Uom;
import com.privasia.procurehere.core.entity.User;
import com.privasia.procurehere.core.enums.AuditActionType;
import com.privasia.procurehere.core.enums.ErpIntegrationTypeForPr;
import com.privasia.procurehere.core.enums.NotificationType;
import com.privasia.procurehere.core.enums.PrAuditType;
import com.privasia.procurehere.core.enums.PrStatus;
import com.privasia.procurehere.core.enums.PricingTypes;
import com.privasia.procurehere.core.enums.RequestAuditType;
import com.privasia.procurehere.core.enums.RfxTypes;
import com.privasia.procurehere.core.enums.SourcingFormStatus;
import com.privasia.procurehere.core.exceptions.ApplicationException;
import com.privasia.procurehere.core.pojo.AwardErpPojo;
import com.privasia.procurehere.core.pojo.AwardReferenceNumberPojo;
import com.privasia.procurehere.core.pojo.AwardResponsePojo;
import com.privasia.procurehere.core.pojo.MobileEventPojo;
import com.privasia.procurehere.core.pojo.PrErp2DeletePojo;
import com.privasia.procurehere.core.pojo.PrErp2Pojo;
import com.privasia.procurehere.core.pojo.PrErpPojo;
import com.privasia.procurehere.core.pojo.PrItemErp2DeletePojo;
import com.privasia.procurehere.core.pojo.PrItemErp2Pojo;
import com.privasia.procurehere.core.pojo.PrToAuctionDetailsErpPojo;
import com.privasia.procurehere.core.pojo.PrToAuctionErpPojo;
import com.privasia.procurehere.core.pojo.RFQResponseErpPojo;
import com.privasia.procurehere.core.pojo.TransferAwardBqPojo;
import com.privasia.procurehere.core.pojo.TransferAwardPojo;
import com.privasia.procurehere.core.utils.CollectionUtil;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.core.utils.SecurityLibrary;
import com.privasia.procurehere.core.utils.StringUtils;
import com.privasia.procurehere.integration.RequestResponseLoggingInterceptor;
import com.privasia.procurehere.integration.RestTemplateResponseErrorHandler;
import com.privasia.procurehere.service.BuyerService;
import com.privasia.procurehere.service.BuyerSettingsService;
import com.privasia.procurehere.service.DashboardNotificationService;
import com.privasia.procurehere.service.ErpAuditService;
import com.privasia.procurehere.service.ErpAwardStaggingService;
import com.privasia.procurehere.service.ErpIntegrationService;
import com.privasia.procurehere.service.ErpSetupService;
import com.privasia.procurehere.service.EventAwardAuditService;
import com.privasia.procurehere.service.IndustryCategoryService;
import com.privasia.procurehere.service.NotificationService;
import com.privasia.procurehere.service.PrAuditService;
import com.privasia.procurehere.service.PrService;
import com.privasia.procurehere.service.RfaBqService;
import com.privasia.procurehere.service.RfaEventService;
import com.privasia.procurehere.service.RfpBqService;
import com.privasia.procurehere.service.RfpEventService;
import com.privasia.procurehere.service.RfqBqService;
import com.privasia.procurehere.service.RfqEventService;
import com.privasia.procurehere.service.RftBqService;
import com.privasia.procurehere.service.RftEventService;
import com.privasia.procurehere.service.SourcingFormRequestService;
import com.privasia.procurehere.service.UomService;
import com.privasia.procurehere.service.UserService;

import freemarker.template.Configuration;

@Service
public class ErpIntegrationServiceImpl implements ErpIntegrationService {

	private static final Logger LOG = Logger.getLogger(Global.ERP_LOG);

	@Autowired
	RfaBqService rfaBqService;

	@Autowired
	RfpBqService rfpBqService;

	@Autowired
	RfqBqService rfqBqService;

	@Autowired
	RftBqService rftBqService;

	@Autowired
	RftEventDao rftEventDao;

	@Autowired
	RftEventService rftEventService;

	@Autowired
	RftEventAuditDao rftEventAuditDao;

	@Autowired
	RfaEventAuditDao rfaEventAuditDao;

	@Autowired
	RfpEventAuditDao rfpEventAuditDao;

	@Autowired
	RftBqItemDao rftBqItemDao;

	@Autowired
	RftBqDao rftEventBqDao;

	@Autowired
	RfpEventDao rfpEventDao;

	@Autowired
	RfpEventService rfpEventService;

	@Autowired
	RfpBqItemDao rfpBqItemDao;

	@Autowired
	RfpBqDao rfpEventBqDao;

	@Autowired
	RfqEventDao rfqEventDao;

	@Autowired
	RfqEventService rfqEventService;

	@Autowired
	RfqBqItemDao rfqBqItemDao;

	@Autowired
	RfqBqDao rfqEventBqDao;

	@Autowired
	RfaEventDao rfaEventDao;

	@Autowired
	RfaEventService rfaEventService;

	@Autowired
	RfaBqItemDao rfaBqItemDao;

	@Autowired
	RfaBqDao rfaEventBqDao;

	@Autowired
	UomService uomService;

	@Autowired
	IndustryCategoryService industryCategoryService;

	@Autowired
	CurrencyDao currencyDao;

	@Autowired
	ErpSetupDao erpConfigDao;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	ErpAuditService erpAuditService;

	@Autowired
	UserDao userDao;

	@Autowired
	PrService prService;

	@Autowired
	PrAuditService prAuditService;

	@Autowired
	RfqEventAuditDao rfqEventAuditDao;

	@Autowired
	BuyerService buyerService;

	@Autowired
	UserService userService;

	@Autowired
	ErpSetupDao erpSetupDao;

	@Autowired
	EventAwardAuditService eventAwardAuditService;

	@Autowired
	BuyerSettingsService buyerSettingsService;

	@Value("${app.url}")
	String APP_URL;

	@Autowired
	NotificationService notificationService;

	@Autowired
	Configuration freemarkerConfiguration;

	@Autowired
	MessageSource messageSource;

	@Autowired
	DashboardNotificationService dashboardNotificationService;

	@Autowired
	ErpSetupService erpSetupService;

	@Autowired
	ErpAwardStaggingService erpAwardStaggingService;

	@Autowired
	ProductContractItemsDao productContractItemsDao;

	@Autowired
	SourcingFormRequestService requestService;

	@Autowired
	RequestAuditDao requestAuditDao;

	@Autowired
	ProductListMaintenanceDao productListMaintenanceDao;

	@Override
	@Transactional(readOnly = false, rollbackFor = { Exception.class, ApplicationException.class })
	public RftEvent copyFromRftTemplateForErp(String templateId, User adminUser, PrToAuctionErpPojo prToAuctionErpPojo) throws Exception {
		RftEvent newEvent = rftEventService.copyFromTemplate(templateId, adminUser, null);
		newEvent = constructRftEventDetials(prToAuctionErpPojo, newEvent);
		// rftEventService.addAssociateOwners(adminUser, newEvent);
		return newEvent;
	}

	/**
	 * @param prToAuctionErpPojo
	 * @param newEvent
	 * @return
	 * @throws ApplicationException
	 */
	public RftEvent constructRftEventDetials(PrToAuctionErpPojo prToAuctionErpPojo, RftEvent newEvent) throws ApplicationException {
		if (StringUtils.checkString(prToAuctionErpPojo.getPrNo()).length() > 0) {
			LOG.info("prToAuctionErpPojo PrNo :" + prToAuctionErpPojo.getPrNo());
			newEvent.setReferanceNumber(prToAuctionErpPojo.getPrNo());
			newEvent.setErpDocNo(prToAuctionErpPojo.getPrNo());
		}
		if (StringUtils.checkString(prToAuctionErpPojo.getCurr()).length() > 0) {
			LOG.info("prToAuctionErpPojo Currrency :" + prToAuctionErpPojo.getCurr());
			Currency baseCurrency = currencyDao.findByCurrencyCode(prToAuctionErpPojo.getCurr());
			newEvent.setBaseCurrency(baseCurrency);
		}
		if (StringUtils.checkString(prToAuctionErpPojo.getItmCat()).length() > 0) {
			LOG.info("prToAuctionErpPojo Itm Cat :" + prToAuctionErpPojo.getItmCat());
			// IndustryCategory ic = industryCategoryService.getIndustryCategoryByCode(prToAuctionErpPojo.getItmCat(),
			// newEvent.getTenantId());
			// newEvent.setIndustryCategories(Arrays.asList(ic));
		}

		newEvent = rftEventDao.saveOrUpdate(newEvent);
		RftEventBq bq = new RftEventBq();
		bq.setName("Bill of Quantity");
		bq.setField1Label("Item Category");
		bq.setField1ToShowSupplier(Boolean.FALSE);
		bq.setField2Label("Bq Item Code");
		bq.setField2ToShowSupplier(Boolean.TRUE);
		bq.setField3Label("Material Group");
		bq.setField3ToShowSupplier(Boolean.FALSE);
		bq.setField4Label("Brand");
		bq.setField4ToShowSupplier(Boolean.TRUE);
		bq.setField5Label("MFR Part No");
		bq.setField5ToShowSupplier(Boolean.TRUE);
		bq.setField6Label(prToAuctionErpPojo.getField6Label());
		bq.setField6Label("Purchase Group");
		bq.setField7Label("Delivery Date");
		bq.setField7ToShowSupplier(Boolean.TRUE);
		bq.setField8Label("Item No");
		bq.setField8ToShowSupplier(Boolean.FALSE);
		bq.setField9Label(prToAuctionErpPojo.getField9Label());
		bq.setField9ToShowSupplier(Boolean.FALSE);
		bq.setField10Label(prToAuctionErpPojo.getField10Label());
		bq.setField10ToShowSupplier(Boolean.FALSE);

		if (CollectionUtil.isNotEmpty(prToAuctionErpPojo.getAuctionDetails())) {

			bq.setRfxEvent(newEvent);
			RftEventBq dbBq = rftEventBqDao.saveOrUpdate(bq);
			LOG.info("dbBq :" + dbBq.getName());
			int order = 0;
			int noOfSections = 1;
			int level = 1;
			String delDateString = "";

			Map<String, RftBqItem> sections = new HashMap<String, RftBqItem>();

			RftBqItem defaultBqItemSection = null;
			RftBqItem section = null;
			for (PrToAuctionDetailsErpPojo detail : prToAuctionErpPojo.getAuctionDetails()) {

				String sapItemNo = StringUtils.checkString(detail.getItmNo());
				if (sapItemNo.length() > 5) {
					sapItemNo = sapItemNo.substring(0, 5);
				}
				String sapItemName = detail.getMatDesc();
				BigDecimal sapQty = new BigDecimal(StringUtils.checkString(detail.getQty()).length() > 0 ? StringUtils.checkString(detail.getQty()) : "0");
				String sapUom = detail.getOrdrUom();
				level = 1;

				if (StringUtils.checkString(detail.getExtSubItm()).length() > 0) {
					section = sections.get(sapItemNo);
					if (section == null) {
						section = new RftBqItem();
						section.setLevel(noOfSections++);
						section.setOrder(0);
						section.setItemName(sapItemName);
						section.setBq(dbBq);
						section.setRfxEvent(newEvent);
						section.setField8(StringUtils.checkString(detail.getItmNo())); // used for SAP item no
						section = rftBqItemDao.saveOrUpdate(section);

						section.setField9(String.valueOf(0)); // used temporarily to get the next item order for
																// section
						// sections.put(sapItemNo, section);
					}

					order = Integer.parseInt(section.getField9()) + 1;
					section.setField9(String.valueOf(order));
					sections.put(sapItemNo, section);

					// Extract the EXT info for the line item
					sapItemName = detail.getExtValItm(); // ex_svcDesc from SAP
					sapQty = new BigDecimal(StringUtils.checkString(detail.getExtSvcQty()).length() > 0 ? StringUtils.checkString(detail.getExtSvcQty()) : "0");
					sapUom = detail.getExtBaseUom();
					level = section.getLevel();
					sapItemNo = detail.getExtSubItm();
				} else {

					if (defaultBqItemSection == null) {
						defaultBqItemSection = new RftBqItem();
						defaultBqItemSection.setLevel(noOfSections++);
						defaultBqItemSection.setOrder(0);
						defaultBqItemSection.setItemName("Item Section");
						defaultBqItemSection.setBq(dbBq);
						defaultBqItemSection.setRfxEvent(newEvent);
						defaultBqItemSection.setField8(StringUtils.checkString(detail.getItmNo()));
						defaultBqItemSection = rftBqItemDao.saveOrUpdate(defaultBqItemSection);
						defaultBqItemSection.setField9(String.valueOf(0));
					}

					order = Integer.parseInt(defaultBqItemSection.getField9()) + 1;
					defaultBqItemSection.setField9(String.valueOf(order));
					level = defaultBqItemSection.getLevel();
					section = defaultBqItemSection;
				}

				// setting Item
				RftBqItem bqItem = new RftBqItem();
				bqItem.setLevel(level);
				bqItem.setOrder(order);
				bqItem.setItemName(sapItemName);
				bqItem.setQuantity(sapQty.toBigDecimal());
				bqItem.setPriceType(PricingTypes.NORMAL_PRICE);
				// bqItem.setField10(sapItemNo); // used for SAP item no

				if (StringUtils.checkString(sapUom).length() > 0) {
					LOG.info("prToAuction deatils ErpPojo UOM :" + sapUom);
					Uom uom = uomService.getUomByUomAndTenantId(sapUom, newEvent.getTenantId());
					if (uom == null) {
						throw new ApplicationException("Invalid UOM : " + sapUom);
					}
					bqItem.setUom(uom);
				} else {
					throw new ApplicationException("Invalid UOM : " + sapUom);
				}
				// setting extra field
				bqItem.setField1(StringUtils.checkString(detail.getField1()));
				bqItem.setField2(StringUtils.checkString(detail.getField2()));
				bqItem.setField3(StringUtils.checkString(detail.getField3()));
				bqItem.setField4(StringUtils.checkString(detail.getField4()));
				bqItem.setField5(StringUtils.checkString(detail.getField5()));
				bqItem.setField6(StringUtils.checkString(detail.getField6()));
				bqItem.setField7(StringUtils.checkString(detail.getField7()));
				bqItem.setField8(StringUtils.checkString(detail.getItmNo()));
				bqItem.setField9(StringUtils.checkString(detail.getField9()));
				bqItem.setField10(StringUtils.checkString(detail.getField10()));

				bqItem.setParent(section);
				bqItem.setBq(dbBq);
				bqItem.setRfxEvent(newEvent);
				rftBqItemDao.saveOrUpdate(bqItem);

				if (StringUtils.checkString(delDateString).length() == 0) {
					delDateString = detail.getField7();
				}

			}
			if (StringUtils.checkString(delDateString).length() > 0) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				Date deliveryDate = null;
				try {
					deliveryDate = sdf.parse(delDateString);
					newEvent.setDeliveryDate(deliveryDate);
					newEvent = rftEventDao.saveOrUpdate(newEvent);
				} catch (ParseException e) {
					LOG.error("Error while parsing Delivery date :" + e.getMessage(), e);
				}
			}

		}
		// bq.setRfxEvent(newEvent);
		// RftEventBq dbBq = rftEventBqDao.saveOrUpdate(bq);
		// LOG.info("dbBq :" + dbBq.getName());
		// int order = 0;
		// // setting Section for each Item
		// RftBqItem bqItemSection = new RftBqItem();
		// bqItemSection.setLevel(1);
		// bqItemSection.setOrder(order++);
		// bqItemSection.setItemName("Item Section");
		// bqItemSection.setBq(dbBq);
		// bqItemSection.setRfxEvent(newEvent);
		// bqItemSection = rftBqItemDao.saveOrUpdate(bqItemSection);
		// String delDateString = "";
		// for (PrToAuctionDetailsErpPojo detail : prToAuctionErpPojo.getAuctionDetails()) {
		// // setting Item
		// RftBqItem bqItem = new RftBqItem();
		// bqItem.setLevel(1);
		// bqItem.setOrder(order++);
		// bqItem.setItemName(detail.getMatDesc());
		// bqItem.setPriceType(PricingTypes.NORMAL_PRICE);
		//
		// BigDecimal qty = new BigDecimal(detail.getQty());
		// bqItem.setQuantity(qty.toBigInteger());
		//
		// if (StringUtils.checkString(detail.getOrdrUom()).length() > 0) {
		// LOG.info("prToAuction deatils ErpPojo UOM :" + detail.getOrdrUom());
		// Uom uom = uomService.getUomByUomAndTenantId(detail.getOrdrUom(), newEvent.getTenantId());
		// if (uom == null) {
		// throw new ApplicationException("Invalid UOM : " + detail.getOrdrUom());
		// }
		// bqItem.setUom(uom);
		// } else {
		// throw new ApplicationException("Invalid UOM : " + detail.getOrdrUom());
		// }
		// // setting extra field
		// bqItem.setField1(detail.getField1());
		// bqItem.setField2(detail.getField2());
		// bqItem.setField3(detail.getField3());
		// bqItem.setField4(detail.getField4());
		// bqItem.setField5(detail.getField5());
		// bqItem.setField6(detail.getField6());
		// bqItem.setField7(detail.getField7());
		// bqItem.setField8(detail.getField8());
		// bqItem.setField9(detail.getField9());
		// bqItem.setField10(detail.getField10());
		//
		// bqItem.setParent(bqItemSection);
		// bqItem.setBq(dbBq);
		// bqItem.setRfxEvent(newEvent);
		// rftBqItemDao.saveOrUpdate(bqItem);
		// if (StringUtils.checkString(delDateString).length() == 0) {
		// delDateString = detail.getField7();
		// }
		//
		// }
		// if (StringUtils.checkString(delDateString).length() > 0) {
		// SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		// Date deliveryDate = null;
		// try {
		// deliveryDate = sdf.parse(delDateString);
		// newEvent.setDeliveryDate(deliveryDate);
		// newEvent = rftEventDao.saveOrUpdate(newEvent);
		// } catch (ParseException e) {
		// LOG.error("Error while parsing Delivery date :" + e.getMessage(), e);
		// }
		// }
		// }
		return newEvent;
	}

	@Override
	@Transactional(readOnly = false, rollbackFor = { Exception.class, ApplicationException.class })
	public RfpEvent copyFromRfpTemplateForErp(String templateId, User adminUser, PrToAuctionErpPojo prToAuctionErpPojo) throws Exception {
		RfpEvent newEvent = rfpEventService.copyFromTemplate(templateId, adminUser, null);
		newEvent = constructRfpEventDetials(prToAuctionErpPojo, newEvent);
		// rfpEventService.addAssociateOwners(adminUser, newEvent);
		return newEvent;
	}

	/**
	 * @param prToAuctionErpPojo
	 * @param newEvent
	 * @return
	 * @throws ApplicationException
	 */
	public RfpEvent constructRfpEventDetials(PrToAuctionErpPojo prToAuctionErpPojo, RfpEvent newEvent) throws ApplicationException {
		if (StringUtils.checkString(prToAuctionErpPojo.getPrNo()).length() > 0) {
			LOG.info("prToAuctionErpPojo PrNo :" + prToAuctionErpPojo.getPrNo());
			newEvent.setReferanceNumber(prToAuctionErpPojo.getPrNo());
			newEvent.setErpDocNo(prToAuctionErpPojo.getPrNo());
		}
		if (StringUtils.checkString(prToAuctionErpPojo.getCurr()).length() > 0) {
			LOG.info("prToAuctionErpPojo Currrency :" + prToAuctionErpPojo.getCurr());
			Currency baseCurrency = currencyDao.findByCurrencyCode(prToAuctionErpPojo.getCurr());
			newEvent.setBaseCurrency(baseCurrency);
		}
		if (StringUtils.checkString(prToAuctionErpPojo.getItmCat()).length() > 0) {
			LOG.info("prToAuctionErpPojo Itm Cat :" + prToAuctionErpPojo.getItmCat());
			// IndustryCategory ic = industryCategoryService.getIndustryCategoryByCode(prToAuctionErpPojo.getItmCat(),
			// newEvent.getTenantId());
			// newEvent.setIndustryCategories(Arrays.asList(ic));
		}

		newEvent = rfpEventDao.saveOrUpdate(newEvent);
		RfpEventBq bq = new RfpEventBq();
		bq.setName("Bill of Quantity");
		bq.setField1Label("Item Category");
		bq.setField1ToShowSupplier(Boolean.FALSE);
		bq.setField2Label("Bq Item Code");
		bq.setField2ToShowSupplier(Boolean.TRUE);
		bq.setField3Label("Material Group");
		bq.setField3ToShowSupplier(Boolean.FALSE);
		bq.setField4Label("Brand");
		bq.setField4ToShowSupplier(Boolean.TRUE);
		bq.setField5Label("MFR Part No");
		bq.setField5ToShowSupplier(Boolean.TRUE);
		bq.setField6Label(prToAuctionErpPojo.getField6Label());
		bq.setField6Label("Purchase Group");
		bq.setField7Label("Delivery Date");
		bq.setField7ToShowSupplier(Boolean.TRUE);
		bq.setField8Label("Item No");
		bq.setField8ToShowSupplier(Boolean.FALSE);
		bq.setField9Label(prToAuctionErpPojo.getField9Label());
		bq.setField9ToShowSupplier(Boolean.FALSE);
		bq.setField10Label(prToAuctionErpPojo.getField10Label());
		bq.setField10ToShowSupplier(Boolean.FALSE);

		if (CollectionUtil.isNotEmpty(prToAuctionErpPojo.getAuctionDetails())) {

			bq.setRfxEvent(newEvent);
			RfpEventBq dbBq = rfpEventBqDao.saveOrUpdate(bq);
			LOG.info("dbBq :" + dbBq.getName());
			int order = 0;
			int noOfSections = 1;
			int level = 1;
			String delDateString = "";

			Map<String, RfpBqItem> sections = new HashMap<String, RfpBqItem>();

			RfpBqItem defaultBqItemSection = null;
			RfpBqItem section = null;
			for (PrToAuctionDetailsErpPojo detail : prToAuctionErpPojo.getAuctionDetails()) {

				String sapItemNo = StringUtils.checkString(detail.getItmNo());
				if (sapItemNo.length() > 5) {
					sapItemNo = sapItemNo.substring(0, 5);
				}
				String sapItemName = detail.getMatDesc();
				BigDecimal sapQty = new BigDecimal(StringUtils.checkString(detail.getQty()).length() > 0 ? StringUtils.checkString(detail.getQty()) : "0");
				String sapUom = detail.getOrdrUom();
				level = 1;

				if (StringUtils.checkString(detail.getExtSubItm()).length() > 0) {
					section = sections.get(sapItemNo);
					if (section == null) {
						section = new RfpBqItem();
						section.setLevel(noOfSections++);
						section.setOrder(0);
						section.setItemName(sapItemName);
						section.setBq(dbBq);
						section.setRfxEvent(newEvent);
						section.setField8(StringUtils.checkString(detail.getItmNo())); // used for SAP item no
						section = rfpBqItemDao.saveOrUpdate(section);

						section.setField9(String.valueOf(0)); // used temporarily to get the next item order for
																// section
						sections.put(sapItemNo, section);
					}

					order = Integer.parseInt(section.getField9()) + 1;
					section.setField9(String.valueOf(order));
					sections.put(sapItemNo, section);

					// Extract the EXT info for the line item
					sapItemName = detail.getExtValItm(); // ex_svcDesc from SAP
					sapQty = new BigDecimal(StringUtils.checkString(detail.getExtSvcQty()).length() > 0 ? StringUtils.checkString(detail.getExtSvcQty()) : "0");
					sapUom = detail.getExtBaseUom();
					level = section.getLevel();
					sapItemNo = detail.getExtSubItm();
				} else {

					if (defaultBqItemSection == null) {
						defaultBqItemSection = new RfpBqItem();
						defaultBqItemSection.setLevel(noOfSections++);
						defaultBqItemSection.setOrder(0);
						defaultBqItemSection.setItemName("Item Section");
						defaultBqItemSection.setBq(dbBq);
						defaultBqItemSection.setRfxEvent(newEvent);
						defaultBqItemSection.setField8(StringUtils.checkString(detail.getItmNo()));
						defaultBqItemSection = rfpBqItemDao.saveOrUpdate(defaultBqItemSection);
						defaultBqItemSection.setField9(String.valueOf(0));
					}

					order = Integer.parseInt(defaultBqItemSection.getField9()) + 1;
					defaultBqItemSection.setField9(String.valueOf(order));
					level = defaultBqItemSection.getLevel();
					section = defaultBqItemSection;
				}

				// setting Item
				RfpBqItem bqItem = new RfpBqItem();
				bqItem.setLevel(level);
				bqItem.setOrder(order);
				bqItem.setItemName(sapItemName);
				bqItem.setQuantity(sapQty.toBigDecimal());
				bqItem.setPriceType(PricingTypes.NORMAL_PRICE);
				bqItem.setField8(StringUtils.checkString(detail.getItmNo())); // used for SAP item no

				if (StringUtils.checkString(sapUom).length() > 0) {
					LOG.info("prToAuction deatils ErpPojo UOM :" + sapUom);
					Uom uom = uomService.getUomByUomAndTenantId(sapUom, newEvent.getTenantId());
					if (uom == null) {
						throw new ApplicationException("Invalid UOM : " + sapUom);
					}
					bqItem.setUom(uom);
				} else {
					throw new ApplicationException("Invalid UOM : " + sapUom);
				}
				// setting extra field
				bqItem.setField1(StringUtils.checkString(detail.getField1()));
				bqItem.setField2(StringUtils.checkString(detail.getField2()));
				bqItem.setField3(StringUtils.checkString(detail.getField3()));
				bqItem.setField4(StringUtils.checkString(detail.getField4()));
				bqItem.setField5(StringUtils.checkString(detail.getField5()));
				bqItem.setField6(StringUtils.checkString(detail.getField6()));
				bqItem.setField7(StringUtils.checkString(detail.getField7()));
				bqItem.setField8(StringUtils.checkString(detail.getItmNo()));
				bqItem.setField9(StringUtils.checkString(detail.getField9()));
				bqItem.setField10(StringUtils.checkString(detail.getField10()));

				bqItem.setParent(section);
				bqItem.setBq(dbBq);
				bqItem.setRfxEvent(newEvent);
				rfpBqItemDao.saveOrUpdate(bqItem);

				if (StringUtils.checkString(delDateString).length() == 0) {
					delDateString = detail.getField7();
				}

			}
			if (StringUtils.checkString(delDateString).length() > 0) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				Date deliveryDate = null;
				try {
					deliveryDate = sdf.parse(delDateString);
					newEvent.setDeliveryDate(deliveryDate);
					newEvent = rfpEventDao.saveOrUpdate(newEvent);
				} catch (ParseException e) {
					LOG.error("Error while parsing Delivery date :" + e.getMessage(), e);
				}
			}
		}

		// bq.setRfxEvent(newEvent);
		// RfpEventBq dbBq = rfpEventBqDao.saveOrUpdate(bq);
		// LOG.info("dbBq :" + dbBq.getName());
		// int order = 0;
		// // setting Section for each Item
		// RfpBqItem bqItemSection = new RfpBqItem();
		// bqItemSection.setLevel(1);
		// bqItemSection.setOrder(order++);
		// bqItemSection.setItemName("Item Section");
		// bqItemSection.setBq(dbBq);
		// bqItemSection.setRfxEvent(newEvent);
		// bqItemSection = rfpBqItemDao.saveOrUpdate(bqItemSection);
		// String delDateString = "";
		// for (PrToAuctionDetailsErpPojo detail : prToAuctionErpPojo.getAuctionDetails()) {
		// // setting Item
		// RfpBqItem bqItem = new RfpBqItem();
		// bqItem.setLevel(1);
		// bqItem.setOrder(order++);
		// bqItem.setItemName(detail.getMatDesc());
		// bqItem.setPriceType(PricingTypes.NORMAL_PRICE);
		//
		// BigDecimal qty = new BigDecimal(detail.getQty());
		// bqItem.setQuantity(qty.toBigInteger());
		//
		// if (StringUtils.checkString(detail.getOrdrUom()).length() > 0) {
		// LOG.info("prToAuction deatils ErpPojo UOM :" + detail.getOrdrUom());
		// Uom uom = uomService.getUomByUomAndTenantId(detail.getOrdrUom(), newEvent.getTenantId());
		// if (uom == null) {
		// throw new ApplicationException("Invalid UOM : " + detail.getOrdrUom());
		// }
		// bqItem.setUom(uom);
		// } else {
		// throw new ApplicationException("Invalid UOM : " + detail.getOrdrUom());
		// }
		// // setting extra field
		// bqItem.setField1(detail.getField1());
		// bqItem.setField2(detail.getField2());
		// bqItem.setField3(detail.getField3());
		// bqItem.setField4(detail.getField4());
		// bqItem.setField5(detail.getField5());
		// bqItem.setField6(detail.getField6());
		// bqItem.setField7(detail.getField7());
		// bqItem.setField8(detail.getField8());
		// bqItem.setField9(detail.getField9());
		// bqItem.setField10(detail.getField10());
		//
		// bqItem.setParent(bqItemSection);
		// bqItem.setBq(dbBq);
		// bqItem.setRfxEvent(newEvent);
		// rfpBqItemDao.saveOrUpdate(bqItem);
		// if (StringUtils.checkString(delDateString).length() == 0) {
		// delDateString = detail.getField7();
		// }
		//
		// }
		// if (StringUtils.checkString(delDateString).length() > 0) {
		// SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		// Date deliveryDate = null;
		// try {
		// deliveryDate = sdf.parse(delDateString);
		// newEvent.setDeliveryDate(deliveryDate);
		// newEvent = rfpEventDao.saveOrUpdate(newEvent);
		// } catch (ParseException e) {
		// LOG.error("Error while parsing Delivery date :" + e.getMessage(), e);
		// }
		// }
		// }

		return newEvent;
	}

	@Override
	@Transactional(readOnly = false, rollbackFor = { Exception.class, ApplicationException.class })
	public RfqEvent copyFromRfqTemplateForErp(String templateId, User adminUser, PrToAuctionErpPojo prToAuctionErpPojo) throws Exception {
		RfqEvent newEvent = rfqEventService.copyFromTemplate(templateId, adminUser, null);
		newEvent = constructRfqEventDetials(prToAuctionErpPojo, newEvent);
		// rfqEventService.addAssociateOwners(adminUser, newEvent);
		return newEvent;
	}

	/**
	 * @param prToAuctionErpPojo
	 * @param newEvent
	 * @return
	 * @throws ApplicationException
	 */
	private RfqEvent constructRfqEventDetials(PrToAuctionErpPojo prToAuctionErpPojo, RfqEvent newEvent) throws ApplicationException {
		if (StringUtils.checkString(prToAuctionErpPojo.getPrNo()).length() > 0) {
			LOG.info("prToAuctionErpPojo PrNo :" + prToAuctionErpPojo.getPrNo());
			newEvent.setReferanceNumber(prToAuctionErpPojo.getPrNo());
			newEvent.setErpDocNo(prToAuctionErpPojo.getPrNo());
		}
		if (StringUtils.checkString(prToAuctionErpPojo.getCurr()).length() > 0) {
			LOG.info("prToAuctionErpPojo Currrency :" + prToAuctionErpPojo.getCurr());
			Currency baseCurrency = currencyDao.findByCurrencyCode(prToAuctionErpPojo.getCurr());
			newEvent.setBaseCurrency(baseCurrency);
		}
		if (StringUtils.checkString(prToAuctionErpPojo.getItmCat()).length() > 0) {
			LOG.info("prToAuctionErpPojo Itm Cat :" + prToAuctionErpPojo.getItmCat());
			// IndustryCategory ic = industryCategoryService.getIndustryCategoryByCode(prToAuctionErpPojo.getItmCat(),
			// newEvent.getTenantId());
			// newEvent.setIndustryCategories(Arrays.asList(ic));
		}

		newEvent = rfqEventDao.saveOrUpdate(newEvent);
		RfqEventBq bq = new RfqEventBq();
		bq.setName("Bill of Quantity");
		bq.setField1Label("Item Category");
		bq.setField1ToShowSupplier(Boolean.FALSE);
		bq.setField2Label("Bq Item Code");
		bq.setField2ToShowSupplier(Boolean.TRUE);
		bq.setField3Label("Material Group");
		bq.setField3ToShowSupplier(Boolean.FALSE);
		bq.setField4Label("Brand");
		bq.setField4ToShowSupplier(Boolean.TRUE);
		bq.setField5Label("MFR Part No");
		bq.setField5ToShowSupplier(Boolean.TRUE);
		bq.setField6Label(prToAuctionErpPojo.getField6Label());
		bq.setField6Label("Purchase Group");
		bq.setField7Label("Delivery Date");
		bq.setField7ToShowSupplier(Boolean.TRUE);
		bq.setField8Label("Item No");
		bq.setField8ToShowSupplier(Boolean.FALSE);
		bq.setField9Label(prToAuctionErpPojo.getField9Label());
		bq.setField9ToShowSupplier(Boolean.FALSE);
		bq.setField10Label(prToAuctionErpPojo.getField10Label());
		bq.setField10ToShowSupplier(Boolean.FALSE);

		if (CollectionUtil.isNotEmpty(prToAuctionErpPojo.getAuctionDetails())) {

			bq.setRfxEvent(newEvent);
			RfqEventBq dbBq = rfqEventBqDao.saveOrUpdate(bq);
			LOG.info("dbBq :" + dbBq.getName());
			int order = 0;
			int noOfSections = 1;
			int level = 1;
			String delDateString = "";

			Map<String, RfqBqItem> sections = new HashMap<String, RfqBqItem>();

			RfqBqItem defaultBqItemSection = null;
			RfqBqItem section = null;
			for (PrToAuctionDetailsErpPojo detail : prToAuctionErpPojo.getAuctionDetails()) {

				String sapItemNo = StringUtils.checkString(detail.getItmNo());
				if (sapItemNo.length() > 5) {
					sapItemNo = sapItemNo.substring(0, 5);
				}
				String sapItemName = detail.getMatDesc();
				BigDecimal sapQty = new BigDecimal(StringUtils.checkString(detail.getQty()).length() > 0 ? StringUtils.checkString(detail.getQty()) : "0");
				String sapUom = detail.getOrdrUom();
				level = 1;

				if (StringUtils.checkString(detail.getExtSubItm()).length() > 0) {
					section = sections.get(sapItemNo);
					if (section == null) {
						section = new RfqBqItem();
						section.setLevel(noOfSections++);
						section.setOrder(0);
						section.setItemName(sapItemName);
						section.setBq(dbBq);
						section.setRfxEvent(newEvent);
						section.setField8(StringUtils.checkString(detail.getItmNo())); // used for SAP item no
						section = rfqBqItemDao.saveOrUpdate(section);

						section.setField9(String.valueOf(0)); // used temporarily to get the next item order for
																// section
						sections.put(sapItemNo, section);
					}

					order = Integer.parseInt(section.getField9()) + 1;
					section.setField9(String.valueOf(order));
					sections.put(sapItemNo, section);

					// Extract the EXT info for the line item
					sapItemName = detail.getExtValItm(); // ex_svcDesc from SAP
					sapQty = new BigDecimal(StringUtils.checkString(detail.getExtSvcQty()).length() > 0 ? StringUtils.checkString(detail.getExtSvcQty()) : "0");
					sapUom = detail.getExtBaseUom();
					level = section.getLevel();
					sapItemNo = detail.getExtSubItm();
				} else {

					if (defaultBqItemSection == null) {
						defaultBqItemSection = new RfqBqItem();
						defaultBqItemSection.setLevel(noOfSections++);
						defaultBqItemSection.setOrder(0);
						defaultBqItemSection.setItemName("Item Section");
						defaultBqItemSection.setBq(dbBq);
						defaultBqItemSection.setRfxEvent(newEvent);
						defaultBqItemSection.setField8(StringUtils.checkString(detail.getItmNo()));
						defaultBqItemSection = rfqBqItemDao.saveOrUpdate(defaultBqItemSection);
						defaultBqItemSection.setField9(String.valueOf(0));
					}

					order = Integer.parseInt(defaultBqItemSection.getField9()) + 1;
					defaultBqItemSection.setField9(String.valueOf(order));
					level = defaultBqItemSection.getLevel();
					section = defaultBqItemSection;
				}

				// setting Item
				RfqBqItem bqItem = new RfqBqItem();
				bqItem.setLevel(level);
				bqItem.setOrder(order);
				bqItem.setItemName(sapItemName);
				bqItem.setQuantity(sapQty.toBigDecimal());
				bqItem.setPriceType(PricingTypes.NORMAL_PRICE);
				bqItem.setField8(StringUtils.checkString(detail.getItmNo())); // used for SAP item no

				if (StringUtils.checkString(sapUom).length() > 0) {
					LOG.info("prToAuction deatils ErpPojo UOM :" + sapUom);
					Uom uom = uomService.getUomByUomAndTenantId(sapUom, newEvent.getTenantId());
					if (uom == null) {
						throw new ApplicationException("Invalid UOM : " + sapUom);
					}
					bqItem.setUom(uom);
				} else {
					throw new ApplicationException("Invalid UOM : " + sapUom);
				}
				// setting extra field
				bqItem.setField1(StringUtils.checkString(detail.getField1()));
				bqItem.setField2(StringUtils.checkString(detail.getField2()));
				bqItem.setField3(StringUtils.checkString(detail.getField3()));
				bqItem.setField4(StringUtils.checkString(detail.getField4()));
				bqItem.setField5(StringUtils.checkString(detail.getField5()));
				bqItem.setField6(StringUtils.checkString(detail.getField6()));
				bqItem.setField7(StringUtils.checkString(detail.getField7()));
				bqItem.setField8(StringUtils.checkString(detail.getItmNo()));
				bqItem.setField9(StringUtils.checkString(detail.getField9()));
				bqItem.setField10(StringUtils.checkString(detail.getField10()));

				bqItem.setParent(section);
				bqItem.setBq(dbBq);
				bqItem.setRfxEvent(newEvent);
				rfqBqItemDao.saveOrUpdate(bqItem);

				if (StringUtils.checkString(delDateString).length() == 0) {
					delDateString = detail.getField7();
				}

			}
			if (StringUtils.checkString(delDateString).length() > 0) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				Date deliveryDate = null;
				try {
					deliveryDate = sdf.parse(delDateString);
					newEvent.setDeliveryDate(deliveryDate);
					newEvent = rfqEventDao.saveOrUpdate(newEvent);
				} catch (ParseException e) {
					LOG.error("Error while parsing Delivery date :" + e.getMessage(), e);
				}
			}

		}
		// bq.setRfxEvent(newEvent);
		// RfqEventBq dbBq = rfqEventBqDao.saveOrUpdate(bq);
		// LOG.info("dbBq :" + dbBq.getName());
		// int order = 0;
		// // setting Section for each Item
		// RfqBqItem bqItemSection = new RfqBqItem();
		// bqItemSection.setLevel(1);
		// bqItemSection.setOrder(order++);
		// bqItemSection.setItemName("Item Section");
		// bqItemSection.setBq(dbBq);
		// bqItemSection.setRfxEvent(newEvent);
		// bqItemSection = rfqBqItemDao.saveOrUpdate(bqItemSection);
		// String delDateString = "";
		// for (PrToAuctionDetailsErpPojo detail : prToAuctionErpPojo.getAuctionDetails()) {
		// // setting Item
		// RfqBqItem bqItem = new RfqBqItem();
		// bqItem.setLevel(1);
		// bqItem.setOrder(order++);
		// bqItem.setItemName(detail.getMatDesc());
		// bqItem.setPriceType(PricingTypes.NORMAL_PRICE);
		//
		// BigDecimal qty = new BigDecimal(detail.getQty());
		// bqItem.setQuantity(qty.toBigInteger());
		//
		// if (StringUtils.checkString(detail.getOrdrUom()).length() > 0) {
		// LOG.info("prToAuction deatils ErpPojo UOM :" + detail.getOrdrUom());
		// Uom uom = uomService.getUomByUomAndTenantId(detail.getOrdrUom(), newEvent.getTenantId());
		// if (uom == null) {
		// throw new ApplicationException("Invalid UOM : " + detail.getOrdrUom());
		// }
		// bqItem.setUom(uom);
		// } else {
		// throw new ApplicationException("No UOM provided.");
		// }
		// // setting extra field
		// bqItem.setField1(detail.getField1());
		// bqItem.setField2(detail.getField2());
		// bqItem.setField3(detail.getField3());
		// bqItem.setField4(detail.getField4());
		// bqItem.setField5(detail.getField5());
		// bqItem.setField6(detail.getField6());
		// bqItem.setField7(detail.getField7());
		// bqItem.setField8(detail.getField8());
		// bqItem.setField9(detail.getField9());
		// bqItem.setField10(detail.getField10());
		//
		// bqItem.setParent(bqItemSection);
		// bqItem.setBq(dbBq);
		// bqItem.setRfxEvent(newEvent);
		// rfqBqItemDao.saveOrUpdate(bqItem);
		// if (StringUtils.checkString(delDateString).length() == 0) {
		// delDateString = detail.getField7();
		// }
		//
		// }
		// if (StringUtils.checkString(delDateString).length() > 0) {
		// SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		// Date deliveryDate = null;
		// try {
		// deliveryDate = sdf.parse(delDateString);
		// newEvent.setDeliveryDate(deliveryDate);
		// newEvent = rfqEventDao.saveOrUpdate(newEvent);
		// } catch (ParseException e) {
		// LOG.error("Error while parsing Delivery date :" + e.getMessage(), e);
		// }
		// }
		// }
		return newEvent;
	}

	@Override
	@Transactional(readOnly = false, rollbackFor = { Exception.class, ApplicationException.class })
	public RfaEvent copyFromRfaTemplateForErp(String templateId, User createdBy, PrToAuctionErpPojo prToAuctionErpPojo) throws Exception {
		RfaEvent newEvent = rfaEventService.copyFromTemplate(templateId, createdBy, null);
		newEvent = constructRfaEventDetials(prToAuctionErpPojo, newEvent);
		// rfaEventService.addAssociateOwners(createdBy, newEvent);
		return newEvent;
	}

	/**
	 * @param prToAuctionErpPojo
	 * @param newEvent
	 * @return
	 * @throws ApplicationException
	 */
	private RfaEvent constructRfaEventDetials(PrToAuctionErpPojo prToAuctionErpPojo, RfaEvent newEvent) throws ApplicationException {
		if (StringUtils.checkString(prToAuctionErpPojo.getPrNo()).length() > 0) {
			LOG.info("prToAuctionErpPojo PrNo :" + prToAuctionErpPojo.getPrNo());
			newEvent.setReferanceNumber(prToAuctionErpPojo.getPrNo());
			newEvent.setErpDocNo(prToAuctionErpPojo.getPrNo());
		}
		if (StringUtils.checkString(prToAuctionErpPojo.getCurr()).length() > 0) {
			LOG.info("prToAuctionErpPojo Currrency :" + prToAuctionErpPojo.getCurr());
			Currency baseCurrency = currencyDao.findByCurrencyCode(prToAuctionErpPojo.getCurr());
			newEvent.setBaseCurrency(baseCurrency);
		}
		if (StringUtils.checkString(prToAuctionErpPojo.getItmCat()).length() > 0) {
			LOG.info("prToAuctionErpPojo Itm Cat :" + prToAuctionErpPojo.getItmCat() + "==== newEvent.getTenantId() :" + newEvent.getTenantId());
			// IndustryCategory ic = industryCategoryService.getIndustryCategoryByCode(prToAuctionErpPojo.getItmCat(),
			// newEvent.getTenantId());
			// if (ic != null) {
			// LOG.info("Industry Cat :" + ic.getCode());
			// newEvent.setIndustryCategories(new ArrayList<IndustryCategory>());
			// newEvent.getIndustryCategories().add(ic);
			// }else{
			// LOG.info("Industry Cat is null");
			// }
		}

		newEvent = rfaEventDao.saveOrUpdate(newEvent);

		RfaEventBq bq = new RfaEventBq();
		bq.setName("Bill of Quantity");
		bq.setName("Bill of Quantity");
		bq.setField1Label("Item Category");
		bq.setField1ToShowSupplier(Boolean.FALSE);
		bq.setField2Label("Bq Item Code");
		bq.setField2ToShowSupplier(Boolean.TRUE);
		bq.setField3Label("Material Group");
		bq.setField3ToShowSupplier(Boolean.FALSE);
		bq.setField4Label("Brand");
		bq.setField4ToShowSupplier(Boolean.TRUE);
		bq.setField5Label("MFR Part No");
		bq.setField5ToShowSupplier(Boolean.TRUE);
		bq.setField6Label(prToAuctionErpPojo.getField6Label());
		bq.setField6Label("Purchase Group");
		bq.setField7Label("Delivery Date");
		bq.setField7ToShowSupplier(Boolean.TRUE);
		bq.setField8Label("Item No");
		bq.setField8ToShowSupplier(Boolean.FALSE);
		bq.setField9Label(prToAuctionErpPojo.getField9Label());
		bq.setField9ToShowSupplier(Boolean.FALSE);
		bq.setField10Label(prToAuctionErpPojo.getField10Label());
		bq.setField10ToShowSupplier(Boolean.FALSE);

		if (CollectionUtil.isNotEmpty(prToAuctionErpPojo.getAuctionDetails())) {
			bq.setRfxEvent(newEvent);
			RfaEventBq dbBq = rfaEventBqDao.saveOrUpdate(bq);
			LOG.info("dbBq :" + dbBq.getName());
			int order = 0;
			int noOfSections = 1;
			int level = 1;
			String delDateString = "";

			Map<String, RfaBqItem> sections = new HashMap<String, RfaBqItem>();

			RfaBqItem defaultBqItemSection = null;
			RfaBqItem section = null;
			for (PrToAuctionDetailsErpPojo detail : prToAuctionErpPojo.getAuctionDetails()) {

				String sapItemNo = StringUtils.checkString(detail.getItmNo());
				if (sapItemNo.length() > 5) {
					sapItemNo = sapItemNo.substring(0, 5);
				}
				String sapItemName = detail.getMatDesc();
				BigDecimal sapQty = new BigDecimal(StringUtils.checkString(detail.getQty()).length() > 0 ? StringUtils.checkString(detail.getQty()) : "0");
				String sapUom = detail.getOrdrUom();
				level = 1;

				if (StringUtils.checkString(detail.getExtSubItm()).length() > 0) {
					section = sections.get(sapItemNo);
					if (section == null) {
						section = new RfaBqItem();
						section.setLevel(noOfSections++);
						section.setOrder(0);
						section.setItemName(sapItemName);
						section.setBq(dbBq);
						section.setRfxEvent(newEvent);
						section.setField8(StringUtils.checkString(detail.getItmNo())); // used for SAP item no
						section = rfaBqItemDao.saveOrUpdate(section);

						section.setField9(String.valueOf(0)); // used temporarily to get the next item order for
																// section
						sections.put(sapItemNo, section);
					}

					order = Integer.parseInt(section.getField9()) + 1;
					section.setField9(String.valueOf(order));
					sections.put(sapItemNo, section);

					// Extract the EXT info for the line item
					sapItemName = detail.getExtValItm(); // ex_svcDesc from SAP
					sapQty = new BigDecimal(StringUtils.checkString(detail.getExtSvcQty()).length() > 0 ? StringUtils.checkString(detail.getExtSvcQty()) : "0");
					sapUom = detail.getExtBaseUom();
					level = section.getLevel();
					sapItemNo = detail.getExtSubItm();
				} else {

					if (defaultBqItemSection == null) {
						defaultBqItemSection = new RfaBqItem();
						defaultBqItemSection.setLevel(noOfSections++);
						defaultBqItemSection.setOrder(0);
						defaultBqItemSection.setItemName("Item Section");
						defaultBqItemSection.setBq(dbBq);
						defaultBqItemSection.setRfxEvent(newEvent);
						defaultBqItemSection.setField8(StringUtils.checkString(detail.getItmNo()));
						defaultBqItemSection = rfaBqItemDao.saveOrUpdate(defaultBqItemSection);
						defaultBqItemSection.setField9(String.valueOf(0));
					}

					order = Integer.parseInt(defaultBqItemSection.getField9()) + 1;
					defaultBqItemSection.setField9(String.valueOf(order));
					level = defaultBqItemSection.getLevel();
					section = defaultBqItemSection;
				}

				// setting Item
				RfaBqItem bqItem = new RfaBqItem();
				bqItem.setLevel(level);
				bqItem.setOrder(order);
				bqItem.setItemName(sapItemName);
				bqItem.setQuantity(sapQty.toBigDecimal());
				bqItem.setPriceType(PricingTypes.NORMAL_PRICE);
				bqItem.setField8(StringUtils.checkString(detail.getItmNo())); // used for SAP item no

				if (StringUtils.checkString(sapUom).length() > 0) {
					LOG.info("prToAuction deatils ErpPojo UOM :" + sapUom);
					Uom uom = uomService.getUomByUomAndTenantId(sapUom, newEvent.getTenantId());
					if (uom == null) {
						throw new ApplicationException("Invalid UOM : " + sapUom);
					}
					bqItem.setUom(uom);
				} else {
					throw new ApplicationException("Invalid UOM : " + sapUom);
				}
				// setting extra field
				bqItem.setField1(StringUtils.checkString(detail.getField1()));
				bqItem.setField2(StringUtils.checkString(detail.getField2()));
				bqItem.setField3(StringUtils.checkString(detail.getField3()));
				bqItem.setField4(StringUtils.checkString(detail.getField4()));
				bqItem.setField5(StringUtils.checkString(detail.getField5()));
				bqItem.setField6(StringUtils.checkString(detail.getField6()));
				bqItem.setField7(StringUtils.checkString(detail.getField7()));
				bqItem.setField8(StringUtils.checkString(detail.getItmNo()));
				bqItem.setField9(StringUtils.checkString(detail.getField9()));
				bqItem.setField10(StringUtils.checkString(detail.getField10()));

				bqItem.setParent(section);
				bqItem.setBq(dbBq);
				bqItem.setRfxEvent(newEvent);
				rfaBqItemDao.saveOrUpdate(bqItem);

				if (StringUtils.checkString(delDateString).length() == 0) {
					delDateString = detail.getField7();
				}

			}
			if (StringUtils.checkString(delDateString).length() > 0) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				Date deliveryDate = null;
				try {
					deliveryDate = sdf.parse(delDateString);
					newEvent.setDeliveryDate(deliveryDate);
					newEvent = rfaEventDao.saveOrUpdate(newEvent);
				} catch (ParseException e) {
					LOG.error("Error while parsing Delivery date :" + e.getMessage(), e);
				}
			}
		}
		return newEvent;
	}

	@Override
	@Transactional(readOnly = false)
	public AwardResponsePojo sendAwardPage(List<AwardErpPojo> awardErpPojoList, String eventId, RfxTypes eventType) throws Exception {
		ErpSetup erpConfig = erpConfigDao.getErpConfigBytenantId(SecurityLibrary.getLoggedInUser().getTenantId());
		String erpSeqNo = erpSetupService.genrateSquanceNumber();
		AwardResponsePojo resPojo = new AwardResponsePojo();
		if (erpConfig != null) {
			LOG.info("erpConfig erp enable :" + erpConfig.getIsErpEnable());

			if (Boolean.TRUE == erpConfig.getIsErpEnable()) {
				try {
					ObjectMapper mapperObj = new ObjectMapper();

					LOG.info("================================================------>" + awardErpPojoList.size());

					if (!erpConfig.getAwardInterfaceTypePull()) {
						for (AwardErpPojo awardErpPojo : awardErpPojoList) {
							awardErpPojo.setErpSeqNo(erpSeqNo);
						}
						HttpHeaders headers = new HttpHeaders();

						headers.set(Global.X_AUTH_KEY_HEADER_PROPERTY, erpConfig.getAppId());

						HttpEntity<List<AwardErpPojo>> request = new HttpEntity<List<AwardErpPojo>>(awardErpPojoList, headers);

						String responseMsg = restTemplate.postForObject(erpConfig.getErpUrl() + "/AwardSendData", request, String.class);

						// String responseMsg = restTemplate.postForObject(erpConfig.getErpUrl() + "/AwardSendData",
						// awardErpPojoList, String.class);
						LOG.info("response :" + responseMsg);

						resPojo = mapperObj.readValue(responseMsg, AwardResponsePojo.class);
					} else {

						TransferAwardPojo transferAwardPojo = new TransferAwardPojo();
						List<TransferAwardBqPojo> awards = new ArrayList<TransferAwardBqPojo>();
						for (AwardErpPojo awardErpPojo : awardErpPojoList) {
							transferAwardPojo.setCurrencyCode(awardErpPojo.getCurrencyCode());
							transferAwardPojo.setEventId(awardErpPojo.getEventId());
							transferAwardPojo.setEventName(awardErpPojo.getEventName());
							transferAwardPojo.setEventOwner(awardErpPojo.getEventOwner());
							transferAwardPojo.setAwardRemark(awardErpPojo.getAwardRemark());
							transferAwardPojo.setBusinessUnitName(awardErpPojo.getBusinessUnitName());
							transferAwardPojo.setDeliveryAddress(awardErpPojo.getDeliveryAddress());
							transferAwardPojo.setEventReferenceNumber(awardErpPojo.getReferenceNumber());
							transferAwardPojo.setCreatedDate(awardErpPojo.getCreatedDate());
							transferAwardPojo.setStartDate(awardErpPojo.getStartDate());
							transferAwardPojo.setEndDate(awardErpPojo.getEndDate());
							transferAwardPojo.setValidityDays(awardErpPojo.getValidityDays());
							transferAwardPojo.setDeliveryDate(awardErpPojo.getDeliveryDate());
							transferAwardPojo.setPaymentTerm(awardErpPojo.getPaymentTerm());
							transferAwardPojo.setErpSeqNo(awardErpPojo.getErpSeqNo());
							TransferAwardBqPojo bq = new TransferAwardBqPojo();
							bq.setBqName(awardErpPojo.getBqName());
							bq.setBqItems(awardErpPojo.getBqItems());
							awards.add(bq);
						}
						transferAwardPojo.setBqList(awards);
						String payload = mapperObj.writeValueAsString(transferAwardPojo);
						LOG.info("jsonObject  :" + payload);
						ErpAwardStaging dBstagging = erpAwardStaggingService.findAwardStaggingByEventID(transferAwardPojo.getEventId(), SecurityLibrary.getLoggedInUserTenantId());
						if (dBstagging != null) {
							dBstagging.setPayload(payload);
							dBstagging.setSentFlag(Boolean.FALSE);
							dBstagging.setActionBy(SecurityLibrary.getLoggedInUser());
							dBstagging.setActionDate(new Date());
							erpAwardStaggingService.update(dBstagging);
						} else {
							ErpAwardStaging stagging = new ErpAwardStaging();
							stagging.setEventType(eventType);
							stagging.setPayload(payload);
							stagging.setDocNo(transferAwardPojo.getEventId());
							stagging.setTenantId(SecurityLibrary.getLoggedInUserTenantId());
							stagging.setActionBy(SecurityLibrary.getLoggedInUser());
							stagging.setActionDate(new Date());
							stagging.setSentFlag(Boolean.FALSE);
							stagging = erpAwardStaggingService.saveErpAwardStaging(stagging);
						}

					}
					if (resPojo != null && StringUtils.checkString(resPojo.getError()).length() > 0) {
						throw new Exception(resPojo.getError());
					}

					switch (eventType) {
					case RFA:
						RfaEvent rfaEvent = rfaEventDao.findByEventId(eventId);
						if (rfaEvent != null) {
							String seprator = ",";
							List<String> refNoList = new ArrayList<String>();
							if (CollectionUtil.isNotEmpty(resPojo.getRefNumList())) {
								for (AwardReferenceNumberPojo awardRefPojo : resPojo.getRefNumList()) {
									refNoList.add(awardRefPojo.getReferenceNumber());

								}
							}
							rfaEvent.setErpAwardRefId(String.join(seprator, refNoList));
						}
						break;
					case RFI:
						break;
					case RFP:
						RfpEvent rfpEvent = rfpEventDao.findByEventId(eventId);
						if (rfpEvent != null) {
							String seprator = " , ";
							List<String> refNoList = new ArrayList<String>();
							if (CollectionUtil.isNotEmpty(resPojo.getRefNumList())) {
								for (AwardReferenceNumberPojo awardRefPojo : resPojo.getRefNumList()) {
									refNoList.add(awardRefPojo.getReferenceNumber());
								}
							}
							rfpEvent.setErpAwardRefId(String.join(seprator, refNoList));
						}
						break;
					case RFQ:
						RfqEvent rfqEvent = rfqEventDao.findByEventId(eventId);
						if (rfqEvent != null) {
							String seprator = " , ";
							List<String> refNoList = new ArrayList<String>();
							if (CollectionUtil.isNotEmpty(resPojo.getRefNumList())) {
								for (AwardReferenceNumberPojo awardRefPojo : resPojo.getRefNumList()) {
									refNoList.add(awardRefPojo.getReferenceNumber());
								}
							}
							rfqEvent.setErpAwardRefId(String.join(seprator, refNoList));
						}
						break;
					case RFT:
						RftEvent rftEvent = rftEventDao.findByEventId(eventId);
						if (rftEvent != null) {
							String seprator1 = " , ";
							List<String> refNoList1 = new ArrayList<String>();
							if (CollectionUtil.isNotEmpty(resPojo.getRefNumList())) {
								for (AwardReferenceNumberPojo awardRefPojo : resPojo.getRefNumList()) {
									refNoList1.add(awardRefPojo.getReferenceNumber());
								}
							}
							rftEvent.setErpAwardRefId(String.join(seprator1, refNoList1));
						}
						break;
					default:
						break;

					}

					/*
					 * responseMap = mapperObj.readValue(responseMsg, Map.class);
					 * LOG.info(responseMap.get("refNumList")); for (Map.Entry<String, String> entry :
					 * responseMap.entrySet()) { String key = entry.getKey(); //String value = entry.getValue();
					 * LOG.info("key : "+key); //LOG.info("value : "+value); // now work with key and value... } //
					 * responseMap1 = mapperObj.readValue(responseMsg, Map.class); // //
					 * LOG.info(responseMap1.get("refNumList")); // ArrayList<JsonObject> msg = (ArrayList<JsonObject>)
					 * responseMap1.get("refNumList"); // Iterator<JsonObject> iterator = msg.iterator(); //
					 * LOG.info("**************GGGGGGGGG"); // while (iterator.hasNext()) { //
					 * LOG.info(iterator.next()); //// String s=iterator.next(); //// LOG.info(s); //// String json =
					 * "{ \"color\" : \"Black\", \"type\" : \"BMW\" }"; //// Car car = objectMapper.readValue(json,
					 * Car.class); // }
					 */
					// if (responseMap != null)
					// LOG.info("response :------------------" + responseMap.toString());
					// if (responseMap.containsKey("error")) {
					// throw new Exception(responseMap.get("error"));
					// }
				} catch (Exception e) {
					LOG.error("Error while sending Award page to ERP :" + e.getMessage(), e);
					throw e;
				}
			}

		}
		return resPojo;
	}

	@Override
	@Transactional(readOnly = false)
	public void transferPrToErp(String prId) throws Exception {

		try {

			ClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());
			restTemplate = new RestTemplate(factory);
			restTemplate.setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
			restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());

			ErpSetup erpConfig = erpConfigDao.getErpConfigBytenantId(SecurityLibrary.getLoggedInUser().getTenantId());
			ErpIntegrationTypeForPr type = erpConfig.getErpIntegrationTypeForPr();
			HttpHeaders headers = new HttpHeaders();

			LOG.info("erpConfig erp enable :" + erpConfig.getIsErpEnable());
			if (erpConfig != null && Boolean.TRUE == erpConfig.getIsErpEnable()) {
				// sending PR to ERP
				LOG.info("ERP Integration is enabled.... sending PR to ERP...");

				Pr pr = prService.getPrForErpById(prId);
				ObjectMapper mapperObj = new ObjectMapper();
				String payload = "";

				String responseMsg = "";
				if (type == null || ErpIntegrationTypeForPr.TYPE_1 == type) {
					PrErpPojo prErpPojo = new PrErpPojo(pr);
					String erpSeqNo = erpSetupService.genrateSquanceNumber();
					prErpPojo.setErpSeqNo(erpSeqNo);
					payload = mapperObj.writeValueAsString(prErpPojo);
					LOG.info("jsonObject  :" + payload);

					if (StringUtils.checkString(prErpPojo.getVendorCode()).length() == 0 && pr.getSupplier() != null) {
						throw new ApplicationException("Vendor Code not Assigned to Supplier");
					} else {
						LOG.info("not empty vendor code ");
					}

					headers.set(Global.X_AUTH_KEY_HEADER_PROPERTY, erpConfig.getAppId());
					HttpEntity<PrErpPojo> request = new HttpEntity<PrErpPojo>(prErpPojo, headers);
					try {
						responseMsg = restTemplate.postForObject(erpConfig.getErpUrl() + "/PrSendData", request, String.class);
					} catch (HttpClientErrorException | HttpServerErrorException ex) {
						responseMsg = ex.getMessage();
						LOG.error("Error received from ERP : " + ex.getMessage() + " Status Text : " + ex.getStatusText());
						LOG.error("Response Body : " + ex.getResponseBodyAsString());
						throw new ApplicationException("Error received from ERP : " + ex.getMessage() + " Status Text : " + ex.getStatusText(), ex);
					}

				} else if (ErpIntegrationTypeForPr.TYPE_2 == type) {

					// Fetch the contract reference numbers and storage location...
					if (pr.getTemplate() != null && Boolean.TRUE == pr.getTemplate().getContractItemsOnly()) {
						for (PrItem item : pr.getPrItems()) {
							if (item.getProduct() != null) {
								ProductContractItems pcItem = productContractItemsDao.findProductContractItemByItemId(item.getProduct().getId(), item.getProductContractItem() != null ? item.getProductContractItem().getId() : null);
								if (pcItem != null) {
									item.setStorageLocation(pcItem.getStorageLocation());
									item.setContractReferenceNumber(pcItem.getProductContract().getContractReferenceNumber());
									item.setItemContractReferenceNumber(pcItem.getContractItemNumber());
									item.setCostCenter(pcItem.getCostCenter() != null ? pcItem.getCostCenter().getCostCenter() : null);
									item.setPurchaseGroup(pcItem.getProductContract().getGroupCode());
								}
							}
						}
					}

					if (StringUtils.checkString(erpConfig.getErpUsername()).length() > 0 && StringUtils.checkString(erpConfig.getErpPassword()).length() > 0) {
						String auth = StringUtils.checkString(erpConfig.getErpUsername()) + ":" + StringUtils.checkString(erpConfig.getErpPassword());
						byte[] encodedAuth = Base64.encode(auth.getBytes(Charset.forName("US-ASCII")));
						String authHeader = "Basic " + new String(encodedAuth);
						headers.set("Authorization", authHeader);
					}

					// for Type 2
					PrErp2Pojo prErp2Pojo = new PrErp2Pojo(pr);
					payload = mapperObj.writeValueAsString(prErp2Pojo);
					LOG.info("jsonObject  :" + payload);

					HttpEntity<PrErp2Pojo> request = new HttpEntity<PrErp2Pojo>(prErp2Pojo, headers);
					try {
						responseMsg = restTemplate.postForObject(erpConfig.getErpUrl() + "/PRCreate/", request, String.class);
					} catch (HttpClientErrorException | HttpServerErrorException ex) {
						responseMsg = ex.getMessage();
						LOG.error("Error received from ERP : " + ex.getMessage() + " Status Text : " + ex.getStatusText());
						LOG.error("Response Body : " + ex.getResponseBodyAsString());
						throw new ApplicationException("Error received from ERP : " + ex.getMessage() + " Status Text : " + ex.getStatusText(), ex);
					}
				}

				LOG.info("response :" + responseMsg);

				// String responseMsg = restTemplate.postForObject(erpConfig.getErpUrl() + "/PrSendData", prErpPojo,
				// String.class);
				// updating PR STATUS
				pr.setStatus(PrStatus.TRANSFERRED);
				pr.setErpPrTransferred(Boolean.TRUE);
				prService.updatePr(pr);
				// Storing audit history
				try {
					PrAudit audit = new PrAudit();
					audit.setAction(PrAuditType.TRANSFERRED);
					audit.setActionBy(SecurityLibrary.getLoggedInUser());
					audit.setActionDate(new Date());
					audit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
					audit.setDescription("Sucessfully transferred to ERP. Response : " + StringUtils.checkString(responseMsg));
					audit.setPr(pr);
					prAuditService.save(audit);
				} catch (Exception e) {
					LOG.error("Error while saving ERP Audit History :" + e.getMessage(), e);
					throw e;
				}
			}
		} catch (Exception e) {
			// Storing audit history for error
			try {
				Pr pr = prService.getPrForErpById(prId);
				PrAudit audit = new PrAudit();
				audit.setAction(PrAuditType.ERROR);
				audit.setActionDate(new Date());
				audit.setActionBy(SecurityLibrary.getLoggedInUser());
				audit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
				audit.setDescription(e.getMessage());
				audit.setPr(pr);
				prAuditService.save(audit);
			} catch (Exception error) {
				LOG.error("Error while saving ERP Audit History in catch block :" + error.getMessage(), error);
			}
			LOG.error("Error while transfer pr to erp :" + e.getMessage(), e);
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, noRollbackFor = Exception.class)
	public void transferRfsToErp(String rfsId, ErpSetup erpSetup) throws Exception {

		SourcingFormRequest sourcingFormRequest = requestService.getSourcingRequestById(rfsId);
		try {

			ClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());
			restTemplate = new RestTemplate(factory);
			restTemplate.setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
			restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());

			ErpIntegrationTypeForPr type = erpSetup.getErpIntegrationTypeForPr();
			HttpHeaders headers = new HttpHeaders();

			LOG.info("erpConfig erp enable :" + erpSetup.getIsErpEnable());
			if (erpSetup != null && Boolean.TRUE == erpSetup.getIsErpEnable() && Boolean.TRUE == erpSetup.getEnableRfsErpPush()) {
				// sending PR to ERP
				LOG.info("ERP Integration is enabled.... sending PR to ERP...");

				ObjectMapper mapperObj = new ObjectMapper();
				String payload = "";

				String responseMsg = "";
				if (ErpIntegrationTypeForPr.TYPE_2 == type) {
					// Fetch the contract reference numbers and storage location...
					/*
					 * if (pr.getTemplate() != null && Boolean.TRUE == pr.getTemplate().getContractItemsOnly()) { for
					 * (PrItem item : pr.getPrItems()) { if (item.getProduct() != null) { ProductContractItems pcItem =
					 * productContractItemsDao.findProductContractItemByItemId(item.getProduct().getId()); if (pcItem !=
					 * null) { item.getProduct().setStorageLocation(pcItem.getStorageLocation());
					 * item.getProduct().setContractReferenceNumber
					 * (pcItem.getProductContract().getContractReferenceNumber());
					 * item.getProduct().setItemContractReferenceNumber(pcItem.getContractItemNumber());
					 * item.setCostCenter(pcItem.getCostCenter() != null ? pcItem.getCostCenter().getCostCenter() :
					 * null); item.setPurchaseGroup(pcItem.getProductContract().getGroupCode()); } } } }
					 */

					if (StringUtils.checkString(erpSetup.getErpUsername()).length() > 0 && StringUtils.checkString(erpSetup.getErpPassword()).length() > 0) {
						String auth = StringUtils.checkString(erpSetup.getErpUsername()) + ":" + StringUtils.checkString(erpSetup.getErpPassword());
						byte[] encodedAuth = Base64.encode(auth.getBytes(Charset.forName("US-ASCII")));
						String authHeader = "Basic " + new String(encodedAuth);
						headers.set("Authorization", authHeader);
					}

					if (CollectionUtil.isNotEmpty(sourcingFormRequest.getSourcingRequestBqs()) && sourcingFormRequest.getSourcingRequestBqs().size() > 1) {
						throw new ApplicationException("Multiple BQs found. Cannot send request to ERP");
					}

					// for Type 2

					PrErp2Pojo prErp2Pojo = new PrErp2Pojo(sourcingFormRequest);
					// Checking for interface code if exisit send interface code to erp insted of product code
					for (PrItemErp2Pojo item : prErp2Pojo.getPrItems()) {
						ProductItem productItem = productListMaintenanceDao.findProductItemByCode(item.getItemCode(), erpSetup.getTenantId(), item.getProductItemType());
						if (productItem != null && StringUtils.checkString(productItem.getInterfaceCode()).length() > 0) {
							item.setItemCode(productItem.getInterfaceCode());
						}
					}

					payload = mapperObj.writeValueAsString(prErp2Pojo);
					LOG.info("jsonObject  :" + payload);

					HttpEntity<PrErp2Pojo> request = new HttpEntity<PrErp2Pojo>(prErp2Pojo, headers);

					if (CollectionUtil.isEmpty(prErp2Pojo.getPrItems())) {
						LOG.warn("No Line Items found for this sourcing form. Not going to send it to ERP : " + sourcingFormRequest.getId());
						return;
					}

					try {
						responseMsg = restTemplate.postForObject(erpSetup.getErpUrl() + "/PRCreate/", request, String.class);
					} catch (HttpClientErrorException | HttpServerErrorException ex) {
						responseMsg = ex.getMessage();
						LOG.error("Error received from ERP : " + ex.getMessage() + " Status Text : " + ex.getStatusText());
						LOG.error("Response Body : " + ex.getResponseBodyAsString());
						throw new ApplicationException("Error received from ERP : " + ex.getMessage() + " Status Text : " + ex.getStatusText(), ex);
					}
				}

				LOG.info("response :" + responseMsg);

				// updating STATUS
				sourcingFormRequest.setErpTransferred(Boolean.TRUE);
				sourcingFormRequest.setStatus(SourcingFormStatus.PENDING);
				if (sourcingFormRequest.getSubmittedDate() == null) {
					sourcingFormRequest.setSubmittedDate(new Date());
				}
				requestService.update(sourcingFormRequest);

				// Storing audit history
				try {
					RequestAudit audit = new RequestAudit();
					audit.setAction(RequestAuditType.ERP);
					audit.setActionBy(SecurityLibrary.getLoggedInUser());
					audit.setActionDate(new Date());
					audit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
					audit.setDescription("Request sent to ERP. Response : " + StringUtils.checkString(responseMsg));
					audit.setReq(sourcingFormRequest);
					requestAuditDao.save(audit);
				} catch (Exception e) {
					LOG.error("Error while saving Sourcing ERP Audit History :" + e.getMessage(), e);
					throw e;
				}
			}
		} catch (Exception e) {
			// Storing audit history for error
			try {
				RequestAudit audit = new RequestAudit();
				audit.setAction(RequestAuditType.ERROR);
				audit.setActionBy(SecurityLibrary.getLoggedInUser());
				audit.setActionDate(new Date());
				audit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
				audit.setDescription(e.getMessage());
				audit.setReq(sourcingFormRequest);
				requestAuditDao.save(audit);
			} catch (Exception error) {
				LOG.error("Error while saving Sourcing ERP Audit History in catch block :" + error.getMessage(), error);
			}
			LOG.error("Error while transfer pr to erp :" + e.getMessage(), e);
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false)
	public void transferRejectRfsToErp(String rfsId, ErpSetup erpSetup) throws Exception {

		SourcingFormRequest sourcingFormRequest = requestService.getSourcingRequestById(rfsId);
		try {

			ClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());
			restTemplate = new RestTemplate(factory);
			restTemplate.setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
			restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());

			ErpIntegrationTypeForPr type = erpSetup.getErpIntegrationTypeForPr();
			HttpHeaders headers = new HttpHeaders();

			LOG.info("erpConfig erp enable :" + erpSetup.getIsErpEnable());
			if (erpSetup != null && Boolean.TRUE == erpSetup.getIsErpEnable() && Boolean.TRUE == erpSetup.getEnableRfsErpPush()) {
				// sending PR to ERP
				LOG.info("ERP Integration is enabled.... sending PR to ERP...");

				ObjectMapper mapperObj = new ObjectMapper();
				String payload = "";

				String responseMsg = "";
				if (ErpIntegrationTypeForPr.TYPE_2 == type) {
					// Fetch the contract reference numbers and storage location...
					/*
					 * if (pr.getTemplate() != null && Boolean.TRUE == pr.getTemplate().getContractItemsOnly()) { for
					 * (PrItem item : pr.getPrItems()) { if (item.getProduct() != null) { ProductContractItems pcItem =
					 * productContractItemsDao.findProductContractItemByItemId(item.getProduct().getId()); if (pcItem !=
					 * null) { item.getProduct().setStorageLocation(pcItem.getStorageLocation());
					 * item.getProduct().setContractReferenceNumber
					 * (pcItem.getProductContract().getContractReferenceNumber());
					 * item.getProduct().setItemContractReferenceNumber(pcItem.getContractItemNumber());
					 * item.setCostCenter(pcItem.getCostCenter() != null ? pcItem.getCostCenter().getCostCenter() :
					 * null); item.setPurchaseGroup(pcItem.getProductContract().getGroupCode()); } } } }
					 */

					if (StringUtils.checkString(erpSetup.getErpUsername()).length() > 0 && StringUtils.checkString(erpSetup.getErpPassword()).length() > 0) {
						String auth = StringUtils.checkString(erpSetup.getErpUsername()) + ":" + StringUtils.checkString(erpSetup.getErpPassword());
						byte[] encodedAuth = Base64.encode(auth.getBytes(Charset.forName("US-ASCII")));
						String authHeader = "Basic " + new String(encodedAuth);
						headers.set("Authorization", authHeader);
					}

					if (CollectionUtil.isNotEmpty(sourcingFormRequest.getSourcingRequestBqs()) && sourcingFormRequest.getSourcingRequestBqs().size() > 1) {
						throw new ApplicationException("Multiple BQs found. Cannot send request to ERP");
					}

					// Check if ERP Doc No exists. If not, then probably this RFS is not sent for approval to ERP.
					// Therefore no need to send cancel request to ERP.
					if (StringUtils.checkString(sourcingFormRequest.getErpDocNo()).length() == 0) {
						LOG.warn("No Erp Doc No found for this sourcing form. Not going to send it to ERP : " + sourcingFormRequest.getId());
						return;
					}

					// for Type 2
					PrErp2DeletePojo prErp2Pojo = new PrErp2DeletePojo(sourcingFormRequest);

					// Checking for interface code if exisit send interface code to erp insted of product code
					for (PrItemErp2DeletePojo item : prErp2Pojo.getPrItems()) {
						ProductItem productItem = productListMaintenanceDao.findProductItemByCode(item.getItemCode(), erpSetup.getTenantId(), null);
						if (productItem != null && StringUtils.checkString(productItem.getInterfaceCode()).length() > 0) {
							item.setItemCode(productItem.getInterfaceCode());
						}
					}

					payload = mapperObj.writeValueAsString(prErp2Pojo);
					LOG.info("jsonObject  :" + payload);

					HttpEntity<PrErp2DeletePojo> request = new HttpEntity<PrErp2DeletePojo>(prErp2Pojo, headers);

					if (CollectionUtil.isEmpty(prErp2Pojo.getPrItems())) {
						LOG.warn("No Line Items found for this sourcing form. Not going to send it to ERP : " + sourcingFormRequest.getId());
						return;
					}

					try {
						responseMsg = restTemplate.postForObject(erpSetup.getErpUrl() + "/PRDelete/", request, String.class);
					} catch (HttpClientErrorException | HttpServerErrorException ex) {
						responseMsg = ex.getMessage();
						LOG.error("Error received from ERP for RFS Delete : " + ex.getMessage() + " Status Text : " + ex.getStatusText());
						LOG.error("Response Body : " + ex.getResponseBodyAsString());
						throw new ApplicationException("Error received from ERP for RFS Delete : " + ex.getMessage() + " Status Text : " + ex.getStatusText(), ex);
					}
				}

				LOG.info("response :" + responseMsg);

				// Storing audit history
				try {
					RequestAudit audit = new RequestAudit();
					audit.setAction(RequestAuditType.ERP);
					audit.setActionBy(SecurityLibrary.getLoggedInUser());
					audit.setActionDate(new Date());
					audit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
					audit.setDescription("Delete RFS Request sent to ERP. Response : " + StringUtils.checkString(responseMsg));
					audit.setReq(sourcingFormRequest);
					requestAuditDao.save(audit);
				} catch (Exception e) {
					LOG.error("Error while saving Sourcing ERP Audit History :" + e.getMessage(), e);
					throw e;
				}
			}
		} catch (Exception e) {
			// Storing audit history for error
			try {
				RequestAudit audit = new RequestAudit();
				audit.setAction(RequestAuditType.ERROR);
				audit.setActionBy(SecurityLibrary.getLoggedInUser());
				audit.setActionDate(new Date());
				audit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
				audit.setDescription(e.getMessage());
				audit.setReq(sourcingFormRequest);
				requestAuditDao.save(audit);
			} catch (Exception error) {
				LOG.error("Error while saving Sourcing ERP Audit History in catch block :" + error.getMessage(), error);
			}
			LOG.error("Error while transfer pr to erp :" + e.getMessage(), e);
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false)
	public void transferRejectRfsToErp(RfqEvent event, ErpSetup erpSetup) throws Exception {

		try {

			ClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());
			restTemplate = new RestTemplate(factory);
			restTemplate.setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
			restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());

			ErpIntegrationTypeForPr type = erpSetup.getErpIntegrationTypeForPr();
			HttpHeaders headers = new HttpHeaders();

			LOG.info("erpConfig erp enable :" + erpSetup.getIsErpEnable());
			if (erpSetup != null && Boolean.TRUE == erpSetup.getIsErpEnable() && Boolean.TRUE == erpSetup.getEnableRfsErpPush()) {
				// sending PR to ERP
				LOG.info("ERP Integration is enabled.... sending CANCEL PR to ERP due to RFQ Cancel...");

				ObjectMapper mapperObj = new ObjectMapper();
				String payload = "";

				String responseMsg = "";
				if (ErpIntegrationTypeForPr.TYPE_2 == type) {
					// Fetch the contract reference numbers and storage location...
					/*
					 * if (pr.getTemplate() != null && Boolean.TRUE == pr.getTemplate().getContractItemsOnly()) { for
					 * (PrItem item : pr.getPrItems()) { if (item.getProduct() != null) { ProductContractItems pcItem =
					 * productContractItemsDao.findProductContractItemByItemId(item.getProduct().getId()); if (pcItem !=
					 * null) { item.getProduct().setStorageLocation(pcItem.getStorageLocation());
					 * item.getProduct().setContractReferenceNumber
					 * (pcItem.getProductContract().getContractReferenceNumber());
					 * item.getProduct().setItemContractReferenceNumber(pcItem.getContractItemNumber());
					 * item.setCostCenter(pcItem.getCostCenter() != null ? pcItem.getCostCenter().getCostCenter() :
					 * null); item.setPurchaseGroup(pcItem.getProductContract().getGroupCode()); } } } }
					 */

					if (StringUtils.checkString(erpSetup.getErpUsername()).length() > 0 && StringUtils.checkString(erpSetup.getErpPassword()).length() > 0) {
						String auth = StringUtils.checkString(erpSetup.getErpUsername()) + ":" + StringUtils.checkString(erpSetup.getErpPassword());
						byte[] encodedAuth = Base64.encode(auth.getBytes(Charset.forName("US-ASCII")));
						String authHeader = "Basic " + new String(encodedAuth);
						headers.set("Authorization", authHeader);
					}

					if (CollectionUtil.isNotEmpty(event.getEventBqs()) && event.getEventBqs().size() > 1) {
						throw new ApplicationException("Multiple BQs found. Cannot send Cancel request to ERP");
					}

					if (StringUtils.checkString(event.getPreviousRequestId()).length() == 0) {
						throw new ApplicationException("Looks like this RFQ isnt created from RFS. Cannot send delete request to ERP");
					}

					SourcingFormRequest sourcingFormRequest = requestService.getSourcingRequestById(event.getPreviousRequestId());

					// for Type 2
					PrErp2DeletePojo prErp2Pojo = new PrErp2DeletePojo(event, sourcingFormRequest.getFormId(), sourcingFormRequest.getErpDocNo());
				
					// Checking for interface code if exisit send interface code to erp insted of product code
					for (PrItemErp2DeletePojo item : prErp2Pojo.getPrItems()) {
						ProductItem productItem = productListMaintenanceDao.findProductItemByCode(item.getItemCode(), erpSetup.getTenantId(), null);
						if (productItem != null && StringUtils.checkString(productItem.getInterfaceCode()).length() > 0) {
							item.setItemCode(productItem.getInterfaceCode());
						}
					}

					payload = mapperObj.writeValueAsString(prErp2Pojo);
					LOG.info("jsonObject  :" + payload);

					HttpEntity<PrErp2DeletePojo> request = new HttpEntity<PrErp2DeletePojo>(prErp2Pojo, headers);
					try {
						responseMsg = restTemplate.postForObject(erpSetup.getErpUrl() + "/PRDelete/", request, String.class);
					} catch (HttpClientErrorException | HttpServerErrorException ex) {
						responseMsg = ex.getMessage();
						LOG.error("Error received from ERP for RFQ Delete : " + ex.getMessage() + " Status Text : " + ex.getStatusText());
						LOG.error("Response Body : " + ex.getResponseBodyAsString());
						throw new ApplicationException("Error received from ERP for RFQ Delete : " + ex.getMessage() + " Status Text : " + ex.getStatusText(), ex);
					}
				}

				LOG.info("response :" + responseMsg);

				// Storing audit history
				try {
					RfqEventAudit audit = new RfqEventAudit();
					audit.setAction(AuditActionType.Transfer);
					audit.setActionBy(SecurityLibrary.getLoggedInUser());
					audit.setActionDate(new Date());
					audit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
					audit.setDescription("Delete RFQ Request sent to ERP. Response : " + StringUtils.checkString(responseMsg));
					audit.setEvent(event);
					rfqEventAuditDao.save(audit);
				} catch (Exception e) {
					LOG.error("Error while saving RFQ ERP Audit History :" + e.getMessage(), e);
					throw e;
				}
			}
		} catch (Exception e) {
			// Storing audit history for error
			try {
				RfqEventAudit audit = new RfqEventAudit();
				audit.setAction(AuditActionType.Transfer);
				audit.setActionBy(SecurityLibrary.getLoggedInUser());
				audit.setActionDate(new Date());
				audit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
				audit.setDescription(e.getMessage());
				audit.setEvent(event);
				rfqEventAuditDao.save(audit);
			} catch (Exception error) {
				LOG.error("Error while saving RFQ ERP Audit History in catch block :" + error.getMessage(), error);
			}
			LOG.error("Error while transfer Cancel Request to erp :" + e.getMessage(), e);
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false)
	public void transferRejectRfsToErp(RftEvent event, ErpSetup erpSetup) throws Exception {

		try {

			ClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());
			restTemplate = new RestTemplate(factory);
			restTemplate.setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
			restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());

			ErpIntegrationTypeForPr type = erpSetup.getErpIntegrationTypeForPr();
			HttpHeaders headers = new HttpHeaders();

			LOG.info("erpConfig erp enable :" + erpSetup.getIsErpEnable());
			if (erpSetup != null && Boolean.TRUE == erpSetup.getIsErpEnable() && Boolean.TRUE == erpSetup.getEnableRfsErpPush()) {
				// sending PR to ERP
				LOG.info("ERP Integration is enabled.... sending CANCEL PR to ERP due to RFT Cancel...");

				ObjectMapper mapperObj = new ObjectMapper();
				String payload = "";

				String responseMsg = "";
				if (ErpIntegrationTypeForPr.TYPE_2 == type) {
					// Fetch the contract reference numbers and storage location...
					/*
					 * if (pr.getTemplate() != null && Boolean.TRUE == pr.getTemplate().getContractItemsOnly()) { for
					 * (PrItem item : pr.getPrItems()) { if (item.getProduct() != null) { ProductContractItems pcItem =
					 * productContractItemsDao.findProductContractItemByItemId(item.getProduct().getId()); if (pcItem !=
					 * null) { item.getProduct().setStorageLocation(pcItem.getStorageLocation());
					 * item.getProduct().setContractReferenceNumber
					 * (pcItem.getProductContract().getContractReferenceNumber());
					 * item.getProduct().setItemContractReferenceNumber(pcItem.getContractItemNumber());
					 * item.setCostCenter(pcItem.getCostCenter() != null ? pcItem.getCostCenter().getCostCenter() :
					 * null); item.setPurchaseGroup(pcItem.getProductContract().getGroupCode()); } } } }
					 */

					if (StringUtils.checkString(erpSetup.getErpUsername()).length() > 0 && StringUtils.checkString(erpSetup.getErpPassword()).length() > 0) {
						String auth = StringUtils.checkString(erpSetup.getErpUsername()) + ":" + StringUtils.checkString(erpSetup.getErpPassword());
						byte[] encodedAuth = Base64.encode(auth.getBytes(Charset.forName("US-ASCII")));
						String authHeader = "Basic " + new String(encodedAuth);
						headers.set("Authorization", authHeader);
					}

					if (CollectionUtil.isNotEmpty(event.getEventBqs()) && event.getEventBqs().size() > 1) {
						throw new ApplicationException("Multiple BQs found. Cannot send Cancel request to ERP");
					}

					if (StringUtils.checkString(event.getPreviousRequestId()).length() == 0) {
						throw new ApplicationException("Looks like this RFT isnt created from RFS. Cannot send delete request to ERP");
					}

					SourcingFormRequest sourcingFormRequest = requestService.getSourcingRequestById(event.getPreviousRequestId());

					// for Type 2
					PrErp2DeletePojo prErp2Pojo = new PrErp2DeletePojo(event, sourcingFormRequest.getFormId(), sourcingFormRequest.getErpDocNo());
					// Checking for interface code if exisit send interface code to erp insted of product code
					for (PrItemErp2DeletePojo item : prErp2Pojo.getPrItems()) {
						ProductItem productItem = productListMaintenanceDao.findProductItemByCode(item.getItemCode(), erpSetup.getTenantId(), null);
						if (productItem != null && StringUtils.checkString(productItem.getInterfaceCode()).length() > 0) {
							item.setItemCode(productItem.getInterfaceCode());
						}
					}
				
					payload = mapperObj.writeValueAsString(prErp2Pojo);
					LOG.info("jsonObject  :" + payload);

					HttpEntity<PrErp2DeletePojo> request = new HttpEntity<PrErp2DeletePojo>(prErp2Pojo, headers);
					try {
						responseMsg = restTemplate.postForObject(erpSetup.getErpUrl() + "/PRDelete/", request, String.class);
					} catch (HttpClientErrorException | HttpServerErrorException ex) {
						responseMsg = ex.getMessage();
						LOG.error("Error received from ERP for RFT Delete : " + ex.getMessage() + " Status Text : " + ex.getStatusText());
						LOG.error("Response Body : " + ex.getResponseBodyAsString());
						throw new ApplicationException("Error received from ERP for RFT Delete : " + ex.getMessage() + " Status Text : " + ex.getStatusText(), ex);
					}
				}

				LOG.info("response :" + responseMsg);

				// Storing audit history
				try {
					RftEventAudit audit = new RftEventAudit();
					audit.setAction(AuditActionType.Transfer);
					audit.setActionBy(SecurityLibrary.getLoggedInUser());
					audit.setActionDate(new Date());
					audit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
					audit.setDescription("Delete RFT Request sent to ERP. Response : " + StringUtils.checkString(responseMsg));
					audit.setEvent(event);
					rftEventAuditDao.save(audit);
				} catch (Exception e) {
					LOG.error("Error while saving RFT ERP Audit History :" + e.getMessage(), e);
					throw e;
				}
			}
		} catch (Exception e) {
			// Storing audit history for error
			try {
				RftEventAudit audit = new RftEventAudit();
				audit.setAction(AuditActionType.Transfer);
				audit.setActionBy(SecurityLibrary.getLoggedInUser());
				audit.setActionDate(new Date());
				audit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
				audit.setDescription(e.getMessage());
				audit.setEvent(event);
				rftEventAuditDao.save(audit);
			} catch (Exception error) {
				LOG.error("Error while saving RFT ERP Audit History in catch block :" + error.getMessage(), error);
			}
			LOG.error("Error while transfer Cancel Request to erp :" + e.getMessage(), e);
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false)
	public void transferRejectRfsToErp(RfaEvent event, ErpSetup erpSetup) throws Exception {

		try {

			ClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());
			restTemplate = new RestTemplate(factory);
			restTemplate.setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
			restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());

			ErpIntegrationTypeForPr type = erpSetup.getErpIntegrationTypeForPr();
			HttpHeaders headers = new HttpHeaders();

			LOG.info("erpConfig erp enable :" + erpSetup.getIsErpEnable());
			if (erpSetup != null && Boolean.TRUE == erpSetup.getIsErpEnable() && Boolean.TRUE == erpSetup.getEnableRfsErpPush()) {
				// sending PR to ERP
				LOG.info("ERP Integration is enabled.... sending CANCEL PR to ERP due to RFA Cancel...");

				ObjectMapper mapperObj = new ObjectMapper();
				String payload = "";

				String responseMsg = "";
				if (ErpIntegrationTypeForPr.TYPE_2 == type) {
					// Fetch the contract reference numbers and storage location...
					/*
					 * if (pr.getTemplate() != null && Boolean.TRUE == pr.getTemplate().getContractItemsOnly()) { for
					 * (PrItem item : pr.getPrItems()) { if (item.getProduct() != null) { ProductContractItems pcItem =
					 * productContractItemsDao.findProductContractItemByItemId(item.getProduct().getId()); if (pcItem !=
					 * null) { item.getProduct().setStorageLocation(pcItem.getStorageLocation());
					 * item.getProduct().setContractReferenceNumber
					 * (pcItem.getProductContract().getContractReferenceNumber());
					 * item.getProduct().setItemContractReferenceNumber(pcItem.getContractItemNumber());
					 * item.setCostCenter(pcItem.getCostCenter() != null ? pcItem.getCostCenter().getCostCenter() :
					 * null); item.setPurchaseGroup(pcItem.getProductContract().getGroupCode()); } } } }
					 */

					if (StringUtils.checkString(erpSetup.getErpUsername()).length() > 0 && StringUtils.checkString(erpSetup.getErpPassword()).length() > 0) {
						String auth = StringUtils.checkString(erpSetup.getErpUsername()) + ":" + StringUtils.checkString(erpSetup.getErpPassword());
						byte[] encodedAuth = Base64.encode(auth.getBytes(Charset.forName("US-ASCII")));
						String authHeader = "Basic " + new String(encodedAuth);
						headers.set("Authorization", authHeader);
					}

					if (CollectionUtil.isNotEmpty(event.getEventBqs()) && event.getEventBqs().size() > 1) {
						throw new ApplicationException("Multiple BQs found. Cannot send Cancel request to ERP");
					}

					if (StringUtils.checkString(event.getPreviousRequestId()).length() == 0) {
						throw new ApplicationException("Looks like this RFA isnt created from RFS. Cannot send delete request to ERP");
					}

					SourcingFormRequest sourcingFormRequest = requestService.getSourcingRequestById(event.getPreviousRequestId());

					// for Type 2
					PrErp2DeletePojo prErp2Pojo = new PrErp2DeletePojo(event, sourcingFormRequest.getFormId(), sourcingFormRequest.getErpDocNo());
					// Checking for interface code if exisit send interface code to erp insted of product code
					for (PrItemErp2DeletePojo item : prErp2Pojo.getPrItems()) {
						ProductItem productItem = productListMaintenanceDao.findProductItemByCode(item.getItemCode(), erpSetup.getTenantId(), null);
						if (productItem != null && StringUtils.checkString(productItem.getInterfaceCode()).length() > 0) {
							item.setItemCode(productItem.getInterfaceCode());
						}
					}
					
					payload = mapperObj.writeValueAsString(prErp2Pojo);
					LOG.info("jsonObject  :" + payload);

					HttpEntity<PrErp2DeletePojo> request = new HttpEntity<PrErp2DeletePojo>(prErp2Pojo, headers);
					try {
						responseMsg = restTemplate.postForObject(erpSetup.getErpUrl() + "/PRDelete/", request, String.class);
					} catch (HttpClientErrorException | HttpServerErrorException ex) {
						responseMsg = ex.getMessage();
						LOG.error("Error received from ERP for RFA Delete : " + ex.getMessage() + " Status Text : " + ex.getStatusText());
						LOG.error("Response Body : " + ex.getResponseBodyAsString());
						throw new ApplicationException("Error received from ERP for RFA Delete : " + ex.getMessage() + " Status Text : " + ex.getStatusText(), ex);
					}
				}

				LOG.info("response :" + responseMsg);

				// Storing audit history
				try {
					RfaEventAudit audit = new RfaEventAudit();
					audit.setAction(AuditActionType.Transfer);
					audit.setActionBy(SecurityLibrary.getLoggedInUser());
					audit.setActionDate(new Date());
					audit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
					audit.setDescription("Delete RFA Request sent to ERP. Response : " + StringUtils.checkString(responseMsg));
					audit.setEvent(event);
					rfaEventAuditDao.save(audit);
				} catch (Exception e) {
					LOG.error("Error while saving RFA ERP Audit History :" + e.getMessage(), e);
					throw e;
				}
			}
		} catch (Exception e) {
			// Storing audit history for error
			try {
				RfaEventAudit audit = new RfaEventAudit();
				audit.setAction(AuditActionType.Transfer);
				audit.setActionBy(SecurityLibrary.getLoggedInUser());
				audit.setActionDate(new Date());
				audit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
				audit.setDescription(e.getMessage());
				audit.setEvent(event);
				rfaEventAuditDao.save(audit);
			} catch (Exception error) {
				LOG.error("Error while saving RFA ERP Audit History in catch block :" + error.getMessage(), error);
			}
			LOG.error("Error while transfer Cancel Request to erp :" + e.getMessage(), e);
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false)
	public void transferRejectRfsToErp(RfpEvent event, ErpSetup erpSetup) throws Exception {

		try {

			ClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());
			restTemplate = new RestTemplate(factory);
			restTemplate.setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
			restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());

			ErpIntegrationTypeForPr type = erpSetup.getErpIntegrationTypeForPr();
			HttpHeaders headers = new HttpHeaders();

			LOG.info("erpConfig erp enable :" + erpSetup.getIsErpEnable());
			if (erpSetup != null && Boolean.TRUE == erpSetup.getIsErpEnable() && Boolean.TRUE == erpSetup.getEnableRfsErpPush()) {
				// sending PR to ERP
				LOG.info("ERP Integration is enabled.... sending CANCEL PR to ERP due to RFP Cancel...");

				ObjectMapper mapperObj = new ObjectMapper();
				String payload = "";

				String responseMsg = "";
				if (ErpIntegrationTypeForPr.TYPE_2 == type) {
					// Fetch the contract reference numbers and storage location...
					/*
					 * if (pr.getTemplate() != null && Boolean.TRUE == pr.getTemplate().getContractItemsOnly()) { for
					 * (PrItem item : pr.getPrItems()) { if (item.getProduct() != null) { ProductContractItems pcItem =
					 * productContractItemsDao.findProductContractItemByItemId(item.getProduct().getId()); if (pcItem !=
					 * null) { item.getProduct().setStorageLocation(pcItem.getStorageLocation());
					 * item.getProduct().setContractReferenceNumber
					 * (pcItem.getProductContract().getContractReferenceNumber());
					 * item.getProduct().setItemContractReferenceNumber(pcItem.getContractItemNumber());
					 * item.setCostCenter(pcItem.getCostCenter() != null ? pcItem.getCostCenter().getCostCenter() :
					 * null); item.setPurchaseGroup(pcItem.getProductContract().getGroupCode()); } } } }
					 */

					if (StringUtils.checkString(erpSetup.getErpUsername()).length() > 0 && StringUtils.checkString(erpSetup.getErpPassword()).length() > 0) {
						String auth = StringUtils.checkString(erpSetup.getErpUsername()) + ":" + StringUtils.checkString(erpSetup.getErpPassword());
						byte[] encodedAuth = Base64.encode(auth.getBytes(Charset.forName("US-ASCII")));
						String authHeader = "Basic " + new String(encodedAuth);
						headers.set("Authorization", authHeader);
					}

					if (CollectionUtil.isNotEmpty(event.getEventBqs()) && event.getEventBqs().size() > 1) {
						throw new ApplicationException("Multiple BQs found. Cannot send Cancel request to ERP");
					}

					if (StringUtils.checkString(event.getPreviousRequestId()).length() == 0) {
						throw new ApplicationException("Looks like this RFP isnt created from RFS. Cannot send delete request to ERP");
					}

					SourcingFormRequest sourcingFormRequest = requestService.getSourcingRequestById(event.getPreviousRequestId());

					// for Type 2
					PrErp2DeletePojo prErp2Pojo = new PrErp2DeletePojo(event, sourcingFormRequest.getFormId(), sourcingFormRequest.getErpDocNo());
					// Checking for interface code if exisit send interface code to erp insted of product code
					for (PrItemErp2DeletePojo item : prErp2Pojo.getPrItems()) {
						ProductItem productItem = productListMaintenanceDao.findProductItemByCode(item.getItemCode(), erpSetup.getTenantId(), null);
						if (productItem != null && StringUtils.checkString(productItem.getInterfaceCode()).length() > 0) {
							item.setItemCode(productItem.getInterfaceCode());
						}
					}
				
					payload = mapperObj.writeValueAsString(prErp2Pojo);
					LOG.info("jsonObject  :" + payload);

					HttpEntity<PrErp2DeletePojo> request = new HttpEntity<PrErp2DeletePojo>(prErp2Pojo, headers);
					try {
						responseMsg = restTemplate.postForObject(erpSetup.getErpUrl() + "/PRDelete/", request, String.class);
					} catch (HttpClientErrorException | HttpServerErrorException ex) {
						responseMsg = ex.getMessage();
						LOG.error("Error received from ERP for RFP Delete : " + ex.getMessage() + " Status Text : " + ex.getStatusText());
						LOG.error("Response Body : " + ex.getResponseBodyAsString());
						throw new ApplicationException("Error received from ERP for RFP Delete : " + ex.getMessage() + " Status Text : " + ex.getStatusText(), ex);
					}
				}

				LOG.info("response :" + responseMsg);

				// Storing audit history
				try {
					RfpEventAudit audit = new RfpEventAudit();
					audit.setAction(AuditActionType.Transfer);
					audit.setActionBy(SecurityLibrary.getLoggedInUser());
					audit.setActionDate(new Date());
					audit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
					audit.setDescription("Delete RFP Request sent to ERP. Response : " + StringUtils.checkString(responseMsg));
					audit.setEvent(event);
					rfpEventAuditDao.save(audit);
				} catch (Exception e) {
					LOG.error("Error while saving RFP ERP Audit History :" + e.getMessage(), e);
					throw e;
				}
			}
		} catch (Exception e) {
			// Storing audit history for error
			try {
				RfpEventAudit audit = new RfpEventAudit();
				audit.setAction(AuditActionType.Transfer);
				audit.setActionBy(SecurityLibrary.getLoggedInUser());
				audit.setActionDate(new Date());
				audit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
				audit.setDescription(e.getMessage());
				audit.setEvent(event);
				rfpEventAuditDao.save(audit);
			} catch (Exception error) {
				LOG.error("Error while saving RFP ERP Audit History in catch block :" + error.getMessage(), error);
			}
			LOG.error("Error while transfer Cancel Request to erp :" + e.getMessage(), e);
			throw e;
		}
	}

	@Override
	public List<MobileEventPojo> getEventTypeFromPrNo(String prNo, String tenantId) {
		return erpConfigDao.getEventTypeFromPrNo(prNo, tenantId);
	}

	@Override
	@Transactional(readOnly = false, rollbackFor = { Exception.class })
	public RfaEvent overwriteFromRfaTemplateForErp(User loggedInUser, PrToAuctionErpPojo prToAuctionErpPojo, MobileEventPojo draftEventPojo) throws ApplicationException {
		RfaEvent newEvent = rfaEventDao.findByEventId(draftEventPojo.getId());
		if (newEvent != null) {
			rfaBqService.deleteAllERPBqByEventId(newEvent.getId());
		}

		newEvent = constructRfaEventDetials(prToAuctionErpPojo, newEvent);
		return newEvent;
	}

	@Override
	@Transactional(readOnly = false, rollbackFor = { Exception.class })
	public RfqEvent overwriteRfqTemplateForErp(User loggedInUser, PrToAuctionErpPojo prToAuctionErpPojo, MobileEventPojo draftEventPojo) throws ApplicationException {
		RfqEvent newEvent = rfqEventDao.findByEventId(draftEventPojo.getId());
		if (newEvent != null) {
			rfqBqService.deleteAllERPBqByEventId(newEvent.getId());
		}
		newEvent = constructRfqEventDetials(prToAuctionErpPojo, newEvent);
		return newEvent;
	}

	@Override
	@Transactional(readOnly = false, rollbackFor = { Exception.class })
	public RfpEvent overwriteRfpTemplateForErp(User loggedInUser, PrToAuctionErpPojo prToAuctionErpPojo, MobileEventPojo draftEventPojo) throws ApplicationException {
		RfpEvent newEvent = rfpEventDao.findByEventId(draftEventPojo.getId());
		if (newEvent != null) {
			rfpBqService.deleteAllERPBqByEventId(newEvent.getId());
		}
		newEvent = constructRfpEventDetials(prToAuctionErpPojo, newEvent);
		return newEvent;
	}

	@Override
	@Transactional(readOnly = false, rollbackFor = { Exception.class })
	public RftEvent overwriteRftTemplateForErp(User loggedInUser, PrToAuctionErpPojo prToAuctionErpPojo, MobileEventPojo draftEventPojo) throws ApplicationException {
		RftEvent newEvent = rftEventDao.findByEventId(draftEventPojo.getId());
		if (newEvent != null) {
			rftBqService.deleteAllERPBqByEventId(newEvent.getId());
		}
		newEvent = constructRftEventDetials(prToAuctionErpPojo, newEvent);
		return newEvent;
	}

	@Override
	@Transactional(readOnly = false)
	public void updateRfqResponse(RFQResponseErpPojo rfqResponseErpPojo, ErpSetup erpSetup) {
		Buyer buyer = buyerService.findBuyerById(erpSetup.getTenantId());
		RfaEvent rfaEvent = rfaEventService.findRfaEventByErpAwardRefNoAndTenantId(rfqResponseErpPojo.getRFQ_docNo(), erpSetup.getTenantId());
		if (rfaEvent != null) {
			try {
				String response = StringUtils.checkString(rfaEvent.getErpAwardResponse());
				response += ",DOCNO=" + rfqResponseErpPojo.getRFQ_docNo() + ";STATUS=" + rfqResponseErpPojo.getStatus();
				rfaEvent.setErpAwardResponse(response);
				rfaEventService.updateRfaEvent(rfaEvent);
				if (rfqResponseErpPojo.getStatus().equalsIgnoreCase("ERROR")) {
					try {
						if (rfaEvent.getCreatedBy() != null) {
							sendRfxAwardTransferErrorEmail(rfaEvent.getCreatedBy(), RfxTypes.RFA, rfaEvent.getEventName(), rfaEvent.getId(), rfaEvent.getEventId(), rfqResponseErpPojo.getRFQ_docNo(), rfqResponseErpPojo.getMessage());

						}
					} catch (Exception e) {
						LOG.error("Error while Sending mail:" + e.getMessage(), e);
					}
					RfaEventAwardAudit audit = new RfaEventAwardAudit(rfaEvent, buyer, null, new Date(), "Error While transfer award to SAP:" + rfqResponseErpPojo.getMessage());
					eventAwardAuditService.saveRfaAwardAudit(audit);
				}
			} catch (Exception e) {
				RfaEventAwardAudit audit = new RfaEventAwardAudit(rfaEvent, buyer, null, new Date(), "Error While Processing SAP Response:" + e.getMessage());
				eventAwardAuditService.saveRfaAwardAudit(audit);
			}
		} else {
			RfpEvent rfpEvent = rfpEventService.findRfpEventByErpAwardRefNoAndTenantId(rfqResponseErpPojo.getRFQ_docNo(), erpSetup.getTenantId());
			if (rfpEvent != null) {
				try {
					String response = StringUtils.checkString(rfpEvent.getErpAwardResponse());
					response += ",DOCNO=" + rfqResponseErpPojo.getRFQ_docNo() + ";STATUS=" + rfqResponseErpPojo.getStatus();
					LOG.info("RES: " + response + ".");
					rfpEvent.setErpAwardResponse(response);
					rfpEventService.updateRfpEvent(rfpEvent);
					if (rfqResponseErpPojo.getStatus().equalsIgnoreCase("ERROR")) {
						try {
							if (rfpEvent.getCreatedBy() != null) {
								sendRfxAwardTransferErrorEmail(rfpEvent.getCreatedBy(), RfxTypes.RFP, rfpEvent.getEventName(), rfpEvent.getId(), rfpEvent.getEventId(), rfqResponseErpPojo.getRFQ_docNo(), rfqResponseErpPojo.getMessage());
							}
						} catch (Exception e) {
							LOG.error("Error while Sending mail :" + e.getMessage(), e);
						}
						RfpEventAwardAudit audit = new RfpEventAwardAudit(rfpEvent, buyer, null, new Date(), "Error While transfer award to SAP:" + rfqResponseErpPojo.getMessage());
						eventAwardAuditService.saveRfpAwardAudit(audit);

					}
				} catch (Exception e) {
					RfpEventAwardAudit audit = new RfpEventAwardAudit(rfpEvent, buyer, null, new Date(), "Error While Processing SAP Response" + e.getMessage());
					eventAwardAuditService.saveRfpAwardAudit(audit);
				}
			} else {
				RftEvent rftEvent = rftEventService.findRftEventByErpAwardRefNoAndTenantId(rfqResponseErpPojo.getRFQ_docNo(), erpSetup.getTenantId());
				if (rftEvent != null) {
					try {
						String response = StringUtils.checkString(rftEvent.getErpAwardResponse());
						response += ",DOCNO=" + rfqResponseErpPojo.getRFQ_docNo() + ";STATUS=" + rfqResponseErpPojo.getStatus();
						rftEvent.setErpAwardResponse(response);
						rftEventService.updateRftEvent(rftEvent);
						if (rfqResponseErpPojo.getStatus().equalsIgnoreCase("ERROR")) {
							try {
								if (rftEvent.getCreatedBy() != null) {
									sendRfxAwardTransferErrorEmail(rftEvent.getCreatedBy(), RfxTypes.RFT, rftEvent.getEventName(), rftEvent.getId(), rftEvent.getEventId(), rfqResponseErpPojo.getRFQ_docNo(), rfqResponseErpPojo.getMessage());
								}
							} catch (Exception e) {
								LOG.error("Error while Sending mail :" + e.getMessage(), e);
							}

							RftEventAwardAudit audit = new RftEventAwardAudit(rftEvent, buyer, null, new Date(), "Error While transfer award to SAP:" + rfqResponseErpPojo.getMessage());
							eventAwardAuditService.saveRftAwardAudit(audit);
						}
					} catch (Exception e) {

						RftEventAwardAudit audit = new RftEventAwardAudit(rftEvent, buyer, null, new Date(), "Error While Processing SAP Response:" + e.getMessage());
						eventAwardAuditService.saveRftAwardAudit(audit);

					}
				} else {
					RfqEvent rfqEvent = rfqEventService.findRfqEventByErpAwardRefNoAndTenantId(rfqResponseErpPojo.getRFQ_docNo(), erpSetup.getTenantId());
					try {
						if (rfqEvent != null) {
							String response = StringUtils.checkString(rfqEvent.getErpAwardResponse());
							response += ",REF_NO" + rfqResponseErpPojo.getRFQ_docNo() + ";STATUS" + rfqResponseErpPojo.getStatus();
							rfqEvent.setErpAwardResponse(response);
							rfqEventService.updateEvent(rfqEvent);
							if (rfqResponseErpPojo.getStatus().equalsIgnoreCase("ERROR")) {
								try {
									if (rfqEvent.getCreatedBy() != null) {
										sendRfxAwardTransferErrorEmail(rfqEvent.getCreatedBy(), RfxTypes.RFQ, rfqEvent.getEventName(), rfqEvent.getId(), rfqEvent.getEventId(), rfqResponseErpPojo.getRFQ_docNo(), rfqResponseErpPojo.getMessage());

									}
								} catch (Exception e) {
									LOG.error("Error while sending mail:" + e.getMessage(), e);

								}

								RfqEventAwardAudit audit = new RfqEventAwardAudit(rfqEvent, buyer, null, new Date(), "Error While transfer award to SAP:" + rfqResponseErpPojo.getMessage());
								eventAwardAuditService.saveRfqAwardAudit(audit);
							}
						} else {

							LOG.warn("No Matching event found for ERP Award Ref No : " + rfqResponseErpPojo.getRFQ_docNo());

						}
					} catch (Exception e) {

						RfqEventAwardAudit audit = new RfqEventAwardAudit(rfqEvent, buyer, null, new Date(), "Error While Processing SAP Response:" + e.getMessage());
						eventAwardAuditService.saveRfqAwardAudit(audit);

					}

				}
			}
		}

	}

	private void sendRfxAwardTransferErrorEmail(User user, RfxTypes eventType, String eventName, String id, String eventId, String referanceNumber, String remarks) {
		LOG.info("Sending error request email to(" + user.getName() + "):" + user.getCommunicationEmail());
		LOG.info("EventType-----------:" + eventType.name());
		String url = APP_URL + "/buyer/" + eventType.name() + "/eventAward/" + id + "";
		String subject = "Award transfer failed to SAP";
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("userName", user.getName());
		map.put("remarks", remarks);
		map.put("eventName", eventName);
		map.put("referanceNumber", referanceNumber);
		LOG.info("RefrenceNumber----------" + referanceNumber);
		map.put("message", "Failed award transfer to SAP");
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		String timeZone = "GMT+8:00";
		timeZone = getTimeZoneByBuyerSettings(user.getTenantId(), timeZone);
		df.setTimeZone(TimeZone.getTimeZone(timeZone));
		map.put("date", df.format(new Date()));
		map.put("loginUrl", APP_URL + "/login");
		map.put("appUrl", url);
		if (StringUtils.checkString(user.getCommunicationEmail()).length() > 0) {
			LOG.info("User Communication email-----------------" + user.getCommunicationEmail());
			sendEmail(user.getCommunicationEmail(), subject, map, Global.ERP_EVENT_REJECT_TEMPLATE);

		} else {
			LOG.warn("No communication email configured for user : " + user.getLoginId() + "... Not going to send email notification");
		}
		LOG.info("Noitification called here----------------");
		String notificationMessage = messageSource.getMessage("erp.rfx.error.notification.message", new Object[] { eventName, eventId, remarks }, Global.LOCALE);
		sendDashboardNotification(id, user, url, subject, notificationMessage, NotificationType.EVENT_MESSAGE);

	}

	private String getTimeZoneByBuyerSettings(String tenantId, String timeZone) {
		try {
			if (StringUtils.checkString(tenantId).length() > 0) {
				String time = buyerSettingsService.getBuyerTimeZoneByTenantId(tenantId);
				if (time != null) {
					timeZone = time;
				}
			}
		} catch (Exception e) {
			LOG.error("Error while fetching buyer time zone :" + e.getMessage(), e);
		}
		return timeZone;
	}

	private void sendEmail(String mailTo, String subject, HashMap<String, Object> map, String template) {
		LOG.info("mailTo-----" + mailTo);
		if (StringUtils.checkString(mailTo).length() > 0) {
			try {
				LOG.info("Sending request email to : " + mailTo);
				String message = FreeMarkerTemplateUtils.processTemplateIntoString(freemarkerConfiguration.getTemplate(template), map);
				notificationService.sendEmail(mailTo, subject, message);
			} catch (Exception e) {
				LOG.error("ERROR while Sending mail :" + e.getMessage(), e);
			}
		} else {
			LOG.warn("No communication email configured for user... Not going to send email notification");
		}
	}

	private void sendDashboardNotification(String id, User messageTo, String url, String subject, String notificationMessage, NotificationType notificationType) {
		try {
			LOG.info("Dashboard notification---------------");
			NotificationMessage message = new NotificationMessage();
			message.setCreatedBy(null);
			message.setCreatedDate(new Date());
			message.setMessage(notificationMessage);
			message.setNotificationType(notificationType);
			message.setMessageTo(messageTo);
			message.setSubject(subject);
			message.setTenantId(messageTo.getTenantId());
			message.setUrl(url);
			dashboardNotificationService.save(message);
		} catch (Exception e) {
			LOG.error("Error while saving dashboard notification :" + e.getMessage(), e);
		}
	}

	/*
	 * public static void main(String[] args) throws Exception { List<AwardReferenceNumberPojo> list = new
	 * ArrayList<>(); list.add(new AwardReferenceNumberPojo(0, 1, "qq")); ObjectMapper mapper = new ObjectMapper();
	 * String res = mapper.writeValueAsString(list); System.out.println(res);
	 * TypeReference<List<AwardReferenceNumberPojo>> typeList = new TypeReference<List<AwardReferenceNumberPojo>>(){};
	 * List<AwardReferenceNumberPojo> pojo = mapper.readValue(res, typeList); for (AwardReferenceNumberPojo
	 * awardReferenceNumberPojo : pojo) { System.out.println(awardReferenceNumberPojo.toLogString()); } }
	 */

}
