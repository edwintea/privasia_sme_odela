package com.privasia.procurehere.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpSession;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.privasia.procurehere.core.dao.BudgetDao;
import com.privasia.procurehere.core.dao.ErpSetupDao;
import com.privasia.procurehere.core.dao.PoFinanceDao;
import com.privasia.procurehere.core.dao.PoSharingBuyerDao;
import com.privasia.procurehere.core.dao.PrDao;
import com.privasia.procurehere.core.dao.ProductContractItemsDao;
import com.privasia.procurehere.core.dao.RfaEventDao;
import com.privasia.procurehere.core.dao.RfiEventDao;
import com.privasia.procurehere.core.dao.RfpEventDao;
import com.privasia.procurehere.core.dao.RfqEventDao;
import com.privasia.procurehere.core.dao.RftEventDao;
import com.privasia.procurehere.core.dao.SourcingFormRequestDao;
import com.privasia.procurehere.core.dao.SupplierFormSubmissionDao;
import com.privasia.procurehere.core.dao.UserDao;
import com.privasia.procurehere.core.entity.ApprovalUser;
import com.privasia.procurehere.core.entity.Budget;
import com.privasia.procurehere.core.entity.BudgetApproval;
import com.privasia.procurehere.core.entity.BudgetApprovalUser;
import com.privasia.procurehere.core.entity.BudgetComment;
import com.privasia.procurehere.core.entity.Buyer;
import com.privasia.procurehere.core.entity.BuyerSettings;
import com.privasia.procurehere.core.entity.ErpSetup;
import com.privasia.procurehere.core.entity.Event;
import com.privasia.procurehere.core.entity.FavouriteSupplier;
import com.privasia.procurehere.core.entity.FinanceCompany;
import com.privasia.procurehere.core.entity.FinanceCompanySettings;
import com.privasia.procurehere.core.entity.FinanceNotificationMessage;
import com.privasia.procurehere.core.entity.FinancePo;
import com.privasia.procurehere.core.entity.NotificationMessage;
import com.privasia.procurehere.core.entity.Po;
import com.privasia.procurehere.core.entity.PoSharingBuyer;
import com.privasia.procurehere.core.entity.Pr;
import com.privasia.procurehere.core.entity.PrApproval;
import com.privasia.procurehere.core.entity.PrApprovalUser;
import com.privasia.procurehere.core.entity.PrAudit;
import com.privasia.procurehere.core.entity.PrComment;
import com.privasia.procurehere.core.entity.PrItem;
import com.privasia.procurehere.core.entity.ProductContractItems;
import com.privasia.procurehere.core.entity.RequestComment;
import com.privasia.procurehere.core.entity.RequestedAssociatedBuyer;
import com.privasia.procurehere.core.entity.RfaApprovalUser;
import com.privasia.procurehere.core.entity.RfaComment;
import com.privasia.procurehere.core.entity.RfaEvent;
import com.privasia.procurehere.core.entity.RfaEventApproval;
import com.privasia.procurehere.core.entity.RfiApprovalUser;
import com.privasia.procurehere.core.entity.RfiComment;
import com.privasia.procurehere.core.entity.RfiEvent;
import com.privasia.procurehere.core.entity.RfiEventApproval;
import com.privasia.procurehere.core.entity.RfpApprovalUser;
import com.privasia.procurehere.core.entity.RfpComment;
import com.privasia.procurehere.core.entity.RfpEvent;
import com.privasia.procurehere.core.entity.RfpEventApproval;
import com.privasia.procurehere.core.entity.RfqApprovalUser;
import com.privasia.procurehere.core.entity.RfqComment;
import com.privasia.procurehere.core.entity.RfqEvent;
import com.privasia.procurehere.core.entity.RfqEventApproval;
import com.privasia.procurehere.core.entity.RftApprovalUser;
import com.privasia.procurehere.core.entity.RftComment;
import com.privasia.procurehere.core.entity.RftEvent;
import com.privasia.procurehere.core.entity.RftEventApproval;
import com.privasia.procurehere.core.entity.RftEventAudit;
import com.privasia.procurehere.core.entity.SourcingFormApprovalRequest;
import com.privasia.procurehere.core.entity.SourcingFormApprovalUserRequest;
import com.privasia.procurehere.core.entity.SourcingFormRequest;
import com.privasia.procurehere.core.entity.Supplier;
import com.privasia.procurehere.core.entity.SupplierFormSubmition;
import com.privasia.procurehere.core.entity.SupplierFormSubmitionApproval;
import com.privasia.procurehere.core.entity.SupplierFormSubmitionApprovalUser;
import com.privasia.procurehere.core.entity.SupplierFormSubmitionComment;
import com.privasia.procurehere.core.entity.SupplierSettings;
import com.privasia.procurehere.core.entity.TransactionLog;
import com.privasia.procurehere.core.entity.User;
import com.privasia.procurehere.core.enums.ApprovalStatus;
import com.privasia.procurehere.core.enums.ApprovalType;
import com.privasia.procurehere.core.enums.AuditActionType;
import com.privasia.procurehere.core.enums.BudgetStatus;
import com.privasia.procurehere.core.enums.ErpIntegrationTypeForPr;
import com.privasia.procurehere.core.enums.EventStatus;
import com.privasia.procurehere.core.enums.FilterTypes;
import com.privasia.procurehere.core.enums.FinancePoStatus;
import com.privasia.procurehere.core.enums.FinancePoType;
import com.privasia.procurehere.core.enums.NotificationType;
import com.privasia.procurehere.core.enums.PoShare;
import com.privasia.procurehere.core.enums.PoStatus;
import com.privasia.procurehere.core.enums.PrAuditType;
import com.privasia.procurehere.core.enums.PrStatus;
import com.privasia.procurehere.core.enums.RfxTypes;
import com.privasia.procurehere.core.enums.SourcingFormStatus;
import com.privasia.procurehere.core.enums.SupplierFormApprovalStatus;
import com.privasia.procurehere.core.enums.TransactionLogStatus;
import com.privasia.procurehere.core.exceptions.ApplicationException;
import com.privasia.procurehere.core.exceptions.NotAllowedException;
import com.privasia.procurehere.core.exceptions.TechOneException;
import com.privasia.procurehere.core.pojo.AuthenticatedUser;
import com.privasia.procurehere.core.pojo.PrErp2Pojo;
import com.privasia.procurehere.core.pojo.PrErpPojo;
import com.privasia.procurehere.core.utils.CollectionUtil;
import com.privasia.procurehere.core.utils.DateUtil;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.core.utils.SecurityLibrary;
import com.privasia.procurehere.core.utils.StringUtils;
import com.privasia.procurehere.integration.PublishEventService;
import com.privasia.procurehere.integration.RequestResponseLoggingInterceptor;
import com.privasia.procurehere.integration.RestTemplateResponseErrorHandler;
import com.privasia.procurehere.service.ApprovalService;
import com.privasia.procurehere.service.BudgetService;
import com.privasia.procurehere.service.BuyerSettingsService;
import com.privasia.procurehere.service.DashboardNotificationService;
import com.privasia.procurehere.service.ErpAuditService;
import com.privasia.procurehere.service.ErpIntegrationService;
import com.privasia.procurehere.service.ErpSetupService;
import com.privasia.procurehere.service.EventAuditService;
import com.privasia.procurehere.service.FavoriteSupplierService;
import com.privasia.procurehere.service.FinanceSettingsService;
import com.privasia.procurehere.service.NotificationService;
import com.privasia.procurehere.service.PoAuditService;
import com.privasia.procurehere.service.PoFinanceService;
import com.privasia.procurehere.service.PoService;
import com.privasia.procurehere.service.PrAuditService;
import com.privasia.procurehere.service.PrService;
import com.privasia.procurehere.service.RfaEventService;
import com.privasia.procurehere.service.RfiEventService;
import com.privasia.procurehere.service.RfpEventService;
import com.privasia.procurehere.service.RfqEventService;
import com.privasia.procurehere.service.RftEventService;
import com.privasia.procurehere.service.SourcingFormRequestService;
import com.privasia.procurehere.service.SupplierSettingsService;
import com.privasia.procurehere.service.TransactionLogService;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;

/**
 * @author Parveen
 */
@Service
@Transactional(readOnly = true)
public class ApprovalServiceImpl implements ApprovalService {

	public static final Logger LOG = Logger.getLogger(ApprovalServiceImpl.class);

	@Autowired
	ErpIntegrationService erpIntegrationService;

	@Autowired
	PrService prService;

	@Autowired
	private SourcingFormRequestService requestService;
	@Autowired
	RftEventDao rftEventDao;

	@Autowired
	RfpEventDao rfpEventDao;

	@Autowired
	RfqEventDao rfqEventDao;

	@Autowired
	RfiEventDao rfiEventDao;

	@Autowired
	RfaEventDao rfaEventDao;

	@Autowired
	MessageSource messageSource;

	@Autowired
	NotificationService notificationService;

	@Autowired
	EventAuditService eventAuditService;

	@Value("${app.url}")
	String APP_URL;

	@Autowired
	PrDao prDao;

	@Autowired
	BudgetService budgetService;

	@Autowired
	TransactionLogService transactionLogService;

	@Autowired
	BudgetDao budgetDao;

	@Autowired
	RftEventService rftEventService;

	@Autowired
	RfpEventService rfpEventService;

	@Autowired
	RfiEventService rfiEventService;

	@Autowired
	RfaEventService rfaEventService;

	@Autowired
	RfqEventService rfqEventService;

	@Autowired
	SourcingFormRequestDao sourcingFormRequestDao;

	@Autowired
	SupplierSettingsService supplierSettingsService;

	@Autowired
	BuyerSettingsService buyerSettingsService;

	@Autowired
	DashboardNotificationService dashboardNotificationService;

	@Autowired
	UserDao userDao;

	@Autowired
	ErpSetupDao erpConfigDao;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	ErpAuditService erpAuditService;

	@Autowired
	PrAuditService prAuditService;

	@Autowired
	ErpSetupService erpSetupService;

	@Autowired
	PoFinanceService poFinanceService;

	@Autowired
	PoFinanceDao poFinanceDao;

	@Autowired
	FinanceSettingsService financeSettingsService;

	@Autowired
	PoSharingBuyerDao poSharingBuyerDao;

	@Autowired
	PublishEventService publishEventService;

	@Autowired
	SnapShotAuditService snapShotAuditService;

	@Autowired
	ProductContractItemsDao productContractItemsDao;

	@Autowired
	PoService poService;

	@Autowired
	PoAuditService poAuditService;

	@Autowired
	FavoriteSupplierService favoriteSupplierService;

	@Autowired
	SupplierFormSubmissionDao supplierFormSubmissionDao;

	@Override
	@Transactional(readOnly = false, noRollbackFor = TechOneException.class)
	public Pr doApproval(Pr pr, User loggedInUser, Boolean isFInish) throws TechOneException, Exception {
		try {
			String buyerTimeZone = "GMT+8:00";
			Buyer buyer = new Buyer();
			buyer.setId(loggedInUser.getTenantId());

			if (Boolean.TRUE == isFInish) {
				// Finish Audit
				try {
					PrAudit audit = new PrAudit();
					audit.setAction(PrAuditType.FINISH);
					audit.setActionBy(loggedInUser);
					audit.setActionDate(new Date());
					audit.setBuyer(buyer);
					audit.setDescription(messageSource.getMessage("pr.audit.finish", new Object[] { pr.getPrId() }, Global.LOCALE));
					audit.setPr(pr);
					prAuditService.save(audit);
				} catch (Exception e) {
					LOG.error("Error While saving Audit Trail : " + e.getMessage(), e);
				}
			}

			pr = prDao.findPrForApprovalById(pr.getId());
			List<PrApproval> approvalList = pr.getPrApprovals(); // prService.getAllPrApprovalsByPrId(pr.getId());

			// If no approval setup - direct approve the PR
			if (CollectionUtil.isEmpty(approvalList)) {

				ErpSetup erpConfig = erpConfigDao.getErpConfigBytenantId(loggedInUser.getTenantId());
				if (erpConfig != null && Boolean.TRUE == erpConfig.getIsErpEnable()) {
					LOG.info("ERP Integration is enabled.... sending PR to ERP...");
					sendingPrToErpAgent(loggedInUser, pr, erpConfig);
				} else {
					pr.setStatus(PrStatus.APPROVED);
					LOG.info("status :" + pr.getStatus());

					try {
						PrAudit audit = new PrAudit();
						audit.setAction(PrAuditType.APPROVED);
						audit.setActionBy(SecurityLibrary.getLoggedInUser());
						audit.setActionDate(new Date());
						audit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
						audit.setDescription(messageSource.getMessage("pr.audit.approve", new Object[] { pr.getPrId() }, Global.LOCALE));
						audit.setPr(pr);
						prAuditService.save(audit);
					} catch (Exception e) {
						LOG.error("Error While saving Audit Trail : " + e.getMessage(), e);
					}
					/********************** Call PR to Finance ***********************************/
					Boolean isAutoCreatePo = buyerSettingsService.isAutoCreatePoSettingsByTenantId(loggedInUser.getTenantId());

					if (erpConfig != null && Boolean.TRUE == erpConfig.getIsGeneratePo() && Boolean.TRUE == isAutoCreatePo) {
						Po po = prService.createPo(pr.getCreatedBy(), pr);
						if (po != null && StringUtils.checkString(po.getId()).length() > 0) {
							LOG.info("po created succefully:" + po.getId());
							pr.setPoNumber(po.getPoNumber());
							pr.setPoCreatedDate(new Date());
							pr.setIsPo(Boolean.TRUE);
							sendPoCreatedEmail(pr.getCreatedBy(), pr, pr.getCreatedBy());
							try {
								if (po.getStatus() == PoStatus.ORDERED) {
									po = poService.getLoadedPoById(po.getId());
									if (po.getSupplier() != null) {
										sendPoReceivedEmailNotificationToSupplier(po, po.getCreatedBy());
									}
								}
							} catch (Exception e) {
								LOG.error("Error while sending PO email notification to supplier:" + e.getMessage(), e);
							}
						}
					}
				}
				try {
					prService.generateTechOnePrFile(pr);
				} catch (TechOneException e1) {
					LOG.error("Error while generating PR file to TechOne " + e1.getMessage(), e1);
					throw e1;
				}
			} else {
				PrApproval currentLevel = null;
				if (pr.getStatus() == PrStatus.DRAFT) {
					pr.setStatus(PrStatus.PENDING);
					for (PrApproval prApproval : approvalList) {
						if (prApproval.getLevel() == 1) {
							prApproval.setActive(true);
							break;
						}
					}
				} else {
					for (PrApproval prApproval : approvalList) {
						if (prApproval.isActive()) {
							currentLevel = prApproval;
							break;
						}
					}
					boolean allUsersDone = true;
					if (currentLevel != null) {
						for (PrApprovalUser user : currentLevel.getApprovalUsers()) {
							if (ApprovalStatus.PENDING == user.getApprovalStatus()) {
								LOG.info("All users of this level have not approved the PR.");
								allUsersDone = false;
								break;
							}
						}
					}
					if (allUsersDone) {
						setNextOrAllDone(loggedInUser, approvalList, currentLevel, pr);
					}
				}
				// Just send emails to users.
				for (PrApproval prApproval : approvalList) {
					if (prApproval.isActive()) {
						for (PrApprovalUser user : prApproval.getApprovalUsers()) {
							if (user.getApprovalStatus() == ApprovalStatus.PENDING) {
								LOG.info("send mail to pending approvers ");
								buyerTimeZone = getTimeZoneByBuyerSettings(user.getUser().getTenantId(), buyerTimeZone);
								sendEmailToPrApprovers(pr, user, buyerTimeZone);

								if (Boolean.TRUE == pr.getEnableApprovalReminder()) {
									Integer reminderHr = pr.getReminderAfterHour();
									Integer reminderCpunt = pr.getReminderCount();
									if (reminderHr != null && reminderCpunt != null) {
										Calendar now = Calendar.getInstance();
										now.add(Calendar.HOUR, reminderHr);
										user.setNextReminderTime(now.getTime());
										user.setReminderCount(reminderCpunt);
									}
								}
							}
						}
					}
				}
			}
			pr.setSummaryCompleted(true);
			pr.setPrCreatedDate(new Date());
			pr = prDao.update(pr);
		} catch (TechOneException e) {
			LOG.info("ERROR While Approving Pr :" + e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			LOG.info("ERROR While Approving Pr :" + e.getMessage(), e);
			throw new Exception("ERROR While Approving Pr :" + e.getMessage());
		}
		return pr;
	}

	private PrErpPojo constructPrErpObject(Pr pr) {
		PrErpPojo prErpPojo = new PrErpPojo(pr);
		return prErpPojo;
	}

	@SuppressWarnings("unused")
	private String generatePo(Pr pr) {
		String poNumber = null;
		if (pr.getPrId().startsWith("PR")) {
			poNumber = pr.getPrId().replaceAll("PR", "PO");
		} else {
			poNumber = "PO" + pr.getPrId();
		}
		return poNumber;
	}

	@Override
	@Transactional(readOnly = false, noRollbackFor = TechOneException.class)
	public Pr doApproval(Pr pr, User actionBy, String remarks, boolean approved) throws TechOneException, Exception {

		pr = prDao.findPrForApprovalById(pr.getId());
		List<PrApproval> approvalList = pr.getPrApprovals(); // prService.getAllPrApprovalsByPrId(pr.getId());

		// Identify Current Approval Level
		PrApproval currentLevel = null;
		for (PrApproval prApproval : approvalList) {
			if (prApproval.isActive()) {
				currentLevel = prApproval;
				LOG.info("Current Approval Level : " + currentLevel.getLevel());
				break;
			}
		}

		// Identify actionUser in the ApprovalUser of current level
		PrApprovalUser actionUser = null;
		if (currentLevel != null) {
			for (PrApprovalUser user : currentLevel.getApprovalUsers()) {
				if (user.getUser().getId().equals(actionBy.getId())) {
					actionUser = user;
					LOG.info("Approval being done by : " + actionBy.getLoginId());
				}
			}
		}
		if (actionUser == null) {
			// throw error
			LOG.error("User " + actionBy.getName() + " is not allowed to Approve or Reject PR '" + pr.getName() + "' at approval level : " + currentLevel.getLevel());
			throw new NotAllowedException("User " + actionBy.getName() + " is not allowed to Approve or Reject PR '" + pr.getName() + "' at approval level : " + currentLevel.getLevel());
		}

		if (actionUser.getApprovalStatus() != ApprovalStatus.PENDING) {
			// throw error
			LOG.error("User " + actionBy.getName() + " has already " + actionUser.getApprovalStatus() + " PR at : " + actionUser.getActionDate());
			throw new NotAllowedException("User " + actionBy.getName() + " has already " + actionUser.getApprovalStatus() + " PR at : " + actionUser.getActionDate());
		}

		// adding remarks into comments
		if (pr.getPrComments() == null) {
			pr.setPrComments(new ArrayList<PrComment>());
		}
		PrComment prComment = new PrComment();
		prComment.setComment(remarks);
		prComment.setApproved(approved);
		prComment.setCreatedBy(actionBy);
		prComment.setCreatedDate(new Date());
		prComment.setPr(pr);
		prComment.setApprovalUserId(actionUser.getId());
		pr.getPrComments().add(prComment);

		// If rejected
		if (!approved) {

			// Reset all approvals for re-approval as the PR is rejected.
			for (PrApproval prApproval : approvalList) {
				prApproval.setDone(false);
				prApproval.setActive(false);
				for (PrApprovalUser user : prApproval.getApprovalUsers()) {
					try {
						// Send rejection email to all approver users
						// if (user.getApprovalStatus() == ApprovalStatus.PENDING) {
						sendPrRejectionEmail(user.getUser(), pr, actionBy, remarks);
						// }
					} catch (Exception e) {
						LOG.info("ERROR while Sending PR reject mail :" + e.getMessage(), e);
					}
					user.setActionDate(null);
					user.setApprovalStatus(ApprovalStatus.PENDING);
					user.setRemarks(null);
					user.setActionDate(null);
				}
			}

			// actionUser.setApprovalStatus(ApprovalStatus.REJECTED);
			actionUser.setActionDate(new Date());
			actionUser.setRemarks(remarks);
			LOG.info("User " + actionBy.getName() + " has Rejected the PR : " + pr.getName());
			pr.setStatus(PrStatus.DRAFT);
			try {
				if (pr.getCreatedBy() != null) {
					sendPrRejectionEmail(pr.getCreatedBy(), pr, actionBy, remarks);
				}

				Pr prForBudget = prService.findPrBUAndCCForBudgetById(pr.getId());
				if (Boolean.TRUE == prForBudget.getLockBudget()) {
					// updateBudget when PR rejected
					Budget budget = budgetService.findBudgetByBusinessUnitAndCostCenter(prForBudget.getBusinessUnit().getId(), prForBudget.getCostCenter().getId());
					// convert PR amount if currency is different
					BigDecimal prAfterConversion = null;

					if (budget != null) {
						// create new transaction of budget
						TransactionLog transactionLog = new TransactionLog();
						if (null != pr.getConversionRate() && !(0 == pr.getConversionRate().compareTo(BigDecimal.ZERO))) {
							prAfterConversion = prForBudget.getGrandTotal().multiply(pr.getConversionRate());
							LOG.info("**************************prAfterConversion " + prAfterConversion);
							transactionLog.setConversionRateAmount(prForBudget.getConversionRate());
							transactionLog.setAfterConversionAmount(prAfterConversion);

						}
						// if budget locking enabled
						if (prForBudget.getLockBudget()) {
							budget.setLockedAmount(budget.getLockedAmount().subtract(prAfterConversion != null ? prAfterConversion : pr.getGrandTotal()));
						} else {
							budget.setPendingAmount(budget.getPendingAmount().subtract(prAfterConversion != null ? prAfterConversion : pr.getGrandTotal()));
						}
						budget.setRemainingAmount(budget.getRemainingAmount().add(prAfterConversion != null ? prAfterConversion : pr.getGrandTotal()));

						transactionLog.setBudget(budget);
						transactionLog.setReferanceNumber(budget.getBudgetId());
						transactionLog.setTenantId(SecurityLibrary.getLoggedInUserTenantId());
						transactionLog.setTransactionTimeStamp(new Date());
						transactionLog.setAddAmount(prAfterConversion != null ? prAfterConversion : pr.getGrandTotal());
						transactionLog.setPrBaseCurrency(prForBudget.getCurrency().getCurrencyCode());
						transactionLog.setTransactionLogStatus(TransactionLogStatus.RELEASE);
						transactionLog.setRemainingAmount(budget.getRemainingAmount());
						transactionLogService.saveTransactionLog(transactionLog);
						budgetService.updateBudget(budget);
					}
				}

			} catch (Exception e) {
				LOG.info("ERROR while Sending mail :" + e.getMessage(), e);
			}

		} else {
			LOG.info("User " + actionBy.getName() + " has Approved the PR : " + pr.getName());
			actionUser.setApprovalStatus(ApprovalStatus.APPROVED);
			actionUser.setActionDate(new Date());
			actionUser.setRemarks(remarks);

			// Send email notification to Creator
			sendPrApprovalEmail(pr.getCreatedBy(), pr, actionBy, remarks);

			// Send email notification to Creator
			sendPrApprovalEmail(actionBy, pr, actionBy, remarks);

			if (ApprovalType.OR == currentLevel.getApprovalType()) {
				LOG.info("This level has OR set for approval. Marking level as done");
				try {
					PrAudit audit = new PrAudit();
					audit.setAction(PrAuditType.APPROVED);
					audit.setActionBy(SecurityLibrary.getLoggedInUser());
					audit.setActionDate(new Date());
					audit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
					audit.setDescription(messageSource.getMessage("pr.audit.approve", new Object[] { pr.getPrId() }, Global.LOCALE));
					audit.setPr(pr);
					prAuditService.save(audit);
				} catch (Exception e) {
					LOG.error("Error While saving Audit Trail : " + e.getMessage(), e);
				}
				setNextOrAllDone(actionBy, approvalList, currentLevel, pr);
				// pr.setPrApprovals(approvalList);

			} else {
				// AND Operation
				LOG.info("This level has AND set for approvals");
				boolean allUsersDone = true;
				if (currentLevel != null) {
					for (PrApprovalUser user : currentLevel.getApprovalUsers()) {
						if (ApprovalStatus.PENDING == user.getApprovalStatus() || ApprovalStatus.REJECTED == user.getApprovalStatus()) {
							LOG.info("All users of this level have not approved the PR.");
							allUsersDone = false;
							break;
						}
					}
				}

				try {
					PrAudit audit = new PrAudit();
					audit.setAction(PrAuditType.APPROVED);
					audit.setActionBy(SecurityLibrary.getLoggedInUser());
					audit.setActionDate(new Date());
					audit.setBuyer(SecurityLibrary.getLoggedInUser().getBuyer());
					audit.setDescription(messageSource.getMessage("pr.audit.approve", new Object[] { pr.getPrId() }, Global.LOCALE));
					audit.setPr(pr);
					prAuditService.save(audit);
				} catch (Exception e) {
					LOG.error("Error While saving Audit Trail : " + e.getMessage(), e);
				}
				if (allUsersDone) {
					LOG.info("All users of this level have approved the PR.");
					setNextOrAllDone(actionBy, approvalList, currentLevel, pr);
				}
			}
		}
		// pr.setPrApprovals(approvalList);
		return prDao.update(pr);
	}

	/**
	 * @param pr
	 * @param actionBy
	 * @param remarks
	 */
	private void sendPrRejectionEmail(User mailTo, Pr pr, User actionBy, String remarks) {
		LOG.info("Sending rejected request email to (" + mailTo.getName() + ") : " + mailTo.getCommunicationEmail());
		String url = APP_URL + "/buyer/prView/" + pr.getId();
		String subject = "PR Rejected";
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("userName", mailTo.getName());
		map.put("actionByName", actionBy.getName());
		map.put("pr", pr);
		map.put("remarks", StringUtils.checkString(remarks));
		map.put("businessUnit", StringUtils.checkString(getBusinessUnitname(pr.getId())));
		map.put("prReferanceNumber", StringUtils.checkString(pr.getReferenceNumber()));
		if (mailTo.getId().equals(actionBy.getId())) {
			map.put("message", "You have Rejected");
		} else {
			map.put("message", actionBy.getName() + " has Rejected");
		}
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		String timeZone = "GMT+8:00";
		timeZone = getTimeZoneByBuyerSettings(mailTo.getTenantId(), timeZone);
		df.setTimeZone(TimeZone.getTimeZone(timeZone));
		map.put("date", df.format(new Date()));
		map.put("loginUrl", APP_URL + "/login");
		map.put("appUrl", url);

		if (StringUtils.checkString(mailTo.getCommunicationEmail()).length() > 0) {
			sendEmail(mailTo.getCommunicationEmail(), subject, map, Global.PR_REJECT_TEMPLATE);
		} else {
			LOG.warn("No communication email configured for user : " + mailTo.getLoginId() + "... Not going to send email notification");
		}

		String notificationMessage = messageSource.getMessage("pr.rejection.notification.message", new Object[] { actionBy.getName(), pr.getName(), remarks }, Global.LOCALE);
		sendDashboardNotification(mailTo, url, subject, notificationMessage, NotificationType.REJECT_MESSAGE);

		if (StringUtils.checkString(mailTo.getDeviceId()).length() > 0) {
			try {
				LOG.info("User '" + mailTo.getCommunicationEmail() + "' and device Id :" + mailTo.getDeviceId());
				Map<String, String> payload = new HashMap<String, String>();
				payload.put("id", pr.getId());
				payload.put("messageType", NotificationType.REJECT_MESSAGE.toString());
				payload.put("eventType", FilterTypes.PR.toString());
				notificationService.pushOneSignalNotification(notificationMessage, null, payload, Arrays.asList(mailTo.getDeviceId()));
			} catch (Exception e) {
				LOG.error("Error While sending PR reject Mobile push notification to '" + mailTo.getCommunicationEmail() + "' : " + e.getMessage(), e);
			}
		} else {
			LOG.info("User '" + mailTo.getCommunicationEmail() + "' Device Id is Null");
		}
	}

	/**
	 * @param pr
	 * @param actionBy
	 * @param remarks
	 */
	private void sendPrApprovalEmail(User mailTo, Pr pr, User actionBy, String remarks) {
		LOG.info("Sending Approval email to (" + mailTo.getName() + ") : " + mailTo.getCommunicationEmail());
		String subject = "PR Approved";
		String url = APP_URL + "/buyer/prView/" + pr.getId();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("userName", mailTo.getName());
		map.put("actionByName", actionBy.getName());
		map.put("pr", pr);
		map.put("remarks", StringUtils.checkString(remarks));
		map.put("businessUnit", StringUtils.checkString(getBusinessUnitname(pr.getId())));
		map.put("prReferanceNumber", StringUtils.checkString(pr.getReferenceNumber()));
		if (mailTo.getId().equals(actionBy.getId())) {
			map.put("message", "You have Approved");
		} else {
			map.put("message", actionBy.getName() + " has Approved");
		}
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		String timeZone = "GMT+8:00";
		timeZone = getTimeZoneByBuyerSettings(mailTo.getTenantId(), timeZone);
		df.setTimeZone(TimeZone.getTimeZone(timeZone));
		map.put("date", df.format(new Date()));
		map.put("loginUrl", APP_URL + "/login");
		map.put("appUrl", url);

		if (StringUtils.checkString(mailTo.getCommunicationEmail()).length() > 0) {
			sendEmail(mailTo.getCommunicationEmail(), subject, map, Global.PR_APPROVAL_TEMPLATE);
		} else {
			LOG.warn("No communication email configured for user : " + mailTo.getLoginId() + "... Not going to send email notification");
		}

		String notificationMessage = messageSource.getMessage("pr.approval.notification.message", new Object[] { actionBy.getName(), pr.getName() }, Global.LOCALE);
		sendDashboardNotification(mailTo, url, subject, notificationMessage, NotificationType.APPROVAL_MESSAGE);

		if (StringUtils.checkString(mailTo.getDeviceId()).length() > 0) {
			try {
				LOG.info("User '" + mailTo.getCommunicationEmail() + "' and device Id :" + mailTo.getDeviceId());
				Map<String, String> payload = new HashMap<String, String>();
				payload.put("id", pr.getId());
				payload.put("messageType", NotificationType.APPROVAL_MESSAGE.toString());
				payload.put("eventType", FilterTypes.PR.toString());
				notificationService.pushOneSignalNotification(notificationMessage, null, payload, Arrays.asList(mailTo.getDeviceId()));
			} catch (Exception e) {
				LOG.error("Error While sending Approval Mobile push notification to '" + mailTo.getCommunicationEmail() + "' : " + e.getMessage(), e);
			}
		} else {
			LOG.info("User '" + mailTo.getCommunicationEmail() + "' Device Id is Null");
		}
	}

	/**
	 * @param pr
	 * @param user
	 */
	public void sendEmailToPrApprovers(Pr pr, PrApprovalUser user, String buyerTimeZone) {
		String mailTo = user.getUser().getCommunicationEmail();
		String subject = "PR Approval request";
		String url = APP_URL + "/buyer/prView/" + pr.getId();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("userName", user.getUser().getName());
		map.put("pr", pr);
		map.put("businessUnit", StringUtils.checkString(getBusinessUnitname(pr.getId())));
		map.put("prReferanceNumber", (pr.getReferenceNumber() == null ? "" : pr.getReferenceNumber()));
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		df.setTimeZone(TimeZone.getTimeZone(buyerTimeZone));
		map.put("date", df.format(new Date()));
		map.put("loginUrl", APP_URL + "/login");
		map.put("appUrl", url);
		if (StringUtils.checkString(mailTo).length() > 0) {
			sendEmail(mailTo, subject, map, Global.PR_APPROVAL_REQUEST_TEMPLATE);
		} else {
			LOG.warn("No communication email configured for user : " + user.getUser().getLoginId() + "... Not going to send email notification");
		}

		String notificationMessage = messageSource.getMessage("pr.approval.request.notification.message", new Object[] { pr.getName() }, Global.LOCALE);
		sendDashboardNotification(user.getUser(), url, subject, notificationMessage, NotificationType.APPROVAL_MESSAGE);
		if (StringUtils.checkString(user.getUser().getDeviceId()).length() > 0) {
			try {
				LOG.info("User '" + mailTo + "' and device Id :" + user.getUser().getDeviceId());
				Map<String, String> payload = new HashMap<String, String>();
				payload.put("id", pr.getId());
				payload.put("messageType", NotificationType.APPROVAL_MESSAGE.toString());
				payload.put("eventType", FilterTypes.PR.toString());
				notificationService.pushOneSignalNotification(notificationMessage, null, payload, Arrays.asList(user.getUser().getDeviceId()));
			} catch (Exception e) {
				LOG.error("Error While sending Approval Mobile push notification to '" + mailTo + "' : " + e.getMessage(), e);
			}
		} else {
			LOG.info("User '" + mailTo + "' Device Id is Null");
		}
	}

	private String getBusinessUnitname(String prId) {
		String displayName = null;
		displayName = prDao.getBusineessUnitname(prId);
		return StringUtils.checkString(displayName);
	}

	/**
	 * @param mailTo
	 * @param pr
	 */
	private void sendPoCreatedEmail(User mailTo, Pr pr, User actionBy) {
		LOG.info("Sending PO created email to (" + mailTo.getName() + ") : " + mailTo.getCommunicationEmail());
		try {
			String subject = "PO Created";
			String url = APP_URL + "/buyer/prView/" + pr.getId();
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("userName", mailTo.getName());
			map.put("message", "You have Created");
			map.put("pr", pr);
			map.put("buyerName", actionBy.getName());
			map.put("buyerLoginEmail", actionBy.getLoginId());
			map.put("businessUnit", StringUtils.checkString(getBusinessUnitname(pr.getId())));
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
			String timeZone = "GMT+8:00";
			timeZone = getTimeZoneByBuyerSettings(mailTo.getTenantId(), timeZone);
			df.setTimeZone(TimeZone.getTimeZone(timeZone));
			sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
			map.put("date", df.format(new Date()));
			map.put("pocreatedDate", sdf.format(new Date()));
			map.put("loginUrl", APP_URL + "/login");
			map.put("appUrl", url);

			if (StringUtils.checkString(mailTo.getCommunicationEmail()).length() > 0) {
				sendEmail(mailTo.getCommunicationEmail(), subject, map, Global.PO_CREATED_TEMPLATE);
			} else {
				LOG.warn("No communication email configured for user : " + mailTo.getLoginId() + "... Not going to send email notification");
			}

			String notificationMessage = messageSource.getMessage("po.create.notification.message", new Object[] { pr.getName() }, Global.LOCALE);
			sendDashboardNotification(mailTo, url, subject, notificationMessage, NotificationType.CREATED_MESSAGE);
			// prService.sendPrFinishMailToSupplier(pr);

		} catch (Exception e) {
			LOG.error("Error while Sending PO Created :" + e.getMessage(), e);
		}

	}

	private void sendDashboardNotification(User messageTo, String url, String subject, String notificationMessage, NotificationType notificationType) {
		NotificationMessage message = new NotificationMessage();
		message.setCreatedBy(null);
		message.setCreatedDate(new Date());
		message.setMessage(notificationMessage);
		message.setNotificationType(notificationType);
		message.setMessageTo(messageTo);
		message.setSubject(subject);
		message.setTenantId(messageTo.getTenantId());
		message.setUrl(url);
		dashboardNotificationService.save(message);
	}

	private void sendDashboardNotificationForFinance(User messageTo, String url, String subject, String notificationMessage, NotificationType notificationType) {
		FinanceNotificationMessage message = new FinanceNotificationMessage();
		message.setCreatedBy(null);
		message.setCreatedDate(new Date());
		message.setMessage(notificationMessage);
		message.setNotificationType(notificationType);
		message.setMessageTo(messageTo);
		message.setSubject(subject);
		message.setTenantId(messageTo.getTenantId());
		message.setUrl(url);
		dashboardNotificationService.saveFinanceNotification(message);
	}

	/**
	 * @param actionBy
	 * @param approvalList
	 * @param currentLevel
	 * @param pr
	 * @throws Exception
	 */
	public void setNextOrAllDone(User actionBy, List<PrApproval> approvalList, PrApproval currentLevel, Pr pr) throws TechOneException, Exception {
		String buyerTimeZone = "GMT+8:00";
		currentLevel.setDone(true);
		currentLevel.setActive(false); // Check if all approvals are done
		if (currentLevel.getLevel() == approvalList.size()) {
			// all approvals done
			if (Boolean.TRUE == pr.getLockBudget()) {
				if (null != pr.getBusinessUnit() && null != pr.getCostCenter()) {
					// update Budget Amount
					Budget budget = budgetService.findBudgetByBusinessUnitAndCostCenter(pr.getBusinessUnit().getId(), pr.getCostCenter().getId());
					// convert PR amount if currency is different
					BigDecimal prAfterConversion = null;
					// conversionRate not equal to zero
					if (null != pr.getConversionRate() && !(0 == pr.getConversionRate().compareTo(BigDecimal.ZERO))) {
						prAfterConversion = pr.getGrandTotal().multiply(pr.getConversionRate());
						LOG.info("**************************prAfterConversion " + prAfterConversion);
					}
					// if budget locking enabled
					if (budget != null) {
						if (pr.getLockBudget()) {
							budget.setLockedAmount(budget.getLockedAmount().subtract(prAfterConversion != null ? prAfterConversion : pr.getGrandTotal()));
							budget.setApprovedAmount(budget.getApprovedAmount().add(prAfterConversion != null ? prAfterConversion : pr.getGrandTotal()));
						} else {
							budget.setPendingAmount(budget.getPendingAmount().subtract(prAfterConversion != null ? prAfterConversion : pr.getGrandTotal()));
							budget.setApprovedAmount(budget.getApprovedAmount().add(prAfterConversion != null ? prAfterConversion : pr.getGrandTotal()));
						}
						budgetService.updateBudget(budget);
					}
				}
			}
			LOG.info("All approvals for this PR is done!!!. Going to Approved Mode.");

			ErpSetup erpConfig = erpConfigDao.getErpConfigBytenantId(actionBy.getTenantId());
			if (erpConfig != null && Boolean.TRUE == erpConfig.getIsErpEnable()) {
				LOG.info("ERP Integration is enabled.... sending PR to ERP...");
				sendingPrToErpAgent(actionBy, pr, erpConfig);
			} else {

				pr.setStatus(PrStatus.APPROVED);
				pr.setActionBy(actionBy);
				pr.setActionDate(new Date());

				Boolean isAutoCreatePo = buyerSettingsService.isAutoCreatePoSettingsByTenantId(actionBy.getTenantId());

				if (erpConfig != null && Boolean.TRUE == erpConfig.getIsGeneratePo() && Boolean.TRUE == isAutoCreatePo) {
					Po po = prService.createPo(pr.getCreatedBy(), pr);
					if (po != null && StringUtils.checkString(po.getId()).length() > 0) {
						pr.setPoNumber(po.getPoNumber());
						pr.setPoCreatedDate(new Date());
						pr.setIsPo(Boolean.TRUE);
						sendPoCreatedEmailToCreater(pr.getCreatedBy(), pr, pr.getCreatedBy());
						try {
							if (po.getStatus() == PoStatus.ORDERED) {
								po = poService.getLoadedPoById(po.getId());
								if (po.getSupplier() != null) {
									sendPoReceivedEmailNotificationToSupplier(po, po.getCreatedBy());
								}
							}
						} catch (Exception e) {
							LOG.error("Error while sending PO email notification to supplier:" + e.getMessage(), e);
						}
					}
				}
			}
			try {
				prService.generateTechOnePrFile(pr);
			} catch (TechOneException e1) {
				LOG.error("Error while generating PR file to TechOne " + e1.getMessage(), e1);
				throw e1;
			}
		} else {
			for (PrApproval prApproval : approvalList) {
				if (prApproval.getLevel() == currentLevel.getLevel() + 1) {
					LOG.info("Setting Approval level " + prApproval.getLevel() + " as Active level");
					prApproval.setActive(true);
					for (PrApprovalUser nextLevelUser : prApproval.getApprovalUsers()) {
						buyerTimeZone = getTimeZoneByBuyerSettings(nextLevelUser.getUser().getTenantId(), buyerTimeZone);
						sendEmailToPrApprovers(pr, nextLevelUser, buyerTimeZone);
						if (Boolean.TRUE == pr.getEnableApprovalReminder()) {
							Integer reminderHr = pr.getReminderAfterHour();
							Integer reminderCpunt = pr.getReminderCount();
							if (reminderHr != null && reminderCpunt != null) {
								Calendar now = Calendar.getInstance();
								now.add(Calendar.HOUR, reminderHr);
								nextLevelUser.setNextReminderTime(now.getTime());
								nextLevelUser.setReminderCount(reminderCpunt);
							}
						}
					}
					break;
				}
			}
		}
	}

	/**
	 * @param actionBy
	 * @param pr
	 * @param erpConfig
	 * @throws Exception
	 */
	void sendingPrToErpAgent(User actionBy, Pr pr, ErpSetup erpConfig) throws Exception {

		// sending PR to ERP
		try {

			ClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());
			restTemplate = new RestTemplate(factory);
			restTemplate.setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
			restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());

			HttpHeaders headers = new HttpHeaders();

			ObjectMapper mapperObj = new ObjectMapper();
			String payload = "";
			String responseMsg = "";
			// If SAP PR Interface type == FGV else Westports
			if (ErpIntegrationTypeForPr.TYPE_2 == erpConfig.getErpIntegrationTypeForPr()) {

				LOG.info("Its a type 2 interface for PR");

				// Fetch the contract reference numbers and storage location...
				if (pr.getTemplate() != null && Boolean.TRUE == pr.getTemplate().getContractItemsOnly()) {
					for (PrItem item : pr.getPrItems()) {
						if (item.getProduct() != null) {
							ProductContractItems pcItem = productContractItemsDao.findProductContractItemByItemId(item.getProduct().getId(), item.getProductContractItem() != null ? item.getProductContractItem().getId() : null);
							if (pcItem != null) {
								item.setStorageLocation(pcItem.getStorageLocation());
								item.setItemContractReferenceNumber(pcItem.getContractItemNumber());
								item.setContractReferenceNumber(pcItem.getProductContract().getContractReferenceNumber());
								item.setCostCenter(pcItem.getCostCenter() != null ? pcItem.getCostCenter().getCostCenter() : null);
								// calculate balance quantity
								pcItem.setBalanceQuantity(pcItem.getBalanceQuantity().subtract(item.getQuantity()));
								item.setPurchaseGroup(pcItem.getProductContract().getGroupCode());
								productContractItemsDao.update(pcItem);
							}
						}
					}
				}

				if (StringUtils.checkString(erpConfig.getErpUsername()).length() > 0 && StringUtils.checkString(erpConfig.getErpPassword()).length() > 0) {
					String auth = StringUtils.checkString(erpConfig.getErpUsername()) + ":" + StringUtils.checkString(erpConfig.getErpPassword());
					byte[] encodedAuth = Base64.encode(auth.getBytes(Charset.forName("US-ASCII")));
					String authHeader = "Basic " + new String(encodedAuth);
					headers.set("Authorization", authHeader);
				}

				PrErp2Pojo prErp2Pojo = new PrErp2Pojo(pr);
				payload = mapperObj.writeValueAsString(prErp2Pojo);
				LOG.info("jsonObject  :" + payload);

				HttpEntity<PrErp2Pojo> request = new HttpEntity<PrErp2Pojo>(prErp2Pojo, headers);
				try {
					responseMsg = restTemplate.postForObject(erpConfig.getErpUrl() + "/PRCreate/", request, String.class);
				} catch (HttpClientErrorException | HttpServerErrorException ex) {
					responseMsg = ex.getMessage();
					LOG.error("Error received from ERP : " + ex.getMessage() + " Status Text : " + ex.getStatusText());
					LOG.error("Response Body : " + ex.getResponseBodyAsString());
					throw new ApplicationException("Error received from ERP : " + ex.getMessage() + " Status Text : " + ex.getStatusText(), ex);
				}

			} else {
				LOG.info("Its a type 1 interface for PR");

				PrErpPojo prErpPojo = constructPrErpObject(pr);
				payload = mapperObj.writeValueAsString(prErpPojo);
				LOG.info("jsonObject  :" + payload);
				if (StringUtils.checkString(prErpPojo.getVendorCode()).length() == 0 && pr.getSupplier() != null) {
					throw new ApplicationException("Vendor Code not Assigned to Supplier");
				}
				String erpSeqNo = erpSetupService.genrateSquanceNumber();
				prErpPojo.setErpSeqNo(erpSeqNo);

				headers.set(Global.X_AUTH_KEY_HEADER_PROPERTY, erpConfig.getAppId());
				HttpEntity<PrErpPojo> request = new HttpEntity<PrErpPojo>(prErpPojo, headers);

				try {
					responseMsg = restTemplate.postForObject(erpConfig.getErpUrl() + "/PrSendData", request, String.class);
				} catch (HttpClientErrorException | HttpServerErrorException ex) {
					responseMsg = ex.getMessage();
					LOG.error("Error received from ERP : " + ex.getMessage() + " Status Text : " + ex.getStatusText());
					LOG.error("Response Body : " + ex.getResponseBodyAsString());
					throw new ApplicationException("Error received from ERP : " + ex.getMessage() + " Status Text : " + ex.getStatusText(), ex);
				}
			}

			// String responseMsg = restTemplate.postForObject(erpConfig.getErpUrl() + "/PrSendData", prErpPojo,
			// String.class);

			LOG.info("response :" + responseMsg);
			pr.setStatus(PrStatus.TRANSFERRED);
			pr.setErpPrTransferred(Boolean.TRUE);
			pr.setIsFinalApproved(Boolean.TRUE);
			// Storing audit history
			try {
				PrAudit audit = new PrAudit();
				audit.setAction(PrAuditType.TRANSFERRED);
				audit.setActionBy(actionBy);
				audit.setActionDate(new Date());
				audit.setBuyer(actionBy.getBuyer());
				audit.setDescription("Sucessfully transferred to ERP. Response : " + StringUtils.checkString(responseMsg));
				audit.setPr(pr);
				prAuditService.save(audit);
			} catch (Exception e) {
				LOG.error("Error while saving ERP Audit History :" + e.getMessage(), e);
			}
		} catch (Exception e) {
			LOG.error("Error while sending PR to ERP :" + e.getMessage(), e);
			pr.setIsFinalApproved(Boolean.TRUE);
			pr.setStatus(PrStatus.APPROVED);
			pr.setErpPrTransferred(Boolean.FALSE);
			try {
				if (pr.getCreatedBy() != null) {
					sendPrErrorEmail(pr.getCreatedBy(), pr, e.getMessage());
				}
			} catch (Exception ee) {
				LOG.info("ERROR while Sending mail :" + ee.getMessage(), ee);
			}

			// Storing audit history for error
			try {
				PrAudit audit = new PrAudit();
				audit.setAction(PrAuditType.ERROR);
				audit.setActionBy(actionBy);
				audit.setActionDate(new Date());
				audit.setBuyer(actionBy.getBuyer());
				audit.setDescription(e.getMessage());
				audit.setPr(pr);
				prAuditService.save(audit);
			} catch (Exception error) {
				LOG.error("Error while saving ERP Audit History in catch block :" + error.getMessage(), error);
			}

			throw new Exception("Error while sending PR to ERP :" + e.getMessage());
		}
	}

	@Override
	@Transactional(readOnly = false)
	public RftEvent doApproval(RftEvent event, HttpSession session, User loggedInUser, JRSwapFileVirtualizer virtualizer) throws Exception {

		try {
			event = rftEventService.loadRftEventById(event.getId());
			List<RftEventApproval> approvalList = event.getApprovals(); // rftEventDao.getAllApprovalsForEvent(event.getId());
			if (CollectionUtil.isEmpty(approvalList)) {
				event.setStatus(EventStatus.APPROVED);
				LOG.info("status :" + event.getStatus());
				try {
					LOG.info("publishing rft event to to epiportal if approval level is empty ");
					publishEventService.pushRftEvent(event.getId(), loggedInUser.getBuyer().getId(), true);
				} catch (Exception e) {
					LOG.error("Error while publishing RFT event to EPortal:" + e.getMessage(), e);
				}
				JasperPrint eventSummary = rftEventService.getEvaluationSummaryPdf(event, loggedInUser, (String) session.getAttribute(Global.SESSION_TIME_ZONE_KEY), virtualizer);
				byte[] summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
				RftEventAudit audit = new RftEventAudit(loggedInUser.getBuyer(), event, loggedInUser, new Date(), AuditActionType.Approve, messageSource.getMessage("event.audit.approved", new Object[] { event.getEventName() }, Global.LOCALE), summarySnapshot);
				eventAuditService.save(audit);
			} else {
				if (event.getStatus() == EventStatus.DRAFT) {
					event.setStatus(EventStatus.PENDING);
					for (RftEventApproval approval : approvalList) {
						if (approval.getLevel() == 1) {
							approval.setActive(true);
							break;
						}
					}
				} else {
					RftEventApproval currentLevel = null;
					for (RftEventApproval approval : approvalList) {
						if (approval.isActive()) {
							currentLevel = approval;
							break;
						}
					}
					boolean allUsersDone = true;
					if (currentLevel != null) {
						for (RftApprovalUser user : currentLevel.getApprovalUsers()) {
							if (ApprovalStatus.PENDING == user.getApprovalStatus()) {
								LOG.info("All users of this level have not approved the Event.");
								allUsersDone = false;
								break;
							}
						}
					}
					if (allUsersDone) {
						setNextOrAllDone(null, approvalList, currentLevel, event, session, loggedInUser);
					}
				}
				// Just send emails to users.
				for (RftEventApproval approval : approvalList) {
					if (approval.isActive()) {
						for (RftApprovalUser user : approval.getApprovalUsers()) {
							if (user.getApprovalStatus() == ApprovalStatus.PENDING) {
								LOG.info("send mail to pending approvers ");
								sendRfxApprovalEmails(event, user, RfxTypes.RFT);

								if (Boolean.TRUE == event.getEnableApprovalReminder()) {
									Integer reminderHr = event.getReminderAfterHour();
									Integer reminderCpunt = event.getReminderCount();
									if (reminderHr != null && reminderCpunt != null) {
										Calendar now = Calendar.getInstance();
										now.add(Calendar.HOUR, reminderHr);
										user.setNextReminderTime(now.getTime());
										user.setReminderCount(reminderCpunt);
									}
								}
							}
						}
					}
				}
			}
			event = rftEventDao.update(event);
		} catch (Exception e) {
			LOG.info("ERROR While Approving RFT :" + e.getMessage(), e);
			throw new Exception("ERROR While Approving RFT :" + e.getMessage());
		}
		return event;
	}

	@Override
	@Transactional(readOnly = false)
	public RftEvent doApproval(RftEvent event, User actionBy, String remarks, boolean approved, HttpSession session, JRSwapFileVirtualizer virtualizer) throws NotAllowedException {
		event = rftEventService.loadRftEventById(event.getId());
		List<RftEventApproval> approvalList = event.getApprovals(); // rftEventDao.getAllApprovalsForEvent(event.getId());

		// Identify Current Approval Level
		RftEventApproval currentLevel = null;
		for (RftEventApproval approval : approvalList) {
			if (approval.isActive()) {
				currentLevel = approval;
				LOG.info("Current Approval Level : " + currentLevel.getLevel());
				break;
			}
		}

		// Identify actionUser in the ApprovalUser of current level
		RftApprovalUser actionUser = null;
		if (currentLevel != null) {
			for (RftApprovalUser user : currentLevel.getApprovalUsers()) {
				if (user.getUser().getId().equals(actionBy.getId())) {
					actionUser = user;
					LOG.info("Approval being done by : " + actionBy.getLoginId());
				}
			}
		}
		if (actionUser == null) {
			// throw error
			LOG.error("User " + actionBy.getName() + " is not allowed to Approve or Reject RFT '" + event.getEventName() + "' at approval level : " + currentLevel.getLevel());
			throw new NotAllowedException("User " + actionBy.getName() + " is not allowed to Approve or Reject RFT '" + event.getEventName() + "' at approval level : " + currentLevel.getLevel());
		}

		if (actionUser.getApprovalStatus() != ApprovalStatus.PENDING) {
			// throw error
			LOG.error("User " + actionBy.getName() + " has already " + actionUser.getApprovalStatus() + " RFT at : " + actionUser.getActionDate());
			throw new NotAllowedException("User " + actionBy.getName() + " has already " + actionUser.getApprovalStatus() + " RFT at : " + actionUser.getActionDate());
		}

		// adding remarks into comments
		if (event.getComment() == null) {
			event.setComment(new ArrayList<RftComment>());
		}
		RftComment comment = new RftComment();
		comment.setComment(remarks);
		comment.setApproved(approved);
		comment.setCreatedBy(actionBy);
		comment.setCreatedDate(new Date());
		comment.setRfxEvent(event);
		comment.setApprovalUserId(actionUser.getId());
		event.getComment().add(comment);

		// If rejected
		if (!approved) {
			// For OR level Rejection should be handled differently
			// commented on user requirement don't want to hold if any user is rejecting

			// if (currentLevel.getApprovalType() == ApprovalType.OR) {
			// boolean pending = false;
			// if (currentLevel != null) {
			// for (RftApprovalUser user : currentLevel.getApprovalUsers()) {
			// // Check if any other user is pending his approval at this OR level other than actionBy user
			// if (!user.getId().equals(actionUser.getId())) {
			// if (user.getApprovalStatus() == ApprovalStatus.PENDING) {
			// pending = true;
			// break;
			// }
			// }
			// }
			// }
			//
			// if (pending) {
			// // Dont reject yet as there are more people waiting for approval in this level
			// actionUser.setApprovalStatus(ApprovalStatus.REJECTED);
			// actionUser.setActionDate(new Date());
			// actionUser.setRemarks(remarks);
			// return rftEventDao.update(event);
			// }
			// }

			// Reset all approvals for re-approval as the Event is rejected.
			for (RftEventApproval approval : approvalList) {
				approval.setDone(false);
				approval.setActive(false);
				for (RftApprovalUser user : approval.getApprovalUsers()) {
					// Send rejection email to all approverss
					// if (user.getApprovalStatus() == ApprovalStatus.PENDING) {
					sentRfxRejectionEmail(user.getUser(), event.getId(), event.getEventName(), actionBy, remarks, event.getCreatedBy(), RfxTypes.RFT, event.getReferanceNumber());
					// }
					user.setActionDate(null);
					user.setApprovalStatus(ApprovalStatus.PENDING);
					user.setRemarks(null);
					user.setActionDate(null);
				}
			}

			// actionUser.setApprovalStatus(ApprovalStatus.REJECTED);
			actionUser.setActionDate(new Date());
			actionUser.setRemarks(remarks);
			LOG.info("User " + actionBy.getName() + " has Rejected the RFT : " + event.getEventName());
			event.setStatus(EventStatus.DRAFT);

			// byte[] summarySnapshot = null;
			// try {
			// JasperPrint eventSummary = rftEventService.getEvaluationSummaryPdf(event, actionBy, (String)
			// session.getAttribute(Global.SESSION_TIME_ZONE_KEY));
			// summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
			// } catch (JRException e) {
			// LOG.error("Error while Store Audit PDF as byte : " + e.getMessage(), e);
			// }
			//
			// RftEventAudit audit = new RftEventAudit(actionBy.getBuyer(), event, actionBy, new Date(),
			// AuditActionType.Reject, messageSource.getMessage("event.audit.rejected", new Object[] {
			// event.getEventName() }, Global.LOCALE), summarySnapshot);
			// eventAuditService.save(audit);
			snapShotAuditService.doRftAudit(event, session, event, actionBy, AuditActionType.Reject, "event.audit.rejected", virtualizer);
			try {
				if (event.getCreatedBy() != null) {
					sentRfxRejectionEmail(event.getCreatedBy(), event.getId(), event.getEventName(), actionBy, remarks, event.getCreatedBy(), RfxTypes.RFT, event.getReferanceNumber());
				}
			} catch (Exception e) {
				LOG.info("ERROR while Sending mail :" + e.getMessage(), e);
			}

		} else {
			LOG.info("User " + actionBy.getName() + " has Approved the RFT : " + event.getEventName());
			actionUser.setApprovalStatus(ApprovalStatus.APPROVED);
			actionUser.setActionDate(new Date());
			actionUser.setRemarks(remarks);

			// Send email notification to Creator
			sentRfxApprovalEmail(event.getCreatedBy(), event.getId(), event.getEventName(), actionBy, remarks, event.getCreatedBy(), RfxTypes.RFT, false, event.getReferanceNumber());

			// Send email notification to actionBy
			sentRfxApprovalEmail(actionBy, event.getId(), event.getEventName(), actionBy, remarks, event.getCreatedBy(), RfxTypes.RFT, true, event.getReferanceNumber());

			if (ApprovalType.OR == currentLevel.getApprovalType()) {
				LOG.info("This level has OR set for approval. Marking level as done");
				setNextOrAllDone(actionBy, approvalList, currentLevel, event, session, actionBy);
			} else {
				// AND Operation
				LOG.info("This level has AND set for approvals");
				boolean allUsersDone = true;
				if (currentLevel != null) {
					for (RftApprovalUser user : currentLevel.getApprovalUsers()) {
						if (ApprovalStatus.PENDING == user.getApprovalStatus() || ApprovalStatus.REJECTED == user.getApprovalStatus()) {
							allUsersDone = false;
							LOG.info("All users of this level have not approved the RFT.");
							break;
						}
					}
				}
				if (allUsersDone) {
					LOG.info("All users of this level have approved the RFT.");
					setNextOrAllDone(actionBy, approvalList, currentLevel, event, session, actionBy);
				}
			}
		}
		// event.setApprovals(approvalList);
		return rftEventDao.update(event);
	}

	/**
	 * @param eventId
	 * @param eventName
	 * @param actionBy
	 * @param remarks
	 * @param ownerUser
	 * @param type
	 */
	private void sentRfxRejectionEmail(User mailTo, String eventId, String eventName, User actionBy, String remarks, User ownerUser, RfxTypes type, String referanceNumber) {
		LOG.info("Sending rejected request email to (" + mailTo.getName() + ") : " + mailTo.getCommunicationEmail());

		String subject = "Event Rejected";
		String url = APP_URL + "/buyer/" + type.name() + "/viewSummary/" + eventId;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("userName", mailTo.getName());
		map.put("actionByName", actionBy.getName());
		map.put("eventName", eventName);
		map.put("eventType", type.name());
		map.put("remarks", remarks);
		map.put("referanceNumber", referanceNumber);
		map.put("businessUnit", StringUtils.checkString(findBusinessUnit(eventId, type)));
		if (mailTo.getId().equals(actionBy.getId())) {
			map.put("message", "You have Rejected");
		} else {
			map.put("message", actionBy.getName() + " has Rejected");
		}
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		String timeZone = "GMT+8:00";
		timeZone = getTimeZoneByBuyerSettings(mailTo.getTenantId(), timeZone);
		df.setTimeZone(TimeZone.getTimeZone(timeZone));
		map.put("date", df.format(new Date()));
		map.put("appUrl", url);
		map.put("loginUrl", APP_URL + "/login");
		if (StringUtils.checkString(mailTo.getCommunicationEmail()).length() > 0) {
			sendEmail(mailTo.getCommunicationEmail(), subject, map, Global.EVENT_REJECT_TEMPLATE);
		} else {
			LOG.warn("No communication email configured for user : " + mailTo.getLoginId() + "... Not going to send email notification");
		}

		String notificationMessage = messageSource.getMessage("event.rejection.notification.message", new Object[] { actionBy.getName(), type.name(), eventName, remarks }, Global.LOCALE);
		sendDashboardNotification(mailTo, url, subject, notificationMessage, NotificationType.REJECT_MESSAGE);

		if (StringUtils.checkString(mailTo.getDeviceId()).length() > 0) {
			try {
				LOG.info("User '" + mailTo.getCommunicationEmail() + "' and device Id :" + mailTo.getDeviceId());
				Map<String, String> payload = new HashMap<String, String>();
				payload.put("id", eventId);
				payload.put("messageType", NotificationType.REJECT_MESSAGE.toString());
				payload.put("eventType", type.name());
				notificationService.pushOneSignalNotification(notificationMessage, null, payload, Arrays.asList(mailTo.getDeviceId()));
			} catch (Exception e) {
				LOG.error("Error While sending Event reject Mobile push notification to '" + mailTo.getCommunicationEmail() + "' : " + e.getMessage(), e);
			}
		} else {
			LOG.info("User '" + mailTo.getCommunicationEmail() + "' Device Id is Null");
		}
	}

	/**
	 * @param eventId
	 * @param eventName
	 * @param actionBy
	 * @param remarks
	 * @param ownerUser
	 * @param type
	 * @param self
	 */
	private void sentRfxApprovalEmail(User mailTo, String eventId, String eventName, User actionBy, String remarks, User ownerUser, RfxTypes type, boolean self, String referenceNumber) {
		LOG.info("Sending approval email to (" + mailTo.getName() + ") : " + mailTo.getCommunicationEmail());

		String subject = "Event Approved";
		String url = APP_URL + "/buyer/" + type.name() + "/eventSummary/" + eventId;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("userName", mailTo.getName());
		map.put("actionByName", actionBy.getName());
		map.put("eventName", eventName);
		map.put("eventType", type.name());
		map.put("remarks", remarks);
		map.put("referenceNumber", referenceNumber);
		map.put("businessUnit", StringUtils.checkString(findBusinessUnit(eventId, type)));
		if (self) {
			map.put("message", "You have Approved");
		} else {
			map.put("message", actionBy.getName() + " has Approved");
		}
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		String timeZone = "GMT+8:00";
		timeZone = getTimeZoneByBuyerSettings(mailTo.getTenantId(), timeZone);
		df.setTimeZone(TimeZone.getTimeZone(timeZone));
		map.put("date", df.format(new Date()));
		map.put("appUrl", url);
		map.put("loginUrl", APP_URL + "/login");
		if (StringUtils.checkString(mailTo.getCommunicationEmail()).length() > 0) {
			sendEmail(mailTo.getCommunicationEmail(), subject, map, Global.EVENT_APPROVAL_TEMPLATE);
		} else {
			LOG.warn("No communication email configured for user : " + mailTo.getLoginId() + "... Not going to send email notification");
		}

		String notificationMessage = messageSource.getMessage("event.approval.notification.message", new Object[] { actionBy.getName(), type.name(), eventName, remarks }, Global.LOCALE);
		sendDashboardNotification(mailTo, url, subject, notificationMessage, NotificationType.APPROVAL_MESSAGE);

		if (StringUtils.checkString(mailTo.getDeviceId()).length() > 0) {
			try {
				LOG.info("User '" + mailTo.getCommunicationEmail() + "' and device Id :" + mailTo.getDeviceId());
				Map<String, String> payload = new HashMap<String, String>();
				payload.put("id", eventId);
				payload.put("messageType", NotificationType.APPROVAL_MESSAGE.toString());
				payload.put("eventType", type.name());
				notificationService.pushOneSignalNotification(notificationMessage, null, payload, Arrays.asList(mailTo.getDeviceId()));
			} catch (Exception e) {
				LOG.error("Error While sending Event approve Mobile push notification to '" + mailTo.getCommunicationEmail() + "' : " + e.getMessage(), e);
			}
		} else {
			LOG.info("User '" + mailTo.getCommunicationEmail() + "' Device Id is Null");
		}
	}

	/**
	 * @param actionBy
	 * @param approvalList
	 * @param currentLevel
	 * @param event
	 * @param loggedInUser
	 */
	private void setNextOrAllDone(User actionBy, List<RftEventApproval> approvalList, RftEventApproval currentLevel, RftEvent event, HttpSession session, User loggedInUser) {
		// Check if all approvals are done
		currentLevel.setDone(true);
		currentLevel.setActive(false); // Check if all approvals are done
		if (currentLevel.getLevel() == approvalList.size()) {
			// all approvals done
			LOG.info("All approvals for this RFT is done!!!. Going to Approved Mode.");
			event.setStatus(EventStatus.APPROVED);
			event.setActionBy(actionBy);
			event.setActionDate(new Date());

			// JasperPrint eventSummary = rftEventService.getEvaluationSummaryPdf(event, loggedInUser, (String)
			// session.getAttribute(Global.SESSION_TIME_ZONE_KEY));
			// byte[] summarySnapshot = null;
			// try {
			// summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
			// } catch (JRException e) {
			// LOG.error("Error while Store summary PDF as byte : " + e.getMessage(), e);
			// }
			// RftEventAudit audit = new RftEventAudit(actionBy.getBuyer(), event, actionBy, new Date(),
			// AuditActionType.Approve, messageSource.getMessage("event.audit.approved", new Object[] {
			// event.getEventName() }, Global.LOCALE), summarySnapshot);
			// eventAuditService.save(audit);

			// snapShotAuditService.doRftAudit(event, session, event, actionBy, AuditActionType.Approve,
			// "event.audit.approved");
			LOG.info("All approvals for this RFT is in Approved Mode.");

			try {
				LOG.info("publishing rft event to epiportal");
				publishEventService.pushRftEvent(event.getId(), actionBy.getBuyer().getId(), true);
			} catch (Exception e) {
				LOG.error("Error while publishing RFT event to EPortal:" + e.getMessage(), e);
			}

		} else {
			for (RftEventApproval approval : approvalList) {
				if (approval.getLevel() == currentLevel.getLevel() + 1) {
					LOG.info("Setting Approval level " + approval.getLevel() + " as Active level");
					approval.setActive(true);
					for (RftApprovalUser nextLevelUser : approval.getApprovalUsers()) {
						sendRfxApprovalEmails(event, nextLevelUser, RfxTypes.RFT);
						if (Boolean.TRUE == event.getEnableApprovalReminder()) {
							Integer reminderHr = event.getReminderAfterHour();
							Integer reminderCpunt = event.getReminderCount();
							if (reminderHr != null && reminderCpunt != null) {
								Calendar now = Calendar.getInstance();
								now.add(Calendar.HOUR, reminderHr);
								nextLevelUser.setNextReminderTime(now.getTime());
								nextLevelUser.setReminderCount(reminderCpunt);
							}
						}
					}
					break;
				}
			}
		}
	}

	@Override
	@Transactional(readOnly = false)
	public RfpEvent doApproval(RfpEvent event, HttpSession session, User loggedInUser, JRSwapFileVirtualizer virtualizer) throws Exception {
		try {
			event = rfpEventService.loadRfpEventById(event.getId());
			List<RfpEventApproval> approvalList = event.getApprovals(); // rfpEventDao.getAllApprovalsForEvent(event.getId());
			if (CollectionUtil.isEmpty(approvalList)) {
				event.setStatus(EventStatus.APPROVED);
				LOG.info("status :" + event.getStatus());

				// byte[] summarySnapshot = null;
				// try {
				// JasperPrint eventSummary = rfpEventService.getEvaluationSummaryPdf(event, loggedInUser, (String)
				// session.getAttribute(Global.SESSION_TIME_ZONE_KEY));
				// summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
				// } catch (JRException e) {
				// LOG.error("Error while Store summary PDF as byte : " + e.getMessage(), e);
				// }
				// RfpEventAudit audit = new RfpEventAudit(loggedInUser.getBuyer(), event, loggedInUser, new Date(),
				// AuditActionType.Approve, messageSource.getMessage("event.audit.approved", new Object[] {
				// event.getEventName() }, Global.LOCALE), summarySnapshot);
				// eventAuditService.save(audit);

				snapShotAuditService.doRfpAudit(event, session, event, loggedInUser, AuditActionType.Approve, "event.audit.approved", virtualizer);

			} else {
				if (event.getStatus() == EventStatus.DRAFT) {
					event.setStatus(EventStatus.PENDING);
					for (RfpEventApproval approval : approvalList) {
						if (approval.getLevel() == 1) {
							approval.setActive(true);
							break;
						}
					}
				} else {
					RfpEventApproval currentLevel = null;
					for (RfpEventApproval approval : approvalList) {
						if (approval.isActive()) {
							currentLevel = approval;
							break;
						}
					}
					boolean allUsersDone = true;
					if (currentLevel != null) {
						for (RfpApprovalUser user : currentLevel.getApprovalUsers()) {
							if (ApprovalStatus.PENDING == user.getApprovalStatus()) {
								LOG.info("All users of this level have not approved the Event.");
								allUsersDone = false;
								break;
							}
						}
					}
					if (allUsersDone) {
						setNextOrAllDone(null, approvalList, currentLevel, event, session, loggedInUser);
					}
				}
				// Just send emails to users.
				for (RfpEventApproval approval : approvalList) {
					if (approval.isActive()) {
						for (RfpApprovalUser user : approval.getApprovalUsers()) {
							if (user.getApprovalStatus() == ApprovalStatus.PENDING) {
								LOG.info("send mail to pending approvers ");
								sendRfxApprovalEmails(event, user, RfxTypes.RFP);

								if (Boolean.TRUE == event.getEnableApprovalReminder()) {
									Integer reminderHr = event.getReminderAfterHour();
									Integer reminderCpunt = event.getReminderCount();
									if (reminderHr != null && reminderCpunt != null) {
										Calendar now = Calendar.getInstance();
										now.add(Calendar.HOUR, reminderHr);
										user.setNextReminderTime(now.getTime());
										user.setReminderCount(reminderCpunt);
									}
								}

							}
						}
					}
				}
			}
			event = rfpEventDao.update(event);
		} catch (Exception e) {
			LOG.info("ERROR While Approving RFP :" + e.getMessage(), e);
			throw new Exception("ERROR While Approving RFP :" + e.getMessage());
		}
		return event;
	}

	@Override
	@Transactional(readOnly = false)
	public RfpEvent doApproval(RfpEvent event, User actionBy, String remarks, boolean approved, HttpSession session, JRSwapFileVirtualizer virtualizer) throws NotAllowedException {

		event = rfpEventService.loadRfpEventById(event.getId());
		List<RfpEventApproval> approvalList = event.getApprovals(); // rfpEventDao.getAllApprovalsForEvent(event.getId());

		// Identify Current Approval Level
		RfpEventApproval currentLevel = null;
		for (RfpEventApproval approval : approvalList) {
			if (approval.isActive()) {
				currentLevel = approval;
				LOG.info("Current Approval Level : " + currentLevel.getLevel());
				break;
			}
		}

		// Identify actionUser in the ApprovalUser of current level
		RfpApprovalUser actionUser = null;
		if (currentLevel != null) {
			for (RfpApprovalUser user : currentLevel.getApprovalUsers()) {
				if (user.getUser().getId().equals(actionBy.getId())) {
					actionUser = user;
					LOG.info("Approval being done by : " + actionBy.getLoginId());
				}
			}
		}
		if (actionUser == null) {
			// throw error
			LOG.error("User " + actionBy.getName() + " is not allowed to Approve or Reject RFP '" + event.getEventName() + "' at approval level : " + currentLevel.getLevel());
			throw new NotAllowedException("User " + actionBy.getName() + " is not allowed to Approve or Reject RFP '" + event.getEventName() + "' at approval level : " + currentLevel.getLevel());
		}

		if (actionUser.getApprovalStatus() != ApprovalStatus.PENDING) {
			// throw error
			LOG.error("User " + actionBy.getName() + " has already " + actionUser.getApprovalStatus() + " RFP at : " + actionUser.getActionDate());
			throw new NotAllowedException("User " + actionBy.getName() + " has already " + actionUser.getApprovalStatus() + " RFP at : " + actionUser.getActionDate());
		}

		// adding remarks into comments
		if (event.getComment() == null) {
			event.setComment(new ArrayList<RfpComment>());
		}
		RfpComment comment = new RfpComment();
		comment.setComment(remarks);
		comment.setApproved(approved);
		comment.setCreatedBy(actionBy);
		comment.setCreatedDate(new Date());
		comment.setRfxEvent(event);
		comment.setApprovalUserId(actionUser.getId());
		event.getComment().add(comment);

		// If rejected
		if (!approved) {
			// For OR level Rejection should be handled differently
			// commented on user requirement don't want to hold if any user is rejecting

			// if (currentLevel.getApprovalType() == ApprovalType.OR) {
			// boolean pending = false;
			// if (currentLevel != null) {
			// for (RfpApprovalUser user : currentLevel.getApprovalUsers()) {
			// // Check if any other user is pending his approval at this OR level other than actionBy user
			// if (!user.getId().equals(actionUser.getId())) {
			// if (user.getApprovalStatus() == ApprovalStatus.PENDING) {
			// pending = true;
			// break;
			// }
			// }
			// }
			// }
			// if (pending) {
			// // Dont reject yet as there are more people waiting for approval in this level
			// actionUser.setApprovalStatus(ApprovalStatus.REJECTED);
			// actionUser.setActionDate(new Date());
			// actionUser.setRemarks(remarks);
			// return rfpEventDao.update(event);
			// }
			// }

			// Reset all approvals for re-approval as the Event is rejected.
			for (RfpEventApproval approval : approvalList) {
				approval.setDone(false);
				approval.setActive(false);
				for (RfpApprovalUser user : approval.getApprovalUsers()) {
					// Send rejection email to all approvers
					// if (user.getApprovalStatus() == ApprovalStatus.PENDING) {
					sentRfxRejectionEmail(user.getUser(), event.getId(), event.getEventName(), actionBy, remarks, event.getCreatedBy(), RfxTypes.RFP, event.getReferanceNumber());
					// }
					user.setActionDate(null);
					user.setApprovalStatus(ApprovalStatus.PENDING);
					user.setRemarks(null);
					user.setActionDate(null);
				}
			}
			// actionUser.setApprovalStatus(ApprovalStatus.REJECTED);
			actionUser.setActionDate(new Date());
			actionUser.setRemarks(remarks);

			LOG.info("User " + actionBy.getName() + " has Rejected the RFP : " + event.getEventName());
			event.setStatus(EventStatus.DRAFT);
			// rftEventDao.update(event);

			// byte[] summarySnapshot = null;
			// try {
			// JasperPrint eventSummary = rfpEventService.getEvaluationSummaryPdf(event, actionBy, (String)
			// session.getAttribute(Global.SESSION_TIME_ZONE_KEY));
			// summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
			// } catch (JRException e) {
			// LOG.error("Error while Store summary PDF as byte : " + e.getMessage(), e);
			// }
			//
			// RfpEventAudit audit = new RfpEventAudit(actionBy.getBuyer(), event, actionBy, new Date(),
			// AuditActionType.Reject, messageSource.getMessage("event.audit.rejected", new Object[] {
			// event.getEventName() }, Global.LOCALE), summarySnapshot);
			// eventAuditService.save(audit);
			snapShotAuditService.doRfpAudit(event, session, event, actionBy, AuditActionType.Reject, "event.audit.rejected", virtualizer);
			try {
				if (event.getCreatedBy() != null) {
					LOG.info("Sending rejected request email to owner : " + event.getCreatedBy().getCommunicationEmail());
					sentRfxRejectionEmail(event.getCreatedBy(), event.getId(), event.getEventName(), actionBy, remarks, event.getCreatedBy(), RfxTypes.RFP, event.getReferanceNumber());
				}
			} catch (Exception e) {
				LOG.info("ERROR while Sending mail :" + e.getMessage(), e);
			}
		} else {
			LOG.info("User " + actionBy.getName() + " has Approved the RFP : " + event.getEventName());
			actionUser.setApprovalStatus(ApprovalStatus.APPROVED);
			actionUser.setActionDate(new Date());
			actionUser.setRemarks(remarks);

			// Send email notification to Creator
			sentRfxApprovalEmail(event.getCreatedBy(), event.getId(), event.getEventName(), actionBy, remarks, event.getCreatedBy(), RfxTypes.RFP, false, event.getReferanceNumber());

			// Send email notification to actionBy
			sentRfxApprovalEmail(actionBy, event.getId(), event.getEventName(), actionBy, remarks, event.getCreatedBy(), RfxTypes.RFP, true, event.getReferanceNumber());

			if (ApprovalType.OR == currentLevel.getApprovalType()) {
				LOG.info("This level has OR set for approval. Marking level as done");
				setNextOrAllDone(actionBy, approvalList, currentLevel, event, session, actionBy);
			} else {
				// AND Operation
				LOG.info("This level has AND set for approvals");
				boolean allUsersDone = true;
				if (currentLevel != null) {
					for (RfpApprovalUser user : currentLevel.getApprovalUsers()) {
						if (ApprovalStatus.PENDING == user.getApprovalStatus() || ApprovalStatus.REJECTED == user.getApprovalStatus()) {
							allUsersDone = false;
							LOG.info("All users of this level have not approved the RFP.");
							break;
						}
					}
				}
				if (allUsersDone) {
					LOG.info("All users of this level have approved the RFP.");
					setNextOrAllDone(actionBy, approvalList, currentLevel, event, session, actionBy);
				}
			}
		}
		// event.setApprovals(approvalList);
		return rfpEventDao.update(event);
	}

	/**
	 * @param actionBy
	 * @param approvalList
	 * @param currentLevel
	 * @param event
	 * @param loggedInUser
	 */
	private void setNextOrAllDone(User actionBy, List<RfpEventApproval> approvalList, RfpEventApproval currentLevel, RfpEvent event, HttpSession session, User loggedInUser) {
		currentLevel.setDone(true);
		currentLevel.setActive(false); // Check if all approvals are done
		// Check if all approvals are done
		if (currentLevel.getLevel() == approvalList.size()) {
			// all approvals done
			LOG.info("All approvals for this RFP is done!!!. Going to Approved Mode.");
			event.setStatus(EventStatus.APPROVED);
			event.setActionBy(actionBy);
			event.setActionDate(new Date());

			// byte[] summarySnapshot = null;
			// try {
			// JasperPrint eventSummary = rfpEventService.getEvaluationSummaryPdf(event, loggedInUser, (String)
			// session.getAttribute(Global.SESSION_TIME_ZONE_KEY));
			// summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
			// } catch (JRException e) {
			// LOG.error("Error while Store summary PDF as byte : " + e.getMessage(), e);
			// }
			// RfpEventAudit audit = new RfpEventAudit(actionBy.getBuyer(), event, actionBy, new Date(),
			// AuditActionType.Approve, messageSource.getMessage("event.audit.approved", new Object[] {
			// event.getEventName() }, Global.LOCALE), summarySnapshot);
			// eventAuditService.save(audit);
			// snapShotAuditService.doRfpAudit(event, session, event, actionBy, AuditActionType.Approve,
			// "event.audit.approved");
		} else {
			for (RfpEventApproval approval : approvalList) {
				if (approval.getLevel() == currentLevel.getLevel() + 1) {
					LOG.info("Setting Approval level " + approval.getLevel() + " as Active level");
					approval.setActive(true);
					for (RfpApprovalUser nextLevelUser : approval.getApprovalUsers()) {
						sendRfxApprovalEmails(event, nextLevelUser, RfxTypes.RFP);
						if (Boolean.TRUE == event.getEnableApprovalReminder()) {
							Integer reminderHr = event.getReminderAfterHour();
							Integer reminderCpunt = event.getReminderCount();
							if (reminderHr != null && reminderCpunt != null) {
								Calendar now = Calendar.getInstance();
								now.add(Calendar.HOUR, reminderHr);
								nextLevelUser.setNextReminderTime(now.getTime());
								nextLevelUser.setReminderCount(reminderCpunt);
							}
						}
					}
					break;
				}
			}
		}
	}

	@Override
	@Transactional(readOnly = false)
	public RfqEvent doApproval(RfqEvent event, HttpSession session, User loggedInUser, JRSwapFileVirtualizer virtualizer) throws Exception {
		try {
			event = rfqEventService.loadRfqEventById(event.getId());
			List<RfqEventApproval> approvalList = event.getApprovals(); // rfqEventDao.getAllApprovalsForEvent(event.getId());
			if (CollectionUtil.isEmpty(approvalList)) {
				event.setStatus(EventStatus.APPROVED);
				LOG.info("status :" + event.getStatus());
				try {
					LOG.info("publishing rfq event to to epiportal if approval level is empty ");
					publishEventService.pushRfqEvent(event.getId(), loggedInUser.getBuyer().getId(), true);
				} catch (Exception e) {
					LOG.error("Error while publishing RFQ event to EPortal:" + e.getMessage(), e);
				}
				snapShotAuditService.doRfqAudit(event, session, event, loggedInUser, AuditActionType.Approve, "Event.is.Approved", virtualizer);

			} else {
				if (event.getStatus() == EventStatus.DRAFT) {
					event.setStatus(EventStatus.PENDING);
					for (RfqEventApproval approval : approvalList) {
						if (approval.getLevel() == 1) {
							approval.setActive(true);
							break;
						}
					}
				} else {
					RfqEventApproval currentLevel = null;
					for (RfqEventApproval approval : approvalList) {
						if (approval.isActive()) {
							currentLevel = approval;
							break;
						}
					}
					boolean allUsersDone = true;
					if (currentLevel != null) {
						for (RfqApprovalUser user : currentLevel.getApprovalUsers()) {
							if (ApprovalStatus.PENDING == user.getApprovalStatus()) {
								LOG.info("All users of this level have not approved the Event.");
								allUsersDone = false;
								break;
							}
						}
					}
					if (allUsersDone) {
						setNextOrAllDone(null, approvalList, currentLevel, event, session, loggedInUser);
					}
				}
				// Just send emails to users.
				if (CollectionUtil.isNotEmpty(approvalList)) {
					for (RfqEventApproval approval : approvalList) {
						if (approval.isActive()) {
							for (RfqApprovalUser user : approval.getApprovalUsers()) {
								if (user.getApprovalStatus() == ApprovalStatus.PENDING) {
									LOG.info("send mail to pending approvers ");
									sendRfxApprovalEmails(event, user, RfxTypes.RFQ);

									if (Boolean.TRUE == event.getEnableApprovalReminder()) {
										Integer reminderHr = event.getReminderAfterHour();
										Integer reminderCpunt = event.getReminderCount();
										if (reminderHr != null && reminderCpunt != null) {
											Calendar now = Calendar.getInstance();
											now.add(Calendar.HOUR, reminderHr);
											user.setNextReminderTime(now.getTime());
											user.setReminderCount(reminderCpunt);
										}
									}

								}
							}
						}
					}
				}
			}
			event = rfqEventDao.update(event);
		} catch (Exception e) {
			LOG.info("ERROR While Approving RFQ :" + e.getMessage(), e);
			throw new Exception("ERROR While Approving RFQ :" + e.getMessage());
		}
		return event;
	}

	@Override
	@Transactional(readOnly = false)
	public RfqEvent doApproval(RfqEvent event, User actionBy, String remarks, boolean approved, HttpSession session, JRSwapFileVirtualizer virtualizer) throws NotAllowedException {

		event = rfqEventService.loadRfqEventById(event.getId());
		List<RfqEventApproval> approvalList = event.getApprovals(); // rfqEventDao.getAllApprovalsForEvent(event.getId());

		// Identify Current Approval Level
		RfqEventApproval currentLevel = null;
		for (RfqEventApproval approval : approvalList) {
			if (approval.isActive()) {
				currentLevel = approval;
				LOG.info("Current Approval Level : " + currentLevel.getLevel());
				break;
			}
		}

		// Identify actionUser in the ApprovalUser of current level
		RfqApprovalUser actionUser = null;
		if (currentLevel != null) {
			for (RfqApprovalUser user : currentLevel.getApprovalUsers()) {
				if (user.getUser().getId().equals(actionBy.getId())) {
					actionUser = user;
					LOG.info("Approval being done by : " + actionBy.getLoginId());
				}
			}
		}
		if (actionUser == null) {
			// throw error
			LOG.error("User " + actionBy.getName() + " is not allowed to Approve or Reject RFQ '" + event.getEventName() + "' at approval level : " + currentLevel.getLevel());
			throw new NotAllowedException("User " + actionBy.getName() + " is not allowed to Approve or Reject RFQ '" + event.getEventName() + "' at approval level : " + currentLevel.getLevel());
		}

		if (actionUser.getApprovalStatus() != ApprovalStatus.PENDING) {
			// throw error
			LOG.error("User " + actionBy.getName() + " has already " + actionUser.getApprovalStatus() + " RFQ at : " + actionUser.getActionDate());
			throw new NotAllowedException("User " + actionBy.getName() + " has already " + actionUser.getApprovalStatus() + " RFQ at : " + actionUser.getActionDate());
		}

		// adding remarks into comments
		if (event.getComment() == null) {
			event.setComment(new ArrayList<RfqComment>());
		}
		RfqComment comment = new RfqComment();
		comment.setComment(remarks);
		comment.setApproved(approved);
		comment.setCreatedBy(actionBy);
		comment.setCreatedDate(new Date());
		comment.setRfxEvent(event);
		comment.setApprovalUserId(actionUser.getId());
		event.getComment().add(comment);

		// If rejected
		if (!approved) {
			// For OR level Rejection should be handled differently
			// commented on user requirement don't want to hold if any user is rejecting

			// if (currentLevel.getApprovalType() == ApprovalType.OR) {
			// boolean pending = false;
			// if (currentLevel != null) {
			// for (RfqApprovalUser user : currentLevel.getApprovalUsers()) {
			// // Check if any other user is pending his approval at this OR level other than actionBy user
			// if (!user.getId().equals(actionUser.getId())) {
			// if (user.getApprovalStatus() == ApprovalStatus.PENDING) {
			// pending = true;
			// break;
			// }
			// }
			// }
			// }
			// if (pending) {
			// // Dont reject yet as there are more people waiting for approval in this level
			// actionUser.setApprovalStatus(ApprovalStatus.REJECTED);
			// actionUser.setActionDate(new Date());
			// actionUser.setRemarks(remarks);
			// return rfqEventDao.update(event);
			// }
			// }

			// Reset all approvals for re-approval as the Event is rejected.
			for (RfqEventApproval approval : approvalList) {
				approval.setDone(false);
				approval.setActive(false);
				for (RfqApprovalUser user : approval.getApprovalUsers()) {
					// Send rejection email to all approvers
					// if (user.getApprovalStatus() == ApprovalStatus.PENDING) {
					sentRfxRejectionEmail(user.getUser(), event.getId(), event.getEventName(), actionBy, remarks, event.getCreatedBy(), RfxTypes.RFQ, event.getReferanceNumber());
					// }
					user.setActionDate(null);
					user.setApprovalStatus(ApprovalStatus.PENDING);
					user.setRemarks(null);
					user.setActionDate(null);
				}
			}
			// actionUser.setApprovalStatus(ApprovalStatus.REJECTED);
			actionUser.setActionDate(new Date());
			actionUser.setRemarks(remarks);

			LOG.info("User " + actionBy.getName() + " has Rejected the RFQ : " + event.getEventName());
			event.setStatus(EventStatus.DRAFT);
			// rftEventDao.update(event);
			// byte[] summarySnapshot = null;
			// try {
			// JasperPrint eventSummary = rfqEventService.getEvaluationSummaryPdf(event, actionBy, (String)
			// session.getAttribute(Global.SESSION_TIME_ZONE_KEY));
			// summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
			// } catch (JRException e) {
			// LOG.error("Error while Store summary PDF as byte : " + e.getMessage(), e);
			// }
			// RfqEventAudit audit = new RfqEventAudit(actionBy.getBuyer(), event, actionBy, new Date(),
			// AuditActionType.Reject, messageSource.getMessage("event.audit.rejected", new Object[] {
			// event.getEventName() }, Global.LOCALE), summarySnapshot);
			// eventAuditService.save(audit);

			snapShotAuditService.doRfqAudit(event, session, event, actionBy, AuditActionType.Reject, "event.audit.rejected", virtualizer);
			try {
				if (event.getCreatedBy() != null) {
					LOG.info("Sending rejected request email to owner : " + event.getCreatedBy().getCommunicationEmail());
					sentRfxRejectionEmail(event.getCreatedBy(), event.getId(), event.getEventName(), actionBy, remarks, event.getCreatedBy(), RfxTypes.RFQ, event.getReferanceNumber());
				}
			} catch (Exception e) {
				LOG.info("ERROR while Sending mail :" + e.getMessage(), e);
			}

		} else {
			LOG.info("User " + actionBy.getName() + " has Approved the RFQ : " + event.getEventName());
			actionUser.setApprovalStatus(ApprovalStatus.APPROVED);
			actionUser.setActionDate(new Date());
			actionUser.setRemarks(remarks);

			// Send email notification to Creator
			sentRfxApprovalEmail(event.getCreatedBy(), event.getId(), event.getEventName(), actionBy, remarks, event.getCreatedBy(), RfxTypes.RFQ, false, event.getReferanceNumber());

			// Send email notification to actionBy
			sentRfxApprovalEmail(actionBy, event.getId(), event.getEventName(), actionBy, remarks, event.getCreatedBy(), RfxTypes.RFQ, true, event.getReferanceNumber());

			if (ApprovalType.OR == currentLevel.getApprovalType()) {
				LOG.info("This level has OR set for approval. Marking level as done");
				setNextOrAllDone(actionBy, approvalList, currentLevel, event, session, actionBy);
			} else {
				// AND Operation
				LOG.info("This level has AND set for approvals");
				boolean allUsersDone = true;
				if (currentLevel != null) {
					for (RfqApprovalUser user : currentLevel.getApprovalUsers()) {
						if (ApprovalStatus.PENDING == user.getApprovalStatus() || ApprovalStatus.REJECTED == user.getApprovalStatus()) {
							allUsersDone = false;
							LOG.info("All users of this level have not approved the RFQ.");
							break;
						}
					}
				}
				if (allUsersDone) {
					LOG.info("All users of this level have approved the RFQ.");
					setNextOrAllDone(actionBy, approvalList, currentLevel, event, session, actionBy);
				}
			}
		}
		// event.setApprovals(approvalList);
		return rfqEventDao.update(event);
	}

	/**
	 * @param actionBy
	 * @param approvalList
	 * @param currentLevel
	 * @param event
	 * @param loggedInUser
	 */
	private void setNextOrAllDone(User actionBy, List<RfqEventApproval> approvalList, RfqEventApproval currentLevel, RfqEvent event, HttpSession session, User loggedInUser) {
		currentLevel.setDone(true);
		currentLevel.setActive(false); // Check if all approvals are done
		// Check if all approvals are done
		if (currentLevel.getLevel() == approvalList.size()) {
			// all approvals done
			LOG.info("All approvals for this RFQ is done!!!. Going to Approved Mode.");
			event.setStatus(EventStatus.APPROVED);
			event.setActionBy(actionBy);
			event.setActionDate(new Date());

			// byte[] summarySnapshot = null;
			// try {
			// JasperPrint eventSummary = rfqEventService.getEvaluationSummaryPdf(event, loggedInUser, (String)
			// session.getAttribute(Global.SESSION_TIME_ZONE_KEY));
			// summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
			// } catch (JRException e) {
			// LOG.error("Error while Store summary PDF as byte : " + e.getMessage(), e);
			// }
			// RfqEventAudit audit = new RfqEventAudit(actionBy.getBuyer(), event, actionBy, new Date(),
			// AuditActionType.Approve, messageSource.getMessage("event.audit.approved", new Object[] {
			// event.getEventName() }, Global.LOCALE), summarySnapshot);
			// eventAuditService.save(audit);

			// snapShotAuditService.doRfqAudit(event, session, event, actionBy, AuditActionType.Approve,
			// "event.audit.approved");
			LOG.info("All approvals for this RFQ is in Approved Mode.");
			try {
				LOG.info("publishing rfq event to epiportal");
				publishEventService.pushRfqEvent(event.getId(), actionBy.getBuyer().getId(), true);
			} catch (Exception e) {
				LOG.error("Error while publishing RFT event to EPortal:" + e.getMessage(), e);
			}
		} else {
			for (RfqEventApproval approval : approvalList) {
				if (approval.getLevel() == currentLevel.getLevel() + 1) {
					LOG.info("Setting Approval level " + approval.getLevel() + " as Active level");
					approval.setActive(true);
					for (RfqApprovalUser nextLevelUser : approval.getApprovalUsers()) {
						sendRfxApprovalEmails(event, nextLevelUser, RfxTypes.RFQ);
						if (Boolean.TRUE == event.getEnableApprovalReminder()) {
							Integer reminderHr = event.getReminderAfterHour();
							Integer reminderCpunt = event.getReminderCount();
							if (reminderHr != null && reminderCpunt != null) {
								Calendar now = Calendar.getInstance();
								now.add(Calendar.HOUR, reminderHr);
								nextLevelUser.setNextReminderTime(now.getTime());
								nextLevelUser.setReminderCount(reminderCpunt);
							}
						}
					}
					break;
				}
			}
		}
	}

	@Override
	@Transactional(readOnly = false)
	public RfiEvent doApproval(RfiEvent event, HttpSession session, User loggedInUser, JRSwapFileVirtualizer virtualizer) throws Exception {
		try {
			LOG.info("Do Approval level is called..");
			event = rfiEventService.loadRfiEventById(event.getId());
			List<RfiEventApproval> approvalList = event.getApprovals(); // rfiEventDao.getAllApprovalsForEvent(event.getId());
			if (CollectionUtil.isEmpty(approvalList)) {
				event.setStatus(EventStatus.APPROVED);
				LOG.info("status :" + event.getStatus());
				try {
					LOG.info("publishing rfi event to to epiportal if approval level is empty ");
					publishEventService.pushRfiEvent(event.getId(), loggedInUser.getBuyer().getId(), true);
				} catch (Exception e) {
					LOG.error("Error while publishing RFI event to EPortal:" + e.getMessage(), e);
				}
				// byte[] summarySnapshot = null;
				// try {
				// JasperPrint eventSummary = rfiEventService.getEvaluationSummaryPdf(event, loggedInUser, (String)
				// session.getAttribute(Global.SESSION_TIME_ZONE_KEY));
				// summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
				// } catch (JRException e) {
				// LOG.error("Error while Store summary PDF as byte : " + e.getMessage(), e);
				// }
				//
				// RfiEventAudit audit = new RfiEventAudit(loggedInUser.getBuyer(), event, loggedInUser, new Date(),
				// AuditActionType.Approve, "User completed the draft", summarySnapshot);
				// eventAuditService.save(audit);
				snapShotAuditService.doRfiAudit(event, session, event, loggedInUser, AuditActionType.Approve, "event.audit.approved", virtualizer);

			} else {
				if (event.getStatus() == EventStatus.DRAFT) {
					event.setStatus(EventStatus.PENDING);
					for (RfiEventApproval approval : approvalList) {
						if (approval.getLevel() == 1) {
							approval.setActive(true);
							break;
						}
					}
				} else {
					RfiEventApproval currentLevel = null;
					for (RfiEventApproval approval : approvalList) {
						if (approval.isActive()) {
							currentLevel = approval;
							break;
						}
					}
					boolean allUsersDone = true;
					if (currentLevel != null) {
						for (RfiApprovalUser user : currentLevel.getApprovalUsers()) {
							if (ApprovalStatus.PENDING == user.getApprovalStatus()) {
								LOG.info("All users of this level have not approved the Event.");
								allUsersDone = false;
								break;
							}
						}
					}
					if (allUsersDone) {
						setNextOrAllDone(null, approvalList, currentLevel, event, session, loggedInUser);
					}
				}
				// Just send emails to users.
				for (RfiEventApproval approval : approvalList) {
					if (approval.isActive()) {
						for (RfiApprovalUser user : approval.getApprovalUsers()) {
							if (user.getApprovalStatus() == ApprovalStatus.PENDING) {
								LOG.info("send mail to pending approvers ");
								sendRfxApprovalEmails(event, user, RfxTypes.RFI);

								if (Boolean.TRUE == event.getEnableApprovalReminder()) {
									Integer reminderHr = event.getReminderAfterHour();
									Integer reminderCpunt = event.getReminderCount();
									if (reminderHr != null && reminderCpunt != null) {
										Calendar now = Calendar.getInstance();
										now.add(Calendar.HOUR, reminderHr);
										user.setNextReminderTime(now.getTime());
										user.setReminderCount(reminderCpunt);
									}
								}

							}
						}
					}
				}
			}
			event = rfiEventDao.update(event);
		} catch (Exception e) {
			LOG.info("ERROR While Approving RFI :" + e.getMessage(), e);
			throw new Exception("ERROR While Approving RFI :" + e.getMessage());
		}
		return event;
	}

	@Override
	@Transactional(readOnly = false)
	public RfiEvent doApproval(RfiEvent event, User actionBy, String remarks, boolean approved, HttpSession session, JRSwapFileVirtualizer virtualizer) throws NotAllowedException {

		event = rfiEventService.loadRfiEventById(event.getId());
		List<RfiEventApproval> approvalList = event.getApprovals(); // rfiEventDao.getAllApprovalsForEvent(event.getId());

		// Identify Current Approval Level
		RfiEventApproval currentLevel = null;
		for (RfiEventApproval approval : approvalList) {
			if (approval.isActive()) {
				currentLevel = approval;
				LOG.info("Current Approval Level : " + currentLevel.getLevel());
				break;
			}
		}

		// Identify actionUser in the ApprovalUser of current level
		RfiApprovalUser actionUser = null;
		if (currentLevel != null) {
			for (RfiApprovalUser user : currentLevel.getApprovalUsers()) {
				if (user.getUser().getId().equals(actionBy.getId())) {
					actionUser = user;
					LOG.info("Approval being done by : " + actionBy.getLoginId());
				}
			}
		}
		if (actionUser == null) {
			// throw error
			LOG.error("User " + actionBy.getName() + " is not allowed to Approve or Reject RFI '" + event.getEventName() + "' at approval level : " + currentLevel.getLevel());
			throw new NotAllowedException("User " + actionBy.getName() + " is not allowed to Approve or Reject RFI '" + event.getEventName() + "' at approval level : " + currentLevel.getLevel());
		}

		if (actionUser.getApprovalStatus() != ApprovalStatus.PENDING) {
			// throw error
			LOG.error("User " + actionBy.getName() + " has already " + actionUser.getApprovalStatus() + " RFI at : " + actionUser.getActionDate());
			throw new NotAllowedException("User " + actionBy.getName() + " has already " + actionUser.getApprovalStatus() + " RFI at : " + actionUser.getActionDate());
		}

		// adding remarks into comments
		if (event.getComment() == null) {
			event.setComment(new ArrayList<RfiComment>());
		}
		RfiComment comment = new RfiComment();
		comment.setComment(remarks);
		comment.setApproved(approved);
		comment.setCreatedBy(actionBy);
		comment.setCreatedDate(new Date());
		comment.setRfxEvent(event);
		comment.setApprovalUserId(actionUser.getId());
		event.getComment().add(comment);

		// If rejected
		if (!approved) {
			// For OR level Rejection should be handled differently
			// commented on user requirement don't want to hold if any user is rejecting

			// if (currentLevel.getApprovalType() == ApprovalType.OR) {
			// boolean pending = false;
			// if (currentLevel != null) {
			// for (RfiApprovalUser user : currentLevel.getApprovalUsers()) {
			// // Check if any other user is pending his approval at this OR level other than actionBy user
			// if (!user.getId().equals(actionUser.getId())) {
			// if (user.getApprovalStatus() == ApprovalStatus.PENDING) {
			// pending = true;
			// break;
			// }
			// }
			// }
			// }
			// if (pending) {
			// // Dont reject yet as there are more people waiting for approval in this level
			// actionUser.setApprovalStatus(ApprovalStatus.REJECTED);
			// actionUser.setActionDate(new Date());
			// actionUser.setRemarks(remarks);
			// return rfiEventDao.update(event);
			// }
			// }

			// Reset all approvals for re-approval as the Event is rejected.
			for (RfiEventApproval approval : approvalList) {
				approval.setDone(false);
				approval.setActive(false);
				for (RfiApprovalUser user : approval.getApprovalUsers()) {
					// Send rejection email to all users
					// if (user.getApprovalStatus() == ApprovalStatus.PENDING) {
					sentRfxRejectionEmail(user.getUser(), event.getId(), event.getEventName(), actionBy, remarks, event.getCreatedBy(), RfxTypes.RFI, event.getReferanceNumber());
					// }
					user.setActionDate(null);
					user.setApprovalStatus(ApprovalStatus.PENDING);
					user.setRemarks(null);
					user.setActionDate(null);
				}
			}
			// actionUser.setApprovalStatus(ApprovalStatus.REJECTED);
			actionUser.setActionDate(new Date());
			actionUser.setRemarks(remarks);

			LOG.info("User " + actionBy.getName() + " has Rejected the RFI : " + event.getEventName());
			event.setStatus(EventStatus.DRAFT);
			// rftEventDao.update(event);

			// byte[] summarySnapshot = null;
			// try {
			// JasperPrint eventSummary = rfiEventService.getEvaluationSummaryPdf(event, actionBy, (String)
			// session.getAttribute(Global.SESSION_TIME_ZONE_KEY));
			// summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
			// } catch (JRException e) {
			// LOG.error("Error while Store summary PDF as byte : " + e.getMessage(), e);
			// }
			//
			// RfiEventAudit audit = new RfiEventAudit(actionBy.getBuyer(), event, actionBy, new Date(),
			// AuditActionType.Reject, messageSource.getMessage("event.audit.rejected", new Object[] {
			// event.getEventName() }, Global.LOCALE), summarySnapshot);
			// eventAuditService.save(audit);
			snapShotAuditService.doRfiAudit(event, session, event, actionBy, AuditActionType.Reject, "event.audit.rejected", virtualizer);

			try {
				if (event.getCreatedBy() != null) {
					LOG.info("Sending rejected request email to owner : " + event.getCreatedBy().getCommunicationEmail());
					sentRfxRejectionEmail(event.getCreatedBy(), event.getId(), event.getEventName(), actionBy, remarks, event.getCreatedBy(), RfxTypes.RFI, event.getReferanceNumber());
				}
			} catch (Exception e) {
				LOG.info("ERROR while Sending mail :" + e.getMessage(), e);
			}
		} else {
			LOG.info("User " + actionBy.getName() + " has Approved the RFI : " + event.getEventName());
			actionUser.setApprovalStatus(ApprovalStatus.APPROVED);
			actionUser.setActionDate(new Date());
			actionUser.setRemarks(remarks);

			// Send email notification to Creator
			sentRfxApprovalEmail(event.getCreatedBy(), event.getId(), event.getEventName(), actionBy, remarks, event.getCreatedBy(), RfxTypes.RFI, false, event.getReferanceNumber());

			// Send email notification to actionBy
			sentRfxApprovalEmail(actionBy, event.getId(), event.getEventName(), actionBy, remarks, event.getCreatedBy(), RfxTypes.RFI, true, event.getReferanceNumber());

			if (ApprovalType.OR == currentLevel.getApprovalType()) {
				LOG.info("This level has OR set for approval. Marking level as done");
				setNextOrAllDone(actionBy, approvalList, currentLevel, event, session, actionBy);
			} else {
				// AND Operation
				LOG.info("This level has AND set for approvals");
				boolean allUsersDone = true;
				if (currentLevel != null) {
					for (RfiApprovalUser user : currentLevel.getApprovalUsers()) {
						if (ApprovalStatus.PENDING == user.getApprovalStatus() || ApprovalStatus.REJECTED == user.getApprovalStatus()) {
							allUsersDone = false;
							LOG.info("All users of this level have not approved the RFI.");
							break;
						}
					}
				}
				if (allUsersDone) {
					LOG.info("All users of this level have approved the RFI.");
					setNextOrAllDone(actionBy, approvalList, currentLevel, event, session, actionBy);
				}
			}
		}
		// event.setApprovals(approvalList);
		return rfiEventDao.update(event);
	}

	/**
	 * @param actionBy
	 * @param approvalList
	 * @param currentLevel
	 * @param event
	 * @param loggedInUser
	 */
	private void setNextOrAllDone(User actionBy, List<RfiEventApproval> approvalList, RfiEventApproval currentLevel, RfiEvent event, HttpSession session, User loggedInUser) {
		currentLevel.setDone(true);
		currentLevel.setActive(false); // Check if all approvals are done
		// Check if all approvals are done
		if (currentLevel.getLevel() == approvalList.size()) {
			// all approvals done
			LOG.info("All approvals for this RFI is done!!!. Going to Approved Mode.");
			event.setStatus(EventStatus.APPROVED);
			event.setActionBy(actionBy);
			event.setActionDate(new Date());

			// byte[] summarySnapshot = null;
			// try {
			// JasperPrint eventSummary = rfiEventService.getEvaluationSummaryPdf(event, loggedInUser, (String)
			// session.getAttribute(Global.SESSION_TIME_ZONE_KEY));
			// summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
			// } catch (JRException e) {
			// LOG.error("Error while Store summary PDF as byte : " + e.getMessage(), e);
			// }
			// RfiEventAudit audit = new RfiEventAudit(actionBy.getBuyer(), event, actionBy, new Date(),
			// AuditActionType.Approve, messageSource.getMessage("event.audit.approved", new Object[] {
			// event.getEventName() }, Global.LOCALE), summarySnapshot);
			// eventAuditService.save(audit);

			// snapShotAuditService.doRfiAudit(event, session, event, actionBy, AuditActionType.Approve,
			// "event.audit.approved");
			LOG.info("All approvals for this RFI is in Approved Mode.");

			try {
				LOG.info("publishing rfi event to epiportal");
				publishEventService.pushRfiEvent(event.getId(), actionBy.getBuyer().getId(), true);
			} catch (Exception e) {
				LOG.error("Error while publishing RFI event to EPortal:" + e.getMessage(), e);
			}

		} else {
			for (RfiEventApproval approval : approvalList) {
				if (approval.getLevel() == currentLevel.getLevel() + 1) {
					LOG.info("Setting Approval level " + approval.getLevel() + " as Active level");
					approval.setActive(true);
					for (RfiApprovalUser nextLevelUser : approval.getApprovalUsers()) {
						sendRfxApprovalEmails(event, nextLevelUser, RfxTypes.RFI);
						if (Boolean.TRUE == event.getEnableApprovalReminder()) {
							Integer reminderHr = event.getReminderAfterHour();
							Integer reminderCpunt = event.getReminderCount();
							if (reminderHr != null && reminderCpunt != null) {
								Calendar now = Calendar.getInstance();
								now.add(Calendar.HOUR, reminderHr);
								nextLevelUser.setNextReminderTime(now.getTime());
								nextLevelUser.setReminderCount(reminderCpunt);
							}
						}
					}
					break;
				}
			}
		}
	}

	@Override
	@Transactional(readOnly = false)
	public RfaEvent doApproval(RfaEvent event, HttpSession session, User loggedInUser, JRSwapFileVirtualizer virtualizer) throws Exception {
		try {
			event = rfaEventService.loadRfaEventById(event.getId());
			List<RfaEventApproval> approvalList = event.getApprovals(); // rfaEventDao.getAllApprovalsForEvent(event.getId());
			if (CollectionUtil.isEmpty(approvalList)) {
				event.setStatus(EventStatus.APPROVED);
				LOG.info("status :" + event.getStatus());

				// byte[] summarySnapshot = null;
				// try {
				// JasperPrint eventSummary = rfaEventService.getEvaluationSummaryPdf(event, loggedInUser, (String)
				// session.getAttribute(Global.SESSION_TIME_ZONE_KEY));
				// summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
				// } catch (JRException e) {
				// LOG.error("Error while Store summary PDF as byte : " + e.getMessage(), e);
				// }
				// RfaEventAudit audit = new RfaEventAudit(loggedInUser.getBuyer(), event, loggedInUser, new Date(),
				// AuditActionType.Approve, messageSource.getMessage("event.audit.approved", new Object[] {
				// event.getEventName() }, Global.LOCALE), summarySnapshot);
				// eventAuditService.save(audit);

				snapShotAuditService.doRfaAudit(event, session, event, loggedInUser, AuditActionType.Approve, "event.audit.approved", virtualizer);
			} else {
				if (event.getStatus() == EventStatus.DRAFT) {
					event.setStatus(EventStatus.PENDING);
					for (RfaEventApproval approval : approvalList) {
						if (approval.getLevel() == 1) {
							approval.setActive(true);
							break;
						}
					}
				} else {
					RfaEventApproval currentLevel = null;
					for (RfaEventApproval approval : approvalList) {
						if (approval.isActive()) {
							currentLevel = approval;
							break;
						}
					}
					boolean allUsersDone = true;
					if (currentLevel != null) {
						for (RfaApprovalUser user : currentLevel.getApprovalUsers()) {
							if (ApprovalStatus.PENDING == user.getApprovalStatus()) {
								LOG.info("All users of this level have not approved the Event.");
								allUsersDone = false;
								break;
							}
						}
					}
					if (allUsersDone) {
						setNextOrAllDone(null, approvalList, currentLevel, event, session, loggedInUser);
					}
				}
				// Just send emails to users.
				for (RfaEventApproval approval : approvalList) {
					if (approval.isActive()) {
						for (RfaApprovalUser user : approval.getApprovalUsers()) {
							if (user.getApprovalStatus() == ApprovalStatus.PENDING) {
								LOG.info("send mail to pending approvers ");
								sendRfxApprovalEmails(event, user, RfxTypes.RFA);

								if (Boolean.TRUE == event.getEnableApprovalReminder()) {
									Integer reminderHr = event.getReminderAfterHour();
									Integer reminderCpunt = event.getReminderCount();
									if (reminderHr != null && reminderCpunt != null) {
										Calendar now = Calendar.getInstance();
										now.add(Calendar.HOUR, reminderHr);
										user.setNextReminderTime(now.getTime());
										user.setReminderCount(reminderCpunt);
									}
								}

							}
						}
					}
				}
			}
			event = rfaEventDao.update(event);
		} catch (Exception e) {
			LOG.info("ERROR While Approving RFA :" + e.getMessage(), e);
			throw new Exception("ERROR While Approving RFA :" + e.getMessage());
		}
		return event;
	}

	@Override
	@Transactional(readOnly = false)
	public RfaEvent doApproval(RfaEvent event, User actionBy, String remarks, boolean approved, HttpSession session, JRSwapFileVirtualizer virtualizer) throws NotAllowedException {

		event = rfaEventService.loadRfaEventById(event.getId());
		List<RfaEventApproval> approvalList = event.getApprovals(); // rfaEventDao.getAllApprovalsForEvent(event.getId());

		// Identify Current Approval Level
		RfaEventApproval currentLevel = null;
		for (RfaEventApproval approval : approvalList) {
			if (approval.isActive()) {
				currentLevel = approval;
				LOG.info("Current Approval Level : " + currentLevel.getLevel());
				break;
			}
		}

		// Identify actionUser in the ApprovalUser of current level
		RfaApprovalUser actionUser = null;
		if (currentLevel != null) {
			for (RfaApprovalUser user : currentLevel.getApprovalUsers()) {
				if (user.getUser().getId().equals(actionBy.getId())) {
					actionUser = user;
					LOG.info("Approval being done by : " + actionBy.getLoginId());
				}
			}
		}
		if (actionUser == null) {
			// throw error
			LOG.error("User " + actionBy.getName() + " is not allowed to Approve or Reject RFA '" + event.getEventName() + "' at approval level : " + currentLevel.getLevel());
			throw new NotAllowedException("User " + actionBy.getName() + " is not allowed to Approve or Reject RFA '" + event.getEventName() + "' at approval level : " + currentLevel.getLevel());
		}

		if (actionUser.getApprovalStatus() != ApprovalStatus.PENDING) {
			// throw error
			LOG.error("User " + actionBy.getName() + " has already " + actionUser.getApprovalStatus() + " RFA at : " + actionUser.getActionDate());
			throw new NotAllowedException("User " + actionBy.getName() + " has already " + actionUser.getApprovalStatus() + " RFA at : " + actionUser.getActionDate());
		}

		// adding remarks into comments
		if (event.getComment() == null) {
			event.setComment(new ArrayList<RfaComment>());
		}
		RfaComment comment = new RfaComment();
		comment.setComment(remarks);
		comment.setApproved(approved);
		comment.setCreatedBy(actionBy);
		comment.setCreatedDate(new Date());
		comment.setRfxEvent(event);
		comment.setApprovalUserId(actionUser.getId());
		event.getComment().add(comment);

		// If rejected
		if (!approved) {
			// For OR level Rejection should be handled differently
			// commented on user requirement don't want to hold if any user is rejecting

			// if (currentLevel.getApprovalType() == ApprovalType.OR) {
			// boolean pending = false;
			// if (currentLevel != null) {
			// for (RfaApprovalUser user : currentLevel.getApprovalUsers()) {
			// // Check if any other user is pending his approval at this OR level other than actionBy user
			// if (!user.getId().equals(actionUser.getId())) {
			// if (user.getApprovalStatus() == ApprovalStatus.PENDING) {
			// pending = true;
			// break;
			// }
			// }
			// }
			// }
			// if (pending) {
			// // Dont reject yet as there are more people waiting for approval in this level
			// actionUser.setApprovalStatus(ApprovalStatus.REJECTED);
			// actionUser.setActionDate(new Date());
			// actionUser.setRemarks(remarks);
			// return rfaEventDao.update(event);
			// }
			// }

			// Reset all approvals for re-approval as the Event is rejected.
			for (RfaEventApproval approval : approvalList) {
				approval.setDone(false);
				approval.setActive(false);
				for (RfaApprovalUser user : approval.getApprovalUsers()) {
					if (user.getApprovalStatus() == ApprovalStatus.PENDING) {
						// Send rejection email
						sentRfxRejectionEmail(user.getUser(), event.getId(), event.getEventName(), actionBy, remarks, event.getCreatedBy(), RfxTypes.RFA, event.getReferanceNumber());
					}
					user.setActionDate(null);
					user.setApprovalStatus(ApprovalStatus.PENDING);
					user.setRemarks(null);
					user.setActionDate(null);
				}
			}
			// actionUser.setApprovalStatus(ApprovalStatus.REJECTED);
			actionUser.setActionDate(new Date());
			actionUser.setRemarks(remarks);

			LOG.info("User " + actionBy.getName() + " has Rejected the RFA : " + event.getEventName());
			event.setStatus(EventStatus.DRAFT);
			// rftEventDao.update(event);

			// byte[] summarySnapshot = null;
			// try {
			// JasperPrint eventSummary = rfaEventService.getEvaluationSummaryPdf(event, actionBy, (String)
			// session.getAttribute(Global.SESSION_TIME_ZONE_KEY));
			// summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
			// } catch (JRException e) {
			// LOG.error("Error while Store summary PDF as byte : " + e.getMessage(), e);
			// }
			//
			// RfaEventAudit audit = new RfaEventAudit(actionBy.getBuyer(), event, actionBy, new Date(),
			// AuditActionType.Reject, messageSource.getMessage("event.audit.rejected", new Object[] {
			// event.getEventName() }, Global.LOCALE), summarySnapshot);
			// eventAuditService.save(audit);

			snapShotAuditService.doRfaAudit(event, session, event, actionBy, AuditActionType.Reject, "event.audit.rejected", virtualizer);
			try {
				if (event.getCreatedBy() != null) {
					LOG.info("Sending rejected request email to owner : " + event.getCreatedBy().getCommunicationEmail());
					sentRfxRejectionEmail(event.getCreatedBy(), event.getId(), event.getEventName(), actionBy, remarks, event.getCreatedBy(), RfxTypes.RFA, event.getReferanceNumber());
				}
			} catch (Exception e) {
				LOG.info("ERROR while Sending mail :" + e.getMessage(), e);
			}
		} else {
			LOG.info("User " + actionBy.getName() + " has Approved the RFA : " + event.getEventName());
			actionUser.setApprovalStatus(ApprovalStatus.APPROVED);
			actionUser.setActionDate(new Date());
			actionUser.setRemarks(remarks);

			// Send email notification to Creator
			sentRfxApprovalEmail(event.getCreatedBy(), event.getId(), event.getEventName(), actionBy, remarks, event.getCreatedBy(), RfxTypes.RFA, false, event.getReferanceNumber());
			// Send email notification to actionBy
			sentRfxApprovalEmail(actionBy, event.getId(), event.getEventName(), actionBy, remarks, event.getCreatedBy(), RfxTypes.RFA, true, event.getReferanceNumber());

			if (ApprovalType.OR == currentLevel.getApprovalType()) {
				LOG.info("This level has OR set for approval. Marking level as done");
				setNextOrAllDone(actionBy, approvalList, currentLevel, event, session, actionBy);
			} else {
				// AND Operation
				LOG.info("This level has AND set for approvals");
				boolean allUsersDone = true;
				if (currentLevel != null) {
					for (RfaApprovalUser user : currentLevel.getApprovalUsers()) {
						if (ApprovalStatus.PENDING == user.getApprovalStatus() || ApprovalStatus.REJECTED == user.getApprovalStatus()) {
							allUsersDone = false;
							LOG.info("All users of this level have not approved the RFA.");
							break;
						}
					}
				}
				if (allUsersDone) {
					LOG.info("All users of this level have approved the RFA.");
					setNextOrAllDone(actionBy, approvalList, currentLevel, event, session, actionBy);
				}
			}
		}
		// event.setApprovals(approvalList);
		return rfaEventDao.update(event);
	}

	/**
	 * @param actionBy
	 * @param approvalList
	 * @param currentLevel
	 * @param event
	 * @param loggedInUser
	 */
	private void setNextOrAllDone(User actionBy, List<RfaEventApproval> approvalList, RfaEventApproval currentLevel, RfaEvent event, HttpSession session, User loggedInUser) {
		currentLevel.setDone(true);
		currentLevel.setActive(false); // Check if all approvals are done
		// Check if all approvals are done
		if (currentLevel.getLevel() == approvalList.size()) {
			// all approvals done
			LOG.info("All approvals for this RFA is done!!!. Going to Approved Mode.");
			event.setStatus(EventStatus.APPROVED);
			event.setActionBy(actionBy);
			event.setActionDate(new Date());

			// byte[] summarySnapshot = null;
			// try {
			// JasperPrint eventSummary = rfaEventService.getEvaluationSummaryPdf(event, loggedInUser, (String)
			// session.getAttribute(Global.SESSION_TIME_ZONE_KEY));
			// summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
			// } catch (JRException e) {
			// LOG.error("Error while Store summary PDF as byte : " + e.getMessage(), e);
			// }
			// RfaEventAudit audit = new RfaEventAudit(actionBy.getBuyer(), event, actionBy, new Date(),
			// AuditActionType.Approve, messageSource.getMessage("event.audit.approved", new Object[] {
			// event.getEventName() }, Global.LOCALE), summarySnapshot);
			// eventAuditService.save(audit);

			// snapShotAuditService.doRfaAudit(event, session, event, loggedInUser, AuditActionType.Approve,
			// "event.audit.approved");
			LOG.info("All approvals for this RFA is in Approved Mode.");
		} else {
			for (RfaEventApproval approval : approvalList) {
				if (approval.getLevel() == currentLevel.getLevel() + 1) {
					LOG.info("Setting Approval level " + approval.getLevel() + " as Active level");
					approval.setActive(true);
					for (RfaApprovalUser nextLevelUser : approval.getApprovalUsers()) {
						sendRfxApprovalEmails(event, nextLevelUser, RfxTypes.RFA);
						if (Boolean.TRUE == event.getEnableApprovalReminder()) {
							Integer reminderHr = event.getReminderAfterHour();
							Integer reminderCpunt = event.getReminderCount();
							if (reminderHr != null && reminderCpunt != null) {
								Calendar now = Calendar.getInstance();
								now.add(Calendar.HOUR, reminderHr);
								nextLevelUser.setNextReminderTime(now.getTime());
								nextLevelUser.setReminderCount(reminderCpunt);
							}
						}
					}
					break;
				}
			}
		}
	}

	private void sendRfxApprovalEmails(Event event, ApprovalUser user, RfxTypes type) {
		String mailTo = user.getUser().getCommunicationEmail();
		String subject = "Event Approval Request";
		String url = APP_URL + "/buyer/" + type.name() + "/eventSummary/" + event.getId();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("userName", user.getUser().getName());
		map.put("event", event);
		map.put("eventType", type.name());
		map.put("businessUnit", StringUtils.checkString(findBusinessUnit(event.getId(), type)));
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		String timeZone = "GMT+8:00";
		timeZone = getTimeZoneByBuyerSettings(user.getUser().getTenantId(), timeZone);
		df.setTimeZone(TimeZone.getTimeZone(timeZone));
		map.put("date", df.format(new Date()));
		map.put("appUrl", url);
		map.put("loginUrl", APP_URL + "/login");
		if (StringUtils.checkString(mailTo).length() > 0) {
			sendEmail(mailTo, subject, map, Global.EVENT_APPROVAL_REQUEST_TEMPLATE);
		} else {
			LOG.warn("No communication email configured for user : " + user.getUser().getLoginId() + "... Not going to send email notification");
		}

		String notificationMessage = messageSource.getMessage("event.approval.request.notification.message", new Object[] { type.name(), event.getEventName() }, Global.LOCALE);
		sendDashboardNotification(user.getUser(), url, subject, notificationMessage, NotificationType.APPROVAL_MESSAGE);

		if (StringUtils.checkString(user.getUser().getDeviceId()).length() > 0) {
			try {
				LOG.info("User '" + user.getUser().getCommunicationEmail() + "' and device Id :" + user.getUser().getDeviceId());
				Map<String, String> payload = new HashMap<String, String>();
				payload.put("id", event.getId());
				payload.put("messageType", NotificationType.APPROVAL_MESSAGE.toString());
				payload.put("eventType", type.name());
				notificationService.pushOneSignalNotification(notificationMessage, null, payload, Arrays.asList(user.getUser().getDeviceId()));
			} catch (Exception e) {
				LOG.error("Error While sending Event approve Mobile push notification to '" + user.getUser().getCommunicationEmail() + "' : " + e.getMessage(), e);
			}
		} else {
			LOG.info("User '" + user.getUser().getCommunicationEmail() + "' Device Id is Null");
		}
	}

	@Override
	public String findBusinessUnit(String eventId, RfxTypes rfxTypes) {
		String displayName = null;
		switch (rfxTypes) {
		case RFA:
			displayName = rfaEventDao.findBusinessUnitName(eventId);
			break;
		case RFI:
			displayName = rfiEventDao.findBusinessUnitName(eventId);
			break;
		case RFP:
			displayName = rfpEventDao.findBusinessUnitName(eventId);
			break;
		case RFQ:
			displayName = rfqEventDao.findBusinessUnitName(eventId);
			break;
		case RFT:
			displayName = rftEventDao.findBusinessUnitName(eventId);
			break;
		default:
			break;
		}
		return displayName;
	}

	private void sendEmail(String mailTo, String subject, Map<String, Object> map, String template) {
		if (StringUtils.checkString(mailTo).length() > 0) {
			try {
				notificationService.sendEmail(mailTo, subject, map, template);
			} catch (Exception e) {
				LOG.info("ERROR while Sending mail :" + e.getMessage(), e);
			}
		} else {
			LOG.warn("No communication email configured for user... Not going to send email notification");
		}
	}

	private String getTimeZoneByBuyerSettings(String tenantId, String timeZone) {
		try {
			if (StringUtils.checkString(tenantId).length() > 0) {
				String time = buyerSettingsService.getBuyerTimeZoneByTenantId(tenantId);
				if (time != null) {
					timeZone = time;
				}
			}
		} catch (Exception e) {
			LOG.error("Error while fetching buyer time zone :" + e.getMessage(), e);
		}
		return timeZone;
	}

	private String getErpNotifiactionEmailsByBuyerSettings(String tenantId) {
		try {
			if (StringUtils.checkString(tenantId).length() > 0) {
				LOG.info("fetching buyer setting-------------------");
				BuyerSettings buyerSettings = buyerSettingsService.getBuyerSettingsByTenantId(tenantId);
				if (buyerSettings != null) {
					return buyerSettings.getErpNotificationEmails();
				}
			}
		} catch (Exception e) {
			LOG.error("Error while fetching buyer setting :" + e.getMessage(), e);
		}
		return null;
	}

	/**
	 * @param suppId
	 * @param timeZone
	 * @return
	 */
	public String getTimeZoneBySupplierSettings(String suppId, String timeZone) {
		try {
			if (StringUtils.checkString(suppId).length() > 0) {
				String time = supplierSettingsService.getSupplierTimeZoneByTenantId(suppId);
				if (time != null) {
					timeZone = time;
					LOG.info("time Zone :" + timeZone);
				}
			}
		} catch (Exception e) {
			LOG.error("Error while fetching supplier time zone :" + e.getMessage(), e);
		}
		return timeZone;
	}

	@Override
	@Transactional(readOnly = false)
	public boolean doMobileApproval(String eventId, FilterTypes type, AuthenticatedUser user, String remarks, boolean approve, HttpSession session, JRSwapFileVirtualizer virtualizer) throws Exception {
		boolean success = false;
		User actionBy = userDao.findById(user.getId());
		try {
			switch (type) {
			case RFA:
				RfaEvent rfaEvent = new RfaEvent();
				rfaEvent.setId(eventId);
				doApproval(rfaEvent, actionBy, remarks, approve, session, virtualizer);

				break;
			case RFI:
				RfiEvent rfiEvent = new RfiEvent();
				rfiEvent.setId(eventId);
				doApproval(rfiEvent, actionBy, remarks, approve, session, virtualizer);
				break;
			case RFP:
				RfpEvent rfpEvent = new RfpEvent();
				rfpEvent.setId(eventId);
				doApproval(rfpEvent, actionBy, remarks, approve, session, virtualizer);
				break;
			case RFQ:
				RfqEvent rfqEvent = new RfqEvent();
				rfqEvent.setId(eventId);
				doApproval(rfqEvent, actionBy, remarks, approve, session, virtualizer);
				break;
			case RFT:
				RftEvent event = new RftEvent();
				event.setId(eventId);
				doApproval(event, actionBy, remarks, approve, session, virtualizer);
				break;
			case PR:
				Pr pr = new Pr();
				pr.setId(eventId);
				doApproval(pr, actionBy, remarks, approve);
				break;

			case REQUEST:
				SourcingFormRequest request = new SourcingFormRequest();
				request.setId(eventId);
				doApprovalRequest(request, actionBy, remarks, approve);
				break;

			default:
				break;
			}
		} catch (Exception e) {
			throw e;
		}
		success = true;
		return success;
	}

	private void sendPrErrorEmail(User mailTo, Pr pr, String remarks) {
		LOG.info("Sending rejected request email to (" + mailTo.getName() + ") : " + mailTo.getCommunicationEmail());
		String url = APP_URL + "/buyer/prView/" + pr.getId();
		String subject = "PR ERROR NOTIFICATION";
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("userName", "");
		map.put("pr", pr);
		map.put("remarks", StringUtils.checkString(remarks));
		map.put("prReferanceNumber", StringUtils.checkString(pr.getReferenceNumber()));
		map.put("message", "Error to Transfer");
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		String timeZone = "GMT+8:00";
		timeZone = getTimeZoneByBuyerSettings(mailTo.getTenantId(), timeZone);
		df.setTimeZone(TimeZone.getTimeZone(timeZone));
		map.put("date", df.format(new Date()));
		map.put("loginUrl", APP_URL + "/login");
		map.put("appUrl", url);

		String erpNotificationEmails = getErpNotifiactionEmailsByBuyerSettings(mailTo.getTenantId());
		LOG.warn("erpNotificationEmails---------->" + erpNotificationEmails);
		if (StringUtils.checkString(erpNotificationEmails).length() > 0) {
			sendEmail(erpNotificationEmails, subject, map, Global.ERP_PR_ERROR_TEMPLATE);
		} else {
			LOG.warn("No communication email configured for user : " + mailTo.getLoginId() + "... Not going to send email notification");
		}

		String notificationMessage = messageSource.getMessage("erp.pr.rejection.notification.message", new Object[] { pr.getName(), remarks }, Global.LOCALE);
		try {
			sendDashboardNotification(mailTo, url, subject, notificationMessage, NotificationType.REJECT_MESSAGE);
		} catch (Exception e) {
			LOG.error("Error sending dashboard notification : " + e.getMessage(), e);
		}

		// if (StringUtils.checkString(mailTo.getDeviceId()).length() > 0) {
		// try {
		// LOG.info("User '" + mailTo.getCommunicationEmail() + "' and device Id :" + mailTo.getDeviceId());
		// Map<String, String> payload = new HashMap<String, String>();
		// payload.put("id", pr.getId());
		// payload.put("messageType", NotificationType.REJECT_MESSAGE.toString());
		// payload.put("eventType", FilterTypes.PR.toString());
		// notificationService.pushOneSignalNotification(notificationMessage, null, payload,
		// Arrays.asList(mailTo.getDeviceId()));
		// } catch (Exception e) {
		// LOG.error("Error While sending PR reject Mobile push notification to '" + mailTo.getCommunicationEmail() + "'
		// : " + e.getMessage(), e);
		// }
		// } else {
		// LOG.info("User '" + mailTo.getCommunicationEmail() + "' Device Id is Null");
		// }
	}

	void sharePrtoFinance(Po po) {
		po = poService.getLoadedPoById(po.getId());
		LOG.info("---supplierId------------------------" + po.getSupplier().getId() + " Buyer Id : " + po.getBuyer().getId());
		FavouriteSupplier favouriteSupplier = favoriteSupplierService.findFavSupplierByFavSuppId(po.getSupplier().getId(), po.getBuyer().getId());
		if (favouriteSupplier != null) {

			SupplierSettings supplierSettings = supplierSettingsService.getSupplierSettingsForFinanceByTenantId(favouriteSupplier.getSupplier().getId());
			if (supplierSettings != null) {
				LOG.info("---supplierSettings----------------------");
				if (supplierSettings.getPoShare() != null) {
					if (supplierSettings.getPoShare() == PoShare.ALL) {
						if (supplierSettings.getFinanceCompany() != null) {
							LOG.info("Sharing  PO to finance company bassed on all setting-------------------------");
							sharePOtoFinance(supplierSettings, po);
						} else {
							LOG.info("---Settings-----finance company---null--------------");
						}
					} else if (supplierSettings.getPoShare() == PoShare.BUYER) {
						/*
						 * if (poSharingBuyerDao.checkPoSharingToFinanceonBuyerSetting(SecurityLibrary.
						 * getLoggedInUserTenantId(), pr.getSupplier().getSupplier().getId())) {
						 * LOG.info("Sharing PO to finance bassed on seleced buyer setting-------------------------");
						 */
						List<PoSharingBuyer> list = supplierSettingsService.getPoSharingBuyersbySupplierId(po.getBuyer().getId(), favouriteSupplier.getSupplier().getId());
						if (CollectionUtil.isNotEmpty(list)) {
							LOG.info("Sharing  PO to finance company bassed on all setting-------------------------" + list.size());
							for (PoSharingBuyer poSharingBuyer : list) {
								LOG.info("---finance company id--------------" + poSharingBuyer.getFinanceCompany().getId());
								supplierSettings.setFinanceCompany(poSharingBuyer.getFinanceCompany());
								sharePOtoFinance(supplierSettings, po);
								break;
							}
						} else {
							LOG.info("---Settings-----finance company---null--------------");
						}

					}
				} else
					LOG.info("---supplierSettings--------null--------------");

			}

		}
	}

	private void sharePOtoFinance(SupplierSettings supplierSettings, Po po) {
		if (supplierSettings.getFinanceCompany() != null) {
			FinancePo financePo = new FinancePo();
			financePo.setFinanceCompany(supplierSettings.getFinanceCompany());
			financePo.setSupplier(po.getSupplier().getSupplier());
			financePo.setPo(po);
			financePo.setReferenceNum(referenceNumber(supplierSettings.getFinanceCompany()));
			financePo.setCreatedDate(new Date());
			financePo.setFinancePoStatus(FinancePoStatus.NEW);
			financePo.setFinancePoType(FinancePoType.SHARED);
			financePo.setSharedDate(new Date());
			poFinanceDao.save(financePo);
			LOG.info("Sending PO share email to---------");
			sendPoShareEmailsToFinance(userDao.getAdminUserForFinance(supplierSettings.getFinanceCompany()), po);
		}

	}

	private String referenceNumber(FinanceCompany financeCompany) {
		String referenceNumber = "";
		Integer length = 6;
		String seqNo = "1";
		if (financeCompany != null) {
			FinanceCompanySettings financeCompanySettings = financeSettingsService.getFinanceSettingsByTenantId(financeCompany.getId());
			if (financeCompanySettings != null) {
				if (StringUtils.checkString(financeCompanySettings.getPoSequencePrefix()).length() > 0) {
					referenceNumber += financeCompanySettings.getPoSequencePrefix();
				}
				if (StringUtils.checkString(financeCompanySettings.getPoSequenceNumber()).length() > 0) {
					seqNo = financeCompanySettings.getPoSequenceNumber();
				}
				if (financeCompanySettings.getPoSequenceLength() != null && financeCompanySettings.getPoSequenceLength() != 0) {
					length = financeCompanySettings.getPoSequenceLength();
				}

				referenceNumber += StringUtils.lpad(seqNo, length, '0');
				LOG.info("-----Updating settings-----------");
				int sequanceNum = Integer.parseInt((seqNo)) + 1;
				financeCompanySettings.setPoSequenceNumber("" + sequanceNum);
				financeSettingsService.updateFinanceSettingsSeqNumber(financeCompanySettings);
			} else {
				LOG.info("Finance Setting is null");
			}

		} else {
			LOG.info("Finance  is null");
		}
		LOG.info("-----referenceNumber----------->" + referenceNumber);
		return referenceNumber;
	}

	private void sendPoShareEmailsToFinance(User mailTo, Po po) {

		LOG.info("Sending PO share email to--------------------------------> (" + mailTo.getName() + ") : " + mailTo.getCommunicationEmail());
		try {
			String subject = "PO Share";
			String url = APP_URL + "/finance/financePOView/" + po.getId();
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("userName", mailTo.getName());
			map.put("message", "");
			map.put("pr", po);
			map.put("businessUnit", StringUtils.checkString(getBusinessUnitname(po.getPr().getId())));
			map.put("prReferanceNumber", (po.getPoNumber() == null ? "" : po.getPoNumber()));
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
			String timeZone = "GMT+8:00";
			timeZone = getTimeZoneByBuyerSettings(mailTo.getTenantId(), timeZone);
			df.setTimeZone(TimeZone.getTimeZone(timeZone));
			map.put("date", df.format(new Date()));
			map.put("loginUrl", APP_URL + "/login");
			map.put("appUrl", url);
			map.put("supplierName", "shared by " + po.getSupplierName());
			if (StringUtils.checkString(mailTo.getCommunicationEmail()).length() > 0) {
				sendEmail(mailTo.getCommunicationEmail(), subject, map, Global.FINANCEPO_SHARE_TEMPLATE);
			} else {
				LOG.warn("No communication email configured for user : " + mailTo.getLoginId() + "... Not going to send email notification");
			}

			sendDashboardNotificationForFinance(mailTo, url, subject, "New PO shared by Supplier", NotificationType.CREATED_MESSAGE);
		} catch (Exception e) {
			LOG.error("Error while Sending PO Created :" + e.getMessage(), e);
		}

		// if (StringUtils.checkString(mailTo.getDeviceId()).length() > 0) {
		// try {
		// LOG.info("User '" + mailTo.getCommunicationEmail() + "' and device Id :" + mailTo.getDeviceId());
		// Map<String, String> payload = new HashMap<String, String>();
		// payload.put("id", pr.getId());
		// payload.put("messageType", NotificationType.REJECT_MESSAGE.toString());
		// payload.put("eventType", FilterTypes.PR.toString());
		// notificationService.pushOneSignalNotification(notificationMessage, null, payload,
		// Arrays.asList(mailTo.getDeviceId()));
		// } catch (Exception e) {
		// LOG.error("Error While sending PR reject Mobile push notification to '" + mailTo.getCommunicationEmail() + "'
		// : " + e.getMessage(), e);
		// }
		// } else {
		// LOG.info("User '" + mailTo.getCommunicationEmail() + "' Device Id is Null");
		// }
	}

	@Override
	@Transactional(readOnly = false)
	public SourcingFormRequest doRequestApproval(SourcingFormRequest sourcingFormRequest, User loggedInUser) throws Exception {
		try {
			String buyerTimeZone = "GMT+8:00";

			sourcingFormRequest = sourcingFormRequestDao.findById(sourcingFormRequest.getId());
			if (sourcingFormRequest.getSubmittedDate() == null) {
				sourcingFormRequest.setSubmittedDate(new Date());
			}
			List<SourcingFormApprovalRequest> approvalList = sourcingFormRequest.getSourcingFormApprovalRequests();
			// If no approval setup - direct approve the Request
			if (CollectionUtil.isEmpty(approvalList)) {
				sourcingFormRequest.setStatus(SourcingFormStatus.APPROVED);
				if (sourcingFormRequest.getFirstApprovedDate() == null) {
					sourcingFormRequest.setFirstApprovedDate(new Date());
				}
				sourcingFormRequest.setApprovedDate(new Date());
				Double days = DateUtil.getDiffHoursInDouble(sourcingFormRequest.getApprovedDate(), sourcingFormRequest.getSubmittedDate());
				sourcingFormRequest.setApprovalDaysHours(BigDecimal.valueOf(days).setScale(2, RoundingMode.HALF_UP));
				sourcingFormRequest.setApprovalTotalLevels(0);
				sourcingFormRequest.setApprovalTotalUsers(0);

				sendrequestCreateEmail(loggedInUser, sourcingFormRequest, loggedInUser, buyerTimeZone);

			} else {

				SourcingFormApprovalRequest currentLevel = null;
				// Removed condition after discussing with Nitin
				if (sourcingFormRequest.getStatus() == SourcingFormStatus.DRAFT || (sourcingFormRequest.getStatus() == SourcingFormStatus.PENDING)) {
					sourcingFormRequest.setStatus(SourcingFormStatus.PENDING);
					for (SourcingFormApprovalRequest prApproval : approvalList) {
						if (prApproval.getLevel() == 1) {
							prApproval.setActive(true);
							break;
						}
					}
				} else {
					for (SourcingFormApprovalRequest requestApproval : approvalList) {
						if (requestApproval.isActive()) {
							currentLevel = requestApproval;
							break;
						}

					}
					boolean allUsersDone = true;
					if (currentLevel != null) {
						for (SourcingFormApprovalUserRequest user : currentLevel.getApprovalUsersRequest()) {
							if (ApprovalStatus.PENDING == user.getApprovalStatus()) {
								LOG.info("All users of this level have not approved the PR.");
								allUsersDone = false;
								break;
							}
						}
					}
					if (allUsersDone) {
						setNextOrAllDoneForRequest(loggedInUser, approvalList, currentLevel, sourcingFormRequest);
					}
				}

				// Just send emails to users.
				for (SourcingFormApprovalRequest requestApproval : approvalList) {
					if (requestApproval.isActive()) {
						for (SourcingFormApprovalUserRequest nextLevelUser : requestApproval.getApprovalUsersRequest()) {
							if (nextLevelUser.getApprovalStatus() == ApprovalStatus.PENDING) {
								LOG.info("send mail to pending approvers ");
								buyerTimeZone = getTimeZoneByBuyerSettings(nextLevelUser.getUser().getTenantId(), buyerTimeZone);
								LOG.info("***********************************************************************************************");
								sendEmailToRequestApprovers(sourcingFormRequest, nextLevelUser, buyerTimeZone);

								if (Boolean.TRUE == sourcingFormRequest.getEnableApprovalReminder()) {
									Integer reminderHr = sourcingFormRequest.getReminderAfterHour();
									Integer reminderCpunt = sourcingFormRequest.getReminderCount();
									if (reminderHr != null && reminderCpunt != null) {
										Calendar now = Calendar.getInstance();
										now.add(Calendar.HOUR, reminderHr);
										nextLevelUser.setNextReminderTime(now.getTime());
										nextLevelUser.setReminderCount(reminderCpunt);
									}
								}
							}
						}
					}
				}
			}
			sourcingFormRequest.setSummaryCompleted(true);

			sourcingFormRequest = requestService.update(sourcingFormRequest);

		} catch (Exception e) {
			LOG.error("ERROR While Approving request :" + e.getMessage(), e);
			throw new Exception("ERROR While Approving request :" + e.getMessage());
		}
		return sourcingFormRequest;
	}

	private void setNextOrAllDoneForRequest(User actionBy, List<SourcingFormApprovalRequest> approvalList, SourcingFormApprovalRequest currentLevel, SourcingFormRequest sourcingFormRequest) {
		String buyerTimeZone = "GMT+8:00";
		currentLevel.setDone(true);
		currentLevel.setActive(false); // Check if all approvals are done
		if (currentLevel.getLevel() == approvalList.size()) {
			// all approvals done
			LOG.info("All approvals for this Request is done!!!. Going to Approved Mode.");

			sourcingFormRequest.setStatus(SourcingFormStatus.APPROVED);
			sourcingFormRequest.setActionBy(actionBy);
			sourcingFormRequest.setActionDate(new Date());
			if (sourcingFormRequest.getFirstApprovedDate() == null) {
				sourcingFormRequest.setFirstApprovedDate(new Date());
			}
			sourcingFormRequest.setApprovedDate(new Date());
			try {
				if (sourcingFormRequest.getApprovedDate() != null && sourcingFormRequest.getSubmittedDate() != null) {
					Double days = DateUtil.getDiffHoursInDouble(sourcingFormRequest.getApprovedDate(), sourcingFormRequest.getSubmittedDate());
					sourcingFormRequest.setApprovalDaysHours(BigDecimal.valueOf(days).setScale(2, RoundingMode.HALF_UP));
				} else {
					sourcingFormRequest.setApprovalDaysHours(BigDecimal.ONE);
				}

				// sourcingFormRequest.setApprovalDaysHours(DateUtil.getDiffHoursInInteger(sourcingFormRequest.getApprovedDate(),
				// sourcingFormRequest.getSubmittedDate()));

				// LOG.info("Form Id : " + sourcingFormRequest.getId());
				// sourcingFormRequest.setApprovalTotalLevels((int)
				// sourcingFormRequestDao.findTotaApprovalLevelsRequestCount(sourcingFormRequest.getId()));
				// LOG.info("Total Levels : " + sourcingFormRequest.getApprovalTotalLevels());

				Integer approvalTotalUsers = 0;
				int maxLevel = 0;
				List<SourcingFormApprovalRequest> requestApprovalList = sourcingFormRequestDao.getApprovalRequestList(sourcingFormRequest.getId());
				for (SourcingFormApprovalRequest approval : requestApprovalList) {
					LOG.info("Level : " + approval.getLevel());
					if (approval.getLevel() > maxLevel) {
						maxLevel = approval.getLevel();
					}
					if (CollectionUtil.isNotEmpty(approval.getApprovalUsersRequest())) {
						LOG.info("Users : " + approval.getApprovalUsersRequest().size());
						approvalTotalUsers += approval.getApprovalUsersRequest().size();
					}
				}
				LOG.info("approvalTotalUsers  : " + approvalTotalUsers);
				sourcingFormRequest.setApprovalTotalUsers(approvalTotalUsers);
				sourcingFormRequest.setApprovalTotalLevels(maxLevel);
			} catch (Exception e) {
				LOG.error("Error computing total approvers and levels and turn around time : " + e.getMessage(), e);
			}
		} else {
			for (SourcingFormApprovalRequest requestApproval : approvalList) {
				if (requestApproval.getLevel() == currentLevel.getLevel() + 1) {
					LOG.info("Setting Approval level " + requestApproval.getLevel() + " as Active level");
					requestApproval.setActive(true);
					for (SourcingFormApprovalUserRequest nextLevelUser : requestApproval.getApprovalUsersRequest()) {
						buyerTimeZone = getTimeZoneByBuyerSettings(nextLevelUser.getUser().getTenantId(), buyerTimeZone);
						// send email to Approver
						sendEmailToRequestApprovers(sourcingFormRequest, nextLevelUser, buyerTimeZone);

						if (Boolean.TRUE == sourcingFormRequest.getEnableApprovalReminder()) {
							Integer reminderHr = sourcingFormRequest.getReminderAfterHour();
							Integer reminderCpunt = sourcingFormRequest.getReminderCount();
							if (reminderHr != null && reminderCpunt != null) {
								Calendar now = Calendar.getInstance();
								now.add(Calendar.HOUR, reminderHr);
								nextLevelUser.setNextReminderTime(now.getTime());
								nextLevelUser.setReminderCount(reminderCpunt);
							}
						}

					}
					break;
				}
			}
		}

	}

	/**
	 * SEND EMAIL REUEST EMAIL HERE
	 * 
	 * @author sudesha
	 * @param mailTo
	 * @param sourcingFormRequest
	 * @param actionBy
	 * @param remarks
	 */

	private void sendrequestCreateEmail(User mailTo, SourcingFormRequest sourcingFormRequest, User actionBy, String remarks) {
		try {
			LOG.info("***********************************************************************************************");
			LOG.info("Sending Approval email to (" + mailTo.getName() + ") : " + mailTo.getCommunicationEmail());

			String subject = "RFS Created";
			String url = APP_URL + "/buyer/viewSourcingSummary/" + sourcingFormRequest.getId();
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("userName", mailTo.getName());
			map.put("actionByName", actionBy.getName());
			map.put("request", sourcingFormRequest);
			map.put("remarks", StringUtils.checkString(remarks));
			map.put("businessUnit", StringUtils.checkString(getBusineessUnitnamerequest(sourcingFormRequest.getId())));
			map.put("requestReferanceNumber", StringUtils.checkString(sourcingFormRequest.getReferanceNumber()));
			if (mailTo.getId().equals(actionBy.getId())) {
				map.put("message", "You have Approved");
			} else {
				map.put("message", actionBy.getName() + " has Approved");
			}
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
			String timeZone = "GMT+8:00";
			timeZone = getTimeZoneByBuyerSettings(mailTo.getTenantId(), timeZone);
			df.setTimeZone(TimeZone.getTimeZone(timeZone));
			map.put("date", df.format(new Date()));
			map.put("loginUrl", APP_URL + "/login");
			map.put("appUrl", url);
			if (StringUtils.checkString(mailTo.getCommunicationEmail()).length() > 0) {
				sendEmail(mailTo.getCommunicationEmail(), subject, map, Global.REQUEST_APPROVAL_CREATE_EMAIL);
			} else {
				LOG.warn("No communication email configured for user : " + mailTo.getLoginId() + "... Not going to send email notification");
			}
			String notificationMessage = messageSource.getMessage("request.create.notification.message", new Object[] { actionBy.getName(), sourcingFormRequest.getSourcingFormName() }, Global.LOCALE);
			sendDashboardNotification(mailTo, url, subject, notificationMessage, NotificationType.APPROVAL_MESSAGE);

			/*
			 * if (StringUtils.checkString(mailTo.getDeviceId()).length() > 0) { try { LOG.info("User '" +
			 * mailTo.getCommunicationEmail() + "' and device Id :" + mailTo.getDeviceId()); Map<String, String> payload
			 * = new HashMap<String, String>(); payload.put("id", sourcingFormRequest.getId());
			 * payload.put("messageType", NotificationType.APPROVAL_MESSAGE.toString()); payload.put("eventType",
			 * FilterTypes.PR.toString()); notificationService.pushOneSignalNotification(notificationMessage, null,
			 * payload, Arrays.asList(mailTo.getDeviceId())); } catch (Exception e) {
			 * LOG.error("Error While sending Approval Mobile push notification to '" + mailTo.getCommunicationEmail() +
			 * "' : " + e.getMessage(), e); } } else { LOG.info("User '" + mailTo.getCommunicationEmail() +
			 * "' Device Id is Null"); }
			 */
		} catch (Exception e) {
			LOG.error("ERROR While Appr" + e.getMessage(), e);
		}
	}

	/**
	 * GET BUSINESS UNIT FOR SEND EMAIL CREATION FOR REQUEST
	 * 
	 * @author sudesha
	 * @param id
	 * @return
	 */
	private String getBusineessUnitnamerequest(String id) {
		String displayName = null;
		displayName = sourcingFormRequestDao.getBusineessUnitnamerequest(id);
		return StringUtils.checkString(displayName);
	}

	/**
	 * SEND REQUEST APPROVAL EMAIL HERE
	 * 
	 * @author sudesha
	 * @param mailTo
	 * @param sourcingFormRequest
	 * @param actionBy
	 * @param remarks
	 */
	private void sendRequestApprovalEmail(User mailTo, SourcingFormRequest sourcingFormRequest, User actionBy, String remarks) {
		LOG.info("Sending Approval email to (" + mailTo.getName() + ") : " + mailTo.getCommunicationEmail());
		String subject = "RFS Approved";
		String url = APP_URL + "/buyer/viewSourcingSummary/" + sourcingFormRequest.getId();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("userName", mailTo.getName());
		map.put("actionByName", actionBy.getName());
		map.put("request", sourcingFormRequest);
		map.put("remarks", StringUtils.checkString(remarks));
		LOG.info("businessUnit************************************************************");
		map.put("businessUnit", StringUtils.checkString(getBusineessUnitnamerequest(sourcingFormRequest.getId())));
		map.put("requestReferanceNumber", StringUtils.checkString(sourcingFormRequest.getReferanceNumber()));
		if (mailTo.getId().equals(actionBy.getId())) {
			map.put("message", "You have Approved");
		} else {
			map.put("message", actionBy.getName() + " has Approved");
		}
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		String timeZone = "GMT+8:00";
		timeZone = getTimeZoneByBuyerSettings(mailTo.getTenantId(), timeZone);
		df.setTimeZone(TimeZone.getTimeZone(timeZone));
		map.put("date", df.format(new Date()));
		map.put("loginUrl", APP_URL + "/login");
		map.put("appUrl", url);

		if (StringUtils.checkString(mailTo.getCommunicationEmail()).length() > 0) {
			sendEmail(mailTo.getCommunicationEmail(), subject, map, Global.REQUEST_APPROVAL_TEMPLATE);
		} else {
			LOG.warn("No communication email configured for user : " + mailTo.getLoginId() + "... Not going to send email notification");
		}
		String notificationMessage = messageSource.getMessage("request.approval.notification.message", new Object[] { actionBy.getName(), sourcingFormRequest.getSourcingFormName() }, Global.LOCALE);
		sendDashboardNotification(mailTo, url, subject, notificationMessage, NotificationType.APPROVAL_MESSAGE);

		/*
		 * if (StringUtils.checkString(mailTo.getDeviceId()).length() > 0) { try { LOG.info("User '" +
		 * mailTo.getCommunicationEmail() + "' and device Id :" + mailTo.getDeviceId()); Map<String, String> payload =
		 * new HashMap<String, String>(); payload.put("id", sourcingFormRequest.getId()); payload.put("messageType",
		 * NotificationType.APPROVAL_MESSAGE.toString()); payload.put("eventType", FilterTypes.PR.toString());
		 * notificationService.pushOneSignalNotification(notificationMessage, null, payload,
		 * Arrays.asList(mailTo.getDeviceId())); } catch (Exception e) {
		 * LOG.error("Error While sending Approval Mobile push notification to '" + mailTo.getCommunicationEmail() +
		 * "' : " + e.getMessage(), e); } } else { LOG.info("User '" + mailTo.getCommunicationEmail() +
		 * "' Device Id is Null"); }
		 */
	}

	@Override
	public void sendEmailToRequestApprovers(SourcingFormRequest sourcingFormRequest, SourcingFormApprovalUserRequest nextLevelUser, String buyerTimeZone) {
		String mailTo = nextLevelUser.getUser().getCommunicationEmail();
		String subject = "RFS Approval request";
		String url = APP_URL + "/buyer/viewSourcingSummary/" + sourcingFormRequest.getId();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("userName", nextLevelUser.getUser().getName());
		map.put("request", sourcingFormRequest);
		map.put("businessUnit", StringUtils.checkString(getBusineessUnitnamerequest(sourcingFormRequest.getId())));
		map.put("requestReferanceNumber", (sourcingFormRequest.getReferanceNumber() == null ? "" : sourcingFormRequest.getReferanceNumber()));
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		df.setTimeZone(TimeZone.getTimeZone(buyerTimeZone));
		map.put("date", df.format(new Date()));
		map.put("loginUrl", APP_URL + "/login");
		map.put("appUrl", url);
		if (StringUtils.checkString(mailTo).length() > 0) {
			sendEmail(mailTo, subject, map, Global.REQURST_APPROVAL_REQUEST_TEMPLATE);
		} else {
			LOG.warn("No communication email configured for user : " + nextLevelUser.getUser().getLoginId() + "... Not going to send email notification");
		}

		String notificationMessage = messageSource.getMessage("request.approval.request.notification.message", new Object[] { sourcingFormRequest.getSourcingFormName() }, Global.LOCALE);
		sendDashboardNotification(nextLevelUser.getUser(), url, subject, notificationMessage, NotificationType.APPROVAL_MESSAGE);
		if (StringUtils.checkString(nextLevelUser.getUser().getDeviceId()).length() > 0) {
			try {
				LOG.info("User '" + mailTo + "' and device Id :" + nextLevelUser.getUser().getDeviceId());
				Map<String, String> payload = new HashMap<String, String>();
				payload.put("id", sourcingFormRequest.getId());
				payload.put("messageType", NotificationType.APPROVAL_MESSAGE.toString());
				payload.put("eventType", FilterTypes.PR.toString());
				notificationService.pushOneSignalNotification(notificationMessage, null, payload, Arrays.asList(nextLevelUser.getUser().getDeviceId()));
			} catch (Exception e) {
				LOG.error("Error While sending Approval Mobile push notification to '" + mailTo + "' : " + e.getMessage(), e);
			}
		} else {
			LOG.info("User '" + mailTo + "' Device Id is Null");
		}
	}

	/**
	 * SEND REQUEST REJECTION EMAIL HERE
	 * 
	 * @author sudesha
	 * @param mailTo
	 * @param sourcingFormRequest
	 * @param actionBy
	 * @param remarks
	 */
	private void sendsourcingRejectionEmail(User mailTo, SourcingFormRequest sourcingFormRequest, User actionBy, String remarks) {
		LOG.info("Sending rejected request email to (" + mailTo.getName() + ") : " + mailTo.getCommunicationEmail());
		String url = APP_URL + "/buyer/viewSourcingSummary/" + sourcingFormRequest.getId();
		String subject = "RFS Rejected";
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("userName", mailTo.getName());
		map.put("actionByName", actionBy.getName());
		map.put("request", sourcingFormRequest);
		map.put("remarks", StringUtils.checkString(remarks));
		map.put("businessUnit", StringUtils.checkString(getBusineessUnitnamerequest(sourcingFormRequest.getId())));
		map.put("requestReferanceNumber", StringUtils.checkString(sourcingFormRequest.getReferanceNumber()));
		if (mailTo.getId().equals(actionBy.getId())) {
			map.put("message", "You have Rejected");
		} else {
			map.put("message", actionBy.getName() + " has Rejected");
		}
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		String timeZone = "GMT+8:00";
		timeZone = getTimeZoneByBuyerSettings(mailTo.getTenantId(), timeZone);
		df.setTimeZone(TimeZone.getTimeZone(timeZone));
		map.put("date", df.format(new Date()));
		map.put("loginUrl", APP_URL + "/login");
		map.put("appUrl", url);

		if (StringUtils.checkString(mailTo.getCommunicationEmail()).length() > 0) {
			sendEmail(mailTo.getCommunicationEmail(), subject, map, Global.SOURCING_REJECT_TEMPLATE);
		} else {
			LOG.warn("No communication email configured for user : " + mailTo.getLoginId() + "... Not going to send email notification");
		}

		String notificationMessage = messageSource.getMessage("request.rejection.notification.message", new Object[] { actionBy.getName(), sourcingFormRequest.getSourcingFormName(), remarks }, Global.LOCALE);
		sendDashboardNotification(mailTo, url, subject, notificationMessage, NotificationType.REJECT_MESSAGE);

		// if (StringUtils.checkString(mailTo.getDeviceId()).length() > 0) {
		// try {
		// LOG.info("User '" + mailTo.getCommunicationEmail() + "' and device Id :" + mailTo.getDeviceId());
		// Map<String, String> payload = new HashMap<String, String>();
		// payload.put("id", sourcingFormRequest.getId());
		// payload.put("messageType", NotificationType.REJECT_MESSAGE.toString());
		// payload.put("eventType", FilterTypes.PR.toString());
		// notificationService.pushOneSignalNotification(notificationMessage, null, payload,
		// Arrays.asList(mailTo.getDeviceId()));
		// } catch (Exception e) {
		// LOG.error("Error While sending Request reject Mobile push notification to '" + mailTo.getCommunicationEmail()
		// + "' : " + e.getMessage(), e);
		// }
		// } else {
		// LOG.info("User '" + mailTo.getCommunicationEmail() + "' Device Id is Null");
		// }
	}

	@Override
	@Transactional(readOnly = false)
	public SourcingFormRequest doApprovalRequest(SourcingFormRequest sourcingFormRequest, User actionBy, String remarks, boolean approved) throws Exception {

		sourcingFormRequest = sourcingFormRequestDao.findById(sourcingFormRequest.getId());
		List<SourcingFormApprovalRequest> approvalList = sourcingFormRequest.getSourcingFormApprovalRequests();

		// Identify Current Approval Level
		SourcingFormApprovalRequest currentLevel = null;
		for (SourcingFormApprovalRequest sourcingFormApproval : approvalList) {
			if (sourcingFormApproval.isActive()) {
				currentLevel = sourcingFormApproval;
				LOG.info("Current Approval Level : " + currentLevel.getLevel());
				break;
			}
		}

		// Identify actionUser in the ApprovalUser of current level
		SourcingFormApprovalUserRequest actionUser = null;
		if (currentLevel != null) {
			for (SourcingFormApprovalUserRequest user : currentLevel.getApprovalUsersRequest()) {
				if (user.getUser().getId().equals(actionBy.getId())) {
					actionUser = user;
					LOG.info("Approval being done by : " + actionBy.getLoginId());
				}
			}
		}
		if (actionUser == null) {
			// throw error
			LOG.error("User " + actionBy.getName() + " is not allowed to Approve or Reject RFS '" + sourcingFormRequest.getSourcingFormName() + "' at approval level : " + (currentLevel != null ? currentLevel.getLevel() : "Not Found"));
			throw new NotAllowedException("User " + actionBy.getName() + " is not allowed to Approve or Reject PR '" + sourcingFormRequest.getSourcingFormName() + "' at approval level : " + (currentLevel != null ? currentLevel.getLevel() : ""));
		}

		if (actionUser.getApprovalStatus() != ApprovalStatus.PENDING) {
			// throw error
			LOG.error("User " + actionBy.getName() + " has already " + actionUser.getApprovalStatus() + " RFS at : " + actionUser.getActionDate());
			throw new NotAllowedException("User " + actionBy.getName() + " has already " + actionUser.getApprovalStatus() + " RFS at : " + actionUser.getActionDate());
		}

		// adding remarks into comments
		if (sourcingFormRequest.getRequestComments() == null) {
			sourcingFormRequest.setRequestComments(new ArrayList<RequestComment>());
		}
		RequestComment requestComment = new RequestComment();
		requestComment.setComment(remarks);
		requestComment.setApproved(approved);
		requestComment.setCreatedBy(actionBy);
		requestComment.setCreatedDate(new Date());
		requestComment.setRequest(sourcingFormRequest);
		requestComment.setApprovalUserId(actionUser.getId());
		sourcingFormRequest.getRequestComments().add(requestComment);

		// If rejected
		if (!approved) {
			// Reset all approvals for re-approval as the PR is rejected.
			for (SourcingFormApprovalRequest sourcingFormApproval : approvalList) {
				sourcingFormApproval.setDone(false);
				sourcingFormApproval.setActive(false);

				for (SourcingFormApprovalUserRequest user : sourcingFormApproval.getApprovalUsersRequest()) {
					try {
						if (!sourcingFormRequest.getCreatedBy().getId().equals(user.getUser().getId())) {
							sendsourcingRejectionEmail(user.getUser(), sourcingFormRequest, actionBy, remarks);
						}
					} catch (Exception e) {
						LOG.info("ERROR while Sending RFS reject mail :" + e.getMessage(), e);
					}
					user.setActionDate(null);
					user.setApprovalStatus(ApprovalStatus.PENDING);
					user.setRemarks(null);
					user.setActionDate(null);
				}
			}

			actionUser.setActionDate(new Date());
			actionUser.setRemarks(remarks);
			LOG.info("User " + actionBy.getName() + " has Rejected the RFS : " + sourcingFormRequest.getSourcingFormName());
			sourcingFormRequest.setStatus(SourcingFormStatus.DRAFT);
			sourcingFormRequest.setApprovedDate(null);
			sourcingFormRequest.setApprovalDaysHours(null);
			sourcingFormRequest.setApprovalTotalLevels(null);
			sourcingFormRequest.setApprovalTotalUsers(null);

			try {
				if (sourcingFormRequest.getCreatedBy() != null) {
					sendsourcingRejectionEmail(sourcingFormRequest.getCreatedBy(), sourcingFormRequest, actionBy, remarks);
				}
			} catch (Exception e) {
				LOG.info("ERROR while Sending mail :" + e.getMessage(), e);
			}

			// Check if budget checking ERP interface is enabled
			ErpSetup erpSetup = erpConfigDao.getErpConfigBytenantId(SecurityLibrary.getLoggedInUserTenantId());
			if (erpSetup != null && Boolean.TRUE == erpSetup.getIsErpEnable() && Boolean.TRUE == erpSetup.getEnableRfsErpPush() && Boolean.TRUE == sourcingFormRequest.getBusinessUnit().getBudgetCheck()) {
				erpIntegrationService.transferRejectRfsToErp(sourcingFormRequest.getId(), erpSetup);
			}

			sourcingFormRequest.setErpDocNo(null);
			sourcingFormRequest.setErpMessage(null);
			sourcingFormRequest.setErpTransferred(Boolean.FALSE);

		} else {
			LOG.info("User " + actionBy.getName() + " has Approved the RFS : " + sourcingFormRequest.getSourcingFormName());
			actionUser.setApprovalStatus(ApprovalStatus.APPROVED);
			actionUser.setActionDate(new Date());
			actionUser.setRemarks(remarks);

			// Send email notification to Approval
			sendRequestApprovalEmail(sourcingFormRequest.getCreatedBy(), sourcingFormRequest, actionBy, remarks);

			// Send email notification to Approval
			if (!sourcingFormRequest.getCreatedBy().getId().equals(actionBy.getId())) {
				sendRequestApprovalEmail(actionBy, sourcingFormRequest, actionBy, remarks);
			}

			if (ApprovalType.OR == currentLevel.getApprovalType()) {
				LOG.info("This level has OR set for approval. Marking level as done");
				setNextOrAllDoneForRequest(actionBy, approvalList, currentLevel, sourcingFormRequest);
			} else {
				// AND Operation
				LOG.info("This level has AND set for approvals");
				boolean allUsersDone = true;
				if (currentLevel != null) {
					for (SourcingFormApprovalUserRequest user : currentLevel.getApprovalUsersRequest()) {
						if (ApprovalStatus.PENDING == user.getApprovalStatus() || ApprovalStatus.REJECTED == user.getApprovalStatus()) {
							LOG.info("All users of this level have not approved the request.");
							allUsersDone = false;
							break;
						}
					}
				}
				if (allUsersDone) {
					LOG.info("All users of this level have approved the RFS.");
					setNextOrAllDoneForRequest(actionBy, approvalList, currentLevel, sourcingFormRequest);
				}
			}
		}
		// pr.setPrApprovals(approvalList);
		return requestService.update(sourcingFormRequest);
	}

	@Override
	public void sendEmailToAssociatedBuyer(Supplier supplier, String supplierRemark, Buyer buyer, TimeZone timeZone) {
		LOG.info("Sending Email Request to associated buyer (" + buyer.getFullName() + ") : " + buyer.getPublishedProfileCommunicationEmail());
		try {
			String subject = "Supplier Request";
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("buyerName", buyer.getFullName());
			map.put("supplierName", supplier.getCompanyName());
			map.put("message", "You have received a new supplier request");
			map.put("supplierCountry", supplier.getRegistrationOfCountry().getCountryName());
			map.put("supplierRemark", supplierRemark);
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
			df.setTimeZone(timeZone);
			map.put("date", df.format(new Date()));
			map.put("loginUrl", APP_URL + "/login");
			map.put("appUrl", APP_URL + "/login");

			if (StringUtils.checkString(buyer.getPublishedProfileCommunicationEmail()).length() > 0) {
				sendEmail(buyer.getPublishedProfileCommunicationEmail(), subject, map, Global.REQUEST_ASSOCIATE_BUYER);
			} else {
				LOG.warn("No communication email configured for user : " + buyer.getFullName() + "... Not going to send email notification");
			}
		} catch (Exception e) {
			LOG.error("Error while Sending request email to associate buyer :" + e.getMessage(), e);
		}
	}

	@Override
	public void sendEmailToAssociatedSupplier(Supplier supplier, Buyer buyer, TimeZone timeZone, boolean approveRejectFlag, RequestedAssociatedBuyer associatedBuyer, String buyerRemark) {
		LOG.info("Sending Email to associated supplier (" + supplier.getFullName() + ") : " + supplier.getCommunicationEmail());
		try {

			HashMap<String, Object> map = new HashMap<String, Object>();
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
			df.setTimeZone(timeZone);
			map.put("loginUrl", APP_URL + "/login");
			map.put("appUrl", APP_URL + "/login");
			if (approveRejectFlag == true) {
				// Request has been accepted.
				map.put("message", "Your request to be associated with \"+buyer.getCompanyName()+\" has been accepted.");
				map.put("supplierName", supplier.getFullName());
				map.put("buyerName", buyer.getCompanyName());
				map.put("buyerCountry", buyer.getRegistrationOfCountry().getCountryName());
				map.put("reqDate", df.format(associatedBuyer.getRequestedDate()));
				map.put("date", df.format(new Date()));
				map.put("buyerRemarks", StringUtils.checkString(buyerRemark));
				if (StringUtils.checkString(supplier.getCommunicationEmail()).length() > 0) {
					sendEmail(supplier.getCommunicationEmail(), "Request Accepted", map, Global.REQUEST_ASSOCIATE_BUYER_ACCEPT);
				} else {
					LOG.warn("No communication email configured for user : " + supplier.getCompanyName() + "... Not going to send email notification");
				}
			} else {
				// Request has been rejected.
				map.put("supplierName", supplier.getFullName());
				map.put("buyerName", buyer.getCompanyName());
				map.put("message", "Your request to be associated with "+buyer.getCompanyName()+" has been rejected.");
				map.put("buyerCountry", buyer.getRegistrationOfCountry().getCountryName());
				map.put("date", df.format(associatedBuyer.getRequestedDate()));
				map.put("rejectionDate", df.format(new Date()));
				map.put("buyerRemark", StringUtils.checkString(buyerRemark));
				if (StringUtils.checkString(supplier.getCommunicationEmail()).length() > 0) {
					sendEmail(supplier.getCommunicationEmail(), "Request Rejected", map, Global.REQUEST_ASSOCIATE_BUYER_REJECT_PH_3618);
				} else {
					LOG.warn("No communication email configured for user : " + supplier.getFullName() + "... Not going to send email notification");
				}
			}
		} catch (Exception e) {
			LOG.error("Error while Sending request email to associate buyer :" + e.getMessage(), e);
		}

	}

	@Override
	public void sendEmailToFavSupplier(Supplier supplier, Buyer buyer, TimeZone timeZone) {
		LOG.info("Sending Email Request to associated buyer (" + buyer.getFullName() + ") : " + buyer.getPublishedProfileCommunicationEmail());
		try {
			String subject = "Buyer Associated";
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("buyerName", buyer.getCompanyName());
			map.put("supplierName", supplier.getFullName());
			map.put("message", "You have been added as a supplier.");
			map.put("buyerCountry", buyer.getRegistrationOfCountry().getCountryName());
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
			df.setTimeZone(timeZone);
			map.put("date", df.format(new Date()));
			map.put("loginUrl", APP_URL + "/login");
			map.put("appUrl", APP_URL + "/login");

			if (StringUtils.checkString(supplier.getCommunicationEmail()).length() > 0) {
				sendEmail(supplier.getCommunicationEmail(), subject, map, Global.ADDED_FAV_SUPPLIER);
			} else {
				LOG.warn("No communication email configured for user : " + supplier.getFullName() + "... Not going to send email notification");
			}
		} catch (Exception e) {
			LOG.error("Error while Sending request email to associate buyer :" + e.getMessage(), e);
		}

	}

	@Override
	@Transactional(readOnly = false)
	public Budget doBudgetApproval(Budget budget, User actionBy, String remarks, boolean approved, HttpSession session) throws NotAllowedException {
		budget = budgetService.findById(budget.getId());

		List<BudgetApproval> approvalList = budget.getApprovals();

		// Identify Current Approval Level
		BudgetApproval currentLevel = null;
		for (BudgetApproval approval : approvalList) {
			if (approval.isActive()) {//
				currentLevel = approval;
				LOG.info("Current Approval Level : " + currentLevel.getLevel());
				break;
			}
		}

		// Identify actionUser in the ApprovalUser of current level
		BudgetApprovalUser actionUser = null;
		if (currentLevel != null) {
			for (BudgetApprovalUser user : currentLevel.getApprovalUsers()) {
				if (user.getUser().getId().equals(actionBy.getId())) {
					actionUser = user;
					LOG.info("Approval being done by : " + actionBy.getLoginId());
				}
			}
		}
		if (actionUser == null) {
			// throw error
			LOG.error("User " + actionBy.getName() + " is not allowed to Approve or Reject budget '" + budget.getBudgetName() + "' at approval level : " + currentLevel.getLevel());
			throw new NotAllowedException("User " + actionBy.getName() + " is not allowed to Approve or Reject budget '" + budget.getBudgetName() + "' at approval level : " + currentLevel.getLevel());
		}

		if (actionUser.getApprovalStatus() != ApprovalStatus.PENDING) {
			// throw error
			LOG.error("User " + actionBy.getName() + " has already " + actionUser.getApprovalStatus() + " Budget at : " + actionUser.getActionDate());
			throw new NotAllowedException("User " + actionBy.getName() + " has already " + actionUser.getApprovalStatus() + " Budget at : " + actionUser.getActionDate());
		}

		// adding remarks into comments
		if (budget.getBudgetComment() == null) {
			budget.setBudgetComment(new ArrayList<BudgetComment>());
		}
		BudgetComment budgetComment = new BudgetComment();
		budgetComment.setComment(remarks);
		budgetComment.setApproved(approved);
		budgetComment.setCreatedBy(actionBy);
		budgetComment.setCreatedDate(new Date());
		budgetComment.setBudget(budget);
		budgetComment.setApprovalUserId(actionUser.getId());
		budget.getBudgetComment().add(budgetComment);

		// If rejected
		if (!approved) {
			// For OR level Rejection should be handled differently
			// commented on user requirement don't want to hold if any user is rejecting

			// reset revision amount and justification if additional approval rejected budget
			if (null != budget.getRevisionJustification()) {
				budget.setRevisionAmount(null);
				budget.setRevisionJustification(null);
				budget.setConversionRate(null);
				budget.setToBusinessUnit(null);
				budget.setToCostCenter(null);

				Integer batchNo = 0;
				// get latest batch
				for (BudgetApproval approval : approvalList) {
					if (approval.getBatchNo() > batchNo) {
						batchNo = approval.getBatchNo();
					}
				}
				// Reset approvals. if additional approval rejects budget .
				for (Iterator<BudgetApproval> iterator = approvalList.iterator(); iterator.hasNext();) {
					BudgetApproval approval = iterator.next();
					LOG.info("additional reject ==========> " + approval.getBatchNo());
					if (batchNo == approval.getBatchNo()) {
						LOG.info("additional reject current==========> " + approval.getBatchNo());
						approval.setDone(false);
						approval.setActive(false);
						for (BudgetApprovalUser user : approval.getApprovalUsers()) {
							if (user.getApprovalStatus() == ApprovalStatus.PENDING) {
								// Send rejection email
								sentBudgetRejectionEmail(user.getUser(), budget.getId(), budget.getBudgetName(), actionBy, remarks, budget.getCreatedBy(), budget, budget.getBudgetId());
							}
							user.setActionDate(null);
							// user.setApprovalStatus(ApprovalStatus.PENDING);
							user.setRemarks(null);
							user.setActionDate(null);
						}
					}
				}

				actionUser.setApprovalStatus(ApprovalStatus.REJECTED);
				actionUser.setActionDate(new Date());
				actionUser.setRemarks(remarks);

				Date now = new Date();
				if (budget.getValidFrom() != null && now.after(budget.getValidFrom())) {
					LOG.info("Changing BUDGET status APPROVED to ACTIVE " + budget.getId());
					LOG.info("additional approval User " + actionBy.getName() + " has Rejected the budget : " + budget.getBudgetName());
					budget.setBudgetStatus(BudgetStatus.ACTIVE);

				} else {
					LOG.info("additional approval User " + actionBy.getName() + " has Rejected the budget : " + budget.getBudgetName());
					budget.setBudgetStatus(BudgetStatus.APPROVED);

				}
				// removed additional approvals
				// budget.setApprovals(approvalList);
				try {
					if (budget.getCreatedBy() != null && !actionBy.equals(budget.getCreatedBy())) {
						LOG.info("Sending rejected request email to owner : " + budget.getCreatedBy().getCommunicationEmail());
						sentBudgetRejectionEmail(budget.getCreatedBy(), budget.getId(), budget.getBudgetName(), actionBy, remarks, budget.getCreatedBy(), budget, budget.getBudgetId());
					}
				} catch (Exception e) {
					LOG.info("ERROR while Sending mail :" + e.getMessage(), e);
				}

			} else {
				// if rejected by original budget approval
				// Reset all approvals for re-approval as the budget is rejected.
				for (BudgetApproval approval : approvalList) {
					approval.setDone(false);
					approval.setActive(false);
					for (BudgetApprovalUser user : approval.getApprovalUsers()) {
						if (user.getApprovalStatus() == ApprovalStatus.PENDING) {
							// Send rejection email
							sentBudgetRejectionEmail(user.getUser(), budget.getId(), budget.getBudgetName(), actionBy, remarks, budget.getCreatedBy(), budget, budget.getBudgetId());
						}
						user.setActionDate(null);
						user.setApprovalStatus(ApprovalStatus.PENDING);
						user.setRemarks(null);
						user.setActionDate(null);
					}
				}
				actionUser.setApprovalStatus(ApprovalStatus.REJECTED);
				actionUser.setActionDate(new Date());
				actionUser.setRemarks(remarks);

				LOG.info("User " + actionBy.getName() + " has Rejected the budget : " + budget.getBudgetName());
				budget.setBudgetStatus(BudgetStatus.REJECTED);

				// snapShotAuditService.doRfaAudit(event, session, event, actionBy, AuditActionType.Reject,
				// "event.audit.rejected");
				try {
					if (budget.getCreatedBy() != null && !actionBy.equals(budget.getCreatedBy())) {
						LOG.info("Sending rejected request email to owner : " + budget.getCreatedBy().getCommunicationEmail());
						sentBudgetRejectionEmail(budget.getCreatedBy(), budget.getId(), budget.getBudgetName(), actionBy, remarks, budget.getCreatedBy(), budget, budget.getBudgetId());
					}
				} catch (Exception e) {
					LOG.info("ERROR while Sending mail :" + e.getMessage(), e);
				}
			}

		} else {
			LOG.info("User " + actionBy.getName() + " has Approved the budget : " + budget.getBudgetName());
			actionUser.setApprovalStatus(ApprovalStatus.APPROVED);
			actionUser.setActionDate(new Date());
			actionUser.setRemarks(remarks);

			// Send email notification to Creator
			sentBudgetApprovalEmail(budget.getCreatedBy(), budget.getId(), budget.getBudgetName(), actionBy, remarks, budget.getCreatedBy(), budget, false, budget.getBudgetId());
			// Send email notification to actionBy
			sentBudgetApprovalEmail(actionBy, budget.getId(), budget.getBudgetName(), actionBy, remarks, budget.getCreatedBy(), budget, true, budget.getBudgetId());

			if (ApprovalType.OR == currentLevel.getApprovalType()) {
				LOG.info("This level has OR set for approval. Marking level as done");
				setNextOrAllDoneForBudget(actionBy, approvalList, currentLevel, budget, session, actionBy);
			} else {
				// AND Operation
				LOG.info("This level has AND set for approvals");
				boolean allUsersDone = true;
				if (currentLevel != null) {
					for (BudgetApprovalUser user : currentLevel.getApprovalUsers()) {
						if (ApprovalStatus.PENDING == user.getApprovalStatus() || ApprovalStatus.REJECTED == user.getApprovalStatus()) {
							allUsersDone = false;
							LOG.info("All users of this level have not approved the Budget.");
							break;
						}
					}
				}
				if (allUsersDone) {
					LOG.info("All users of this level have approved the Budget.");
					setNextOrAllDoneForBudget(actionBy, approvalList, currentLevel, budget, session, actionBy);
				}
			}
		}
		return budgetDao.update(budget);
	}

	private void sentBudgetRejectionEmail(User mailTo, String id, String budgetName, User actionBy, String remarks, User ownerUser, Budget budget, String budgetId) {
		LOG.info("Sending rejected request email to (" + mailTo.getName() + ") : " + mailTo.getCommunicationEmail());

		String subject = "Budget Rejected";
		String url = APP_URL + "/admin/budgets/viewBudget/" + id;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("userName", mailTo.getName());
		map.put("actionByName", actionBy.getName());
		map.put("budgetName", budgetName);
		// map.put("eventType", type.name());
		map.put("remarks", remarks);
		map.put("budgetId", budgetId);
		map.put("businessUnit", StringUtils.checkString(budget.getBusinessUnit().getUnitName()));
		if (mailTo.getId().equals(actionBy.getId())) {
			map.put("message", "You have Rejected");
		} else {
			map.put("message", actionBy.getName() + " has Rejected");
		}
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		String timeZone = "GMT+8:00";
		timeZone = getTimeZoneByBuyerSettings(mailTo.getTenantId(), timeZone);
		df.setTimeZone(TimeZone.getTimeZone(timeZone));
		map.put("date", df.format(new Date()));
		map.put("appUrl", url);
		map.put("loginUrl", APP_URL + "/login");
		if (StringUtils.checkString(mailTo.getCommunicationEmail()).length() > 0) {
			sendEmail(mailTo.getCommunicationEmail(), subject, map, Global.BUDGET_REJECT_TEMPLATE);
		} else {
			LOG.warn("No communication email configured for user : " + mailTo.getLoginId() + "... Not going to send email notification");
		}

		String notificationMessage = messageSource.getMessage("event.rejection.notification.message", new Object[] { actionBy.getName(), budgetId, budgetName, remarks }, Global.LOCALE);
		sendDashboardNotification(mailTo, url, subject, notificationMessage, NotificationType.REJECT_MESSAGE);

		if (StringUtils.checkString(mailTo.getDeviceId()).length() > 0) {
			try {
				LOG.info("User '" + mailTo.getCommunicationEmail() + "' and device Id :" + mailTo.getDeviceId());
				Map<String, String> payload = new HashMap<String, String>();
				payload.put("id", id);
				payload.put("messageType", NotificationType.REJECT_MESSAGE.toString());
				// payload.put("eventType", type.name());
				notificationService.pushOneSignalNotification(notificationMessage, null, payload, Arrays.asList(mailTo.getDeviceId()));
			} catch (Exception e) {
				LOG.error("Error While sending Event reject Mobile push notification to '" + mailTo.getCommunicationEmail() + "' : " + e.getMessage(), e);
			}
		} else {
			LOG.info("User '" + mailTo.getCommunicationEmail() + "' Device Id is Null");
		}
	}

	private void sentBudgetApprovalEmail(User mailTo, String id, String budgetName, User actionBy, String remarks, User ownerUser, Budget budget, boolean self, String budgetId) {
		LOG.info("Sending approval email to (" + mailTo.getName() + ") : " + mailTo.getCommunicationEmail());

		String subject = "Budget Approved";
		String url = APP_URL + "/admin/budgets/viewBudget/" + budget.getId();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("userName", mailTo.getName());
		map.put("actionByName", actionBy.getName());
		map.put("budgetName", budgetName);
		// map.put("eventType", type.name());
		map.put("remarks", remarks);
		map.put("budgetId", budgetId);
		map.put("businessUnit", StringUtils.checkString(budget.getBusinessUnit().getUnitName()));
		if (self) {
			map.put("message", "You have Approved");
		} else {
			map.put("message", actionBy.getName() + " has Approved");
		}
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		String timeZone = "GMT+8:00";
		timeZone = getTimeZoneByBuyerSettings(mailTo.getTenantId(), timeZone);
		df.setTimeZone(TimeZone.getTimeZone(timeZone));
		map.put("date", df.format(new Date()));
		map.put("appUrl", url);
		map.put("loginUrl", APP_URL + "/login");
		if (StringUtils.checkString(mailTo.getCommunicationEmail()).length() > 0) {
			sendEmail(mailTo.getCommunicationEmail(), subject, map, Global.BUDGET_APPROVAL_TEMPLATE);
		} else {
			LOG.warn("No communication email configured for user : " + mailTo.getLoginId() + "... Not going to send email notification");
		}

		String notificationMessage = messageSource.getMessage("event.approval.notification.message", new Object[] { actionBy.getName(), budgetId, remarks }, Global.LOCALE);
		sendDashboardNotification(mailTo, url, subject, notificationMessage, NotificationType.APPROVAL_MESSAGE);

		if (StringUtils.checkString(mailTo.getDeviceId()).length() > 0) {
			try {
				LOG.info("User '" + mailTo.getCommunicationEmail() + "' and device Id :" + mailTo.getDeviceId());
				Map<String, String> payload = new HashMap<String, String>();
				payload.put("id", id);
				payload.put("messageType", NotificationType.APPROVAL_MESSAGE.toString());
				// payload.put("eventType", type.name());
				notificationService.pushOneSignalNotification(notificationMessage, null, payload, Arrays.asList(mailTo.getDeviceId()));
			} catch (Exception e) {
				LOG.error("Error While sending Budget approve Mobile push notification to '" + mailTo.getCommunicationEmail() + "' : " + e.getMessage(), e);
			}
		} else {
			LOG.info("User '" + mailTo.getCommunicationEmail() + "' Device Id is Null");
		}
	}

	private void setNextOrAllDoneForBudget(User actionBy, List<BudgetApproval> approvalList, BudgetApproval currentLevel, Budget budget, HttpSession session, User loggedInUser) {
		// Check if all approvals are done
		currentLevel.setDone(true);
		currentLevel.setActive(false);
		// Check if all approvals are done
		if (currentLevel.getLevel() == approvalList.size()) {
			// all approvals done
			LOG.info("All approvals for this budget is done!!!. Going to Approved Mode.");
			budget.setBudgetStatus(BudgetStatus.APPROVED);
			budget.setModifiedBy(actionBy);
			budget.setModifiedDate(new Date());

			// reset revision amount and justification
			if (null != budget.getRevisionJustification()) {
				// budget add/deduct/transfer
				TransactionLog transactionLog = new TransactionLog();
				transactionLog.setBudget(budget);
				transactionLog.setReferanceNumber(budget.getBudgetId());
				transactionLog.setTenantId(SecurityLibrary.getLoggedInUserTenantId());
				transactionLog.setTransactionTimeStamp(new Date());
				// if Add
				if (1 == budget.getRevisionAmount().compareTo(budget.getTotalAmount())) {
					BigDecimal addAmount = budget.getRevisionAmount().subtract(budget.getTotalAmount());
					// add amount
					budget.setRemainingAmount((addAmount).add(budget.getRemainingAmount()));
					budget.setTotalAmount(budget.getRevisionAmount());
					budget.setRevisionAmount(null);
					budget.setRevisionJustification(null);
					// save transaction for Add
					transactionLog.setAddAmount(addAmount);
					transactionLog.setRemainingAmount(budget.getRemainingAmount());
					transactionLog.setTransactionLogStatus(TransactionLogStatus.ADD);
					transactionLogService.saveTransactionLog(transactionLog);

				} else if (-1 == budget.getRevisionAmount().compareTo(budget.getTotalAmount()) && null == budget.getToBusinessUnit()) {
					// if deduct
					BigDecimal deductAmount = budget.getTotalAmount().subtract(budget.getRevisionAmount());
					budget.setRemainingAmount(budget.getRemainingAmount().subtract(deductAmount));
					budget.setTotalAmount(budget.getTotalAmount().subtract(deductAmount));
					budget.setRevisionAmount(null);
					budget.setRevisionJustification(null);
					// save transaction for deduct
					transactionLog.setDeductAmount(deductAmount);
					transactionLog.setRemainingAmount(budget.getRemainingAmount());
					transactionLog.setTransactionLogStatus(TransactionLogStatus.DEDUCT);
					transactionLogService.saveTransactionLog(transactionLog);
				} else if (-1 == budget.getRevisionAmount().compareTo(budget.getTotalAmount()) && null != budget.getToBusinessUnit() && null != budget.getToCostCenter()) {
					// if transfer
					// Budget budgetBUCC = budgetService.findTransferToBudgetById(budget.getId());
					Budget toBudget = budgetService.findBudgetByBusinessUnitAndCostCenter(budget.getToBusinessUnit().getId(), budget.getToCostCenter().getId());
					BigDecimal changeAmount = budget.getTotalAmount().subtract(budget.getRevisionAmount());
					if (budget.getConversionRate() != null) {
						BigDecimal changeAmountAfterConversion = budget.getConversionRate().multiply(changeAmount);
						LOG.info("***************change amount " + changeAmount);
						LOG.info("*************** changeAmountAfterConversion " + changeAmountAfterConversion);
						// save toBudget for add transfer amount
						if (toBudget != null) {
							toBudget.setTotalAmount(toBudget.getTotalAmount().add(changeAmountAfterConversion));
							toBudget.setRemainingAmount(toBudget.getRemainingAmount().add(changeAmountAfterConversion));
							budgetDao.update(toBudget);
						}
					} else {
						if (toBudget != null) {
							toBudget.setTotalAmount(toBudget.getTotalAmount().add(changeAmount));
							toBudget.setRemainingAmount(toBudget.getRemainingAmount().add(changeAmount));
							budgetDao.update(toBudget);
						}
					}
					// save transaction for add transfer amount
					TransactionLog addBudgetTransactionLog = new TransactionLog();
					if (toBudget != null) {
						addBudgetTransactionLog.setBudget(toBudget);
						addBudgetTransactionLog.setReferanceNumber(toBudget.getBudgetId());
					} else {
						LOG.warn("Budget details not found ");
					}
					addBudgetTransactionLog.setTenantId(SecurityLibrary.getLoggedInUserTenantId());
					addBudgetTransactionLog.setTransactionTimeStamp(new Date());
					addBudgetTransactionLog.setAddAmount(changeAmount);
					if (budget.getConversionRate() != null) {
						addBudgetTransactionLog.setConversionRateAmount(budget.getConversionRate());
						addBudgetTransactionLog.setAfterConversionAmount(budget.getConversionRate().multiply(changeAmount));
					}
					addBudgetTransactionLog.setRemainingAmount(toBudget.getRemainingAmount());
					addBudgetTransactionLog.setTransactionLogStatus(TransactionLogStatus.TRANSFER);
					addBudgetTransactionLog.setFromBusinessUnit(budget.getBusinessUnit());
					transactionLogService.saveTransactionLog(addBudgetTransactionLog);

					// save budget for deduct transfer amount
					budget.setRemainingAmount(budget.getRemainingAmount().subtract(changeAmount));
					// budget.setTotalAmount(budget.getRevisionAmount());
					budget.setTransferAmount(budget.getTransferAmount().add(changeAmount));
					budget.setRevisionAmount(null);
					budget.setRevisionJustification(null);
					budget.setConversionRate(null);
					budget.setToBusinessUnit(null);
					budget.setToCostCenter(null);
					budgetDao.update(budget);

					// save transaction for deduct transfer amount
					TransactionLog deductBudgetTransactionLog = new TransactionLog();
					deductBudgetTransactionLog.setBudget(budget);
					deductBudgetTransactionLog.setReferanceNumber(budget.getBudgetId());
					deductBudgetTransactionLog.setTenantId(SecurityLibrary.getLoggedInUserTenantId());
					deductBudgetTransactionLog.setTransactionTimeStamp(new Date());
					deductBudgetTransactionLog.setDeductAmount(changeAmount);
					if (budget.getConversionRate() != null) {
						deductBudgetTransactionLog.setConversionRateAmount(addBudgetTransactionLog.getConversionRateAmount());
						deductBudgetTransactionLog.setAfterConversionAmount(addBudgetTransactionLog.getAfterConversionAmount());
					}
					deductBudgetTransactionLog.setRemainingAmount(budget.getRemainingAmount());
					deductBudgetTransactionLog.setTransactionLogStatus(TransactionLogStatus.TRANSFER);
					deductBudgetTransactionLog.setToBusinessUnit(toBudget.getBusinessUnit());
					deductBudgetTransactionLog.setCostCenter(toBudget.getCostCenter());
					transactionLogService.saveTransactionLog(deductBudgetTransactionLog);

				}
			}

			// snapShotAuditService.doRftAudit(budget, session, budget, actionBy, AuditActionType.Approve,
			// "event.audit.approved");
			LOG.info("All approvals for this budget is in Approved Mode.");

			try {
				// LOG.info("publishing rft event to epiportal");
				// publishEventService.pushRftEvent(budget.getId(), actionBy.getBuyer().getId(), true);
			} catch (Exception e) {
				LOG.error("Error while publishing RFT event to EPortal:" + e.getMessage(), e);
			}

		} else {
			for (BudgetApproval approval : approvalList) {
				if (approval.getLevel() == currentLevel.getLevel() + 1) {
					LOG.info("Setting Approval level " + approval.getLevel() + " as Active level");
					approval.setActive(true);
					for (BudgetApprovalUser nextLevelUser : approval.getApprovalUsers()) {
						LOG.info("Sending email for next approver level user:" + nextLevelUser.getUser().getLoginId());
						sendBudgetApprovalRequestEmails(budget, nextLevelUser);
					}
					break;
				}
			}
		}
	}

	private void sendBudgetApprovalRequestEmails(Budget budget, ApprovalUser user) {
		LOG.info("UnitName" + budget.getBusinessUnit().getUnitName());
		String mailTo = user.getUser().getCommunicationEmail();
		String subject = "Budget Approval Request";
		String url = APP_URL + "/admin/budgets/viewBudget/" + budget.getId();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("userName", user.getUser().getName());
		map.put("budgetName", budget.getBudgetName());
		map.put("budgetId", budget.getBudgetId());
		map.put("businessUnit", budget.getBusinessUnit().getUnitName());
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		String timeZone = "GMT+8:00";
		timeZone = getTimeZoneByBuyerSettings(user.getUser().getTenantId(), timeZone);
		df.setTimeZone(TimeZone.getTimeZone(timeZone));
		map.put("date", df.format(new Date()));
		map.put("appUrl", url);
		map.put("loginUrl", APP_URL + "/login");
		if (StringUtils.checkString(mailTo).length() > 0) {
			sendEmail(mailTo, subject, map, Global.BUDGET_APPROVAL_REQUEST_TEMPLATE);
		} else {
			LOG.warn("No communication email configured for user : " + user.getUser().getLoginId() + "... Not going to send email notification");
		}

		String notificationMessage = messageSource.getMessage("event.approval.request.notification.message", new Object[] { "Budget", budget.getBudgetName() }, Global.LOCALE);
		sendDashboardNotification(user.getUser(), url, subject, notificationMessage, NotificationType.APPROVAL_MESSAGE);

		if (StringUtils.checkString(user.getUser().getDeviceId()).length() > 0) {
			try {
				LOG.info("User '" + user.getUser().getCommunicationEmail() + "' and device Id :" + user.getUser().getDeviceId());
				Map<String, String> payload = new HashMap<String, String>();
				payload.put("id", budget.getId());
				payload.put("messageType", NotificationType.APPROVAL_MESSAGE.toString());
				notificationService.pushOneSignalNotification(notificationMessage, null, payload, Arrays.asList(user.getUser().getDeviceId()));
			} catch (Exception e) {
				LOG.error("Error While sending Event approve Mobile push notification to '" + user.getUser().getCommunicationEmail() + "' : " + e.getMessage(), e);
			}
		} else {
			LOG.info("User '" + user.getUser().getCommunicationEmail() + "' Device Id is Null");
		}
	}

	public void sentBudgetUtilizedNotifications(Budget budget, BigDecimal percentageUtilized) {

		List<User> budgetCreatorAndApprovals = new ArrayList<User>();
		budgetCreatorAndApprovals.add(budget.getCreatedBy());

		if (CollectionUtil.isNotEmpty(budget.getApprovals())) {
			for (BudgetApproval budgetApproval : budget.getApprovals()) {
				for (BudgetApprovalUser budgetApprovalUser : budgetApproval.getApprovalUsers()) {
					if (!budgetCreatorAndApprovals.contains(budgetApprovalUser.getUser())) {
						budgetCreatorAndApprovals.add(budgetApprovalUser.getUser());
					}
				}
			}
		}
		// send notifications to creator
		LOG.info("Sending budget utilized email to (" + budget.getCreatedBy().getName() + ") : " + budget.getCreatedBy().getCommunicationEmail());
		String subject = "Budget Utilization";
		String url = APP_URL + "/admin/budgets/viewBudget/" + budget.getId();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("percentageUtilized", percentageUtilized);
		map.put("budgetName", budget.getBudgetName());
		map.put("budgetId", budget.getBudgetId());
		map.put("businessUnit", StringUtils.checkString(budget.getBusinessUnit().getUnitName()));
		map.put("costCenter", StringUtils.checkString(budget.getCostCenter().getCostCenter()));

		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		String timeZone = "GMT+8:00";
		timeZone = getTimeZoneByBuyerSettings(budget.getCreatedBy().getTenantId(), timeZone);
		df.setTimeZone(TimeZone.getTimeZone(timeZone));
		map.put("date", df.format(new Date()));
		map.put("appUrl", url);
		map.put("loginUrl", APP_URL + "/login");

		String notificationMessage = messageSource.getMessage("budget.utilization.notification.message", new Object[] { percentageUtilized }, Global.LOCALE);
		for (User mailTo : budgetCreatorAndApprovals) {
			map.put("userName", mailTo.getName());
			if (StringUtils.checkString(mailTo.getCommunicationEmail()).length() > 0) {
				sendEmail(mailTo.getCommunicationEmail(), subject, map, Global.BUDGET_UTILIZED_TEMPLATE);
			} else {
				LOG.warn("No communication email configured for user : " + budget.getCreatedBy().getLoginId() + "... Not going to send budget utilized email notification");
			}
			sendDashboardNotification(mailTo, url, subject, notificationMessage, NotificationType.APPROVAL_MESSAGE);

			if (StringUtils.checkString(mailTo.getDeviceId()).length() > 0) {
				try {
					LOG.info("User '" + mailTo.getCommunicationEmail() + "' and device Id :" + mailTo.getDeviceId());
					Map<String, String> payload = new HashMap<String, String>();
					payload.put("id", budget.getId());
					payload.put("messageType", NotificationType.APPROVAL_MESSAGE.toString());
					notificationService.pushOneSignalNotification(notificationMessage, null, payload, Arrays.asList(mailTo.getDeviceId()));
				} catch (Exception e) {
					LOG.error("Error While sending Budget utilzed Mobile push notification to '" + mailTo.getCommunicationEmail() + "' : " + e.getMessage(), e);
				}
			} else {
				LOG.info("User '" + mailTo.getCommunicationEmail() + "' Device Id is Null");
			}
		}

	}

	@Override
	public void sendBudgetOverrunNotification(Budget budget) {
		List<User> budgetCreatorAndApprovals = new ArrayList<User>();
		budgetCreatorAndApprovals.add(budget.getCreatedBy());

		if (CollectionUtil.isNotEmpty(budget.getApprovals())) {
			for (BudgetApproval budgetApproval : budget.getApprovals()) {
				for (BudgetApprovalUser budgetApprovalUser : budgetApproval.getApprovalUsers()) {
					if (!budgetCreatorAndApprovals.contains(budgetApprovalUser.getUser())) {
						budgetCreatorAndApprovals.add(budgetApprovalUser.getUser());
					}
				}
			}
		}
		// send notifications to creator
		LOG.info("Sending budget overrun email to (" + budget.getCreatedBy().getName() + ") : " + budget.getCreatedBy().getCommunicationEmail());
		String subject = "Budget Overrun";
		String url = APP_URL + "/admin/budgets/viewBudget/" + budget.getId();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("budgetName", budget.getBudgetName());
		map.put("budgetId", budget.getBudgetId());
		map.put("businessUnit", StringUtils.checkString(budget.getBusinessUnit().getUnitName()));
		map.put("costCenter", StringUtils.checkString(budget.getCostCenter().getCostCenter()));

		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		String timeZone = "GMT+8:00";
		timeZone = getTimeZoneByBuyerSettings(budget.getCreatedBy().getTenantId(), timeZone);
		df.setTimeZone(TimeZone.getTimeZone(timeZone));
		map.put("date", df.format(new Date()));
		map.put("appUrl", url);
		map.put("loginUrl", APP_URL + "/login");

		String notificationMessage = messageSource.getMessage("budget.overrun.notification.message", new Object[] {}, Global.LOCALE);
		for (User mailTo : budgetCreatorAndApprovals) {
			map.put("userName", mailTo.getName());
			if (StringUtils.checkString(mailTo.getCommunicationEmail()).length() > 0) {
				LOG.info("Sending budget overrun email to (" + mailTo.getName() + ") : " + mailTo.getCommunicationEmail());
				sendEmail(mailTo.getCommunicationEmail(), subject, map, Global.BUDGET_OVERRUN_TEMPLATE);
			} else {
				LOG.warn("No communication email configured for user : " + budget.getCreatedBy().getLoginId() + "... Not going to send budget overrun email notification");
			}
			sendDashboardNotification(mailTo, url, subject, notificationMessage, NotificationType.APPROVAL_MESSAGE);

			if (StringUtils.checkString(mailTo.getDeviceId()).length() > 0) {
				try {
					LOG.info("User '" + mailTo.getCommunicationEmail() + "' and device Id :" + mailTo.getDeviceId());
					Map<String, String> payload = new HashMap<String, String>();
					payload.put("id", budget.getId());
					payload.put("messageType", NotificationType.APPROVAL_MESSAGE.toString());
					notificationService.pushOneSignalNotification(notificationMessage, null, payload, Arrays.asList(mailTo.getDeviceId()));
				} catch (Exception e) {
					LOG.error("Error While sending Budget overrun Mobile push notification to '" + mailTo.getCommunicationEmail() + "' : " + e.getMessage(), e);
				}
			} else {
				LOG.info("User '" + mailTo.getCommunicationEmail() + "' Device Id is Null");
			}
		}

	}

	@Override
	public void sendBudgetApprovalReqEmailsOnCreate(Budget budget, BudgetApprovalUser nextLevelUser) {
		sendBudgetApprovalRequestEmails(budget, nextLevelUser);
	}

	@Override
	public void sendPoSupplierActionEmailNotificationToBuyer(Supplier supplier, boolean isAccept, Po po, String supplierRemark) {
		LOG.info("Sending supplier po action email to (" + po.getCreatedBy().getName() + ") : " + po.getCreatedBy().getCommunicationEmail());
		List<User> userMailList = new ArrayList<User>();
		if (po.getCreatedBy() != null) {
			userMailList.add(po.getCreatedBy());
		}
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
		String timeZone = "GMT+8:00";
		timeZone = getTimeZoneByBuyerSettings(po.getCreatedBy().getTenantId(), timeZone);
		df.setTimeZone(TimeZone.getTimeZone(timeZone));
		sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
		String subject = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		if (isAccept) {
			subject = "PO Accepted";
			map.put("message", "Supplier " + supplier.getCompanyName() + " has accepted the PO " + po.getName());
		} else {
			subject = "PO Declined";
			map.put("message", "Supplier " + supplier.getCompanyName() + " has declined the PO " + po.getName());
		}

		map.put("supplierName", supplier.getFullName());
		map.put("supplierLoginEmail", supplier.getLoginEmail());
		map.put("acceptRejectDate", sdf.format(new Date()));
		map.put("date", df.format(new Date()));
		map.put("supplierRemark", StringUtils.checkString(supplierRemark).length() > 0 ? supplierRemark : "N/A");
		map.put("poDate", sdf.format(po.getCreatedDate()));
		map.put("poNumber", po.getPoNumber());
		map.put("isAccept", isAccept ? "Acceptance" : "Declined");
		map.put("loginUrl", APP_URL + "/login");
		map.put("appUrl", APP_URL + "/login");

		if (CollectionUtil.isNotEmpty(userMailList)) {
			for (User mailTo : userMailList) {
				if (StringUtils.checkString(mailTo.getCommunicationEmail()).length() > 0) {
					map.put("userName", mailTo.getName());
					sendEmail(mailTo.getCommunicationEmail(), subject, map, Global.PO_ACCEPT_REJECT_TEMPLATE);
				} else {
					LOG.warn("No communication email configured for user : " + mailTo.getLoginId() + "... Not going to send email notification");
				}
			}
		}

	}

	@Override
	public void sendCancelPoEmailNotificationToSupplier(Po po, String poRemarks, User actionBy) {
		LOG.info("Sending buyer cancel po  email to supplier (" + po.getSupplier().getSupplier().getFullName() + ") : " + po.getSupplier().getSupplier().getCommunicationEmail());

		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
		String timeZone = "GMT+8:00";
		timeZone = getTimeZoneByBuyerSettings(actionBy.getTenantId(), timeZone);
		df.setTimeZone(TimeZone.getTimeZone(timeZone));
		sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
		String subject = "PO Cancelled";
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("supplierName", po.getSupplier().getSupplier().getFullName());
		map.put("buyerLoginEmail", actionBy.getLoginId());
		map.put("date", df.format(new Date()));
		map.put("cancellationDate", sdf.format(new Date()));
		map.put("buyerRemark", StringUtils.checkString(poRemarks).length() > 0 ? poRemarks : "N/A");
		map.put("poDate", sdf.format(po.getCreatedDate()));
		map.put("poNumber", po.getPoNumber());
		map.put("buyerCompanyName", po.getBuyer().getCompanyName());
		map.put("businessUnit", po.getBusinessUnit().getUnitName());
		map.put("poName", po.getName());
		map.put("buyerName", actionBy.getName());
		map.put("loginUrl", APP_URL + "/login");
		map.put("appUrl", APP_URL + "/login");
		if (StringUtils.checkString(po.getSupplier().getSupplier().getCommunicationEmail()).length() > 0) {
			sendEmail(po.getSupplier().getSupplier().getCommunicationEmail(), subject, map, Global.PO_CANCEL_TEMPLATE);
		} else {
			LOG.warn("No communication email configured for user : " + po.getSupplier().getSupplier().getLoginEmail() + "... Not going to send email notification");
		}
	}

	@Override
	public void sendPoReceivedEmailNotificationToSupplier(Po po, User actionBy) {

		if (po.getSupplier() != null && po.getSupplier().getSupplier() != null && StringUtils.checkString(po.getSupplier().getSupplier().getCommunicationEmail()).length() > 0) {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
			String timeZone = "GMT+8:00";
			timeZone = getTimeZoneByBuyerSettings(po.getCreatedBy().getTenantId(), timeZone);
			df.setTimeZone(TimeZone.getTimeZone(timeZone));
			sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
			String subject = "PO Received";
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("supplierName", (po.getSupplier() != null ? po.getSupplier().getSupplier().getFullName() : po.getSupplierName()));
			map.put("buyerLoginEmail", actionBy.getLoginId());
			map.put("date", df.format(new Date()));
			map.put("poDate", sdf.format(po.getCreatedDate()));
			map.put("deliveryDate", sdf.format(po.getDeliveryDate()));
			map.put("poNumber", po.getPoNumber());
			map.put("buyerCompanyName", po.getBuyer().getCompanyName());
			map.put("businessUnit", po.getBusinessUnit().getUnitName());
			map.put("poName", po.getName());
			map.put("buyerName", actionBy.getName());
			map.put("loginUrl", APP_URL + "/login");
			map.put("appUrl", APP_URL + "/login");
			LOG.info("Sending po received email to supplier (" + po.getSupplier().getSupplier().getFullName() + ") : " + po.getSupplier().getSupplier().getCommunicationEmail());
			sendEmail(po.getSupplier().getSupplier().getCommunicationEmail(), subject, map, Global.PO_RECEIVED_TEMPLATE);
		} else {
			LOG.warn("No communication email configured for user : " + po.getSupplier().getSupplier().getLoginEmail() + "... Not going to send email notification");
		}
	}

	@Override
	public void sendPoCreatedEmailToCreater(User mailTo, Pr pr, User actionBy) {
		sendPoCreatedEmail(mailTo, pr, actionBy);

	}

	@Override
	public void sharePoToFinance(Po po) {
		sharePrtoFinance(po);
	}

	@Override
	@Transactional(readOnly = false)
	public SupplierFormSubmition doApprovalFormSubmition(SupplierFormSubmition supplierFormSubmition, User actionBy, String remarks, boolean approved) throws NotAllowedException {
		supplierFormSubmition = supplierFormSubmissionDao.findById(supplierFormSubmition.getId());
		List<SupplierFormSubmitionApproval> approvalList = supplierFormSubmition.getApprovals();

		// Identify Current Approval Level
		SupplierFormSubmitionApproval currentLevel = null;
		for (SupplierFormSubmitionApproval sourcingFormApproval : approvalList) {
			if (sourcingFormApproval.isActive()) {
				currentLevel = sourcingFormApproval;
				LOG.info("Current Approval Level : " + currentLevel.getLevel());
				break;
			}
		}

		// Identify actionUser in the ApprovalUser of current level
		SupplierFormSubmitionApprovalUser actionUser = null;
		if (currentLevel != null) {
			for (SupplierFormSubmitionApprovalUser user : currentLevel.getApprovalUsers()) {
				if (user.getUser().getId().equals(actionBy.getId())) {
					actionUser = user;
					LOG.info("Approval being done by : " + actionBy.getLoginId());
				}
			}
		}
		if (actionUser == null) {
			// throw error
			LOG.error("User " + actionBy.getName() + " is not allowed to Approve or Reject Supplier Form '" + supplierFormSubmition.getName() + "'");
			throw new NotAllowedException("User " + actionBy.getName() + " is not allowed to Approve or Reject Supplier Form '" + supplierFormSubmition.getName() + "'");
		}

		if (actionUser.getApprovalStatus() != ApprovalStatus.PENDING) {
			// throw error
			LOG.error("User " + actionBy.getName() + " has already " + actionUser.getApprovalStatus() + " Supplier Form at : " + actionUser.getActionDate());
			throw new NotAllowedException("User " + actionBy.getName() + " has already " + actionUser.getApprovalStatus() + " Supplier Form at : " + actionUser.getActionDate());
		}

		// adding remarks into comments
		if (supplierFormSubmition.getFormComments() == null) {
			supplierFormSubmition.setFormComments(new ArrayList<SupplierFormSubmitionComment>());
		}
		SupplierFormSubmitionComment formComment = new SupplierFormSubmitionComment();
		formComment.setComment(remarks);
		formComment.setApproved(approved);
		formComment.setCreatedBy(actionBy);
		formComment.setCreatedDate(new Date());
		formComment.setSupplierFormSubmition(supplierFormSubmition);
		formComment.setApprovalUserId(actionUser.getId());
		supplierFormSubmition.getFormComments().add(formComment);

		// If rejected
		if (!approved) {

			actionUser.setApprovalStatus(ApprovalStatus.REJECTED);
			actionUser.setActionDate(new Date());
			actionUser.setRemarks(remarks);
			LOG.info("User " + actionBy.getName() + " has Rejected the Supplier Form : " + supplierFormSubmition.getName());
			supplierFormSubmition.setApprovalStatus(SupplierFormApprovalStatus.REJECTED);

			for (SupplierFormSubmitionApproval supplierFormApproval : approvalList) {
				if (Boolean.TRUE == supplierFormApproval.isActive()) {
					// Send email notification to Approval Active level User
					for (SupplierFormSubmitionApprovalUser formAppUser : supplierFormApproval.getApprovalUsers()) {
						if (!supplierFormSubmition.getRequestedBy().getId().equals(formAppUser.getUser().getId())) {
							sendSupplierFormRejectionEmail(formAppUser.getUser(), supplierFormSubmition, actionBy, remarks);

						}
					}
				}
				supplierFormApproval.setActive(false);
			}
			// Send email notification to Form owner
			if (supplierFormSubmition.getRequestedBy() != null) {
				sendSupplierFormRejectionEmail(supplierFormSubmition.getRequestedBy(), supplierFormSubmition, actionBy, remarks);
			}

		} else {
			LOG.info("User " + actionBy.getName() + " has Approved the Supplier Form : " + supplierFormSubmition.getName());
			actionUser.setApprovalStatus(ApprovalStatus.APPROVED);
			actionUser.setActionDate(new Date());
			actionUser.setRemarks(remarks);

			// Send email notification to Form owner
			if (supplierFormSubmition.getRequestedBy() != null) {
				sendSupplierFormApprovalEmail(supplierFormSubmition.getRequestedBy(), supplierFormSubmition, actionBy, remarks);
			}
			// Send email notification to Approval Action User
			if (!supplierFormSubmition.getRequestedBy().getId().equals(actionBy.getId())) {
				sendSupplierFormApprovalEmail(actionBy, supplierFormSubmition, actionBy, remarks);
			}

			if (ApprovalType.OR == currentLevel.getApprovalType()) {
				LOG.info("This level has OR set for approval. Marking level as done");
				setNextOrAllDoneForSupplierForm(actionBy, approvalList, currentLevel, supplierFormSubmition);
			} else {
				// AND Operation
				LOG.info("This level has AND set for approvals");
				boolean allUsersDone = true;
				if (currentLevel != null) {
					for (SupplierFormSubmitionApprovalUser user : currentLevel.getApprovalUsers()) {
						if (ApprovalStatus.PENDING == user.getApprovalStatus() || ApprovalStatus.REJECTED == user.getApprovalStatus()) {
							LOG.info("All users of this level have not approved the request.");
							allUsersDone = false;
							break;
						}
					}
				}
				if (allUsersDone) {
					LOG.info("All users of this level have approved the Supplier Form.");
					setNextOrAllDoneForSupplierForm(actionBy, approvalList, currentLevel, supplierFormSubmition);
				}
			}
		}
		return supplierFormSubmissionDao.update(supplierFormSubmition);

	}

	private void setNextOrAllDoneForSupplierForm(User actionBy, List<SupplierFormSubmitionApproval> approvalList, SupplierFormSubmitionApproval currentLevel, SupplierFormSubmition supplierFormSubmition) {
		String buyerTimeZone = "GMT+8:00";
		currentLevel.setDone(true);
		currentLevel.setActive(false); // Check if all approvals are done
		if (currentLevel.getLevel() == approvalList.size()) {
			// all approvals done
			LOG.info("All approvals for this form is done!!!. Going to Approved Mode.");
			supplierFormSubmition.setApprovalStatus(SupplierFormApprovalStatus.APPROVED);
		} else {
			for (SupplierFormSubmitionApproval formApproval : approvalList) {
				if (formApproval.getLevel() == currentLevel.getLevel() + 1) {
					LOG.info("Setting Approval level " + formApproval.getLevel() + " as Active level");
					formApproval.setActive(true);
					for (SupplierFormSubmitionApprovalUser nextLevelUser : formApproval.getApprovalUsers()) {
						buyerTimeZone = getTimeZoneByBuyerSettings(nextLevelUser.getUser().getTenantId(), buyerTimeZone);
						// send email to Approver
						sendEmailToSupplierFormApprovers(supplierFormSubmition, nextLevelUser, buyerTimeZone);
					}
					break;
				}
			}
		}

	}

	@Override
	public void sendEmailToSupplierFormApprovers(SupplierFormSubmition supplierFormSubmition, SupplierFormSubmitionApprovalUser nextLevelUser, String buyerTimeZone) {
		LOG.info("Sending approval email request to approval : " + nextLevelUser.getUser().getCommunicationEmail());
		String mailTo = nextLevelUser.getUser().getCommunicationEmail();
		String subject = "Supplier Form Approval request";
		String url = APP_URL + "/buyer/supplierFormSubView/" + supplierFormSubmition.getId();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("userName", nextLevelUser.getUser().getName());
		map.put("supplierForm", supplierFormSubmition);
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		df.setTimeZone(TimeZone.getTimeZone(buyerTimeZone));
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
		sdf.setTimeZone(TimeZone.getTimeZone(buyerTimeZone));
		map.put("submitedDate", sdf.format(supplierFormSubmition.getSubmitedDate()));
		map.put("date", df.format(new Date()));
		map.put("loginUrl", APP_URL + "/login");
		map.put("appUrl", url);
		if (StringUtils.checkString(mailTo).length() > 0) {
			sendEmail(mailTo, subject, map, Global.SUPP_FORM_APPROVAL_REQUEST_TEMPLATE);
		} else {
			LOG.warn("No communication email configured for user : " + nextLevelUser.getUser().getLoginId() + "... Not going to send email notification");
		}

		String notificationMessage = messageSource.getMessage("supplierform.approval.request.notification.message", new Object[] { supplierFormSubmition.getName() }, Global.LOCALE);
		sendDashboardNotification(nextLevelUser.getUser(), url, subject, notificationMessage, NotificationType.APPROVAL_MESSAGE);

	}

	private void sendSupplierFormApprovalEmail(User mailTo, SupplierFormSubmition supplierFormSubmition, User actionBy, String remarks) {
		LOG.info("Sending approved supplier form email to (" + mailTo.getName() + ") : " + mailTo.getCommunicationEmail());
		String url = APP_URL + "/buyer/supplierFormSubView/" + supplierFormSubmition.getId();
		String subject = "Supplier Form Approved";
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("userName", mailTo.getName());
		map.put("actionByName", actionBy.getName());
		map.put("supplierForm", supplierFormSubmition);
		map.put("remarks", StringUtils.checkString(remarks));
		if (mailTo.getId().equals(actionBy.getId())) {
			map.put("message", "You have Approved the following Supplier Form:");
		} else {
			map.put("message", actionBy.getName() + " has Approved the following Supplier Form:");
		}
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		String timeZone = "GMT+8:00";
		timeZone = getTimeZoneByBuyerSettings(mailTo.getTenantId(), timeZone);
		df.setTimeZone(TimeZone.getTimeZone(timeZone));
		map.put("submitedDate", df.format(supplierFormSubmition.getSubmitedDate()));
		map.put("date", df.format(new Date()));
		map.put("loginUrl", APP_URL + "/login");
		map.put("appUrl", url);

		if (StringUtils.checkString(mailTo.getCommunicationEmail()).length() > 0) {
			sendEmail(mailTo.getCommunicationEmail(), subject, map, Global.SUPPLIER_FORM_APPROVED_TEMPLATE);
		} else {
			LOG.warn("No communication email configured for user : " + mailTo.getLoginId() + "... Not going to send email notification");
		}

		String notificationMessage = messageSource.getMessage("supplierform.approval.notification.message", new Object[] { actionBy.getName(), supplierFormSubmition.getName(), remarks }, Global.LOCALE);
		sendDashboardNotification(mailTo, url, subject, notificationMessage, NotificationType.APPROVAL_MESSAGE);

	}

	private void sendSupplierFormRejectionEmail(User mailTo, SupplierFormSubmition supplierFormSubmition, User actionBy, String remarks) {
		LOG.info("Sending rejected supplier form email to (" + mailTo.getName() + ") : " + mailTo.getCommunicationEmail());
		String url = APP_URL + "/buyer/supplierFormSubView/" + supplierFormSubmition.getId();
		String subject = "Supplier Form Rejected";
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("userName", mailTo.getName());
		map.put("actionByName", actionBy.getName());
		map.put("supplierForm", supplierFormSubmition);
		map.put("remarks", StringUtils.checkString(remarks));
		if (mailTo.getId().equals(actionBy.getId())) {
			map.put("message", "You have Rejected the following Supplier Form:");
		} else {
			map.put("message", actionBy.getName() + " has Rejected the following Supplier Form:");
		}
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		String timeZone = "GMT+8:00";
		timeZone = getTimeZoneByBuyerSettings(mailTo.getTenantId(), timeZone);
		df.setTimeZone(TimeZone.getTimeZone(timeZone));
		map.put("submitedDate", df.format(supplierFormSubmition.getSubmitedDate()));
		map.put("date", df.format(new Date()));
		map.put("loginUrl", APP_URL + "/login");
		map.put("appUrl", url);

		if (StringUtils.checkString(mailTo.getCommunicationEmail()).length() > 0) {
			sendEmail(mailTo.getCommunicationEmail(), subject, map, Global.SUPPLIER_FORM_APP_REJECT_TEMPLATE);
		} else {
			LOG.warn("No communication email configured for user : " + mailTo.getLoginId() + "... Not going to send email notification");
		}

		String notificationMessage = messageSource.getMessage("supplierform.rejection.notification.message", new Object[] { actionBy.getName(), supplierFormSubmition.getName(), remarks }, Global.LOCALE);
		sendDashboardNotification(mailTo, url, subject, notificationMessage, NotificationType.REJECT_MESSAGE);

	}

}
