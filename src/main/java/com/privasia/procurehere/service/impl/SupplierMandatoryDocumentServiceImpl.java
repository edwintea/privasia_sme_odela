package com.privasia.procurehere.service.impl;

import com.privasia.procurehere.core.dao.SupplierMandatoryDocumentDao;
import com.privasia.procurehere.core.entity.SupplierMandatoryDocument;
import com.privasia.procurehere.service.SupplierMandatoryDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class SupplierMandatoryDocumentServiceImpl implements SupplierMandatoryDocumentService {

	@Autowired
	SupplierMandatoryDocumentDao supplierMandatoryDocumentDao;

	@Override
	public List<SupplierMandatoryDocument> getAll(){
		return supplierMandatoryDocumentDao.findAll();
	}

	@Override
	public SupplierMandatoryDocument findByIds(String docId){
		return 	supplierMandatoryDocumentDao.findByIds(docId);
	}

	@Override
	public  boolean isExistsByActiveTitle(SupplierMandatoryDocument document){
		return 	supplierMandatoryDocumentDao.isExistsByActiveTitle(document);
	}


	@Override
	@Transactional(readOnly = false)
	public void deleteDocument(String docId) {
		supplierMandatoryDocumentDao.deleteById(docId);
	}
}
