package com.privasia.procurehere.service.impl;

import java.util.List;

import javax.servlet.ServletContext;
import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.privasia.procurehere.core.dao.BuyerAuditTrailDao;
import com.privasia.procurehere.core.dao.SupplierTagsDao;
import com.privasia.procurehere.core.entity.SupplierTags;
import com.privasia.procurehere.core.pojo.TableDataInput;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.service.SupplierTagsService;

@Service
@Transactional(readOnly = true)
public class SupplierTagsServiceImpl implements SupplierTagsService {

	static final Logger LOG = Logger.getLogger(Global.ADMIN_LOG);

	protected Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

	@Autowired
	SupplierTagsDao supplierTagsDao;

	@Autowired
	BuyerAuditTrailDao buyerAuditTrailDao;

	@Autowired
	ServletContext context;

	@Override
	@Transactional(readOnly = false)
	public void saveSupplierTags(SupplierTags supplierTags) {
		supplierTagsDao.saveOrUpdate(supplierTags);
	}

	@Override
	@Transactional(readOnly = false)
	public void updateSupplierTags(SupplierTags supplierTags) {
		supplierTagsDao.update(supplierTags);
	}

	@Override
	public SupplierTags getSupplierTagsById(String id) {
		SupplierTags supplierTags = supplierTagsDao.findById(id);

		if (supplierTags != null && supplierTags.getBuyer() != null)
			supplierTags.getBuyer().getFullName();

		return supplierTags;
	}

	@Override
	public boolean isExists(SupplierTags supplierTags, String tenantId) {
		return supplierTagsDao.isExists(supplierTags, tenantId);
	}

	@Override
	public List<SupplierTags> findSupplierTagsForTenant(String tenantId, TableDataInput tableParams) {
		return supplierTagsDao.findSupplierTagsForTenant(tenantId, tableParams);
	}

	@Override
	public long findTotalFilteredSupplierTagsForTenant(String tenantId, TableDataInput tableParams) {
		return supplierTagsDao.findTotalFilteredSupplierTagsForTenant(tenantId, tableParams);
	}

	@Override
	public long findTotalSupplierTagsForTenant(String tenantId) {
		return supplierTagsDao.findTotalSupplierTagsForTenant(tenantId);
	}

	@Override
	@Transactional(readOnly = false)
	public void deleteSupplierTags(SupplierTags supplierTags) {
		supplierTagsDao.delete(supplierTags);
	}

	@Override
	public List<SupplierTags> searchAllActiveSupplierTagsForTenant(String loggedInUserTenantId) {
		return supplierTagsDao.searchAllActiveSupplierTagsForTenant(loggedInUserTenantId);
	}

	@Override
	public List<SupplierTags> getAllSupplierTagsOnlyByIds(List<String> supplierTags) {
		return supplierTagsDao.getAllSupplierTagsOnlyByIds(supplierTags);
	}

	@Override
	public SupplierTags getSuppliertagsAndTenantId(String suppliertags, String tenantId) {
		return supplierTagsDao.getSuppliertagsAndTenantId(suppliertags, tenantId);
	}


}
