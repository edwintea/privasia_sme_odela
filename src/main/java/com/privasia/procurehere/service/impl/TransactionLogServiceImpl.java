package com.privasia.procurehere.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.privasia.procurehere.core.entity.TransactionLog;
import com.privasia.procurehere.core.pojo.TableDataInput;
import com.privasia.procurehere.core.pojo.TransactionLogPojo;
import com.privasia.procurehere.core.supplier.dao.TransactionLogDao;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.service.BusinessUnitService;
import com.privasia.procurehere.service.CostCenterService;
import com.privasia.procurehere.service.CurrencyService;
import com.privasia.procurehere.service.TransactionLogService;

/**
 * @author shubham
 */
@Service
@Transactional(readOnly = true)
public class TransactionLogServiceImpl implements TransactionLogService {

	@SuppressWarnings("unused")
	private static Logger LOG = Logger.getLogger(Global.BUDGET_PLANNER);

	@Autowired
	BusinessUnitService businessUnitService;

	@Autowired
	CostCenterService costCenterService;

	@Autowired
	CurrencyService currencyService;

	@Autowired
	TransactionLogDao transactionLogDao;

	@Override
	@Transactional(readOnly = false)
	public TransactionLog saveTransactionLog(TransactionLog transactionLog) {
		return transactionLogDao.saveOrUpdate(transactionLog);
	}

	@Override
	public List<TransactionLogPojo> getAlltransactionLogsForTenantId(String loggedInUserTenantId, TableDataInput input) {
		return transactionLogDao.getAlltransactionLogsForTenantId(loggedInUserTenantId, input);
	}

	@Override
	public long findfilteredTotalCountTransactionLogForTenantId(String loggedInUserTenantId, TableDataInput input) {
		return transactionLogDao.findfilteredTotalCountTransactionLogForTenantId(loggedInUserTenantId, input);
	}

	@Override
	public long findTotalTransactionLogForTenantId(String loggedInUserTenantId, TableDataInput input) {
		return transactionLogDao.findTotalTransactionLogForTenantId(loggedInUserTenantId, input);
	}

	@Override
	public void buildHeader(XSSFWorkbook workbook, XSSFSheet sheet) {
		Row rowHeading = sheet.createRow(0);
		// Style of Heading Cells
		CellStyle styleHeading = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		styleHeading.setFont(font);
		styleHeading.setVerticalAlignment(CellStyle.ALIGN_CENTER);

		int i = 0;
		Cell cell = null;
		for (String column : Global.TRANSACTION_LOGS) {
			cell = rowHeading.createCell(i++);
			cell.setCellValue(column);
			cell.setCellStyle(styleHeading);
		}
	}

	@Override
	public List<TransactionLogPojo> getAlltransactionLogsForTenantId(String loggedInUserTenantId) {
		return transactionLogDao.getAlltransactionLogsForTenantId(loggedInUserTenantId);
	}
}
