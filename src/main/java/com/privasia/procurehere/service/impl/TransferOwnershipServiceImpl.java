package com.privasia.procurehere.service.impl;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.privasia.procurehere.core.dao.PrDao;
import com.privasia.procurehere.core.dao.RfaEventDao;
import com.privasia.procurehere.core.dao.RfiEventDao;
import com.privasia.procurehere.core.dao.RfpEventDao;
import com.privasia.procurehere.core.dao.RfqEventDao;
import com.privasia.procurehere.core.dao.RftEventDao;
import com.privasia.procurehere.core.entity.Pr;
import com.privasia.procurehere.core.entity.RfaEvent;
import com.privasia.procurehere.core.entity.RfaEventAudit;
import com.privasia.procurehere.core.entity.RfiEvent;
import com.privasia.procurehere.core.entity.RfiEventAudit;
import com.privasia.procurehere.core.entity.RfpEvent;
import com.privasia.procurehere.core.entity.RfpEventAudit;
import com.privasia.procurehere.core.entity.RfqEvent;
import com.privasia.procurehere.core.entity.RfqEventAudit;
import com.privasia.procurehere.core.entity.RftEvent;
import com.privasia.procurehere.core.entity.RftEventAudit;
import com.privasia.procurehere.core.entity.User;
import com.privasia.procurehere.core.enums.AuditActionType;
import com.privasia.procurehere.core.utils.CollectionUtil;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.core.utils.SecurityLibrary;
import com.privasia.procurehere.service.EventAuditService;
import com.privasia.procurehere.service.RfaEventService;
import com.privasia.procurehere.service.RfiEventService;
import com.privasia.procurehere.service.RfpEventService;
import com.privasia.procurehere.service.RfqEventService;
import com.privasia.procurehere.service.RftEventService;
import com.privasia.procurehere.service.TransferOwnershipService;
import com.privasia.procurehere.service.UserService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;

@Service
@Transactional(readOnly = true)
public class TransferOwnershipServiceImpl implements TransferOwnershipService {

	private static final Logger LOG = Logger.getLogger(Global.BUYER_LOG);

	@Autowired
	RfaEventDao rfaEventDao;

	@Autowired
	RfiEventDao rfiEventDao;

	@Autowired
	RfpEventDao rfpEventDao;

	@Autowired
	RfqEventDao rfqEventDao;

	@Autowired
	RftEventDao rftEventDao;

	@Autowired
	UserService userService;

	@Resource
	MessageSource messageSource;

	@Autowired
	EventAuditService eventAuditService;

	@Autowired
	RftEventService rftEventService;

	@Autowired
	RfpEventService rfpEventService;

	@Autowired
	RfqEventService rfqEventService;

	@Autowired
	RfiEventService rfiEventService;

	@Autowired
	RfaEventService rfaEventService;

	@Autowired
	PrDao prDao;

	@Override
	@Transactional(readOnly = false)
	public void saveTransferOwnership(String fromUser, String toUser, HttpSession session, JRSwapFileVirtualizer virtualizer) {
		User assignTo = userService.getUsersById(toUser);
		/*
		 * if(rftEvent == null){ rftEvent = new ArrayList<RftEvent>(); }
		 */
		List<RftEvent> rftEvent = rftEventDao.getAllRftEventByLoginId(fromUser);
		if (CollectionUtil.isNotEmpty(rftEvent)) {
			for (RftEvent event : rftEvent) {
				event.setCreatedBy(assignTo);
				event.setEventOwner(assignTo);
				rftEventDao.update(event);

				byte[] summarySnapshot = null;
				try {
					JasperPrint eventSummary = rftEventService.getEvaluationSummaryPdf(event, SecurityLibrary.getLoggedInUser(), (String) session.getAttribute(Global.SESSION_TIME_ZONE_KEY), virtualizer);
					summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
				} catch (JRException e) {
					LOG.error("Error while Store summary PDF as byte : " + e.getMessage(), e);
				} finally {
					if (virtualizer != null) {
						virtualizer.cleanup();
					}
				}

				RftEventAudit audit = new RftEventAudit(SecurityLibrary.getLoggedInUser().getBuyer(), event, SecurityLibrary.getLoggedInUser(), new java.util.Date(), AuditActionType.Resume, messageSource.getMessage("event.audit.transfer", new Object[] { event.getEventName() }, Global.LOCALE), summarySnapshot);
				eventAuditService.save(audit);

			}
		}

		List<RfqEvent> rfqEvent = rfqEventDao.getAllRfqEventByLoginId(fromUser);
		if (CollectionUtil.isNotEmpty(rfqEvent)) {
			for (RfqEvent event : rfqEvent) {
				event.setCreatedBy(assignTo);
				event.setEventOwner(assignTo);
				rfqEventDao.update(event);

				byte[] summarySnapshot = null;
				try {
					JasperPrint eventSummary = rfqEventService.getEvaluationSummaryPdf(event, SecurityLibrary.getLoggedInUser(), (String) session.getAttribute(Global.SESSION_TIME_ZONE_KEY), virtualizer);
					summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
					RfqEventAudit audit = new RfqEventAudit(SecurityLibrary.getLoggedInUser().getBuyer(), event, SecurityLibrary.getLoggedInUser(), new java.util.Date(), AuditActionType.Suspend, messageSource.getMessage("event.audit.transfer", new Object[] { event.getEventName() }, Global.LOCALE), summarySnapshot);
					eventAuditService.save(audit);
				} catch (JRException e) {
					LOG.error("Error while Store summary PDF as byte : " + e.getMessage(), e);
				} finally {
					if (virtualizer != null) {
						virtualizer.cleanup();
					}
				}

			}
		}

		List<RfpEvent> rfpEvent = rfpEventDao.getAllRfpEventByLoginId(fromUser);
		if (CollectionUtil.isNotEmpty(rfpEvent)) {
			for (RfpEvent event : rfpEvent) {
				event.setCreatedBy(assignTo);
				event.setEventOwner(assignTo);
				rfpEventDao.update(event);

				byte[] summarySnapshot = null;
				try {
					JasperPrint eventSummary = rfpEventService.getEvaluationSummaryPdf(event, SecurityLibrary.getLoggedInUser(), (String) session.getAttribute(Global.SESSION_TIME_ZONE_KEY), virtualizer);
					summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
				} catch (JRException e) {
					LOG.error("Error while Store summary PDF as byte : " + e.getMessage(), e);
				} finally {
					if (virtualizer != null) {
						virtualizer.cleanup();
					}
				}
				RfpEventAudit audit = new RfpEventAudit(SecurityLibrary.getLoggedInUser().getBuyer(), event, SecurityLibrary.getLoggedInUser(), new java.util.Date(), AuditActionType.Resume, messageSource.getMessage("event.audit.transfer", new Object[] { event.getEventName() }, Global.LOCALE), summarySnapshot);
				eventAuditService.save(audit);
			}
		}

		List<RfiEvent> rfiEvent = rfiEventDao.getAllRfiEventByLoginId(fromUser);
		if (CollectionUtil.isNotEmpty(rfiEvent)) {
			for (RfiEvent event : rfiEvent) {
				event.setCreatedBy(assignTo);
				event.setEventOwner(assignTo);
				rfiEventDao.update(event);

				byte[] summarySnapshot = null;
				try {
					JasperPrint eventSummary = rfiEventService.getEvaluationSummaryPdf(event, SecurityLibrary.getLoggedInUser(), (String) session.getAttribute(Global.SESSION_TIME_ZONE_KEY), virtualizer);
					summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
				} catch (JRException e) {
					LOG.error("Error while Store summary PDF as byte : " + e.getMessage(), e);
				} finally {
					if (virtualizer != null) {
						virtualizer.cleanup();
					}
				}

				RfiEventAudit audit = new RfiEventAudit(SecurityLibrary.getLoggedInUser().getBuyer(), event, SecurityLibrary.getLoggedInUser(), new java.util.Date(), AuditActionType.Resume, messageSource.getMessage("event.audit.resume", new Object[] { event.getEventName() }, Global.LOCALE), summarySnapshot);
				eventAuditService.save(audit);
			}
		}

		List<RfaEvent> rfaEvent = rfaEventDao.getAllRfaEventByLoginId(fromUser);
		if (CollectionUtil.isNotEmpty(rfaEvent)) {
			for (RfaEvent event : rfaEvent) {
				event.setCreatedBy(assignTo);
				event.setEventOwner(assignTo);
				rfaEventDao.update(event);

				byte[] summarySnapshot = null;
				try {

					JasperPrint eventSummary = rfaEventService.getEvaluationSummaryPdf(event, SecurityLibrary.getLoggedInUser(), (String) session.getAttribute(Global.SESSION_TIME_ZONE_KEY), virtualizer);
					summarySnapshot = JasperExportManager.exportReportToPdf(eventSummary);
				} catch (JRException e) {
					LOG.error("Error while Store summary PDF as byte : " + e.getMessage(), e);
				} finally {
					if (virtualizer != null) {
						virtualizer.cleanup();
					}
				}

				RfaEventAudit audit = new RfaEventAudit(SecurityLibrary.getLoggedInUser().getBuyer(), event, SecurityLibrary.getLoggedInUser(), new java.util.Date(), AuditActionType.Resume, messageSource.getMessage("event.audit.transfer", new Object[] { event.getEventName() }, Global.LOCALE), summarySnapshot);
				eventAuditService.save(audit);
			}
		}

		List<Pr> prs = prDao.getAllPrByLoginId(fromUser);
		if (CollectionUtil.isNotEmpty(prs)) {
			for (Pr prTrans : prs) {
				prTrans.setCreatedBy(assignTo);
				prDao.update(prTrans);
			}
		}

	}

}
