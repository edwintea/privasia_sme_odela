package com.privasia.procurehere.service.impl;

import java.io.IOException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.mail.internet.MimeMessage;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.jboss.logging.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.privasia.procurehere.core.entity.BuyerNotificationMessage;
import com.privasia.procurehere.core.entity.DeliveryOrder;
import com.privasia.procurehere.core.entity.Supplier;
import com.privasia.procurehere.core.entity.User;
import com.privasia.procurehere.core.enums.NotificationType;
import com.privasia.procurehere.core.pojo.PrErp2Pojo;
import com.privasia.procurehere.core.pojo.RfiEventPublishResponse;
import com.privasia.procurehere.core.pojo.SupplierPojo;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.core.utils.OdelaConstant;
import com.privasia.procurehere.core.utils.StringUtils;
import com.privasia.procurehere.integration.RequestResponseLoggingInterceptor;
import com.privasia.procurehere.integration.RestTemplateResponseErrorHandler;
import com.privasia.procurehere.service.NotificationService;
import com.privasia.procurehere.service.OdelaIntegrationService;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import freemarker.cache.FileTemplateLoader;
import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateNotFoundException;
import javax.net.ssl.SSLContext;

@Service
public class OdelaIntegrationServiceImpl implements OdelaIntegrationService{
	
	
	
	public static final Logger LOG = Logger.getLogger(OdelaIntegrationServiceImpl.class);

	
	public static final String MAX_TRANSFER_SIZE = "4294967295";
	
	@Value("${odela.endpoint}")
	String ODELA_ENDPOINT_URL;
	
	@Autowired
	NotificationService notificationService;
	
	@Autowired     
	private JavaMailSender javaMailSender;
	
	
	@Value("${app.url}")
	String APP_URL;
	

	@Autowired
	Configuration freemarkerConfiguration;
	
	
	@Override
	public boolean integrateSupplierIntoOdela(Supplier supplier) throws Exception {
		
		boolean isIntegrated = false ;
		try {
			
			StringBuilder toBeEncrypted = new StringBuilder();
			toBeEncrypted.append(supplier.getCompanyName());
			toBeEncrypted.append(supplier.getCompanyRegistrationNumber());
			toBeEncrypted.append(supplier.getFullName());
			toBeEncrypted.append(supplier.getCommunicationEmail());
			toBeEncrypted.append(supplier.getMobileNumber());
			toBeEncrypted.append("smeagent0001");
			
						
			String secrectKey ="testingkey";
			String signature = hmacSha256(secrectKey,toBeEncrypted.toString());
			
					
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.MULTIPART_FORM_DATA);
			headers.setAccept(Arrays.asList(MediaType.ALL));
			headers.add(HttpHeaders.ACCEPT_ENCODING,"gzip, deflate, br");
			headers.add("apikey","JRds0HEOV7FYnpJwPPCuRbEcfXha074j");
			headers.setContentLength(8);

			
			MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
			map.add("company_name",supplier.getCompanyName());
			map.add("company_reg_no",supplier.getCompanyRegistrationNumber());
			map.add("company_contact_no",supplier.getCompanyContactNumber());
			map.add("full_name",supplier.getFullName());
			map.add("email",supplier.getCommunicationEmail());
			map.add("mobile_no",supplier.getMobileNumber());
			map.add("merchant_code","smeagent0001");
			map.add("signature", signature);
			map.add("secretKey","testingkey");

			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
			TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

			SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy)
					.build();
	
			SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
	
			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();
	
			HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
			requestFactory.setConnectTimeout(60000);
			requestFactory.setConnectionRequestTimeout(60000);
			requestFactory.setHttpClient(httpClient);
			RestTemplate restTemplate = new RestTemplate(requestFactory);
			
			try {
				ResponseEntity<String> response = restTemplate.exchange(ODELA_ENDPOINT_URL, HttpMethod.POST, request,String.class);
				String resBody = response.getBody().toString();
				JSONObject jsonObject = new JSONObject(resBody);
				
				LOG.info("The response code :"+ jsonObject.getString(OdelaConstant.STATUS_CODE));
				LOG.info("The response message :"+ jsonObject.getString(OdelaConstant.STATUS_MESSAGE));

				if(response.getStatusCode().toString().equalsIgnoreCase("200") && jsonObject.getString(OdelaConstant.STATUS_CODE).equalsIgnoreCase(OdelaConstant.STATUS_CODE_S001)) {
					try {
						LOG.info("Sending email to supplier"); 
						isIntegrated = true;
						sendOdelaNotificatoSupplier(supplier);
					}catch(Exception e) {
						e.printStackTrace();
						LOG.info("Sending email failed"); 
					}		
					
				}else {
					
					LOG.info("Odela integration : Failed | Reason : " +OdelaConstant.RESPONSE_CODE_MSG.get(jsonObject.getString(OdelaConstant.STATUS_CODE)));
				}
			}catch(Exception e) {
				
				e.printStackTrace();
				LOG.info("Error while calling ODELA integration api ");
			}
			
		}catch(Exception e) {
			
			e.printStackTrace();
			LOG.info("Erro while integrating supplier data to ODELA ");
		}
		
	
	return isIntegrated;
	}
	
	public void sendOdelaNotificatoSupplier(Supplier supplier) throws Exception {
		
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
		String timeZone = "GMT+8:00";
		sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
		String subject = null;

		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("loginUrl", APP_URL + "/login");
		map.put("appLink", APP_URL + "/login");
		map.put("date", sdf.format(new Date()));
		map.put("fullName", "Adhole Ramit");
		map.put("requestDate", sdf.format(new Date()));
		map.put("associatedDate",sdf.format(new Date()));
		map.put("buyerRemarks", supplier.getRemarks() );
			
		String message = FreeMarkerTemplateUtils.processTemplateIntoString(freemarkerConfiguration.getTemplate(Global.ODELA_INTEGRATION_SUPPLIER_ACCEPT_TEMPLATE), map);
		notificationService.sendEmail(supplier.getCommunicationEmail(),OdelaConstant.ODELA_MAIL_ACCEPTED,message);
		
	}
	

	public static String hmacSha256(String secretKey, String toBeEncrypted) 
	{
	byte[] secretKeyByte =secretKey.getBytes();
	byte[] toBeEncryptedByte =toBeEncrypted.getBytes();
	byte[] hmacSha256 = null;
	try {
		Mac mac = Mac.getInstance("HmacSHA256");
		SecretKeySpec secretKeySpec = new SecretKeySpec(secretKeyByte,"HmacSHA256");
		mac.init(secretKeySpec);
		hmacSha256 = mac.doFinal(toBeEncryptedByte);
	} catch (Exception e) {
		throw new RuntimeException("Failed to calculate hmac-sha256", e);
	 }
		String signature = Base64.encodeBase64String(hmacSha256);
		return signature;
	}
	

	}
	
	



