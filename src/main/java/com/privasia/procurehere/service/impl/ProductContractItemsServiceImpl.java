package com.privasia.procurehere.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.privasia.procurehere.core.dao.ProductContractItemsDao;
import com.privasia.procurehere.core.entity.ProductCategory;
import com.privasia.procurehere.core.entity.ProductContractItems;
import com.privasia.procurehere.core.pojo.ProductContractItemsPojo;
import com.privasia.procurehere.core.pojo.TableDataInput;
import com.privasia.procurehere.service.ProductContractItemsService;

@Service
@Transactional(readOnly = true)
public class ProductContractItemsServiceImpl implements ProductContractItemsService {

	@Autowired
	ProductContractItemsDao productContractItemsDao;

	@Transactional(readOnly = false)
	@Override
	public void deleteBycontractItemNumber(String contractItemNumber) {
		productContractItemsDao.deleteBycontractItemNumber(contractItemNumber);
	}

	@Override
	@SuppressWarnings("unused")
	public ProductContractItems findProductContractItemByItemId(String itemId) {
		ProductContractItems item = productContractItemsDao.findProductContractItemByItemId(itemId, null);
		if (item.getProductContract() != null) {
			item.getProductContract().getSupplier().getSupplier().getCompanyName();
		}
		if (item.getProductCategory() != null)
			item.getProductCategory().getProductCode();
		if (item.getUom() != null)
			item.getUom().getUom();

		return item;

	}

	@Override
	public ProductContractItems findProductContractItemById(String itemId) {
		return productContractItemsDao.findProductContractItemById(itemId);
	}

	@Override
	public List<ProductCategory> findProductContractByTenantIDSupplierID(String loggedInUserTenantId, String supplierId) {
		List<ProductCategory> pcList = productContractItemsDao.findProductContractByTenantIDSupplierID(loggedInUserTenantId, supplierId);
		if (pcList != null) {
			for (ProductCategory bpc : pcList) {
				if (bpc != null) {
					bpc.setCreatedBy(null);
					bpc.setModifiedBy(null);
				}
			}
		}
		return pcList;
	}

	@Override
	public List<ProductContractItemsPojo> findProductContractItemListForTenant(String loggedInUserTenantId, TableDataInput input, String id) {
		return productContractItemsDao.findProductContractItemListForTenant(loggedInUserTenantId, input, id);
	}

	@Override
	public long findTotalFilteredProductItemListForTenant(String loggedInUserTenantId, TableDataInput input, String id) {
		return productContractItemsDao.findTotalFilteredProductItemListForTenant(loggedInUserTenantId, input, id);
	}

	@Override
	public long findTotalProductItemListForTenant(String loggedInUserTenantId, String id) {
		return productContractItemsDao.findTotalProductItemListForTenant(loggedInUserTenantId, id);
	}

}