package com.privasia.procurehere.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.privasia.procurehere.core.dao.ErpSetupDao;
import com.privasia.procurehere.core.dao.FavoriteSupplierDao;
import com.privasia.procurehere.core.dao.RfqEventAwardDao;
import com.privasia.procurehere.core.dao.RfqEventDao;
import com.privasia.procurehere.core.dao.RfqSupplierBqItemDao;
import com.privasia.procurehere.core.dao.SourcingFormRequestDao;
import com.privasia.procurehere.core.entity.ErpSetup;
import com.privasia.procurehere.core.entity.FavouriteSupplier;
import com.privasia.procurehere.core.entity.ProductCategory;
import com.privasia.procurehere.core.entity.RfqBqItem;
import com.privasia.procurehere.core.entity.RfqEvent;
import com.privasia.procurehere.core.entity.RfqEventAward;
import com.privasia.procurehere.core.entity.RfqEventAwardAudit;
import com.privasia.procurehere.core.entity.RfqEventAwardDetails;
import com.privasia.procurehere.core.entity.RfqSupplierBqItem;
import com.privasia.procurehere.core.entity.SourcingFormRequest;
import com.privasia.procurehere.core.entity.State;
import com.privasia.procurehere.core.entity.Supplier;
import com.privasia.procurehere.core.entity.User;
import com.privasia.procurehere.core.enums.ErpIntegrationTypeForAward;
import com.privasia.procurehere.core.enums.EventStatus;
import com.privasia.procurehere.core.enums.RfxTypes;
import com.privasia.procurehere.core.enums.SapDocType;
import com.privasia.procurehere.core.exceptions.ApplicationException;
import com.privasia.procurehere.core.pojo.AwardDetailsErp2Pojo;
import com.privasia.procurehere.core.pojo.AwardDetailsErpPojo;
import com.privasia.procurehere.core.pojo.AwardErp2Pojo;
import com.privasia.procurehere.core.pojo.AwardErpPojo;
import com.privasia.procurehere.core.pojo.AwardReferenceNumberPojo;
import com.privasia.procurehere.core.pojo.AwardResponsePojo;
import com.privasia.procurehere.core.pojo.DeliveryAddressPojo;
import com.privasia.procurehere.core.pojo.ErpSupplierPojo;
import com.privasia.procurehere.core.pojo.RftEventAwardDetailsPojo;
import com.privasia.procurehere.core.utils.CollectionUtil;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.core.utils.SecurityLibrary;
import com.privasia.procurehere.core.utils.StringUtils;
import com.privasia.procurehere.integration.RequestResponseLoggingInterceptor;
import com.privasia.procurehere.integration.RestTemplateResponseErrorHandler;
import com.privasia.procurehere.service.ErpIntegrationService;
import com.privasia.procurehere.service.ErpSetupService;
import com.privasia.procurehere.service.EventAuditService;
import com.privasia.procurehere.service.EventAwardAuditService;
import com.privasia.procurehere.service.FavoriteSupplierService;
import com.privasia.procurehere.service.RfqAwardService;
import com.privasia.procurehere.service.StateService;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
@Transactional(readOnly = true)
public class RfqAwardServiceImpl implements RfqAwardService {

	private static final Logger LOG = Logger.getLogger(RfqAwardServiceImpl.class);
	private static final Logger ERP_LOG = Logger.getLogger(Global.ERP_LOG);

	@Autowired
	RfqEventAwardDao rfqEventAwardDao;

	@Autowired
	RfqSupplierBqItemDao rfqSupplierBqItemDao;

	@Autowired
	RfqEventDao rfqEventDao;

	@Autowired
	ErpIntegrationService erpIntegrationService;

	@Autowired
	StateService stateService;

	@Autowired
	FavoriteSupplierService favoriteSupplierService;

	@Autowired
	ApplicationContext applicationContext;

	@Autowired
	EventAwardAuditService eventAwardAuditService;

	@Autowired
	ServletContext context;

	@Autowired
	FavoriteSupplierDao favoriteSupplierDao;

	@Autowired
	ErpSetupDao erpSetupDao;

	@Autowired
	SourcingFormRequestDao sourcingFormRequestDao;

	@Autowired
	ErpSetupService erpSetupService;

	@Autowired
	EventAuditService eventAuditService;

	@Override
	@Transactional(readOnly = false)
	public RfqEventAward saveEventAward(RfqEventAward rfqEventAward, HttpSession session, User loggedInUser, Boolean transfer, Boolean conclude) {
		rfqEventAward.setCreatedBy(SecurityLibrary.getLoggedInUser());
		rfqEventAward.setCreatedDate(new Date());
		rfqEventAward.setModifiedBy(SecurityLibrary.getLoggedInUser());
		rfqEventAward.setModifiedDate(new Date());

		List<RfqEventAwardDetails> awardList = rfqEventAward.getRfxAwardDetails();
		Set<String> awardedSupplierIds = new HashSet<String>();
		if (CollectionUtil.isNotEmpty(awardList)) {
			for (RfqEventAwardDetails rfqEventAwardDetails : awardList) {
				Supplier supplier = rfqEventAwardDetails.getSupplier();
				if ((supplier != null) && (supplier.getId() != null)) {
					awardedSupplierIds.add(supplier.getId());
				}
			}
		}

		RfqEvent rfqEvent = rfqEventDao.findById(rfqEventAward.getRfxEvent().getId());
		if (CollectionUtil.isNotEmpty(awardedSupplierIds)) {
			if (rfqEvent != null) {
				List<Supplier> awardedSuppliers = null;
				if (CollectionUtil.isNotEmpty(rfqEvent.getAwardedSuppliers())) {
					awardedSuppliers = rfqEvent.getAwardedSuppliers();
				} else {
					awardedSuppliers = new ArrayList<Supplier>();
				}
				for (String suppId : awardedSupplierIds) {
					Supplier supplier = new Supplier();
					supplier.setId(suppId);
					awardedSuppliers.add(supplier);
				}
				rfqEvent.setAwardedSuppliers(awardedSuppliers);
				rfqEventDao.saveOrUpdate(rfqEvent);
			}
		}

		Set<String> supplierList = new HashSet<String>();

		if (CollectionUtil.isNotEmpty(rfqEventAward.getRfxAwardDetails())) {
			for (RfqEventAwardDetails awardDetails : rfqEventAward.getRfxAwardDetails()) {
				awardDetails.setEventAward(rfqEventAward);
				if (awardDetails.getSupplier() != null) {
					supplierList.add(awardDetails.getSupplier().getId());
				}
			}
		}
		RfqEventAward dbRfqEventAward = rfqEventAwardDao.saveOrUpdate(rfqEventAward);

		if (dbRfqEventAward.getRfxEvent() != null) {
			Double sumOfAwardedPriceForEvent = rfqEventAwardDao.getSumOfAwardedPrice(dbRfqEventAward.getRfxEvent().getId());
			BigDecimal sumAwardPrice = null;
			if (sumOfAwardedPriceForEvent != null) {
				sumAwardPrice = new BigDecimal(sumOfAwardedPriceForEvent);
			}
			try {
				rfqEventDao.updateRfaForAwardPrice(dbRfqEventAward.getRfxEvent().getId(), sumAwardPrice);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
		}
		// Save the list of awarded suppliers into separate table
		List<Supplier> awardedSuppliers = new ArrayList<Supplier>();
		for (String supplierId : supplierList) {
			Supplier supplier = new Supplier();
			supplier.setId(supplierId);
			awardedSuppliers.add(supplier);
		}
		RfqEvent event = rfqEventDao.findById(dbRfqEventAward.getRfxEvent().getId());
		event.setAwardedSuppliers(awardedSuppliers);
		event = rfqEventDao.update(event);
		// Jasper report method calling here-------------------------------------------------------------------------
		if (transfer == false) {
			if (StringUtils.checkString(dbRfqEventAward.getId()).length() > 0) {
				// dbRfqEventAward = rfqEventAwardDao.findById(eventAward.getId());
				LOG.info("********" + dbRfqEventAward.toString());
				try {
					AwardResponsePojo awardResponsePojo = new AwardResponsePojo();
					JasperPrint eventAward1 = getAwardSnapShotPdf(dbRfqEventAward, session, loggedInUser, awardResponsePojo, transfer, conclude);
					ByteArrayOutputStream workbook = getAwardSnapShotXcel(dbRfqEventAward, session, loggedInUser, awardResponsePojo, transfer, conclude);
					if (workbook != null) {
						byte[] snapshot = JasperExportManager.exportReportToPdf(eventAward1);
						byte[] excelSnapshot = workbook.toByteArray();

						LOG.info("SUSPEND FILE Size : " + snapshot.length);
						RfqEventAwardAudit audit = null;
						if (Boolean.TRUE == conclude) {
							audit = new RfqEventAwardAudit(event, loggedInUser.getBuyer(), loggedInUser, new Date(), "Award Saved and Concluded", snapshot, excelSnapshot);
						} else {
							audit = new RfqEventAwardAudit(event, loggedInUser.getBuyer(), loggedInUser, new Date(), "Award saved", snapshot, excelSnapshot);
						}
						if (rfqEventAward.getAttachment() != null) {
							LOG.info("File Name:-------->" + rfqEventAward.getAttachment().getOriginalFilename());
							try {
								audit.setFileData(rfqEventAward.getAttachment().getBytes());
							} catch (IOException e) {
								LOG.error("Eroor While getting file data" + e.getMessage(), e);
							}
							audit.setFileName(rfqEventAward.getAttachment().getOriginalFilename());
							audit.setCredContentType(rfqEventAward.getAttachment().getContentType());
						}
						eventAwardAuditService.saveRfqAwardAudit(audit);
					}
				} catch (Exception e) {
					LOG.error("Error while :" + e.getMessage(), e);
				}
			}
		}

		// Jasper end-------------------------------------------------------------------------

		if (dbRfqEventAward != null) {

			if (dbRfqEventAward.getRfxEvent() != null) {
				dbRfqEventAward.getRfxEvent().getId();
				dbRfqEventAward.getRfxEvent().getEventName();
			}
		}
		if (CollectionUtil.isNotEmpty(dbRfqEventAward.getRfxAwardDetails())) {
			for (RfqEventAwardDetails details : dbRfqEventAward.getRfxAwardDetails()) {
				if (details.getSupplier() != null) {
					LOG.info(details.getSupplier().getId());

					details.getSupplier().getId();
				}
				if (details.getBqItem() != null) {
					details.getBqItem().getId();
				}
				LOG.info(details.getAwardedPrice());
				LOG.info(details.getOriginalPrice());
				details.getAwardedPrice();
				details.getOriginalPrice();
				details.getTaxType();
				details.getTax();
			}

		}

		ErpSetup erpSetup = erpSetupService.findErpByWithTepmlateTenantId(loggedInUser.getTenantId());
		if (erpSetup == null || (erpSetup != null && Boolean.FALSE == erpSetup.getIsErpEnable())) {
			if (Boolean.TRUE == conclude) {
				rfqEvent.setConcludeRemarks(dbRfqEventAward.getAwardRemarks());
				rfqEvent.setConcludeBy(loggedInUser);
				rfqEvent.setConcludeDate(new Date());
				rfqEvent.setStatus(EventStatus.FINISHED);
				rfqEvent.setAwarded(Boolean.TRUE);
			}
			rfqEventDao.update(rfqEvent);
		} else if (erpSetup != null && Boolean.TRUE == erpSetup.getIsErpEnable()) {
			// If Event Award is supposed to be pushed to ERP, check if its interface type is TYPE_2 (which means FGV -
			// i.e. with budget checking)
			// If this business unit is not enabled for budget checking then complete the process.
			if (Boolean.TRUE == erpSetup.getEnableAwardErpPush() && erpSetup.getErpIntegrationTypeForAward() == ErpIntegrationTypeForAward.TYPE_2 && Boolean.FALSE == rfqEvent.getBusinessUnit().getBudgetCheck()) {
				rfqEvent.setStatus(EventStatus.FINISHED);
				rfqEvent.setAwarded(Boolean.TRUE);
				rfqEventDao.update(rfqEvent);
			}
		}

		/*
		 * try { RfqEventAudit audit = new RfqEventAudit(); audit.setActionDate(new Date());
		 * audit.setActionBy(SecurityLibrary.getLoggedInUser()); audit.setDescription("Event ' " +
		 * dbRfqEventAward.getRfxEvent().getEventName() + "' is awarded");
		 * audit.setEvent(dbRfqEventAward.getRfxEvent()); audit.setAction(AuditActionType.Award);
		 * eventAuditService.save(audit); } catch (Exception e) { LOG.error(e.getMessage(), e); }
		 */
		return dbRfqEventAward;
	}

	@Override
	public RfqEventAward rfqEventAwardByEventIdandBqId(String eventId, String bqId) {
		RfqEventAward eventAward = rfqEventAwardDao.rfqEventAwardByEventIdandBqId(eventId, bqId);
		if (eventAward != null) {
			if (eventAward.getRfxEvent() != null) {
				eventAward.getRfxEvent().getId();
				eventAward.getRfxEvent().getEventName();
			}
			if (CollectionUtil.isNotEmpty(eventAward.getRfxAwardDetails())) {
				for (RfqEventAwardDetails details : eventAward.getRfxAwardDetails()) {
					if (details.getSupplier() != null) {
						LOG.info(details.getSupplier().getId());

						details.getSupplier().getId();
					}
					if (details.getBqItem() != null) {
						details.getBqItem().getId();
					}
					LOG.info(details.getAwardedPrice());
					LOG.info(details.getOriginalPrice());
					details.getAwardedPrice();
					details.getOriginalPrice();
					details.getTaxType();
					details.getTax();
				}

			}
		}
		return eventAward;
	}

	@Override
	public RfqSupplierBqItem getBqItemByBqItemId(String bqItemId, String supplierId, String tenantId) {
		RfqSupplierBqItem rfqSupplierBqItem = rfqSupplierBqItemDao.getBqItemByRfqBqItemId(bqItemId, supplierId);
		if (rfqSupplierBqItem != null) {
			rfqSupplierBqItem.setProductCategoryList(favoriteSupplierDao.getSupplierProductCategoryBySupIdORTenantId(tenantId, supplierId));
			if (CollectionUtil.isNotEmpty(rfqSupplierBqItem.getProductCategoryList())) {
				for (ProductCategory productCategory : rfqSupplierBqItem.getProductCategoryList()) {
					productCategory.setBuyer(null);
					productCategory.setCreatedBy(null);
					productCategory.setModifiedBy(null);
				}
			}
			if (rfqSupplierBqItem.getSupplier() != null) {
				rfqSupplierBqItem.getSupplier().getFullName();
				// supp.getFullName();
			}
			if (CollectionUtil.isNotEmpty(rfqSupplierBqItem.getChildren())) {
				rfqSupplierBqItem.setChildren(null);
			}
			if (rfqSupplierBqItem.getParent() != null) {
				rfqSupplierBqItem.setParent(null);
				// if()
			}
			if (rfqSupplierBqItem.getBqItem() != null) {
				RfqBqItem bqitem = rfqSupplierBqItem.getBqItem();
				bqitem.getItemName();
			}
			if (rfqSupplierBqItem.getUom() != null) {
				rfqSupplierBqItem.setUom(null);
			}

		}
		return rfqSupplierBqItem;
	}

	@Override
	public List<RfqEventAward> getRfqEventAwardsByEventId(String eventId) {
		List<RfqEventAward> awardList = rfqEventAwardDao.getRfqEventAwardsByEventId(eventId);
		if (CollectionUtil.isNotEmpty(awardList)) {
			for (RfqEventAward rfqEventAward : awardList) {
				if (CollectionUtil.isNotEmpty(rfqEventAward.getRfxAwardDetails())) {
					for (RfqEventAwardDetails detail : rfqEventAward.getRfxAwardDetails()) {
						if (detail.getBqItem() != null) {
							detail.getBqItem().getItemName();
						}
					}
				}
			}
		}
		return awardList;
	}

	@Override
	@Transactional(readOnly = false)
	public void transferRfqAward(String eventId, String tenantId, HttpSession session, User loggedInUser, String rfqAwardEvevtId, Boolean transfer, RfxTypes eventType) throws Exception {

		ErpSetup erpSetup = erpSetupDao.getErpConfigBytenantId(tenantId);

		if (Boolean.FALSE == erpSetup.getIsErpEnable()) {
			LOG.warn(">>>>>>>>> ERP IS NOT ENABLED. Skipping transfer...  Event Id: " + eventId);
			return;
		}

		RfqEvent rfqEvent = rfqEventDao.getPlainEventById(eventId);
		// If award type integration is FGV
		if (Boolean.TRUE == erpSetup.getEnableAwardErpPush() && erpSetup.getErpIntegrationTypeForAward() == ErpIntegrationTypeForAward.TYPE_2) {

			if (Boolean.FALSE == rfqEvent.getBusinessUnit().getBudgetCheck()) {
				LOG.warn("Business unit not enabled for budget checking ... Event Id : " + eventId + ", Business Unit : " + rfqEvent.getBusinessUnit().getUnitName());
				throw new ApplicationException("Cannot send this event award details to ERP as Event Business unit not enabled for budget checking.");
			}

			RfqEventAward dbRfqEventAward = rfqEventAwardDao.findById(rfqAwardEvevtId);

			if (dbRfqEventAward == null) {
				LOG.error("Could not identify the award details to push to ERP. Please contact the application administrator. Award ID : " + rfqAwardEvevtId + ", Event Id : " + eventId);
				throw new ApplicationException("Could not identify the award details to push to ERP. Please contact the application administrator.");
			}

			AwardErp2Pojo awardPojo = new AwardErp2Pojo();

			if (StringUtils.checkString(rfqEvent.getPreviousRequestId()).length() == 0) {
				throw new ApplicationException("Cannot send this event award details to ERP as it does not seem to have created from a Sourcing Request.");
			}
			SourcingFormRequest rfs = sourcingFormRequestDao.findById(rfqEvent.getPreviousRequestId());

			if (rfs == null) {
				LOG.warn("RFS not found during award... Event Id : " + eventId + ", RFS ID : " + rfqEvent.getPreviousRequestId());
				throw new ApplicationException("Cannot send this event award details to ERP as it does not seem to have created from a Sourcing Request.");
			} else if (Boolean.FALSE == rfs.getBusinessUnit().getBudgetCheck()) {
				LOG.warn("RFS Business unit not enabled for budget checking ... Event Id : " + eventId + ", RFS ID : " + rfqEvent.getPreviousRequestId());
				throw new ApplicationException("Cannot send this event award details to ERP as RFS Business unit not enabled for budget checking.");
			}

			// pr.setErpDocNo(prResponse.getSapRefNo());
			// pr.setErpMessage(prResponse.getMessage());
			// pr.setErpPrTransferred(Boolean.TRUE);
			// pr.setErpStatus(prResponse.getStatus());

			awardPojo.setSapRefNo(rfs.getErpDocNo());
			awardPojo.setSapDocType(SapDocType.RFQ);
			awardPojo.setEventId(rfqEvent.getEventId());
			List<AwardDetailsErp2Pojo> items = new ArrayList<AwardDetailsErp2Pojo>();

			// Order the award details by BQ Level and Order sequence
			SortedMap<Integer, RfqEventAwardDetails> sm = new TreeMap<Integer, RfqEventAwardDetails>();
			for (RfqEventAwardDetails eventAward : dbRfqEventAward.getRfxAwardDetails()) {
				if (eventAward.getBqItem().getOrder() > 0) {
					String str = String.valueOf(eventAward.getBqItem().getLevel() * 10000) + String.valueOf(eventAward.getBqItem().getOrder());
					sm.put(Integer.parseInt(str), eventAward);
				}
			}

			int sequence = 1;
			for (Map.Entry<Integer, RfqEventAwardDetails> m : sm.entrySet()) {
				AwardDetailsErp2Pojo item = new AwardDetailsErp2Pojo();
				item.setItemNo(StringUtils.lpad(String.valueOf(sequence * 10), 5, '0'));
				item.setItemCode(m.getValue().getBqItem().getField1());
				item.setQuantity(m.getValue().getBqItem().getQuantity());
				// item.setUnitPrice(m.getValue().getAwardedPrice());
				// FIX for PH-1168 - Unitprice should be computed from total award price/quantity rounding to 2 decimal
				item.setUnitPrice(m.getValue().getAwardedPrice().divide(m.getValue().getBqItem().getQuantity(), 2, RoundingMode.DOWN));
				// get Fav Supplier Vendor Code based on supplier id for the tenant.
				String vendorCode = favoriteSupplierDao.getFavouriteSupplierVendorCodeBySupplierIdAndTenant(m.getValue().getSupplier().getId(), tenantId);
				item.setVendorCode(vendorCode);
				items.add(item);
				sequence++;
			}
			awardPojo.setBqItems(items);

			ClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory());
			RestTemplate restTemplate = new RestTemplate(factory);
			restTemplate.setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
			restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());

			String responseMsg = "";
			HttpHeaders headers = new HttpHeaders();
			ObjectMapper mapperObj = new ObjectMapper();

			if (StringUtils.checkString(erpSetup.getErpUsername()).length() > 0 && StringUtils.checkString(erpSetup.getErpPassword()).length() > 0) {
				String auth = StringUtils.checkString(erpSetup.getErpUsername()) + ":" + StringUtils.checkString(erpSetup.getErpPassword());
				byte[] encodedAuth = Base64.encode(auth.getBytes(Charset.forName("US-ASCII")));
				String authHeader = "Basic " + new String(encodedAuth);
				headers.set("Authorization", authHeader);
			}

			String payload = mapperObj.writeValueAsString(awardPojo);
			ERP_LOG.info("jsonObject  :" + payload);

			HttpEntity<AwardErp2Pojo> request = new HttpEntity<AwardErp2Pojo>(awardPojo, headers);
			try {
				responseMsg = restTemplate.postForObject(erpSetup.getErpUrl() + "/PRUpdate/", request, String.class);

				ERP_LOG.info("ERP Response : " + responseMsg);

				try {
					JasperPrint eventAward1 = getAwardSnapShotPdf(dbRfqEventAward, session, loggedInUser, null, transfer, false);
					ByteArrayOutputStream workbook = getAwardSnapShotXcel(dbRfqEventAward, session, loggedInUser, null, transfer, false);
					if (workbook != null) {
						byte[] snapshot = JasperExportManager.exportReportToPdf(eventAward1);
						byte[] excelSnapshot = workbook.toByteArray();

						LOG.info("FILE Size : " + snapshot.length);
						RfqEventAwardAudit audit = new RfqEventAwardAudit(dbRfqEventAward.getRfxEvent(), loggedInUser.getBuyer(), loggedInUser, new Date(), "Award Save and transfer", snapshot, excelSnapshot);
						eventAwardAuditService.saveRfqAwardAudit(audit);
					}
				} catch (Exception e) {
					LOG.error("Error while saving Award Audit :" + e.getMessage(), e);
				}

			} catch (HttpClientErrorException | HttpServerErrorException ex) {
				responseMsg = ex.getMessage();
				ERP_LOG.error("Error received from ERP : " + ex.getMessage() + " Status Text : " + ex.getStatusText());
				ERP_LOG.error("Response Body : " + ex.getResponseBodyAsString());
				throw new ApplicationException("Error received from ERP : " + ex.getMessage() + " Status Text : " + ex.getStatusText(), ex);
			}

		} else { // Assumed to be null or type_1
			List<RfqEventAward> awardList = getRfqEventAwardsByEventId(eventId);

			if (CollectionUtil.isEmpty(awardList)) {
				LOG.warn("Award List is Empty for eventId :" + eventId);
				return;
			}
			List<AwardErpPojo> awardPojoList = new ArrayList<>();
			for (RfqEventAward eventAward : awardList) {
				AwardErpPojo award = new AwardErpPojo();
				award.setId(eventAward.getId());
				award.setEventId(eventAward.getRfxEvent().getEventId());
				award.setEventName(eventAward.getRfxEvent().getEventName());
				award.setCurrencyCode(eventAward.getRfxEvent().getBaseCurrency() != null ? eventAward.getRfxEvent().getBaseCurrency().getCurrencyCode() : "");
				award.setAwardRemark(eventAward.getAwardRemarks());
				award.setEventOwner(eventAward.getRfxEvent().getCreatedBy() != null ? eventAward.getCreatedBy().getName() : "");
				award.setBusinessUnitName(eventAward.getRfxEvent().getBusinessUnit() != null ? eventAward.getRfxEvent().getBusinessUnit().getDisplayName() : "");
				award.setReferenceNumber(StringUtils.checkString(eventAward.getRfxEvent().getReferanceNumber()));
				award.setCreatedDate(eventAward.getRfxEvent().getCreatedDate());
				award.setStartDate(eventAward.getRfxEvent().getEventStart());
				award.setEndDate(eventAward.getRfxEvent().getEventEnd());
				award.setValidityDays(eventAward.getRfxEvent().getSubmissionValidityDays());
				award.setDeliveryDate(eventAward.getRfxEvent().getDeliveryDate());
				award.setPaymentTerm(eventAward.getRfxEvent().getPaymentTerm());
				award.setBqName(eventAward.getBq() != null ? eventAward.getBq().getName() : "");
				if (eventAward.getRfxEvent().getDeliveryAddress() != null) {
					DeliveryAddressPojo deliveryAddress = new DeliveryAddressPojo();
					deliveryAddress.setTitle(eventAward.getRfxEvent().getDeliveryAddress().getTitle());
					deliveryAddress.setLine1(eventAward.getRfxEvent().getDeliveryAddress().getLine1());
					deliveryAddress.setLine2(eventAward.getRfxEvent().getDeliveryAddress().getLine2());
					deliveryAddress.setCity(eventAward.getRfxEvent().getDeliveryAddress().getCity());
					deliveryAddress.setZip(eventAward.getRfxEvent().getDeliveryAddress().getZip());

					if (eventAward.getRfxEvent().getDeliveryAddress().getState() != null) {
						deliveryAddress.setState(eventAward.getRfxEvent().getDeliveryAddress().getState().getStateCode());
						State state = stateService.getState(eventAward.getRfxEvent().getDeliveryAddress().getState().getId());
						eventAward.getRfxEvent().getDeliveryAddress().setCountry(state.getCountry());
					}
					if (eventAward.getRfxEvent().getDeliveryAddress().getCountry() != null)
						deliveryAddress.setCountry(eventAward.getRfxEvent().getDeliveryAddress().getCountry().getCountryCode());

					award.setDeliveryAddress(deliveryAddress);
				}
				List<AwardDetailsErpPojo> awardDetailsList = new ArrayList<>();
				for (RfqEventAwardDetails eventAwardDetails : eventAward.getRfxAwardDetails()) {
					if (eventAwardDetails.getBqItem().getOrder() == 0) {
						continue;
					}
					AwardDetailsErpPojo awardDetails = new AwardDetailsErpPojo();
					// setting fav supplier vendorCode
					if (eventAwardDetails.getSupplier() != null) {
						FavouriteSupplier supplier = favoriteSupplierService.findFavSupplierBySuppId(eventAwardDetails.getSupplier().getId(), tenantId);
						if (StringUtils.checkString(supplier.getVendorCode()).length() > 0) {
							ErpSupplierPojo supplierPojo = new ErpSupplierPojo();
							supplierPojo.setCity(supplier.getSupplier().getCity());
							supplierPojo.setCommunicationEmail(supplier.getSupplier().getCommunicationEmail());
							supplierPojo.setCompanyName(supplier.getSupplier().getCompanyName());
							supplierPojo.setMobileNumber(supplier.getSupplier().getMobileNumber());
							supplierPojo.setCompanyContactNumber(supplier.getSupplier().getCompanyContactNumber());
							supplierPojo.setCountryCode(supplier.getSupplier().getRegistrationOfCountry().getCountryCode());
							supplierPojo.setCompanyRegistrationNumber(supplier.getSupplier().getCompanyRegistrationNumber());
							supplierPojo.setFullName(supplier.getSupplier().getFullName());
							supplierPojo.setDesignation(supplier.getSupplier().getDesignation());
							supplierPojo.setYearOfEstablished(supplier.getSupplier().getYearOfEstablished());
							supplierPojo.setFaxNumber(supplier.getSupplier().getFaxNumber());
							supplierPojo.setLine1(supplier.getSupplier().getLine1());
							supplierPojo.setLine2(supplier.getSupplier().getLine2());
							supplierPojo.setStateCode(supplier.getSupplier().getState().getStateCode());
							supplierPojo.setTaxNumber(supplier.getTaxNumber());
							awardDetails.setVendorCode(supplier.getVendorCode());
							awardDetails.setSupplier(supplierPojo);
							awardDetails.setVendorCode(supplier.getVendorCode());
						} else {
							throw new ApplicationException("Vendor Code not Assigned to " + supplier.getSupplier().getCompanyName());
						}
					}
					awardDetails.setTotalAmount(eventAwardDetails.getTotalPrice());
					awardDetails.setLnno(eventAwardDetails.getBqItem().getParent().getField8());
					awardDetails.setId(eventAwardDetails.getId());
					awardDetails.setLevel(eventAwardDetails.getBqItem().getLevel());
					awardDetails.setOrder(eventAwardDetails.getBqItem().getOrder());
					awardDetails.setItemName(eventAwardDetails.getBqItem().getItemName());
					awardDetails.setItemDesc(eventAwardDetails.getBqItem().getItemDescription());
					awardDetails.setQuantity(eventAwardDetails.getBqItem().getQuantity());
					awardDetails.setUnitPrice(eventAwardDetails.getAwardedPrice().divide(eventAwardDetails.getBqItem().getQuantity(), 3, RoundingMode.DOWN));

					if (eventAwardDetails.getBqItem().getUom() != null) {
						awardDetails.setUom(eventAwardDetails.getBqItem().getUom().getUom());
					}
					awardDetails.setItemCategory(eventAwardDetails.getBqItem().getField1());
					awardDetails.setBqItemCode(eventAwardDetails.getBqItem().getField2());
					awardDetails.setMfr_PartNO(eventAwardDetails.getBqItem().getField5());
					awardDetails.setMaterialGroup(eventAwardDetails.getBqItem().getField3());
					awardDetails.setItemNo(eventAwardDetails.getBqItem().getField8());

					awardDetailsList.add(awardDetails);
				}
				award.setBqItems(awardDetailsList);
				awardPojoList.add(award);
			}
			AwardResponsePojo resultMap = erpIntegrationService.sendAwardPage(awardPojoList, eventId, eventType);

			if (StringUtils.checkString(rfqAwardEvevtId).length() > 0) {
				RfqEventAward dbRfqEventAward = rfqEventAwardDao.findById(rfqAwardEvevtId);

				try {
					JasperPrint eventAward1 = getAwardSnapShotPdf(dbRfqEventAward, session, loggedInUser, resultMap, transfer, false);
					ByteArrayOutputStream workbook = getAwardSnapShotXcel(dbRfqEventAward, session, loggedInUser, resultMap, transfer, false);
					if (workbook != null) {
						byte[] snapshot = JasperExportManager.exportReportToPdf(eventAward1);
						byte[] excelSnapshot = workbook.toByteArray();

						LOG.info("SUSPEND FILE Size : " + snapshot.length);
						RfqEventAwardAudit audit = new RfqEventAwardAudit(dbRfqEventAward.getRfxEvent(), loggedInUser.getBuyer(), loggedInUser, new Date(), "Award Save and transfer", snapshot, excelSnapshot);
						eventAwardAuditService.saveRfqAwardAudit(audit);
					}
				} catch (Exception e) {
					LOG.error("Error while :" + e.getMessage(), e);
				}
			}
		}

		if (rfqEvent != null && rfqEvent.getAwardDate() == null) {
			rfqEvent.setAwardDate(new Date());
			rfqEvent.setConcludeDate(new Date());
		}

		rfqEvent.setStatus(EventStatus.FINISHED);
		rfqEvent.setConcludeBy(loggedInUser);
		rfqEvent.setAwarded(Boolean.TRUE);
		rfqEventDao.update(rfqEvent);

	}

	private JasperPrint getAwardSnapShotPdf(RfqEventAward dbRfqEventAward, HttpSession session, User loggedInUser, AwardResponsePojo awardResponsePojo, Boolean transfer, Boolean conclude) {

		// For Financial Standard
		DecimalFormat df = null;
		TimeZone timeZone = TimeZone.getDefault();
		String strTimeZone = (String) session.getAttribute(Global.SESSION_TIME_ZONE_KEY);
		if (strTimeZone != null) {
			timeZone = TimeZone.getTimeZone(strTimeZone);
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
		sdf.setTimeZone(timeZone);

		if (dbRfqEventAward.getRfxEvent().getDecimal().equals("1")) {
			df = new DecimalFormat("#,###,###,##0.0");
		} else if (dbRfqEventAward.getRfxEvent().getDecimal().equals("2")) {
			df = new DecimalFormat("#,###,###,##0.00");
		} else if (dbRfqEventAward.getRfxEvent().getDecimal().equals("3")) {
			df = new DecimalFormat("#,###,###,##0.000");
		} else if (dbRfqEventAward.getRfxEvent().getDecimal().equals("4")) {
			df = new DecimalFormat("#,###,###,##0.0000");
		} else if (dbRfqEventAward.getRfxEvent().getDecimal().equals("5")) {
			df = new DecimalFormat("#,###,###,##0.00000");
		} else if (dbRfqEventAward.getRfxEvent().getDecimal().equals("6")) {
			df = new DecimalFormat("#,###,###,##0.000000");
		}

		JasperPrint eventAward = null;
		List<RftEventAwardDetailsPojo> auditTrailSummary = new ArrayList<RftEventAwardDetailsPojo>();

		List<RfqEventAwardDetails> rfqEventAwardDetails = dbRfqEventAward.getRfxAwardDetails();

		if (CollectionUtil.isNotEmpty(rfqEventAwardDetails)) {

			for (RfqEventAwardDetails rfqEventAwardDetail : rfqEventAwardDetails) {
				RftEventAwardDetailsPojo rftEventAwardDetailsPojo = new RftEventAwardDetailsPojo();
				if (rfqEventAwardDetail.getBqItem() != null) {
					String itemSeq = rfqEventAwardDetail.getBqItem().getLevel() + "." + rfqEventAwardDetail.getBqItem().getOrder();
					rftEventAwardDetailsPojo.setItemSeq(itemSeq);

					// rfqEventAwardDetail.getEventAward().getBq().getRfxEvent().getEventId();
					// rfqEventAwardDetail.getEventAward().getRfxEvent().getEventName();

					rftEventAwardDetailsPojo.setItemName(StringUtils.checkString(rfqEventAwardDetail.getBqItem().getItemName()));
					if (rfqEventAwardDetail.getSupplier() != null && rfqEventAwardDetail.getSupplier().getCompanyName() != null) {
						rftEventAwardDetailsPojo.setSupplierName(rfqEventAwardDetail.getSupplier().getCompanyName());
					} else {
						rftEventAwardDetailsPojo.setSupplierName("");
					}
					rftEventAwardDetailsPojo.setSupplierPrice(rfqEventAwardDetail.getOriginalPrice() != null ? df.format(rfqEventAwardDetail.getOriginalPrice()) : "");
					rftEventAwardDetailsPojo.setAwardedPrice(rfqEventAwardDetail.getAwardedPrice() != null ? df.format(rfqEventAwardDetail.getAwardedPrice()) : "");
					if ((rfqEventAwardDetail.getBqItem().getOrder() > 0)) {
						if (rfqEventAwardDetail.getTax() != null) {
							rftEventAwardDetailsPojo.setTax(df.format(rfqEventAwardDetail.getTax()));
						} else {
							rftEventAwardDetailsPojo.setTax("N/A");
						}
						if (awardResponsePojo != null) {
							if (CollectionUtil.isNotEmpty(awardResponsePojo.getRefNumList())) {
								for (AwardReferenceNumberPojo awardReferenceNumberPojo : awardResponsePojo.getRefNumList()) {
									if (rfqEventAwardDetail.getBqItem().getLevel() == awardReferenceNumberPojo.getLevel() && rfqEventAwardDetail.getBqItem().getOrder() == awardReferenceNumberPojo.getOrder()) {
										rftEventAwardDetailsPojo.setRefNo(awardReferenceNumberPojo.getReferenceNumber());
									}
								}
							} else {
								rftEventAwardDetailsPojo.setRefNo("N/A");
							}
						} else {
							rftEventAwardDetailsPojo.setRefNo("N/A");
						}
					} else {
						rftEventAwardDetailsPojo.setTax(" ");
					}
					// rftEventAwardDetailsPojo.setTax(((rftEventAwardDetail.getBqItem().getOrder() > 0) &&
					// rftEventAwardDetail.getTax() != null) ? df.format(rftEventAwardDetail.getTax()) : "N/A");
					rftEventAwardDetailsPojo.setTaxType(rfqEventAwardDetail.getTaxType() != null ? rfqEventAwardDetail.getTaxType().toString() : "");
					rftEventAwardDetailsPojo.setTotalPrice(rfqEventAwardDetail.getTotalPrice() != null ? df.format(rfqEventAwardDetail.getTotalPrice()) : "");

				} else {
					rftEventAwardDetailsPojo.setItemName(" ");
					rftEventAwardDetailsPojo.setSupplierName(" ");
					rftEventAwardDetailsPojo.setAwardedPrice(" ");
					rftEventAwardDetailsPojo.setTax(" ");
					rftEventAwardDetailsPojo.setTaxType("");
				}
				rftEventAwardDetailsPojo.setCurrentDate(sdf.format(new Date()));
				auditTrailSummary.add(rftEventAwardDetailsPojo);

			}

			/*
			 * rftEventAwardDetailsTotalPojo.setSupplierName("Total");
			 * rftEventAwardDetailsTotalPojo.setSupplierPrice(String.valueOf(df.format(dbRftEventAward.
			 * getTotalSupplierPrice())));
			 * rftEventAwardDetailsTotalPojo.setAwardedPrice(String.valueOf(df.format(dbRftEventAward.getTotalAwardPrice
			 * ())));
			 * rftEventAwardDetailsTotalPojo.setTotalPrice(String.valueOf(df.format(dbRftEventAward.getGrandTotalPrice()
			 * )));
			 */

			// auditTrailSummary.add(rftEventAwardDetailsTotalPojo);
		}

		Map<String, Object> parameters = new HashMap<String, Object>();

		try {
			Resource resource = applicationContext.getResource("classpath:reports/AwardAuditReport.jasper");
			File jasperfile = resource.getFile();
			parameters.put("AUDIT_TRAIL", auditTrailSummary);
			parameters.put("date", sdf.format(new Date()));
			parameters.put("title", "Award History");
			parameters.put("remark", dbRfqEventAward.getAwardRemarks());
			parameters.put("eventNo", dbRfqEventAward.getRfxEvent().getEventId());
			parameters.put("eventName", dbRfqEventAward.getRfxEvent().getEventName());
			parameters.put("bqName", dbRfqEventAward.getBq().getName());
			parameters.put("actionBy", loggedInUser.getName());
			if (Boolean.TRUE == transfer) {
				parameters.put("actionType", "Save & Transfer");
			} else if (Boolean.TRUE == conclude) {
				parameters.put("actionType", "Save & Conclude");
			} else {
				parameters.put("actionType", "Save");
			}

			parameters.put("totalSupplierPrice", String.valueOf(df.format(dbRfqEventAward.getTotalSupplierPrice())));
			parameters.put("totalAwardPrice", String.valueOf(df.format(dbRfqEventAward.getTotalAwardPrice())));
			parameters.put("GrandPrice", String.valueOf(df.format(dbRfqEventAward.getGrandTotalPrice())));

			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(auditTrailSummary, false);
			eventAward = JasperFillManager.fillReport(jasperfile.getPath(), parameters, beanCollectionDataSource);

		} catch (Exception e) {
			LOG.error("Could not generate Audit Trail PDF Report. " + e.getMessage(), e);
		}

		return eventAward;
	}

	private ByteArrayOutputStream getAwardSnapShotXcel(RfqEventAward dbRfqEventAward, HttpSession session, User loggedInUser, AwardResponsePojo awardResponsePojo, Boolean transfer, Boolean conclude) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DecimalFormat df = null;
		if (dbRfqEventAward.getRfxEvent().getDecimal().equals("1")) {
			df = new DecimalFormat("#,###,###,##0.0");
		} else if (dbRfqEventAward.getRfxEvent().getDecimal().equals("2")) {
			df = new DecimalFormat("#,###,###,##0.00");
		} else if (dbRfqEventAward.getRfxEvent().getDecimal().equals("3")) {
			df = new DecimalFormat("#,###,###,##0.000");
		} else if (dbRfqEventAward.getRfxEvent().getDecimal().equals("4")) {
			df = new DecimalFormat("#,###,###,##0.0000");
		} else if (dbRfqEventAward.getRfxEvent().getDecimal().equals("5")) {
			df = new DecimalFormat("#,###,###,##0.00000");
		} else if (dbRfqEventAward.getRfxEvent().getDecimal().equals("6")) {
			df = new DecimalFormat("#,###,###,##0.000000");
		}

		Workbook workbook = new XSSFWorkbook();
		try {
			List<RfqEventAwardDetails> rftEventAwardDetails = dbRfqEventAward.getRfxAwardDetails();
			Sheet spreadsheet = workbook.createSheet("History");
			Cell cell;
			CellStyle styleHeading = workbook.createCellStyle();
			Font font = workbook.createFont();
			font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
			styleHeading.setFont(font);
			styleHeading.setVerticalAlignment(CellStyle.ALIGN_CENTER);
			Row row1 = spreadsheet.createRow((short) 0);
			cell = row1.createCell(1);
			cell.setCellValue("Award History Report");
			CellStyle styleHeading1 = workbook.createCellStyle();
			font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
			styleHeading1.setFont(font);
			styleHeading1.setAlignment(CellStyle.ALIGN_CENTER);
			cell.setCellStyle(styleHeading1);
			spreadsheet.addMergedRegion(new CellRangeAddress(0, 1, 1, 7));

			row1 = spreadsheet.createRow((short) 2);
			cell = row1.createCell(1);
			cell.setCellValue("Event no : ");
			cell.setCellStyle(styleHeading);
			cell = row1.createCell(2);
			cell.setCellValue(dbRfqEventAward.getRfxEvent().getEventId());

			cell = row1.createCell(4);
			cell.setCellValue("Action By");
			cell.setCellStyle(styleHeading);
			cell = row1.createCell(5);
			cell.setCellValue(loggedInUser.getName());

			row1 = spreadsheet.createRow((short) 3);
			cell = row1.createCell(1);
			cell.setCellValue("Event name");
			cell.setCellStyle(styleHeading);
			cell = row1.createCell(2);
			cell.setCellValue(dbRfqEventAward.getRfxEvent().getEventName());

			cell = row1.createCell(4);
			cell.setCellValue("Action Type");
			cell.setCellStyle(styleHeading);
			cell = row1.createCell(5);
			if (Boolean.TRUE == transfer) {
				cell.setCellValue("Save & Transfer");
			} else if (Boolean.TRUE == conclude) {
				cell.setCellValue("Save & Conclude");
			} else {
				cell.setCellValue("Save");
			}

			row1 = spreadsheet.createRow((short) 4);
			cell = row1.createCell(1);
			cell.setCellValue("BQ name : ");
			cell.setCellStyle(styleHeading);
			cell = row1.createCell(2);
			cell.setCellValue(dbRfqEventAward.getBq().getName());

			TimeZone timeZone = TimeZone.getDefault();
			String strTimeZone = (String) session.getAttribute(Global.SESSION_TIME_ZONE_KEY);
			if (strTimeZone != null) {
				timeZone = TimeZone.getTimeZone(strTimeZone);
			}
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
			sdf.setTimeZone(timeZone);
			String strDate = sdf.format(new Date());
			cell = row1.createCell(4);
			cell.setCellValue("Action Date");
			cell.setCellStyle(styleHeading);
			cell = row1.createCell(5);
			cell.setCellValue(strDate);

			Row row = spreadsheet.createRow(7);

			int cellNo = 0;
			styleHeading.setVerticalAlignment(CellStyle.ALIGN_CENTER);
			for (String col : Global.EVENT_AWARD_EXCEL_COLUMNS) {
				cell = row.createCell(cellNo++);
				cell.setCellValue(col);
				cell.setCellStyle(styleHeading);
			}
			// Write Data into Excel
			int cellNumber = 0;
			int i = 8;
			for (RfqEventAwardDetails rftEventAwardDetail : rftEventAwardDetails) {
				row = spreadsheet.createRow(i++);
				int cellNum = 0;
				row.createCell(cellNum++).setCellValue(rftEventAwardDetail.getBqItem().getLevel() + "." + rftEventAwardDetail.getBqItem().getOrder());
				row.createCell(cellNum++).setCellValue(rftEventAwardDetail.getBqItem() != null ? StringUtils.checkString(rftEventAwardDetail.getBqItem().getItemName()).length() > 0 ? rftEventAwardDetail.getBqItem().getItemName() : "" : "");

				if (rftEventAwardDetail.getSupplier() != null) {
					row.createCell(cellNum++).setCellValue(StringUtils.checkString(rftEventAwardDetail.getSupplier().getCompanyName()).length() > 0 ? rftEventAwardDetail.getSupplier().getCompanyName() : "");
				} else {
					row.createCell(cellNum++).setCellValue("");
				}

				row.createCell(cellNum++).setCellValue(rftEventAwardDetail.getOriginalPrice() != null ? StringUtils.checkString(df.format(rftEventAwardDetail.getOriginalPrice())).length() > 0 ? df.format(rftEventAwardDetail.getOriginalPrice()) : "N/A" : "");
				row.createCell(cellNum++).setCellValue(rftEventAwardDetail.getAwardedPrice() != null ? df.format(rftEventAwardDetail.getAwardedPrice()) : "");
				// row.createCell(cellNum++).setCellValue("XYZ");
				// row.createCell(cellNum++).setCellValue("abc");

				row.createCell(cellNum++).setCellValue(rftEventAwardDetail.getTaxType() != null ? rftEventAwardDetail.getTaxType().toString() : "");
				if ((rftEventAwardDetail.getBqItem().getOrder() > 0)) {
					if (rftEventAwardDetail.getTax() != null) {
						row.createCell(cellNum++).setCellValue(df.format(rftEventAwardDetail.getTax()));
					} else {
						row.createCell(cellNum++).setCellValue("N/A");
					}
				} else {
					row.createCell(cellNum++).setCellValue(" ");
				}
				row.createCell(cellNum++).setCellValue(rftEventAwardDetail.getTotalPrice() != null ? df.format(rftEventAwardDetail.getTotalPrice()) : "");
				if ((rftEventAwardDetail.getBqItem().getOrder() > 0)) {
					if (awardResponsePojo != null) {
						if (CollectionUtil.isNotEmpty(awardResponsePojo.getRefNumList())) {
							for (AwardReferenceNumberPojo awardReferenceNumberPojo : awardResponsePojo.getRefNumList()) {
								if (rftEventAwardDetail.getBqItem().getLevel() == awardReferenceNumberPojo.getLevel() && rftEventAwardDetail.getBqItem().getOrder() == awardReferenceNumberPojo.getOrder()) {
									row.createCell(cellNum++).setCellValue(awardReferenceNumberPojo.getReferenceNumber());
								}
							}
						} else {
							row.createCell(cellNum++).setCellValue("N/A");
						}
					} else {
						row.createCell(cellNum++).setCellValue("N/A");
					}
				}
				cellNumber = cellNum;
			}
			i++;
			row = spreadsheet.createRow(i++);
			LOG.info("cellNumber : " + cellNumber);
			cell = row.createCell(cellNumber - 2);
			LOG.info("cellNumber : " + cellNumber);
			cell.setCellValue(df.format(dbRfqEventAward.getGrandTotalPrice()));
			cell.setCellStyle(styleHeading);
			try {
				cell = row.createCell(cellNumber - 5);
				cell.setCellValue(String.valueOf(df.format(dbRfqEventAward.getTotalAwardPrice())));
				cell.setCellStyle(styleHeading);
				cell = row.createCell(cellNumber - 6);
				cell.setCellValue(String.valueOf(df.format(dbRfqEventAward.getTotalAwardPrice())));
				cell.setCellStyle(styleHeading);
				cell = row.createCell(cellNumber - 7);
				cell.setCellValue("Total");
				cell.setCellStyle(styleHeading);
			} catch (Exception e) {
				LOG.error("Error while :" + e.getMessage(), e);
			}

			int j = (i++) + 1;
			row = spreadsheet.createRow(j);
			cell = row.createCell(0);
			cell.setCellValue("Remarks");
			cell.setCellStyle(styleHeading);

			Cell cellRemarks = row.createCell(1);
			cellRemarks.setCellValue(dbRfqEventAward.getAwardRemarks());
			for (int k = 0; k < 15; k++) {
				spreadsheet.autoSizeColumn(k, true);
			}

			workbook.write(baos);
		} catch (Exception e) {
			LOG.error("Error while :" + e.getMessage(), e);
		}

		return baos;
	}

	@Override
	public void downloadAwardAuditSnapshot(String id, HttpServletResponse response) throws IOException {
		RfqEventAwardAudit audit = eventAwardAuditService.findByRfqAuditId(id);
		response.setContentLength(audit.getSnapshot().length);
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "attachment; filename=\"Award_Audit_" + audit.getActionDate() + ".pdf\"");

		FileCopyUtils.copy(audit.getSnapshot(), response.getOutputStream());
		response.flushBuffer();
		response.setStatus(HttpServletResponse.SC_OK);

	}

	@Override
	public void downloadAwardAuditExcelSnapShot(String id, HttpServletResponse response) throws IOException {
		RfqEventAwardAudit audit = eventAwardAuditService.findByRfqAuditId(id);
		response.setContentLength(audit.getExcelSnapshot().length);
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=\"Award_Audit_" + audit.getActionDate() + ".xls\"");

		FileCopyUtils.copy(audit.getExcelSnapshot(), response.getOutputStream());
		response.flushBuffer();
		response.setStatus(HttpServletResponse.SC_OK);

	}

	@Override
	public void downloadAwardAttachFileSnapShot(String id, HttpServletResponse response) throws IOException {
		RfqEventAwardAudit audit = eventAwardAuditService.findByRfqAuditId(id);
		LOG.info("File Name:" + audit.getFileName());
		response.setContentLength(audit.getFileData().length);
		response.setContentType(audit.getCredContentType());
		response.addHeader("Content-Disposition", "attachment; filename=" + audit.getFileName());
		FileCopyUtils.copy(audit.getFileData(), response.getOutputStream());
		response.flushBuffer();
		response.setStatus(HttpServletResponse.SC_OK);

	}

	@Override
	public RfqEventAward rfqEventAwardDetailsByEventIdandBqId(String eventId, String bqId) {
		RfqEventAward eventAward = rfqEventAwardDao.rfqEventAwardDetailsByEventIdandBqId(eventId, bqId);
		if (eventAward != null) {
			if (eventAward.getRfxEvent() != null) {
				eventAward.getRfxEvent().getId();
				eventAward.getRfxEvent().getEventName();
			}
			if (CollectionUtil.isNotEmpty(eventAward.getRfxAwardDetails())) {
				for (RfqEventAwardDetails details : eventAward.getRfxAwardDetails()) {
					if (details.getSupplier() != null) {
						details.getSupplier().getId();
					}
					if (details.getBqItem() != null) {
						details.getBqItem().getId();
					}
					details.getAwardedPrice();
					details.getOriginalPrice();
					details.getTaxType();
					details.getTax();
				}

			}
		}
		return eventAward;
	}
}
