package com.privasia.procurehere.service.impl;

import java.util.List;

import javax.servlet.ServletContext;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.privasia.procurehere.core.dao.DeclarationDao;
import com.privasia.procurehere.core.dao.RfxTemplateDao;
import com.privasia.procurehere.core.entity.Declaration;
import com.privasia.procurehere.core.enums.DeclarationType;
import com.privasia.procurehere.core.pojo.DeclarationTemplatePojo;
import com.privasia.procurehere.core.pojo.TableDataInput;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.service.DeclarationService;

@Service
@Transactional(readOnly = true)
public class DeclarationServiceImpl implements DeclarationService {

	@Autowired
	DeclarationDao declarationDao;

	@Autowired
	RfxTemplateDao rfxTemplateDao;

	@Autowired
	ServletContext context;

	@Autowired
	MessageSource messageSource;

	@Transactional(readOnly = false)
	@Override
	public void saveOrUpdateDeclaration(Declaration declaration) {
		declarationDao.saveOrUpdate(declaration);

	}

	@Override
	public Declaration getDeclarationById(String id) {
		return declarationDao.findById(id);
	}

	@Transactional(readOnly = false)
	@Override
	public void updateDeclaration(Declaration declarationObj) {
		declarationDao.update(declarationObj);
	}

	@Transactional(readOnly = false)
	@Override
	public void deleteDeclaration(Declaration declarationObj) {
		if (rfxTemplateDao.isAssignedDeclarationToTemplate(declarationObj.getId())) {
			ConstraintViolationException cause = new ConstraintViolationException(messageSource.getMessage("declaration.error.delete.dataIntegrity", new Object[] { declarationObj.getTitle() }, Global.LOCALE), null, "Unique Constraint");
			throw new DataIntegrityViolationException("Constraint Violation", cause);
		}
		declarationDao.delete(declarationObj);

	}

	@Override
	public List<DeclarationTemplatePojo> findDeclarationsByTeantId(String loggedInUserTenantId, TableDataInput input) {
		return declarationDao.findDeclarationsByTeantId(loggedInUserTenantId, input);
	}

	@Override
	public long findTotalFilteredDeclarationsForTenant(String loggedInUserTenantId, TableDataInput input) {
		return declarationDao.findTotalFilteredDeclarationsForTenant(loggedInUserTenantId, input);
	}

	@Override
	public long findTotalActiveDeclarationsForTenant(String loggedInUserTenantId) {
		return declarationDao.findTotalActiveDeclarationsForTenant(loggedInUserTenantId);
	}

	@Override
	public List<Declaration> getDeclarationsByTypeForTenant(DeclarationType declarationType, String loggedInUserTenantId) {
		return declarationDao.getDeclarationsByTypeForTenant(declarationType, loggedInUserTenantId);
	}

	@Override
	public long findAssignedCountOfDeclaration(String declarationId) {
		return rfxTemplateDao.findAssignedCountOfDeclaration(declarationId);
	}

}