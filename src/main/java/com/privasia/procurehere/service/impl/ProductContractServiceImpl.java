package com.privasia.procurehere.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.privasia.procurehere.core.dao.ProductContractDao;
import com.privasia.procurehere.core.entity.ProductContract;
import com.privasia.procurehere.core.pojo.ProductContractPojo;
import com.privasia.procurehere.core.pojo.TableDataInput;
import com.privasia.procurehere.service.ProductContractService;

@Service
@Transactional(readOnly = true)
public class ProductContractServiceImpl implements ProductContractService {

	@Autowired
	ProductContractDao productContractDao;

	@Override
	public ProductContract findProductContractByReferenceNumber(String contractReferenceNumber, String tenantId) {
		return productContractDao.findByContractByReferenceNumber(contractReferenceNumber, tenantId);
	}

	@Transactional(readOnly = false)
	@Override
	public ProductContract createProductContract(ProductContract productContract) {
		return productContractDao.saveOrUpdate(productContract);
	}

	@Transactional(readOnly = false)
	@Override
	public ProductContract update(ProductContract productContract) {
		return productContractDao.update(productContract);
	}

	@Override
	public List<ProductContractPojo> findProductContractListForTenant(String loggedInUserTenantId, TableDataInput input) {
		return productContractDao.findProductContractListForTenant(loggedInUserTenantId, input);
	}

	@Override
	public long findTotalFilteredProductListForTenant(String loggedInUserTenantId, TableDataInput input) {
		return productContractDao.findTotalFilteredProductListForTenant(loggedInUserTenantId, input);
	}

	@Override
	public long findTotalProductListForTenant(String loggedInUserTenantId) {
		return productContractDao.findTotalProductListForTenant(loggedInUserTenantId);
	}

	@Override
	public ProductContract findProductContractById(String id, String loggedInUser) {
		return productContractDao.findProductContractById(id, loggedInUser);
	}

	@Override
	public ProductContract getProductContractById(String id) {
		ProductContract product=productContractDao.findById(id);
		if(product !=null && product.getSupplier()!=null) {
			product.getSupplier().getSupplier().getCompanyName();
		}
		return product;
	}

}