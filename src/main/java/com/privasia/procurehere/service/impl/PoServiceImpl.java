package com.privasia.procurehere.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.ImageIcon;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.privasia.procurehere.core.dao.BuyerAddressDao;
import com.privasia.procurehere.core.dao.EventIdSettingsDao;
import com.privasia.procurehere.core.dao.PoDao;
import com.privasia.procurehere.core.dao.PoFinanceDao;
import com.privasia.procurehere.core.dao.PoItemDao;
import com.privasia.procurehere.core.dao.PrItemDao;
import com.privasia.procurehere.core.entity.BusinessUnit;
import com.privasia.procurehere.core.entity.Buyer;
import com.privasia.procurehere.core.entity.BuyerAddress;
import com.privasia.procurehere.core.entity.FavouriteSupplier;
import com.privasia.procurehere.core.entity.FinancePo;
import com.privasia.procurehere.core.entity.Po;
import com.privasia.procurehere.core.entity.PoAudit;
import com.privasia.procurehere.core.entity.PoItem;
import com.privasia.procurehere.core.enums.PoStatus;
import com.privasia.procurehere.core.pojo.PoSupplierPojo;
import com.privasia.procurehere.core.pojo.SearchFilterPoPojo;
import com.privasia.procurehere.core.pojo.TableDataInput;
import com.privasia.procurehere.core.utils.CollectionUtil;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.core.utils.StringUtils;
import com.privasia.procurehere.service.BuyerSettingsService;
import com.privasia.procurehere.service.PoAuditService;
import com.privasia.procurehere.service.PoService;
import com.privasia.procurehere.web.controller.PrItemsSummaryPojo;
import com.privasia.procurehere.web.controller.PrSummaryPojo;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * @author Nitin Otageri
 */
@Service
@Transactional(readOnly = true)
public class PoServiceImpl implements PoService {

	private static final Logger LOG = Logger.getLogger(Global.PR_LOG);

	@Autowired
	EventIdSettingsDao eventIdSettingsDao;

	@Autowired
	PoDao poDao;

	@Autowired
	BuyerAddressDao buyerAddressDao;

	@Autowired
	PoItemDao poItemDao;

	@Autowired
	ServletContext context;

	@Autowired
	PoAuditService poAuditService;

	@Autowired
	MessageSource messageSource;

	@Autowired
	BuyerSettingsService buyerSettingsService;

	@Autowired
	PrItemDao prItemDao;

	@Autowired
	ApplicationContext applicationContext;

	@Autowired
	PoFinanceDao poFinanceDao;

	@Override
	@Transactional(readOnly = false)
	public Po createPo(Po po) {
		po.setPoId(eventIdSettingsDao.generateEventId(po.getBuyer().getId(), "PO"));
		LOG.info("Generated PO ID as : " + po.getPoId());
		return poDao.saveOrUpdate(po);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
	public Po savePo(Po po) {

		// Create Delivery Address if not already created.
		BuyerAddress deliveryAddress = buyerAddressDao.getBuyerAddressForTenantByTitle(po.getDeliveryAddress().getTitle(), po.getBuyer().getId());
		if (deliveryAddress == null) {
			deliveryAddress = buyerAddressDao.save(po.getDeliveryAddress());
		}
		po.setDeliveryAddress(deliveryAddress);

		// Create Correspondence Address if not already created.
		BuyerAddress correspondenceAddress = buyerAddressDao.getBuyerAddressForTenantByTitle(po.getCorrespondenceAddress().getTitle(), po.getBuyer().getId());
		if (correspondenceAddress == null) {
			correspondenceAddress = buyerAddressDao.save(po.getCorrespondenceAddress());
		}
		po.setCorrespondenceAddress(correspondenceAddress);

		// Create Delivery Address if not already created.
		if (CollectionUtil.isNotEmpty(po.getPoItems())) {
			for (PoItem poItem : po.getPoItems()) {
				if (poItem.getDeliveryAddress() != null && StringUtils.checkString(poItem.getDeliveryAddress().getId()).length() == 0) {
					deliveryAddress = buyerAddressDao.getBuyerAddressForTenantByTitle(poItem.getDeliveryAddress().getTitle(), po.getBuyer().getId());
					if (deliveryAddress == null) {
						deliveryAddress = buyerAddressDao.save(poItem.getDeliveryAddress());
					}
					poItem.setDeliveryAddress(deliveryAddress);
				}
			}
		}

		return poDao.saveOrUpdate(po);
	}

	@Override
	@Transactional(readOnly = false)
	public Po updatePo(Po po) {
		return poDao.saveOrUpdate(po);
	}

	@Override
	public List<PoItem> findAllPoItemByPoIdForSummary(String poId) {
		return poItemDao.getAllPoItemByPoId(poId);
	}

	@Override
	public long findTotalPo(String tenantId) {
		return poDao.findTotalPo(tenantId);
	}

	@Override
	public List<Po> findAllPo(String tenantId, TableDataInput input, Date startDate, Date endDate) {
		return poDao.findAllPo(tenantId, input, startDate, endDate);
	}

	@Override
	public long findTotalFilteredPo(String tenantId, TableDataInput input, Date startDate, Date endDate) {
		return poDao.findTotalFilteredPo(tenantId, input, startDate, endDate);
	}

	@Override
	public Po findPoById(String poId) {
		Po po = poDao.findById(poId);
		if (po != null) {
			if (po.getCurrency() != null) {
				po.getCurrency().getCurrencyCode();
			}
		}
		if (po.getSupplier() != null) {
			po.getSupplier().getFullName();
			if (po.getSupplier().getSupplier() != null) {
				po.getSupplier().getSupplier().getCompanyName();
			}
		}
		if (po.getBusinessUnit() != null) {
			po.getBusinessUnit().getDisplayName();
		}
		if (po.getDeliveryAddress() != null) {
			po.getDeliveryAddress().getTitle();
			if (po.getDeliveryAddress().getState() != null) {
				po.getDeliveryAddress().getState().getStateName();
				if (po.getDeliveryAddress().getState().getCountry() != null) {
					po.getDeliveryAddress().getState().getCountry().getCountryCode();
				}

			}
		}
		if (po.getCorrespondenceAddress() != null) {
			po.getCorrespondenceAddress().getTitle();
			if (po.getCorrespondenceAddress().getState() != null) {
				po.getCorrespondenceAddress().getState().getStateName();
				if (po.getCorrespondenceAddress().getState().getCountry() != null) {
					po.getCorrespondenceAddress().getState().getCountry().getCountryCode();
				}
			}
		}
		return po;
	}

	@Override
	public Po loadPoById(String poId) {
		Po po = poDao.findById(poId);
		if (po != null) {
			if (po.getCurrency() != null) {
				po.getCurrency().getCurrencyCode();
			}
		}
		if (po.getSupplier() != null) {
			po.getSupplier().getFullName();
			if (po.getSupplier().getSupplier() != null) {
				po.getSupplier().getSupplier().getCompanyName();
			}
		}
		if (po.getBusinessUnit() != null) {
			po.getBusinessUnit().getDisplayName();
		}
		if (po.getDeliveryAddress() != null) {
			po.getDeliveryAddress().getTitle();
			if (po.getDeliveryAddress().getState() != null) {
				po.getDeliveryAddress().getState().getStateName();
				if (po.getDeliveryAddress().getState().getCountry() != null) {
					po.getDeliveryAddress().getState().getCountry().getCountryCode();
				}

			}
		}
		if (po.getCorrespondenceAddress() != null) {
			po.getCorrespondenceAddress().getTitle();
			if (po.getCorrespondenceAddress().getState() != null) {
				po.getCorrespondenceAddress().getState().getStateName();
				if (po.getCorrespondenceAddress().getState().getCountry() != null) {
					po.getCorrespondenceAddress().getState().getCountry().getCountryCode();
				}
			}
		}
		if (CollectionUtil.isNotEmpty(po.getPoItems())) {
			for (PoItem item : po.getPoItems()) {
				item.getItemName();
			}
		}
		return po;
	}

	@Override
	public void downloadPoReports(String tenantId, String[] poArr, HttpServletResponse response, HttpSession session, boolean select_all, Date startDate, Date endDate, SearchFilterPoPojo searchFilterPoPojo) {
		try {
			List<PoSupplierPojo> poList = poDao.findSearchPoByIds(tenantId, poArr, select_all, startDate, endDate, searchFilterPoPojo);
			String downloadFolder = context.getRealPath("/WEB-INF/");
			String fileName = "poReports.xlsx";
			Path file = Paths.get(downloadFolder, fileName);
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("PO Report");

			// Style of Heading Cells
			CellStyle styleHeading = workbook.createCellStyle();
			Font font = workbook.createFont();
			font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
			styleHeading.setFont(font);
			styleHeading.setAlignment(CellStyle.ALIGN_CENTER);

			CellStyle totalStyle = workbook.createCellStyle();
			totalStyle.setAlignment(CellStyle.ALIGN_RIGHT);

			// Creating Headings
			Row rowHeading = sheet.createRow(0);
			int i = 0;
			for (String column : Global.SUPPLIER_PO_REPORT_EXCEL_COLUMNS) {
				Cell cell = rowHeading.createCell(i++);

				cell.setCellValue(column);
				cell.setCellStyle(styleHeading);
			}

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
			sdf.setTimeZone(TimeZone.getTimeZone((String) session.getAttribute(Global.SESSION_TIME_ZONE_KEY)));

			int r = 1;
			// Write Data into Excel
			for (PoSupplierPojo po : poList) {
				// For Financial Standard
				DecimalFormat df = null;
				if (po.getDecimal().equals("1")) {
					df = new DecimalFormat("#,###,###,##0.0");
				} else if (po.getDecimal().equals("2")) {
					df = new DecimalFormat("#,###,###,##0.00");
				} else if (po.getDecimal().equals("3")) {
					df = new DecimalFormat("#,###,###,##0.000");
				} else if (po.getDecimal().equals("4")) {
					df = new DecimalFormat("#,###,###,##0.0000");
				} else if (po.getDecimal().equals("5")) {
					df = new DecimalFormat("#,###,###,##0.00000");
				} else if (po.getDecimal().equals("6")) {
					df = new DecimalFormat("#,###,###,##0.000000");
				} else {
					df = new DecimalFormat("#,###,###,##0.00");
				}

				Row row = sheet.createRow(r++);
				int cellNum = 0;
				row.createCell(cellNum++).setCellValue(r - 1);
				row.createCell(cellNum++).setCellValue(StringUtils.checkString(po.getPoNumber()).length() > 0 ? po.getPoNumber() : "");
				row.createCell(cellNum++).setCellValue(StringUtils.checkString(po.getName()).length() > 0 ? po.getName() : "");
				row.createCell(cellNum++).setCellValue(StringUtils.checkString(po.getDescription()).length() > 0 ? po.getDescription() : "");
				row.createCell(cellNum++).setCellValue(StringUtils.checkString(po.getBuyerCompanyName()).length() > 0 ? po.getBuyerCompanyName() : "");
				row.createCell(cellNum++).setCellValue(StringUtils.checkString(po.getBusinessUnit()).length() > 0 ? po.getBusinessUnit() : "");
				row.createCell(cellNum++).setCellValue(po.getCreatedDate() != null ? sdf.format(po.getCreatedDate()) : "");
				row.createCell(cellNum++).setCellValue(po.getAcceptRejectDate() != null ? sdf.format(po.getAcceptRejectDate()) : "");
				row.createCell(cellNum++).setCellValue(StringUtils.checkString(po.getCurrency()).length() > 0 ? po.getCurrency() : "");
				Cell grandTotalCell = row.createCell(cellNum++);
				grandTotalCell.setCellStyle(totalStyle);
				grandTotalCell.setCellValue(po.getGrandTotal() != null ? df.format(po.getGrandTotal()) : "");
				row.createCell(cellNum++).setCellValue(po.getStatus() != null ? po.getStatus().toString() : "");

			}
			// Auto Fit
			for (int k = 0; k < 15; k++) {
				sheet.autoSizeColumn(k, true);
			}

			// Save Excel File
			FileOutputStream out = new FileOutputStream(downloadFolder + "/" + fileName);
			workbook.write(out);
			out.close();
			LOG.info("Successfully written in Excel");

			if (Files.exists(file)) {
				response.setContentType("application/vnd.ms-excel");
				response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
				try {
					Files.copy(file, response.getOutputStream());
					response.flushBuffer();
					response.setStatus(HttpServletResponse.SC_OK);
					LOG.info("Successfully written in Excel===========================");
				} catch (IOException e) {
					LOG.error("Error :- " + e.getMessage(), e);
				}
			}
			LOG.info("updating Po Report Sent");
		} catch (Exception e) {
			LOG.error("Error while downloading PO Reports Excel : " + e.getMessage(), e);
		}
	}

	@Override
	public long findPoCountBasedOnStatusAndTenant(String loggedInUser, String tenantId, PoStatus status) {
		return poDao.findPoCountBasedOnStatusAndTenant(loggedInUser, tenantId, status);
	}

	@Override
	public List<Po> findAllPoByStatus(String loggedInUser, String tenantId, TableDataInput input, PoStatus status) {
		return poDao.findAllPoByStatus(loggedInUser, tenantId, input, status);
	}

	@Override
	public long findTotalFilteredPoByStatus(String loggedInUserId, String tenantId, TableDataInput input, PoStatus status) {
		return poDao.findTotalFilteredPoByStatus(loggedInUserId, tenantId, input, status);
	}

	@Override
	public Po getLoadedPoById(String poId) {
		Po po = poDao.findByPoId(poId);
		if (po != null) {
			LOG.info("PO Not null:" + po.getPoNumber());
			if (po.getCorrespondenceAddress() != null) {
				po.getCorrespondenceAddress().getState().getCountry().getCountryName();
			}
			if (po.getBuyer() != null) {
				po.getBuyer().getCompanyName();
				po.getBuyer().getCompanyContactNumber();
				po.getBuyer().getFaxNumber();
				po.getBuyer().getLine1();
				po.getBuyer().getLine2();
				po.getBuyer().getCity();
				if (po.getBuyer().getState() != null)
					po.getBuyer().getState().getStateName();
				if (po.getBuyer().getRegistrationOfCountry() != null)
					po.getBuyer().getRegistrationOfCountry().getCountryName();
			}
			if (po.getSupplier() != null) {
				FavouriteSupplier favSupplier = po.getSupplier();
				favSupplier.getSupplier().getCompanyName();
				favSupplier.getSupplier().getFaxNumber();
				favSupplier.getSupplier().getCompanyContactNumber();
				favSupplier.getSupplier().getLine1();
				favSupplier.getSupplier().getLine2();
				favSupplier.getSupplier().getCity();
				if (favSupplier.getSupplier().getState() != null) {
					favSupplier.getSupplier().getState().getStateName();
				}
				if (favSupplier.getSupplier().getRegistrationOfCountry() != null) {
					favSupplier.getSupplier().getRegistrationOfCountry().getCountryCode();
				}
			}
			if (po.getBusinessUnit() != null) {
				po.getBusinessUnit().getUnitName();
			}
			if (po.getCreatedBy() != null) {
				po.getCreatedBy().getLoginId();
				po.getCreatedBy().getName();
			}
		}
		return po;

	}

	@Override
	public List<PoItem> findAllPoItemByPoId(String poId) {
		List<PoItem> returnList = new ArrayList<PoItem>();
		List<PoItem> list = poItemDao.getAllPoItemByPoId(poId);
		LOG.info("List :" + list.size());
		if (CollectionUtil.isNotEmpty(list)) {
			for (PoItem item : list) {
				PoItem parent = item.createShallowCopy();
				if (item.getParent() == null) {
					returnList.add(parent);
				}

				if (item.getProductCategory() != null) {
					item.getProductCategory().getProductName();
				}

				if (CollectionUtil.isNotEmpty(item.getChildren())) {
					for (PoItem child : item.getChildren()) {
						if (parent.getChildren() == null) {
							parent.setChildren(new ArrayList<PoItem>());
						}
						if (child.getUnit() != null) {
							child.getUnit().getUom();
						}

						if (child.getProductCategory() != null) {
							child.getProductCategory().getProductName();
						}

						parent.getChildren().add(child.createShallowCopy());
					}
				}
			}
		}
		return returnList;
	}

	@Override
	@Transactional(readOnly = false, rollbackFor = Exception.class)
	public void updatePoStatus(Po po, PoAudit buyerAudit, PoAudit supplierAudit) {
		poDao.saveOrUpdate(po);
		LOG.info("Po Audit :" + buyerAudit.toLogString());
		poAuditService.save(buyerAudit);
		poAuditService.save(supplierAudit);

		if (PoStatus.CANCELLED == po.getStatus()) {
			// If any New status finance POs delete
			List<FinancePo> poList = poFinanceDao.findFinancePoByIdsAndStatus(po.getId());
			if (CollectionUtil.isNotEmpty(poList)) {
				for (FinancePo fpo : poList) {
					poFinanceDao.deletePo(fpo.getId());
				}
			}
		}
	}

	@Override
	public List<Po> findAllSearchFilterPo(String loggedInUserId, String loggedInUserTenantId, TableDataInput input, Date startDate, Date endDate) {
		return poDao.findAllSearchFilterPo(loggedInUserId, loggedInUserTenantId, input, startDate, endDate);
	}

	@Override
	public long findTotalSearchFilterPoCount(String loggedInuserId, String loggedInUserTenantId, TableDataInput input, Date startDate, Date endDate) {
		return poDao.findTotalSearchFilterPoCount(loggedInuserId, loggedInUserTenantId, input, startDate, endDate);
	}

	@Override
	@Transactional(readOnly = false)
	public void downloadBuyerPoReports(String tenantId, String[] poArr, HttpServletResponse response, HttpSession session, boolean select_all, Date startDate, Date endDate, SearchFilterPoPojo searchFilterPoPojo, String loggedInUser) {
		LOG.info("Download PO reports... " + response.getHeaderNames());
		try {
			List<Po> poList = poDao.findSearchBuyerPoByIds(tenantId, poArr, select_all, startDate, endDate, searchFilterPoPojo, loggedInUser);
			LOG.info("poList>>>>>>>>" + poList.size());
			String downloadFolder = context.getRealPath("/WEB-INF/");
			String fileName = "poReports.xlsx";
			Path file = Paths.get(downloadFolder, fileName);
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("PO Report");

			// Style of Heading Cells
			CellStyle styleHeading = workbook.createCellStyle();
			Font font = workbook.createFont();

			font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
			styleHeading.setFont(font);
			styleHeading.setAlignment(CellStyle.ALIGN_CENTER);

			CellStyle totalStyle = workbook.createCellStyle();
			totalStyle.setAlignment(CellStyle.ALIGN_RIGHT);

			// Creating Headings
			Row rowHeading = sheet.createRow(0);
			int i = 0;
			for (String column : Global.BUYER_PO_REPORT_EXCEL_COLUMNS) {
				Cell cell = rowHeading.createCell(i++);

				cell.setCellValue(column);
				cell.setCellStyle(styleHeading);
			}

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
			sdf.setTimeZone(TimeZone.getTimeZone((String) session.getAttribute(Global.SESSION_TIME_ZONE_KEY)));

			int r = 1;
			// Write Data into Excel
			for (Po po : poList) {
				// For Financial Standard
				DecimalFormat df = null;
				if (po.getDecimal().equals("1")) {
					df = new DecimalFormat("#,###,###,##0.0");
				} else if (po.getDecimal().equals("2")) {
					df = new DecimalFormat("#,###,###,##0.00");
				} else if (po.getDecimal().equals("3")) {
					df = new DecimalFormat("#,###,###,##0.000");
				} else if (po.getDecimal().equals("4")) {
					df = new DecimalFormat("#,###,###,##0.0000");
				} else if (po.getDecimal().equals("5")) {
					df = new DecimalFormat("#,###,###,##0.00000");
				} else if (po.getDecimal().equals("6")) {
					df = new DecimalFormat("#,###,###,##0.000000");
				} else {
					df = new DecimalFormat("#,###,###,##0.00");
				}

				Row row = sheet.createRow(r++);
				int cellNum = 0;
				row.createCell(cellNum++).setCellValue(r - 1);
				row.createCell(cellNum++).setCellValue(po.getPoNumber() != null ? po.getPoNumber() : "");
				row.createCell(cellNum++).setCellValue(po.getName() != null ? po.getName() : "");
				row.createCell(cellNum++).setCellValue((po.getSupplier() != null && po.getSupplier().getFullName() != null) ? po.getSupplier().getSupplier().getCompanyName() : (po.getSupplierName() != null ? po.getSupplierName() : ""));
				row.createCell(cellNum++).setCellValue(po.getCreatedBy() != null ? po.getCreatedBy().getName() : "");
				row.createCell(cellNum++).setCellValue(po.getCreatedDate() != null ? sdf.format(po.getCreatedDate()) : "");
				row.createCell(cellNum++).setCellValue(po.getOrderedBy() != null ? po.getOrderedBy().getName() : "");
				row.createCell(cellNum++).setCellValue(po.getOrderedDate() != null ? sdf.format(po.getOrderedDate()) : "");
				row.createCell(cellNum++).setCellValue(po.getActionDate() != null ? sdf.format(po.getActionDate()) : "");
				row.createCell(cellNum++).setCellValue(po.getCurrency() != null ? po.getCurrency().getCurrencyCode() : "");
				Cell grandTotalCell = row.createCell(cellNum++);
				grandTotalCell.setCellValue(po.getGrandTotal() != null ? df.format(po.getGrandTotal()) : "");
				grandTotalCell.setCellStyle(totalStyle);
				row.createCell(cellNum++).setCellValue(po.getBusinessUnit() != null ? po.getBusinessUnit().getUnitName() : "");
				row.createCell(cellNum++).setCellValue(po.getStatus() != null ? po.getStatus().toString() : "");

			}
			// Auto Fit
			for (int k = 0; k < 15; k++) {
				sheet.autoSizeColumn(k, true);
			}

			// Save Excel File
			FileOutputStream out = new FileOutputStream(downloadFolder + "/" + fileName);
			workbook.write(out);
			out.close();
			LOG.info("Successfully written in Excel");

			if (Files.exists(file)) {
				response.setContentType("application/vnd.ms-excel");
				response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
				try {
					Files.copy(file, response.getOutputStream());
					response.flushBuffer();
					response.setStatus(HttpServletResponse.SC_OK);
					LOG.info("Successfully written in Excel===========================");
				} catch (IOException e) {
					LOG.error("Error :- " + e.getMessage(), e);
				}
			}
		} catch (Exception e) {
			LOG.error("Error while downloading PO Reports Excel : " + e.getMessage(), e);
		}

	}

	@Override
	public List<Po> findAllPoforSharingAll(String supplierId, String buyerId) {
		return poDao.findAllPoforSharingAll(supplierId, buyerId);
	}

	@Override
	public String getBusinessUnitname(String poId) {
		String displayName = null;
		displayName = poDao.getBusineessUnitname(poId);
		return StringUtils.checkString(displayName);
	}

	@Override
	public JasperPrint getBuyerPoPdf(Po po, String strTimeZone) {

		JasperPrint jasperPrint = null;
		Map<String, Object> parameters = new HashMap<String, Object>();
		TimeZone timeZone = TimeZone.getDefault();

		if (strTimeZone != null) {
			timeZone = TimeZone.getTimeZone(strTimeZone);
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm a");
		sdf.setTimeZone(timeZone);

		DecimalFormat df = null;
		if (po.getDecimal().equals("1")) {
			df = new DecimalFormat("#,###,###,##0.0");
		} else if (po.getDecimal().equals("2")) {
			df = new DecimalFormat("#,###,###,##0.00");
		} else if (po.getDecimal().equals("3")) {
			df = new DecimalFormat("#,###,###,##0.000");
		} else if (po.getDecimal().equals("4")) {
			df = new DecimalFormat("#,###,###,##0.0000");
		} else if (po.getDecimal().equals("5")) {
			df = new DecimalFormat("#,###,###,##0.00000");
		} else if (po.getDecimal().equals("6")) {
			df = new DecimalFormat("#,###,###,##0.000000");
		} else {
			df = new DecimalFormat("#,###,###,##0.00");
		}

		try {
			Resource resource = applicationContext.getResource("classpath:reports/PrSummary.jasper");
			File jasperfile = resource.getFile();

			PrSummaryPojo summary = new PrSummaryPojo();
			String createDate = po.getCreatedDate() != null ? sdf.format(po.getCreatedDate()).toUpperCase() : "";
			String deliveryDate = po.getDeliveryDate() != null ? sdf.format(po.getDeliveryDate()).toUpperCase() : "";

			summary.setPrName(po.getName());
			summary.setRemarks(po.getRemarks());
			summary.setPaymentTerm(po.getPaymentTerm());
			summary.setTermsAndConditions(po.getTermsAndConditions() != null ? po.getTermsAndConditions().replaceAll("(?m)^[ \t]*\r?\n", "") : "");
			summary.setRequester(po.getRequester());
			summary.setPoNumber(po.getPoNumber());
			summary.setCreatedDate(createDate);

			BusinessUnit bUnit = po.getBusinessUnit();
			Buyer buyer = po.getBuyer();
			// Buyer Address
			String buyerAddress = "";

			if (bUnit != null) {
				ImageIcon n;
				if (bUnit.getFileAttatchment() != null) {
					n = new ImageIcon(bUnit.getFileAttatchment());
					summary.setComanyName(null);
				} else {
					n = new ImageIcon();
					summary.setComanyName(bUnit.getDisplayName());
				}
				summary.setLogo(n.getImage());

				getSummaryOfAddressAndPoitems(po, df, summary, deliveryDate);
				if (StringUtils.checkString(bUnit.getLine1()).length() > 0) {
					buyerAddress = bUnit.getLine1() + "\r\n";
				} else {
					buyerAddress = " \r\n";
				}
				if (StringUtils.checkString(bUnit.getLine2()).length() > 0) {
					buyerAddress += bUnit.getLine2() + "\r\n";
				} else {
					buyerAddress += " \r\n";
				}
				if (StringUtils.checkString(bUnit.getLine3()).length() > 0) {
					buyerAddress += bUnit.getLine3() + "\r\n";
				} else {
					buyerAddress += " \r\n";
				}
				if (StringUtils.checkString(bUnit.getLine4()).length() > 0) {
					buyerAddress += bUnit.getLine4() + "\r\n";
				} else {
					buyerAddress += " \r\n";
				}
				if (StringUtils.checkString(bUnit.getLine5()).length() > 0) {
					buyerAddress += bUnit.getLine5() + "\r\n";
				} else {
					buyerAddress += " \r\n";
				}
				if (StringUtils.checkString(bUnit.getLine6()).length() > 0) {
					buyerAddress += bUnit.getLine6() + "\r\n";
				} else {
					buyerAddress += " \r\n";
				}
				if (StringUtils.checkString(bUnit.getLine7()).length() > 0) {
					buyerAddress += bUnit.getLine7() + "\r\n";
				} else {
					buyerAddress += " \r\n";
				}

				summary.setDisplayName(bUnit.getDisplayName());
			} else {
				buyerAddress += "\r\n" + buyer.getCity();
				if (buyer.getState() != null) {
					buyerAddress += ", " + buyer.getState().getStateName();
					buyerAddress += "\r\n" + buyer.getState().getCountry().getCountryName();
				}
				buyerAddress += "\r\n";
				buyerAddress += "TEL : " + buyer.getCompanyContactNumber();
				buyerAddress += " FAX : ";
				if (buyer.getFaxNumber() != null) {
					buyerAddress += buyer.getFaxNumber();
				}
				summary.setComanyName(buyer.getCompanyName());
				summary.setDisplayName(buyer.getCompanyName());
			}
			LOG.info("buyerAddress : " + buyerAddress);
			summary.setBuyerAddress(buyerAddress);

			List<PrSummaryPojo> prSummary = Arrays.asList(summary);

			// Supplier Address
			String supplierAddress = "";

			if (po.getSupplier() != null) {
				FavouriteSupplier supplier = po.getSupplier();
				supplierAddress += supplier.getSupplier().getCompanyName() + "\r\n";
				supplierAddress += supplier.getSupplier().getLine1();
				if (StringUtils.checkString(po.getSupplier().getSupplier().getLine2()).length() > 0) {
					supplierAddress += "\r\n" + supplier.getSupplier().getLine2();
				}
				supplierAddress += "\r\n" + supplier.getSupplier().getCity() + ", ";
				if (supplier.getSupplier().getState() != null) {
					supplierAddress += supplier.getSupplier().getState().getStateName() + "\r\n\n";
				}
				supplierAddress += "TEL : ";

				if (supplier.getSupplier().getCompanyContactNumber() != null) {
					supplierAddress += supplier.getSupplier().getCompanyContactNumber();
				}
				supplierAddress += "\r\nFAX : ";
				if (supplier.getSupplier().getFaxNumber() != null) {
					supplierAddress += supplier.getSupplier().getFaxNumber() + "\n\n";
				}
				supplierAddress += "Attention: " + supplier.getFullName() + "\nEmail: " + supplier.getCommunicationEmail() + "\n";
			} else {
				supplierAddress += po.getSupplierName() + "\r\n";
				supplierAddress += po.getSupplierAddress() + "\r\n\n";
				supplierAddress += "TEL :";
				if (po.getSupplierTelNumber() != null) {
					supplierAddress += po.getSupplierTelNumber();
				}
				supplierAddress += "\r\nFAX : ";
				if (po.getSupplierFaxNumber() != null) {
					supplierAddress += po.getSupplierFaxNumber();
				}
			}
			if (po.getSupplier() != null) {
				summary.setSupplierName(po.getSupplier().getSupplier() != null ? po.getSupplier().getSupplier().getCompanyName() : "");
			} else {
				summary.setSupplierName(po.getSupplierName());
			}
			summary.setSupplierAddress(supplierAddress);
			summary.setTaxnumber(po.getSupplierTaxNumber() != null ? po.getSupplierTaxNumber() : "");

			parameters.put("PR_SUMMARY", prSummary);
			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(prSummary, false);
			jasperPrint = JasperFillManager.fillReport(jasperfile.getPath(), parameters, beanCollectionDataSource);
		} catch (

		Exception e) {
			LOG.error("Could not generate PO  Report. " + e.getMessage(), e);
		}
		return jasperPrint;
	}

	private void getSummaryOfAddressAndPoitems(Po po, DecimalFormat df, PrSummaryPojo summary, String deliveryDate) {

		try {

			// Delivery Address

			String deliveryAddress = "";
			if (po.getDeliveryAddress() != null) {
				deliveryAddress += po.getDeliveryAddress().getTitle() + "\n";
				deliveryAddress += po.getDeliveryAddress().getLine1();
				if (po.getDeliveryAddress().getLine2() != null) {
					deliveryAddress += "\n" + po.getDeliveryAddress().getLine2();
				}
				deliveryAddress += "\n" + po.getDeliveryAddress().getZip() + ", " + po.getDeliveryAddress().getCity() + "\n";
				deliveryAddress += po.getDeliveryAddress().getState().getStateName() + ", " + po.getDeliveryAddress().getState().getCountry().getCountryName();
			}

			summary.setDeliveryAddress(deliveryAddress);
			summary.setDeliveryReceiver(po.getDeliveryReceiver());
			summary.setDeliveryDate(deliveryDate);

			// Correspondence Address
			if (po.getCorrespondenceAddress() != null) {

				String correspondAddress = "";
				correspondAddress += po.getCorrespondenceAddress().getTitle();
				correspondAddress += "\r\n" + po.getCorrespondenceAddress().getLine1();
				if (po.getCorrespondenceAddress().getLine2() != null) {
					correspondAddress += ", " + po.getCorrespondenceAddress().getLine2();
				}
				correspondAddress += "\r\n" + po.getCorrespondenceAddress().getZip() + ", " + po.getCorrespondenceAddress().getCity();
				correspondAddress += "\r\n" + po.getCorrespondenceAddress().getState().getStateName() + ", " + po.getCorrespondenceAddress().getState().getCountry().getCountryName();
				summary.setCorrespondAddress(correspondAddress);
			}
			// Po items List
			List<PrItemsSummaryPojo> prItemList = new ArrayList<PrItemsSummaryPojo>();
			List<PoItem> poList = findAllPoItemByPoId(po.getId());
			if (CollectionUtil.isNotEmpty(poList)) {
				for (PoItem item : poList) {
					PrItemsSummaryPojo pos = new PrItemsSummaryPojo();
					pos.setSlno(item.getLevel() + "." + item.getOrder());
					pos.setItemName(item.getItemName());
					pos.setCurrency(item.getPo().getCurrency().getCurrencyCode());
					pos.setItemDescription(item.getItemDescription());
					pos.setAdditionalTax(df.format(item.getPo().getAdditionalTax()));
					pos.setGrandTotal(df.format(item.getPo().getGrandTotal()));
					pos.setSumAmount(df.format(po.getTotal()));
					pos.setTaxDescription(item.getPo().getTaxDescription());
					pos.setDecimal(po.getDecimal());
					prItemList.add(pos);
					if (item.getChildren() != null) {
						for (PoItem childItem : item.getChildren()) {
							PrItemsSummaryPojo childPr = new PrItemsSummaryPojo();
							childPr.setSlno(childItem.getLevel() + "." + childItem.getOrder());
							childPr.setItemName(childItem.getItemName());
							childPr.setItemDescription(childItem.getItemDescription());
							childPr.setQuantity(df.format(childItem.getQuantity()));
							childPr.setUnitPrice(df.format(childItem.getUnitPrice()));
							childPr.setTaxAmount(df.format(childItem.getTaxAmount()));
							childPr.setTotalAmount(df.format(childItem.getTotalAmount()));
							childPr.setTotalAmountWithTax(df.format(childItem.getTotalAmountWithTax()));
							childPr.setUom(childItem.getProduct() != null ? (childItem.getProduct().getUom() != null ? childItem.getProduct().getUom().getUom() : "") : (childItem.getUnit() != null ? childItem.getUnit().getUom() : ""));
							childPr.setCurrency(childItem.getPo().getCurrency().getCurrencyCode());
							childPr.setAdditionalTax(df.format(childItem.getPo().getAdditionalTax()));
							childPr.setGrandTotal(df.format(childItem.getPo().getGrandTotal()));
							childPr.setSumAmount(df.format(po.getTotal()));
							childPr.setTaxDescription(childItem.getPo().getTaxDescription());
							childPr.setSumTaxAmount(childItem.getTaxAmount());
							childPr.setSumTotalAmt(childItem.getTotalAmount());
							childPr.setDecimal(po.getDecimal());
							prItemList.add(childPr);
						}
					}

				}
			}
			summary.setPrItems(prItemList);
		} catch (Exception e) {
			LOG.error("Could not Get PO Items and Address " + e.getMessage(), e);
		}
	}

	@Override
	public Po findByPrId(String prId) {
		return poDao.findByPrId(prId);
	}

	@Override
	public Po findSupplierByFavSupplierId(String id) {
		return poDao.findSupplierByFavSupplierId(id);
	}
}
