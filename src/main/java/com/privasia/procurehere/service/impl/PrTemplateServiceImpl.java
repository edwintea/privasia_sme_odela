package com.privasia.procurehere.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.privasia.procurehere.core.dao.PrTemplateDao;
import com.privasia.procurehere.core.dao.UserDao;
import com.privasia.procurehere.core.entity.PrTemplate;
import com.privasia.procurehere.core.entity.PrTemplateApproval;
import com.privasia.procurehere.core.entity.PrTemplateApprovalUser;
import com.privasia.procurehere.core.entity.PrTemplateField;
import com.privasia.procurehere.core.entity.RfxTemplate;
import com.privasia.procurehere.core.entity.TemplatePrTeamMembers;
import com.privasia.procurehere.core.entity.User;
import com.privasia.procurehere.core.pojo.PrTemplatePojo;
import com.privasia.procurehere.core.pojo.TableDataInput;
import com.privasia.procurehere.core.supplier.dao.BuyerDao;
import com.privasia.procurehere.core.utils.CollectionUtil;
import com.privasia.procurehere.core.utils.SecurityLibrary;
import com.privasia.procurehere.service.PrTemplateService;
import com.privasia.procurehere.service.UserService;

/**
 * @author parveen
 */
@Service
@Transactional(readOnly = true)
public class PrTemplateServiceImpl implements PrTemplateService {

	@SuppressWarnings("unused")
	private static Logger LOG = Logger.getLogger(PrTemplateServiceImpl.class);

	@Autowired
	PrTemplateDao prTemplateDao;

	@Autowired
	BuyerDao buyerDao;

	@Autowired
	UserDao userDao;

	@Autowired
	UserService userService;

	@Autowired
	PrTemplateService prTemplateService;

	@Override
	public List<PrTemplate> findTemplatesForTenant(String tenantId, TableDataInput tableParams, String userId) {
		List<PrTemplate> returnList = new ArrayList<PrTemplate>();

		List<PrTemplate> list = prTemplateDao.findTemplatesForTenant(tenantId, tableParams, userId);
		for (PrTemplate rt : list) {
			returnList.add(rt.createShallowCopy());
		}
		return returnList;
	}

	@Override
	public boolean isExists(PrTemplate template) {
		return prTemplateDao.isExists(template);
	}

	@Override
	public List<PrTemplate> findAllActiveTemplatesForTenant(String tenantId) {
		return prTemplateDao.findAllActiveTemplatesForTenant(tenantId);
	}

	@Override
	@Transactional(readOnly = false)
	public PrTemplate save(PrTemplate template) {
		return prTemplateDao.saveOrUpdate(template);
	}

	@Override
	@Transactional(readOnly = false)
	public void update(PrTemplate template) {
		prTemplateDao.update(template);
	}

	@Override
	@Transactional(readOnly = false)
	public void delete(PrTemplate template) {
		prTemplateDao.delete(template);
	}

	@Override
	public PrTemplate getPrTemplateById(String id) {
		PrTemplate template = prTemplateDao.findById(id);
		for (PrTemplateApproval approval : template.getApprovals()) {
			for (PrTemplateApprovalUser approvalUser : approval.getApprovalUsers()) {
				approvalUser.getRemarks();
			}
		}
		if (CollectionUtil.isNotEmpty(template.getTeamMembers())) {
			for (TemplatePrTeamMembers team : template.getTeamMembers()) {
				team.getTeamMemberType();
				if (team.getUser() != null) {
					team.getUser().getId();
				}
			}
		}
		return template;
	}

	@Override
	public long findTotalTemplatesForTenant(String tenantId) {
		return prTemplateDao.findTotalTemplatesForTenant(tenantId);
	}

	@Override
	public long findTotalFilteredTemplatesForTenant(String tenantId, TableDataInput tableParams, String userId) {

		return prTemplateDao.findTotalFilteredTemplatesForTenant(tenantId, tableParams, userId);
	}

	@Override
	public PrTemplate getPrTemplateForEditById(String templateId, String tenantId) {
		PrTemplate template = prTemplateDao.getPrTemplateForEditById(templateId, tenantId);
		if (template.getApprovals() != null) {
			for (PrTemplateApproval approval : template.getApprovals()) {
				for (PrTemplateApprovalUser approvalUser : approval.getApprovalUsers()) {
					approvalUser.getRemarks();
				}
			}
			if (CollectionUtil.isNotEmpty(template.getTeamMembers())) {
				for (TemplatePrTeamMembers team : template.getTeamMembers()) {
					team.getTeamMemberType();
					if (team.getUser() != null) {
						team.getUser().getLoginId();
					}
				}
			}
		}
		return template;

	}

	@Override
	public List<PrTemplate> findByTemplateNameForTenant(String searchValue, String tenantId, String userId, String pageNo) {
		return prTemplateDao.findByTemplateNameForTenant(searchValue, tenantId, userId, pageNo);
	}

	@Override
	public Integer findAssignedTemplateCount(String templateId) {
		return prTemplateDao.findAssignedTemplateCount(templateId);

	}

	@Override
	public List<PrTemplate> findAllActiveTemplatesForTenantAndUser(String tenantId, String userId, TableDataInput input) {
		return prTemplateDao.findAllActiveTemplatesForTenantAndUser(tenantId, userId, input);
	}

	@Override
	@Transactional(readOnly = false)
	public PrTemplate copyTemplate(PrTemplate oldTemplate, PrTemplate newTemplate, User createdBy) {

		if (CollectionUtil.isNotEmpty(oldTemplate.getFields())) {
			List<PrTemplateField> templateFields = new ArrayList<PrTemplateField>();
			for (PrTemplateField tempField : oldTemplate.getFields()) {
				PrTemplateField temp = new PrTemplateField();
				temp.setDefaultValue(tempField.getDefaultValue());
				temp.setFieldName(tempField.getFieldName());
				temp.setOptional(tempField.getOptional());
				temp.setReadOnly(tempField.getReadOnly());
				temp.setTemplate(newTemplate);
				temp.setVisible(tempField.getVisible());
				temp.setBuyer(tempField.getBuyer());
				templateFields.add(temp);
			}
			newTemplate.setFields(templateFields);
		}
		newTemplate.setTemplateFieldBinding(oldTemplate.getTemplateFieldBinding());
		newTemplate.setApprovalVisible(oldTemplate.getApprovalVisible());
		newTemplate.setApprovalReadOnly(oldTemplate.getApprovalReadOnly());
		newTemplate.setApprovalOptional(oldTemplate.getApprovalOptional());
		newTemplate.setLockBudget(oldTemplate.getLockBudget());

		// copy approval from template
		if (CollectionUtil.isNotEmpty(oldTemplate.getApprovals())) {
			List<PrTemplateApproval> approvalList = new ArrayList<PrTemplateApproval>();
			for (PrTemplateApproval templateApproval : oldTemplate.getApprovals()) {
				PrTemplateApproval newTemplateApproval = new PrTemplateApproval();
				newTemplateApproval.setApprovalType(templateApproval.getApprovalType());
				newTemplateApproval.setLevel(templateApproval.getLevel());
				newTemplateApproval.setPrTemplate(newTemplate);
				if (CollectionUtil.isNotEmpty(templateApproval.getApprovalUsers())) {
					List<PrTemplateApprovalUser> tempApprovalList = new ArrayList<PrTemplateApprovalUser>();
					for (PrTemplateApprovalUser templateApprovalUser : templateApproval.getApprovalUsers()) {
						PrTemplateApprovalUser approvalUser = new PrTemplateApprovalUser();
						approvalUser.setApprovalStatus(templateApprovalUser.getApprovalStatus());
						approvalUser.setApproval(newTemplateApproval);
						approvalUser.setRemarks(templateApprovalUser.getRemarks());
						approvalUser.setUser(templateApprovalUser.getUser());
						tempApprovalList.add(approvalUser);
					}
					newTemplateApproval.setApprovalUsers(tempApprovalList);
				}
				approvalList.add(newTemplateApproval);
			}
			newTemplate.setApprovals(approvalList);
		}

		// copy Team Members from template
		List<TemplatePrTeamMembers> teamMembers = new ArrayList<TemplatePrTeamMembers>();
		if (CollectionUtil.isNotEmpty(oldTemplate.getTeamMembers())) {
			for (TemplatePrTeamMembers team : oldTemplate.getTeamMembers()) {
				TemplatePrTeamMembers newTeamMembers = new TemplatePrTeamMembers();
				newTeamMembers.setTeamMemberType(team.getTeamMemberType());
				newTeamMembers.setUser(team.getUser());
				newTeamMembers.setPrTemplate(newTemplate);
				teamMembers.add(newTeamMembers);
			}
			newTemplate.setTeamMembers(teamMembers);
		}

		newTemplate.setCreatedBy(createdBy);
		newTemplate.setCreatedDate(new Date());

		newTemplate = prTemplateDao.saveOrUpdate(newTemplate);
		List<String> assignedUserId = prTemplateService.getTemplateByUserIdAndTemplateId(oldTemplate.getId(), SecurityLibrary.getLoggedInUserTenantId());
		if (CollectionUtil.isNotEmpty(assignedUserId)) {
			for (String assgnedUser : assignedUserId) {
				User user = userService.getUsersForRfxById(assgnedUser);
				List<PrTemplate> assignedTemplateList = user.getAssignedPrTemplates();
				assignedTemplateList.add(newTemplate);
				user.setAssignedPrTemplates(assignedTemplateList);
				userService.updateUser(user);
			}
		}
		LOG.info("=======pr Template============" + newTemplate.getId() + "===========" + newTemplate.getTemplateName());
		return newTemplate;
	}

	@Override
	public List<PrTemplate> findAllPrTemplatesForTenantAndUser(String tenantId) {
		return prTemplateDao.findAllPrTemplatesForTenantAndUser(tenantId);
	}

	@Override
	public List<PrTemplatePojo> findTemplatesForTenantId(String tenantId, TableDataInput tableParams, String userId) {
		return prTemplateDao.findTemplatesForTenantId(tenantId, tableParams, userId);
	}

	@Override
	public List<User> getAllUsers(String tempId) {
		return prTemplateDao.getAllUsers(tempId);
	}

	@Override
	public List<String> getTemplateByUserIdAndTemplateId(String userId, String loggedInUserTenantId) {
		return prTemplateDao.getTemplateByUserIdAndTemplateId(userId, loggedInUserTenantId);
	}

	@Override
	public void deleteusersForTemplate(String prTemplateId) {
		prTemplateDao.deleteusersForTemplate(prTemplateId);
	}

	@Override
	public boolean validateContractItemSetting(String id) {
		return prTemplateDao.validateContractItemSetting(id);
	}

}
