/**
 * 
 */
package com.privasia.procurehere.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.privasia.procurehere.core.dao.PaymentTransactionDao;
import com.privasia.procurehere.core.entity.PaymentTransaction;
import com.privasia.procurehere.core.pojo.TableDataInput;
import com.privasia.procurehere.core.utils.CollectionUtil;
import com.privasia.procurehere.service.PaymentTransactionService;

/**
 * @author Nitin Otageri
 */
@Service
@Transactional(readOnly = true)
public class PaymentTransactionServiceImpl implements PaymentTransactionService {

	@SuppressWarnings("unused")
	private static Logger LOG = Logger.getLogger(PaymentTransactionServiceImpl.class);

	@Autowired
	PaymentTransactionDao paymentTransactionDao;

	@Override
	public List<PaymentTransaction> findPaymentTransactions(TableDataInput tableParams) {
		// List<PaymentTransaction> retList = new ArrayList<PaymentTransaction>();
		List<PaymentTransaction> list = paymentTransactionDao.findPaymentTransactions(tableParams);
		if (CollectionUtil.isNotEmpty(list)) {
			for (PaymentTransaction t : list) {
				// if(t != null && t.getCountry() != null) {
				// t.getCountry().setCreatedBy(null);
				// t.getCountry().setModifiedBy(null);
				// }
				if (t.getBuyerPlan() != null) {
					t.getBuyerPlan().getShortDescription();
					t.getBuyerPlan().setCreatedBy(null);
					t.getBuyerPlan().setModifiedBy(null);
					t.getBuyerPlan().setRangeList(null);
				}
				if (t.getSupplierPlan() != null) {
					t.getSupplierPlan().getShortDescription();
					t.getSupplierPlan().setCreatedBy(null);
					t.getSupplierPlan().setModifiedBy(null);
				}
				t.setPromoCode(null);
			} 
		}
		return list;
	}

	@Override
	@Transactional(readOnly = false)
	public PaymentTransaction save(PaymentTransaction paymentTransaction) {
		return paymentTransactionDao.saveOrUpdate(paymentTransaction);
	}

	@Override
	@Transactional(readOnly = false)
	public PaymentTransaction update(PaymentTransaction paymentTransaction) {
		return paymentTransactionDao.update(paymentTransaction);
	}

	@Override
	public PaymentTransaction getPaymentTransactionById(String id) {
		return paymentTransactionDao.findById(id);
	}

	@Override
	public long findTotalPaymentTransactions() {
		return paymentTransactionDao.findTotalPaymentTransactions();
	}

	@Override
	public long findTotalFilteredPaymentTransactions(TableDataInput tableParams) {
		return paymentTransactionDao.findTotalFilteredPaymentTransactions(tableParams);
	}

	@Override
	public long findTotalPaymentTransactionForBuyer(String buyerId) {
		return paymentTransactionDao.findTotalPaymentTransactionsForBuyer(buyerId);
	}

	@Override
	public long findTotalFilteredPaymentTransactionsForBuyer(String buyerId, TableDataInput tableParams) {
		return paymentTransactionDao.findTotalFilteredPaymentTransactionsForBuyer(buyerId, tableParams);
	}

	@Override
	public List<PaymentTransaction> findPaymentTransactionsForBuyer(String buyerId, TableDataInput tableParams) {
		List<PaymentTransaction> list = paymentTransactionDao.findPaymentTransactionsForBuyer(buyerId, tableParams);
		for (PaymentTransaction t : list) {
			if (t.getBuyerPlan() != null) {
				t.getBuyerPlan().getShortDescription();
				t.getBuyerPlan().setCreatedBy(null);
				t.getBuyerPlan().setModifiedBy(null);
				t.getBuyerPlan().setRangeList(null);
				t.getBuyerPlan().setPlanPeriodList(null);
			}
			if (t.getPromoCode() != null) {
				t.getPromoCode().getPromoCode();
				t.getPromoCode().setCreatedBy(null);
			}
		}
		return list;
	}

	@Override
	public long findTotalSuccessfulPaymentTransactionForBuyer(String buyerId) {
		return paymentTransactionDao.findTotalSuccessfulPaymentTransactionsForBuyer(buyerId);
	}

	@Override
	public long findTotalSuccessfulFilteredPaymentTransactionsForBuyer(String buyerId, TableDataInput tableParams) {
		return paymentTransactionDao.findTotalSuccessfulFilteredPaymentTransactionsForBuyer(buyerId, tableParams);
	}

	@Override
	public List<PaymentTransaction> findSuccessfulPaymentTransactionsForBuyer(String buyerId, TableDataInput tableParams) {
		List<PaymentTransaction> list = paymentTransactionDao.findSuccessfulPaymentTransactionsForBuyer(buyerId, tableParams);
		for (PaymentTransaction t : list) {
			if (t.getBuyerPlan() != null) {
				t.getBuyerPlan().getShortDescription();
				t.getBuyerPlan().setCreatedBy(null);
				t.getBuyerPlan().setModifiedBy(null);
				t.getBuyerPlan().setRangeList(null);
				t.getBuyerPlan().setPlanPeriodList(null);
			}
			if (t.getPromoCode() != null) {
				t.getPromoCode().setCreatedBy(null);
			}
		}
		return list;
	}

	@Override
	public List<PaymentTransaction> findSuccessfulPaymentTransactionsForSupplier(String loggedInUserTenantId, TableDataInput input) {
		List<PaymentTransaction> list = paymentTransactionDao.findSuccessfulPaymentTransactionsForSupplier(loggedInUserTenantId, input);
		for (PaymentTransaction t : list) {
			if (t.getSupplierPlan() != null) {
				t.getSupplierPlan().getShortDescription();
				t.getSupplierPlan().setCreatedBy(null);
				t.getSupplierPlan().setModifiedBy(null);
			}
		}
		return list;
	}

	@Override
	public long findTotalSuccessfulFilteredPaymentTransactionsForSupplier(String loggedInUserTenantId, TableDataInput input) {
		return paymentTransactionDao.findTotalSuccessfulFilteredPaymentTransactionsForSupplier(loggedInUserTenantId, input);
	}

	@Override
	public long findTotalSuccessfulPaymentTransactionForSupplier(String loggedInUserTenantId) {
		return paymentTransactionDao.findTotalSuccessfulPaymentTransactionForSupplier(loggedInUserTenantId);
	}

	@Override
	public PaymentTransaction getPaymentTransactionWithSupplierPlanByPaymentTransactionId(String paymentTransactionId) {
		return paymentTransactionDao.getPaymentTransactionWithSupplierPlanByPaymentTransactionId(paymentTransactionId);
	}

	@Override
	public PaymentTransaction getPaymentTransactionWithBuyerPlanById(String paymentTransactionId) {
		return paymentTransactionDao.getPaymentTransactionWithBuyerPlanById(paymentTransactionId);
	}

}
