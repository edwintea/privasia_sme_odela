package com.privasia.procurehere.service.impl;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.swing.ImageIcon;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.privasia.procurehere.core.dao.DeliveryOrderAuditDao;
import com.privasia.procurehere.core.dao.DeliveryOrderDao;
import com.privasia.procurehere.core.dao.DeliveryOrderItemDao;
import com.privasia.procurehere.core.dao.PoDao;
import com.privasia.procurehere.core.dao.PoItemDao;
import com.privasia.procurehere.core.dao.SupplierSettingsDao;
import com.privasia.procurehere.core.entity.BusinessUnit;
import com.privasia.procurehere.core.entity.DeliveryOrder;
import com.privasia.procurehere.core.entity.DeliveryOrderAudit;
import com.privasia.procurehere.core.entity.DeliveryOrderItem;
import com.privasia.procurehere.core.entity.Po;
import com.privasia.procurehere.core.entity.PoItem;
import com.privasia.procurehere.core.entity.Supplier;
import com.privasia.procurehere.core.entity.SupplierNotificationMessage;
import com.privasia.procurehere.core.entity.SupplierSettings;
import com.privasia.procurehere.core.entity.User;
import com.privasia.procurehere.core.enums.DeliveryOrderAuditType;
import com.privasia.procurehere.core.enums.DoAuditVisibilityType;
import com.privasia.procurehere.core.enums.DoStatus;
import com.privasia.procurehere.core.enums.NotificationType;
import com.privasia.procurehere.core.exceptions.ApplicationException;
import com.privasia.procurehere.core.exceptions.EmailException;
import com.privasia.procurehere.core.pojo.DoSupplierPojo;
import com.privasia.procurehere.core.pojo.TableDataInput;
import com.privasia.procurehere.core.utils.CollectionUtil;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.core.utils.SecurityLibrary;
import com.privasia.procurehere.core.utils.StringUtils;
import com.privasia.procurehere.service.DashboardNotificationService;
import com.privasia.procurehere.service.DeliveryOrderService;
import com.privasia.procurehere.service.EventIdSettingsService;
import com.privasia.procurehere.service.NotificationService;
import com.privasia.procurehere.service.SupplierSettingsService;
import com.privasia.procurehere.web.controller.DeliveryItemsSummaryPojo;
import com.privasia.procurehere.web.controller.DeliveryOrderSummaryPojo;

import freemarker.template.Configuration;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
@Transactional(readOnly = true)
public class DeliveryOrderServiceImpl implements DeliveryOrderService {

	private static final Logger LOG = Logger.getLogger(Global.DO_LOG);

	@Value("${app.url}")
	String APP_URL;

	@Autowired
	DeliveryOrderDao deliveryOrderDao;

	@Autowired
	MessageSource messageSource;

	@Autowired
	EventIdSettingsService eventIdSettingsService;

	@Autowired
	PoDao poDao;

	@Autowired
	PoItemDao poItemDao;

	@Autowired
	DeliveryOrderAuditDao deliveryOrderAuditDao;

	@Autowired
	DeliveryOrderItemDao deliveryOrderItemDao;

	@Autowired
	ApplicationContext applicationContext;

	@Autowired
	SupplierSettingsDao supplierSettingsDao;

	@Autowired
	SupplierSettingsService supplierSettingsService;

	@Autowired
	Configuration freemarkerConfiguration;

	@Autowired
	NotificationService notificationService;

	@Autowired
	DashboardNotificationService dashboardNotificationService;

	@Override
	public List<DoSupplierPojo> findAllSearchFilterDoForSupplier(String tenantId, TableDataInput input, Date startDate, Date endDate) {
		return deliveryOrderDao.findAllSearchFilterDoForSupplier(tenantId, input, startDate, endDate);
	}

	@Override
	public long findTotalSearchFilterDoForSupplier(String tenantId, TableDataInput input, Date startDate, Date endDate) {
		return deliveryOrderDao.findTotalSearchFilterDoForSupplier(tenantId, input, startDate, endDate);

	}

	@Override
	public long findTotalDoForSupplier(String tenantId) {
		return deliveryOrderDao.findTotalDoForSupplier(tenantId);
	}

	@Override
	@Transactional(readOnly = false, noRollbackFor = { EmailException.class })
	public DeliveryOrder createDo(User loggedInUser, Po po) throws ApplicationException, EmailException {
		DeliveryOrder deliveryOrder = null;
		if (po != null) {
			po = poDao.findById(po.getId());

			deliveryOrder = new DeliveryOrder();
			deliveryOrder.setDeliveryId(eventIdSettingsService.generateEventIdByBusinessUnit(loggedInUser.getTenantId(), "DO", null));
			deliveryOrder.setBuyer(po.getBuyer());
			deliveryOrder.setBusinessUnit(po.getBusinessUnit());

			if (StringUtils.checkString(po.getLine1()).length() > 0) {
				deliveryOrder.setLine1(po.getLine1());
				deliveryOrder.setLine2(po.getLine2());
				deliveryOrder.setLine3(po.getLine3());
				deliveryOrder.setLine4(po.getLine4());
				deliveryOrder.setLine5(po.getLine5());
				deliveryOrder.setLine6(po.getLine6());
				deliveryOrder.setLine7(po.getLine7());

			} else {
				deliveryOrder.setLine1(StringUtils.checkString(po.getLine1()).length() > 0 ? po.getLine1() : po.getBusinessUnit().getLine1());
				deliveryOrder.setLine2(StringUtils.checkString(po.getLine2()).length() > 0 ? po.getLine2() : po.getBusinessUnit().getLine2());
				deliveryOrder.setLine3(StringUtils.checkString(po.getLine3()).length() > 0 ? po.getLine3() : po.getBusinessUnit().getLine3());
				deliveryOrder.setLine4(StringUtils.checkString(po.getLine4()).length() > 0 ? po.getLine4() : po.getBusinessUnit().getLine4());
				deliveryOrder.setLine5(StringUtils.checkString(po.getLine5()).length() > 0 ? po.getLine5() : po.getBusinessUnit().getLine5());
				deliveryOrder.setLine6(StringUtils.checkString(po.getLine6()).length() > 0 ? po.getLine6() : po.getBusinessUnit().getLine6());
				deliveryOrder.setLine7(StringUtils.checkString(po.getLine7()).length() > 0 ? po.getLine7() : po.getBusinessUnit().getLine7());
			}

			deliveryOrder.setCorrespondenceAddress(po.getCorrespondenceAddress());
			if (po.getCorrespondenceAddress() != null) {
				deliveryOrder.setCorrespondAddressTitle(StringUtils.checkString(po.getCorrespondenceAddress().getTitle()));
				deliveryOrder.setCorrespondAddressLine1(StringUtils.checkString(po.getCorrespondenceAddress().getLine1()));
				deliveryOrder.setCorrespondAddressLine2(StringUtils.checkString(po.getCorrespondenceAddress().getLine2()));
				deliveryOrder.setCorrespondAddressZip(StringUtils.checkString(po.getCorrespondenceAddress().getZip()));
				deliveryOrder.setCorrespondAddressCity(StringUtils.checkString(po.getCorrespondenceAddress().getCity()));
				deliveryOrder.setCorrespondAddressState(StringUtils.checkString(po.getCorrespondenceAddress().getState().getStateName()));
				deliveryOrder.setCorrespondAddressCountry(StringUtils.checkString(po.getCorrespondenceAddress().getState().getCountry().getCountryName()));
			}
			if (po.getDeliveryAddress() != null) {
				deliveryOrder.setDeliveryAddressTitle(StringUtils.checkString(po.getDeliveryAddress().getTitle()));
				deliveryOrder.setDeliveryAddressLine1(StringUtils.checkString(po.getDeliveryAddress().getLine1()));
				deliveryOrder.setDeliveryAddressLine2(StringUtils.checkString(po.getDeliveryAddress().getLine2()));
				deliveryOrder.setDeliveryAddressZip(StringUtils.checkString(po.getDeliveryAddress().getZip()));
				deliveryOrder.setDeliveryAddressCity(StringUtils.checkString(po.getDeliveryAddress().getCity()));
				deliveryOrder.setDeliveryAddressState(StringUtils.checkString(po.getDeliveryAddress().getState().getStateName()));
				deliveryOrder.setDeliveryAddressCountry(StringUtils.checkString(po.getDeliveryAddress().getState().getCountry().getCountryName()));
			}
			deliveryOrder.setCostCenter(po.getCostCenter());
			deliveryOrder.setCreatedBy(loggedInUser);
			deliveryOrder.setCreatedDate(new Date());
			deliveryOrder.setCurrency(po.getCurrency());
			deliveryOrder.setDecimal(po.getDecimal());
			deliveryOrder.setDeliveryAddress(po.getDeliveryAddress());
			deliveryOrder.setDeliveryDate(po.getDeliveryDate());
			deliveryOrder.setDeliveryReceiver(po.getDeliveryReceiver());
			deliveryOrder.setDescription(po.getDescription());
			deliveryOrder.setGrandTotal(po.getGrandTotal());
			deliveryOrder.setName(po.getName());
			// deliveryOrder.setPaymentTerm(po.getPaymentTerm());
			deliveryOrder.setReferenceNumber(po.getReferenceNumber());
			deliveryOrder.setRemarks(po.getRemarks());
			deliveryOrder.setRequester(po.getRequester());
			deliveryOrder.setStatus(DoStatus.DRAFT);

			deliveryOrder.setSupplierName(po.getSupplierName());
			deliveryOrder.setSupplierAddress(po.getSupplierAddress());
			deliveryOrder.setSupplierTelNumber(po.getSupplierTelNumber());
			deliveryOrder.setSupplierAddress(po.getSupplierAddress());
			deliveryOrder.setSupplierTaxNumber(po.getSupplierTaxNumber());
			deliveryOrder.setSupplierFaxNumber(po.getSupplierFaxNumber());

			if (po.getSupplier() != null) {
				if (po.getSupplier().getSupplier() != null) {
					deliveryOrder.setSupplier(po.getSupplier().getSupplier());
					po.getSupplier().getSupplier().getCompanyName();
				}
			}

			deliveryOrder.setTaxDescription(po.getTaxDescription());
			deliveryOrder.setTermsAndConditions(po.getTermsAndConditions());
			deliveryOrder.setTotal(po.getTotal());
			if (po.getAdditionalTax() != null) {
				deliveryOrder.setAdditionalTax(po.getAdditionalTax());
			}
			deliveryOrder.setGrandTotal(po.getGrandTotal());
			deliveryOrder.setPo(po);

			List<DeliveryOrderItem> deliveryOrderItems = new ArrayList<DeliveryOrderItem>();
			List<PoItem> itemList = poItemDao.getAllPoItemByPoId(po.getId());
			if (CollectionUtil.isNotEmpty(itemList)) {
				for (PoItem prItem : itemList) {
					if (prItem.getParent() == null) {
						// LOG.info("pr parent");
						DeliveryOrderItem parent = new DeliveryOrderItem();
						parent.setItemName(prItem.getItemName());
						parent.setLevel(prItem.getLevel());
						parent.setOrder(prItem.getOrder());
						parent.setDeliveryReceiver(po.getDeliveryReceiver());
						parent.setBuyer(prItem.getBuyer());
						parent.setDeliverOrder(deliveryOrder);
						parent.setItemDescription(prItem.getItemDescription());
						List<DeliveryOrderItem> childrenList = new ArrayList<DeliveryOrderItem>();
						if (CollectionUtil.isNotEmpty(prItem.getChildren())) {
							for (PoItem itemPojo : prItem.getChildren()) {
								LOG.info("Children not empty > " + prItem.getChildren().size());
								DeliveryOrderItem item = new DeliveryOrderItem();
								item.setBusinessUnit(po.getBusinessUnit());
								item.setBuyer(itemPojo.getBuyer());
								item.setCostCenter(po.getCostCenter());
								item.setDeliveryAddress(po.getDeliveryAddress());
								if (po.getDeliveryAddress() != null) {
									item.setDeliveryAddressTitle(StringUtils.checkString(po.getDeliveryAddress().getTitle()));
									item.setDeliveryAddressLine1(StringUtils.checkString(po.getDeliveryAddress().getLine1()));
									item.setDeliveryAddressLine2(StringUtils.checkString(po.getDeliveryAddress().getLine2()));
									item.setDeliveryAddressZip(StringUtils.checkString(po.getDeliveryAddress().getZip()));
									item.setDeliveryAddressCity(StringUtils.checkString(po.getDeliveryAddress().getCity()));
									item.setDeliveryAddressState(StringUtils.checkString(po.getDeliveryAddress().getState().getStateName()));
									item.setDeliveryAddressCountry(StringUtils.checkString(po.getDeliveryAddress().getState().getCountry().getCountryName()));
								}
								item.setDeliveryDate(po.getDeliveryDate());
								item.setDeliveryReceiver(po.getDeliveryReceiver());
								item.setFreeTextItemEntered(itemPojo.getFreeTextItemEntered());
								item.setItemDescription(itemPojo.getItemDescription());
								item.setItemName(itemPojo.getItemName());
								item.setItemTax(new BigDecimal(itemPojo.getItemTax()).setScale(Integer.parseInt(po.getDecimal()), RoundingMode.HALF_DOWN));
								item.setLevel(itemPojo.getLevel());
								item.setOrder(itemPojo.getOrder());
								item.setParent(parent);
								item.setDeliverOrder(deliveryOrder);
								item.setQuantity(itemPojo.getQuantity());
								item.setTaxAmount(itemPojo.getTaxAmount());
								item.setTotalAmount(itemPojo.getTotalAmount());
								item.setTotalAmountWithTax(itemPojo.getTotalAmountWithTax());
								item.setUnit(itemPojo.getUnit());
								item.setUnitPrice(itemPojo.getUnitPrice());
								if (itemPojo.getPo() != null) {
									item.setPo(itemPojo.getPo());
									item.setPoItem(itemPojo);
								}
								childrenList.add(item);
							}
							parent.setChildren(childrenList);
							deliveryOrderItems.add(parent);
						}
					}

				}
			} else {
				throw new ApplicationException("Missing PO Items");
			}

			if (CollectionUtil.isNotEmpty(deliveryOrderItems)) {
				deliveryOrder.setDeliveryOrderItems(deliveryOrderItems);

				deliveryOrder = deliveryOrderDao.saveOrUpdate(deliveryOrder);

				// Update DO count in PO
				po.setDoCount((int) deliveryOrderDao.findTotalDoForPo(po.getId()));
				poDao.update(po);

				DeliveryOrderAudit audit = new DeliveryOrderAudit();
				audit.setAction(DeliveryOrderAuditType.CREATE);
				audit.setActionBy(loggedInUser);
				audit.setActionDate(new Date());
				audit.setBuyer(po.getBuyer());
				Supplier supplier = new Supplier();
				supplier.setId(loggedInUser.getTenantId());
				audit.setSupplier(supplier);
				audit.setVisibilityType(DoAuditVisibilityType.SUPPLIER);
				audit.setDescription(messageSource.getMessage("do.audit.ready", new Object[] { po.getPoNumber() }, Global.LOCALE));
				audit.setDeliveryOrder(deliveryOrder);
				deliveryOrderAuditDao.save(audit);

			} else {
				LOG.warn("NO Items found ..");
			}

		}
		return deliveryOrder;
	}

	@Override
	public DeliveryOrder getDoByIdForSupplierView(String doId) {
		DeliveryOrder deliveryOrder = deliveryOrderDao.findByDoId(doId);
		if (deliveryOrder != null) {
			if (deliveryOrder.getBuyer() != null) {
				deliveryOrder.getBuyer().getCompanyName();
			}
			if (deliveryOrder.getSupplier() != null) {
				deliveryOrder.getSupplier().getCompanyName();
				deliveryOrder.getSupplier().getFaxNumber();
			}
			if (deliveryOrder.getCreatedBy() != null) {
				deliveryOrder.getCreatedBy().getName();
				deliveryOrder.getCreatedBy().getCommunicationEmail();
				deliveryOrder.getCreatedBy().getPhoneNumber();
			}
			if (deliveryOrder.getPo() != null) {
				deliveryOrder.getPo().getPoNumber();
			}
			if (deliveryOrder.getCurrency() != null) {
				deliveryOrder.getCurrency().getCurrencyCode();
			}
			if (deliveryOrder.getBusinessUnit() != null) {
				deliveryOrder.getBusinessUnit().getDisplayName();
			}

		}
		return deliveryOrder;
	}

	@Override
	public List<DeliveryOrderItem> findAllDoItemByDoIdForSummary(String doId) {
		return deliveryOrderItemDao.getAllDoItemByDoId(doId);
	}

	@Override
	public List<DoSupplierPojo> findAllSearchFilterDoForBuyer(String tenantId, TableDataInput input, Date startDate, Date endDate) {
		return deliveryOrderDao.findAllSearchFilterDoForBuyer(tenantId, input, startDate, endDate);
	}

	@Override
	public long findTotalSearchFilterDoForBuyer(String tenantId, TableDataInput input, Date startDate, Date endDate) {
		return deliveryOrderDao.findTotalSearchFilterDoForBuyer(tenantId, input, startDate, endDate);

	}

	@Override
	public long findTotalDoForBuyer(String tenantId) {
		return deliveryOrderDao.findTotalDoForBuyer(tenantId);
	}

	@Override
	@Transactional(readOnly = false)
	public DeliveryOrder finishDeliverOrder(String doId, User loggedInUser) {

		DeliveryOrder deliveryOrder = deliveryOrderDao.findByDoId(doId);

		deliveryOrder.setStatus(DoStatus.DELIVERED);
		deliveryOrderDao.update(deliveryOrder);

		try {
			DeliveryOrderAudit audit = new DeliveryOrderAudit();
			audit.setAction(DeliveryOrderAuditType.DELIVERED);
			audit.setActionBy(SecurityLibrary.getLoggedInUser());
			audit.setActionDate(new Date());
			audit.setBuyer(deliveryOrder.getBuyer());
			Supplier supplier = new Supplier();
			supplier.setId(loggedInUser.getTenantId());
			audit.setSupplier(supplier);
			audit.setDescription(messageSource.getMessage("do.finish.notification.message", new Object[] { deliveryOrder.getDeliveryId() }, Global.LOCALE));
			audit.setDeliveryOrder(deliveryOrder);
			audit.setVisibilityType(DoAuditVisibilityType.BOTH);
			deliveryOrderAuditDao.save(audit);
		} catch (Exception e) {
			LOG.error("Error while Finihing Delivery order : " + e.getMessage(), e);
		}

		return deliveryOrder;
	}

	@Override
	@Transactional(readOnly = false)
	public DeliveryOrder declineOrder(String doId, User loggedInUser, String buyerRemark) throws EmailException {

		DeliveryOrder deliveryOrder = deliveryOrderDao.findByDoId(doId);

		if (deliveryOrder != null) {
			deliveryOrder.setStatus(DoStatus.DECLINED);
			deliveryOrder.setActionBy(loggedInUser);
			deliveryOrder.setActionDate(new Date());
			deliveryOrderDao.update(deliveryOrder);

			DeliveryOrderAudit buyerAudit = new DeliveryOrderAudit();
			buyerAudit.setAction(DeliveryOrderAuditType.DECLINED);
			buyerAudit.setActionBy(loggedInUser);
			buyerAudit.setActionDate(new Date());
			buyerAudit.setBuyer(deliveryOrder.getBuyer());
			buyerAudit.setSupplier(deliveryOrder.getSupplier());
			buyerAudit.setDescription(messageSource.getMessage("do.declined", new Object[] { buyerRemark }, Global.LOCALE));
			buyerAudit.setVisibilityType(DoAuditVisibilityType.BOTH);
			buyerAudit.setDeliveryOrder(deliveryOrder);
			deliveryOrderAuditDao.save(buyerAudit);

			try {
				sendEmailNotificationToSupplier(deliveryOrder, loggedInUser, false, buyerRemark);
			} catch (Exception e) {
				LOG.error("Error while sending do decline mail to supplier " + deliveryOrder.getSupplierName() + " :  " + e.getMessage(), e);
				throw new EmailException("Error while sending do decline mail to supplier");
			}

		}

		return deliveryOrder;
	}

	@Override
	@Transactional(readOnly = false)
	public DeliveryOrder acceptOrder(String doId, User loggedInUser, String buyerRemark) throws EmailException {

		DeliveryOrder deliveryOrder = deliveryOrderDao.findByDoId(doId);

		if (deliveryOrder != null) {
			deliveryOrder.setStatus(DoStatus.ACCEPTED);
			deliveryOrder.setActionBy(loggedInUser);
			deliveryOrder.setActionDate(new Date());
			deliveryOrderDao.update(deliveryOrder);

			DeliveryOrderAudit buyerAudit = new DeliveryOrderAudit();
			buyerAudit.setAction(DeliveryOrderAuditType.ACCEPTED);
			buyerAudit.setActionBy(loggedInUser);
			buyerAudit.setActionDate(new Date());
			buyerAudit.setBuyer(deliveryOrder.getBuyer());
			buyerAudit.setSupplier(deliveryOrder.getSupplier());
			buyerAudit.setDescription(messageSource.getMessage("do.buyerAudit.accepted", new Object[] { buyerRemark }, Global.LOCALE));
			buyerAudit.setVisibilityType(DoAuditVisibilityType.BOTH);
			buyerAudit.setDeliveryOrder(deliveryOrder);
			deliveryOrderAuditDao.save(buyerAudit);

			try {
				sendEmailNotificationToSupplier(deliveryOrder, loggedInUser, true, buyerRemark);

			} catch (Exception e) {
				LOG.error("Error while sending do accept mail to supplier " + deliveryOrder.getSupplierName() + " :  " + e.getMessage(), e);
				throw new EmailException("Error while sending do accept mail to supplier");
			}

		}

		return deliveryOrder;
	}

	@Override
	@Transactional(readOnly = false, noRollbackFor = { EmailException.class })
	public DeliveryOrder cancelDo(String doId, User loggedInUser, String supplierRemark) throws EmailException {

		DeliveryOrder deliveryOrder = deliveryOrderDao.findByDoId(doId);

		if (deliveryOrder != null) {
			DoStatus status = deliveryOrder.getStatus();
			deliveryOrder.setStatus(DoStatus.CANCELLED);
			deliveryOrderDao.update(deliveryOrder);

			Po po = deliveryOrder.getPo();
			if (po.getDoCancelCount() == null) {
				po.setDoCancelCount(1);
			} else {
				po.setDoCancelCount(po.getDoCancelCount() + 1);
			}
			poDao.saveOrUpdate(po);

			DeliveryOrderAudit buyerAudit = new DeliveryOrderAudit();
			buyerAudit.setAction(DeliveryOrderAuditType.CANCELLED);
			buyerAudit.setActionBy(loggedInUser);
			buyerAudit.setActionDate(new Date());
			buyerAudit.setBuyer(deliveryOrder.getBuyer());
			buyerAudit.setSupplier(deliveryOrder.getSupplier());
			buyerAudit.setDescription(messageSource.getMessage("do.audit.cancel", new Object[] { supplierRemark }, Global.LOCALE));
			if (DoStatus.DELIVERED == status) {
				buyerAudit.setVisibilityType(DoAuditVisibilityType.BOTH);
			} else {
				buyerAudit.setVisibilityType(DoAuditVisibilityType.SUPPLIER);
			}
			buyerAudit.setDeliveryOrder(deliveryOrder);
			deliveryOrderAuditDao.save(buyerAudit);
			// if (DoStatus.DELIVERED == status) {
			// try {
			// sendEmailNotificationToBuyer(deliveryOrder, loggedInUser, false, supplierRemark);
			// } catch (Exception e) {
			// throw new EmailException("Error while sending cancel email to buyer");
			// }
			// }
		}

		return deliveryOrder;
	}

	@Override
	public DeliveryOrder findByDoId(String doId) {
		return deliveryOrderDao.findById(doId);
	}

	@Override
	public JasperPrint getGeneratedBuyerDoPdf(DeliveryOrder deliveryOrder, String strTimeZone) {
		deliveryOrder = deliveryOrderDao.findById(deliveryOrder.getId());
		JasperPrint jasperPrint = null;
		Map<String, Object> parameters = new HashMap<String, Object>();
		TimeZone timeZone = TimeZone.getDefault();

		if (strTimeZone != null) {
			timeZone = TimeZone.getTimeZone(strTimeZone);
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm a");
		sdf.setTimeZone(timeZone);

		DecimalFormat df = null;
		if (deliveryOrder.getDecimal().equals("1")) {
			df = new DecimalFormat("#,###,###,##0.0");
		} else if (deliveryOrder.getDecimal().equals("2")) {
			df = new DecimalFormat("#,###,###,##0.00");
		} else if (deliveryOrder.getDecimal().equals("3")) {
			df = new DecimalFormat("#,###,###,##0.000");
		} else if (deliveryOrder.getDecimal().equals("4")) {
			df = new DecimalFormat("#,###,###,##0.0000");
		} else if (deliveryOrder.getDecimal().equals("5")) {
			df = new DecimalFormat("#,###,###,##0.00000");
		} else if (deliveryOrder.getDecimal().equals("6")) {
			df = new DecimalFormat("#,###,###,##0.000000");
		} else {
			df = new DecimalFormat("#,###,###,##0.00");
		}

		try {
			Resource resource = applicationContext.getResource("classpath:reports/BuyerDoReport.jasper");
			File jasperfile = resource.getFile();

			DeliveryOrderSummaryPojo summary = new DeliveryOrderSummaryPojo();
			String createDate = deliveryOrder.getDoSendDate() != null ? sdf.format(deliveryOrder.getDoSendDate()).toUpperCase() : "";
			String deliveryDate = deliveryOrder.getDeliveryDate() != null ? sdf.format(deliveryOrder.getDeliveryDate()).toUpperCase() : "";

			summary.setDoName(deliveryOrder.getName());
			summary.setDeliveryId(deliveryOrder.getDeliveryId());
			summary.setPaymentNote("Hello");
			summary.setPoNumber(deliveryOrder.getPo().getPoNumber());
			summary.setCreatedDate(createDate);
			if (deliveryOrder.getFooter() != null) {
				summary.setFooter(deliveryOrder.getFooter().getContent());
			}

			SupplierSettings supplierSettings = supplierSettingsDao.getSupplierSettingsByTenantId(deliveryOrder.getSupplier().getId());

			if (supplierSettings != null) {
				ImageIcon n;
				if (supplierSettings.getFileAttatchment() != null) {
					n = new ImageIcon(supplierSettings.getFileAttatchment());
					summary.setComanyName(null);
				} else {
					n = new ImageIcon();
					summary.setComanyName(deliveryOrder.getSupplier().getCompanyName());
				}
				summary.setLogo(n.getImage());
			}
			getSummaryOfAddressAndDoitems(deliveryOrder, df, summary, deliveryDate);

			BusinessUnit bUnit = deliveryOrder.getBusinessUnit();
			String buyerAddress = "";

			if (bUnit != null) {
				summary.setDisplayName(bUnit.getUnitName());
			}
			if (StringUtils.checkString(deliveryOrder.getLine1()).length() > 0) {
				buyerAddress = deliveryOrder.getLine1() + "\r\n";
			}
			if (StringUtils.checkString(deliveryOrder.getLine2()).length() > 0) {
				buyerAddress += deliveryOrder.getLine2() + "\r\n";
			}
			if (StringUtils.checkString(deliveryOrder.getLine3()).length() > 0) {
				buyerAddress += deliveryOrder.getLine3() + "\r\n";
			}
			if (StringUtils.checkString(deliveryOrder.getLine4()).length() > 0) {
				buyerAddress += deliveryOrder.getLine4() + "\r\n";
			}
			if (StringUtils.checkString(deliveryOrder.getLine5()).length() > 0) {
				buyerAddress += deliveryOrder.getLine5() + "\r\n";
			}
			if (StringUtils.checkString(deliveryOrder.getLine6()).length() > 0) {
				buyerAddress += deliveryOrder.getLine6() + "\r\n";
			}
			if (StringUtils.checkString(deliveryOrder.getLine7()).length() > 0) {
				buyerAddress += deliveryOrder.getLine7() + "\r\n";
			}
			summary.setBuyerAddress(buyerAddress);

			List<DeliveryOrderSummaryPojo> deliveryOrderSummary = Arrays.asList(summary);

			// Supplier Address
			String supplierAddress = "";

			if (deliveryOrder.getSupplier() != null) {
				Supplier supplier = deliveryOrder.getSupplier();
				supplierAddress += supplier.getCompanyName() + "\r\n";
				supplierAddress += supplier.getLine1();
				if (StringUtils.checkString(deliveryOrder.getSupplier().getLine2()).length() > 0) {
					supplierAddress += "\r\n" + supplier.getLine2();
				}
				supplierAddress += "\r\n" + supplier.getCity() + ", ";
				if (supplier.getState() != null) {
					supplierAddress += supplier.getState().getStateName() + "\r\n\n";
				}
				supplierAddress += "TEL : ";

				if (supplier.getCompanyContactNumber() != null) {
					supplierAddress += supplier.getCompanyContactNumber();
				}
				supplierAddress += "\r\nFAX : ";
				if (supplier.getFaxNumber() != null) {
					supplierAddress += supplier.getFaxNumber() + "\r\n\n";
				}
				supplierAddress += "Company Reg No : ";

				if (supplier.getCompanyRegistrationNumber() != null) {
					supplierAddress += supplier.getCompanyRegistrationNumber();
				}
				supplierAddress += "\r\nTax Reg No : ";
				if (supplier.getTaxRegistrationNumber() != null) {
					supplierAddress += supplier.getTaxRegistrationNumber();
				}

			} else {
				supplierAddress += deliveryOrder.getSupplierName() + "\r\n";
				supplierAddress += deliveryOrder.getSupplierAddress() + "\r\n\n";
				supplierAddress += "TEL :";
				if (deliveryOrder.getSupplierTelNumber() != null) {
					supplierAddress += deliveryOrder.getSupplierTelNumber();
				}
				supplierAddress += "\r\nFAX : ";
				if (deliveryOrder.getSupplierFaxNumber() != null) {
					supplierAddress += deliveryOrder.getSupplierFaxNumber() + "\r\n\n";
				}

				supplierAddress += "Tax Reg No : ";
				if (deliveryOrder.getSupplierTaxNumber() != null) {
					supplierAddress += deliveryOrder.getSupplierTaxNumber();
				}
			}

			if (deliveryOrder.getSupplier() != null) {
				summary.setSupplierName(deliveryOrder.getSupplier() != null ? deliveryOrder.getSupplier().getCompanyName() : "");
			} else {
				summary.setSupplierName(deliveryOrder.getSupplierName());
			}
			summary.setSupplierAddress(supplierAddress);
			summary.setTaxnumber(deliveryOrder.getSupplierTaxNumber() != null ? deliveryOrder.getSupplierTaxNumber() : "");

			parameters.put("DELIVERYORDER_SUMMARY", deliveryOrderSummary);
			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(deliveryOrderSummary, false);
			jasperPrint = JasperFillManager.fillReport(jasperfile.getPath(), parameters, beanCollectionDataSource);
		} catch (

		Exception e) {
			LOG.error("Could not generate Do Report. " + e.getMessage(), e);
		}
		return jasperPrint;
	}

	private void getSummaryOfAddressAndDoitems(DeliveryOrder deliveryOrder, DecimalFormat df, DeliveryOrderSummaryPojo summary, String deliveryDate) {

		try {

			// Delivery Address

			String deliveryAddress = "";
			if (StringUtils.checkString(deliveryOrder.getDeliveryAddressTitle()).length() > 0) {
				deliveryAddress += deliveryOrder.getDeliveryAddressTitle() + "\n";

			}
			deliveryAddress += deliveryOrder.getDeliveryAddressLine1();
			if (deliveryOrder.getDeliveryAddressLine2() != null) {
				deliveryAddress += "\n" + deliveryOrder.getDeliveryAddressLine2();
			}
			deliveryAddress += "\n" + deliveryOrder.getDeliveryAddressZip() + ", " + deliveryOrder.getDeliveryAddressCity() + "\n";
			deliveryAddress += deliveryOrder.getDeliveryAddressState() + ", " + deliveryOrder.getDeliveryAddressCountry();

			summary.setDeliveryAddress(deliveryAddress);
			if (StringUtils.checkString(deliveryOrder.getDeliveryReceiver()).length() > 0) {
				summary.setDeliveryReceiver(deliveryOrder.getDeliveryReceiver());
			}
			summary.setDeliveryDate(deliveryDate);
			// deliveryOrder items List
			List<DeliveryItemsSummaryPojo> doItemList = new ArrayList<DeliveryItemsSummaryPojo>();
			List<DeliveryOrderItem> doList = findAllDoItemByDoId(deliveryOrder.getId());
			if (CollectionUtil.isNotEmpty(doList)) {
				for (DeliveryOrderItem item : doList) {
					DeliveryItemsSummaryPojo dos = new DeliveryItemsSummaryPojo();
					dos.setSlno(item.getLevel() + "." + item.getOrder());
					dos.setItemName(item.getItemName());
					dos.setCurrency(item.getDeliverOrder().getCurrency().getCurrencyCode());
					dos.setItemDescription(item.getItemDescription());
					dos.setAdditionalTax(df.format(item.getDeliverOrder().getAdditionalTax()));
					dos.setGrandTotal(df.format(item.getDeliverOrder().getGrandTotal()));
					dos.setSumAmount(df.format(deliveryOrder.getTotal()));
					dos.setTaxDescription(item.getDeliverOrder().getTaxDescription());
					dos.setDecimal(deliveryOrder.getDecimal());
					doItemList.add(dos);
					if (item.getChildren() != null) {
						for (DeliveryOrderItem childItem : item.getChildren()) {
							DeliveryItemsSummaryPojo childPr = new DeliveryItemsSummaryPojo();
							childPr.setSlno(childItem.getLevel() + "." + childItem.getOrder());
							childPr.setItemName(childItem.getItemName());
							childPr.setItemDescription(childItem.getItemDescription());
							childPr.setQuantity(childItem.getQuantity());
							childPr.setUnitPrice(df.format(childItem.getUnitPrice()));
							childPr.setTaxAmount(df.format(childItem.getTaxAmount()));
							childPr.setTotalAmount(df.format(childItem.getTotalAmount()));
							childPr.setTotalAmountWithTax(df.format(childItem.getTotalAmountWithTax()));
							childPr.setUom(childItem.getUnit() != null ? (childItem.getUnit().getUom() != null ? childItem.getUnit().getUom() : "") : (childItem.getUnit() != null ? childItem.getUnit().getUom() : ""));
							childPr.setCurrency(childItem.getDeliverOrder().getCurrency().getCurrencyCode());
							childPr.setAdditionalTax(df.format(childItem.getDeliverOrder().getAdditionalTax()));
							childPr.setGrandTotal(df.format(childItem.getDeliverOrder().getGrandTotal()));
							childPr.setSumAmount(df.format(deliveryOrder.getTotal()));
							childPr.setTaxDescription(StringUtils.checkString(childItem.getDeliverOrder().getTaxDescription()).length() > 0 ? childItem.getDeliverOrder().getTaxDescription() : "");
							childPr.setSumTaxAmount(childItem.getTaxAmount());
							childPr.setSumTotalAmt(childItem.getTotalAmount());
							childPr.setDecimal(deliveryOrder.getDecimal());
							doItemList.add(childPr);
						}
					}

				}
			}
			summary.setDoItems(doItemList);
		} catch (Exception e) {
			LOG.error("Could not Get Do Items and Address " + e.getMessage(), e);
		}
	}

	@Override
	public List<DeliveryOrderItem> findAllDoItemByDoId(String doId) {
		List<DeliveryOrderItem> returnList = new ArrayList<DeliveryOrderItem>();
		List<DeliveryOrderItem> list = deliveryOrderItemDao.getAllDoItemByDoId(doId);
		if (CollectionUtil.isNotEmpty(list)) {
			for (DeliveryOrderItem item : list) {
				DeliveryOrderItem parent = item.createShallowCopy();
				if (item.getParent() == null) {
					returnList.add(parent);
				}

				if (CollectionUtil.isNotEmpty(item.getChildren())) {
					for (DeliveryOrderItem child : item.getChildren()) {
						if (parent.getChildren() == null) {
							parent.setChildren(new ArrayList<DeliveryOrderItem>());
						}
						if (child.getUnit() != null) {
							child.getUnit().getUom();
						}

						parent.getChildren().add(child.createShallowCopy());
					}
				}
			}
		}
		return returnList;

	}

	@Override
	public JasperPrint getGeneratedSupplierDoPdf(DeliveryOrder deliveryOrder, String strTimeZone) {
		deliveryOrder = deliveryOrderDao.findById(deliveryOrder.getId());
		JasperPrint jasperPrint = null;
		Map<String, Object> parameters = new HashMap<String, Object>();
		TimeZone timeZone = TimeZone.getDefault();

		if (strTimeZone != null) {
			timeZone = TimeZone.getTimeZone(strTimeZone);
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm a");
		sdf.setTimeZone(timeZone);

		DecimalFormat df = null;
		if (deliveryOrder.getDecimal().equals("1")) {
			df = new DecimalFormat("#,###,###,##0.0");
		} else if (deliveryOrder.getDecimal().equals("2")) {
			df = new DecimalFormat("#,###,###,##0.00");
		} else if (deliveryOrder.getDecimal().equals("3")) {
			df = new DecimalFormat("#,###,###,##0.000");
		} else if (deliveryOrder.getDecimal().equals("4")) {
			df = new DecimalFormat("#,###,###,##0.0000");
		} else if (deliveryOrder.getDecimal().equals("5")) {
			df = new DecimalFormat("#,###,###,##0.00000");
		} else if (deliveryOrder.getDecimal().equals("6")) {
			df = new DecimalFormat("#,###,###,##0.000000");
		} else {
			df = new DecimalFormat("#,###,###,##0.00");
		}

		try {
			Resource resource = applicationContext.getResource("classpath:reports/SupplierDoReport.jasper");
			File jasperfile = resource.getFile();

			DeliveryOrderSummaryPojo summary = new DeliveryOrderSummaryPojo();
			String createDate = deliveryOrder.getDoSendDate() != null ? sdf.format(deliveryOrder.getDoSendDate()).toUpperCase() : "";
			String deliveryDate = deliveryOrder.getDeliveryDate() != null ? sdf.format(deliveryOrder.getDeliveryDate()).toUpperCase() : "";

			summary.setDoName(deliveryOrder.getName());
			summary.setDeliveryId(deliveryOrder.getDeliveryId());
			summary.setPaymentNote("Hello");
			summary.setPoNumber(deliveryOrder.getPo().getPoNumber());
			summary.setCreatedDate(createDate);
			if (deliveryOrder.getFooter() != null) {
				summary.setFooter(deliveryOrder.getFooter().getContent());
			}
			SupplierSettings supplierSettings = supplierSettingsDao.getSupplierSettingsByTenantId(deliveryOrder.getSupplier().getId());

			if (supplierSettings != null) {
				ImageIcon n;
				if (supplierSettings.getFileAttatchment() != null) {
					n = new ImageIcon(supplierSettings.getFileAttatchment());
					summary.setComanyName(null);
				} else {
					n = new ImageIcon();
					summary.setComanyName(deliveryOrder.getSupplier().getCompanyName());
				}
				summary.setLogo(n.getImage());
			}
			getSummaryOfAddressAndDoitems(deliveryOrder, df, summary, deliveryDate);

			BusinessUnit bUnit = deliveryOrder.getBusinessUnit();
			String buyerAddress = "";

			if (bUnit != null) {
				summary.setDisplayName(bUnit.getUnitName());
			}
			if (StringUtils.checkString(deliveryOrder.getLine1()).length() > 0) {
				buyerAddress = deliveryOrder.getLine1() + "\r\n";
			}
			if (StringUtils.checkString(deliveryOrder.getLine2()).length() > 0) {
				buyerAddress += deliveryOrder.getLine2() + "\r\n";
			}
			if (StringUtils.checkString(deliveryOrder.getLine3()).length() > 0) {
				buyerAddress += deliveryOrder.getLine3() + "\r\n";
			}
			if (StringUtils.checkString(deliveryOrder.getLine4()).length() > 0) {
				buyerAddress += deliveryOrder.getLine4() + "\r\n";
			}
			if (StringUtils.checkString(deliveryOrder.getLine5()).length() > 0) {
				buyerAddress += deliveryOrder.getLine5() + "\r\n";
			}
			if (StringUtils.checkString(deliveryOrder.getLine6()).length() > 0) {
				buyerAddress += deliveryOrder.getLine6() + "\r\n";
			}
			if (StringUtils.checkString(deliveryOrder.getLine7()).length() > 0) {
				buyerAddress += deliveryOrder.getLine7() + "\r\n";
			}

			summary.setBuyerAddress(buyerAddress);

			List<DeliveryOrderSummaryPojo> deliveryOrderSummary = Arrays.asList(summary);

			// Supplier Address
			String supplierAddress = "";

			if (deliveryOrder.getSupplier() != null) {
				Supplier supplier = deliveryOrder.getSupplier();
				supplierAddress += supplier.getCompanyName() + "\r\n";
				supplierAddress += supplier.getLine1();
				if (StringUtils.checkString(deliveryOrder.getSupplier().getLine2()).length() > 0) {
					supplierAddress += "\r\n" + supplier.getLine2();
				}
				supplierAddress += "\r\n" + supplier.getCity() + ", ";
				if (supplier.getState() != null) {
					supplierAddress += supplier.getState().getStateName() + "\r\n\n";
				}
				supplierAddress += "TEL : ";

				if (supplier.getCompanyContactNumber() != null) {
					supplierAddress += supplier.getCompanyContactNumber();
				}
				supplierAddress += "\r\nFAX : ";
				if (supplier.getFaxNumber() != null) {
					supplierAddress += supplier.getFaxNumber() + "\r\n\n";
				}
				supplierAddress += "Company Reg No : ";

				if (supplier.getCompanyRegistrationNumber() != null) {
					supplierAddress += supplier.getCompanyRegistrationNumber();
				}
				supplierAddress += "\r\nTax Reg No : ";
				if (supplier.getTaxRegistrationNumber() != null) {
					supplierAddress += supplier.getTaxRegistrationNumber();
				}

			} else {
				supplierAddress += deliveryOrder.getSupplierName() + "\r\n";
				supplierAddress += deliveryOrder.getSupplierAddress() + "\r\n\n";
				supplierAddress += "TEL :";
				if (deliveryOrder.getSupplierTelNumber() != null) {
					supplierAddress += deliveryOrder.getSupplierTelNumber();
				}
				supplierAddress += "\r\nFAX : ";
				if (deliveryOrder.getSupplierFaxNumber() != null) {
					supplierAddress += deliveryOrder.getSupplierFaxNumber() + "\r\n\n";
				}

				supplierAddress += "Tax Reg No : ";
				if (deliveryOrder.getSupplierTaxNumber() != null) {
					supplierAddress += deliveryOrder.getSupplierTaxNumber();
				}
			}

			if (deliveryOrder.getSupplier() != null) {
				summary.setSupplierName(deliveryOrder.getSupplier() != null ? deliveryOrder.getSupplier().getCompanyName() : "");
			} else {
				summary.setSupplierName(deliveryOrder.getSupplierName());
			}
			summary.setSupplierAddress(supplierAddress);
			summary.setTaxnumber(deliveryOrder.getSupplierTaxNumber() != null ? deliveryOrder.getSupplierTaxNumber() : "");

			parameters.put("DELIVERYORDER_SUMMARY", deliveryOrderSummary);
			JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(deliveryOrderSummary, false);
			jasperPrint = JasperFillManager.fillReport(jasperfile.getPath(), parameters, beanCollectionDataSource);
		} catch (Exception e) {
			LOG.error("Could not generate Do  Report. " + e.getMessage(), e);
		}
		return jasperPrint;
	}

	private String getTimeZoneBySupplierSettings(String tenantId, String timeZone) {
		try {
			if (StringUtils.checkString(tenantId).length() > 0) {
				String time = supplierSettingsService.getSupplierTimeZoneByTenantId(tenantId);
				if (time != null) {
					timeZone = time;
				}
			}
		} catch (Exception e) {
			LOG.error("Error while fetching supplier time zone :" + e.getMessage(), e);
		}
		return timeZone;
	}

	private void sendEmail(String mailTo, String subject, Map<String, Object> map, String template) {
		if (StringUtils.checkString(mailTo).length() > 0) {
			try {
				LOG.info("Sending request email to : " + mailTo);
				String message = FreeMarkerTemplateUtils.processTemplateIntoString(freemarkerConfiguration.getTemplate(template), map);
				notificationService.sendEmail(mailTo, subject, message);
			} catch (Exception e) {
				LOG.error("ERROR while Sending mail :" + e.getMessage(), e);
			}
		} else {
			LOG.warn("No communication email configured for user... Not going to send email notification");
		}
	}

	private void sendEmailNotificationToSupplier(DeliveryOrder deliveryOrder, User actionBy, boolean isAccept, String buyerRemark) {

		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a (z)");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
		String timeZone = "GMT+8:00";
		timeZone = getTimeZoneBySupplierSettings(deliveryOrder.getCreatedBy().getTenantId(), timeZone);
		df.setTimeZone(TimeZone.getTimeZone(timeZone));
		sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
		String subject = null;
		if (isAccept) {
			subject = "DO Accepted";
		} else {
			subject = "DO Declined";
		}
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("supplierName", deliveryOrder.getSupplier() != null ? deliveryOrder.getSupplier().getCompanyName() : deliveryOrder.getSupplierName());
		map.put("doNumber", deliveryOrder.getDeliveryId());
		map.put("doDate", sdf.format(deliveryOrder.getDoSendDate()));
		map.put("poNumber", deliveryOrder.getPo() != null ? deliveryOrder.getPo().getPoNumber() : "N/A");
		map.put("dueDate", sdf.format(deliveryOrder.getDeliveryDate()));
		map.put("deliveryDate", sdf.format(deliveryOrder.getDeliveryDate()));
		map.put("comments", buyerRemark);
		map.put("buyerCompany", deliveryOrder.getBuyer().getCompanyName());

		map.put("buyerName", actionBy.getName());
		map.put("buyerLoginEmail", actionBy.getLoginId());
		map.put("loginUrl", APP_URL + "/login");
		map.put("appUrl", APP_URL + "/login");
		map.put("date", sdf.format(new Date()));
		sendEmail(deliveryOrder.getCreatedBy().getCommunicationEmail(), subject, map, isAccept ? Global.DO_ACCEPTED_TEMPLATE : Global.DO_DECLINE_TEMPLATE);

		SupplierNotificationMessage suppMessage = new SupplierNotificationMessage();
		suppMessage.setCreatedBy(actionBy);
		suppMessage.setCreatedDate(new Date());
		String message = null;
		if (isAccept) {
			message = messageSource.getMessage("do.declined", new Object[] { buyerRemark }, Global.LOCALE);
		} else {
			message = messageSource.getMessage("do.buyerAudit.accepted", new Object[] { buyerRemark }, Global.LOCALE);
		}
		suppMessage.setMessageTo(deliveryOrder.getCreatedBy());
		suppMessage.setMessage(message);
		suppMessage.setNotificationType(NotificationType.GENERAL);
		suppMessage.setSubject(subject);
		suppMessage.setTenantId(deliveryOrder.getSupplier().getId());
		dashboardNotificationService.saveSupplierNotificationMessage(suppMessage);

	}

	@Override
	public List<DoSupplierPojo> getAllBuyerDoDetailsForExcelReport(String tenantId, String[] doIds, DoSupplierPojo doSupplierPojo, boolean select_all, Date startDate, Date endDate, SimpleDateFormat sdf) {
		return deliveryOrderDao.getAllBuyerDoDetailsForExcelReport(tenantId, doIds, doSupplierPojo, select_all, startDate, endDate);
	}

	@Override
	public List<DoSupplierPojo> getAllSupplierDoDetailsForExcelReport(String tenantId, String[] doIds, DoSupplierPojo doSupplierPojo, boolean select_all, Date startDate, Date endDate, SimpleDateFormat sdf) {
		return deliveryOrderDao.getAllSupplierDoDetailsForExcelReport(tenantId, doIds, doSupplierPojo, select_all, startDate, endDate, sdf);
	}

	@Override
	@Transactional(readOnly = true)
	public List<DoSupplierPojo> getDosByPoId(String poId) {
		return deliveryOrderDao.getDosByPoId(poId);
	}

	@Override
	@Transactional(readOnly = true)
	public List<DoSupplierPojo> getDosByPoIdForBuyer(String poId) {
		return deliveryOrderDao.getDosByPoIdForBuyer(poId);
	}

	@Override
	public long findTotalDoForBuyerPo(String poId) {
		return deliveryOrderDao.findTotalDoForBuyerPo(poId);
	}

}