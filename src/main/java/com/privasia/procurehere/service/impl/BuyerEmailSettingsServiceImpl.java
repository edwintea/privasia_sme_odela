/**
 * 
 */
package com.privasia.procurehere.service.impl;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.privasia.procurehere.core.dao.BuyerAuditTrailDao;
import com.privasia.procurehere.core.dao.BuyerEmailSettingsDao;
import com.privasia.procurehere.core.entity.Buyer;
import com.privasia.procurehere.core.entity.BuyerAuditTrail;
import com.privasia.procurehere.core.entity.BuyerEmailSettings;
import com.privasia.procurehere.core.entity.User;
import com.privasia.procurehere.core.enums.AuditTypes;
import com.privasia.procurehere.core.enums.ModuleType;
import com.privasia.procurehere.core.supplier.dao.BuyerDao;
import com.privasia.procurehere.core.utils.Global;
import com.privasia.procurehere.service.BuyerEmailSettingsService;

/**
 * @author jayshree
 *
 */
@Service
@Transactional(readOnly=true)
public class BuyerEmailSettingsServiceImpl implements BuyerEmailSettingsService{

	private static final Logger LOG = Logger.getLogger(Global.BUYER_LOG);
	
	@Autowired
	BuyerEmailSettingsDao buyerEmailSettingsDao;
	
	@Autowired
	BuyerDao buyerDao;
	
	@Autowired
	BuyerAuditTrailDao buyerAuditTrailDao;
	
	@Override
	public BuyerEmailSettings getEmailSettingByTenantId(String tenantId) {
		BuyerEmailSettings emailSettings = buyerEmailSettingsDao.getEmailSettingByTenantId(tenantId);
		if(emailSettings == null) {
			return new BuyerEmailSettings();
		}else {
			return emailSettings;
		}
	}

	@Override
	@Transactional(readOnly=false)
	public void saveBuyerEmailSettings(String tenantId, BuyerEmailSettings buyerEmailSettings, String emailPassword, User user) {
		BuyerEmailSettings emailSettings = buyerEmailSettingsDao.getBuyerEmailSettingsByIdAndTenantId(tenantId, buyerEmailSettings.getId());
		Buyer buyer = buyerDao.findById(tenantId);

		if(emailSettings == null) {
			BuyerEmailSettings newSettings = new BuyerEmailSettings();
			
			newSettings.setHost(buyerEmailSettings.getHost());
			newSettings.setPort(buyerEmailSettings.getPort());
			newSettings.setTls(buyerEmailSettings.getTls());
			newSettings.setEmailAddress(buyerEmailSettings.getEmailAddress());
			newSettings.setUserName(buyerEmailSettings.getUserName());
			newSettings.setPassword(emailPassword);
			newSettings.setBuyer(buyer);
			newSettings.setModifiedBy(user);
			newSettings.setModifiedDate(new Date());
			buyerEmailSettingsDao.saveOrUpdate(newSettings);
			LOG.info("Saved Successfully>>>>>>>>>>>>>>>>>");
		}else {
			LOG.info("Update >>>>>>>>>>>>>>>>>");
			emailSettings.setHost(buyerEmailSettings.getHost());
			emailSettings.setPort(buyerEmailSettings.getPort());
			emailSettings.setTls(buyerEmailSettings.getTls());
			emailSettings.setEmailAddress(buyerEmailSettings.getEmailAddress());
			emailSettings.setUserName(buyerEmailSettings.getUserName());
			emailSettings.setPassword(emailPassword);
			emailSettings.setBuyer(buyer);
			emailSettings.setModifiedBy(user);
			emailSettings.setModifiedDate(new Date());
			buyerEmailSettingsDao.saveOrUpdate(emailSettings);
			LOG.info("Updated Successfully>>>>>>>>>>>>>>>>>");
		}
		
		try {
			BuyerAuditTrail auditTrail = new BuyerAuditTrail(AuditTypes.UPDATE, "Email Settings updated", tenantId, user, new Date(), ModuleType.EmailSettings);
			buyerAuditTrailDao.save(auditTrail);
			LOG.info("Audit Trail Saved >>>>>>>>>>>>>>>>>");
		}catch(Exception e){
			
		}
	}

	@Override
	public BuyerEmailSettings getEmailSetting() {
		return buyerEmailSettingsDao.getEmailSetting();
	}

}
