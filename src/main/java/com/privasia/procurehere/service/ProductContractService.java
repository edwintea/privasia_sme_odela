package com.privasia.procurehere.service;

import java.util.List;

import com.privasia.procurehere.core.entity.ProductContract;
import com.privasia.procurehere.core.entity.User;
import com.privasia.procurehere.core.pojo.ProductContractPojo;
import com.privasia.procurehere.core.pojo.TableDataInput;

public interface ProductContractService {

	ProductContract findProductContractByReferenceNumber(String contractReferenceNumber, String tenantId);

	/**
	 * @param productContract
	 * @return
	 */

	ProductContract createProductContract(ProductContract productContract);

	/**
	 * @param productContract
	 * @param true1
	 * @return TODO
	 */
	ProductContract update(ProductContract persistObj);

	/**
	 * @param loggedInUserTenantId
	 * @param input
	 * @return
	 */
	List<ProductContractPojo> findProductContractListForTenant(String loggedInUserTenantId, TableDataInput input);

	/**
	 * @param loggedInUserTenantId
	 * @param input
	 * @return
	 */
	long findTotalFilteredProductListForTenant(String loggedInUserTenantId, TableDataInput input);

	/**
	 * @param loggedInUserTenantId
	 * @return
	 */
	long findTotalProductListForTenant(String loggedInUserTenantId);

	/**
	 * @param id
	 * @param string
	 * @return
	 */
	ProductContract findProductContractById(String id, String string);

	/**
	 * @param id
	 * @return
	 */
	ProductContract getProductContractById(String id);

}
