package com.privasia.procurehere.service;

import com.privasia.procurehere.core.entity.SupplierMandatoryDocument;

import java.util.List;

/**
 * @author Edwin
 * @author Edwin
 */

public interface SupplierMandatoryDocumentService {

	/**
	 * @param
	 * @return
	 */
	List<SupplierMandatoryDocument> getAll();

	/**
	 * @param id
	 */

	SupplierMandatoryDocument findByIds(String id);


	boolean isExistsByActiveTitle(SupplierMandatoryDocument document);

	/**
	 * @param id
	 */
	void deleteDocument(String id);
}
